import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'bottomnav_event.dart';
part 'bottomnav_state.dart';

class BottomnavBloc extends Bloc<BottomnavEvent, BottomnavState> {
  BottomnavBloc() : super(FirstShow());

  @override
  Stream<BottomnavState> mapEventToState(
    BottomnavEvent event,
  ) async* {
    if (event is First) {
      yield FirstShow();
    }
    if (event is Second) {
      yield SecondShow();
    }
    if (event is Third) {
      yield ThirdShow();
    }
  }
}
