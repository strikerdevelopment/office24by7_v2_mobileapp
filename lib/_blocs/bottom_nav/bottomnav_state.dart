part of 'bottomnav_bloc.dart';

abstract class BottomnavState extends Equatable {
  const BottomnavState();
  @override
  List<Object> get props => [];
}

class FirstShow extends BottomnavState {
  final String title = "First";
  final int itemIndex = 0;
}

class SecondShow extends BottomnavState {
  final String title = "Second";
  final int itemIndex = 1;
}

class ThirdShow extends BottomnavState {
  final String title = "Third";
  final int itemIndex = 3;
}
