import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/callog_model/caller_id_model.dart';

part 'caller_ids_event.dart';
part 'caller_ids_state.dart';

class CallerIdsBloc extends Bloc<CallerIdsEvent, CallerIdsState> {
  final CallogRepository callogRepository;
  final Environment environment = Environment();

  CallerIdsBloc({this.callogRepository})
      : assert(callogRepository != null),
        super(CallerIdsInitial());

  @override
  Stream<CallerIdsState> mapEventToState(
    CallerIdsEvent event,
  ) async* {
    if (event is CallerIdsPressed) {
      yield CallerIdsInProgress();
      try {
        List<CallerIdModel> getData =
            await callogRepository.getCallerIds(await environment.getToken());
        yield CallerIdsSuccess(stateData: getData);
      } catch (_) {
        yield CallerIdsFailure(error: _.toString());
      }
    }
  }
}
