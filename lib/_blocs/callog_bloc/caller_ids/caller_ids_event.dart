part of 'caller_ids_bloc.dart';

abstract class CallerIdsEvent extends Equatable {
  const CallerIdsEvent();

  @override
  List<Object> get props => [];
}

class CallerIdsPressed extends CallerIdsEvent {
  const CallerIdsPressed();

  @override
  List<Object> get props => [];
}
