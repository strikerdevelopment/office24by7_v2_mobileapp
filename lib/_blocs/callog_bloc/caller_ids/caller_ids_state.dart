part of 'caller_ids_bloc.dart';

abstract class CallerIdsState extends Equatable {
  const CallerIdsState();

  @override
  List<Object> get props => [];
}

class CallerIdsInitial extends CallerIdsState {}

class CallerIdsInProgress extends CallerIdsState {}

class CallerIdsSuccess extends CallerIdsState {
  final List<CallerIdModel> stateData;

  CallerIdsSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'CallerIdsSuccess { stateData: ${stateData.length} }';
}

class CallerIdsFailure extends CallerIdsState {
  final String error;
  const CallerIdsFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'CallerIdsFailure { error: $error }';
}
