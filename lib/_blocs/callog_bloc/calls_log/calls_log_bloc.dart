import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/callog_model/callog.dart';

part 'calls_log_event.dart';
part 'calls_log_state.dart';

class CallsLogBloc extends Bloc<CallsLogEvent, CallsLogState> {
  final CallogRepository callogRepository;
  final Environment environment = Environment();

  CallsLogBloc({this.callogRepository}) : super(CallsLogInitial());

  @override
  Stream<CallsLogState> mapEventToState(
    CallsLogEvent event,
  ) async* {
    final currentState = state;

    if (event is CallsLogPressed && !_hasReachedMax(currentState)) {
      try {
        if (state is CallsLogInitial) {
          List<CallsLogModel> getData = await callogRepository.getCallsLog(
              await environment.getToken(), Environment.dataDisplayCount, 0);
          yield CallsLogSuccess(
              stateData: getData, hasReachedMax: false, currentState: getData);
        }
        if (currentState is CallsLogSuccess) {
          final stateData = await callogRepository.getCallsLog(
            await environment.getToken(),
            Environment.dataDisplayCount,
            currentState.stateData.length,
          );
          yield stateData.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : CallsLogSuccess(
                  stateData: currentState.stateData + stateData,
                  hasReachedMax: false,
                  currentState: stateData);
        }
      } catch (_) {
        yield CallsLogFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(CallsLogState state) =>
      state is CallsLogSuccess && state.hasReachedMax;
}
