part of 'calls_log_bloc.dart';

abstract class CallsLogEvent extends Equatable {
  const CallsLogEvent();

  @override
  List<Object> get props => [];
}

class CallsLogPressed extends CallsLogEvent {
  CallsLogPressed();

  @override
  @override
  List<Object> get props => [];

  // @override
  // String toString() => 'CallsLogPressed { searchStatusId: $displayAs }';
}
