part of 'calls_log_bloc.dart';

abstract class CallsLogState extends Equatable {
  const CallsLogState();

  @override
  List<Object> get props => [];
}

class CallsLogInitial extends CallsLogState {}

class CallsLogSuccess extends CallsLogState {
  final List<CallsLogModel> stateData;
  final bool hasReachedMax;
  final List<CallsLogModel> currentState;

  CallsLogSuccess({this.stateData, this.hasReachedMax, this.currentState});

  CallsLogSuccess copyWith(
      {List<CallsLogModel> stateData, bool hasReachedMax}) {
    return CallsLogSuccess(
        stateData: stateData ?? this.stateData,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [stateData, currentState];

  @override
  String toString() => 'CallsLogSuccess { stateData: ${stateData.length} }';
}

class CallsLogFailure extends CallsLogState {
  final String error;
  const CallsLogFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'CallsLogFailure { error: $error }';
}
