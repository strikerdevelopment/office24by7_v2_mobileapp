import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/callog_model/callog.dart';

part 'calls_log_history_event.dart';
part 'calls_log_history_state.dart';

class CallsLogHistoryBloc
    extends Bloc<CallsLogHistoryEvent, CallsLogHistoryState> {
  final CallogRepository callogRepository;
  final Environment environment = Environment();

  CallsLogHistoryBloc({this.callogRepository})
      : super(CallsLogHistoryInitial());

  @override
  Stream<CallsLogHistoryState> mapEventToState(
    CallsLogHistoryEvent event,
  ) async* {
    final currentState = state;

    if (event is CallsLogHistoryPressed && !_hasReachedMax(currentState)) {
      try {
        if (state is CallsLogHistoryInitial) {
          List<CallsLogHistoryModel> getData =
              await callogRepository.getCallsLogHistory(
                  await environment.getToken(),
                  event.callerPhoneNumber,
                  Environment.dataDisplayCount,
                  0);
          yield CallsLogHistorySuccess(
              stateData: getData, hasReachedMax: false, currentState: getData);
        }
        if (currentState is CallsLogHistorySuccess) {
          final stateData = await callogRepository.getCallsLogHistory(
            await environment.getToken(),
            event.callerPhoneNumber,
            Environment.dataDisplayCount,
            currentState.stateData.length,
          );
          yield stateData.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : CallsLogHistorySuccess(
                  stateData: currentState.stateData + stateData,
                  hasReachedMax: false,
                  currentState: stateData);
        }
      } catch (_) {
        yield CallsLogHistoryFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(CallsLogHistoryState state) =>
      state is CallsLogHistorySuccess && state.hasReachedMax;
}
