part of 'calls_log_history_bloc.dart';

abstract class CallsLogHistoryEvent extends Equatable {
  const CallsLogHistoryEvent();

  @override
  List<Object> get props => [];
}

class CallsLogHistoryPressed extends CallsLogHistoryEvent {
  final String callerPhoneNumber;

  CallsLogHistoryPressed({this.callerPhoneNumber});

  @override
  List<Object> get props => [];

  // @override
  // String toString() => 'CallsLogHistoryPressed { searchStatusId: $displayAs }';
}
