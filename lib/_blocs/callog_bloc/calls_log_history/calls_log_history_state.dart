part of 'calls_log_history_bloc.dart';

abstract class CallsLogHistoryState extends Equatable {
  const CallsLogHistoryState();

  @override
  List<Object> get props => [];
}

class CallsLogHistoryInitial extends CallsLogHistoryState {}

class CallsLogHistorySuccess extends CallsLogHistoryState {
  final List<CallsLogHistoryModel> stateData;
  final bool hasReachedMax;
  final List<CallsLogHistoryModel> currentState;

  CallsLogHistorySuccess(
      {this.stateData, this.hasReachedMax, this.currentState});

  CallsLogHistorySuccess copyWith(
      {List<CallsLogHistoryModel> stateData, bool hasReachedMax}) {
    return CallsLogHistorySuccess(
        stateData: stateData ?? this.stateData,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [stateData, currentState];

  @override
  String toString() =>
      'CallsLogHistorySuccess { stateData: ${stateData.length} }';
}

class CallsLogHistoryFailure extends CallsLogHistoryState {
  final String error;
  const CallsLogHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'CallsLogHistoryFailure { error: $error }';
}
