import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'click_to_call_event.dart';
part 'click_to_call_state.dart';

class ClickToCallBloc extends Bloc<ClickToCallEvent, ClickToCallState> {
  final CallogRepository callogRepository;
  final Environment environment = Environment();

  ClickToCallBloc({this.callogRepository})
      : assert(callogRepository != null),
        super(ClickToCallInitial());

  @override
  Stream<ClickToCallState> mapEventToState(
    ClickToCallEvent event,
  ) async* {
    if (event is ClickToCallPressed) {
      yield ClickToCallInProgress();
      try {
        if (await environment.getUserRole() == '2') {
          String getData = await callogRepository.getClicktoCall(
              await environment.getToken(),
              event.phoneNumber,
              event.callerId,
              event.leadId,
              event.contactId,
              event.callId);
          yield ClickToCallSuccess(stateData: getData);
        } else {
          throw 'Sorry, We allowing Call Functionality for Executive Only.';
        }
      } catch (_) {
        yield ClickToCallFailure(error: _.toString());
      }
    }
  }
}
