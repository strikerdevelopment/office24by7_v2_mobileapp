part of 'click_to_call_bloc.dart';

abstract class ClickToCallEvent extends Equatable {
  const ClickToCallEvent();

  @override
  List<Object> get props => [];
}

class ClickToCallPressed extends ClickToCallEvent {
  final String phoneNumber;
  final String callerId;
  final String leadId;
  final String contactId;
  final String callId;
  const ClickToCallPressed(
      {this.phoneNumber,
      this.callerId,
      this.leadId,
      this.contactId,
      this.callId});

  @override
  List<Object> get props => [phoneNumber];
}
