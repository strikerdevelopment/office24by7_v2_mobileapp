part of 'click_to_call_bloc.dart';

abstract class ClickToCallState extends Equatable {
  const ClickToCallState();

  @override
  List<Object> get props => [];
}

class ClickToCallInitial extends ClickToCallState {}

class ClickToCallInProgress extends ClickToCallState {}

class ClickToCallSuccess extends ClickToCallState {
  final String stateData;

  ClickToCallSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ClickToCallSuccess { stateData: $stateData }';
}

class ClickToCallFailure extends ClickToCallState {
  final String error;
  const ClickToCallFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ClickToCallFailure { error: $error }';
}
