import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'add_chat_group_event.dart';
part 'add_chat_group_state.dart';

class AddChatGroupBloc extends Bloc<AddChatGroupEvent, AddChatGroupState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  AddChatGroupBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(AddChatGroupInitial());

  @override
  Stream<AddChatGroupState> mapEventToState(
    AddChatGroupEvent event,
  ) async* {
    if (event is AddChatGroupPressed) {
      yield AddChatGroupInProgress();
      try {
        String getData = await chatRepository.getAddChartGroup(
          await environment.getToken(),
          event.formData,
          event.groupMembers,
          event.groupId,
          event.groupUniqueId,
        );
        yield AddChatGroupSuccess(stateData: getData);
      } catch (_) {
        yield AddChatGroupFailure(error: _.toString());
      }
    }
  }
}
