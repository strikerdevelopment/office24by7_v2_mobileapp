part of 'add_chat_group_bloc.dart';

abstract class AddChatGroupEvent extends Equatable {
  const AddChatGroupEvent();

  @override
  List<Object> get props => [];
}

class AddChatGroupPressed extends AddChatGroupEvent {
  final Map formData;
  final List<String> groupMembers;
  final String groupId;
  final String groupUniqueId;

  const AddChatGroupPressed(
      {this.formData, this.groupMembers, this.groupId, this.groupUniqueId});

  @override
  List<Object> get props => [groupId];
}
