part of 'add_chat_group_bloc.dart';

abstract class AddChatGroupState extends Equatable {
  const AddChatGroupState();

  @override
  List<Object> get props => [];
}

class AddChatGroupInitial extends AddChatGroupState {}

class AddChatGroupInProgress extends AddChatGroupState {}

class AddChatGroupSuccess extends AddChatGroupState {
  final String stateData;

  AddChatGroupSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'AddChatGroupSuccess { stateData: $stateData }';
}

class AddChatGroupFailure extends AddChatGroupState {
  final String error;
  const AddChatGroupFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddChatGroupFailure { error: $error }';
}
