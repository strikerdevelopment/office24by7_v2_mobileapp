import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'add_live_chat_session_state.dart';

class AddLiveChatSessionCubit extends Cubit<AddLiveChatSessionState> {
  final ChatRepository chatRepository;
  final GenericRepository genericRepository;

  final Environment environment = Environment();

  AddLiveChatSessionCubit({this.chatRepository, this.genericRepository})
      : assert(chatRepository != null),
        assert(genericRepository != null),
        super(AddLiveChatSessionInitial());

  Future<void> addChatLiveSessionPressed() async {
    try {
      emit(AddLiveChatSessionInitial());
      final String data = await chatRepository.getAddLiveChatSession(
          await environment.getToken(), await environment.getSession());
      final AuthModel authData = await genericRepository.getGenericAPIdata(
          await environment.getToken(),
          GenericModel(apiName: 'PolUserauth', versionNumber: ""));
      emit(AddLiveChatSessionSuccess(stateData: data, authData: authData));
    } catch (_) {
      emit(AddLiveChatSessionFailure(error: _.toString()));
    }
  }
}
