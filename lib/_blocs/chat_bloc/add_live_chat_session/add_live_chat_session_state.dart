part of 'add_live_chat_session_cubit.dart';

abstract class AddLiveChatSessionState extends Equatable {
  const AddLiveChatSessionState();

  @override
  List<Object> get props => [];
}

class AddLiveChatSessionInitial extends AddLiveChatSessionState {}

class AddLiveChatSessionSuccess extends AddLiveChatSessionState {
  final String stateData;
  final AuthModel authData;

  AddLiveChatSessionSuccess({this.stateData, this.authData});

  @override
  List<Object> get props => [stateData, authData];

  @override
  String toString() =>
      'AddLiveChatSessionSuccess { stateData: $stateData, authData: ${authData.isChatEnabled} }';
}

class AddLiveChatSessionFailure extends AddLiveChatSessionState {
  final String error;

  AddLiveChatSessionFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AddLiveChatSessionFailure { stateData: $error }';
}
