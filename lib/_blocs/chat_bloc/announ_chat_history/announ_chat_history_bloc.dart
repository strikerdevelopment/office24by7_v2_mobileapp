import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'announ_chat_history_event.dart';
part 'announ_chat_history_state.dart';

class AnnounChatHistoryBloc
    extends Bloc<AnnounChatHistoryEvent, AnnounChatHistoryState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  AnnounChatHistoryBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(AnnounChatHistoryInitial());

  @override
  Stream<AnnounChatHistoryState> mapEventToState(
      AnnounChatHistoryEvent event) async* {
    final currentState = state;
    if (event is AnnounChatHistoryPressed && !_hasReachedMax(currentState)) {
      try {
        if (state is AnnounChatHistoryInitial) {
          List<ChatHistoryModel> getData =
              await chatRepository.getAnnounChatHistory(
                  await environment.getToken(),
                  event.groupId,
                  event.groupUniqueId,
                  event.groupType,
                  0);
          yield AnnounChatHistorySuccess(
            stateData: getData,
            hasReachedMax: false,
            currentState: getData,
          );
          return;
        }
        if (currentState is AnnounChatHistorySuccess) {
          List<ChatHistoryModel> getData =
              await chatRepository.getAnnounChatHistory(
                  await environment.getToken(),
                  event.groupId,
                  event.groupUniqueId,
                  event.groupType,
                  currentState.stateData.length);
          yield getData.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : AnnounChatHistorySuccess(
                  stateData: currentState.stateData + getData,
                  hasReachedMax: false,
                  currentState: getData,
                );
        }
      } catch (_) {
        yield AnnounChatHistoryFailure(error: _.toString());
      }
    }

    if (event is AnnounChatHistoryRefresh) {
      try {
        List<ChatHistoryModel> getData =
            await chatRepository.getAnnounChatHistory(
                await environment.getToken(),
                event.groupId,
                event.groupUniqueId,
                event.groupType,
                event.offset);
        yield AnnounChatHistorySuccess(
          stateData: getData,
          hasReachedMax: false,
          currentState: getData,
        );
        return;
      } catch (_) {
        yield AnnounChatHistoryFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(AnnounChatHistoryState state) =>
      state is AnnounChatHistorySuccess && state.hasReachedMax;
}
