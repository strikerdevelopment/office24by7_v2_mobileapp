part of 'announ_chat_history_bloc.dart';

abstract class AnnounChatHistoryEvent extends Equatable {
  const AnnounChatHistoryEvent();

  @override
  List<Object> get props => [];
}

class AnnounChatHistoryPressed extends AnnounChatHistoryEvent {
  final String groupId;
  final String groupUniqueId;
  final String groupType;
  final int offset;

  const AnnounChatHistoryPressed(
      {this.offset, this.groupId, this.groupUniqueId, this.groupType});

  @override
  List<Object> get props => [offset];
}

class AnnounChatHistoryRefresh extends AnnounChatHistoryEvent {
  final String groupId;
  final String groupUniqueId;
  final String groupType;
  final int offset;

  const AnnounChatHistoryRefresh(
      {this.offset, this.groupId, this.groupUniqueId, this.groupType});

  @override
  List<Object> get props => [offset];
}
