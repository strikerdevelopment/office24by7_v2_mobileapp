part of 'announ_chat_history_bloc.dart';

abstract class AnnounChatHistoryState extends Equatable {
  const AnnounChatHistoryState();

  @override
  List<Object> get props => [];
}

class AnnounChatHistoryInitial extends AnnounChatHistoryState {}

class AnnounChatHistorySuccess extends AnnounChatHistoryState {
  final List<ChatHistoryModel> stateData;
  final bool hasReachedMax;
  final List<ChatHistoryModel> currentState;

  AnnounChatHistorySuccess(
      {this.stateData, this.hasReachedMax, this.currentState});

  AnnounChatHistorySuccess copyWith(
      {List<ChatHistoryModel> stateData, bool hasReachedMax}) {
    return AnnounChatHistorySuccess(
      stateData: stateData ?? this.stateData,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      currentState: currentState == null ? [] : currentState,
    );
  }

  @override
  List<Object> get props => [stateData, hasReachedMax, currentState];

  @override
  String toString() =>
      'AnnounChatHistorySuccess { stateData: ${stateData.length}, hasReachedMax: $hasReachedMax }';
}

class AnnounChatHistoryFailure extends AnnounChatHistoryState {
  final String error;
  const AnnounChatHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'AnnounChatHistoryFailure { error: $error }';
}
