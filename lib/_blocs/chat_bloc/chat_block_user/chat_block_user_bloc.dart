import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'chat_block_user_event.dart';
part 'chat_block_user_state.dart';

class ChatBlockUserBloc extends Bloc<ChatBlockUserEvent, ChatBlockUserState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ChatBlockUserBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(ChatBlockUserInitial());

  @override
  Stream<ChatBlockUserState> mapEventToState(
    ChatBlockUserEvent event,
  ) async* {
    if (event is ChatBlockUserPressed) {
      yield ChatBlockUserInProgress();
      try {
        String getData = await chatRepository.getBlockUser(
          await environment.getToken(),
          await environment.getUserId(),
          event.userId,
        );
        yield ChatBlockUserSuccess(stateData: getData);
      } catch (_) {
        yield ChatBlockUserFailure(error: _.toString());
      }
    }
  }
}
