part of 'chat_block_user_bloc.dart';

abstract class ChatBlockUserEvent extends Equatable {
  const ChatBlockUserEvent();

  @override
  List<Object> get props => [];
}

class ChatBlockUserPressed extends ChatBlockUserEvent {
  final String userId;
  const ChatBlockUserPressed({this.userId});

  @override
  List<Object> get props => [userId];
}
