part of 'chat_block_user_bloc.dart';

abstract class ChatBlockUserState extends Equatable {
  const ChatBlockUserState();

  @override
  List<Object> get props => [];
}

class ChatBlockUserInitial extends ChatBlockUserState {}

class ChatBlockUserInProgress extends ChatBlockUserState {}

class ChatBlockUserSuccess extends ChatBlockUserState {
  final String stateData;

  ChatBlockUserSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ChatBlockUserSuccess { stateData: $stateData }';
}

class ChatBlockUserFailure extends ChatBlockUserState {
  final String error;
  const ChatBlockUserFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatBlockUserFailure { error: $error }';
}
