import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_group_info_model.dart';

part 'chat_group_info_event.dart';
part 'chat_group_info_state.dart';

class ChatGroupInfoBloc extends Bloc<ChatGroupInfoEvent, ChatGroupInfoState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ChatGroupInfoBloc({this.chatRepository}) : super(ChatGroupInfoInitial());

  @override
  Stream<ChatGroupInfoState> mapEventToState(
    ChatGroupInfoEvent event,
  ) async* {
    if (event is ChatGroupInfoPressed) {
      yield ChatGroupInfoInitial();
      try {
        ChatGroupInfoModel getData = await chatRepository.getChatGroupInfo(
            await environment.getToken(), event.groupId, event.groupUniqueId);
        yield ChatGroupInfoSuccess(stateData: getData);
      } catch (_) {
        yield ChatGroupInfoFailure(error: _.toString());
      }
    }
  }
}
