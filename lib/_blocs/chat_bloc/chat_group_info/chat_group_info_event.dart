part of 'chat_group_info_bloc.dart';

abstract class ChatGroupInfoEvent extends Equatable {
  const ChatGroupInfoEvent();

  @override
  List<Object> get props => [];
}

class ChatGroupInfoPressed extends ChatGroupInfoEvent {
  final String groupId;
  final String groupUniqueId;

  ChatGroupInfoPressed({this.groupId, this.groupUniqueId});

  @override
  List<Object> get props => [groupId];
}
