part of 'chat_group_info_bloc.dart';

abstract class ChatGroupInfoState extends Equatable {
  const ChatGroupInfoState();

  @override
  List<Object> get props => [];
}

class ChatGroupInfoInitial extends ChatGroupInfoState {}

class ChatGroupInfoSuccess extends ChatGroupInfoState {
  final ChatGroupInfoModel stateData;

  ChatGroupInfoSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ChatGroupInfoSuccess { stateData: $stateData }';
}

class ChatGroupInfoFailure extends ChatGroupInfoState {
  final String error;
  const ChatGroupInfoFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatGroupInfoFailure { error: $error }';
}
