import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_history_model.dart';

part 'chat_history_event.dart';
part 'chat_history_state.dart';

class ChatHistoryBloc extends Bloc<ChatHistoryEvent, ChatHistoryState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ChatHistoryBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(ChatHistoryInitial());

  @override
  Stream<ChatHistoryState> mapEventToState(ChatHistoryEvent event) async* {
    final currentState = state;
    if (event is ChatHistoryPressed && !_hasReachedMax(currentState)) {
      try {
        if (state is ChatHistoryInitial) {
          List<ChatHistoryModel> getData =
              await chatRepository.getChatHistoryDetails(
                  await environment.getToken(),
                  await environment.getUserId(),
                  event.toLoginId,
                  0);
          yield ChatHistorySuccess(
            stateData: getData,
            hasReachedMax: false,
            currentState: getData,
          );
          return;
        }
        if (currentState is ChatHistorySuccess) {
          List<ChatHistoryModel> getData =
              await chatRepository.getChatHistoryDetails(
                  await environment.getToken(),
                  await environment.getUserId(),
                  event.toLoginId,
                  currentState.stateData.length);
          yield getData.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ChatHistorySuccess(
                  stateData: currentState.stateData + getData,
                  hasReachedMax: false,
                  currentState: getData,
                );
        }
      } catch (_) {
        yield ChatHistoryFailure(error: _.toString());
      }
    }

    if (event is ChatHistoryRefresh) {
      try {
        List<ChatHistoryModel> getData =
            await chatRepository.getChatHistoryDetails(
                await environment.getToken(),
                await environment.getUserId(),
                event.toLoginId,
                event.offset);
        yield ChatHistorySuccess(
          stateData: getData,
          hasReachedMax: false,
          currentState: getData,
        );
        return;
      } catch (_) {
        yield ChatHistoryFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ChatHistoryState state) =>
      state is ChatHistorySuccess && state.hasReachedMax;
}
