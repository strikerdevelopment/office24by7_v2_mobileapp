part of 'chat_history_bloc.dart';

abstract class ChatHistoryEvent extends Equatable {
  const ChatHistoryEvent();

  @override
  List<Object> get props => [];
}

class ChatHistoryPressed extends ChatHistoryEvent {
  final int offset;
  final String toLoginId;

  const ChatHistoryPressed({this.offset, this.toLoginId});

  @override
  List<Object> get props => [offset];
}

class ChatHistoryRefresh extends ChatHistoryEvent {
  final int offset;
  final String toLoginId;

  const ChatHistoryRefresh({this.offset, this.toLoginId});

  @override
  List<Object> get props => [offset];
}
