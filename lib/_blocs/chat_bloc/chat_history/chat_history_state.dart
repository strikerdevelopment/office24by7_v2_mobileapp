part of 'chat_history_bloc.dart';

abstract class ChatHistoryState extends Equatable {
  const ChatHistoryState();

  @override
  List<Object> get props => [];
}

class ChatHistoryInitial extends ChatHistoryState {}

class ChatHistorySuccess extends ChatHistoryState {
  final List<ChatHistoryModel> stateData;
  final bool hasReachedMax;
  final List<ChatHistoryModel> currentState;

  ChatHistorySuccess({this.stateData, this.hasReachedMax, this.currentState});

  ChatHistorySuccess copyWith(
      {List<ChatHistoryModel> stateData, bool hasReachedMax}) {
    return ChatHistorySuccess(
      stateData: stateData ?? this.stateData,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      currentState: currentState == null ? [] : currentState,
    );
  }

  @override
  List<Object> get props => [stateData, hasReachedMax, currentState];

  @override
  String toString() =>
      'ChatHistorySuccess { stateData: ${stateData.length}, hasReachedMax: $hasReachedMax }';
}

class ChatHistoryFailure extends ChatHistoryState {
  final String error;
  const ChatHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatHistoryFailure { error: $error }';
}
