import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';

part 'chat_user_info_event.dart';
part 'chat_user_info_state.dart';

class ChatUserInfoBloc extends Bloc<ChatUserInfoEvent, ChatUserInfoState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ChatUserInfoBloc({this.chatRepository}) : super(ChatUserInfoInitial());

  @override
  Stream<ChatUserInfoState> mapEventToState(
    ChatUserInfoEvent event,
  ) async* {
    if (event is ChatUserInfoPressed) {
      yield ChatUserInfoInitial();
      try {
        ChatUserInfoModel getData = await chatRepository.getChatUserInfo(
            await environment.getToken(),
            event.userId == null
                ? await environment.getUserId()
                : event.userId);
        yield ChatUserInfoSuccess(stateData: getData);
      } catch (_) {
        yield ChatUserInfoFailure(error: _.toString());
      }
    }
  }
}
