part of 'chat_user_info_bloc.dart';

abstract class ChatUserInfoEvent extends Equatable {
  const ChatUserInfoEvent();

  @override
  List<Object> get props => [];
}

class ChatUserInfoPressed extends ChatUserInfoEvent {
  final String userId;

  ChatUserInfoPressed({this.userId});

  @override
  List<Object> get props => [];
}
