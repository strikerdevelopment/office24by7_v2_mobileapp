part of 'chat_user_info_bloc.dart';

abstract class ChatUserInfoState extends Equatable {
  const ChatUserInfoState();

  @override
  List<Object> get props => [];
}

class ChatUserInfoInitial extends ChatUserInfoState {}

class ChatUserInfoSuccess extends ChatUserInfoState {
  final ChatUserInfoModel stateData;

  ChatUserInfoSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ChatUserInfoSuccess { stateData: $stateData }';
}

class ChatUserInfoFailure extends ChatUserInfoState {
  final String error;
  const ChatUserInfoFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatUserInfoFailure { error: $error }';
}
