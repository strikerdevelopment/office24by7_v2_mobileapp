import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';

part 'chat_users_event.dart';
part 'chat_users_state.dart';

class ChatUsersBloc extends Bloc<ChatUsersEvent, ChatUsersState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ChatUsersBloc({this.chatRepository}) : super(ChatUsersInitial());

  @override
  Stream<ChatUsersState> mapEventToState(
    ChatUsersEvent event,
  ) async* {
    if (event is ChatUsersPressed) {
      yield ChatUsersInitial();
      try {
        ChatUsersModel getData = await chatRepository.getAllUsers(
            await environment.getToken(),
            event.type,
            event.searchTerm,
            event.isHierarchyUsers);
        yield ChatUsersSuccess(stateData: getData);
      } catch (_) {
        yield ChatUsersFailure(error: _.toString());
      }
    }
    if (event is ChatUsersPressedRefresh) {
      try {
        ChatUsersModel getData = await chatRepository.getAllUsers(
            await environment.getToken(),
            event.type,
            event.searchTerm,
            event.isHierarchyUsers);
        yield ChatUsersSuccess(stateData: getData);
      } catch (_) {
        yield ChatUsersFailure(error: _.toString());
      }
    }
  }
}
