part of 'chat_users_bloc.dart';

abstract class ChatUsersEvent extends Equatable {
  const ChatUsersEvent();

  @override
  List<Object> get props => [];
}

class ChatUsersPressed extends ChatUsersEvent {
  final String type;
  final String searchTerm;
  final String isHierarchyUsers;

  ChatUsersPressed({this.type, this.searchTerm, this.isHierarchyUsers});

  @override
  List<Object> get props => [searchTerm, isHierarchyUsers];
}

class ChatUsersPressedRefresh extends ChatUsersEvent {
  final String type;
  final String searchTerm;
  final String isHierarchyUsers;

  ChatUsersPressedRefresh({this.type, this.searchTerm, this.isHierarchyUsers});

  @override
  List<Object> get props => [searchTerm, isHierarchyUsers];
}
