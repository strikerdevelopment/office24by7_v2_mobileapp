part of 'chat_users_bloc.dart';

abstract class ChatUsersState extends Equatable {
  const ChatUsersState();

  @override
  List<Object> get props => [];
}

class ChatUsersInitial extends ChatUsersState {}

class ChatUsersSuccess extends ChatUsersState {
  final ChatUsersModel stateData;

  ChatUsersSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ChatUsersSuccess { stateData: $stateData }';
}

class ChatUsersFailure extends ChatUsersState {
  final String error;
  const ChatUsersFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatUsersFailure { error: $error }';
}
