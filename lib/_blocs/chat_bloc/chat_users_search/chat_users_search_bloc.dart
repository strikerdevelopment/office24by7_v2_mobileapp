import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';

part 'chat_users_search_event.dart';
part 'chat_users_search_state.dart';

class ChatUsersSearchBloc
    extends Bloc<ChatUsersSearchEvent, ChatUsersSearchState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ChatUsersSearchBloc({this.chatRepository}) : super(ChatUsersSearchInitial());

  @override
  Stream<ChatUsersSearchState> mapEventToState(
    ChatUsersSearchEvent event,
  ) async* {
    if (event is ChatUsersPressedSearch) {
      yield ChatUsersSearchInitial();
      try {
        ChatUsersModel getData = await chatRepository.getAllUsers(
            await environment.getToken(),
            event.type,
            event.searchTerm,
            event.isHierarchyUsers);
        yield ChatUsersSearchSuccess(stateData: getData);
      } catch (_) {
        yield ChatUsersSearchFailure(error: _.toString());
      }
    }
  }
}
