part of 'chat_users_search_bloc.dart';

abstract class ChatUsersSearchEvent extends Equatable {
  const ChatUsersSearchEvent();

  @override
  List<Object> get props => [];
}

class ChatUsersPressedSearch extends ChatUsersSearchEvent {
  final String type;
  final String searchTerm;
  final String isHierarchyUsers;

  ChatUsersPressedSearch({this.type, this.searchTerm, this.isHierarchyUsers});

  @override
  List<Object> get props => [searchTerm, isHierarchyUsers];
}
