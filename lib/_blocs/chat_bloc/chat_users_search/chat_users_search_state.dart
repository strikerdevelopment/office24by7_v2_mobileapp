part of 'chat_users_search_bloc.dart';

abstract class ChatUsersSearchState extends Equatable {
  const ChatUsersSearchState();

  @override
  List<Object> get props => [];
}

class ChatUsersSearchInitial extends ChatUsersSearchState {}

class ChatUsersSearchSuccess extends ChatUsersSearchState {
  final ChatUsersModel stateData;

  ChatUsersSearchSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ChatUsersSuccess { stateData: $stateData }';
}

class ChatUsersSearchFailure extends ChatUsersSearchState {
  final String error;
  const ChatUsersSearchFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatUsersFailure { error: $error }';
}
