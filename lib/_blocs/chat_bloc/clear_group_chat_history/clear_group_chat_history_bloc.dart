import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'clear_group_chat_history_event.dart';
part 'clear_group_chat_history_state.dart';

class ClearGroupChatHistoryBloc
    extends Bloc<ClearGroupChatHistoryEvent, ClearGroupChatHistoryState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ClearGroupChatHistoryBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(ClearGroupChatHistoryInitial());

  @override
  Stream<ClearGroupChatHistoryState> mapEventToState(
    ClearGroupChatHistoryEvent event,
  ) async* {
    if (event is ClearGroupChatHistoryPressed) {
      yield ClearGroupChatHistoryInProgress();
      try {
        String getData = await chatRepository.getClearGroupChatHistory(
            await environment.getToken(),
            event.groupId,
            event.groupUniqueId,
            event.groupType);
        yield ClearGroupChatHistorySuccess(stateData: getData);
      } catch (_) {
        yield ClearGroupChatHistoryFailure(error: _.toString());
      }
    }
  }
}
