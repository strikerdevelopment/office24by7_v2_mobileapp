part of 'clear_group_chat_history_bloc.dart';

abstract class ClearGroupChatHistoryEvent extends Equatable {
  const ClearGroupChatHistoryEvent();

  @override
  List<Object> get props => [];
}

class ClearGroupChatHistoryPressed extends ClearGroupChatHistoryEvent {
  final String groupId;
  final String groupUniqueId;
  final String groupType;
  const ClearGroupChatHistoryPressed(
      {this.groupId, this.groupUniqueId, this.groupType});

  @override
  List<Object> get props => [groupId];
}
