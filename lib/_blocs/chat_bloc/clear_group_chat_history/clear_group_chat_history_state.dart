part of 'clear_group_chat_history_bloc.dart';

abstract class ClearGroupChatHistoryState extends Equatable {
  const ClearGroupChatHistoryState();

  @override
  List<Object> get props => [];
}

class ClearGroupChatHistoryInitial extends ClearGroupChatHistoryState {}

class ClearGroupChatHistoryInProgress extends ClearGroupChatHistoryState {}

class ClearGroupChatHistorySuccess extends ClearGroupChatHistoryState {
  final String stateData;

  ClearGroupChatHistorySuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ClearGroupChatHistorySuccess { stateData: $stateData }';
}

class ClearGroupChatHistoryFailure extends ClearGroupChatHistoryState {
  final String error;
  const ClearGroupChatHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ClearGroupChatHistoryFailure { error: $error }';
}
