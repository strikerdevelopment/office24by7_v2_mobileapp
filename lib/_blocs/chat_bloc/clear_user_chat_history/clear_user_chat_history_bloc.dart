import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'clear_user_chat_history_event.dart';
part 'clear_user_chat_history_state.dart';

class ClearUserChatHistoryBloc
    extends Bloc<ClearUserChatHistoryEvent, ClearUserChatHistoryState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  ClearUserChatHistoryBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(ClearUserChatHistoryInitial());

  @override
  Stream<ClearUserChatHistoryState> mapEventToState(
    ClearUserChatHistoryEvent event,
  ) async* {
    if (event is ClearUserChatHistoryPressed) {
      yield ClearUserChatHistoryInProgress();
      try {
        String getData = await chatRepository.getClearUserChatHistory(
            await environment.getToken(),
            await environment.getUserId(),
            event.toLoginId);
        yield ClearUserChatHistorySuccess(stateData: getData);
      } catch (_) {
        yield ClearUserChatHistoryFailure(error: _.toString());
      }
    }
  }
}
