part of 'clear_user_chat_history_bloc.dart';

abstract class ClearUserChatHistoryEvent extends Equatable {
  const ClearUserChatHistoryEvent();

  @override
  List<Object> get props => [];
}

class ClearUserChatHistoryPressed extends ClearUserChatHistoryEvent {
  final String toLoginId;

  const ClearUserChatHistoryPressed({this.toLoginId});

  @override
  List<Object> get props => [toLoginId];
}
