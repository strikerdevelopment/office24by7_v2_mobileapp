part of 'clear_user_chat_history_bloc.dart';

abstract class ClearUserChatHistoryState extends Equatable {
  const ClearUserChatHistoryState();

  @override
  List<Object> get props => [];
}

class ClearUserChatHistoryInitial extends ClearUserChatHistoryState {}

class ClearUserChatHistoryInProgress extends ClearUserChatHistoryState {}

class ClearUserChatHistorySuccess extends ClearUserChatHistoryState {
  final String stateData;

  ClearUserChatHistorySuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ClearUserChatHistorySuccess { stateData: $stateData }';
}

class ClearUserChatHistoryFailure extends ClearUserChatHistoryState {
  final String error;
  const ClearUserChatHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ClearUserChatHistoryFailure { error: $error }';
}
