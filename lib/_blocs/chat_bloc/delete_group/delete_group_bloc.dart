import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'delete_group_event.dart';
part 'delete_group_state.dart';

class DeleteGroupBloc extends Bloc<DeleteGroupEvent, DeleteGroupState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  DeleteGroupBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(DeleteGroupInitial());

  @override
  Stream<DeleteGroupState> mapEventToState(
    DeleteGroupEvent event,
  ) async* {
    if (event is DeleteGroupPressed) {
      yield DeleteGroupInProgress();
      try {
        String getData = await chatRepository.getDeleteGroup(
            await environment.getToken(),
            event.groupId,
            event.groupUniqueId,
            event.groupType);
        yield DeleteGroupSuccess(stateData: getData);
      } catch (_) {
        yield DeleteGroupFailure(error: _.toString());
      }
    }
  }
}
