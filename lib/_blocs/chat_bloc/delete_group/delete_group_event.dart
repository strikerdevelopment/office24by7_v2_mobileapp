part of 'delete_group_bloc.dart';

abstract class DeleteGroupEvent extends Equatable {
  const DeleteGroupEvent();

  @override
  List<Object> get props => [];
}

class DeleteGroupPressed extends DeleteGroupEvent {
  final String groupId;
  final String groupUniqueId;
  final String groupType;
  const DeleteGroupPressed({this.groupId, this.groupUniqueId, this.groupType});

  @override
  List<Object> get props => [groupId];
}
