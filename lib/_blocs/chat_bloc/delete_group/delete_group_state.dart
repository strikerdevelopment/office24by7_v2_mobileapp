part of 'delete_group_bloc.dart';

abstract class DeleteGroupState extends Equatable {
  const DeleteGroupState();

  @override
  List<Object> get props => [];
}

class DeleteGroupInitial extends DeleteGroupState {}

class DeleteGroupInProgress extends DeleteGroupState {}

class DeleteGroupSuccess extends DeleteGroupState {
  final String stateData;

  DeleteGroupSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'DeleteGroupSuccess { stateData: $stateData }';
}

class DeleteGroupFailure extends DeleteGroupState {
  final String error;
  const DeleteGroupFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DeleteGroupFailure { error: $error }';
}
