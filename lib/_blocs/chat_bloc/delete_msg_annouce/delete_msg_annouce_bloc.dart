import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'delete_msg_annouce_event.dart';
part 'delete_msg_annouce_state.dart';

class DeleteMsgAnnouceBloc
    extends Bloc<DeleteMsgAnnouceEvent, DeleteMsgAnnouceState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  DeleteMsgAnnouceBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(DeleteMsgAnnouceInitial());

  @override
  Stream<DeleteMsgAnnouceState> mapEventToState(
    DeleteMsgAnnouceEvent event,
  ) async* {
    if (event is DeleteMsgAnnoucePressed) {
      yield DeleteMsgAnnouceInProgress();
      try {
        String getData = await chatRepository.getDeleteMsgAnnouce(
            await environment.getToken(),
            event.chatTextLogId,
            event.groupId,
            event.groupUniqueId,
            event.groupType);
        yield DeleteMsgAnnouceSuccess(stateData: getData);
      } catch (_) {
        yield DeleteMsgAnnouceFailure(error: _.toString());
      }
    }
  }
}
