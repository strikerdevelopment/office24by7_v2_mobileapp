part of 'delete_msg_annouce_bloc.dart';

abstract class DeleteMsgAnnouceEvent extends Equatable {
  const DeleteMsgAnnouceEvent();

  @override
  List<Object> get props => [];
}

class DeleteMsgAnnoucePressed extends DeleteMsgAnnouceEvent {
  final List chatTextLogId;
  final String groupId;
  final String groupUniqueId;
  final String groupType;
  const DeleteMsgAnnoucePressed(
      {this.chatTextLogId, this.groupId, this.groupUniqueId, this.groupType});

  @override
  List<Object> get props => [chatTextLogId.length, groupId];
}
