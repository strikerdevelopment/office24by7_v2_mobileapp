part of 'delete_msg_annouce_bloc.dart';

abstract class DeleteMsgAnnouceState extends Equatable {
  const DeleteMsgAnnouceState();

  @override
  List<Object> get props => [];
}

class DeleteMsgAnnouceInitial extends DeleteMsgAnnouceState {}

class DeleteMsgAnnouceInProgress extends DeleteMsgAnnouceState {}

class DeleteMsgAnnouceSuccess extends DeleteMsgAnnouceState {
  final String stateData;

  DeleteMsgAnnouceSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'DeleteMsgAnnouceSuccess { stateData: $stateData }';
}

class DeleteMsgAnnouceFailure extends DeleteMsgAnnouceState {
  final String error;
  const DeleteMsgAnnouceFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DeleteMsgAnnouceFailure { error: $error }';
}
