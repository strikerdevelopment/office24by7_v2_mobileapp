import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'delete_user_messages_event.dart';
part 'delete_user_messages_state.dart';

class DeleteUserMessagesBloc
    extends Bloc<DeleteUserMessagesEvent, DeleteUserMessagesState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  DeleteUserMessagesBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(DeleteUserMessagesInitial());

  @override
  Stream<DeleteUserMessagesState> mapEventToState(
    DeleteUserMessagesEvent event,
  ) async* {
    if (event is DeleteUserMessagesPressed) {
      yield DeleteUserMessagesInProgress();
      try {
        String getData = await chatRepository.getDeleteUserMessages(
            await environment.getToken(),
            event.chatTextLogId,
            await environment.getUserId(),
            event.toLoginId);
        yield DeleteUserMessagesSuccess(stateData: getData);
      } catch (_) {
        yield DeleteUserMessagesFailure(error: _.toString());
      }
    }
  }
}
