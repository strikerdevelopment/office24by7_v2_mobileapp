part of 'delete_user_messages_bloc.dart';

abstract class DeleteUserMessagesEvent extends Equatable {
  const DeleteUserMessagesEvent();

  @override
  List<Object> get props => [];
}

class DeleteUserMessagesPressed extends DeleteUserMessagesEvent {
  final String toLoginId;
  final List chatTextLogId;

  const DeleteUserMessagesPressed({this.toLoginId, this.chatTextLogId});

  @override
  List<Object> get props => [toLoginId];
}
