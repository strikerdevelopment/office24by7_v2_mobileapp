part of 'delete_user_messages_bloc.dart';

abstract class DeleteUserMessagesState extends Equatable {
  const DeleteUserMessagesState();

  @override
  List<Object> get props => [];
}

class DeleteUserMessagesInitial extends DeleteUserMessagesState {}

class DeleteUserMessagesInProgress extends DeleteUserMessagesState {}

class DeleteUserMessagesSuccess extends DeleteUserMessagesState {
  final String stateData;

  DeleteUserMessagesSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'DeleteUserMessagesSuccess { stateData: $stateData }';
}

class DeleteUserMessagesFailure extends DeleteUserMessagesState {
  final String error;
  const DeleteUserMessagesFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DeleteUserMessagesFailure { error: $error }';
}
