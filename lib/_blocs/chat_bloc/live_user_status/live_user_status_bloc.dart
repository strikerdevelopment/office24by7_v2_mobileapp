import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'live_user_status_event.dart';
part 'live_user_status_state.dart';

class LiveUserStatusBloc
    extends Bloc<LiveUserStatusEvent, LiveUserStatusState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  LiveUserStatusBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(LiveUserStatusInitial());

  @override
  Stream<LiveUserStatusState> mapEventToState(
    LiveUserStatusEvent event,
  ) async* {
    if (event is LiveUserStatusPressed) {
      yield LiveUserStatusInProgress();
      try {
        String getData = await chatRepository.getLiveUserStatus(
            await environment.getToken(), event.status);
        yield LiveUserStatusSuccess(stateData: getData);
      } catch (_) {
        yield LiveUserStatusFailure(error: _.toString());
      }
    }
  }
}
