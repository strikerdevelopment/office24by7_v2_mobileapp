part of 'live_user_status_bloc.dart';

abstract class LiveUserStatusEvent extends Equatable {
  const LiveUserStatusEvent();

  @override
  List<Object> get props => [];
}

class LiveUserStatusPressed extends LiveUserStatusEvent {
  final String status;

  const LiveUserStatusPressed({this.status});

  @override
  List<Object> get props => [status];
}
