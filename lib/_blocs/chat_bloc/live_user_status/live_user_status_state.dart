part of 'live_user_status_bloc.dart';

abstract class LiveUserStatusState extends Equatable {
  const LiveUserStatusState();

  @override
  List<Object> get props => [];
}

class LiveUserStatusInitial extends LiveUserStatusState {}

class LiveUserStatusInProgress extends LiveUserStatusState {}

class LiveUserStatusSuccess extends LiveUserStatusState {
  final String stateData;

  LiveUserStatusSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'LiveUserStatusSuccess { stateData: $stateData }';
}

class LiveUserStatusFailure extends LiveUserStatusState {
  final String error;
  const LiveUserStatusFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LiveUserStatusFailure { error: $error }';
}
