import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/chat_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'message_forward_event.dart';
part 'message_forward_state.dart';

class MessageForwardBloc
    extends Bloc<MessageForwardEvent, MessageForwardState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  MessageForwardBloc({this.chatRepository})
      : assert(chatRepository != null),
        super(MessageForwardInitial());

  @override
  Stream<MessageForwardState> mapEventToState(
    MessageForwardEvent event,
  ) async* {
    if (event is MessageForwardPressed) {
      yield MessageForwardInProgress();
      try {
        String getData = await chatRepository.getMessageForward(
            await environment.getToken(),
            await environment.getUserId(),
            event.chatTextLogId,
            event.recipients);
        yield MessageForwardSuccess(stateData: getData);
      } catch (_) {
        yield MessageForwardFailure(error: _.toString());
      }
    }
  }
}
