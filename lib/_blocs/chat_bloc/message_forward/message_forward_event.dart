part of 'message_forward_bloc.dart';

abstract class MessageForwardEvent extends Equatable {
  const MessageForwardEvent();

  @override
  List<Object> get props => [];
}

class MessageForwardPressed extends MessageForwardEvent {
  final Map recipients;
  final List chatTextLogId;

  const MessageForwardPressed({this.recipients, this.chatTextLogId});

  @override
  List<Object> get props => [recipients];
}
