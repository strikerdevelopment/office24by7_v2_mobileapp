part of 'message_forward_bloc.dart';

abstract class MessageForwardState extends Equatable {
  const MessageForwardState();

  @override
  List<Object> get props => [];
}

class MessageForwardInitial extends MessageForwardState {}

class MessageForwardInProgress extends MessageForwardState {}

class MessageForwardSuccess extends MessageForwardState {
  final String stateData;

  MessageForwardSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'MessageForwardSuccess { stateData: $stateData }';
}

class MessageForwardFailure extends MessageForwardState {
  final String error;
  const MessageForwardFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'MessageForwardFailure { error: $error }';
}
