import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_models/chat_model/chat_history_model.dart';

part 'multi_select_announ_data_event.dart';
part 'multi_select_announ_data_state.dart';

class MultiSelectAnnounDataBloc
    extends Bloc<MultiSelectAnnounDataEvent, MultiSelectAnnounDataState> {
  MultiSelectAnnounDataBloc() : super(MultiSelectAnnounDataInitial());

  @override
  Stream<MultiSelectAnnounDataState> mapEventToState(
    MultiSelectAnnounDataEvent event,
  ) async* {
    if (event is MSDannounTextLogIdPressed) {
      yield MultiSelectAnnounDataInProgress();
      yield MSDannounTextLogIdSuccess(
        chatTextLogId: event.chatTextLogId,
        singleChatData: event.singleChatData,
        isReply: event.isReply,
      );
    }
    if (event is MSDannounTextLogIdClose) {
      yield MSDannounTextLogIdSuccess(
          chatTextLogId: [], singleChatData: null, isReply: false);
      yield MultiSelectAnnounDataInitial();
    }
  }
}
