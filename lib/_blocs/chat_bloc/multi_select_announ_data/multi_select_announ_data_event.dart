part of 'multi_select_announ_data_bloc.dart';

abstract class MultiSelectAnnounDataEvent extends Equatable {
  const MultiSelectAnnounDataEvent();

  @override
  List<Object> get props => [];
}

class MSDannounTextLogIdPressed extends MultiSelectAnnounDataEvent {
  final List chatTextLogId;
  final ChatHistoryModel singleChatData;
  final bool isReply;

  const MSDannounTextLogIdPressed(
      {this.chatTextLogId, this.singleChatData, this.isReply});

  @override
  List<Object> get props => [chatTextLogId];
}

class MSDannounTextLogIdClose extends MultiSelectAnnounDataEvent {}
