part of 'multi_select_announ_data_bloc.dart';

abstract class MultiSelectAnnounDataState extends Equatable {
  const MultiSelectAnnounDataState();

  @override
  List<Object> get props => [];
}

class MultiSelectAnnounDataInitial extends MultiSelectAnnounDataState {}

class MultiSelectAnnounDataInProgress extends MultiSelectAnnounDataState {}

class MSDannounTextLogIdSuccess extends MultiSelectAnnounDataState {
  final List chatTextLogId;
  final ChatHistoryModel singleChatData;
  final bool isReply;
  MSDannounTextLogIdSuccess(
      {this.chatTextLogId, this.singleChatData, this.isReply});

  @override
  List<Object> get props => [chatTextLogId];

  @override
  String toString() =>
      'DeleteUserMessagesSuccess { stateData: ${chatTextLogId.length} }';
}
