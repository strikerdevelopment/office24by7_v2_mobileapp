import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_models/chat_model/chat_history_model.dart';

part 'multi_select_data_event.dart';
part 'multi_select_data_state.dart';

class MultiSelectDataBloc
    extends Bloc<MultiSelectDataEvent, MultiSelectDataState> {
  MultiSelectDataBloc() : super(MultiSelectDataInitial());

  @override
  Stream<MultiSelectDataState> mapEventToState(
    MultiSelectDataEvent event,
  ) async* {
    if (event is MSDchatTextLogIdPressed) {
      yield MultiSelectDataInProgress();
      yield MSDchatTextLogIdSuccess(
        chatTextLogId: event.chatTextLogId,
        singleChatData: event.singleChatData,
        isReply: event.isReply,
      );
    }
    if (event is MSDchatTextLogIdClose) {
      yield MSDchatTextLogIdSuccess(
          chatTextLogId: [], singleChatData: null, isReply: false);
      yield MultiSelectDataInitial();
    }
  }
}
