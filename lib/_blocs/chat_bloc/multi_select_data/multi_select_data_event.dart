part of 'multi_select_data_bloc.dart';

abstract class MultiSelectDataEvent extends Equatable {
  const MultiSelectDataEvent();

  @override
  List<Object> get props => [];
}

class MSDchatTextLogIdPressed extends MultiSelectDataEvent {
  final List chatTextLogId;
  final ChatHistoryModel singleChatData;
  final bool isReply;

  const MSDchatTextLogIdPressed(
      {this.chatTextLogId, this.singleChatData, this.isReply});

  @override
  List<Object> get props => [chatTextLogId];
}

class MSDchatTextLogIdClose extends MultiSelectDataEvent {}
