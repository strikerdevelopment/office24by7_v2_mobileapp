part of 'multi_select_data_bloc.dart';

abstract class MultiSelectDataState extends Equatable {
  const MultiSelectDataState();

  @override
  List<Object> get props => [];
}

class MultiSelectDataInitial extends MultiSelectDataState {}

class MultiSelectDataInProgress extends MultiSelectDataState {}

class MSDchatTextLogIdSuccess extends MultiSelectDataState {
  final List chatTextLogId;
  final ChatHistoryModel singleChatData;
  final bool isReply;
  MSDchatTextLogIdSuccess(
      {this.chatTextLogId, this.singleChatData, this.isReply});

  @override
  List<Object> get props => [chatTextLogId];

  @override
  String toString() =>
      'DeleteUserMessagesSuccess { stateData: ${chatTextLogId.length} }';
}
