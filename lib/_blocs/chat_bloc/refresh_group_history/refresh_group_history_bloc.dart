import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';

part 'refresh_group_history_event.dart';
part 'refresh_group_history_state.dart';

class RefreshGroupHistoryBloc extends Bloc<RefreshGroupHistoryEvent, RefreshGroupHistoryState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  RefreshGroupHistoryBloc({this.chatRepository})
      : super(RefreshGroupHistoryInitial());

  @override
  Stream<RefreshGroupHistoryState> mapEventToState(
    RefreshGroupHistoryEvent event,
  ) async* {
    if (event is RefreshGroupHistoryPressed) {
      yield RefreshGroupHistoryInProgress();
      try {
        RefreshGroupHistoryModel getData =
            await chatRepository.getRefreshGroupChatHistory(
                await environment.getToken(),
                event.groupId,
                event.groupUniqueId,
                event.groupType,);
        yield RefreshGroupHistorySuccess(stateData: getData);
      } catch (_) {
        yield RefreshGroupHistoryFailure(error: _.toString());
      }
    }
  }
}
