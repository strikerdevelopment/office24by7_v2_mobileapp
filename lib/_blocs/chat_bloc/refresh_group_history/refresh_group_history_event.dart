part of 'refresh_group_history_bloc.dart';

abstract class RefreshGroupHistoryEvent extends Equatable {
  const RefreshGroupHistoryEvent();

  @override
  List<Object> get props => [];
}

class RefreshGroupHistoryPressed extends RefreshGroupHistoryEvent {
  final String groupId;
  final String groupUniqueId;
  final String groupType;

  RefreshGroupHistoryPressed(
      {this.groupId, this.groupUniqueId, this.groupType});

  @override
  List<Object> get props => [];
}
