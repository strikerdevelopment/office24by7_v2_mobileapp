part of 'refresh_group_history_bloc.dart';

abstract class RefreshGroupHistoryState extends Equatable {
  const RefreshGroupHistoryState();
  
  @override
  List<Object> get props => [];
}

class RefreshGroupHistoryInitial extends RefreshGroupHistoryState {}


class RefreshGroupHistoryInProgress extends RefreshGroupHistoryState {}

class RefreshGroupHistorySuccess extends RefreshGroupHistoryState {
  final RefreshGroupHistoryModel stateData;

  RefreshGroupHistorySuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'RefreshGroupHistorySuccess { stateData: $stateData }';
}

class RefreshGroupHistoryFailure extends RefreshGroupHistoryState {
  final String error;
  const RefreshGroupHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RefreshGroupHistoryFailure { error: $error }';
}