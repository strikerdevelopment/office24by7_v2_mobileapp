import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';

part 'refresh_user_history_event.dart';
part 'refresh_user_history_state.dart';

class RefreshUserHistoryBloc
    extends Bloc<RefreshUserHistoryEvent, RefreshUserHistoryState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();

  RefreshUserHistoryBloc({this.chatRepository})
      : super(RefreshUserHistoryInitial());

  @override
  Stream<RefreshUserHistoryState> mapEventToState(
    RefreshUserHistoryEvent event,
  ) async* {
    if (event is RefreshUserHistoryPressed) {
      yield RefreshUserHistoryInProgress();
      try {
        RefreshUserHistoryModel getData =
            await chatRepository.getRefreshUserChatHistory(
                await environment.getToken(),
                await environment.getUserId(),
                event.toUserId);
        yield RefreshUserHistorySuccess(stateData: getData);
      } catch (_) {
        yield RefreshUserHistoryFailure(error: _.toString());
      }
    }
  }
}
