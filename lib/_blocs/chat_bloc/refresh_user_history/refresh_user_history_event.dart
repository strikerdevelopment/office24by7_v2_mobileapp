part of 'refresh_user_history_bloc.dart';

abstract class RefreshUserHistoryEvent extends Equatable {
  const RefreshUserHistoryEvent();

  @override
  List<Object> get props => [];
}

class RefreshUserHistoryPressed extends RefreshUserHistoryEvent {
  final String toUserId;

  RefreshUserHistoryPressed({this.toUserId});

  @override
  List<Object> get props => [];
}
