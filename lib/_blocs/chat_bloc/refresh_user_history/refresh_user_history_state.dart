part of 'refresh_user_history_bloc.dart';

abstract class RefreshUserHistoryState extends Equatable {
  const RefreshUserHistoryState();

  @override
  List<Object> get props => [];
}

class RefreshUserHistoryInitial extends RefreshUserHistoryState {}

class RefreshUserHistoryInProgress extends RefreshUserHistoryState {}

class RefreshUserHistorySuccess extends RefreshUserHistoryState {
  final RefreshUserHistoryModel stateData;

  RefreshUserHistorySuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'RefreshUserHistorySuccess { stateData: $stateData }';
}

class RefreshUserHistoryFailure extends RefreshUserHistoryState {
  final String error;
  const RefreshUserHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RefreshUserHistoryFailure { error: $error }';
}
