import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';

part 'send_msg_annouce_event.dart';
part 'send_msg_annouce_state.dart';

class SendMsgAnnouceBloc
    extends Bloc<SendMsgAnnouceEvent, SendMsgAnnouceState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();
  final FirebaseRepository firebaseRepository;

  SendMsgAnnouceBloc({this.chatRepository, this.firebaseRepository})
      : assert(chatRepository != null, firebaseRepository != null),
        super(SendMsgAnnouceInitial());

  @override
  Stream<SendMsgAnnouceState> mapEventToState(
    SendMsgAnnouceEvent event,
  ) async* {
    if (event is SendMsgAnnoucePressed) {
      yield SendMsgAnnouceInProgress();
      try {
        String getData = await chatRepository.getSendMsgAnnouce(
            await environment.getToken(),
            event.user.chatGroupId,
            event.user.groupUniqueId,
            event.user.groupType,
            event.msg,
            event.msgReferenceId);
        yield SendMsgAnnouceSuccess(stateData: getData);
        String groupIds = await chatRepository.getGroupChatIds(
            await environment.getToken(),
            event.user.chatGroupId,
            event.user.groupUniqueId,
            event.user.groupType);
        String userId = await environment.getUserId();
        List<String> finalGroupIds = groupIds.split(',');
        finalGroupIds.removeWhere((id) => id == userId);
        await firebaseRepository.getSendFCM(finalGroupIds.join(','),
            event.user.name, event.msg, event.user, event.authData, null);
      } catch (_) {
        yield SendMsgAnnouceFailure(error: _.toString());
      }
    }
  }
}
