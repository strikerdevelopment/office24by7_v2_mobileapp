part of 'send_msg_annouce_bloc.dart';

abstract class SendMsgAnnouceEvent extends Equatable {
  const SendMsgAnnouceEvent();

  @override
  List<Object> get props => [];
}

class SendMsgAnnoucePressed extends SendMsgAnnouceEvent {
  final User user;
  final AuthModel authData;
  final String msg;
  final String msgReferenceId;

  const SendMsgAnnoucePressed(
      {this.user, this.authData, this.msg, this.msgReferenceId});

  @override
  List<Object> get props => [msg, user.chatGroupId];
}
