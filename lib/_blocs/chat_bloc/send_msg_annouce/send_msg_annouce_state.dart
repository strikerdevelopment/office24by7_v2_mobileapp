part of 'send_msg_annouce_bloc.dart';

abstract class SendMsgAnnouceState extends Equatable {
  const SendMsgAnnouceState();

  @override
  List<Object> get props => [];
}

class SendMsgAnnouceInitial extends SendMsgAnnouceState {}

class SendMsgAnnouceInProgress extends SendMsgAnnouceState {}

class SendMsgAnnouceSuccess extends SendMsgAnnouceState {
  final String stateData;

  SendMsgAnnouceSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'SendMsgAnnouceSuccess { stateData: $stateData }';
}

class SendMsgAnnouceFailure extends SendMsgAnnouceState {
  final String error;
  const SendMsgAnnouceFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SendMsgAnnouceFailure { error: $error }';
}
