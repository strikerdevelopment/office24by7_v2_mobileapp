import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';

part 'send_text_message_event.dart';
part 'send_text_message_state.dart';

class SendTextMessageBloc
    extends Bloc<SendTextMessageEvent, SendTextMessageState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();
  final FirebaseRepository firebaseRepository;

  SendTextMessageBloc({this.chatRepository, this.firebaseRepository})
      : assert(chatRepository != null, firebaseRepository != null),
        super(SendTextMessageInitial());

  @override
  Stream<SendTextMessageState> mapEventToState(
    SendTextMessageEvent event,
  ) async* {
    if (event is SendTextMessagePressed) {
      yield SendTextMessageInProgress();
      try {
        String getData = await chatRepository.getsendTextMessage(
          await environment.getToken(),
          await environment.getUserId(),
          event.user.userId,
          event.formData,
          event.msgReferenceId,
        );
        yield SendTextMessageSuccess(stateData: getData);

        ChatUserInfoModel userData = await chatRepository.getChatUserInfo(
            await environment.getToken(), await environment.getUserId());

        await firebaseRepository.getSendFCM(event.user.userId, userData.name,
            event.formData['message'], event.user, event.authData, userData);
      } catch (_) {
        yield SendTextMessageFailure(error: _.toString());
      }
    }
  }
}
