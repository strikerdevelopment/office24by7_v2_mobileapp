part of 'send_text_message_bloc.dart';

abstract class SendTextMessageEvent extends Equatable {
  const SendTextMessageEvent();

  @override
  List<Object> get props => [];
}

class SendTextMessagePressed extends SendTextMessageEvent {
  final User user;
  final AuthModel authData;
  final Map formData;
  final String msgReferenceId;

  const SendTextMessagePressed(
      {this.user, this.authData, this.formData, this.msgReferenceId = "0"});

  @override
  List<Object> get props => [user.userId];
}
