part of 'send_text_message_bloc.dart';

abstract class SendTextMessageState extends Equatable {
  const SendTextMessageState();

  @override
  List<Object> get props => [];
}

class SendTextMessageInitial extends SendTextMessageState {}

class SendTextMessageInProgress extends SendTextMessageState {}

class SendTextMessageSuccess extends SendTextMessageState {
  final String stateData;

  SendTextMessageSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'SendTextMessageSuccess { stateData: $stateData }';
}

class SendTextMessageFailure extends SendTextMessageState {
  final String error;
  const SendTextMessageFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SendTextMessageFailure { error: $error }';
}
