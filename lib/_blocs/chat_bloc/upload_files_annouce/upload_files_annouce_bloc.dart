import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';

part 'upload_files_annouce_event.dart';
part 'upload_files_annouce_state.dart';

class UploadFilesAnnouceBloc
    extends Bloc<UploadFilesAnnouceEvent, UploadFilesAnnouceState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();
  final FirebaseRepository firebaseRepository;

  UploadFilesAnnouceBloc({this.chatRepository, this.firebaseRepository})
      : assert(chatRepository != null, firebaseRepository != null),
        super(UploadFilesAnnouceInitial());

  @override
  Stream<UploadFilesAnnouceState> mapEventToState(
    UploadFilesAnnouceEvent event,
  ) async* {
    if (event is UploadFilesAnnoucePressed) {
      yield UploadFilesAnnouceInProgress();
      try {
        String getData = await chatRepository.getUploadFilesAnnouce(
            await environment.getToken(),
            event.user.chatGroupId,
            event.user.groupUniqueId,
            event.user.groupType,
            event.files,
            event.msgReferenceId);
        yield UploadFilesAnnouceSuccess(stateData: getData);
        String groupIds = await chatRepository.getGroupChatIds(
            await environment.getToken(),
            event.user.chatGroupId,
            event.user.groupUniqueId,
            event.user.groupType);
        String userId = await environment.getUserId();
        List<String> finalGroupIds = groupIds.split(',');
        finalGroupIds.removeWhere((id) => id == userId);
        await firebaseRepository.getSendFCM(
            finalGroupIds.join(','),
            event.user.name,
            "File Attachment sent...",
            event.user,
            event.authData,
            null);
      } catch (_) {
        yield UploadFilesAnnouceFailure(error: _.toString());
      }
    }
  }
}
