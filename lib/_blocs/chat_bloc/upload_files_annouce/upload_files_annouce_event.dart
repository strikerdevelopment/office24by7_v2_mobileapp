part of 'upload_files_annouce_bloc.dart';

abstract class UploadFilesAnnouceEvent extends Equatable {
  const UploadFilesAnnouceEvent();

  @override
  List<Object> get props => [];
}

class UploadFilesAnnoucePressed extends UploadFilesAnnouceEvent {
  final User user;
  final AuthModel authData;
  final List files;
  final String msgReferenceId;

  const UploadFilesAnnoucePressed(
      {this.user, this.authData, this.files, this.msgReferenceId});

  @override
  List<Object> get props => [files.length, user.chatGroupId];
}
