part of 'upload_files_annouce_bloc.dart';

abstract class UploadFilesAnnouceState extends Equatable {
  const UploadFilesAnnouceState();

  @override
  List<Object> get props => [];
}

class UploadFilesAnnouceInitial extends UploadFilesAnnouceState {}

class UploadFilesAnnouceInProgress extends UploadFilesAnnouceState {}

class UploadFilesAnnouceSuccess extends UploadFilesAnnouceState {
  final String stateData;

  UploadFilesAnnouceSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'UploadFilesAnnouceSuccess { stateData: $stateData }';
}

class UploadFilesAnnouceFailure extends UploadFilesAnnouceState {
  final String error;
  const UploadFilesAnnouceFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'UploadFilesAnnouceFailure { error: $error }';
}
