import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';

part 'upload_user_chat_event.dart';
part 'upload_user_chat_state.dart';

class UploadUserChatBloc
    extends Bloc<UploadUserChatEvent, UploadUserChatState> {
  final ChatRepository chatRepository;
  final Environment environment = Environment();
  final FirebaseRepository firebaseRepository;

  UploadUserChatBloc({this.chatRepository, this.firebaseRepository})
      : assert(chatRepository != null),
        super(UploadUserChatInitial());

  @override
  Stream<UploadUserChatState> mapEventToState(
    UploadUserChatEvent event,
  ) async* {
    if (event is UploadUserChatPressed) {
      yield UploadUserChatInProgress();
      try {
        String getData = await chatRepository.getUploadUserChat(
            await environment.getToken(),
            event.files,
            await environment.getUserId(),
            event.user.userId,
            event.msgReferenceId);

        yield UploadUserChatSuccess(stateData: getData);
        ChatUserInfoModel userData = await chatRepository.getChatUserInfo(
            await environment.getToken(), await environment.getUserId());
        await firebaseRepository.getSendFCM(
            event.user.userId,
            userData.name,
            "File Attachment Sent you...",
            event.user,
            event.authData,
            userData);
      } catch (_) {
        yield UploadUserChatFailure(error: _.toString());
      }
    }
  }
}
