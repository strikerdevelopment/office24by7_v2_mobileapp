part of 'upload_user_chat_bloc.dart';

abstract class UploadUserChatEvent extends Equatable {
  const UploadUserChatEvent();

  @override
  List<Object> get props => [];
}

class UploadUserChatPressed extends UploadUserChatEvent {
  final List files;
  final User user;
  final AuthModel authData;
  final String msgReferenceId;

  const UploadUserChatPressed(
      {this.files, this.user, this.authData, this.msgReferenceId});

  @override
  List<Object> get props => [files.length];
}
