part of 'upload_user_chat_bloc.dart';

abstract class UploadUserChatState extends Equatable {
  const UploadUserChatState();

  @override
  List<Object> get props => [];
}

class UploadUserChatInitial extends UploadUserChatState {}

class UploadUserChatInProgress extends UploadUserChatState {}

class UploadUserChatSuccess extends UploadUserChatState {
  final String stateData;

  UploadUserChatSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'UploadUserChatSuccess { stateData: $stateData }';
}

class UploadUserChatFailure extends UploadUserChatState {
  final String error;
  const UploadUserChatFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'UploadUserChatFailure { error: $error }';
}
