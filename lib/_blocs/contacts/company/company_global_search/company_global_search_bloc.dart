import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'company_global_search_event.dart';
part 'company_global_search_state.dart';

class CompanyGlobalSearchBloc
    extends Bloc<CompanyGlobalSearchEvent, CompanyGlobalSearchState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  CompanyGlobalSearchBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(CompanyGlobalSearchInitial());

  @override
  Stream<CompanyGlobalSearchState> mapEventToState(
    CompanyGlobalSearchEvent event,
  ) async* {
    final currentState = state;
    if (event is CompanyGlobalSearchFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is CompanyGlobalSearchInitial) {
          final List<ContactCompanyList> contacts =
              await contactRepository.getCompanyGlobalSearch(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.keyword);
          yield CompanyGlobalSearchSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is CompanyGlobalSearchSuccess) {
          final List<ContactCompanyList> contacts =
              await contactRepository.getCompanyGlobalSearch(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.keyword,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : CompanyGlobalSearchSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield CompanyGlobalSearchFailure(error: _.toString());
      }
    }

    if (event is CompanyGlobalSearchRefresh) {
      yield CompanyGlobalSearchInitial();
      try {
        final List<ContactCompanyList> contacts =
            await contactRepository.getCompanyGlobalSearch(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.keyword);
        yield CompanyGlobalSearchSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield CompanyGlobalSearchFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(CompanyGlobalSearchState state) =>
      state is CompanyGlobalSearchSuccess && state.hasReachedMax;
}
