part of 'company_global_search_bloc.dart';

abstract class CompanyGlobalSearchEvent extends Equatable {
  const CompanyGlobalSearchEvent();

  @override
  List<Object> get props => [];
}

class CompanyGlobalSearchFetched extends CompanyGlobalSearchEvent {
  final String filterHeader;
  final String keyword;

  const CompanyGlobalSearchFetched({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, keyword];
}

class CompanyGlobalSearchRefresh extends CompanyGlobalSearchEvent {
  final String filterHeader;
  final String keyword;
  const CompanyGlobalSearchRefresh({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
