part of 'company_global_search_bloc.dart';

abstract class CompanyGlobalSearchState extends Equatable {
  const CompanyGlobalSearchState();

  @override
  List<Object> get props => [];
}

class CompanyGlobalSearchInitial extends CompanyGlobalSearchState {}

class CompanyGlobalSearchFailure extends CompanyGlobalSearchState {
  final String error;

  CompanyGlobalSearchFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'CompanyGlobalSearchFailure { error: $error }';
}

class CompanyGlobalSearchSuccess extends CompanyGlobalSearchState {
  final List<ContactCompanyList> contacts;
  final bool hasReachedMax;
  final List<ContactCompanyList> currentState;

  const CompanyGlobalSearchSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  CompanyGlobalSearchSuccess copyWith({
    List<ContactCompanyList> contacts,
    bool hasReachedMax,
  }) {
    return CompanyGlobalSearchSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'CompanyGlobalSearchSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
