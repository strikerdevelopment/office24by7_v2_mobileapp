import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_company_list_event.dart';
part 'contact_company_list_state.dart';

class ContactCompanyListBloc
    extends Bloc<ContactCompanyListEvent, ContactCompanyListState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactCompanyListBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactCompanyListInitial());

  @override
  Stream<ContactCompanyListState> mapEventToState(
    ContactCompanyListEvent event,
  ) async* {
    final currentState = state;
    if (event is ContactCompanyListFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ContactCompanyListInitial) {
          final List<ContactCompanyList> contacts =
              await contactRepository.getContactCompanyList(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.filterOption);
          yield ContactCompanyListSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is ContactCompanyListSuccess) {
          final List<ContactCompanyList> contacts =
              await contactRepository.getContactCompanyList(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.filterOption,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ContactCompanyListSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield ContactCompanyListFailure(error: _.toString());
      }
    }
    if (event is ContactCompanyListRefresh) {
      yield ContactCompanyListInitial();
      try {
        final List<ContactCompanyList> contacts =
            await contactRepository.getContactCompanyList(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.filterOption);
        yield ContactCompanyListSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield ContactCompanyListFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ContactCompanyListState state) =>
      state is ContactCompanyListSuccess && state.hasReachedMax;
}
