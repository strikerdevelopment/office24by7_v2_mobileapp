part of 'contact_company_list_bloc.dart';

abstract class ContactCompanyListEvent extends Equatable {
  const ContactCompanyListEvent();

  @override
  List<Object> get props => [];
}

class ContactCompanyListFetched extends ContactCompanyListEvent {
  final String filterHeader;
  final String filterOption;

  const ContactCompanyListFetched({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, filterOption];
}

class ContactCompanyListRefresh extends ContactCompanyListEvent {
  final String filterHeader;
  final String filterOption;
  const ContactCompanyListRefresh({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
