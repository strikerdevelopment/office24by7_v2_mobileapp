part of 'contact_company_list_bloc.dart';

abstract class ContactCompanyListState extends Equatable {
  const ContactCompanyListState();

  @override
  List<Object> get props => [];
}

class ContactCompanyListInitial extends ContactCompanyListState {}

class ContactCompanyListFailure extends ContactCompanyListState {
  final String error;

  ContactCompanyListFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactCompanyListFailure { error: $error }';
}

class ContactCompanyListSuccess extends ContactCompanyListState {
  final List<ContactCompanyList> contacts;
  final bool hasReachedMax;
  final List<dynamic> currentState;

  const ContactCompanyListSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  ContactCompanyListSuccess copyWith({
    List<ContactCompanyList> contacts,
    bool hasReachedMax,
  }) {
    return ContactCompanyListSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'ContactsSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
