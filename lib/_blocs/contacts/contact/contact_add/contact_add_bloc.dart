import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_add_event.dart';
part 'contact_add_state.dart';

class ContactAddBloc extends Bloc<ContactAddEvent, ContactAddState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactAddBloc({this.contactRepository}) : super(ContactAddInitial());

  @override
  Stream<ContactAddState> mapEventToState(
    ContactAddEvent event,
  ) async* {
    if (event is ContactAddPressed) {
      yield ContactAddInProgress();
      try {
        dynamic contactAddData = await contactRepository.getAddContact(
            await environment.getToken(), event.formData);
        yield ContactaddSuccess(formResponse: contactAddData);
      } catch (_) {
        yield ContactAddFailure(error: _.toString());
      }
    }

    if (event is ContactCopyPressed) {
      yield ContactCopyInProgress();
      try {
        dynamic contactAddData = await contactRepository.getAddContact(
            await environment.getToken(), event.formData);
        yield ContactCopySuccess(formResponse: contactAddData);
      } catch (_) {
        yield ContactCopyFailure(error: _.toString());
      }
    }
  }
}
