part of 'contact_add_bloc.dart';

abstract class ContactAddEvent extends Equatable {
  const ContactAddEvent();

  @override
  List<Object> get props => [];
}

class ContactAddPressed extends ContactAddEvent {
  final Map formData;
  const ContactAddPressed({this.formData});
}

class ContactCopyPressed extends ContactAddEvent {
  final Map formData;
  const ContactCopyPressed({this.formData});
}
