part of 'contact_add_bloc.dart';

abstract class ContactAddState extends Equatable {
  const ContactAddState();

  @override
  List<Object> get props => [];
}

class ContactAddInitial extends ContactAddState {}

class ContactAddInProgress extends ContactAddState {}

class ContactaddSuccess extends ContactAddState {
  final String formResponse;

  ContactaddSuccess({this.formResponse});

  @override
  String toString() =>
      'ContactaddcheckSuccess : {contactAddCheck : $formResponse}';
}

class ContactAddFailure extends ContactAddState {
  final String error;
  ContactAddFailure({this.error});

  @override
  String toString() => 'ContactAddFailure : {ContactAddFailure : $error}';
}

class ContactCopyInProgress extends ContactAddState {}

class ContactCopySuccess extends ContactAddState {
  final String formResponse;

  ContactCopySuccess({this.formResponse});

  @override
  String toString() =>
      'ContactaddcheckSuccess : {contactAddCheck : $formResponse}';
}

class ContactCopyFailure extends ContactAddState {
  final String error;
  ContactCopyFailure({this.error});

  @override
  String toString() => 'ContactCopyFailure : {ContactCopyFailure : $error}';
}
