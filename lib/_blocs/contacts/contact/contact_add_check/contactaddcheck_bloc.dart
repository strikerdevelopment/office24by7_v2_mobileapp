import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contactaddcheck_event.dart';
part 'contactaddcheck_state.dart';

class ContactaddcheckBloc
    extends Bloc<ContactaddcheckEvent, ContactaddcheckState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactaddcheckBloc({this.contactRepository})
      : super(ContactaddcheckInitial());

  @override
  Stream<ContactaddcheckState> mapEventToState(
    ContactaddcheckEvent event,
  ) async* {
    if (event is ContactaddcheckPressed) {
      yield ContactaddcheckInitial();
      try {
        ContactAddCheck contactAddCheck = await contactRepository
            .getcontactAddCheck(await environment.getToken());
        yield ContactaddcheckSuccess(contactAddCheck: contactAddCheck);
      } catch (_) {
        yield ContactaddcheckFailure();
      }
    }
  }
}
