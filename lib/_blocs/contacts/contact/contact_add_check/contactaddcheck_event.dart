part of 'contactaddcheck_bloc.dart';

abstract class ContactaddcheckEvent extends Equatable {
  const ContactaddcheckEvent();

  @override
  List<Object> get props => [];
}

class ContactaddcheckPressed extends ContactaddcheckEvent {}
