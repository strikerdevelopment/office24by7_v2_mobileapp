part of 'contactaddcheck_bloc.dart';

abstract class ContactaddcheckState extends Equatable {
  const ContactaddcheckState();

  @override
  List<Object> get props => [];
}

class ContactaddcheckInitial extends ContactaddcheckState {}

class ContactaddcheckSuccess extends ContactaddcheckState {
  final ContactAddCheck contactAddCheck;

  ContactaddcheckSuccess({this.contactAddCheck});

  @override
  String toString() =>
      'ContactaddcheckSuccess : {contactAddCheck : ${contactAddCheck.formId}}';
}

class ContactaddcheckFailure extends ContactaddcheckState {}
