import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_add_meeting_event.dart';
part 'contact_add_meeting_state.dart';

class ContactAddMeetingBloc
    extends Bloc<ContactAddMeetingEvent, ContactAddMeetingState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactAddMeetingBloc({this.contactRepository})
      : super(ContactAddMeetingInitial());

  @override
  Stream<ContactAddMeetingState> mapEventToState(
    ContactAddMeetingEvent event,
  ) async* {
    if (event is ContactAddMeetingPressed) {
      yield ContactAddMeetingInProgress();
      try {
        String getData = await contactRepository.getAddContactMeeting(
            await environment.getToken(), event.eventData);
        yield ContactAddMeetingSuccess(stateData: getData);
      } catch (_) {
        yield ContactAddMeetingFailure(error: _.toString());
      }
    }
  }
}
