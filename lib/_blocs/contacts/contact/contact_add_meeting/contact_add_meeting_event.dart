part of 'contact_add_meeting_bloc.dart';

abstract class ContactAddMeetingEvent extends Equatable {
  const ContactAddMeetingEvent();

  @override
  List<Object> get props => [];
}

class ContactAddMeetingPressed extends ContactAddMeetingEvent {
  final Map eventData;
  ContactAddMeetingPressed({this.eventData});
}
