part of 'contact_add_meeting_bloc.dart';

abstract class ContactAddMeetingState extends Equatable {
  const ContactAddMeetingState();

  @override
  List<Object> get props => [];
}

class ContactAddMeetingInitial extends ContactAddMeetingState {}

class ContactAddMeetingInProgress extends ContactAddMeetingState {}

class ContactAddMeetingSuccess extends ContactAddMeetingState {
  final String stateData;

  ContactAddMeetingSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactAddMeetingFailure extends ContactAddMeetingState {
  final String error;
  const ContactAddMeetingFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
