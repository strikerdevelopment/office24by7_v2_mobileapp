import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_add_to_list_event.dart';
part 'contact_add_to_list_state.dart';

class ContactAddToListBloc
    extends Bloc<ContactAddToListEvent, ContactAddToListState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactAddToListBloc({this.contactRepository})
      : super(ContactAddToListInitial());

  @override
  Stream<ContactAddToListState> mapEventToState(
    ContactAddToListEvent event,
  ) async* {
    if (event is ContactAddToListPressed) {
      yield ContactAddToListInProgress();
      try {
        String getData = await contactRepository.getAddContactToList(
            await environment.getToken(), event.eventData);
        yield ContactAddToListSuccess(stateData: getData);
      } catch (_) {
        yield ContactAddToListFailure(error: _.toString());
      }
    }
  }
}
