part of 'contact_add_to_list_bloc.dart';

abstract class ContactAddToListEvent extends Equatable {
  const ContactAddToListEvent();

  @override
  List<Object> get props => [];
}

class ContactAddToListPressed extends ContactAddToListEvent {
  final Map eventData;
  ContactAddToListPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
