part of 'contact_add_to_list_bloc.dart';

abstract class ContactAddToListState extends Equatable {
  const ContactAddToListState();

  @override
  List<Object> get props => [];
}

class ContactAddToListInitial extends ContactAddToListState {}

class ContactAddToListInProgress extends ContactAddToListState {}

class ContactAddToListSuccess extends ContactAddToListState {
  final String stateData;

  ContactAddToListSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactAddToListFailure extends ContactAddToListState {
  final String error;
  const ContactAddToListFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
