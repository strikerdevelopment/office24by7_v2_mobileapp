import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_add_to_reassign_event.dart';
part 'contact_add_to_reassign_state.dart';

class ContactAddToReassignBloc
    extends Bloc<ContactAddToReassignEvent, ContactAddToReassignState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactAddToReassignBloc({this.contactRepository})
      : super(ContactAddToReassignInitial());

  @override
  Stream<ContactAddToReassignState> mapEventToState(
    ContactAddToReassignEvent event,
  ) async* {
    if (event is ContactAddToReassignPressed) {
      yield ContactAddToReassignInProgress();
      try {
        String getData = await contactRepository.getContactAddToReassign(
            await environment.getToken(), event.eventData);
        yield ContactAddToReassignSuccess(stateData: getData);
      } catch (_) {
        yield ContactAddToReassignFailure(error: _.toString());
      }
    }
  }
}
