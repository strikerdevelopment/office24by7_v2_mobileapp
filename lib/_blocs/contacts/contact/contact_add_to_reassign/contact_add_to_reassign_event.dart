part of 'contact_add_to_reassign_bloc.dart';

abstract class ContactAddToReassignEvent extends Equatable {
  const ContactAddToReassignEvent();

  @override
  List<Object> get props => [];
}

class ContactAddToReassignPressed extends ContactAddToReassignEvent {
  final Map eventData;
  ContactAddToReassignPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
