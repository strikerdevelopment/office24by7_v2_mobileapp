part of 'contact_add_to_reassign_bloc.dart';

abstract class ContactAddToReassignState extends Equatable {
  const ContactAddToReassignState();

  @override
  List<Object> get props => [];
}

class ContactAddToReassignInitial extends ContactAddToReassignState {}

class ContactAddToReassignInProgress extends ContactAddToReassignState {}

class ContactAddToReassignSuccess extends ContactAddToReassignState {
  final String stateData;

  ContactAddToReassignSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactAddToReassignFailure extends ContactAddToReassignState {
  final String error;
  const ContactAddToReassignFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
