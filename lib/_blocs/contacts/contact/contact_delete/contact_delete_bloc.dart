import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_delete_event.dart';
part 'contact_delete_state.dart';

class ContactDeleteBloc extends Bloc<ContactDeleteEvent, ContactDeleteState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactDeleteBloc({this.contactRepository}) : super(ContactDeleteInitial());

  @override
  Stream<ContactDeleteState> mapEventToState(
    ContactDeleteEvent event,
  ) async* {
    if (event is ContactDeletePressed) {
      yield ContactDeleteInProgress();
      try {
        String getData = await contactRepository.getContactDelete(
            await environment.getToken(), event.eventData);
        yield ContactDeleteSuccess(stateData: getData);
      } catch (_) {
        yield ContactDeleteFailure(error: _.toString());
      }
    }
  }
}
