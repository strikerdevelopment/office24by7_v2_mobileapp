part of 'contact_delete_bloc.dart';

abstract class ContactDeleteEvent extends Equatable {
  const ContactDeleteEvent();

  @override
  List<Object> get props => [];
}

class ContactDeletePressed extends ContactDeleteEvent {
  final Map eventData;
  ContactDeletePressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
