part of 'contact_delete_bloc.dart';

abstract class ContactDeleteState extends Equatable {
  const ContactDeleteState();

  @override
  List<Object> get props => [];
}

class ContactDeleteInitial extends ContactDeleteState {}

class ContactDeleteInProgress extends ContactDeleteState {}

class ContactDeleteSuccess extends ContactDeleteState {
  final String stateData;

  ContactDeleteSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactDeleteFailure extends ContactDeleteState {
  final String error;
  const ContactDeleteFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
