import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_details_event.dart';
part 'contact_details_state.dart';

class ContactDetailsBloc
    extends Bloc<ContactDetailsEvent, ContactDetailsState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactDetailsBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactDetailsInitial());

  @override
  Stream<ContactDetailsState> mapEventToState(
    ContactDetailsEvent event,
  ) async* {
    if (event is ContactDetailsPressed) {
      yield ContactDetailsInitial();
      try {
        final ContactDetailsModel contactDetails = await contactRepository
            .getContactDetails(await environment.getToken(), event.contactId);
        yield ContactDetailsSuccess(contactDetails: contactDetails);
      } catch (_) {
        yield ContactsDetailsFailure();
      }
    }
  }
}
