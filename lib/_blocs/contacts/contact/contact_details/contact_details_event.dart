part of 'contact_details_bloc.dart';

abstract class ContactDetailsEvent extends Equatable {
  const ContactDetailsEvent();
  @override
  List<Object> get props => [];
}

class ContactDetailsPressed extends ContactDetailsEvent {
  final String contactId;
  const ContactDetailsPressed({@required this.contactId})
      : assert(contactId != null);

  @override
  List<Object> get props => [contactId];

  @override
  String toString() => 'ContactDetailsPressed {"contactId": $contactId}';
}
