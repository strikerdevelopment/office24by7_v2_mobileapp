part of 'contact_details_bloc.dart';

abstract class ContactDetailsState extends Equatable {
  const ContactDetailsState();
  @override
  List<Object> get props => [];
}

class ContactDetailsInitial extends ContactDetailsState {}

class ContactsDetailsFailure extends ContactDetailsState {}

class ContactDetailsSuccess extends ContactDetailsState {
  final ContactDetailsModel contactDetails;

  const ContactDetailsSuccess({this.contactDetails});

  @override
  List<Object> get props => [contactDetails];

  @override
  String toString() =>
      'ContactDetailsSuccess { contactDetails: $contactDetails }';
}
