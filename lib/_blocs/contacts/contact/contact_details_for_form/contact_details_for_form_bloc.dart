import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_details_for_form_event.dart';
part 'contact_details_for_form_state.dart';

class ContactDetailsForFormBloc
    extends Bloc<ContactDetailsForFormEvent, ContactDetailsForFormState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactDetailsForFormBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactDetailsForFormInitial());

  @override
  Stream<ContactDetailsForFormState> mapEventToState(
    ContactDetailsForFormEvent event,
  ) async* {
    if (event is ContactDetailsForFormPressed) {
      yield ContactDetailsForFormInitial();
      try {
        final Map contactDetails =
            await contactRepository.getContactDetailsForForm(
                await environment.getToken(), event.contactId);
        yield ContactDetailsForFormSuccess(stateData: contactDetails);
      } catch (_) {
        yield ContactDetailsForFormFailure(error: _.toString());
      }
    }
  }
}
