part of 'contact_details_for_form_bloc.dart';

abstract class ContactDetailsForFormEvent extends Equatable {
  const ContactDetailsForFormEvent();

  @override
  List<Object> get props => [];
}

class ContactDetailsForFormPressed extends ContactDetailsForFormEvent {
  final String contactId;
  const ContactDetailsForFormPressed({this.contactId})
      : assert(contactId != null);

  @override
  List<Object> get props => [contactId];

  @override
  String toString() => 'ContactDetailsForFormPressed {"contactId": $contactId}';
}
