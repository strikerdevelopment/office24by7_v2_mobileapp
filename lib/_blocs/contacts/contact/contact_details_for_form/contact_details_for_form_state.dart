part of 'contact_details_for_form_bloc.dart';

abstract class ContactDetailsForFormState extends Equatable {
  const ContactDetailsForFormState();

  @override
  List<Object> get props => [];
}

class ContactDetailsForFormInitial extends ContactDetailsForFormState {}

class ContactDetailsForFormFailure extends ContactDetailsForFormState {
  final String error;

  ContactDetailsForFormFailure({this.error});

  @override
  @override
  List<Object> get props => [error];

  @override
  String toString() =>
      'ContactDetailsForFormFailure { contactDetails: $error }';
}

class ContactDetailsForFormSuccess extends ContactDetailsForFormState {
  final Map stateData;

  const ContactDetailsForFormSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactDetailsForFormSuccess { contactDetails: $stateData }';
}
