import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_edit_event.dart';
part 'contact_edit_state.dart';

class ContactEditBloc extends Bloc<ContactEditEvent, ContactEditState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactEditBloc({this.contactRepository}) : super(ContactEditInitial());

  @override
  Stream<ContactEditState> mapEventToState(
    ContactEditEvent event,
  ) async* {
    if (event is ContactEditPressed) {
      yield ContactEditInProgress();
      try {
        dynamic contactEditData = await contactRepository.getEditContact(
            await environment.getToken(), event.formData);
        yield ContactEditSuccess(stateData: contactEditData);
      } catch (_) {
        yield ContactEditFailure(error: _.toString());
      }
    }
  }
}
