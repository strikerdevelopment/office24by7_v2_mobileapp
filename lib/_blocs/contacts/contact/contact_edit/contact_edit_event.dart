part of 'contact_edit_bloc.dart';

abstract class ContactEditEvent extends Equatable {
  const ContactEditEvent();

  @override
  List<Object> get props => [];
}

class ContactEditPressed extends ContactEditEvent {
  final Map formData;
  const ContactEditPressed({this.formData});
}
