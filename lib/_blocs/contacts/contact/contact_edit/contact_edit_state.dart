part of 'contact_edit_bloc.dart';

abstract class ContactEditState extends Equatable {
  const ContactEditState();

  @override
  List<Object> get props => [];
}

class ContactEditInitial extends ContactEditState {}

class ContactEditInProgress extends ContactEditState {}

class ContactEditSuccess extends ContactEditState {
  final String stateData;

  ContactEditSuccess({this.stateData});

  @override
  String toString() =>
      'ContactEditcheckSuccess : {ContactEditCheck : $stateData}';
}

class ContactEditFailure extends ContactEditState {
  final String error;
  ContactEditFailure({this.error});
}
