import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'contact_email_id_event.dart';
part 'contact_email_id_state.dart';

class ContactEmailIdBloc
    extends Bloc<ContactEmailIdEvent, ContactEmailIdState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactEmailIdBloc({this.contactRepository}) : super(ContactEmailIdInitial());

  @override
  Stream<ContactEmailIdState> mapEventToState(
    ContactEmailIdEvent event,
  ) async* {
    if (event is ContactEmailIDPressed) {
      yield ContactEmailIdInitial();
      try {
        List<GetContactEmailId> getData = await contactRepository
            .loadgetcontactEmailID(await environment.getToken());
        yield ContactEmailIdSuccess(stateData: getData);
      } catch (_) {
        yield ContactEmailIdFailure(error: _.toString());
      }
    }
  }
}
