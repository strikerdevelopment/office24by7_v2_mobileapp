part of 'contact_email_id_bloc.dart';

abstract class ContactEmailIdEvent extends Equatable {
  const ContactEmailIdEvent();

  @override
  List<Object> get props => [];
}

class ContactEmailIDPressed extends ContactEmailIdEvent {}
