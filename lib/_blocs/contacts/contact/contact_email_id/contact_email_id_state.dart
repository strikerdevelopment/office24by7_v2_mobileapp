part of 'contact_email_id_bloc.dart';

abstract class ContactEmailIdState extends Equatable {
  const ContactEmailIdState();

  @override
  List<Object> get props => [];
}

class ContactEmailIdInitial extends ContactEmailIdState {}

class ContactEmailIdSuccess extends ContactEmailIdState {
  final List<GetContactEmailId> stateData;

  ContactEmailIdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactEmailIdFailure extends ContactEmailIdState {
  final String error;
  const ContactEmailIdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
