import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'contactformload_event.dart';
part 'contactformload_state.dart';

class ContactformloadBloc
    extends Bloc<ContactformloadEvent, ContactformloadState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactformloadBloc({this.contactRepository})
      : super(ContactformloadInitial());

  @override
  Stream<ContactformloadState> mapEventToState(
    ContactformloadEvent event,
  ) async* {
    if (event is ContactFormLoadPressed) {
      yield ContactformloadInitial();
      try {
        final List<ContactFormLoad> contactFormLoadData =
            await contactRepository
                .getcontactFormLoad(await environment.getToken());

        yield ContactformloadSuccess(contactFormLoadData: contactFormLoadData);
      } catch (_) {
        yield ContactformloadFailure();
      }
    }
  }
}
