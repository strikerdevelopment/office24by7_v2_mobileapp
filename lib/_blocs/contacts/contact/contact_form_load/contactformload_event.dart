part of 'contactformload_bloc.dart';

abstract class ContactformloadEvent extends Equatable {
  const ContactformloadEvent();

  @override
  List<Object> get props => [];
}

class ContactFormLoadPressed extends ContactformloadEvent {}
