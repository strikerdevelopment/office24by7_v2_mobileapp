part of 'contactformload_bloc.dart';

abstract class ContactformloadState extends Equatable {
  const ContactformloadState();

  @override
  List<Object> get props => [];
}

class ContactformloadInitial extends ContactformloadState {}

class ContactformloadSuccess extends ContactformloadState {
  final List<ContactFormLoad> contactFormLoadData;

  ContactformloadSuccess({this.contactFormLoadData});

  @override
  String toString() =>
      'ContactformloadSuccess : {contactFormLoadData: ${contactFormLoadData.length}}';
}

class ContactformloadFailure extends ContactformloadState {}
