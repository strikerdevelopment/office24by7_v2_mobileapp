import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_from_email_event.dart';
part 'contact_from_email_state.dart';

class ContactFromEmailBloc
    extends Bloc<ContactFromEmailEvent, ContactFromEmailState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactFromEmailBloc({this.contactRepository})
      : super(ContactFromEmailInitial());

  @override
  Stream<ContactFromEmailState> mapEventToState(
    ContactFromEmailEvent event,
  ) async* {
    if (event is ContactFromEmailPressed) {
      yield ContactFromEmailInitial();
      try {
        List<ContactFromEmail> getData = await contactRepository
            .getContactsFromEmail(await environment.getToken());
        yield ContactFromEmailSuccess(stateData: getData);
      } catch (_) {
        yield ContactFromEmailFailure(error: _.toString());
      }
    }
  }
}
