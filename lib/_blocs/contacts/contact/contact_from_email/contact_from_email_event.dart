part of 'contact_from_email_bloc.dart';

abstract class ContactFromEmailEvent extends Equatable {
  const ContactFromEmailEvent();

  @override
  List<Object> get props => [];
}

class ContactFromEmailPressed extends ContactFromEmailEvent {}
