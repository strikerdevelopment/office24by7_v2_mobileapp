part of 'contact_from_email_bloc.dart';

abstract class ContactFromEmailState extends Equatable {
  const ContactFromEmailState();

  @override
  List<Object> get props => [];
}

class ContactFromEmailInitial extends ContactFromEmailState {}

class ContactFromEmailSuccess extends ContactFromEmailState {
  final List<ContactFromEmail> stateData;

  ContactFromEmailSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactFromEmailSuccess { stateData: ${stateData.length} }';
}

class ContactFromEmailFailure extends ContactFromEmailState {
  final String error;
  const ContactFromEmailFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactFromEmailFailure { error: $error }';
}
