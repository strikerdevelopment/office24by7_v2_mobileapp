import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'contact_get_more_fields_event.dart';
part 'contact_get_more_fields_state.dart';

class ContactGetMoreFieldsBloc
    extends Bloc<ContactGetMoreFieldsEvent, ContactGetMoreFieldsState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactGetMoreFieldsBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactGetMoreFieldsInitial());

  @override
  Stream<ContactGetMoreFieldsState> mapEventToState(
    ContactGetMoreFieldsEvent event,
  ) async* {
    if (event is ContactGetMoreFieldsPressed) {
      yield ContactGetMoreFieldsInitial();
      try {
        final List<GetMoreFields> getMoreFieldsData = await contactRepository
            .getGetMoreFiels(await environment.getToken(), event.fieldType);

        yield ContactGetMoreFieldsSuccess(getMoreFieldsData: getMoreFieldsData);
      } catch (_) {
        yield ContactGetMoreFieldsFailure();
      }
    }
  }
}
