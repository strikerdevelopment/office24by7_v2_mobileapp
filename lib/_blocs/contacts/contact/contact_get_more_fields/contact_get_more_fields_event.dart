part of 'contact_get_more_fields_bloc.dart';

abstract class ContactGetMoreFieldsEvent extends Equatable {
  const ContactGetMoreFieldsEvent();

  @override
  List<Object> get props => [];
}

class ContactGetMoreFieldsPressed extends ContactGetMoreFieldsEvent {
  final String fieldType;

  ContactGetMoreFieldsPressed({this.fieldType});

  @override
  List<Object> get props => [fieldType];
}
