part of 'contact_get_more_fields_bloc.dart';

abstract class ContactGetMoreFieldsState extends Equatable {
  const ContactGetMoreFieldsState();

  @override
  List<Object> get props => [];
}

class ContactGetMoreFieldsInitial extends ContactGetMoreFieldsState {}

class ContactGetMoreFieldsSuccess extends ContactGetMoreFieldsState {
  final List<GetMoreFields> getMoreFieldsData;

  ContactGetMoreFieldsSuccess({this.getMoreFieldsData});

  @override
  String toString() =>
      'ContactGetMoreFieldsSuccess : {getMoreFieldsData : ${getMoreFieldsData.length}}';
}

class ContactGetMoreFieldsFailure extends ContactGetMoreFieldsState {}
