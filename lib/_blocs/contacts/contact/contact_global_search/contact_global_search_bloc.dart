import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_global_search_event.dart';
part 'contact_global_search_state.dart';

class ContactGlobalSearchBloc
    extends Bloc<ContactGlobalSearchEvent, ContactGlobalSearchState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactGlobalSearchBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactGlobalSearchInitial());

  @override
  Stream<ContactGlobalSearchState> mapEventToState(
    ContactGlobalSearchEvent event,
  ) async* {
    final currentState = state;
    if (event is ContactGlobalSearchFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ContactGlobalSearchInitial) {
          final List<dynamic> contacts =
              await contactRepository.getContactsGlobalSearch(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.keyword);
          yield ContactGlobalSearchSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is ContactGlobalSearchSuccess) {
          final List<dynamic> contacts =
              await contactRepository.getContactsGlobalSearch(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.keyword,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ContactGlobalSearchSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield ContactGlobalSearchFailure(error: _.toString());
      }
    }

    if (event is ContactGlobalSearchRefresh) {
      yield ContactGlobalSearchInitial();
      try {
        final List<dynamic> contacts =
            await contactRepository.getContactsGlobalSearch(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.keyword);
        yield ContactGlobalSearchSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield ContactGlobalSearchFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ContactGlobalSearchState state) =>
      state is ContactGlobalSearchSuccess && state.hasReachedMax;
}
