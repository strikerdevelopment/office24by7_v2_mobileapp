part of 'contact_global_search_bloc.dart';

abstract class ContactGlobalSearchEvent extends Equatable {
  const ContactGlobalSearchEvent();

  @override
  List<Object> get props => [];
}

class ContactGlobalSearchFetched extends ContactGlobalSearchEvent {
  final String filterHeader;
  final String keyword;

  const ContactGlobalSearchFetched({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, keyword];
}

class ContactGlobalSearchRefresh extends ContactGlobalSearchEvent {
  final String filterHeader;
  final String keyword;
  const ContactGlobalSearchRefresh({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
