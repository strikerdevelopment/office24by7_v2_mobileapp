part of 'contact_global_search_bloc.dart';

abstract class ContactGlobalSearchState extends Equatable {
  const ContactGlobalSearchState();

  @override
  List<Object> get props => [];
}

class ContactGlobalSearchInitial extends ContactGlobalSearchState {}

class ContactGlobalSearchFailure extends ContactGlobalSearchState {
  final String error;

  ContactGlobalSearchFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactGlobalSearchFailure { error: $error }';
}

class ContactGlobalSearchSuccess extends ContactGlobalSearchState {
  final List<dynamic> contacts;
  final bool hasReachedMax;
  final List<dynamic> currentState;

  const ContactGlobalSearchSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  ContactGlobalSearchSuccess copyWith({
    List<dynamic> contacts,
    bool hasReachedMax,
  }) {
    return ContactGlobalSearchSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'ContactGlobalSearchSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
