import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'contact_group_event.dart';
part 'contact_group_state.dart';

class ContactGroupBloc extends Bloc<ContactGroupEvent, ContactGroupState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactGroupBloc({this.contactRepository}) : super(ContactGroupInitial());

  @override
  Stream<ContactGroupState> mapEventToState(
    ContactGroupEvent event,
  ) async* {
    if (event is ContactGroupPressed) {
      yield ContactGroupInitial();
      try {
        final List<ContactGroup> contactGroupData = await contactRepository
            .getContactGroup(await environment.getToken());

        yield ContactGroupSuccess(contactGroupData: contactGroupData);
      } catch (_) {
        yield ContactGroupFailure();
      }
    }
  }
}
