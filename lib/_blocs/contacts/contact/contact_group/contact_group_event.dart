part of 'contact_group_bloc.dart';

abstract class ContactGroupEvent extends Equatable {
  const ContactGroupEvent();

  @override
  List<Object> get props => [];
}

class ContactGroupPressed extends ContactGroupEvent {}
