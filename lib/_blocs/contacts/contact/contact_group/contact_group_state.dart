part of 'contact_group_bloc.dart';

abstract class ContactGroupState extends Equatable {
  const ContactGroupState();

  @override
  List<Object> get props => [];
}

class ContactGroupInitial extends ContactGroupState {}

class ContactGroupSuccess extends ContactGroupState {
  final List<ContactGroup> contactGroupData;
  ContactGroupSuccess({this.contactGroupData});

  @override
  String toString() =>
      'ContactGroupSuccess : {contactGroupSuccess : ${contactGroupData.length}}';
}

class ContactGroupFailure extends ContactGroupState {}
