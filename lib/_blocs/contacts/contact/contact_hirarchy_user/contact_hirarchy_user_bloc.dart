import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_hirarchy_user_event.dart';
part 'contact_hirarchy_user_state.dart';

class ContactHirarchyUserBloc
    extends Bloc<ContactHirarchyUserEvent, ContactHirarchyUserState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactHirarchyUserBloc({this.contactRepository})
      : super(ContactHirarchyUserInitial());

  @override
  Stream<ContactHirarchyUserState> mapEventToState(
    ContactHirarchyUserEvent event,
  ) async* {
    if (event is ContactHirarchyUserPressed) {
      yield ContactHirarchyUserInitial();
      try {
        List<ContactHirarchyUser> getData = await contactRepository
            .getContactHirarchyUser(await environment.getToken());
        yield ContactHirarchyUserSuccess(stateData: getData);
      } catch (_) {
        yield ContactHirarchyUserFailure(error: _.toString());
      }
    }
  }
}
