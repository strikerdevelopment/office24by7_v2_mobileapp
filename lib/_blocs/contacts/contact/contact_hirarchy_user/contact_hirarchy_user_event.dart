part of 'contact_hirarchy_user_bloc.dart';

abstract class ContactHirarchyUserEvent extends Equatable {
  const ContactHirarchyUserEvent();

  @override
  List<Object> get props => [];
}

class ContactHirarchyUserPressed extends ContactHirarchyUserEvent {}
