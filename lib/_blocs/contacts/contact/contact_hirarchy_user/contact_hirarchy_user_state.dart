part of 'contact_hirarchy_user_bloc.dart';

abstract class ContactHirarchyUserState extends Equatable {
  const ContactHirarchyUserState();

  @override
  List<Object> get props => [];
}

class ContactHirarchyUserInitial extends ContactHirarchyUserState {}

class ContactHirarchyUserSuccess extends ContactHirarchyUserState {
  final List<ContactHirarchyUser> stateData;

  ContactHirarchyUserSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactHirarchyUserSuccess { stateData: ${stateData.length} }';
}

class ContactHirarchyUserFailure extends ContactHirarchyUserState {
  final String error;
  const ContactHirarchyUserFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactHirarchyUserFailure { error: $error }';
}
