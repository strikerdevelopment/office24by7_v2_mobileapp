import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/global/global_model.dart';

part 'contact_history_event.dart';
part 'contact_history_state.dart';

class ContactHistoryBloc
    extends Bloc<ContactHistoryEvent, ContactHistoryState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactHistoryBloc({this.contactRepository}) : super(ContactHistoryInitial());

  @override
  Stream<ContactHistoryState> mapEventToState(
    ContactHistoryEvent event,
  ) async* {
    if (event is ContactHistoryPressed) {
      yield ContactHistoryInitial();
      try {
        List<GlobalHistoryModel> getData =
            await contactRepository.getContactHistoryData(
                await environment.getToken(), event.eventData);
        yield ContactHistorySuccess(stateData: getData);
      } catch (_) {
        yield ContactHistoryFailure(error: _.toString());
      }
    }
  }
}
