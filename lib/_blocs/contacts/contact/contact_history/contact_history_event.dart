part of 'contact_history_bloc.dart';

abstract class ContactHistoryEvent extends Equatable {
  const ContactHistoryEvent();

  @override
  List<Object> get props => [];
}

class ContactHistoryPressed extends ContactHistoryEvent {
  final String eventData;
  ContactHistoryPressed({this.eventData});

  @override
  List<Object> get props => [eventData];

  @override
  String toString() => 'ContactHistoryPressed {"contactId": $eventData}';
}
