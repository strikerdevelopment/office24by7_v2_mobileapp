part of 'contact_history_bloc.dart';

abstract class ContactHistoryState extends Equatable {
  const ContactHistoryState();

  @override
  List<Object> get props => [];
}

class ContactHistoryInitial extends ContactHistoryState {}

class ContactHistorySuccess extends ContactHistoryState {
  final List<GlobalHistoryModel> stateData;

  ContactHistorySuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactHistorySuccess { stateData: ${stateData.length} }';
}

class ContactHistoryFailure extends ContactHistoryState {
  final String error;
  const ContactHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
