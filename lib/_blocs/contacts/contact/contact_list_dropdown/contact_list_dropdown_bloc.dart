import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_list_dropdown_event.dart';
part 'contact_list_dropdown_state.dart';

class ContactListDropdownBloc
    extends Bloc<ContactListDropdownEvent, ContactListDropdownState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactListDropdownBloc({this.contactRepository})
      : super(ContactListDropdownInitial());

  @override
  Stream<ContactListDropdownState> mapEventToState(
    ContactListDropdownEvent event,
  ) async* {
    if (event is ContactListDropdownPressed) {
      yield ContactListDropdownInitial();
      try {
        List<ContactListDropdown> getData = await contactRepository
            .getContactListDropdown(await environment.getToken());
        yield ContactListDropdownSuccess(stateData: getData);
      } catch (_) {
        yield ContactListDropdownFailure(error: _.toString());
      }
    }
  }
}
