part of 'contact_list_dropdown_bloc.dart';

abstract class ContactListDropdownEvent extends Equatable {
  const ContactListDropdownEvent();

  @override
  List<Object> get props => [];
}

class ContactListDropdownPressed extends ContactListDropdownEvent {}
