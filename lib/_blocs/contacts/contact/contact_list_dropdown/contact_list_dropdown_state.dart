part of 'contact_list_dropdown_bloc.dart';

abstract class ContactListDropdownState extends Equatable {
  const ContactListDropdownState();

  @override
  List<Object> get props => [];
}

class ContactListDropdownInitial extends ContactListDropdownState {}

class ContactListDropdownSuccess extends ContactListDropdownState {
  final List<ContactListDropdown> stateData;

  ContactListDropdownSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactListDropdownSuccess { stateData: ${stateData.length} }';
}

class ContactListDropdownFailure extends ContactListDropdownState {
  final String error;
  const ContactListDropdownFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactListDropdownFailure { error: $error }';
}
