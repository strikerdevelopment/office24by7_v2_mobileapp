import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_send_email_event.dart';
part 'contact_send_email_state.dart';

class ContactSendEmailBloc
    extends Bloc<ContactSendEmailEvent, ContactSendEmailState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactSendEmailBloc({this.contactRepository})
      : super(ContactSendEmailInitial());

  @override
  Stream<ContactSendEmailState> mapEventToState(
    ContactSendEmailEvent event,
  ) async* {
    if (event is ContactSendEmailPressed) {
      yield ContactSendEmailInProgress();
      try {
        String getData = await contactRepository.getContactSendEmail(
            await environment.getToken(), event.eventData);
        yield ContactSendEmailSuccess(stateData: getData);
      } catch (_) {
        yield ContactSendEmailFailure(error: _.toString());
      }
    }
  }
}
