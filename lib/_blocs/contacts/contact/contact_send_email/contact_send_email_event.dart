part of 'contact_send_email_bloc.dart';

abstract class ContactSendEmailEvent extends Equatable {
  const ContactSendEmailEvent();

  @override
  List<Object> get props => [];
}

class ContactSendEmailPressed extends ContactSendEmailEvent {
  final Map eventData;
  ContactSendEmailPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
