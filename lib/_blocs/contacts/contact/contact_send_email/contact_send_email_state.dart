part of 'contact_send_email_bloc.dart';

abstract class ContactSendEmailState extends Equatable {
  const ContactSendEmailState();

  @override
  List<Object> get props => [];
}

class ContactSendEmailInitial extends ContactSendEmailState {}

class ContactSendEmailInProgress extends ContactSendEmailState {}

class ContactSendEmailSuccess extends ContactSendEmailState {
  final String stateData;

  ContactSendEmailSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactSendEmailFailure extends ContactSendEmailState {
  final String error;
  const ContactSendEmailFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
