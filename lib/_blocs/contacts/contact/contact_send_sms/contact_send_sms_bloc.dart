import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_send_sms_event.dart';
part 'contact_send_sms_state.dart';

class ContactSendSmsBloc
    extends Bloc<ContactSendSmsEvent, ContactSendSmsState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactSendSmsBloc({this.contactRepository}) : super(ContactSendSmsInitial());

  @override
  Stream<ContactSendSmsState> mapEventToState(
    ContactSendSmsEvent event,
  ) async* {
    if (event is ContactSendSmsPressed) {
      yield ContactSendSmsInProgress();
      try {
        String getData = await contactRepository.getContactSendSMS(
            await environment.getToken(), event.eventData);
        yield ContactSendSmsSuccess(stateData: getData);
      } catch (_) {
        yield ContactSendSmsFailure(error: _.toString());
      }
    }
  }
}
