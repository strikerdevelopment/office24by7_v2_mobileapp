part of 'contact_send_sms_bloc.dart';

abstract class ContactSendSmsEvent extends Equatable {
  const ContactSendSmsEvent();

  @override
  List<Object> get props => [];
}

class ContactSendSmsPressed extends ContactSendSmsEvent {
  final Map eventData;
  ContactSendSmsPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
