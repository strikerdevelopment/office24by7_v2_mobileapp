part of 'contact_send_sms_bloc.dart';

abstract class ContactSendSmsState extends Equatable {
  const ContactSendSmsState();

  @override
  List<Object> get props => [];
}

class ContactSendSmsInitial extends ContactSendSmsState {}

class ContactSendSmsInProgress extends ContactSendSmsState {}

class ContactSendSmsSuccess extends ContactSendSmsState {
  final String stateData;

  ContactSendSmsSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactSendSmsFailure extends ContactSendSmsState {
  final String error;
  const ContactSendSmsFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
