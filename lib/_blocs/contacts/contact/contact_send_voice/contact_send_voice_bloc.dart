import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_send_voice_event.dart';
part 'contact_send_voice_state.dart';

class ContactSendVoiceBloc
    extends Bloc<ContactSendVoiceEvent, ContactSendVoiceState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactSendVoiceBloc({this.contactRepository})
      : super(ContactSendVoiceInitial());

  @override
  Stream<ContactSendVoiceState> mapEventToState(
    ContactSendVoiceEvent event,
  ) async* {
    if (event is ContactSendVoicePressed) {
      yield ContactSendVoiceInProgress();
      try {
        String getData = await contactRepository.getContactSendVoice(
            await environment.getToken(), event.eventData);
        yield ContactSendVoiceSuccess(stateData: getData);
      } catch (_) {
        yield ContactSendVoiceFailure(error: _.toString());
      }
    }
  }
}
