part of 'contact_send_voice_bloc.dart';

abstract class ContactSendVoiceEvent extends Equatable {
  const ContactSendVoiceEvent();

  @override
  List<Object> get props => [];
}

class ContactSendVoicePressed extends ContactSendVoiceEvent {
  final Map eventData;
  ContactSendVoicePressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
