part of 'contact_send_voice_bloc.dart';

abstract class ContactSendVoiceState extends Equatable {
  const ContactSendVoiceState();

  @override
  List<Object> get props => [];
}

class ContactSendVoiceInitial extends ContactSendVoiceState {}

class ContactSendVoiceInProgress extends ContactSendVoiceState {}

class ContactSendVoiceSuccess extends ContactSendVoiceState {
  final String stateData;

  ContactSendVoiceSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ContactSendVoiceFailure extends ContactSendVoiceState {
  final String error;
  const ContactSendVoiceFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
