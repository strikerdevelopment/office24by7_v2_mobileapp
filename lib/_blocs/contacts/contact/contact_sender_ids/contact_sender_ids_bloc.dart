import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_sender_ids_event.dart';
part 'contact_sender_ids_state.dart';

class ContactSenderIdsBloc
    extends Bloc<ContactSenderIdsEvent, ContactSenderIdsState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactSenderIdsBloc({this.contactRepository})
      : super(ContactSenderIdsInitial());

  @override
  Stream<ContactSenderIdsState> mapEventToState(
    ContactSenderIdsEvent event,
  ) async* {
    if (event is ContactSenderIdsPressed) {
      yield ContactSenderIdsInitial();
      try {
        List<ContactSenderIds> getData = await contactRepository
            .getContactsSenderIds(await environment.getToken());
        yield ContactSenderIdsSuccess(stateData: getData);
      } catch (_) {
        yield ContactSenderIdsFailure(error: _.toString());
      }
    }
  }
}
