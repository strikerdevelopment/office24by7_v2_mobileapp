part of 'contact_sender_ids_bloc.dart';

abstract class ContactSenderIdsEvent extends Equatable {
  const ContactSenderIdsEvent();

  @override
  List<Object> get props => [];
}

class ContactSenderIdsPressed extends ContactSenderIdsEvent {}
