part of 'contact_sender_ids_bloc.dart';

abstract class ContactSenderIdsState extends Equatable {
  const ContactSenderIdsState();

  @override
  List<Object> get props => [];
}

class ContactSenderIdsInitial extends ContactSenderIdsState {}

class ContactSenderIdsSuccess extends ContactSenderIdsState {
  final List<ContactSenderIds> stateData;

  ContactSenderIdsSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactSenderIdsSuccess { stateData: ${stateData.length} }';
}

class ContactSenderIdsFailure extends ContactSenderIdsState {
  final String error;
  const ContactSenderIdsFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactSenderIdsFailure { error: $error }';
}
