import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'contact_sms_type_event.dart';
part 'contact_sms_type_state.dart';

class ContactSmsTypeBloc
    extends Bloc<ContactSmsTypeEvent, ContactSmsTypeState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactSmsTypeBloc({this.contactRepository}) : super(ContactSmsTypeInitial());

  @override
  Stream<ContactSmsTypeState> mapEventToState(
    ContactSmsTypeEvent event,
  ) async* {
    if (event is ContactSmsTypePressed) {
      yield ContactSmsTypeInitial();
      try {
        List<dynamic> getData = await contactRepository
            .getContactsSMSType(await environment.getToken());
        yield ContactSmsTypeSuccess(stateData: getData);
      } catch (_) {
        yield ContactSmsTypeFailure(error: _.toString());
      }
    }
  }
}
