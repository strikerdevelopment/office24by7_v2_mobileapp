part of 'contact_sms_type_bloc.dart';

abstract class ContactSmsTypeEvent extends Equatable {
  const ContactSmsTypeEvent();

  @override
  List<Object> get props => [];
}

class ContactSmsTypePressed extends ContactSmsTypeEvent {}
