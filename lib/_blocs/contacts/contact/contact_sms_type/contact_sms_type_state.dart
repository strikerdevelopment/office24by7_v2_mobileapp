part of 'contact_sms_type_bloc.dart';

abstract class ContactSmsTypeState extends Equatable {
  const ContactSmsTypeState();

  @override
  List<Object> get props => [];
}

class ContactSmsTypeInitial extends ContactSmsTypeState {}

class ContactSmsTypeSuccess extends ContactSmsTypeState {
  final List<dynamic> stateData;

  ContactSmsTypeSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactSmsTypeSuccess { stateData: ${stateData.length} }';
}

class ContactSmsTypeFailure extends ContactSmsTypeState {
  final String error;
  const ContactSmsTypeFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactSmsTypeFailure { error: $error }';
}
