import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'contact_source_event.dart';
part 'contact_source_state.dart';

class ContactSourceBloc extends Bloc<ContactSourceEvent, ContactSourceState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactSourceBloc({this.contactRepository}) : super(ContactSourceInitial());

  @override
  Stream<ContactSourceState> mapEventToState(
    ContactSourceEvent event,
  ) async* {
    if (event is ContactSourcePressed) {
      yield ContactSourceInitial();
      try {
        final List<ContactSource> contactSourceData = await contactRepository
            .getContactSource(await environment.getToken());
        yield ContactSourceSuccess(contactSourceData: contactSourceData);
      } catch (_) {
        yield ContactSourceFailure();
      }
    }
  }
}
