part of 'contact_source_bloc.dart';

abstract class ContactSourceEvent extends Equatable {
  const ContactSourceEvent();

  @override
  List<Object> get props => [];
}

class ContactSourcePressed extends ContactSourceEvent {}
