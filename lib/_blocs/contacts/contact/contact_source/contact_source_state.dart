part of 'contact_source_bloc.dart';

abstract class ContactSourceState extends Equatable {
  const ContactSourceState();

  @override
  List<Object> get props => [];
}

class ContactSourceInitial extends ContactSourceState {}

class ContactSourceSuccess extends ContactSourceState {
  final List<ContactSource> contactSourceData;

  ContactSourceSuccess({this.contactSourceData});

  @override
  String toString() =>
      'ContactSourceSuccess : {contactSourceSuccess : ${contactSourceData.length}}';
}

class ContactSourceFailure extends ContactSourceState {}
