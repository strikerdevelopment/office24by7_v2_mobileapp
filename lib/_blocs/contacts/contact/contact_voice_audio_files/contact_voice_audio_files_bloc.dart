import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_voice_audio_files_event.dart';
part 'contact_voice_audio_files_state.dart';

class ContactVoiceAudioFilesBloc
    extends Bloc<ContactVoiceAudioFilesEvent, ContactVoiceAudioFilesState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactVoiceAudioFilesBloc({this.contactRepository})
      : super(ContactVoiceAudioFilesInitial());

  @override
  Stream<ContactVoiceAudioFilesState> mapEventToState(
    ContactVoiceAudioFilesEvent event,
  ) async* {
    if (event is ContactVoiceAudioFilesPressed) {
      yield ContactVoiceAudioFilesInitial();
      try {
        List<ContactVoiceAudioFiles> getData = await contactRepository
            .getContactVoiceAudioFiles(await environment.getToken());
        yield ContactVoiceAudioFilesSuccess(stateData: getData);
      } catch (_) {
        yield ContactVoiceAudioFilesFailure(error: _.toString());
      }
    }
  }
}
