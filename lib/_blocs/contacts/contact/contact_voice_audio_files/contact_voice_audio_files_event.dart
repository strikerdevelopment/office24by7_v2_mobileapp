part of 'contact_voice_audio_files_bloc.dart';

abstract class ContactVoiceAudioFilesEvent extends Equatable {
  const ContactVoiceAudioFilesEvent();

  @override
  List<Object> get props => [];
}

class ContactVoiceAudioFilesPressed extends ContactVoiceAudioFilesEvent {}
