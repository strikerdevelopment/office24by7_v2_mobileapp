part of 'contact_voice_audio_files_bloc.dart';

abstract class ContactVoiceAudioFilesState extends Equatable {
  const ContactVoiceAudioFilesState();

  @override
  List<Object> get props => [];
}

class ContactVoiceAudioFilesInitial extends ContactVoiceAudioFilesState {}

class ContactVoiceAudioFilesSuccess extends ContactVoiceAudioFilesState {
  final List<ContactVoiceAudioFiles> stateData;

  ContactVoiceAudioFilesSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactVoiceAudioFilesSuccess { stateData: ${stateData.length} }';
}

class ContactVoiceAudioFilesFailure extends ContactVoiceAudioFilesState {
  final String error;
  const ContactVoiceAudioFilesFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactVoiceAudioFilesFailure { error: $error }';
}
