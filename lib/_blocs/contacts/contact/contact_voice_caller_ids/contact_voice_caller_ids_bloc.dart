import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_voice_caller_ids_event.dart';
part 'contact_voice_caller_ids_state.dart';

class ContactVoiceCallerIdsBloc
    extends Bloc<ContactVoiceCallerIdsEvent, ContactVoiceCallerIdsState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactVoiceCallerIdsBloc({this.contactRepository})
      : super(ContactVoiceCallerIdsInitial());

  @override
  Stream<ContactVoiceCallerIdsState> mapEventToState(
    ContactVoiceCallerIdsEvent event,
  ) async* {
    if (event is ContactVoiceCallerIdsPressed) {
      yield ContactVoiceCallerIdsInitial();
      try {
        List<ContactVoiceCallerIds> getData = await contactRepository
            .getContactVoiceCallerIds(await environment.getToken());
        yield ContactVoiceCallerIdsSuccess(stateData: getData);
      } catch (_) {
        yield ContactVoiceCallerIdsFailure(error: _.toString());
      }
    }
  }
}
