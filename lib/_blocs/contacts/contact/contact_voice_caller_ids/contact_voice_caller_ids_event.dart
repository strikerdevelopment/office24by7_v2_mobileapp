part of 'contact_voice_caller_ids_bloc.dart';

abstract class ContactVoiceCallerIdsEvent extends Equatable {
  const ContactVoiceCallerIdsEvent();

  @override
  List<Object> get props => [];
}

class ContactVoiceCallerIdsPressed extends ContactVoiceCallerIdsEvent {}
