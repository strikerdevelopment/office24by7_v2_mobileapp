part of 'contact_voice_caller_ids_bloc.dart';

abstract class ContactVoiceCallerIdsState extends Equatable {
  const ContactVoiceCallerIdsState();

  @override
  List<Object> get props => [];
}

class ContactVoiceCallerIdsInitial extends ContactVoiceCallerIdsState {}

class ContactVoiceCallerIdsSuccess extends ContactVoiceCallerIdsState {
  final List<ContactVoiceCallerIds> stateData;

  ContactVoiceCallerIdsSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactVoiceCallerIdsSuccess { stateData: ${stateData.length} }';
}

class ContactVoiceCallerIdsFailure extends ContactVoiceCallerIdsState {
  final String error;
  const ContactVoiceCallerIdsFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactVoiceCallerIdsFailure { error: $error }';
}
