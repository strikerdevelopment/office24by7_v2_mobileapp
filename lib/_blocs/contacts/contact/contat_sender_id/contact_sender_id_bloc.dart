import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_sender_id_event.dart';
part 'contact_sender_id_state.dart';

class ContactSenderIdBloc
    extends Bloc<ContactSenderIdEvent, ContactSenderIdState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ContactSenderIdBloc({this.contactRepository})
      : super(ContactSenderIdInitial());

  @override
  Stream<ContactSenderIdState> mapEventToState(
    ContactSenderIdEvent event,
  ) async* {
    if (event is ContactSenderIDPressed) {
      yield ContactSenderIdInitial();
      try {
        List<GetContactSenderId> getData = await contactRepository
            .loadgetcontactSenderID(await environment.getToken());
        yield ContactSenderIdSuccess(getSenderID: getData);
      } catch (_) {
        yield ContactSenderIdFailure(error: _.toString());
      }
    }
  }
}
