part of 'contact_sender_id_bloc.dart';

abstract class ContactSenderIdEvent extends Equatable {
  const ContactSenderIdEvent();

  @override
  List<Object> get props => [];
}

class ContactSenderIDPressed extends ContactSenderIdEvent {}
