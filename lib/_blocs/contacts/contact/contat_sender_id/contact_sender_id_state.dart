part of 'contact_sender_id_bloc.dart';

abstract class ContactSenderIdState extends Equatable {
  const ContactSenderIdState();

  @override
  List<Object> get props => [];
}

class ContactSenderIdInitial extends ContactSenderIdState {}

class ContactSenderIdSuccess extends ContactSenderIdState {
  final List<GetContactSenderId> getSenderID;

  ContactSenderIdSuccess({this.getSenderID});

  @override
  List<Object> get props => [getSenderID];

  @override
  String toString() =>
      'ContactSenderIdSuccess { contactSenderID: ${getSenderID.length} }';
}

class ContactSenderIdFailure extends ContactSenderIdState {
  final String error;
  const ContactSenderIdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}
