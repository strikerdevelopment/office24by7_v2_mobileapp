import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'create_list_event.dart';
part 'create_list_state.dart';

class CreateListBloc extends Bloc<CreateListEvent, CreateListState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  CreateListBloc({this.contactRepository}) : super(CreateListInitial());

  @override
  Stream<CreateListState> mapEventToState(
    CreateListEvent event,
  ) async* {
    if (event is CreateListPressed) {
      yield CreateListInProgress();
      try {
        Map getData = await contactRepository.getCreateList(
            await environment.getToken(), event.eventData);
        yield CreateListSuccess(stateData: getData);
      } catch (_) {
        yield CreateListFailure(error: _.toString());
      }
    }
  }
}
