part of 'create_list_bloc.dart';

abstract class CreateListEvent extends Equatable {
  const CreateListEvent();

  @override
  List<Object> get props => [];
}

class CreateListPressed extends CreateListEvent {
  final Map eventData;
  CreateListPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
