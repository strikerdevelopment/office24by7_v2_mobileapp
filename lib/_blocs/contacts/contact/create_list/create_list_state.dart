part of 'create_list_bloc.dart';

abstract class CreateListState extends Equatable {
  const CreateListState();

  @override
  List<Object> get props => [];
}

class CreateListInitial extends CreateListState {}

class CreateListInProgress extends CreateListState {}

class CreateListSuccess extends CreateListState {
  final Map stateData;

  CreateListSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class CreateListFailure extends CreateListState {
  final String error;
  const CreateListFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
