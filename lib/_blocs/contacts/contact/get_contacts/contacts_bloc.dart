import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'contacts_event.dart';
part 'contacts_state.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactsBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactsInitial());

  @override
  Stream<ContactsState> mapEventToState(ContactsEvent event) async* {
    final currentState = state;
    if (event is ContactFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ContactsInitial) {
          final List<dynamic> contacts =
              await contactRepository.getContactsData(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.filterOption);
          yield ContactsSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is ContactsSuccess) {
          final List<dynamic> contacts =
              await contactRepository.getContactsData(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.filterOption,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ContactsSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield ContactsFailure(error: _.toString());
      }
    }
    if (event is ContactRefresh) {
      yield ContactsInitial();
      try {
        final List<dynamic> contacts = await contactRepository.getContactsData(
            0,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.filterOption);
        yield ContactsSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield ContactsFailure(error: _.toString());
      }
    }

    if (event is ContactDuplicatePressed) {
      yield ContactDuplicateInitial();
      try {
        List<ContactList> getData = await contactRepository
            .getContactsDuplicate(await environment.getToken(), event.cId);
        yield ContactDublicateSuccess(stateData: getData);
      } catch (_) {
        yield ContactDublicateFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ContactsState state) =>
      state is ContactsSuccess && state.hasReachedMax;
}
