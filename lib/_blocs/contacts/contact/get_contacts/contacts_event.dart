part of 'contacts_bloc.dart';

abstract class ContactsEvent extends Equatable {
  const ContactsEvent();
  List<Object> get props => [];
}

class ContactFetched extends ContactsEvent {
  final String filterHeader;
  final String filterOption;

  const ContactFetched({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, filterOption];
}

class ContactRefresh extends ContactsEvent {
  final String filterHeader;
  final String filterOption;
  const ContactRefresh({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}

class ContactDuplicatePressed extends ContactsEvent {
  final String cId;
  const ContactDuplicatePressed({this.cId}) : assert(cId != null);

  List<Object> get props => [cId];
}
