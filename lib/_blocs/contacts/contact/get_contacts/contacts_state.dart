part of 'contacts_bloc.dart';

abstract class ContactsState extends Equatable {
  const ContactsState();
  @override
  List<Object> get props => [];
}

class ContactsInitial extends ContactsState {}

class ContactsFailure extends ContactsState {
  final String error;

  ContactsFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactsFailure { error: $error }';
}

class ContactsSuccess extends ContactsState {
  final List<dynamic> contacts;
  final bool hasReachedMax;
  final List<dynamic> currentState;

  const ContactsSuccess({this.contacts, this.hasReachedMax, this.currentState});

  ContactsSuccess copyWith({
    List<dynamic> contacts,
    bool hasReachedMax,
  }) {
    return ContactsSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'ContactsSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}

class ContactDuplicateInitial extends ContactsState {}

class ContactDublicateSuccess extends ContactsState {
  final List<ContactList> stateData;

  ContactDublicateSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactDublicateSuccess { ContactDublicateSuccess: ${stateData.length} }';
}

class ContactDublicateFailure extends ContactsState {
  final String error;
  const ContactDublicateFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactDublicateFailure { error: $error }';
}
