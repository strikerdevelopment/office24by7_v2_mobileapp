import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'group_contacts_view_event.dart';
part 'group_contacts_view_state.dart';

class GroupContactsViewBloc
    extends Bloc<GroupContactsViewEvent, GroupContactsViewState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  GroupContactsViewBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(GroupContactsViewInitial());

  @override
  Stream<GroupContactsViewState> mapEventToState(
    GroupContactsViewEvent event,
  ) async* {
    final currentState = state;
    if (event is GroupContactsViewFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is GroupContactsViewInitial) {
          final List<ContactList> contacts =
              await contactRepository.getGroupContactsView(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.id);
          yield GroupContactsViewSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is GroupContactsViewSuccess) {
          final List<ContactList> contacts =
              await contactRepository.getGroupContactsView(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.id,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : GroupContactsViewSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield GroupContactsViewFailure(error: _.toString());
      }
    }

    if (event is GroupContactsViewRefresh) {
      yield GroupContactsViewInitial();
      try {
        final List<ContactList> contacts =
            await contactRepository.getGroupContactsView(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.id);
        yield GroupContactsViewSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield GroupContactsViewFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(GroupContactsViewState state) =>
      state is GroupContactsViewSuccess && state.hasReachedMax;
}
