part of 'group_contacts_view_bloc.dart';

abstract class GroupContactsViewEvent extends Equatable {
  const GroupContactsViewEvent();

  @override
  List<Object> get props => [];
}

class GroupContactsViewFetched extends GroupContactsViewEvent {
  final String filterHeader;
  final String id;

  const GroupContactsViewFetched({this.filterHeader, this.id})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, id];
}

class GroupContactsViewRefresh extends GroupContactsViewEvent {
  final String filterHeader;
  final String id;
  const GroupContactsViewRefresh({this.filterHeader, this.id})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
