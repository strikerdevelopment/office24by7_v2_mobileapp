part of 'group_contacts_view_bloc.dart';

abstract class GroupContactsViewState extends Equatable {
  const GroupContactsViewState();

  @override
  List<Object> get props => [];
}

class GroupContactsViewInitial extends GroupContactsViewState {}

class GroupContactsViewFailure extends GroupContactsViewState {
  final String error;

  GroupContactsViewFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GroupContactsViewFailure { error: $error }';
}

class GroupContactsViewSuccess extends GroupContactsViewState {
  final List<ContactList> contacts;
  final bool hasReachedMax;
  final List<ContactList> currentState;

  const GroupContactsViewSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  GroupContactsViewSuccess copyWith({
    List<ContactList> contacts,
    bool hasReachedMax,
  }) {
    return GroupContactsViewSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'GroupContactsViewSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
