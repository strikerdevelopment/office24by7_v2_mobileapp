import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'contact_group_list_event.dart';
part 'contact_group_list_state.dart';

class ContactGroupListBloc
    extends Bloc<ContactGroupListEvent, ContactGroupListState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactGroupListBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactGroupListInitial());

  @override
  Stream<ContactGroupListState> mapEventToState(
    ContactGroupListEvent event,
  ) async* {
    final currentState = state;
    if (event is ContactGroupListFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ContactGroupListInitial) {
          final List<ContactGroupList> contacts =
              await contactRepository.getContactGroupList(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.filterOption);
          yield ContactGroupListSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is ContactGroupListSuccess) {
          final List<ContactGroupList> contacts =
              await contactRepository.getContactGroupList(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.filterOption,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ContactGroupListSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield ContactGroupListFailure(error: _.toString());
      }
    }
    if (event is ContactGroupListRefresh) {
      yield ContactGroupListInitial();
      try {
        final List<ContactGroupList> contacts =
            await contactRepository.getContactGroupList(
          0,
          Environment.dataDisplayCount,
          event.filterHeader,
          await environment.getToken(),
          event.filterOption,
        );
        yield ContactGroupListSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield ContactGroupListFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ContactGroupListState state) =>
      state is ContactGroupListSuccess && state.hasReachedMax;
}
