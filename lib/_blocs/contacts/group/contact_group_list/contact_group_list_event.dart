part of 'contact_group_list_bloc.dart';

abstract class ContactGroupListEvent extends Equatable {
  const ContactGroupListEvent();

  @override
  List<Object> get props => [];
}

class ContactGroupListFetched extends ContactGroupListEvent {
  final String filterHeader;
  final String filterOption;

  const ContactGroupListFetched({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, filterOption];
}

class ContactGroupListRefresh extends ContactGroupListEvent {
  final String filterHeader;
  final String filterOption;
  const ContactGroupListRefresh({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
