part of 'contact_group_list_bloc.dart';

abstract class ContactGroupListState extends Equatable {
  const ContactGroupListState();

  @override
  List<Object> get props => [];
}

class ContactGroupListInitial extends ContactGroupListState {}

class ContactGroupListFailure extends ContactGroupListState {
  final String error;

  ContactGroupListFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactGroupListFailure { error: $error }';
}

class ContactGroupListSuccess extends ContactGroupListState {
  final List<ContactGroupList> contacts;
  final bool hasReachedMax;
  final List<dynamic> currentState;

  const ContactGroupListSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  ContactGroupListSuccess copyWith({
    List<ContactGroupList> contacts,
    bool hasReachedMax,
  }) {
    return ContactGroupListSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'ContactsSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
