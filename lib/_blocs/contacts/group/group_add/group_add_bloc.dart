import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'group_add_event.dart';
part 'group_add_state.dart';

class GroupAddBloc extends Bloc<GroupAddEvent, GroupAddState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  GroupAddBloc({this.contactRepository}) : super(GroupAddInitial());

  @override
  Stream<GroupAddState> mapEventToState(
    GroupAddEvent event,
  ) async* {
    if (event is GroupAddPressed) {
      yield GroupAddInProgress();
      try {
        String getData = await contactRepository.getCreateGroup(
            await environment.getToken(), event.eventData);
        yield GroupAddSuccess(stateData: getData);
      } catch (_) {
        yield GroupAddFailure(error: _.toString());
      }
    }
  }
}
