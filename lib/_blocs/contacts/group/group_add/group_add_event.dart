part of 'group_add_bloc.dart';

abstract class GroupAddEvent extends Equatable {
  const GroupAddEvent();

  @override
  List<Object> get props => [];
}

class GroupAddPressed extends GroupAddEvent {
  final Map eventData;
  GroupAddPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
