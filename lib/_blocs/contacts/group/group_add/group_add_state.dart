part of 'group_add_bloc.dart';

abstract class GroupAddState extends Equatable {
  const GroupAddState();

  @override
  List<Object> get props => [];
}

class GroupAddInitial extends GroupAddState {}

class GroupAddInProgress extends GroupAddState {}

class GroupAddSuccess extends GroupAddState {
  final String stateData;

  GroupAddSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class GroupAddFailure extends GroupAddState {
  final String error;
  const GroupAddFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
