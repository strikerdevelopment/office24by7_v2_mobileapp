import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'group_delete_event.dart';
part 'group_delete_state.dart';

class GroupDeleteBloc extends Bloc<GroupDeleteEvent, GroupDeleteState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  GroupDeleteBloc({this.contactRepository}) : super(GroupDeleteInitial());

  @override
  Stream<GroupDeleteState> mapEventToState(
    GroupDeleteEvent event,
  ) async* {
    if (event is GroupDeletePressed) {
      yield GroupDeleteInProgress();
      try {
        String getData = await contactRepository.getGroupDelete(
            await environment.getToken(), event.eventData);
        yield GroupDeleteSuccess(stateData: getData);
      } catch (_) {
        yield GroupDeleteFailure(error: _.toString());
      }
    }
  }
}
