part of 'group_delete_bloc.dart';

abstract class GroupDeleteEvent extends Equatable {
  const GroupDeleteEvent();

  @override
  List<Object> get props => [];
}

class GroupDeletePressed extends GroupDeleteEvent {
  final Map eventData;
  GroupDeletePressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
