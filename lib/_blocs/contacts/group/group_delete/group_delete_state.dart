part of 'group_delete_bloc.dart';

abstract class GroupDeleteState extends Equatable {
  const GroupDeleteState();

  @override
  List<Object> get props => [];
}

class GroupDeleteInitial extends GroupDeleteState {}

class GroupDeleteInProgress extends GroupDeleteState {}

class GroupDeleteSuccess extends GroupDeleteState {
  final String stateData;

  GroupDeleteSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class GroupDeleteFailure extends GroupDeleteState {
  final String error;
  const GroupDeleteFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
