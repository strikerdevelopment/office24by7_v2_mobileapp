import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'group_edit_event.dart';
part 'group_edit_state.dart';

class GroupEditBloc extends Bloc<GroupEditEvent, GroupEditState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  GroupEditBloc({this.contactRepository}) : super(GroupEditInitial());

  @override
  Stream<GroupEditState> mapEventToState(
    GroupEditEvent event,
  ) async* {
    if (event is GroupEditPressed) {
      yield GroupEditInProgress();
      try {
        String getData = await contactRepository.getEditGroup(
            await environment.getToken(), event.eventData);
        yield GroupEditSuccess(stateData: getData);
      } catch (_) {
        yield GroupEditFailure(error: _.toString());
      }
    }
  }
}
