part of 'group_edit_bloc.dart';

abstract class GroupEditEvent extends Equatable {
  const GroupEditEvent();

  @override
  List<Object> get props => [];
}

class GroupEditPressed extends GroupEditEvent {
  final Map eventData;
  GroupEditPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
