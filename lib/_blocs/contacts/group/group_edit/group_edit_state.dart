part of 'group_edit_bloc.dart';

abstract class GroupEditState extends Equatable {
  const GroupEditState();

  @override
  List<Object> get props => [];
}

class GroupEditInitial extends GroupEditState {}

class GroupEditInProgress extends GroupEditState {}

class GroupEditSuccess extends GroupEditState {
  final String stateData;

  GroupEditSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class GroupEditFailure extends GroupEditState {
  final String error;
  const GroupEditFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
