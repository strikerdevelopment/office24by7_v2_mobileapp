import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';

part 'group_global_search_event.dart';
part 'group_global_search_state.dart';

class GroupGlobalSearchBloc
    extends Bloc<GroupGlobalSearchEvent, GroupGlobalSearchState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  GroupGlobalSearchBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(GroupGlobalSearchInitial());

  @override
  Stream<GroupGlobalSearchState> mapEventToState(
    GroupGlobalSearchEvent event,
  ) async* {
    final currentState = state;
    if (event is GroupGlobalSearchFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is GroupGlobalSearchInitial) {
          final List<ContactGroupList> contacts =
              await contactRepository.getGroupGlobalSearch(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.keyword);
          yield GroupGlobalSearchSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is GroupGlobalSearchSuccess) {
          final List<ContactGroupList> contacts =
              await contactRepository.getGroupGlobalSearch(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.keyword,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : GroupGlobalSearchSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield GroupGlobalSearchFailure(error: _.toString());
      }
    }

    if (event is GroupGlobalSearchRefresh) {
      yield GroupGlobalSearchInitial();
      try {
        final List<ContactGroupList> contacts =
            await contactRepository.getGroupGlobalSearch(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.keyword);
        yield GroupGlobalSearchSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield GroupGlobalSearchFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(GroupGlobalSearchState state) =>
      state is GroupGlobalSearchSuccess && state.hasReachedMax;
}
