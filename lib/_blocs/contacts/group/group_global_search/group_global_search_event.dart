part of 'group_global_search_bloc.dart';

abstract class GroupGlobalSearchEvent extends Equatable {
  const GroupGlobalSearchEvent();

  @override
  List<Object> get props => [];
}

class GroupGlobalSearchFetched extends GroupGlobalSearchEvent {
  final String filterHeader;
  final String keyword;

  const GroupGlobalSearchFetched({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, keyword];
}

class GroupGlobalSearchRefresh extends GroupGlobalSearchEvent {
  final String filterHeader;
  final String keyword;
  const GroupGlobalSearchRefresh({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
