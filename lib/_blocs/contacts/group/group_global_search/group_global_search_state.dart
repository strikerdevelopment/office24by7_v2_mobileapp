part of 'group_global_search_bloc.dart';

abstract class GroupGlobalSearchState extends Equatable {
  const GroupGlobalSearchState();

  @override
  List<Object> get props => [];
}

class GroupGlobalSearchInitial extends GroupGlobalSearchState {}

class GroupGlobalSearchFailure extends GroupGlobalSearchState {
  final String error;

  GroupGlobalSearchFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GroupGlobalSearchFailure { error: $error }';
}

class GroupGlobalSearchSuccess extends GroupGlobalSearchState {
  final List<ContactGroupList> contacts;
  final bool hasReachedMax;
  final List<ContactGroupList> currentState;

  const GroupGlobalSearchSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  GroupGlobalSearchSuccess copyWith({
    List<ContactGroupList> contacts,
    bool hasReachedMax,
  }) {
    return GroupGlobalSearchSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'GroupGlobalSearchSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
