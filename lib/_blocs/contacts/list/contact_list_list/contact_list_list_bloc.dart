import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_list_list_event.dart';
part 'contact_list_list_state.dart';

class ContactListListBloc
    extends Bloc<ContactListListEvent, ContactListListState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactListListBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactListListInitial());

  @override
  Stream<ContactListListState> mapEventToState(
    ContactListListEvent event,
  ) async* {
    final currentState = state;
    if (event is ContactListListFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ContactListListInitial) {
          final List<ContactListList> contacts =
              await contactRepository.getContactListList(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.filterOption);
          yield ContactListListSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is ContactListListSuccess) {
          final List<ContactListList> contacts =
              await contactRepository.getContactListList(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.filterOption,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ContactListListSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield ContactListListFailure(error: _.toString());
      }
    }
    if (event is ContactListListRefresh) {
      yield ContactListListInitial();
      try {
        final List<ContactListList> contacts =
            await contactRepository.getContactListList(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.filterOption);
        yield ContactListListSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield ContactListListFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ContactListListState state) =>
      state is ContactListListSuccess && state.hasReachedMax;
}
