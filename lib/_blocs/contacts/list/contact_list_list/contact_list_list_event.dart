part of 'contact_list_list_bloc.dart';

abstract class ContactListListEvent extends Equatable {
  const ContactListListEvent();

  @override
  List<Object> get props => [];
}

class ContactListListFetched extends ContactListListEvent {
  final String filterHeader;
  final String filterOption;

  const ContactListListFetched({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, filterOption];
}

class ContactListListRefresh extends ContactListListEvent {
  final String filterHeader;
  final String filterOption;
  const ContactListListRefresh({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
