part of 'contact_list_list_bloc.dart';

abstract class ContactListListState extends Equatable {
  const ContactListListState();

  @override
  List<Object> get props => [];
}

class ContactListListInitial extends ContactListListState {}

class ContactListListFailure extends ContactListListState {
  final String error;

  ContactListListFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactListListFailure { error: $error }';
}

class ContactListListSuccess extends ContactListListState {
  final List<ContactListList> contacts;
  final bool hasReachedMax;
  final List<dynamic> currentState;

  const ContactListListSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  ContactListListSuccess copyWith({
    List<ContactListList> contacts,
    bool hasReachedMax,
  }) {
    return ContactListListSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'ContactsSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
