import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'list_delete_event.dart';
part 'list_delete_state.dart';

class ListDeleteBloc extends Bloc<ListDeleteEvent, ListDeleteState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ListDeleteBloc({this.contactRepository}) : super(ListDeleteInitial());

  @override
  Stream<ListDeleteState> mapEventToState(
    ListDeleteEvent event,
  ) async* {
    if (event is ListDeletePressed) {
      yield ListDeleteInProgress();
      try {
        String getData = await contactRepository.getListDelete(
            await environment.getToken(), event.eventData);
        yield ListDeleteSuccess(stateData: getData);
      } catch (_) {
        yield ListDeleteFailure(error: _.toString());
      }
    }
  }
}
