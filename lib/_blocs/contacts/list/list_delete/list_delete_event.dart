part of 'list_delete_bloc.dart';

abstract class ListDeleteEvent extends Equatable {
  const ListDeleteEvent();

  @override
  List<Object> get props => [];
}

class ListDeletePressed extends ListDeleteEvent {
  final Map eventData;
  ListDeletePressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
