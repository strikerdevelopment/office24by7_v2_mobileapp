part of 'list_delete_bloc.dart';

abstract class ListDeleteState extends Equatable {
  const ListDeleteState();

  @override
  List<Object> get props => [];
}

class ListDeleteInitial extends ListDeleteState {}

class ListDeleteInProgress extends ListDeleteState {}

class ListDeleteSuccess extends ListDeleteState {
  final String stateData;

  ListDeleteSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ListDeleteFailure extends ListDeleteState {
  final String error;
  const ListDeleteFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
