import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'list_edit_event.dart';
part 'list_edit_state.dart';

class ListEditBloc extends Bloc<ListEditEvent, ListEditState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();
  ListEditBloc({this.contactRepository}) : super(ListEditInitial());

  @override
  Stream<ListEditState> mapEventToState(
    ListEditEvent event,
  ) async* {
    if (event is ListEditPressed) {
      yield ListEditInProgress();
      try {
        String getData = await contactRepository.getListEdit(
            await environment.getToken(), event.eventData);
        yield ListEditSuccess(stateData: getData);
      } catch (_) {
        yield ListEditFailure(error: _.toString());
      }
    }
  }
}
