part of 'list_edit_bloc.dart';

abstract class ListEditEvent extends Equatable {
  const ListEditEvent();

  @override
  List<Object> get props => [];
}

class ListEditPressed extends ListEditEvent {
  final Map eventData;
  ListEditPressed({this.eventData});

  @override
  List<Object> get props => [eventData];
}
