part of 'list_edit_bloc.dart';

abstract class ListEditState extends Equatable {
  const ListEditState();

  @override
  List<Object> get props => [];
}

class ListEditInitial extends ListEditState {}

class ListEditInProgress extends ListEditState {}

class ListEditSuccess extends ListEditState {
  final String stateData;

  ListEditSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class ListEditFailure extends ListEditState {
  final String error;
  const ListEditFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
