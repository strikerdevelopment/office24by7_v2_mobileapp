import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'list_global_search_event.dart';
part 'list_global_search_state.dart';

class ListGlobalSearchBloc
    extends Bloc<ListGlobalSearchEvent, ListGlobalSearchState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ListGlobalSearchBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ListGlobalSearchInitial());

  @override
  Stream<ListGlobalSearchState> mapEventToState(
    ListGlobalSearchEvent event,
  ) async* {
    final currentState = state;
    if (event is ListGlobalSearchFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ListGlobalSearchInitial) {
          final List<ContactListList> contacts =
              await contactRepository.getListGlobalSearch(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.keyword);
          yield ListGlobalSearchSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is ListGlobalSearchSuccess) {
          final List<ContactListList> contacts =
              await contactRepository.getListGlobalSearch(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.keyword,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ListGlobalSearchSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield ListGlobalSearchFailure(error: _.toString());
      }
    }

    if (event is ListGlobalSearchRefresh) {
      yield ListGlobalSearchInitial();
      try {
        final List<ContactListList> contacts =
            await contactRepository.getListGlobalSearch(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.keyword);
        yield ListGlobalSearchSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield ListGlobalSearchFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ListGlobalSearchState state) =>
      state is ListGlobalSearchSuccess && state.hasReachedMax;
}
