part of 'list_global_search_bloc.dart';

abstract class ListGlobalSearchEvent extends Equatable {
  const ListGlobalSearchEvent();

  @override
  List<Object> get props => [];
}

class ListGlobalSearchFetched extends ListGlobalSearchEvent {
  final String filterHeader;
  final String keyword;

  const ListGlobalSearchFetched({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, keyword];
}

class ListGlobalSearchRefresh extends ListGlobalSearchEvent {
  final String filterHeader;
  final String keyword;
  const ListGlobalSearchRefresh({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
