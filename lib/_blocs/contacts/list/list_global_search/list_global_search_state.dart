part of 'list_global_search_bloc.dart';

abstract class ListGlobalSearchState extends Equatable {
  const ListGlobalSearchState();

  @override
  List<Object> get props => [];
}

class ListGlobalSearchInitial extends ListGlobalSearchState {}

class ListGlobalSearchFailure extends ListGlobalSearchState {
  final String error;

  ListGlobalSearchFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ListGlobalSearchFailure { error: $error }';
}

class ListGlobalSearchSuccess extends ListGlobalSearchState {
  final List<ContactListList> contacts;
  final bool hasReachedMax;
  final List<ContactListList> currentState;

  const ListGlobalSearchSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  ListGlobalSearchSuccess copyWith({
    List<ContactListList> contacts,
    bool hasReachedMax,
  }) {
    return ListGlobalSearchSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'ListGlobalSearchSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
