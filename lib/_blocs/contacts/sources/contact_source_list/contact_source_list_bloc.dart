import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'contact_source_list_event.dart';
part 'contact_source_list_state.dart';

class ContactSourceListBloc
    extends Bloc<ContactSourceListEvent, ContactSourceListState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  ContactSourceListBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(ContactSourceListInitial());

  @override
  Stream<ContactSourceListState> mapEventToState(
    ContactSourceListEvent event,
  ) async* {
    final currentState = state;
    if (event is ContactSourceListFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ContactSourceListInitial) {
          final List<ContactSourceList> contacts =
              await contactRepository.getContactSourceList(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.filterOption);
          yield ContactSourceListSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is ContactSourceListSuccess) {
          final List<ContactSourceList> contacts =
              await contactRepository.getContactSourceList(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.filterOption,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : ContactSourceListSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield ContactSourceListFailure(error: _.toString());
      }
    }
    if (event is ContactSourceListRefresh) {
      yield ContactSourceListInitial();
      try {
        final List<ContactSourceList> contacts =
            await contactRepository.getContactSourceList(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.filterOption);
        yield ContactSourceListSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield ContactSourceListFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(ContactSourceListState state) =>
      state is ContactSourceListSuccess && state.hasReachedMax;
}
