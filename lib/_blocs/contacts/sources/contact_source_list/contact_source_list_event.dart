part of 'contact_source_list_bloc.dart';

abstract class ContactSourceListEvent extends Equatable {
  const ContactSourceListEvent();

  @override
  List<Object> get props => [];
}

class ContactSourceListFetched extends ContactSourceListEvent {
  final String filterHeader;
  final String filterOption;

  const ContactSourceListFetched({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, filterOption];
}

class ContactSourceListRefresh extends ContactSourceListEvent {
  final String filterHeader;
  final String filterOption;
  const ContactSourceListRefresh({this.filterHeader, this.filterOption})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
