part of 'contact_source_list_bloc.dart';

abstract class ContactSourceListState extends Equatable {
  const ContactSourceListState();

  @override
  List<Object> get props => [];
}

class ContactSourceListInitial extends ContactSourceListState {}

class ContactSourceListFailure extends ContactSourceListState {
  final String error;

  ContactSourceListFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactSourceListFailure { error: $error }';
}

class ContactSourceListSuccess extends ContactSourceListState {
  final List<ContactSourceList> contacts;
  final bool hasReachedMax;
  final List<dynamic> currentState;

  const ContactSourceListSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  ContactSourceListSuccess copyWith({
    List<ContactSourceList> contacts,
    bool hasReachedMax,
  }) {
    return ContactSourceListSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'ContactsSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
