import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'source_global_search_event.dart';
part 'source_global_search_state.dart';

class SourceGlobalSearchBloc
    extends Bloc<SourceGlobalSearchEvent, SourceGlobalSearchState> {
  final ContactRepository contactRepository;
  final Environment environment = Environment();

  SourceGlobalSearchBloc({this.contactRepository})
      : assert(contactRepository != null),
        super(SourceGlobalSearchInitial());

  @override
  Stream<SourceGlobalSearchState> mapEventToState(
    SourceGlobalSearchEvent event,
  ) async* {
    final currentState = state;
    if (event is SourceGlobalSearchFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is SourceGlobalSearchInitial) {
          final List<ContactSourceList> contacts =
              await contactRepository.getSourceGlobalSearch(
                  0,
                  Environment.dataDisplayCount,
                  event.filterHeader,
                  await environment.getToken(),
                  event.keyword);
          yield SourceGlobalSearchSuccess(
              contacts: contacts, hasReachedMax: false, currentState: contacts);
          return;
        }
        if (currentState is SourceGlobalSearchSuccess) {
          final List<ContactSourceList> contacts =
              await contactRepository.getSourceGlobalSearch(
            currentState.contacts.length,
            Environment.dataDisplayCount,
            event.filterHeader,
            await environment.getToken(),
            event.keyword,
          );
          yield contacts.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : SourceGlobalSearchSuccess(
                  contacts: currentState.contacts + contacts,
                  hasReachedMax: false,
                  currentState: contacts,
                );
        }
      } catch (_) {
        yield SourceGlobalSearchFailure(error: _.toString());
      }
    }

    if (event is SourceGlobalSearchRefresh) {
      yield SourceGlobalSearchInitial();
      try {
        final List<ContactSourceList> contacts =
            await contactRepository.getSourceGlobalSearch(
                0,
                Environment.dataDisplayCount,
                event.filterHeader,
                await environment.getToken(),
                event.keyword);
        yield SourceGlobalSearchSuccess(
            contacts: contacts, hasReachedMax: false, currentState: contacts);
      } catch (_) {
        yield SourceGlobalSearchFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(SourceGlobalSearchState state) =>
      state is SourceGlobalSearchSuccess && state.hasReachedMax;
}
