part of 'source_global_search_bloc.dart';

abstract class SourceGlobalSearchEvent extends Equatable {
  const SourceGlobalSearchEvent();

  @override
  List<Object> get props => [];
}

class SourceGlobalSearchFetched extends SourceGlobalSearchEvent {
  final String filterHeader;
  final String keyword;

  const SourceGlobalSearchFetched({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader, keyword];
}

class SourceGlobalSearchRefresh extends SourceGlobalSearchEvent {
  final String filterHeader;
  final String keyword;
  const SourceGlobalSearchRefresh({this.filterHeader, this.keyword})
      : assert(filterHeader != null);

  List<Object> get props => [filterHeader];
}
