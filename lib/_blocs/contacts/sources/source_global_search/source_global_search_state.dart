part of 'source_global_search_bloc.dart';

abstract class SourceGlobalSearchState extends Equatable {
  const SourceGlobalSearchState();

  @override
  List<Object> get props => [];
}

class SourceGlobalSearchInitial extends SourceGlobalSearchState {}

class SourceGlobalSearchFailure extends SourceGlobalSearchState {
  final String error;

  SourceGlobalSearchFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SourceGlobalSearchFailure { error: $error }';
}

class SourceGlobalSearchSuccess extends SourceGlobalSearchState {
  final List<ContactSourceList> contacts;
  final bool hasReachedMax;
  final List<ContactSourceList> currentState;

  const SourceGlobalSearchSuccess(
      {this.contacts, this.hasReachedMax, this.currentState});

  SourceGlobalSearchSuccess copyWith({
    List<ContactSourceList> contacts,
    bool hasReachedMax,
  }) {
    return SourceGlobalSearchSuccess(
        contacts: contacts ?? this.contacts,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [contacts, hasReachedMax, currentState];

  @override
  String toString() =>
      'SourceGlobalSearchSuccess { items: ${contacts.length}, hasReachedMax: $hasReachedMax  }';
}
