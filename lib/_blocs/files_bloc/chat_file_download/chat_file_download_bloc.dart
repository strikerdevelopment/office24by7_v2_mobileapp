import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';

part 'chat_file_download_event.dart';
part 'chat_file_download_state.dart';

class ChatFileDownloadBloc
    extends Bloc<ChatFileDownloadEvent, ChatFileDownloadState> {
  final FileDownloadRepository fileDownloadRepository;

  ChatFileDownloadBloc({this.fileDownloadRepository})
      : assert(fileDownloadRepository != null),
        super(ChatFileDownloadInitial());

  @override
  Stream<ChatFileDownloadState> mapEventToState(
    ChatFileDownloadEvent event,
  ) async* {
    if (event is ChatFileDownloadExistPressed) {
      var dir = Platform.isIOS
          ? await getApplicationDocumentsDirectory()
          : await getExternalStorageDirectory();

      bool isFileExist =
          File("${dir.path}/${event.file.fileName}").existsSync();

      yield ChatFileDownloadIsExist(isExist: isFileExist);
    }

    if (event is ChatFileDownloadPressed) {
      yield ChatFileDownloadInProgress();
      try {
        FileDownload getData =
            await fileDownloadRepository.getFileDownloads(event.file);
        yield ChatFileDownloadSuccess(stateData: getData);
      } catch (_) {
        yield ChatFileDownloadFailure(error: _.toString());
      }
    }

    if (event is ChatFileDownloadSharedPressed) {
      var dir = Platform.isIOS
          ? await getApplicationDocumentsDirectory()
          : await getExternalStorageDirectory();
      Share.shareFiles(['${dir.path}/${event.file.fileName}']);
    }
  }
}
