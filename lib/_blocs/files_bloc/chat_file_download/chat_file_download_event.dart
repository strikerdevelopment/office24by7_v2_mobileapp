part of 'chat_file_download_bloc.dart';

abstract class ChatFileDownloadEvent extends Equatable {
  const ChatFileDownloadEvent();

  @override
  List<Object> get props => [];
}

class ChatFileDownloadPressed extends ChatFileDownloadEvent {
  final ChatFileInfo file;

  const ChatFileDownloadPressed({this.file});

  @override
  List<Object> get props => [file.fileName];
}

class ChatFileDownloadExistPressed extends ChatFileDownloadEvent {
  final ChatFileInfo file;

  const ChatFileDownloadExistPressed({this.file});

  @override
  List<Object> get props => [file.fileName];
}

class ChatFileDownloadSharedPressed extends ChatFileDownloadEvent {
  final ChatFileInfo file;

  const ChatFileDownloadSharedPressed({this.file});

  @override
  List<Object> get props => [file.fileName];
}
