part of 'chat_file_download_bloc.dart';

abstract class ChatFileDownloadState extends Equatable {
  const ChatFileDownloadState();

  @override
  List<Object> get props => [];
}

class ChatFileDownloadInitial extends ChatFileDownloadState {}

class ChatFileDownloadInProgress extends ChatFileDownloadState {}

class ChatFileDownloadIsExist extends ChatFileDownloadState {
  final bool isExist;
  ChatFileDownloadIsExist({this.isExist});
  @override
  List<Object> get props => [isExist];

  @override
  String toString() => 'ChatFileDownloadSuccess { stateData: $isExist }';
}

class ChatFileDownloadSuccess extends ChatFileDownloadState {
  final FileDownload stateData;

  ChatFileDownloadSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ChatFileDownloadSuccess { stateData: $stateData }';
}

class ChatFileDownloadFailure extends ChatFileDownloadState {
  final String error;
  const ChatFileDownloadFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatFileDownloadFailure { error: $error }';
}
