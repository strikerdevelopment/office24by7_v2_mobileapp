import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/global_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'city_event.dart';
part 'city_state.dart';

class CityBloc extends Bloc<CityEvent, CityState> {
  final GlobalRepository globalRepository;
  final Environment environment = Environment();

  CityBloc({this.globalRepository})
      : assert(globalRepository != null),
        super(CityInitial());

  @override
  Stream<CityState> mapEventToState(
    CityEvent event,
  ) async* {
    if (event is CityPressed) {
      yield CityInitial();
      try {
        final List<City> citysData = await globalRepository.getCityData(
            await environment.getToken(), event.state);
        yield CitySuccess(cityData: citysData);
      } catch (_) {
        yield CityFailure();
      }
    }
  }
}
