part of 'city_bloc.dart';

abstract class CityState extends Equatable {
  const CityState();

  @override
  List<Object> get props => [];
}

class CityInitial extends CityState {}

class CitySuccess extends CityState {
  final List<City> cityData;

  CitySuccess({this.cityData});

  @override
  String toString() => 'CitySuccess : {cityData : ${cityData.length}}';
}

class CityFailure extends CityState {}
