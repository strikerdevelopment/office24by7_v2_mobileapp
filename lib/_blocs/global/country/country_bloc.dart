import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/global_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'country_event.dart';
part 'country_state.dart';

class CountryBloc extends Bloc<CountryEvent, CountryState> {
  final GlobalRepository globalRepository;
  final Environment environment = Environment();
  CountryBloc({this.globalRepository})
      : assert(globalRepository != null),
        super(CountryInitial());

  @override
  Stream<CountryState> mapEventToState(
    CountryEvent event,
  ) async* {
    if (event is CountryPressed) {
      yield CountryInitial();
      try {
        final List<Country> countryData =
            await globalRepository.getCountryData(await environment.getToken());
        yield CountrySuccess(countryData: countryData);
      } catch (_) {
        yield CountryFailure();
      }
    }
  }
}
