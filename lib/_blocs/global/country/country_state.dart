part of 'country_bloc.dart';

abstract class CountryState extends Equatable {
  const CountryState();

  @override
  List<Object> get props => [];
}

class CountryInitial extends CountryState {}

class CountrySuccess extends CountryState {
  final List<Country> countryData;

  CountrySuccess({this.countryData});

  @override
  String toString() => 'CountrySuccess: {countryData: ${countryData.length}}';
}

class CountryFailure extends CountryState {}
