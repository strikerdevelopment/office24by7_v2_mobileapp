import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'country_flag_event.dart';
part 'country_flag_state.dart';

class CountryFlagBloc extends Bloc<CountryFlagEvent, CountryFlagState> {
  final GlobalRepository globalRepository;
  final Environment environment = Environment();

  CountryFlagBloc({this.globalRepository}) : super(CountryFlagInitial());

  @override
  Stream<CountryFlagState> mapEventToState(
    CountryFlagEvent event,
  ) async* {
    if (event is CountryFlagPressed) {
      yield CountryFlagInitial();
      try {
        List<String> getData = await globalRepository
            .getCountryFlags(await environment.getToken());
        yield CountryFlagSuccess(stateData: getData);
      } catch (_) {
        yield CountryFlagFailure(error: _.toString());
      }
    }
  }
}
