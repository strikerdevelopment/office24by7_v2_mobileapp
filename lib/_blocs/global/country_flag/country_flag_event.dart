part of 'country_flag_bloc.dart';

abstract class CountryFlagEvent extends Equatable {
  const CountryFlagEvent();

  @override
  List<Object> get props => [];
}

class CountryFlagPressed extends CountryFlagEvent {}
