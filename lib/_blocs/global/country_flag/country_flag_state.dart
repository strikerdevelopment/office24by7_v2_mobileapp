part of 'country_flag_bloc.dart';

abstract class CountryFlagState extends Equatable {
  const CountryFlagState();

  @override
  List<Object> get props => [];
}

class CountryFlagInitial extends CountryFlagState {}

class CountryFlagSuccess extends CountryFlagState {
  final List<String> stateData;

  CountryFlagSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'CountryFlagSuccess { stateData: ${stateData.length} }';
}

class CountryFlagFailure extends CountryFlagState {
  final String error;
  const CountryFlagFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'CountryFlagFailure { error: $error }';
}
