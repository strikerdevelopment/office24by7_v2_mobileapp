import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'custom_dependent_dropdown_event.dart';
part 'custom_dependent_dropdown_state.dart';

class CustomDependentDropdownBloc
    extends Bloc<CustomDependentDropdownEvent, CustomDependentDropdownState> {
  final GenericRepository genericRepository;
  final Environment environment = Environment();

  CustomDependentDropdownBloc({this.genericRepository})
      : super(CustomDependentDropdownInitial());

  @override
  Stream<CustomDependentDropdownState> mapEventToState(
    CustomDependentDropdownEvent event,
  ) async* {
    if (event is CustomDependentDropdownPrentPressed) {
      yield CustomDependentDropdownInitial();
      try {
        List<DependencyFieldsModel> getData = await genericRepository
            .getGenericAPIdata(await environment.getToken(), event.formData);
        yield CustomDependentDropdownSuccess(stateData: getData);
      } catch (_) {
        yield CustomDependentDropdownFailure(error: _.toString());
      }
    }

    if (event is CustomDependentDropdownPressed) {
      yield CustomDependentDropdownInitial();
      try {
        List<DependencyFieldsModel> getData = await genericRepository
            .getGenericAPIdata(await environment.getToken(), event.formData);
        yield CustomDependentDropdownSuccess(stateData: getData);
      } catch (_) {
        yield CustomDependentDropdownFailure(error: _.toString());
      }
    }
  }
}
