part of 'custom_dependent_dropdown_bloc.dart';

abstract class CustomDependentDropdownEvent extends Equatable {
  const CustomDependentDropdownEvent();

  @override
  List<Object> get props => [];
}

class CustomDependentDropdownPrentPressed extends CustomDependentDropdownEvent {
  final GenericModel formData;
  CustomDependentDropdownPrentPressed({this.formData});

  @override
  List<Object> get props => [];

  String toString() =>
      'CustomFormFieldsPressed : {form fields : ${formData.fieldid}}';
}

class CustomDependentDropdownPressed extends CustomDependentDropdownEvent {
  final GenericModel formData;

  CustomDependentDropdownPressed({this.formData});

  @override
  List<Object> get props => [];

  String toString() =>
      'CustomFormFieldsPressed : {form fields : ${formData.fieldid}}';
}
