part of 'custom_dependent_dropdown_bloc.dart';

abstract class CustomDependentDropdownState extends Equatable {
  const CustomDependentDropdownState();

  @override
  List<Object> get props => [];
}

class CustomDependentDropdownInitial extends CustomDependentDropdownState {}

class CustomDependentDropdownSuccess extends CustomDependentDropdownState {
  final List<DependencyFieldsModel> stateData;

  CustomDependentDropdownSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'CustomDependentDropdownSuccess { stateData: $stateData }';
}

class CustomDependentDropdownFailure extends CustomDependentDropdownState {
  final String error;
  const CustomDependentDropdownFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'CustomDependentDropdownFailure { error: $error }';
}
