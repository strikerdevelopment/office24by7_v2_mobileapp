import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/global_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'state_event.dart';
part 'state_state.dart';

class StateBloc extends Bloc<StateEvent, StateState> {
  final GlobalRepository globalRepository;
  final Environment environment = Environment();

  StateBloc({this.globalRepository})
      : assert(globalRepository != null),
        super(StateInitial());

  @override
  Stream<StateState> mapEventToState(
    StateEvent event,
  ) async* {
    if (event is StatePressed) {
      yield StateInitial();
      try {
        final List<StateModel> statesData = await globalRepository.getStateData(
            await environment.getToken(), event.country);
        yield StateSuccess(statesData: statesData);
      } catch (_) {
        yield StateFailure();
      }
    }
  }
}
