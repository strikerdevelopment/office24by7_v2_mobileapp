part of 'state_bloc.dart';

abstract class StateEvent extends Equatable {
  const StateEvent();

  @override
  List<Object> get props => [];
}

class StatePressed extends StateEvent {
  final String country;

  StatePressed({this.country});

  @override
  List<Object> get props => [country];
}
