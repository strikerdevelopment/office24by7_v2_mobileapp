part of 'state_bloc.dart';

abstract class StateState extends Equatable {
  const StateState();

  @override
  List<Object> get props => [];
}

class StateInitial extends StateState {}

class StateSuccess extends StateState {
  final List<StateModel> statesData;

  StateSuccess({this.statesData});

  @override
  String toString() => 'StateSuccess : {statesData : ${statesData.length}}';
}

class StateFailure extends StateState {}
