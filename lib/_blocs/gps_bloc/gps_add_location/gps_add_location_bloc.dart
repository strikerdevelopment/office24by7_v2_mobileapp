import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'gps_add_location_event.dart';
part 'gps_add_location_state.dart';

class GpsAddLocationBloc
    extends Bloc<GpsAddLocationEvent, GpsAddLocationState> {
  final GpsRepository gpsRepository;
  final Environment environment = Environment();

  GpsAddLocationBloc({this.gpsRepository}) : super(GpsAddLocationInitial());

  @override
  Stream<GpsAddLocationState> mapEventToState(
    GpsAddLocationEvent event,
  ) async* {
    if (event is GpsAddLocationPressed) {
      yield GpsAddLocationInitial();
      try {
        String getData = await gpsRepository.getGpsAddLocation(
            await environment.getToken(),
            event.latitude,
            event.longitude,
            event.eventType,
            event.deviceId,
            event.phoneNumber);
        yield GpsAddLocationSuccess(stateData: getData);
      } catch (_) {
        yield GpsAddLocationFailure(error: _.toString());
      }
    }
  }
}
