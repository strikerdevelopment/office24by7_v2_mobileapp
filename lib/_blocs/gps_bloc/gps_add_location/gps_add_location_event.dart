part of 'gps_add_location_bloc.dart';

abstract class GpsAddLocationEvent extends Equatable {
  const GpsAddLocationEvent();

  @override
  List<Object> get props => [];
}

class GpsAddLocationPressed extends GpsAddLocationEvent {
  final String latitude;
  final String longitude;
  final String eventType;
  final String deviceId;
  final String phoneNumber;

  const GpsAddLocationPressed(
      {this.latitude,
      this.longitude,
      this.eventType,
      this.deviceId,
      this.phoneNumber});

  @override
  List<Object> get props => [latitude, longitude];
}
