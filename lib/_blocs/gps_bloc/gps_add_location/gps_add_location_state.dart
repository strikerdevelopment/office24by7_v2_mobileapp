part of 'gps_add_location_bloc.dart';

abstract class GpsAddLocationState extends Equatable {
  const GpsAddLocationState();

  @override
  List<Object> get props => [];
}

class GpsAddLocationInitial extends GpsAddLocationState {}

class GpsAddLocationInProgress extends GpsAddLocationState {}

class GpsAddLocationSuccess extends GpsAddLocationState {
  final String stateData;

  GpsAddLocationSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'GpsAddLocationSuccess { stateData: $stateData }';
}

class GpsAddLocationFailure extends GpsAddLocationState {
  final String error;
  const GpsAddLocationFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GpsAddLocationFailure { error: $error }';
}
