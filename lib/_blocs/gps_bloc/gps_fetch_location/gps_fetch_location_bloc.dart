import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:office24by7_v2/_data/repositories/gps_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'gps_fetch_location_event.dart';
part 'gps_fetch_location_state.dart';

class GpsFetchLocationBloc
    extends Bloc<GpsFetchLocationEvent, GpsFetchLocationState> {
  final GpsRepository gpsRepository;
  final Environment environment = Environment();

  GpsFetchLocationBloc({this.gpsRepository}) : super(GpsFetchLocationInitial());

  @override
  Stream<GpsFetchLocationState> mapEventToState(
    GpsFetchLocationEvent event,
  ) async* {
    if (event is GpsFetchLocationPressed) {
      yield GpsFetchLocationInitial();
      try {
        Position getData = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
        yield GpsFetchLocationSuccess(stateData: getData);
      } catch (_) {
        yield GpsFetchLocationFailure(error: _.toString());
      }
    }

    if (event is GpsFetchLocationRefreshed) {
      try {
        Position getData = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
        yield GpsFetchLocationSuccess(stateData: getData);
      } catch (_) {
        yield GpsFetchLocationFailure(error: _.toString());
      }
    }
  }
}
