part of 'gps_fetch_location_bloc.dart';

abstract class GpsFetchLocationEvent extends Equatable {
  const GpsFetchLocationEvent();

  @override
  List<Object> get props => [];
}

class GpsFetchLocationPressed extends GpsFetchLocationEvent {
  GpsFetchLocationPressed();

  @override
  List<Object> get props => [];
}

class GpsFetchLocationRefreshed extends GpsFetchLocationEvent {
  GpsFetchLocationRefreshed();

  @override
  List<Object> get props => [];
}
