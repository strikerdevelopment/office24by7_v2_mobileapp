part of 'gps_fetch_location_bloc.dart';

abstract class GpsFetchLocationState extends Equatable {
  const GpsFetchLocationState();

  @override
  List<Object> get props => [];
}

class GpsFetchLocationInitial extends GpsFetchLocationState {}

class GpsFetchLocationInProgress extends GpsFetchLocationState {}

class GpsFetchLocationSuccess extends GpsFetchLocationState {
  final Position stateData;

  GpsFetchLocationSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'GpsFetchLocationSuccess { stateData: $stateData }';
}

class GpsFetchLocationFailure extends GpsFetchLocationState {
  final String error;
  const GpsFetchLocationFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GpsFetchLocationFailure { error: $error }';
}
