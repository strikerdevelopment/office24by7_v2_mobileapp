import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/gps_model/gps_hierarchy_users_model.dart';

part 'gps_hierarchy_users_event.dart';
part 'gps_hierarchy_users_state.dart';

class GpsHierarchyUsersBloc
    extends Bloc<GpsHierarchyUsersEvent, GpsHierarchyUsersState> {
  final GpsRepository gpsRepository;
  final Environment environment = Environment();

  GpsHierarchyUsersBloc({this.gpsRepository})
      : super(GpsHierarchyUsersInitial());

  @override
  Stream<GpsHierarchyUsersState> mapEventToState(
    GpsHierarchyUsersEvent event,
  ) async* {
    if (event is GpsHierarchyUsersPressed) {
      yield GpsHierarchyUsersInitial();
      try {
        List<GpsHierarchyUsersModel> getData = await gpsRepository
            .getGpsHierarchyUsers(await environment.getToken());
        yield GpsHierarchyUsersSuccess(stateData: getData);
      } catch (_) {
        yield GpsHierarchyUsersFailure(error: _.toString());
      }
    }
  }
}
