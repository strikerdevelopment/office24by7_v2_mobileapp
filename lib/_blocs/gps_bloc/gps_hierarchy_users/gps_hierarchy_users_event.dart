part of 'gps_hierarchy_users_bloc.dart';

abstract class GpsHierarchyUsersEvent extends Equatable {
  const GpsHierarchyUsersEvent();

  @override
  List<Object> get props => [];
}

class GpsHierarchyUsersPressed extends GpsHierarchyUsersEvent {
  GpsHierarchyUsersPressed();

  @override
  List<Object> get props => [];
}
