part of 'gps_hierarchy_users_bloc.dart';

abstract class GpsHierarchyUsersState extends Equatable {
  const GpsHierarchyUsersState();

  @override
  List<Object> get props => [];
}

class GpsHierarchyUsersInitial extends GpsHierarchyUsersState {}

class GpsHierarchyUsersSuccess extends GpsHierarchyUsersState {
  final List<GpsHierarchyUsersModel> stateData;

  GpsHierarchyUsersSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'GpsHierarchyUsersSuccess { stateData: $stateData }';
}

class GpsHierarchyUsersFailure extends GpsHierarchyUsersState {
  final String error;
  const GpsHierarchyUsersFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GpsHierarchyUsersFailure { error: $error }';
}
