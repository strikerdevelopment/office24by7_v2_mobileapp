import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/gps_model/gps_location_history_model.dart';

part 'gps_location_history_event.dart';
part 'gps_location_history_state.dart';

class GpsLocationHistoryBloc
    extends Bloc<GpsLocationHistoryEvent, GpsLocationHistoryState> {
  final GpsRepository gpsRepository;
  final Environment environment = Environment();

  GpsLocationHistoryBloc({this.gpsRepository})
      : super(GpsLocationHistoryInitial());

  @override
  Stream<GpsLocationHistoryState> mapEventToState(
    GpsLocationHistoryEvent event,
  ) async* {
    if (event is GpsLocationHistoryPressed) {
      yield GpsLocationHistoryInitial();
      try {
        List<GpsLocationHistoryModel> getData =
            await gpsRepository.getGpsLocationHistory(
                await environment.getToken(),
                event.users,
                event.fromDate,
                event.toDate,
                event.eventType);
        yield GpsLocationHistorySuccess(stateData: getData);
      } catch (_) {
        yield GpsLocationHistoryFailure(error: _.toString());
      }
    }
  }
}
