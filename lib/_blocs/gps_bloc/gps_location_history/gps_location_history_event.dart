part of 'gps_location_history_bloc.dart';

abstract class GpsLocationHistoryEvent extends Equatable {
  const GpsLocationHistoryEvent();

  @override
  List<Object> get props => [];
}

class GpsLocationHistoryPressed extends GpsLocationHistoryEvent {
  final String users;
  final String fromDate;
  final String toDate;
  final String eventType;

  GpsLocationHistoryPressed(
      {this.users, this.fromDate, this.toDate, this.eventType});

  @override
  List<Object> get props => [users, fromDate, toDate];
}
