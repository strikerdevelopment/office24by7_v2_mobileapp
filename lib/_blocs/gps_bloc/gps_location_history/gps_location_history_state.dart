part of 'gps_location_history_bloc.dart';

abstract class GpsLocationHistoryState extends Equatable {
  const GpsLocationHistoryState();

  @override
  List<Object> get props => [];
}

class GpsLocationHistoryInitial extends GpsLocationHistoryState {}

class GpsLocationHistorySuccess extends GpsLocationHistoryState {
  final List<GpsLocationHistoryModel> stateData;

  GpsLocationHistorySuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'GpsLocationHistorySuccess { stateData: $stateData }';
}

class GpsLocationHistoryFailure extends GpsLocationHistoryState {
  final String error;
  const GpsLocationHistoryFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GpsLocationHistoryFailure { error: $error }';
}
