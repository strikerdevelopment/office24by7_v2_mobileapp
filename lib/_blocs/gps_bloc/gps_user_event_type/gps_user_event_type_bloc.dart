import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'gps_user_event_type_event.dart';
part 'gps_user_event_type_state.dart';

class GpsUserEventTypeBloc
    extends Bloc<GpsUserEventTypeEvent, GpsUserEventTypeState> {
  final GpsRepository gpsRepository;
  final Environment environment = Environment();

  GpsUserEventTypeBloc({this.gpsRepository}) : super(GpsUserEventTypeInitial());

  @override
  Stream<GpsUserEventTypeState> mapEventToState(
    GpsUserEventTypeEvent event,
  ) async* {
    if (event is GpsUserEventTypePressed) {
      yield GpsUserEventTypeInitial();
      try {
        List<GpsUserEventTypeModel> getData = await gpsRepository
            .getGpsUserEventType(await environment.getToken());
        yield GpsUserEventTypeSuccess(stateData: getData);
      } catch (_) {
        yield GpsUserEventTypeFailure(error: _.toString());
      }
    }
  }
}
