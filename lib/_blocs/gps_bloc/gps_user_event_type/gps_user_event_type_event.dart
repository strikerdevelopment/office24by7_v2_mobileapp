part of 'gps_user_event_type_bloc.dart';

abstract class GpsUserEventTypeEvent extends Equatable {
  const GpsUserEventTypeEvent();

  @override
  List<Object> get props => [];
}

class GpsUserEventTypePressed extends GpsUserEventTypeEvent {
  GpsUserEventTypePressed();

  @override
  List<Object> get props => [];
}
