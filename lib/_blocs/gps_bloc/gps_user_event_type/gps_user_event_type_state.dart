part of 'gps_user_event_type_bloc.dart';

abstract class GpsUserEventTypeState extends Equatable {
  const GpsUserEventTypeState();

  @override
  List<Object> get props => [];
}

class GpsUserEventTypeInitial extends GpsUserEventTypeState {}

class GpsUserEventTypeSuccess extends GpsUserEventTypeState {
  final List<GpsUserEventTypeModel> stateData;

  GpsUserEventTypeSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'GpsUserEventTypeSuccess { stateData: $stateData }';
}

class GpsUserEventTypeFailure extends GpsUserEventTypeState {
  final String error;
  const GpsUserEventTypeFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GpsUserEventTypeFailure { error: $error }';
}
