import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/gps_model/gps_model.dart';

part 'gps_user_location_event.dart';
part 'gps_user_location_state.dart';

class GpsUserLocationBloc
    extends Bloc<GpsUserLocationEvent, GpsUserLocationState> {
  final GpsRepository gpsRepository;
  final Environment environment = Environment();

  GpsUserLocationBloc({this.gpsRepository}) : super(GpsUserLocationInitial());

  @override
  Stream<GpsUserLocationState> mapEventToState(
    GpsUserLocationEvent event,
  ) async* {
    if (event is GpsUserLocationPressed) {
      yield GpsUserLocationInitial();
      try {
        List<GpsUserLocationModel> getData =
            await gpsRepository.getGpsUserLocation(await environment.getToken(),
                event.users, event.fromDate, event.toDate);
        yield GpsUserLocationSuccess(stateData: getData);
      } catch (_) {
        yield GpsUserLocationFailure(error: _.toString());
      }
    }
  }
}
