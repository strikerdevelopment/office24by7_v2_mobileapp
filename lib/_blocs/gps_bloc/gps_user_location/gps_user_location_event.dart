part of 'gps_user_location_bloc.dart';

abstract class GpsUserLocationEvent extends Equatable {
  const GpsUserLocationEvent();

  @override
  List<Object> get props => [];
}

class GpsUserLocationPressed extends GpsUserLocationEvent {
  final String users;
  final String fromDate;
  final String toDate;

  GpsUserLocationPressed({this.users, this.fromDate, this.toDate});

  @override
  List<Object> get props => [users, fromDate, toDate];
}
