part of 'gps_user_location_bloc.dart';

abstract class GpsUserLocationState extends Equatable {
  const GpsUserLocationState();

  @override
  List<Object> get props => [];
}

class GpsUserLocationInitial extends GpsUserLocationState {}

class GpsUserLocationSuccess extends GpsUserLocationState {
  final List<GpsUserLocationModel> stateData;

  GpsUserLocationSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'GpsUserLocationSuccess { stateData: $stateData }';
}

class GpsUserLocationFailure extends GpsUserLocationState {
  final String error;
  const GpsUserLocationFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GpsUserLocationFailure { error: $error }';
}
