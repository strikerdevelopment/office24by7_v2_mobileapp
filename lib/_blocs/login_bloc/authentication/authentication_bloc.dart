import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/login/login_info.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final LoginRepository loginRepository;
  final FirebaseRepository firebaseRepository;
  final Environment environment = Environment();

  AuthenticationBloc(
      {@required this.loginRepository, @required this.firebaseRepository})
      : assert(loginRepository != null),
        super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticationStarted) {
      final bool hasToken = await loginRepository.hasToken();
      if (hasToken) {
        await firebaseRepository.getAddUserCollection(
            await environment.getUserId(),
            await environment.getToken(),
            await environment.fcmToken());
        yield AuthenticationSuccess();
      } else {
        yield AuthenticationFailure();
      }
    }

    if (event is AuthenticationLoggedIn) {
      yield AuthenticationInProgress();
      await loginRepository.persistToken(event.loginInfo);
      yield AuthenticationSuccess();
    }

    if (event is AuthenticationLoggedOut) {
      yield AuthenticationInProgress();
      await loginRepository.deleteToken();
      yield AuthenticationFailure();
    }
  }
}
