part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AuthenticationStarted extends AuthenticationEvent {}

class AuthenticationLoggedIn extends AuthenticationEvent {
  final LoginInfo loginInfo;

  const AuthenticationLoggedIn({@required this.loginInfo});

  @override
  List<Object> get props => [loginInfo];

  @override
  String toString() => 'AuthenticationLoggedIn { loginInfo: $loginInfo }';
}

class AuthenticationLoggedOut extends AuthenticationEvent {}
