import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'force_logout_event.dart';
part 'force_logout_state.dart';

class ForceLogoutBloc extends Bloc<ForceLogoutEvent, ForceLogoutState> {
  final LoginRepository loginRepository;
  final Environment environment = Environment();

  ForceLogoutBloc({this.loginRepository}) : super(ForceLogoutInitial());

  @override
  Stream<ForceLogoutState> mapEventToState(
    ForceLogoutEvent event,
  ) async* {
    if (event is ForceLogoutPressed) {
      yield ForceLogoutInProgress();
      try {
        dynamic getData = await loginRepository.getForceLogout(event.loginId);
        yield ForceLogoutSuccess(stateData: getData);
      } catch (_) {
        yield ForceLogoutFailure(error: _.toString());
      }
    }
  }
}
