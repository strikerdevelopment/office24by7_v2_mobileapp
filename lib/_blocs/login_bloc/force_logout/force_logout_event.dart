part of 'force_logout_bloc.dart';

abstract class ForceLogoutEvent extends Equatable {
  const ForceLogoutEvent();

  @override
  List<Object> get props => [];
}

class ForceLogoutPressed extends ForceLogoutEvent {
  final String loginId;
  ForceLogoutPressed({this.loginId});
}
