part of 'force_logout_bloc.dart';

abstract class ForceLogoutState extends Equatable {
  const ForceLogoutState();

  @override
  List<Object> get props => [];
}

class ForceLogoutInitial extends ForceLogoutState {}

class ForceLogoutInProgress extends ForceLogoutState {}

class ForceLogoutSuccess extends ForceLogoutState {
  final dynamic stateData;

  ForceLogoutSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ContactEmailIdSuccess { stateData: $stateData }';
}

class ForceLogoutFailure extends ForceLogoutState {
  final String error;
  const ForceLogoutFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
