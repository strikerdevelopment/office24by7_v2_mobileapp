import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/login/login.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginRepository loginRepository;

  LoginBloc({this.loginRepository})
      : assert(loginRepository != null),
        super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginPressed) {
      yield LoginLoadInProgress();
      try {
        final OtpInfo otpInfo =
            await loginRepository.getLoginData(event.userID);
        yield LoginLoadSuccess(otpInfo: otpInfo);
        yield LoginInitial();
      } catch (_) {
        yield LoginLoadFailure(error: _.toString());
      }
    }
    if (event is LogoutPressed) {
      yield LoginLoadInProgress();
      try {
        final contactData =
            await loginRepository.getLogout(event.token, event.session);
        yield LogoutSuccess(contactData: contactData);
        // authenticationBloc.add(AuthenticationLoggedIn(loginInfo: loginInfo));
        yield LoginInitial();
      } catch (_) {
        yield LogoutLoadFailure(error: _.toString());
      }
    }
  }
}
