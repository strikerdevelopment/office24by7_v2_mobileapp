part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginPressed extends LoginEvent {
  final String userID;
  const LoginPressed({this.userID});
  @override
  List<Object> get props => [userID];
}

class LogoutPressed extends LoginEvent {
  final String token;
  final String session;

  const LogoutPressed({@required this.token, @required this.session})
      : assert(token != null),
        assert(session != null);

  @override
  List<Object> get props => [token, session];
}
