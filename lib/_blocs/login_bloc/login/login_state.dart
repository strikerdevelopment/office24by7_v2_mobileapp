part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();
  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginLoadInProgress extends LoginState {}

class LoginLoadSuccess extends LoginState {
  final OtpInfo otpInfo;
  const LoginLoadSuccess({this.otpInfo});

  @override
  List<Object> get props => [otpInfo];

  String toString() => 'LoginLoadSuccess {otp : ${otpInfo.otpcode}}';
}

class LogoutSuccess extends LoginState {
  final List<ContactList> contactData;
  const LogoutSuccess({this.contactData});

  @override
  List<Object> get props => [contactData];

  String toString() => 'LogoutSuccess {logout : ${contactData.length}}';
}

class LoginLoadFailure extends LoginState {
  final String error;

  const LoginLoadFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class LogoutLoadFailure extends LoginState {
  final String error;
  const LogoutLoadFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}
