import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_blocs/login_bloc/authentication/authentication_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/login/login.dart';
import 'package:office24by7_v2/_models/login/login_info.dart';

part 'otp_event.dart';
part 'otp_state.dart';

class OtpBloc extends Bloc<OtpEvent, OtpState> {
  final AuthenticationBloc authenticationBloc;
  final LoginRepository loginRepository;
  final FirebaseRepository firebaseRepository;
  final Environment environment = Environment();

  OtpBloc({
    @required this.loginRepository,
    @required this.authenticationBloc,
    @required this.firebaseRepository,
  })  : assert(loginRepository != null),
        assert(authenticationBloc != null),
        super(OtpInitial());

  @override
  Stream<OtpState> mapEventToState(
    OtpEvent event,
  ) async* {
    if (event is OtpPressed) {
      yield OtpInProgress();
      try {
        final LoginInfo loginInfo =
            await loginRepository.getOtpData(event.otpInfo);

        await firebaseRepository.getAddUserCollection(loginInfo.userId,
            loginInfo.userAuthToken, await environment.fcmToken());

        yield OtpSuccess(loginInfo: loginInfo);
        authenticationBloc.add(AuthenticationLoggedIn(loginInfo: loginInfo));
        yield OtpInitial();
      } catch (_) {
        yield OtpFailure();
      }
    }
    if (event is OtpWithoutPressed) {
      yield OtpInProgress();
      try {
        await firebaseRepository.getAddUserCollection(event.loginInfo.userId,
            event.loginInfo.userAuthToken, await environment.fcmToken());
        yield OtpSuccess(loginInfo: event.loginInfo);
        authenticationBloc
            .add(AuthenticationLoggedIn(loginInfo: event.loginInfo));
        yield OtpInitial();
      } catch (_) {
        yield OtpFailure();
      }
    }
  }
}
