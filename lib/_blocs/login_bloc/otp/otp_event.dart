part of 'otp_bloc.dart';

abstract class OtpEvent extends Equatable {
  const OtpEvent();
}

class OtpPressed extends OtpEvent {
  final OtpInfo otpInfo;

  const OtpPressed({@required this.otpInfo}) : assert(otpInfo != null);

  @override
  List<Object> get props => [otpInfo];
}

class OtpWithoutPressed extends OtpEvent {
  final LoginInfo loginInfo;

  const OtpWithoutPressed({@required this.loginInfo})
      : assert(loginInfo != null);

  @override
  List<Object> get props => [loginInfo];
}
