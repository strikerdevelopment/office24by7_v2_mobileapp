part of 'otp_bloc.dart';

abstract class OtpState extends Equatable {
  const OtpState();
  @override
  List<Object> get props => [];
}

class OtpInitial extends OtpState {}

class OtpInProgress extends OtpState {}

class OtpSuccess extends OtpState {
  final LoginInfo loginInfo;

  const OtpSuccess({@required this.loginInfo}) : assert(loginInfo != null);

  @override
  List<Object> get props => [loginInfo];

  String toString() => 'OtpSuccess {User Id: ${loginInfo.userId}}';
}

class OtpFailure extends OtpState {}
