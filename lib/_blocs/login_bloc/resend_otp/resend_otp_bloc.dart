import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/login/login.dart';

part 'resend_otp_event.dart';
part 'resend_otp_state.dart';

class ResendOtpBloc extends Bloc<ResendOtpEvent, ResendOtpState> {
  final LoginRepository loginRepository;
  final Environment environment = Environment();

  ResendOtpBloc({this.loginRepository})
      : assert(loginRepository != null),
        super(ResendOtpInitial());

  @override
  Stream<ResendOtpState> mapEventToState(
    ResendOtpEvent event,
  ) async* {
    if (event is ResendOtpPressed) {
      yield ResendOtpInProgress();
      try {
        String getData = await loginRepository.getResendOtp(event.otpInfo);
        yield ResendOtpSuccess(stateData: getData);
      } catch (_) {
        yield ResendOtpFailure(error: _.toString());
      }
    }
  }
}
