part of 'resend_otp_bloc.dart';

abstract class ResendOtpEvent extends Equatable {
  const ResendOtpEvent();

  @override
  List<Object> get props => [];
}

class ResendOtpPressed extends ResendOtpEvent {
  final OtpInfo otpInfo;
  const ResendOtpPressed({this.otpInfo});

  @override
  List<Object> get props => [otpInfo];
}
