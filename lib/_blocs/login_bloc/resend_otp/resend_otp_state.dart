part of 'resend_otp_bloc.dart';

abstract class ResendOtpState extends Equatable {
  const ResendOtpState();

  @override
  List<Object> get props => [];
}

class ResendOtpInitial extends ResendOtpState {}

class ResendOtpInProgress extends ResendOtpState {}

class ResendOtpSuccess extends ResendOtpState {
  final String stateData;

  ResendOtpSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ResendOtpSuccess { stateData: $stateData }';
}

class ResendOtpFailure extends ResendOtpState {
  final String error;
  const ResendOtpFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ResendOtpFailure { error: $error }';
}
