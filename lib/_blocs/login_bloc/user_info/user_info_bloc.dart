import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/login/login.dart';

part 'user_info_event.dart';
part 'user_info_state.dart';

class UserInfoBloc extends Bloc<UserInfoEvent, UserInfoState> {
  final LoginRepository loginRepository;
  final Environment environment = Environment();

  UserInfoBloc({this.loginRepository}) : super(UserInfoInitial());

  @override
  Stream<UserInfoState> mapEventToState(
    UserInfoEvent event,
  ) async* {
    if (event is UserInfoPressed) {
      yield UserInfoInProgress();
      try {
        UserInfo getData =
            await loginRepository.getUserInfo(await environment.getToken());
        yield UserInfoSuccess(stateData: getData);
      } catch (_) {
        yield UserInfoFailure(error: _.toString());
      }
    }
  }
}
