part of 'user_info_bloc.dart';

abstract class UserInfoState extends Equatable {
  const UserInfoState();

  @override
  List<Object> get props => [];
}

class UserInfoInitial extends UserInfoState {}

class UserInfoInProgress extends UserInfoState {}

class UserInfoSuccess extends UserInfoState {
  final UserInfo stateData;

  UserInfoSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'ContactEmailIdSuccess { stateData: $stateData }';
}

class UserInfoFailure extends UserInfoState {
  final String error;
  const UserInfoFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
