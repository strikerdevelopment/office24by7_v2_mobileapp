import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_add_email_event.dart';
part 'oppo_add_email_state.dart';

class OppoAddEmailBloc extends Bloc<OppoAddEmailEvent, OppoAddEmailState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoAddEmailBloc({this.oppoRepository}) : super(OppoAddEmailInitial());

  @override
  Stream<OppoAddEmailState> mapEventToState(
    OppoAddEmailEvent event,
  ) async* {
    if (event is OppoAddEmailPressed) {
      yield OppoAddEmailInProgress();
      try {
        dynamic getData = await oppoRepository.getOppoAddEmail(
            await environment.getToken(), event.eventData);
        yield OppoAddEmailSuccess(stateData: getData);
      } catch (_) {
        yield OppoAddEmailFailure(error: _.toString());
      }
    }
  }
}
