part of 'oppo_add_email_bloc.dart';

abstract class OppoAddEmailEvent extends Equatable {
  const OppoAddEmailEvent();

  @override
  List<Object> get props => [];
}

class OppoAddEmailPressed extends OppoAddEmailEvent {
  final Map eventData;
  OppoAddEmailPressed({this.eventData});
}
