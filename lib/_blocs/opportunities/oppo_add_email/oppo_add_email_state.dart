part of 'oppo_add_email_bloc.dart';

abstract class OppoAddEmailState extends Equatable {
  const OppoAddEmailState();

  @override
  List<Object> get props => [];
}

class OppoAddEmailInitial extends OppoAddEmailState {}

class OppoAddEmailInProgress extends OppoAddEmailState {}

class OppoAddEmailSuccess extends OppoAddEmailState {
  final dynamic stateData;

  OppoAddEmailSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoAddEmailFailure extends OppoAddEmailState {
  final String error;
  const OppoAddEmailFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
