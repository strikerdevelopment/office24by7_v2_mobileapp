import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_add_lead_event.dart';
part 'oppo_add_lead_state.dart';

class OppoAddLeadBloc extends Bloc<OppoAddLeadEvent, OppoAddLeadState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoAddLeadBloc({this.oppoRepository}) : super(OppoAddLeadInitial());

  @override
  Stream<OppoAddLeadState> mapEventToState(
    OppoAddLeadEvent event,
  ) async* {
    if (event is OppoAddLeadPressed) {
      yield OppoAddLeadInProgress();
      try {
        String getData = await oppoRepository.getOppoAddLead(
            await environment.getToken(), event.eventData);
        yield OppoAddLeadSuccess(stateData: getData);
      } catch (_) {
        yield OppoAddLeadFailure(error: _.toString());
      }
    }
  }
}
