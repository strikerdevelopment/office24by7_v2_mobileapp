part of 'oppo_add_lead_bloc.dart';

abstract class OppoAddLeadEvent extends Equatable {
  const OppoAddLeadEvent();

  @override
  List<Object> get props => [];
}

class OppoAddLeadPressed extends OppoAddLeadEvent {
  final Map eventData;
  OppoAddLeadPressed({this.eventData});
}
