part of 'oppo_add_lead_bloc.dart';

abstract class OppoAddLeadState extends Equatable {
  const OppoAddLeadState();

  @override
  List<Object> get props => [];
}

class OppoAddLeadInitial extends OppoAddLeadState {}

class OppoAddLeadInProgress extends OppoAddLeadState {}

class OppoAddLeadSuccess extends OppoAddLeadState {
  final String stateData;

  OppoAddLeadSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoAddLeadFailure extends OppoAddLeadState {
  final String error;
  const OppoAddLeadFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
