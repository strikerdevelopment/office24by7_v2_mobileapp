import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_add_meeting_event.dart';
part 'oppo_add_meeting_state.dart';

class OppoAddMeetingBloc
    extends Bloc<OppoAddMeetingEvent, OppoAddMeetingState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoAddMeetingBloc({this.oppoRepository}) : super(OppoAddMeetingInitial());

  @override
  Stream<OppoAddMeetingState> mapEventToState(
    OppoAddMeetingEvent event,
  ) async* {
    if (event is OppoAddMeetingPressed) {
      yield OppoAddMeetingInProgress();
      try {
        String getData = await oppoRepository.getOppoAddMeeting(
            await environment.getToken(), event.eventData);
        yield OppoAddMeetingSuccess(stateData: getData);
      } catch (_) {
        yield OppoAddMeetingFailure(error: _.toString());
      }
    }
  }
}
