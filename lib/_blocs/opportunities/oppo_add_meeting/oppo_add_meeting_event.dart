part of 'oppo_add_meeting_bloc.dart';

abstract class OppoAddMeetingEvent extends Equatable {
  const OppoAddMeetingEvent();

  @override
  List<Object> get props => [];
}

class OppoAddMeetingPressed extends OppoAddMeetingEvent {
  final Map eventData;
  OppoAddMeetingPressed({this.eventData});
}
