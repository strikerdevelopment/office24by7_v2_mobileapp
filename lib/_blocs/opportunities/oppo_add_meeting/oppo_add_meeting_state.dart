part of 'oppo_add_meeting_bloc.dart';

abstract class OppoAddMeetingState extends Equatable {
  const OppoAddMeetingState();

  @override
  List<Object> get props => [];
}

class OppoAddMeetingInitial extends OppoAddMeetingState {}

class OppoAddMeetingInProgress extends OppoAddMeetingState {}

class OppoAddMeetingSuccess extends OppoAddMeetingState {
  final String stateData;

  OppoAddMeetingSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoAddMeetingFailure extends OppoAddMeetingState {
  final String error;
  const OppoAddMeetingFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
