import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_add_reassign_event.dart';
part 'oppo_add_reassign_state.dart';

class OppoAddReassignBloc
    extends Bloc<OppoAddReassignEvent, OppoAddReassignState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoAddReassignBloc({this.oppoRepository}) : super(OppoAddReassignInitial());

  @override
  Stream<OppoAddReassignState> mapEventToState(
    OppoAddReassignEvent event,
  ) async* {
    if (event is OppoAddReassignPressed) {
      yield OppoAddReassignInProgress();
      try {
        String getData = await oppoRepository.getOppoCreateReassing(
            await environment.getToken(), event.eventData);
        yield OppoAddReassignSuccess(stateData: getData);
      } catch (_) {
        yield OppoAddReassignFailure(error: _.toString());
      }
    }
  }
}
