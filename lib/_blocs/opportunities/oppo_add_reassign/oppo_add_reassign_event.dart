part of 'oppo_add_reassign_bloc.dart';

abstract class OppoAddReassignEvent extends Equatable {
  const OppoAddReassignEvent();

  @override
  List<Object> get props => [];
}

class OppoAddReassignPressed extends OppoAddReassignEvent {
  final Map eventData;
  OppoAddReassignPressed({this.eventData});
}
