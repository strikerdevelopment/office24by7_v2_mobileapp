part of 'oppo_add_reassign_bloc.dart';

abstract class OppoAddReassignState extends Equatable {
  const OppoAddReassignState();

  @override
  List<Object> get props => [];
}

class OppoAddReassignInitial extends OppoAddReassignState {}

class OppoAddReassignInProgress extends OppoAddReassignState {}

class OppoAddReassignSuccess extends OppoAddReassignState {
  final String stateData;

  OppoAddReassignSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoAddReassignFailure extends OppoAddReassignState {
  final String error;
  const OppoAddReassignFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
