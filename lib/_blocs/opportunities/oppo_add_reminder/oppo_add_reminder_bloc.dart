import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_add_reminder_event.dart';
part 'oppo_add_reminder_state.dart';

class OppoAddReminderBloc
    extends Bloc<OppoAddReminderEvent, OppoAddReminderState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoAddReminderBloc({this.oppoRepository}) : super(OppoAddReminderInitial());

  @override
  Stream<OppoAddReminderState> mapEventToState(
    OppoAddReminderEvent event,
  ) async* {
    if (event is OppoAddReminderPressed) {
      yield OppoAddReminderInProgress();
      try {
        String getData = await oppoRepository.getOppoAddReminder(
            await environment.getToken(), event.eventData);
        yield OppoAddReminderSuccess(stateData: getData);
      } catch (_) {
        yield OppoAddReminderFailure(error: _.toString());
      }
    }
  }
}
