part of 'oppo_add_reminder_bloc.dart';

abstract class OppoAddReminderEvent extends Equatable {
  const OppoAddReminderEvent();

  @override
  List<Object> get props => [];
}

class OppoAddReminderPressed extends OppoAddReminderEvent {
  final Map eventData;
  OppoAddReminderPressed({this.eventData});
}
