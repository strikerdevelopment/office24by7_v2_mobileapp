part of 'oppo_add_reminder_bloc.dart';

abstract class OppoAddReminderState extends Equatable {
  const OppoAddReminderState();

  @override
  List<Object> get props => [];
}

class OppoAddReminderInitial extends OppoAddReminderState {}

class OppoAddReminderInProgress extends OppoAddReminderState {}

class OppoAddReminderSuccess extends OppoAddReminderState {
  final String stateData;

  OppoAddReminderSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoAddReminderFailure extends OppoAddReminderState {
  final String error;
  const OppoAddReminderFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
