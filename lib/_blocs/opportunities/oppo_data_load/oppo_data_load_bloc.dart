import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_data_load_event.dart';
part 'oppo_data_load_state.dart';

class OppoDataLoadBloc extends Bloc<OppoDataLoadEvent, OppoDataLoadState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDataLoadBloc({this.oppoRepository}) : super(OppoDataLoadInitial());

  @override
  Stream<OppoDataLoadState> mapEventToState(
    OppoDataLoadEvent event,
  ) async* {
    final currentState = state;

    if (event is OppoDataLoadPressed && !_hasReachedMax(currentState)) {
      try {
        if (state is OppoDataLoadInitial) {
          List<OppoLoadData> getData = await oppoRepository.getOppoLoadData(
            await environment.getToken(),
            event.displayType,
            event.displayAs,
            event.displayID,
            event.dateType,
            event.fromDate,
            event.toDate,
            event.searchValue,
            event.assignedTo,
            0,
            Environment.dataDisplayCount,
          );
          yield OppoDataLoadSuccess(
              stateData: getData, hasReachedMax: false, currentState: getData);
        }
        if (currentState is OppoDataLoadSuccess) {
          final stateData = await oppoRepository.getOppoLoadData(
            await environment.getToken(),
            event.displayType,
            event.displayAs,
            event.displayID,
            event.dateType,
            event.fromDate,
            event.toDate,
            event.searchValue,
            event.assignedTo,
            currentState.stateData.length,
            Environment.dataDisplayCount,
          );
          yield stateData.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : OppoDataLoadSuccess(
                  stateData: currentState.stateData + stateData,
                  hasReachedMax: false,
                  currentState: stateData);
        }
      } catch (_) {
        yield OppoDataLoadFailure(error: _.toString());
      }
    }
  }

  bool _hasReachedMax(OppoDataLoadState state) =>
      state is OppoDataLoadSuccess && state.hasReachedMax;
}
