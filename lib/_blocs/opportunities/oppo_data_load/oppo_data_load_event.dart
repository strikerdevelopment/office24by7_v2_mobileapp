part of 'oppo_data_load_bloc.dart';

abstract class OppoDataLoadEvent extends Equatable {
  const OppoDataLoadEvent();

  @override
  List<Object> get props => [];
}

class OppoDataLoadPressed extends OppoDataLoadEvent {
  final String displayType;
  final String displayAs;
  final String displayID;
  final String dateType;
  final String fromDate;
  final String toDate;
  final String searchValue;
  final String assignedTo;

  OppoDataLoadPressed({
    this.displayAs,
    this.displayID,
    this.displayType,
    this.assignedTo,
    this.dateType,
    this.fromDate,
    this.searchValue,
    this.toDate,
  });

  @override
  @override
  List<Object> get props => [displayAs, displayID, displayType];

  @override
  String toString() => 'OppoDataLoadPressed { searchStatusId: $displayAs }';
}
