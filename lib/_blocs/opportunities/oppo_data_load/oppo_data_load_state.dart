part of 'oppo_data_load_bloc.dart';

abstract class OppoDataLoadState extends Equatable {
  const OppoDataLoadState();

  @override
  List<Object> get props => [];
}

class OppoDataLoadInitial extends OppoDataLoadState {}

class OppoDataLoadSuccess extends OppoDataLoadState {
  final List<OppoLoadData> stateData;
  final bool hasReachedMax;
  final List<OppoLoadData> currentState;

  OppoDataLoadSuccess({this.stateData, this.hasReachedMax, this.currentState});

  OppoDataLoadSuccess copyWith(
      {List<OppoLoadData> stateData, bool hasReachedMax}) {
    return OppoDataLoadSuccess(
        stateData: stateData ?? this.stateData,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        currentState: currentState == null ? [] : currentState);
  }

  @override
  List<Object> get props => [stateData, currentState];

  @override
  String toString() => 'OppoDataLoadSuccess { stateData: ${stateData.length} }';
}

class OppoDataLoadFailure extends OppoDataLoadState {
  final String error;
  const OppoDataLoadFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDataLoadFailure { error: $error }';
}
