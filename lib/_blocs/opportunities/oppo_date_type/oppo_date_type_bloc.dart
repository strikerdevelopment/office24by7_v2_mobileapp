import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_date_type_event.dart';
part 'oppo_date_type_state.dart';

class OppoDateTypeBloc extends Bloc<OppoDateTypeEvent, OppoDateTypeState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDateTypeBloc({this.oppoRepository}) : super(OppoDateTypeInitial());

  @override
  Stream<OppoDateTypeState> mapEventToState(
    OppoDateTypeEvent event,
  ) async* {
    if (event is OppoDateTypePressed) {
      yield OppoDateTypeInitial();
      try {
        List<OppoDateType> getData =
            await oppoRepository.getOppoDateType(await environment.getToken());
        yield OppoDateTypeSuccess(stateData: getData);
      } catch (_) {
        yield OppoDateTypeFailure(error: _.toString());
      }
    }
  }
}
