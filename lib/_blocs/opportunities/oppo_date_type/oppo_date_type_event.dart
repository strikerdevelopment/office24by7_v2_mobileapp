part of 'oppo_date_type_bloc.dart';

abstract class OppoDateTypeEvent extends Equatable {
  const OppoDateTypeEvent();

  @override
  List<Object> get props => [];
}

class OppoDateTypePressed extends OppoDateTypeEvent {}
