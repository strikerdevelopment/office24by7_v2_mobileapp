part of 'oppo_date_type_bloc.dart';

abstract class OppoDateTypeState extends Equatable {
  const OppoDateTypeState();

  @override
  List<Object> get props => [];
}

class OppoDateTypeInitial extends OppoDateTypeState {}

class OppoDateTypeSuccess extends OppoDateTypeState {
  final List<OppoDateType> stateData;

  OppoDateTypeSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'OppoDateTypeSuccess { stateData: ${stateData.length} }';
}

class OppoDateTypeFailure extends OppoDateTypeState {
  final String error;
  const OppoDateTypeFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDateTypeFailure { error: $error }';
}
