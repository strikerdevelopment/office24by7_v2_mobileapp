import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_division_dd_event.dart';
part 'oppo_division_dd_state.dart';

class OppoDivisionDdBloc
    extends Bloc<OppoDivisionDdEvent, OppoDivisionDdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDivisionDdBloc({this.oppoRepository}) : super(OppoDivisionDdInitial());

  @override
  Stream<OppoDivisionDdState> mapEventToState(
    OppoDivisionDdEvent event,
  ) async* {
    if (event is OppoDivisionDdPressed) {
      yield OppoDivisionDdInitial();
      try {
        List<OppoDivisionDd> getData = await oppoRepository
            .getOppoDivisionDD(await environment.getToken());
        yield OppoDivisionDdSuccess(stateData: getData);
      } catch (_) {
        yield OppoDivisionDdFailure(error: _.toString());
      }
    }
  }
}
