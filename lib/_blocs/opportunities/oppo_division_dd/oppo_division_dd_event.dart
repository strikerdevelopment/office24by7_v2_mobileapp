part of 'oppo_division_dd_bloc.dart';

abstract class OppoDivisionDdEvent extends Equatable {
  const OppoDivisionDdEvent();

  @override
  List<Object> get props => [];
}

class OppoDivisionDdPressed extends OppoDivisionDdEvent {}
