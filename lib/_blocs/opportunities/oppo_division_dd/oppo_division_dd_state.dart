part of 'oppo_division_dd_bloc.dart';

abstract class OppoDivisionDdState extends Equatable {
  const OppoDivisionDdState();

  @override
  List<Object> get props => [];
}

class OppoDivisionDdInitial extends OppoDivisionDdState {}

class OppoDivisionDdSuccess extends OppoDivisionDdState {
  final List<OppoDivisionDd> stateData;

  OppoDivisionDdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoDivisionDdSuccess { stateData: ${stateData.length} }';
}

class OppoDivisionDdFailure extends OppoDivisionDdState {
  final String error;
  const OppoDivisionDdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDivisionDdFailure { error: $error }';
}
