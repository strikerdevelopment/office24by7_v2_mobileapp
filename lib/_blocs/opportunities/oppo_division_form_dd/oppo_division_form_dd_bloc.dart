import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_division_form_dd_event.dart';
part 'oppo_division_form_dd_state.dart';

class OppoDivisionFormDdBloc
    extends Bloc<OppoDivisionFormDdEvent, OppoDivisionFormDdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDivisionFormDdBloc({this.oppoRepository})
      : super(OppoDivisionFormDdInitial());

  @override
  Stream<OppoDivisionFormDdState> mapEventToState(
    OppoDivisionFormDdEvent event,
  ) async* {
    if (event is OppoDivisionFormDdPressed) {
      yield OppoDivisionFormDdInitial();
      try {
        List<OppoDivisionFormDd> getData =
            await oppoRepository.getOppoDivisionFormDD(
                await environment.getToken(), event.divisionID);
        yield OppoDivisionFormDdSuccess(stateData: getData);
      } catch (_) {
        yield OppoDivisionFormDdFailure(error: _.toString());
      }
    }
  }
}
