part of 'oppo_division_form_dd_bloc.dart';

abstract class OppoDivisionFormDdEvent extends Equatable {
  const OppoDivisionFormDdEvent();

  @override
  List<Object> get props => [];
}

class OppoDivisionFormDdPressed extends OppoDivisionFormDdEvent {
  final String divisionID;

  OppoDivisionFormDdPressed({this.divisionID});

  @override
  List<Object> get props => [divisionID];

  String toString() => 'OppoDivisionFormDdPressed : {divisionID : $divisionID}';
}
