part of 'oppo_division_form_dd_bloc.dart';

abstract class OppoDivisionFormDdState extends Equatable {
  const OppoDivisionFormDdState();

  @override
  List<Object> get props => [];
}

class OppoDivisionFormDdInitial extends OppoDivisionFormDdState {}

class OppoDivisionFormDdSuccess extends OppoDivisionFormDdState {
  final List<OppoDivisionFormDd> stateData;

  OppoDivisionFormDdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoDivisionFormDdSuccess { stateData: ${stateData.length} }';
}

class OppoDivisionFormDdFailure extends OppoDivisionFormDdState {
  final String error;
  const OppoDivisionFormDdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDivisionFormDdFailure { error: $error }';
}
