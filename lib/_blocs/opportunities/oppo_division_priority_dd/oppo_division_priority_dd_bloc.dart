import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_division_priority_dd_event.dart';
part 'oppo_division_priority_dd_state.dart';

class OppoDivisionPriorityDdBloc
    extends Bloc<OppoDivisionPriorityDdEvent, OppoDivisionPriorityDdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDivisionPriorityDdBloc({this.oppoRepository})
      : super(OppoDivisionPriorityDdInitial());

  @override
  Stream<OppoDivisionPriorityDdState> mapEventToState(
    OppoDivisionPriorityDdEvent event,
  ) async* {
    if (event is OppoDivisionPriorityDdPressed) {
      yield OppoDivisionPriorityDdInitial();
      try {
        List<OppoDivisionPriorityDd> getData =
            await oppoRepository.getOppoDivisionPriorityDd(
                await environment.getToken(), event.divisionID);
        yield OppoDivisionPriorityDdSuccess(stateData: getData);
      } catch (_) {
        yield OppoDivisionPriorityDdFailure(error: _.toString());
      }
    }
  }
}
