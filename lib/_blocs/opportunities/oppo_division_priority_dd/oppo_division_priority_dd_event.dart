part of 'oppo_division_priority_dd_bloc.dart';

abstract class OppoDivisionPriorityDdEvent extends Equatable {
  const OppoDivisionPriorityDdEvent();

  @override
  List<Object> get props => [];
}

class OppoDivisionPriorityDdPressed extends OppoDivisionPriorityDdEvent {
  final String divisionID;

  OppoDivisionPriorityDdPressed({this.divisionID});

  @override
  List<Object> get props => [divisionID];

  @override
  String toString() =>
      'OppoDivisionPriorityDdPressed { eventData: $divisionID }';
}
