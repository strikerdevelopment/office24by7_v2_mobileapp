part of 'oppo_division_priority_dd_bloc.dart';

abstract class OppoDivisionPriorityDdState extends Equatable {
  const OppoDivisionPriorityDdState();

  @override
  List<Object> get props => [];
}

class OppoDivisionPriorityDdInitial extends OppoDivisionPriorityDdState {}

class OppoDivisionPriorityDdSuccess extends OppoDivisionPriorityDdState {
  final List<OppoDivisionPriorityDd> stateData;

  OppoDivisionPriorityDdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoDivisionPriorityDdSuccess { stateData: ${stateData.length} }';
}

class OppoDivisionPriorityDdFailure extends OppoDivisionPriorityDdState {
  final String error;
  const OppoDivisionPriorityDdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDivisionPriorityDdFailure { error: $error }';
}
