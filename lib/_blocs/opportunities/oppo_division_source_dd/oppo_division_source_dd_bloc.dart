import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_division_source_dd_event.dart';
part 'oppo_division_source_dd_state.dart';

class OppoDivisionSourceDdBloc
    extends Bloc<OppoDivisionSourceDdEvent, OppoDivisionSourceDdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDivisionSourceDdBloc({this.oppoRepository})
      : super(OppoDivisionSourceDdInitial());

  @override
  Stream<OppoDivisionSourceDdState> mapEventToState(
    OppoDivisionSourceDdEvent event,
  ) async* {
    if (event is OppoDivisionSourceDdPressed) {
      yield OppoDivisionSourceDdInitial();
      try {
        List<OppoDivisionSourceDd> getData =
            await oppoRepository.getOppoDivisionSourceDd(
                await environment.getToken(), event.divisionID);
        yield OppoDivisionSourceDdSuccess(stateData: getData);
      } catch (_) {
        yield OppoDivisionSourceDdFailure(error: _.toString());
      }
    }
  }
}
