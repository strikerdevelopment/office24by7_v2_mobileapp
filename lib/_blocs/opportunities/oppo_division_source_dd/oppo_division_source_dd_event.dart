part of 'oppo_division_source_dd_bloc.dart';

abstract class OppoDivisionSourceDdEvent extends Equatable {
  const OppoDivisionSourceDdEvent();

  @override
  List<Object> get props => [];
}

class OppoDivisionSourceDdPressed extends OppoDivisionSourceDdEvent {
  final String divisionID;

  OppoDivisionSourceDdPressed({this.divisionID});

  @override
  List<Object> get props => [divisionID];

  String toString() =>
      'OppoDivisionSourceDdPressed : {divisionID : $divisionID}';
}
