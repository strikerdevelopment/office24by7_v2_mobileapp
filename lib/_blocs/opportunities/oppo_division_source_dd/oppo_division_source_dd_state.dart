part of 'oppo_division_source_dd_bloc.dart';

abstract class OppoDivisionSourceDdState extends Equatable {
  const OppoDivisionSourceDdState();

  @override
  List<Object> get props => [];
}

class OppoDivisionSourceDdInitial extends OppoDivisionSourceDdState {}

class OppoDivisionSourceDdSuccess extends OppoDivisionSourceDdState {
  final List<OppoDivisionSourceDd> stateData;

  OppoDivisionSourceDdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoDivisionSourceDdSuccess { stateData: ${stateData.length} }';
}

class OppoDivisionSourceDdFailure extends OppoDivisionSourceDdState {
  final String error;
  const OppoDivisionSourceDdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDivisionSourceDdFailure { error: $error }';
}
