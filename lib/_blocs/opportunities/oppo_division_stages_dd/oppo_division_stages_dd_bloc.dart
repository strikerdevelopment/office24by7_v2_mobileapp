import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_division_stages_dd_event.dart';
part 'oppo_division_stages_dd_state.dart';

class OppoDivisionStagesDdBloc
    extends Bloc<OppoDivisionStagesDdEvent, OppoDivisionStagesDdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDivisionStagesDdBloc({this.oppoRepository})
      : super(OppoDivisionStagesDdInitial());

  @override
  Stream<OppoDivisionStagesDdState> mapEventToState(
    OppoDivisionStagesDdEvent event,
  ) async* {
    if (event is OppoDivisionStagesDdPressed) {
      yield OppoDivisionStagesDdInitial();
      try {
        List<OppoDivisionStagesDd> getData =
            await oppoRepository.getOppoDivisionStagesDd(
                await environment.getToken(), event.divisionID, event.statusID);
        yield OppoDivisionStagesDdSuccess(stateData: getData);
      } catch (_) {
        yield OppoDivisionStagesDdFailure(error: _.toString());
      }
    }
  }
}
