part of 'oppo_division_stages_dd_bloc.dart';

abstract class OppoDivisionStagesDdEvent extends Equatable {
  const OppoDivisionStagesDdEvent();

  @override
  List<Object> get props => [];
}

class OppoDivisionStagesDdPressed extends OppoDivisionStagesDdEvent {
  final String divisionID;
  final String statusID;

  OppoDivisionStagesDdPressed({this.divisionID, this.statusID});

  @override
  List<Object> get props => [divisionID];

  @override
  String toString() => 'OppoDivisionStagesDdPressed { eventData: $divisionID }';
}
