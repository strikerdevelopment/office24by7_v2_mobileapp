part of 'oppo_division_stages_dd_bloc.dart';

abstract class OppoDivisionStagesDdState extends Equatable {
  const OppoDivisionStagesDdState();

  @override
  List<Object> get props => [];
}

class OppoDivisionStagesDdInitial extends OppoDivisionStagesDdState {}

class OppoDivisionStagesDdSuccess extends OppoDivisionStagesDdState {
  final List<OppoDivisionStagesDd> stateData;

  OppoDivisionStagesDdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoDivisionStagesDdSuccess { stateData: ${stateData.length} }';
}

class OppoDivisionStagesDdFailure extends OppoDivisionStagesDdState {
  final String error;
  const OppoDivisionStagesDdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDivisionStagesDdFailure { error: $error }';
}
