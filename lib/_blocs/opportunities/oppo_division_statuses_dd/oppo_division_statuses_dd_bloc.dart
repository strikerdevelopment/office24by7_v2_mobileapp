import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_division_statuses_dd_event.dart';
part 'oppo_division_statuses_dd_state.dart';

class OppoDivisionStatusesDdBloc
    extends Bloc<OppoDivisionStatusesDdEvent, OppoDivisionStatusesDdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDivisionStatusesDdBloc({this.oppoRepository})
      : super(OppoDivisionStatusesDdInitial());

  @override
  Stream<OppoDivisionStatusesDdState> mapEventToState(
    OppoDivisionStatusesDdEvent event,
  ) async* {
    if (event is OppoDivisionStatusesDdPressed) {
      yield OppoDivisionStatusesDdInitial();
      try {
        List<OppoDivisionStatusesDd> getData =
            await oppoRepository.getOppoDivisionStatusesDd(
                await environment.getToken(), event.eventData);
        yield OppoDivisionStatusesDdSuccess(stateData: getData);
      } catch (_) {
        yield OppoDivisionStatusesDdFailure(error: _.toString());
      }
    }
  }
}
