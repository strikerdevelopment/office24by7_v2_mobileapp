part of 'oppo_division_statuses_dd_bloc.dart';

abstract class OppoDivisionStatusesDdEvent extends Equatable {
  const OppoDivisionStatusesDdEvent();

  @override
  List<Object> get props => [];
}

class OppoDivisionStatusesDdPressed extends OppoDivisionStatusesDdEvent {
  final String eventData;

  OppoDivisionStatusesDdPressed({this.eventData});

  @override
  List<Object> get props => [eventData];

  @override
  String toString() =>
      'OppoDivisionStatusesDdPressed { eventData: $eventData }';
}
