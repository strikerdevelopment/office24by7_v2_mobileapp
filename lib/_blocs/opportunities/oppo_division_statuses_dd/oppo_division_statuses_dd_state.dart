part of 'oppo_division_statuses_dd_bloc.dart';

abstract class OppoDivisionStatusesDdState extends Equatable {
  const OppoDivisionStatusesDdState();

  @override
  List<Object> get props => [];
}

class OppoDivisionStatusesDdInitial extends OppoDivisionStatusesDdState {}

class OppoDivisionStatusesDdSuccess extends OppoDivisionStatusesDdState {
  final List<OppoDivisionStatusesDd> stateData;

  OppoDivisionStatusesDdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoDivisionStatusesDdSuccess { stateData: ${stateData.length} }';
}

class OppoDivisionStatusesDdFailure extends OppoDivisionStatusesDdState {
  final String error;
  const OppoDivisionStatusesDdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDivisionStatusesDdFailure { error: $error }';
}
