import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_division_user_dd_event.dart';
part 'oppo_division_user_dd_state.dart';

class OppoDivisionUserDdBloc
    extends Bloc<OppoDivisionUserDdEvent, OppoDivisionUserDdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoDivisionUserDdBloc({this.oppoRepository})
      : super(OppoDivisionUserDdInitial());

  @override
  Stream<OppoDivisionUserDdState> mapEventToState(
    OppoDivisionUserDdEvent event,
  ) async* {
    if (event is OppoDivisionUserDdPressed) {
      yield OppoDivisionUserDdInitial();
      try {
        List<OppoDivisionUserDd> getData =
            await oppoRepository.getOppoDivisionUserDD(
                await environment.getToken(), event.eventData);
        yield OppoDivisionUserDdSuccess(stateData: getData);
      } catch (_) {
        yield OppoDivisionUserDdFailure(error: _.toString());
      }
    }
  }
}
