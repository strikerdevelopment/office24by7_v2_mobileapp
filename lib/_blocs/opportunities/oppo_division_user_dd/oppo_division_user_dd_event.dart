part of 'oppo_division_user_dd_bloc.dart';

abstract class OppoDivisionUserDdEvent extends Equatable {
  const OppoDivisionUserDdEvent();

  @override
  List<Object> get props => [];
}

class OppoDivisionUserDdPressed extends OppoDivisionUserDdEvent {
  final String eventData;

  OppoDivisionUserDdPressed({this.eventData});

  @override
  List<Object> get props => [eventData];

  @override
  String toString() => 'OppoDivisionUserDdPressed { eventData: $eventData }';
}
