part of 'oppo_division_user_dd_bloc.dart';

abstract class OppoDivisionUserDdState extends Equatable {
  const OppoDivisionUserDdState();

  @override
  List<Object> get props => [];
}

class OppoDivisionUserDdInitial extends OppoDivisionUserDdState {}

class OppoDivisionUserDdSuccess extends OppoDivisionUserDdState {
  final List<OppoDivisionUserDd> stateData;

  OppoDivisionUserDdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoDivisionUserDdSuccess { stateData: ${stateData.length} }';
}

class OppoDivisionUserDdFailure extends OppoDivisionUserDdState {
  final String error;
  const OppoDivisionUserDdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoDivisionUserDdFailure { error: $error }';
}
