import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_edit_lead_event.dart';
part 'oppo_edit_lead_state.dart';

class OppoEditLeadBloc extends Bloc<OppoEditLeadEvent, OppoEditLeadState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoEditLeadBloc({this.oppoRepository}) : super(OppoEditLeadInitial());

  @override
  Stream<OppoEditLeadState> mapEventToState(
    OppoEditLeadEvent event,
  ) async* {
    if (event is OppoEditLeadPressed) {
      yield OppoEditLeadInProgress();
      try {
        String getData = await oppoRepository.getOppoEditLead(
            await environment.getToken(), event.eventData);
        yield OppoEditLeadSuccess(stateData: getData);
      } catch (_) {
        yield OppoEditLeadFailure(error: _.toString());
      }
    }
  }
}
