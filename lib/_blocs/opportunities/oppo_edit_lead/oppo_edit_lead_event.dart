part of 'oppo_edit_lead_bloc.dart';

abstract class OppoEditLeadEvent extends Equatable {
  const OppoEditLeadEvent();

  @override
  List<Object> get props => [];
}

class OppoEditLeadPressed extends OppoEditLeadEvent {
  final Map eventData;
  OppoEditLeadPressed({this.eventData});
}
