part of 'oppo_edit_lead_bloc.dart';

abstract class OppoEditLeadState extends Equatable {
  const OppoEditLeadState();

  @override
  List<Object> get props => [];
}

class OppoEditLeadInitial extends OppoEditLeadState {}

class OppoEditLeadInProgress extends OppoEditLeadState {}

class OppoEditLeadSuccess extends OppoEditLeadState {
  final String stateData;

  OppoEditLeadSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoEditLeadFailure extends OppoEditLeadState {
  final String error;
  const OppoEditLeadFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
