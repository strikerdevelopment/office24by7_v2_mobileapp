import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_email_id_event.dart';
part 'oppo_email_id_state.dart';

class OppoEmailIdBloc extends Bloc<OppoEmailIdEvent, OppoEmailIdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoEmailIdBloc({this.oppoRepository}) : super(OppoEmailIdInitial());

  @override
  Stream<OppoEmailIdState> mapEventToState(
    OppoEmailIdEvent event,
  ) async* {
    if (event is OppoEmailIdPressed) {
      yield OppoEmailIdInitial();
      try {
        List<OppoEmailId> getData =
            await oppoRepository.getOppoEmailId(await environment.getToken());
        yield OppoEmailIdSuccess(stateData: getData);
      } catch (_) {
        yield OppoEmailIdFailure(error: _.toString());
      }
    }
  }
}
