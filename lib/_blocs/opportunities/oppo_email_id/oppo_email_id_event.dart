part of 'oppo_email_id_bloc.dart';

abstract class OppoEmailIdEvent extends Equatable {
  const OppoEmailIdEvent();

  @override
  List<Object> get props => [];
}

class OppoEmailIdPressed extends OppoEmailIdEvent {}
