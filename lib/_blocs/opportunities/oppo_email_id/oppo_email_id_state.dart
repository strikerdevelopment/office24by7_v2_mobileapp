part of 'oppo_email_id_bloc.dart';

abstract class OppoEmailIdState extends Equatable {
  const OppoEmailIdState();

  @override
  List<Object> get props => [];
}

class OppoEmailIdInitial extends OppoEmailIdState {}

class OppoEmailIdSuccess extends OppoEmailIdState {
  final List<OppoEmailId> stateData;

  OppoEmailIdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'OppoEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoEmailIdFailure extends OppoEmailIdState {
  final String error;
  const OppoEmailIdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoEmailIdFailure { error: $error }';
}
