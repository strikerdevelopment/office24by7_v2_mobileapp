import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_form_fields_event.dart';
part 'oppo_form_fields_state.dart';

class OppoFormFieldsBloc
    extends Bloc<OppoFormFieldsEvent, OppoFormFieldsState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoFormFieldsBloc({this.oppoRepository}) : super(OppoFormFieldsInitial());

  @override
  Stream<OppoFormFieldsState> mapEventToState(
    OppoFormFieldsEvent event,
  ) async* {
    if (event is OppoFormFieldsPressed) {
      yield OppoFormFieldsInProgress();
      try {
        List<OppoFormFieldsModel> getData = await oppoRepository
            .getOppoFormFields(await environment.getToken(), event.formID);
        yield OppoFormFieldsSuccess(stateData: getData);
      } catch (_) {
        yield OppoFormFieldsFailure(error: _.toString());
      }
    }
  }
}
