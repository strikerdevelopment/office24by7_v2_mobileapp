part of 'oppo_form_fields_bloc.dart';

abstract class OppoFormFieldsEvent extends Equatable {
  const OppoFormFieldsEvent();

  @override
  List<Object> get props => [];
}

class OppoFormFieldsPressed extends OppoFormFieldsEvent {
  final String formID;

  OppoFormFieldsPressed({this.formID});

  @override
  List<Object> get props => [formID];

  String toString() => 'OppoFormFieldsPressed : {formID : $formID}';
}
