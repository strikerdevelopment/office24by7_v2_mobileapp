part of 'oppo_form_fields_bloc.dart';

abstract class OppoFormFieldsState extends Equatable {
  const OppoFormFieldsState();

  @override
  List<Object> get props => [];
}

class OppoFormFieldsInitial extends OppoFormFieldsState {}

class OppoFormFieldsInProgress extends OppoFormFieldsState {}

class OppoFormFieldsSuccess extends OppoFormFieldsState {
  final List<OppoFormFieldsModel> stateData;

  OppoFormFieldsSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'OppoFormFieldsSuccess { stateData: $stateData }';
}

class OppoFormFieldsFailure extends OppoFormFieldsState {
  final String error;
  const OppoFormFieldsFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoFormFieldsFailure { error: $error }';
}
