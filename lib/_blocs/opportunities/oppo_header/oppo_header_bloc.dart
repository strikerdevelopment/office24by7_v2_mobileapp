import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_header_event.dart';
part 'oppo_header_state.dart';

class OppoHeaderBloc extends Bloc<OppoHeaderEvent, OppoHeaderState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoHeaderBloc({this.oppoRepository}) : super(OppoHeaderInitial());

  @override
  Stream<OppoHeaderState> mapEventToState(
    OppoHeaderEvent event,
  ) async* {
    if (event is OppoHeaderPressed) {
      yield OppoHeaderInitial();
      try {
        List<OppoHeader> getData = await oppoRepository.getOppoHeaders(
            await environment.getToken(), event.displayAs);
        yield OppoHeaderSuccess(stateData: getData);
      } catch (_) {
        yield OppoHeaderFailure(error: _.toString());
      }
    }
  }
}
