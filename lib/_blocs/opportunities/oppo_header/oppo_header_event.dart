part of 'oppo_header_bloc.dart';

abstract class OppoHeaderEvent extends Equatable {
  const OppoHeaderEvent();

  @override
  List<Object> get props => [];
}

class OppoHeaderPressed extends OppoHeaderEvent {
  final String displayAs;

  OppoHeaderPressed({this.displayAs});

  @override
  List<Object> get props => [displayAs];
}
