part of 'oppo_header_bloc.dart';

abstract class OppoHeaderState extends Equatable {
  const OppoHeaderState();

  @override
  List<Object> get props => [];
}

class OppoHeaderInitial extends OppoHeaderState {}

class OppoHeaderSuccess extends OppoHeaderState {
  final List<OppoHeader> stateData;

  OppoHeaderSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'OppoHeaderSuccess { stateData: ${stateData.length} }';
}

class OppoHeaderFailure extends OppoHeaderState {
  final String error;
  const OppoHeaderFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoHeaderFailure { error: $error }';
}
