import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_lead_activity_event.dart';
part 'oppo_lead_activity_state.dart';

class OppoLeadActivityBloc
    extends Bloc<OppoLeadActivityEvent, OppoLeadActivityState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoLeadActivityBloc({this.oppoRepository})
      : super(OppoLeadActivityInitial());

  @override
  Stream<OppoLeadActivityState> mapEventToState(
    OppoLeadActivityEvent event,
  ) async* {
    if (event is OppoLeadActivityPressed) {
      yield OppoLeadActivityInitial();
      yield OppoLeadActivityInProgress();
      try {
        List<OppoLeadActivityModel> getData =
            await oppoRepository.getOppoLeadActivity(
                await environment.getToken(),
                event.leadID,
                event.activityType,
                event.fromDate,
                event.toDate);
        yield OppoLeadActivitySuccess(stateData: getData);
      } catch (_) {
        yield OppoLeadActivityFailure(error: _.toString());
      }
    }
  }
}
