part of 'oppo_lead_activity_bloc.dart';

abstract class OppoLeadActivityEvent extends Equatable {
  const OppoLeadActivityEvent();

  @override
  List<Object> get props => [];
}

class OppoLeadActivityPressed extends OppoLeadActivityEvent {
  final String leadID;
  final String activityType;
  final String fromDate;
  final String toDate;

  OppoLeadActivityPressed(
      {this.leadID, this.activityType, this.fromDate, this.toDate});

  @override
  List<Object> get props => [leadID];

  String toString() => 'OppoLeadActivityPressed : {divisionID : $leadID}';
}
