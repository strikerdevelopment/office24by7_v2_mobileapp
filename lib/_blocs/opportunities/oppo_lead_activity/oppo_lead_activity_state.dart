part of 'oppo_lead_activity_bloc.dart';

abstract class OppoLeadActivityState extends Equatable {
  const OppoLeadActivityState();

  @override
  List<Object> get props => [];
}

class OppoLeadActivityInitial extends OppoLeadActivityState {}

class OppoLeadActivityInProgress extends OppoLeadActivityState {}

class OppoLeadActivitySuccess extends OppoLeadActivityState {
  final List<OppoLeadActivityModel> stateData;

  OppoLeadActivitySuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoLeadActivitySuccess { stateData: ${stateData.length} }';
}

class OppoLeadActivityFailure extends OppoLeadActivityState {
  final String error;
  const OppoLeadActivityFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoLeadActivityFailure { error: $error }';
}
