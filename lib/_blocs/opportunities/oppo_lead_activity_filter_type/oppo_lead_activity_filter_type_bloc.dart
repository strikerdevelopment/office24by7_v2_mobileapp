import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_lead_activity_filter_type_event.dart';
part 'oppo_lead_activity_filter_type_state.dart';

class OppoLeadActivityFilterTypeBloc extends Bloc<
    OppoLeadActivityFilterTypeEvent, OppoLeadActivityFilterTypeState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoLeadActivityFilterTypeBloc({this.oppoRepository})
      : super(OppoLeadActivityFilterTypeInitial());

  @override
  Stream<OppoLeadActivityFilterTypeState> mapEventToState(
    OppoLeadActivityFilterTypeEvent event,
  ) async* {
    if (event is OppoLeadActivityFilterTypePressed) {
      yield OppoLeadActivityFilterTypeInitial();
      try {
        List<dynamic> getData = await oppoRepository
            .getOppoLeadActivityFilterType(await environment.getToken());
        yield OppoLeadActivityFilterTypeSuccess(stateData: getData);
      } catch (_) {
        yield OppoLeadActivityFilterTypeFailure(error: _.toString());
      }
    }
  }
}
