part of 'oppo_lead_activity_filter_type_bloc.dart';

abstract class OppoLeadActivityFilterTypeEvent extends Equatable {
  const OppoLeadActivityFilterTypeEvent();

  @override
  List<Object> get props => [];
}

class OppoLeadActivityFilterTypePressed
    extends OppoLeadActivityFilterTypeEvent {}
