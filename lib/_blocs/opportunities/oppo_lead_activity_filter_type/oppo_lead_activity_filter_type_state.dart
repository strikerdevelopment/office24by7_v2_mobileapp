part of 'oppo_lead_activity_filter_type_bloc.dart';

abstract class OppoLeadActivityFilterTypeState extends Equatable {
  const OppoLeadActivityFilterTypeState();

  @override
  List<Object> get props => [];
}

class OppoLeadActivityFilterTypeInitial
    extends OppoLeadActivityFilterTypeState {}

class OppoLeadActivityFilterTypeSuccess
    extends OppoLeadActivityFilterTypeState {
  final List<dynamic> stateData;

  OppoLeadActivityFilterTypeSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'OppoLeadActivityFilterTypeSuccess { stateData: ${stateData.length} }';
}

class OppoLeadActivityFilterTypeFailure
    extends OppoLeadActivityFilterTypeState {
  final String error;
  const OppoLeadActivityFilterTypeFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoLeadActivityFilterTypeFailure { error: $error }';
}
