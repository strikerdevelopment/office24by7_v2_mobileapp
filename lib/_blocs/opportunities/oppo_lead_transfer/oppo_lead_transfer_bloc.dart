import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_lead_transfer_event.dart';
part 'oppo_lead_transfer_state.dart';

class OppoLeadTransferBloc
    extends Bloc<OppoLeadTransferEvent, OppoLeadTransferState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoLeadTransferBloc({this.oppoRepository})
      : super(OppoLeadTransferInitial());

  @override
  Stream<OppoLeadTransferState> mapEventToState(
    OppoLeadTransferEvent event,
  ) async* {
    if (event is OppoLeadTransferPressed) {
      yield OppoLeadTransferInProgress();
      try {
        String getData = await oppoRepository.getOppoLeadTransfer(
            await environment.getToken(), event.eventData);
        yield OppoLeadTransferSuccess(stateData: getData);
      } catch (_) {
        yield OppoLeadTransferFailure(error: _.toString());
      }
    }
  }
}
