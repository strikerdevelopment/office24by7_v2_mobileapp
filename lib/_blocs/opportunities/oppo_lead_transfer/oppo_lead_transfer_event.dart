part of 'oppo_lead_transfer_bloc.dart';

abstract class OppoLeadTransferEvent extends Equatable {
  const OppoLeadTransferEvent();

  @override
  List<Object> get props => [];
}

class OppoLeadTransferPressed extends OppoLeadTransferEvent {
  final Map eventData;
  OppoLeadTransferPressed({this.eventData});
}
