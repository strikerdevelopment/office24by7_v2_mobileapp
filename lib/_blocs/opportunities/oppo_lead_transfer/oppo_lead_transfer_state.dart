part of 'oppo_lead_transfer_bloc.dart';

abstract class OppoLeadTransferState extends Equatable {
  const OppoLeadTransferState();

  @override
  List<Object> get props => [];
}

class OppoLeadTransferInitial extends OppoLeadTransferState {}

class OppoLeadTransferInProgress extends OppoLeadTransferState {}

class OppoLeadTransferSuccess extends OppoLeadTransferState {
  final String stateData;

  OppoLeadTransferSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoLeadTransferFailure extends OppoLeadTransferState {
  final String error;
  const OppoLeadTransferFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
