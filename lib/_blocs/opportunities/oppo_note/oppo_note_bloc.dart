import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';

part 'oppo_note_event.dart';
part 'oppo_note_state.dart';

class OppoNoteBloc extends Bloc<OppoNoteEvent, OppoNoteState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoNoteBloc({this.oppoRepository}) : super(OppoNoteInitial());

  @override
  Stream<OppoNoteState> mapEventToState(
    OppoNoteEvent event,
  ) async* {
    if (event is OppoNotePressed) {
      yield OppoNoteInProgress();
      try {
        String getData = await oppoRepository.getOppoCreateNote(
            await environment.getToken(), event.eventData);
        yield OppoNoteSuccess(stateData: getData);
      } catch (_) {
        yield OppoNoteFailure(error: _.toString());
      }
    }
  }
}
