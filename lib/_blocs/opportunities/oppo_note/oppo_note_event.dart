part of 'oppo_note_bloc.dart';

abstract class OppoNoteEvent extends Equatable {
  const OppoNoteEvent();

  @override
  List<Object> get props => [];
}

class OppoNotePressed extends OppoNoteEvent {
  final Map eventData;
  OppoNotePressed({this.eventData});
}
