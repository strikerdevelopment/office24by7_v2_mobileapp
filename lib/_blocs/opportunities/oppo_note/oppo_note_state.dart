part of 'oppo_note_bloc.dart';

abstract class OppoNoteState extends Equatable {
  const OppoNoteState();

  @override
  List<Object> get props => [];
}

class OppoNoteInitial extends OppoNoteState {}

class OppoNoteInProgress extends OppoNoteState {}

class OppoNoteSuccess extends OppoNoteState {
  final String stateData;

  OppoNoteSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() =>
      'ContactEmailIdSuccess { stateData: ${stateData.length} }';
}

class OppoNoteFailure extends OppoNoteState {
  final String error;
  const OppoNoteFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ContactEmailIdFailure { error: $error }';
}
