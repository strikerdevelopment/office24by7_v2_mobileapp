import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'oppo_sender_id_event.dart';
part 'oppo_sender_id_state.dart';

class OppoSenderIdBloc extends Bloc<OppoSenderIdEvent, OppoSenderIdState> {
  final OppoRepository oppoRepository;
  final Environment environment = Environment();

  OppoSenderIdBloc({this.oppoRepository}) : super(OppoSenderIdInitial());

  @override
  Stream<OppoSenderIdState> mapEventToState(
    OppoSenderIdEvent event,
  ) async* {
    if (event is OppoSenderIdPressed) {
      yield OppoSenderIdInitial();
      try {
        List<OppoSenderId> getData =
            await oppoRepository.getOppoSenderId(await environment.getToken());
        yield OppoSenderIdSuccess(stateData: getData);
      } catch (_) {
        yield OppoSenderIdFailure(error: _.toString());
      }
    }
  }
}
