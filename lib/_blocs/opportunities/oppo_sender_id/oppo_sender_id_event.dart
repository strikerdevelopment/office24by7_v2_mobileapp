part of 'oppo_sender_id_bloc.dart';

abstract class OppoSenderIdEvent extends Equatable {
  const OppoSenderIdEvent();

  @override
  List<Object> get props => [];
}

class OppoSenderIdPressed extends OppoSenderIdEvent {}
