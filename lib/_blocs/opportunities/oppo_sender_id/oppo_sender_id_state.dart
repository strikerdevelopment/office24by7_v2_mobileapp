part of 'oppo_sender_id_bloc.dart';

abstract class OppoSenderIdState extends Equatable {
  const OppoSenderIdState();

  @override
  List<Object> get props => [];
}

class OppoSenderIdInitial extends OppoSenderIdState {}

class OppoSenderIdSuccess extends OppoSenderIdState {
  final List<OppoSenderId> stateData;

  OppoSenderIdSuccess({this.stateData});

  @override
  List<Object> get props => [stateData];

  @override
  String toString() => 'OppoSenderIdSuccess { stateData: ${stateData.length} }';
}

class OppoSenderIdFailure extends OppoSenderIdState {
  final String error;
  const OppoSenderIdFailure({this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OppoSenderIdFailure { error: $error }';
}
