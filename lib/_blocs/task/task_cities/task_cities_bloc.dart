import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_cities.dart';

part 'task_cities_event.dart';
part 'task_cities_state.dart';

class TaskCitiesBloc extends Bloc<TaskCitiesEvent, TaskCitiesState> {
  TaskRepository taskRepository;
  final Environment environment = Environment();
  TaskCitiesBloc({this.taskRepository}) : super(TaskCitiesInitial());

  @override
  Stream<TaskCitiesState> mapEventToState(
    TaskCitiesEvent event,
  ) async* {
    if (event is TaskCitiesPressed) {
      yield TaskCitiesInProgress();
      try {
        final List<TaskCities> getdata = await taskRepository.getTaskCities(
            await environment.getToken(), event.state);
        yield TaskCitiesSuccess(statedata: getdata);
      } catch (_) {
        yield TaskCitiesFailure(error: _.toString());
      }
    }
  }
}
