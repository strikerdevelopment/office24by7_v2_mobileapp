part of 'task_cities_bloc.dart';

abstract class TaskCitiesEvent extends Equatable {
  const TaskCitiesEvent();

  @override
  List<Object> get props => [];
}

class TaskCitiesPressed extends TaskCitiesEvent {
  final int state;
  const TaskCitiesPressed({this.state});

  @override
  List<Object> get props => [];
}
