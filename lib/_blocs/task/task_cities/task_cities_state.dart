part of 'task_cities_bloc.dart';

abstract class TaskCitiesState extends Equatable {
  const TaskCitiesState();

  @override
  List<Object> get props => [];
}

class TaskCitiesInitial extends TaskCitiesState {}

class TaskCitiesInProgress extends TaskCitiesState {}

class TaskCitiesSuccess extends TaskCitiesState {
  final List<TaskCities> statedata;
  TaskCitiesSuccess({this.statedata});
}

class TaskCitiesFailure extends TaskCitiesState {
  final String error;
  TaskCitiesFailure({this.error});
}
