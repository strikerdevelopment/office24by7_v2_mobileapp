import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_countries.dart';

part 'task_countries_event.dart';
part 'task_countries_state.dart';

class TaskCountriesBloc extends Bloc<TaskCountriesEvent, TaskCountriesState> {
  TaskRepository taskRepository;
  final Environment environment = Environment();
  TaskCountriesBloc({this.taskRepository}) : super(TaskCountriesInitial());

  @override
  Stream<TaskCountriesState> mapEventToState(
    TaskCountriesEvent event,
  ) async* {
    if (event is TaskCountriesPressed) {
      yield TaskCountriesInProgress();
      try {
        final List<TaskCountries> getdata = await taskRepository
            .getTaskCountries(await environment.getToken(), event.countryId);
        yield TaskCountriesSuccess(statedata: getdata);
      } catch (_) {
        yield TaskCountriesFailure(error: _.toString());
      }
    }
  }
}
