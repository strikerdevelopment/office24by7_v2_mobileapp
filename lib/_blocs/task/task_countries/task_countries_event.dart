part of 'task_countries_bloc.dart';

abstract class TaskCountriesEvent extends Equatable {
  const TaskCountriesEvent();

  @override
  List<Object> get props => [];
}

class TaskCountriesPressed extends TaskCountriesEvent {
  final int countryId;
  const TaskCountriesPressed({this.countryId});

  @override
  List<Object> get props => [];
}
