part of 'task_countries_bloc.dart';

abstract class TaskCountriesState extends Equatable {
  const TaskCountriesState();

  @override
  List<Object> get props => [];
}

class TaskCountriesInitial extends TaskCountriesState {}

class TaskCountriesInProgress extends TaskCountriesState {}

class TaskCountriesSuccess extends TaskCountriesState {
  final List<TaskCountries> statedata;
  TaskCountriesSuccess({this.statedata});
}

class TaskCountriesFailure extends TaskCountriesState {
  final String error;
  TaskCountriesFailure({this.error});
}
