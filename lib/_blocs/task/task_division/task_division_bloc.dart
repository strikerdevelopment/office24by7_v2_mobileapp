import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_data/repositories/task_repository.dart';
import 'package:office24by7_v2/_models/task_model/task_division.dart';

part 'task_division_event.dart';
part 'task_division_state.dart';

class TaskDivisionBloc extends Bloc<TaskDivisionEvent, TaskDivisionState> {
  TaskRepository taskRepository;
  final Environment environment = Environment();
  TaskDivisionBloc({this.taskRepository}) : super(TaskDivisionInitial());

  @override
  Stream<TaskDivisionState> mapEventToState(
    TaskDivisionEvent event,
  ) async* {
    if (event is TaskDivisionPressed) {
      yield TaskDivisionInProgress();
      try {
        final List<TaskDivision> getdata =
            await taskRepository.getTaskDivision(await environment.getToken());
        yield TaskDivisionSuccess(statedata: getdata);
      } catch (_) {
        yield TaskDivisionFailure(error: _.toString());
      }
    }
  }
}
