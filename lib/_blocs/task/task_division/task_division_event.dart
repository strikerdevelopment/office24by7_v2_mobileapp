part of 'task_division_bloc.dart';

abstract class TaskDivisionEvent extends Equatable {
  const TaskDivisionEvent();

  @override
  List<Object> get props => [];
}

class TaskDivisionPressed extends TaskDivisionEvent {
  const TaskDivisionPressed();

  @override
  List<Object> get props => [];
}
