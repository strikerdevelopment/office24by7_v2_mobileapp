part of 'task_division_bloc.dart';

abstract class TaskDivisionState extends Equatable {
  const TaskDivisionState();

  @override
  List<Object> get props => [];
}

class TaskDivisionInitial extends TaskDivisionState {}

class TaskDivisionInProgress extends TaskDivisionState {}

class TaskDivisionSuccess extends TaskDivisionState {
  final List<TaskDivision> statedata;
  TaskDivisionSuccess({this.statedata});
}

class TaskDivisionFailure extends TaskDivisionState {
  final String error;
  TaskDivisionFailure({this.error});
}
