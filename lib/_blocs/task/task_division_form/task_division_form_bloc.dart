import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_division_form.dart';

part 'task_division_form_event.dart';
part 'task_division_form_state.dart';

class TaskDivisionFormBloc
    extends Bloc<TaskDivisionFormEvent, TaskDivisionFormState> {
  final Environment environment = Environment();
  TaskRepository taskRepository;
  TaskDivisionFormBloc({this.taskRepository})
      : super(TaskDivisionFormInitial());

  @override
  Stream<TaskDivisionFormState> mapEventToState(
    TaskDivisionFormEvent event,
  ) async* {
    if (event is TaskDivisionFormPressed) {
      yield TaskDivisionFormInProgress();
      try {
        final List<TaskDivisionForm> getdata =
            await taskRepository.getTaskDivisionForm(
                await environment.getToken(), event.divisionId);
        yield TaskDivisionFormSuccess(statedata: getdata);
      } catch (_) {
        yield TaskDivisionFormFailure(error: _.toString());
      }
    }
  }
}
