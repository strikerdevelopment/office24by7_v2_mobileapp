part of 'task_division_form_bloc.dart';

abstract class TaskDivisionFormEvent extends Equatable {
  const TaskDivisionFormEvent();

  @override
  List<Object> get props => [];
}

class TaskDivisionFormPressed extends TaskDivisionFormEvent {
  final int divisionId;
  const TaskDivisionFormPressed({this.divisionId});

  @override
  List<Object> get props => [];
}
