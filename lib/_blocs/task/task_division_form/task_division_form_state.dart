part of 'task_division_form_bloc.dart';

abstract class TaskDivisionFormState extends Equatable {
  const TaskDivisionFormState();

  @override
  List<Object> get props => [];
}

class TaskDivisionFormInitial extends TaskDivisionFormState {}

class TaskDivisionFormInProgress extends TaskDivisionFormState {}

class TaskDivisionFormSuccess extends TaskDivisionFormState {
  final List<TaskDivisionForm> statedata;
  TaskDivisionFormSuccess({this.statedata});
}

class TaskDivisionFormFailure extends TaskDivisionFormState {
  final String error;
  TaskDivisionFormFailure({this.error});
}
