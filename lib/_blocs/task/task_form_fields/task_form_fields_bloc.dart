import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/task_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_form_fields.dart';

part 'task_form_fields_event.dart';
part 'task_form_fields_state.dart';

class TaskFormFieldsBloc
    extends Bloc<TaskFormFieldsEvent, TaskFormFieldsState> {
  final Environment environment = Environment();
  TaskRepository taskRepository;
  TaskFormFieldsBloc({this.taskRepository}) : super(TaskFormFieldsInitial());

  @override
  Stream<TaskFormFieldsState> mapEventToState(
    TaskFormFieldsEvent event,
  ) async* {
    if (event is TaskFormFieldsPressed) {
      yield TaskFormFieldsInProgress();
      try {
        final List<TaskFormFields> getdata = await taskRepository
            .getTaskFormFields(await environment.getToken(), event.formId);
        yield TaskFormFieldsSuccess(statedata: getdata);
      } catch (_) {
        yield TaskFormFieldsFailure(error: _.toString());
      }
    }
  }
}
