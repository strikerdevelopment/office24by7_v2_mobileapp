part of 'task_form_fields_bloc.dart';

abstract class TaskFormFieldsEvent extends Equatable {
  const TaskFormFieldsEvent();

  @override
  List<Object> get props => [];
}

class TaskFormFieldsPressed extends TaskFormFieldsEvent {
  final int formId;
  const TaskFormFieldsPressed({this.formId});

  @override
  List<Object> get props => [];
}
