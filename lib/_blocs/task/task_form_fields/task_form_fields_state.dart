part of 'task_form_fields_bloc.dart';

abstract class TaskFormFieldsState extends Equatable {
  const TaskFormFieldsState();

  @override
  List<Object> get props => [];
}

class TaskFormFieldsInitial extends TaskFormFieldsState {}

class TaskFormFieldsInProgress extends TaskFormFieldsState {}

class TaskFormFieldsSuccess extends TaskFormFieldsState {
  final List<TaskFormFields> statedata;
  TaskFormFieldsSuccess({this.statedata});
}

class TaskFormFieldsFailure extends TaskFormFieldsState {
  final String error;
  TaskFormFieldsFailure({this.error});
}
