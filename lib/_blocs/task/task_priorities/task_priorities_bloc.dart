import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_priorities.dart';

part 'task_priorities_event.dart';
part 'task_priorities_state.dart';

class TaskPrioritiesBloc
    extends Bloc<TaskPrioritiesEvent, TaskPrioritiesState> {
  final Environment environment = Environment();
  TaskRepository taskRepository;
  TaskPrioritiesBloc({this.taskRepository}) : super(TaskPrioritiesInitial());

  @override
  Stream<TaskPrioritiesState> mapEventToState(
    TaskPrioritiesEvent event,
  ) async* {
    if (event is TaskPrioritiesPressed) {
      yield TaskPrioritiesInProgress();
      try {
        final List<TaskPriorities> getdata = await taskRepository
            .getTaskPriorities(await environment.getToken(), event.divisionId);
        yield TaskPrioritiesSuccess(statedata: getdata);
      } catch (_) {
        yield TaskPrioritiesFailure(error: _.toString());
      }
    }
  }
}
