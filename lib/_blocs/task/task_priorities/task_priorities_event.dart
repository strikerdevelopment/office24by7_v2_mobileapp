part of 'task_priorities_bloc.dart';

abstract class TaskPrioritiesEvent extends Equatable {
  const TaskPrioritiesEvent();

  @override
  List<Object> get props => [];
}

class TaskPrioritiesPressed extends TaskPrioritiesEvent {
  final int divisionId;
  const TaskPrioritiesPressed({this.divisionId});

  @override
  List<Object> get props => [];
}
