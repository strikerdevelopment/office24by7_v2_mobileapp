part of 'task_priorities_bloc.dart';

abstract class TaskPrioritiesState extends Equatable {
  const TaskPrioritiesState();

  @override
  List<Object> get props => [];
}

class TaskPrioritiesInitial extends TaskPrioritiesState {}

class TaskPrioritiesInProgress extends TaskPrioritiesState {}

class TaskPrioritiesSuccess extends TaskPrioritiesState {
  final List<TaskPriorities> statedata;
  TaskPrioritiesSuccess({this.statedata});
}

class TaskPrioritiesFailure extends TaskPrioritiesState {
  final String error;
  TaskPrioritiesFailure({this.error});
}
