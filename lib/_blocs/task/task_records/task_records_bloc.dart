import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/task_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

part 'task_records_event.dart';
part 'task_records_state.dart';

class TaskRecordsBloc extends Bloc<TaskRecordsEvent, TaskRecordsState> {
  TaskRepository taskRepository;
  final Environment environment = Environment();
  TaskRecordsBloc({this.taskRepository}) : super(TaskRecordsInitial());

  @override
  Stream<TaskRecordsState> mapEventToState(
    TaskRecordsEvent event,
  ) async* {
    if (event is TaskRecordsPressed) {
      yield TaskRecordsInProgress();
      try {
        final TaskDataModel getdata = await taskRepository.getTaskRecords(
            await environment.getToken(),
            event.displayType,
            event.searchStatusId,
            event.searchDateFrom,
            event.searchDateTo,
            event.searchDuration,
            event.displayAs,
            event.searchassignedTo,
            event.globalSearchValue,
            event.offset,
            event.limit);
        yield TaskRecordsSuccess(statedata: getdata);
      } catch (_) {
        yield TaskRecordsFailure(error: _.toString());
      }
    }
  }
}
