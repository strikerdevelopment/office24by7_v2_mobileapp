part of 'task_records_bloc.dart';

abstract class TaskRecordsEvent extends Equatable {
  const TaskRecordsEvent();

  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class TaskRecordsPressed extends TaskRecordsEvent {
  final String displayType;
  final String searchStatusId;
  final String searchDateFrom;
  final String searchDateTo;
  final String searchDuration;
  final String displayAs;
  final String searchassignedTo;
  final String globalSearchValue;
  final String offset;
  final String limit;
  const TaskRecordsPressed(
      {this.displayType,
      this.searchStatusId,
      this.searchDateFrom,
      this.searchDateTo,
      this.searchDuration,
      this.displayAs,
      this.globalSearchValue,
      this.limit,
      this.offset,
      this.searchassignedTo});

  @override
  List<Object> get props => [];
}
