part of 'task_records_bloc.dart';

abstract class TaskRecordsState extends Equatable {
  const TaskRecordsState();

  @override
  List<Object> get props => [];

  get statedata => null;
}

class TaskRecordsInitial extends TaskRecordsState {}

class TaskRecordsInProgress extends TaskRecordsState {}

class TaskRecordsSuccess extends TaskRecordsState {
  final TaskDataModel statedata;
  TaskRecordsSuccess({this.statedata});
}

class TaskRecordsFailure extends TaskRecordsState {
  final String error;
  TaskRecordsFailure({this.error});
}
