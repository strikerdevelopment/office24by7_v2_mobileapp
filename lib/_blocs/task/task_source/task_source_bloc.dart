import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/task_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_source.dart';

part 'task_source_event.dart';
part 'task_source_state.dart';

class TaskSourceBloc extends Bloc<TaskSourceEvent, TaskSourceState> {
  TaskRepository taskRepository;
  final Environment environment = Environment();
  TaskSourceBloc({this.taskRepository}) : super(TaskSourceInitial());

  @override
  Stream<TaskSourceState> mapEventToState(
    TaskSourceEvent event,
  ) async* {
    if (event is TaskSourcePressed) {
      yield TaskSourceInProgress();
      try {
        final List<TaskSource> getdata = await taskRepository.getTaskSource(
            await environment.getToken(), event.divisionId);
        yield TaskSourceSuccess(statedata: getdata);
      } catch (_) {
        yield TaskSourceFailure(error: _.toString());
      }
    }
  }
}
