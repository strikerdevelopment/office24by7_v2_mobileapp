part of 'task_source_bloc.dart';

abstract class TaskSourceEvent extends Equatable {
  const TaskSourceEvent();

  @override
  List<Object> get props => [];
}

class TaskSourcePressed extends TaskSourceEvent {
  final int divisionId;
  const TaskSourcePressed({this.divisionId});

  @override
  List<Object> get props => [];
}
