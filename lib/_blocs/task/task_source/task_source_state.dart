part of 'task_source_bloc.dart';

abstract class TaskSourceState extends Equatable {
  const TaskSourceState();

  @override
  List<Object> get props => [];
}

class TaskSourceInitial extends TaskSourceState {}

class TaskSourceInProgress extends TaskSourceState {}

class TaskSourceSuccess extends TaskSourceState {
  final List<TaskSource> statedata;
  TaskSourceSuccess({this.statedata});
}

class TaskSourceFailure extends TaskSourceState {
  final String error;
  TaskSourceFailure({this.error});
}
