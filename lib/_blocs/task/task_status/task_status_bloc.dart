import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/task_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_division_status.dart';

part 'task_status_event.dart';
part 'task_status_state.dart';

class TaskStatusBloc extends Bloc<TaskStatusEvent, TaskStatusState> {
  TaskRepository taskRepository;
  final Environment environment = Environment();
  TaskStatusBloc({this.taskRepository}) : super(TaskStatusInitial());

  @override
  Stream<TaskStatusState> mapEventToState(
    TaskStatusEvent event,
  ) async* {
    if (event is TaskStatusPressed) {
      yield TaskStatusInProgress();
      try {
        final List<TaskDivisionStatus> getdata = await taskRepository
            .getTaskDivisionStatus(await environment.getToken());
        yield TaskStatusSuccess(statedata: getdata);
      } catch (_) {
        yield TaskStatusFailure(error: _.toString());
      }
    }
  }
}
