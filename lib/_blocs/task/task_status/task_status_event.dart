part of 'task_status_bloc.dart';

abstract class TaskStatusEvent extends Equatable {
  const TaskStatusEvent();

  @override
  List<Object> get props => [];
}

class TaskStatusPressed extends TaskStatusEvent {
  const TaskStatusPressed();

  @override
  List<Object> get props => [];
}
