part of 'task_status_bloc.dart';

abstract class TaskStatusState extends Equatable {
  const TaskStatusState();

  @override
  List<Object> get props => [];
}

class TaskStatusInitial extends TaskStatusState {}

class TaskStatusInProgress extends TaskStatusState {}

class TaskStatusSuccess extends TaskStatusState {
  final List<TaskDivisionStatus> statedata;
  TaskStatusSuccess({this.statedata});
}

class TaskStatusFailure extends TaskStatusState {
  final String error;
  TaskStatusFailure({this.error});
}
