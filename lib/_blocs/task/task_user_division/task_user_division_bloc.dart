import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:office24by7_v2/_data/repositories/task_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/task_model/task_user_division.dart';

part 'task_user_division_event.dart';
part 'task_user_division_state.dart';

class TaskUserDivisionBloc
    extends Bloc<TaskUserDivisionEvent, TaskUserDivisionState> {
  TaskRepository taskRepository;
  final Environment environment = Environment();
  TaskUserDivisionBloc({this.taskRepository})
      : super(TaskUserDivisionInitial());

  @override
  Stream<TaskUserDivisionState> mapEventToState(
    TaskUserDivisionEvent event,
  ) async* {
    if (event is TaskUserDivisionPressed) {
      yield TaskUserDivisionInProgress();
      try {
        final List<TaskUserDivision> getdata =
            await taskRepository.getTaskUserDivision(
                await environment.getToken(), event.divisionId);
        yield TaskUserDivisionSuccess(statedata: getdata);
      } catch (_) {
        yield TaskUserDivisionFailure(error: _.toString());
      }
    }
  }
}
