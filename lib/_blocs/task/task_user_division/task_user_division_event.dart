part of 'task_user_division_bloc.dart';

abstract class TaskUserDivisionEvent extends Equatable {
  const TaskUserDivisionEvent();

  @override
  List<Object> get props => [];
}

class TaskUserDivisionPressed extends TaskUserDivisionEvent {
  final int divisionId;
  const TaskUserDivisionPressed({this.divisionId});

  @override
  List<Object> get props => [];
}
