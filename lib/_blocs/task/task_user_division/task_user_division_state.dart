part of 'task_user_division_bloc.dart';

abstract class TaskUserDivisionState extends Equatable {
  const TaskUserDivisionState();

  @override
  List<Object> get props => [];
}

class TaskUserDivisionInitial extends TaskUserDivisionState {}

class TaskUserDivisionInProgress extends TaskUserDivisionState {}

class TaskUserDivisionSuccess extends TaskUserDivisionState {
  final List<TaskUserDivision> statedata;
  TaskUserDivisionSuccess({this.statedata});
}

class TaskUserDivisionFailure extends TaskUserDivisionState {
  final String error;
  TaskUserDivisionFailure({this.error});
}
