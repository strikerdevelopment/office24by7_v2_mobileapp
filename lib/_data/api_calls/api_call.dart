import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

var getDate = DateTime.now();
List<APICall> lstAPICalls = [];

//enum APIStatus { NOTINITIATED, WAITING, INPROGRESS, COMPLETED, FAILED }

// ignore: must_be_immutable
class APICall extends Equatable {
  final String url;
  final String method;
  Object headers;
  Object body;
  int apiCallTime;
  DateTime apiStartTime;
  DateTime apiEndTime;
  DateTime updatedTime;
  //APIStatus status;
  Function successCallBack;
  http.Response apiResponse;
  int sortOrder;

  APICall({
    @required this.method,
    @required this.url,
    this.headers,
    this.body,
    this.apiCallTime,
    this.updatedTime,
    //this.status = APIStatus.NOTINITIATED,
    this.successCallBack,
  }) : assert(method != null) {
    this.sortOrder = lstAPICalls.length + 1;
  }

  @override
  List<Object> get props => [method, url, headers, body, apiCallTime];

  Future<dynamic> getMethod(APICall objAPI) async {
    return await http.get(Uri.parse(objAPI.url));
  }

  Future<dynamic> postMethod(APICall objAPI) async {
    if (objAPI.body != null) {
      return await http.post(
        Uri.parse(objAPI.url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: objAPI.body,
      );
    } else {
      final result = await http.post(Uri.parse(objAPI.url));
      return result;
    }
  }

  Future<dynamic> deleteMethod(APICall objAPI) async {
    return await http.delete(Uri.parse(objAPI.url));
  }

  Future<dynamic> updateMethod(APICall objAPI) async {
    return await http.post(Uri.parse(objAPI.url));
  }

  Future<dynamic> apiCall(APICall objAPI) async {
    //this.status = APIStatus.INPROGRESS;
    try {
      switch (method) {
        case "GET":
          apiResponse = await getMethod(objAPI);
          break;
        case "POST":
          apiResponse = await postMethod(objAPI);
          break;
        case "DELETE":
          apiResponse = await deleteMethod(objAPI);
          break;
        case "UPDATE":
          apiResponse = await updateMethod(objAPI);
          break;
      }
    } catch (ex) {
      throw ("Seems there is some connectivity issue. Please try again. ");
    }

    //statusCheck(apiResponse);
    if (apiResponse.statusCode == 200 || apiResponse.statusCode == 201) {
      //this.status = APIStatus.COMPLETED;
      if (this.successCallBack != null) {
        apiEndTime = new DateTime.now();
        print("End Time:-  $apiEndTime");
        this.apiCallTime = apiEndTime.difference(apiStartTime).inMilliseconds;
        print("API Call Time ==>  $apiCallTime");
        lstAPICalls.add(this);
        //print(lstAPICalls);
        return this.successCallBack(apiResponse, this);
      }
    } else {
      if (apiResponse.statusCode == 404 ||
          apiResponse.statusCode == 500 ||
          apiResponse.statusCode == 400 ||
          apiResponse.statusCode == 401 ||
          apiResponse.statusCode == 405 ||
          apiResponse.statusCode == 403) {
        throw ("Seems there is some connectivity issue. Please try again. ");
      } else {
        print("${apiResponse.statusCode} => ${apiResponse.body}");
        final data = json.decode(apiResponse.body);
        if (data['statusMessage'] != null) {
          throw (data['response']);
        } else if (data['response'] != null) {
          throw (data['response']['statusMessage']);
        }
        throw ("$data");
      }
      //Implement error handling code
    }
    //checkAndInitiateNextAPI();
    //return apiResponse;
  }

  // void checkAndInitiateNextAPI() {
  //   var apis =
  //       lstAPICalls.where((api) => api.status == APIStatus.WAITING).toList();
  //   if (apis.length > 0) {
  //     //Get the old APICall object and initiate the API call.
  //   }
  // }

  initiateAPI() async {
    apiStartTime = new DateTime.now();
    // print("StartTime:- $apiStartTime");
    return await this.apiCall(this);

    // if (lstAPICalls
    //         .where((api) => api.status == APIStatus.INPROGRESS)
    //         .toList()
    //         .length ==
    //     0) {
    //   lstAPICalls.add(this);
    //   return await this.apiCall(this);
    // } else {
    //   this.status = APIStatus.WAITING;
    //   //lstAPICalls.add(this);
    //   print("Your ignored request: " + this.url + "\n");
    // }
  }

  // statusCheck(response) {
  //   if (response.statusCode == 200) {
  //     this.status = APIStatus.COMPLETED;
  //     if (this.successCallBack != null) {
  //       this.successCallBack(response, this);
  //     }
  //   } else {
  //     throw (response.statusMessage);
  //     //Implement error handling code
  //   }
  // }
}
