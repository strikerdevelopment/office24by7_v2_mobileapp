import 'dart:async';
import 'dart:convert';
import 'package:office24by7_v2/_data/api_calls/api_call.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

class CallogAPIClient {
  final baseUrl = Environment.baseUrl;
  CallogAPIClient();

  // Click to call data provider
  Future<String> clicktoCall(String token, String phoneNumber, String callerId,
      String leadId, String contactId, String callId) async {
    final url = '$baseUrl/mobileapp/agentdialing/clicktocallapi';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "phone_number": phoneNumber,
      "caller_id": callerId,
      "lead_id": leadId,
      "contact_id": contactId,
      "call_id": callId,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successClicktoCall,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successClicktoCall(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return jsonData["response"];
  }

  // Caller IDs data provider
  Future<List<CallerIdModel>> callerIds(String token) async {
    final url = '$baseUrl/mobileapp/agentapi/callerids';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCallerIds,
    );

    List<CallerIdModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<CallerIdModel> successCallerIds(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return callerIdModelFromJson(json.encode(jsonData["CallerIds"]));
  }

  // Calls log data provider
  Future<List<CallsLogModel>> callsLog(
      String token, int limit, int offset) async {
    final url = '$baseUrl/mobileapp/agentapi/callslog';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "limit": limit,
      "offset": offset,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCallsLog,
    );

    List<CallsLogModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<CallsLogModel> successCallsLog(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return callsLogModelFromJson(json.encode(jsonData["MyCallsLog"]));
  }

  // Calls log data provider
  Future<List<CallsLogHistoryModel>> callsLogHistory(
      String token, String callerPhoneNumber, int limit, int offset) async {
    final url = '$baseUrl/mobileapp/agentapi/eachcallslog';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "caller_phone_number": callerPhoneNumber,
      "limit": limit,
      "offset": offset,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCallsLogHistory,
    );

    List<CallsLogHistoryModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<CallsLogHistoryModel> successCallsLogHistory(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return callsLogHistoryModelFromJson(
        json.encode(jsonData["CallsLogHistory"]));
  }
}
