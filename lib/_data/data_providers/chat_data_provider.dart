import 'dart:async';
import 'dart:convert';
import 'package:office24by7_v2/_data/api_calls/api_call.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:dio/dio.dart';

class ChatAPIClient {
  final baseUrl = Environment.baseUrl;
  ChatAPIClient();

  // Add Live Chat Session data provider
  Future<String> addLiveChatSession(String token, String session) async {
    final url = '$baseUrl/chat/Chat_common/add_users_to_chat_session';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "session_name": session,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successAddLiveChatSession,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successAddLiveChatSession(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return jsonData["response"];
  }

  Future<List<ChatHistoryModel>> announChatHistory(String token, String groupId,
      String groupUniqueId, String groupType, int offset) async {
    final url = '$baseUrl/chat/Chat_common/getGroup_chat_history_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType,
      "offset": offset
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successAnnounChatHistory,
    );

    List<ChatHistoryModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<ChatHistoryModel> successAnnounChatHistory(response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    if (jsonData['statusCode'] == 201) {
      throw (jsonData['response']);
    }
    return chatHistoryModelFromJson(
        json.encode(jsonData["response"]['chat_history']));
  }

  // send text message announement data provider
  Future<String> sendMsgAnnouce(
      String token,
      String groupId,
      String groupUniqueId,
      String groupType,
      String msg,
      String msgReferenceId) async {
    final url = '$baseUrl/chat/Chat_common/sending_group_chat';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType,
      "message": msg,
      "type_of_chat": "client",
      "file": "",
      "msg_reference_id": msgReferenceId
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successSendMsgAnnouce,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successSendMsgAnnouce(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return jsonData["response"]["status"];
  }

  // upload_group_chat announement data provider
  Future<String> uploadFilesAnnouce(
      String token,
      String groupId,
      String groupUniqueId,
      String groupType,
      List getFiles,
      String msgReferenceId) async {
    final url = '$baseUrl/chat/Chat_common/upload_group_chat';

    FormData formData = new FormData.fromMap({
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType,
      'file[]':
          getFiles.map((path) => MultipartFile.fromFileSync(path)).toList(),
      "msg_reference_id": msgReferenceId,
    });

    try {
      Response response = await Dio().post(url, data: formData);
      print(response);
      if (response.statusCode != 200) {
        throw ("Failed to Send Files.");
      }
      return response.statusMessage;
    } catch (e) {
      print(e);
      throw (e);
    }
  }

  // delete_group_messages announement data provider
  Future<String> deleteMsgAnnouce(String token, List chatTextLogId,
      String groupId, String groupUniqueId, String groupType) async {
    final url = '$baseUrl/chat/Chat_common/delete_group_messages';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "chat_text_log_id": chatTextLogId,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successDeleteMsgAnnouce,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successDeleteMsgAnnouce(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return jsonData["response"];
  }

  // send text message announement data provider
  Future<ChatUsersModel> allUsers(String token, String type, String searchTerm,
      String isHierarchyUsers) async {
    final url = type == 'search'
        ? '$baseUrl/chat/Chat_common/get_users'
        : '$baseUrl/chat/Chat_common/getUsers_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "search_term": searchTerm,
      "is_hierarchy_users": isHierarchyUsers
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successAllUsers,
    );

    ChatUsersModel data = await apiCall.initiateAPI();
    return data;
  }

  ChatUsersModel successAllUsers(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return chatUsersModelFromJson(json.encode(jsonData["response"]));
  }

  Future<List<ChatHistoryModel>> chatHistoryDetails(
      String token, String fromLoginId, String toLoginId, int offset) async {
    final url = '$baseUrl/chat/Chat_common/getUser_chat_history_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "chat_type": "Internal",
      "from_login_id": fromLoginId,
      "to_login_id": toLoginId,
      "offset": offset,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successChatHistoryDetails,
    );

    List<ChatHistoryModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<ChatHistoryModel> successChatHistoryDetails(response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    if (jsonData['statusCode'] == 201) {
      throw (jsonData['response']);
    }
    return chatHistoryModelFromJson(
        json.encode(jsonData["response"]['chat_history']));
  }

  // create chat announement data provider
  Future<String> addChartGroup(String token, Map formData,
      List<String> groupMembers, String groupId, String groupUniqueId) async {
    final url = '$baseUrl/chat/Chat_common/add_chat_group_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
      "group_members": groupMembers,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successAddChartGroup,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successAddChartGroup(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] == 201) {
      throw (jsonData["response"]["status"]);
    }
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return jsonData["response"]["status"];
  }

  // clearUser_chat_history data provider
  Future<String> clearUserChatHistory(
      String token, String fromLoginId, String toLoginId) async {
    final url = '$baseUrl/chat/Chat_common/clearUser_chat_history';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "chat_type": "Internal",
      "from_login_id": fromLoginId,
      "to_login_id": toLoginId,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successClearUserChatHistory,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successClearUserChatHistory(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return jsonData["response"];
  }

  // send_text_message data provider
  Future<String> sendTextMessage(String token, String fromLoginId,
      String toLoginId, Map formData, String msgReferenceId) async {
    final url = '$baseUrl/chat/Chat_common/send_text_message';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "chat_type": "Internal",
      "from_login_id": fromLoginId,
      "to_login_id": toLoginId,
      ...formData,
      "type_of_chat": "client",
      "file": "",
      "msg_reference_id": msgReferenceId,
      "to_user_session": ""
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successSendTextMessage,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successSendTextMessage(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return jsonData["response"]["status"];
  }

  // send uploadfiles message data provider
  Future<String> uploadUserChat(String token, List getFiles, String fromLoginId,
      String toLoginId, String msgReferenceId) async {
    final url = '$baseUrl/chat/Chat_common/upload_user_chat_pol';

    FormData formData = new FormData.fromMap({
      "user_auth_token": token,
      "chat_type": "Internal",
      "from_login_id": fromLoginId,
      "to_login_id": toLoginId,
      "to_user_color": "",
      "msg_reference_id": msgReferenceId,
      'file[]':
          getFiles.map((path) => MultipartFile.fromFileSync(path)).toList(),
    });

    try {
      Response response = await Dio().post(url, data: formData);
      print(response);
      if (response.statusCode != 200) {
        throw ("Failed to Send Files.");
      }
      return response.statusMessage;
    } catch (e) {
      print(e);
      throw (e);
    }
  }

  // Chat user info data provider
  Future<ChatUserInfoModel> chatUserInfo(String token, String userId) async {
    final url = '$baseUrl/chat/Chat_common/get_user_info';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "user_id": userId,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successChatUserInfo,
    );

    ChatUserInfoModel data = await apiCall.initiateAPI();
    return data;
  }

  ChatUserInfoModel successChatUserInfo(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return chatUserInfoModelFromJson(json.encode(jsonData["response"]));
  }

  // send_text_message data provider
  Future<String> deleteUserMessages(String token, List chatTextLogId,
      String fromLoginId, String toLoginId) async {
    final url = '$baseUrl/chat/Chat_common/delete_user_messages';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "chat_text_log_id": chatTextLogId,
      "from_login_id": fromLoginId,
      "to_login_id": toLoginId
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successDeleteUserMessages,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successDeleteUserMessages(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return jsonData["response"];
  }

  // messageForward data provider
  Future<String> messageForward(String token, String fromLoginId,
      List chatTextLogId, Map recipients) async {
    final url = '$baseUrl/chat/Chat_common/message_forward_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "from_login_id": fromLoginId,
      "chat_text_log_id": chatTextLogId,
      "recipients": recipients,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successMessageForward,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successMessageForward(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return jsonData["response"];
  }

  // deleteGroup data provider
  Future<String> deleteGroup(String token, String groupId, String groupUniqueId,
      String groupType) async {
    final url = '$baseUrl/chat/Chat_common/delete_group';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successDeleteGroup,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successDeleteGroup(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] == 201) {
      // throw (jsonData["response"]);
      throw ("You don't have access to delete the selected group");
    } else if (jsonData['statusCode'] != 200) {
      throw (jsonData["response"]);
    }
    return jsonData["response"];
  }

  // clear_group_chat_history data provider
  Future<String> clearGroupChatHistory(String token, String groupId,
      String groupUniqueId, String groupType) async {
    final url = '$baseUrl/chat/Chat_common/clear_group_chat_history';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successClearGroupChatHistory,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successClearGroupChatHistory(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData["response"]);
    }
    return jsonData["response"];
  }

  // Chat user info data provider
  Future<ChatGroupInfoModel> chatGroupInfo(
      String token, String groupId, String groupUniqueId) async {
    final url = '$baseUrl/chat/Chat_common/get_group_info';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successChatGroupInfo,
    );

    ChatGroupInfoModel data = await apiCall.initiateAPI();
    return data;
  }

  ChatGroupInfoModel successChatGroupInfo(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return chatGroupInfoModelFromJson(
        json.encode(jsonData["response"]["group"]));
  }

  // blockuser data provider
  Future<String> blockUser(
      String token, String createdBy, String userId) async {
    final url = '$baseUrl/chat/Chat_common/blockuser';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "created_by": createdBy,
      "user_id": userId,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successBlockUser,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successBlockUser(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData["response"]);
    }
    return jsonData["response"];
  }

  // Chat user info data provider
  Future<RefreshUserHistoryModel> refreshUserChatHistory(
      String token, String fromLoginId, String toLoginId) async {
    final url = '$baseUrl/chat/Chat_common/refresh_user_chat_history_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "chat_type": "Internal",
      "from_login_id": fromLoginId,
      "to_login_id": toLoginId
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successRefreshUserChatHistory,
    );

    RefreshUserHistoryModel data = await apiCall.initiateAPI();
    return data;
  }

  RefreshUserHistoryModel successRefreshUserChatHistory(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return refreshUserHistoryModelFromJson(json.encode(jsonData["response"]));
  }

  // Chat user info data provider
  Future<RefreshGroupHistoryModel> refreshGroupChatHistory(String token,
      String groupId, String groupUniqueId, String groupType) async {
    final url = '$baseUrl/chat/Chat_common/refresh_group_chat_history_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successRefreshGroupChatHistory,
    );

    RefreshGroupHistoryModel data = await apiCall.initiateAPI();
    return data;
  }

  RefreshGroupHistoryModel successRefreshGroupChatHistory(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Send Message.");
    }
    return refreshGroupHistoryModelFromJson(json.encode(jsonData["response"]));
  }

  // blockuser data provider
  Future<String> liveUserStatus(String token, String status) async {
    final url = '$baseUrl/chat/Chat_common/updateliveuserstatus';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "status": status,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successLiveUserStatus,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successLiveUserStatus(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData["response"]);
    }
    return jsonData["response"];
  }

  Future<String> groupChatIds(String token, String groupId,
      String groupUniqueId, String groupType) async {
    final url = '$baseUrl/chat/Chat_common/getGroup_chat_history_pol';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "group_id": groupId,
      "group_unique_id": groupUniqueId,
      "group_type": groupType,
      "offset": 0,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGroupChatIds,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successGroupChatIds(response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    if (jsonData['statusCode'] == 201) {
      throw (jsonData['response']);
    }
    return jsonData["response"]['group_users'].join(',');
  }
}
