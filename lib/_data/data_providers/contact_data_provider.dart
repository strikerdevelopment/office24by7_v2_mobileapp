import 'dart:async';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:office24by7_v2/_data/api_calls/api_call.dart';

import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

class ContactAPIClient {
  final baseUrl = Environment.baseUrl;
  ContactAPIClient();
  var response;

  // Get Contacts, Goups, lists, company, source data provider
  // ignore: missing_return
  Future<List<dynamic>> getContacts(int offset, int limit, String filterHeader,
      String token, String filterOption) async {
    final url = '$baseUrl/mobileapp/contacts/get_contacts';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_header": filterHeader,
      "filter_option": filterOption,
      "offset": "$offset",
      "limit": "$limit",
    });

    APICall apiCall = APICall(
        method: 'POST',
        url: url,
        body: body,
        successCallBack: successGetContacts);

    List<dynamic> lstContact = await apiCall.initiateAPI();
    return lstContact;
  }

  successGetContacts(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;
    return data.map((e) => ContactList.fromJson(e)).toList();
  }

  // Get Goups data provider
  // ignore: missing_return
  Future<List<ContactGroupList>> contactGroupList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final url = '$baseUrl/mobileapp/contacts/get_contacts';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_header": filterHeader,
      "filter_option": filterOption,
      "offset": "$offset",
      "limit": "$limit",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactGroupList,
    );

    List<ContactGroupList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactGroupList> successContactGroupList(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;
    return data.map((e) => ContactGroupList.fromJson(e)).toList();
  }

  // Get Source data provider
  // ignore: missing_return
  Future<List<ContactSourceList>> contactSourceList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final url = '$baseUrl/mobileapp/contacts/get_contacts';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_header": filterHeader,
      "filter_option": filterOption,
      "offset": "$offset",
      "limit": "$limit",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactSourceList,
    );

    List<ContactSourceList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactSourceList> successContactSourceList(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;
    return data.map((e) => ContactSourceList.fromJson(e)).toList();
  }

  // Get Company data provider
  // ignore: missing_return
  Future<List<ContactCompanyList>> contactCompanyList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final url = '$baseUrl/mobileapp/contacts/get_contacts';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_header": filterHeader,
      "filter_option": filterOption,
      "offset": "$offset",
      "limit": "$limit",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactCompanyList,
    );

    List<ContactCompanyList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactCompanyList> successContactCompanyList(
      response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;
    return data.map((e) => ContactCompanyList.fromJson(e)).toList();
  }

  // Get List data provider
  // ignore: missing_return
  Future<List<ContactListList>> contactListList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final url = '$baseUrl/mobileapp/contacts/get_contacts';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_header": filterHeader,
      "filter_option": filterOption,
      "offset": "$offset",
      "limit": "$limit",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactListList,
    );

    List<ContactListList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactListList> successContactListList(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;
    return data.map((e) => ContactListList.fromJson(e)).toList();
  }

  // Get Contact Details data provider
  Future<ContactDetailsModel> getContactInfo(
      String token, String contactId) async {
    final url = '$baseUrl/mobileapp/contacts/get_contact_info';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "contact_id": contactId,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGetContactInfo,
    );

    ContactDetailsModel data = await apiCall.initiateAPI();
    return data;
  }

  ContactDetailsModel successGetContactInfo(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);
    return ContactDetailsModel.fromJson(jsonData['response'][0]);
  }

// Check Contact add provision data provider
  Future<ContactAddCheck> contactAddCheck(String token) async {
    final url = '$baseUrl/mobileapp/contacts/get_form';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactAddCheck,
    );

    ContactAddCheck data = await apiCall.initiateAPI();
    return data;
  }

  ContactAddCheck successContactAddCheck(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);
    return ContactAddCheck.fromJson(jsonData['response']);
  }

// Check Contact add Form Loading data provider
  Future<List<ContactFormLoad>> contactFormLoad(String token) async {
    final url = '$baseUrl/mobileapp/contacts/getFormFields';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactFormLoad,
    );

    List<ContactFormLoad> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactFormLoad> successContactFormLoad(response, APICall apiCall) {
    final jsonData = json.decode(response.body)['response'] as List;
    return jsonData.map((e) => ContactFormLoad.fromJson(e)).toList();
  }

  // Contact Sources load data provider
  Future<List<ContactSource>> contactSource(String token) async {
    final url = '$baseUrl/mobileapp/contacts/get_sources';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactSource,
    );

    List<ContactSource> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactSource> successContactSource(response, APICall apiCall) {
    final jsonData = json.decode(response.body)['response'] as List;
    return jsonData.map((e) => ContactSource.fromJson(e)).toList();
  }

  // Contact Sources load data provider
  Future<List<ContactGroup>> contactGroup(String token) async {
    final url = '$baseUrl/mobileapp/contacts/get_groups';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactGroup,
    );

    List<ContactGroup> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactGroup> successContactGroup(response, APICall apiCall) {
    final jsonData = json.decode(response.body)['response'] as List;
    return jsonData.map((e) => ContactGroup.fromJson(e)).toList();
  }

  // Contact get more details load data provider
  Future<List<GetMoreFields>> getMoreFields(
      String token, String fieldType) async {
    final url = '$baseUrl/mobileapp/contacts/get_morecontact_fields';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "field_type": fieldType,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGetMoreFields,
    );

    List<GetMoreFields> data = await apiCall.initiateAPI();
    return data;
  }

  List<GetMoreFields> successGetMoreFields(response, APICall apiCall) {
    final jsonData = json.decode(response.body)['response'] as List;
    return jsonData.map((e) => GetMoreFields.fromJson(e)).toList();
  }

  // Contact add contact load data provider
  Future<dynamic> addContact(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/add_contact';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "contact_data": formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successAddContact,
    );

    dynamic data = await apiCall.initiateAPI();
    return data;
  }

  dynamic successAddContact(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact get Sender Ids load data provider
  Future<List<GetContactSenderId>> getContactSenderID(String token) async {
    final url = '$baseUrl/mobileapp/contacts/reminder_sms_senderids';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successgetContactSenderID,
    );

    List<GetContactSenderId> data = await apiCall.initiateAPI();
    return data;
  }

  List<GetContactSenderId> successgetContactSenderID(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final listData = jsonData['response'] as List;
    return listData.map((e) => GetContactSenderId.fromJson(e)).toList();
  }

  // Contact get Email Ids load data provider
  Future<List<GetContactEmailId>> getContactEmailID(String token) async {
    final url = '$baseUrl/mobileapp/contacts/reminder_org_from_mail_ids';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successgetContactEmailID,
    );

    List<GetContactEmailId> data = await apiCall.initiateAPI();
    return data;
  }

  List<GetContactEmailId> successgetContactEmailID(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final listData = jsonData['response'] as List;
    return listData.map((e) => GetContactEmailId.fromJson(e)).toList();
  }

  // Contact add Meeting load data provider
  Future<String> addContactMeeting(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/add_Contact_reminder';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successaddContactMeeting,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successaddContactMeeting(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return jsonData['response'];
  }

  // Contact History Data load data provider
  Future<List<GlobalHistoryModel>> contactHistoryData(
      String token, String contactID) async {
    final url = '$baseUrl/mobileapp/contacts/contactHistory';

    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String formatted = formatter.format(now);
    print(formatted);

    final body = jsonEncode(<String, String>{
      "user_auth_token": "$token",
      "contact_id": "$contactID",
      "activity_type": "All",
      "from_date": "2020-09-10",
      "to_date": formatted
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactHistory,
    );

    List<GlobalHistoryModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<GlobalHistoryModel> successContactHistory(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final historyData =
        json.decode(response.body)['response']['history'] as List;
    return historyData.map((e) => GlobalHistoryModel.fromJson(e)).toList();
  }

  // Contact List Dropdown Data load data provider
  Future<List<ContactListDropdown>> contactListDropdown(String token) async {
    final url = '$baseUrl/mobileapp/contacts/get_list';

    final body = jsonEncode(<String, String>{
      "user_auth_token": "$token",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactListDropdown,
    );

    List<ContactListDropdown> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactListDropdown> successContactListDropdown(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final historyData = json.decode(response.body)['response'] as List;
    return historyData.map((e) => ContactListDropdown.fromJson(e)).toList();
  }

  // Contact add to list load data provider
  Future<String> addContactToList(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/contacts_add_list_to';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successaddContactToList,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successaddContactToList(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact List Dropdown Data load data provider
  Future<List<ContactHirarchyUser>> contactHirarchyUser(String token) async {
    final url = '$baseUrl/mobileapp/contacts/hirarchy_users';

    final body = jsonEncode(<String, String>{
      "user_auth_token": "$token",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactHirarchyUser,
    );

    List<ContactHirarchyUser> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactHirarchyUser> successContactHirarchyUser(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final historyData = json.decode(response.body)['response'] as List;
    return historyData.map((e) => ContactHirarchyUser.fromJson(e)).toList();
  }

  // Contact add to Reassign load data provider
  Future<String> contactAddToReassign(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/contact_reassign';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successcontactAddToReassign,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successcontactAddToReassign(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact Delete load data provider
  Future<String> contactDelete(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/contact_delete';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactDelete,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successContactDelete(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response']['message'];
  }

  // Contact Delete load data provider
  Future<Map> contactDetailsForForm(String token, String contactID) async {
    final url = '$baseUrl/mobileapp/contacts/get_contact_details';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "contact_id": contactID,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactDetailsForForm,
    );

    Map data = await apiCall.initiateAPI();
    return data;
  }

  Map successContactDetailsForForm(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact add contact load data provider
  Future<dynamic> editContact(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/edit_contact';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "contact_data": formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successEditContact,
    );

    dynamic data = await apiCall.initiateAPI();
    return data;
  }

  dynamic successEditContact(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return jsonData['response'];
  }

  Future<List<ContactList>> contactsDuplicate(String token, String cId) async {
    final url = '$baseUrl/mobileapp/contacts/getDuplicateContacts';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_options": "all",
      "filter_headers": "contact",
      "contact_id": cId
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactsDuplicate,
    );

    List<ContactList> lstContact = await apiCall.initiateAPI();
    return lstContact;
  }

  List<ContactList> successContactsDuplicate(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactList.fromJson(e)).toList();
  }

// Create List load data provider
  Future<Map> createList(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/create_list';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCreateList,
    );

    Map data = await apiCall.initiateAPI();
    return data;
  }

  Map successCreateList(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  Future<List<ContactSenderIds>> contactsSenderIds(String token) async {
    final url = '$baseUrl/mobileapp/contacts/sms_senderids';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "Is_Active": "1",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactsSenderIds,
    );

    List<ContactSenderIds> lstContact = await apiCall.initiateAPI();
    return lstContact;
  }

  List<ContactSenderIds> successContactsSenderIds(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactSenderIds.fromJson(e)).toList();
  }

  Future<List<dynamic>> contactsSMSType(String token) async {
    final url = '$baseUrl/mobileapp/contacts/getOrgAccountType';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactsSMSType,
    );

    List<dynamic> lstContact = await apiCall.initiateAPI();
    return lstContact;
  }

  List<dynamic> successContactsSMSType(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;
    return data;
  }

  // Contact send sms load data provider
  Future<String> contactSendSMS(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/addSMSCampaign';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactSendSMS,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successContactSendSMS(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  Future<List<ContactFromEmail>> contactsFromEmail(String token) async {
    final url = '$baseUrl/mobileapp/contacts/org_from_mail_ids';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "Is_Active": "1",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactsFromEmail,
    );

    List<ContactFromEmail> lstContact = await apiCall.initiateAPI();
    return lstContact;
  }

  List<ContactFromEmail> successContactsFromEmail(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactFromEmail.fromJson(e)).toList();
  }

  // Contact send sms load data provider
  Future<String> contactSendEmail(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/addEmailCampaign';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactSendEmail,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successContactSendEmail(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact get voice caller ids load data provider
  Future<List<ContactVoiceCallerIds>> contactVoiceCallerIds(
      String token) async {
    final url = '$baseUrl/mobileapp/contacts/getVoiceCallerID';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactVoiceCallerIds,
    );

    List<ContactVoiceCallerIds> lstContact = await apiCall.initiateAPI();
    return lstContact;
  }

  List<ContactVoiceCallerIds> successContactVoiceCallerIds(
      response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactVoiceCallerIds.fromJson(e)).toList();
  }

// Contact get voice audio files load data provider
  Future<List<ContactVoiceAudioFiles>> contactVoiceAudioFiles(
      String token) async {
    final url = '$baseUrl/mobileapp/contacts/getVoiceAudioFile';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactVoiceAudioFiles,
    );

    List<ContactVoiceAudioFiles> lstContact = await apiCall.initiateAPI();
    return lstContact;
  }

  List<ContactVoiceAudioFiles> successContactVoiceAudioFiles(
      response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactVoiceAudioFiles.fromJson(e)).toList();
  }

  // Contact send sms load data provider
  Future<String> contactSendVoice(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/addVoiceCampaign';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successContactSendVoice,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successContactSendVoice(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact send sms load data provider
  Future<String> createGroup(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/add_group';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "postdata": formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCreateGroup,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successCreateGroup(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact send sms load data provider
  Future<String> editGroup(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/edit_group';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "contact_group_id": formData["groupId"],
      "contact_group_desc": formData["groupName"],
      "postdata": formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successEditGroup,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successEditGroup(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Contact Delete Group load data provider
  Future<String> groupDelete(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/delete_group';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGroupDelete,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successGroupDelete(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Get Contacts, Goups, lists, company, source data provider
  // ignore: missing_return
  Future<List<ContactList>> groupContactsView(int offset, int limit,
      String filterHeader, String token, String id) async {
    final url = '$baseUrl/mobileapp/contacts/getGroupContacts';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_option": "all",
      "filter_header": filterHeader,
      "id": id,
      "offset": "$offset",
      "limit": "$limit",
      "origin": ""
    });

    APICall apiCall = APICall(
        method: 'POST',
        url: url,
        body: body,
        successCallBack: successGroupContactsView);

    List<ContactList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactList> successGroupContactsView(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactList.fromJson(e)).toList();
  }

  // Get Contacts, Goups, lists, company, source data provider
  // ignore: missing_return
  Future<List<dynamic>> contactsGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final url = '$baseUrl/mobileapp/contacts/Keywordsearch';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_option": "all",
      "search_term": keyword,
      "filter_header": filterHeader,
      "id": "0",
      "offset": "$offset",
      "limit": "$limit",
      "contact_origin_type": "0"
    });

    APICall apiCall = APICall(
        method: 'POST',
        url: url,
        body: body,
        successCallBack: successContactsGlobalSearch);

    List<dynamic> data = await apiCall.initiateAPI();
    return data;
  }

  successContactsGlobalSearch(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactList.fromJson(e)).toList();
  }

  // Get Contacts, Goups, lists, company, source data provider
  // ignore: missing_return
  Future<List<ContactGroupList>> groupGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final url = '$baseUrl/mobileapp/contacts/Keywordsearch';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_option": "all",
      "search_term": keyword,
      "filter_header": filterHeader,
      "id": "0",
      "offset": "$offset",
      "limit": "$limit",
      "contact_origin_type": "0"
    });

    APICall apiCall = APICall(
        method: 'POST',
        url: url,
        body: body,
        successCallBack: successGroupGlobalSearch);

    List<ContactGroupList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactGroupList> successGroupGlobalSearch(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactGroupList.fromJson(e)).toList();
  }

  // Get Contacts, Goups, lists, company, source data provider
  // ignore: missing_return
  Future<List<ContactSourceList>> sourceGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final url = '$baseUrl/mobileapp/contacts/Keywordsearch';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_option": "all",
      "search_term": keyword,
      "filter_header": filterHeader,
      "id": "0",
      "offset": "$offset",
      "limit": "$limit",
      "contact_origin_type": "0"
    });

    APICall apiCall = APICall(
        method: 'POST',
        url: url,
        body: body,
        successCallBack: successSourceGlobalSearch);

    List<ContactSourceList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactSourceList> successSourceGlobalSearch(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactSourceList.fromJson(e)).toList();
  }

  // Get Contacts, Goups, lists, company, source data provider
  // ignore: missing_return
  Future<List<ContactCompanyList>> companyGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final url = '$baseUrl/mobileapp/contacts/Keywordsearch';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_option": "all",
      "search_term": keyword,
      "filter_header": filterHeader,
      "id": "0",
      "offset": "$offset",
      "limit": "$limit",
      "contact_origin_type": "0"
    });

    APICall apiCall = APICall(
        method: 'POST',
        url: url,
        body: body,
        successCallBack: successCompanyGlobalSearch);

    List<ContactCompanyList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactCompanyList> successCompanyGlobalSearch(
      response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactCompanyList.fromJson(e)).toList();
  }

  // Get Contacts, Goups, lists, company, source data provider
  // ignore: missing_return
  Future<List<ContactListList>> listGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final url = '$baseUrl/mobileapp/contacts/Keywordsearch';

    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "filter_option": "all",
      "search_term": keyword,
      "filter_header": filterHeader,
      "id": "0",
      "offset": "$offset",
      "limit": "$limit",
      "contact_origin_type": "0"
    });

    APICall apiCall = APICall(
        method: 'POST',
        url: url,
        body: body,
        successCallBack: successListGlobalSearch);

    List<ContactListList> data = await apiCall.initiateAPI();
    return data;
  }

  List<ContactListList> successListGlobalSearch(response, APICall apiCall) {
    final jsonData = jsonDecode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw "Con't Fetch Data...";
    }
    final data = jsonData['response'] as List;

    return data.map((e) => ContactListList.fromJson(e)).toList();
  }

  // List Delete Group load data provider
  Future<String> listDelete(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/delete_list';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successlistDelete,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successlistDelete(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // List Edit list load data provider
  Future<String> listEdit(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/contacts/update_list';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successListEdit,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successListEdit(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response']['message'];
  }
}
