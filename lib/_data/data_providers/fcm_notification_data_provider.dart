import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_data/repositories/chat_repository.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';

class FCMNotificationService {
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Random random = Random();

  FCMNotificationService();

  Future initialize() async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings('ic_launcher');

    IOSInitializationSettings iosInitializationSettings =
        IOSInitializationSettings();

    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: androidInitializationSettings,
      iOS: iosInitializationSettings,
    );

    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  Future instantNotification() async {
    AndroidNotificationDetails android = AndroidNotificationDetails(
      "id",
      "channel",
      "description",
      importance: Importance.max,
      priority: Priority.max,
      enableLights: true,
      enableVibration: true,
      fullScreenIntent: true,
      playSound: true,
    );
    IOSNotificationDetails ios = IOSNotificationDetails(
      sound: 'default',
      presentSound: true,
      presentBadge: true,
      presentAlert: true,
    );

    NotificationDetails notificationDetails =
        NotificationDetails(android: android, iOS: ios);
    print(random.nextInt(100));
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      return await _flutterLocalNotificationsPlugin.show(
          random.nextInt(100),
          message.notification.title,
          message.notification.body,
          notificationDetails,
          payload: "office24by7");
    });
  }

  fcmOnTapListner(BuildContext context) {
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('adsfadf${message.data}');

      if (message.data['currentUser'] == null) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BlocProvider(
              create: (context) => RefreshGroupHistoryBloc(
                  chatRepository:
                      ChatRepository(chatAPIClient: ChatAPIClient())),
              child: AnnounChatHistoryDetails(
                  authData: authModelFromJson(message.data['authData']),
                  user: chatUserFromJson(message.data['user'])),
            ),
          ),
        );
      } else {
        Map currentUser = json.decode(message.data['currentUser']);
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BlocProvider(
                create: (context) => RefreshUserHistoryBloc(
                    chatRepository:
                        ChatRepository(chatAPIClient: ChatAPIClient())),
                child: ChatHistoryDetails(
                    authData: authModelFromJson(message.data['authData']),
                    user: User(
                      userId: currentUser['User_ID'],
                      name: currentUser['name'],
                      phone: currentUser['Mobile_Number'],
                    )),
              ),
            ));
      }
    });
  }
}
