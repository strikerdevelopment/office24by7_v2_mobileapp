import 'dart:async';
import 'dart:io';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:dio/dio.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/_models/global/file_download_model.dart';
import 'package:path_provider/path_provider.dart';

class FileDownloadAPIClient {
  final baseUrl = Environment.baseUrl;
  FileDownloadAPIClient();

  // send text message announement data provider
  // ignore: missing_return
  Future<FileDownload> fileDownloads(ChatFileInfo fileInfo) async {
    final imgUrl = fileInfo.src;

    try {
      var dir = Platform.isIOS
          ? await getApplicationDocumentsDirectory()
          : await getExternalStorageDirectory();
      await Dio().download(imgUrl, "${dir.path}/${fileInfo.fileName}",
          onReceiveProgress: (rec, total) {
        print("Rec: $rec , Total: $total");
        return FileDownload(
          downloading: true,
          progressDouble: rec / total,
          progressString: ((rec / total) * 100).toStringAsFixed(0) + "%",
          filePath: "${dir.path}/${fileInfo.fileName}",
        );
      });
      print("Download completed");
    } catch (e) {
      throw (e);
    }
  }
}
