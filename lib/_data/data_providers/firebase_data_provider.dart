import 'dart:async';
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;

class FirebaseAPIClient {
  Uuid uuid = Uuid();
  final baseUrl = Environment.baseUrl;
  final _fcmServerKey = Environment.fcmServerKey;
  FirebaseFirestore db = FirebaseFirestore.instance;

  FirebaseAPIClient();

// adding fcm colection tokens in firebase
  Future<String> addUserCollection(
    String userId,
    String userToken,
    String fcmToken,
  ) async {
    final QuerySnapshot result = await FirebaseFirestore.instance
        .collection('users')
        .where('user_id', isEqualTo: userId)
        .limit(1)
        .get();
    final List<DocumentSnapshot> documents = result.docs;
    if (documents.length >= 1) {
      await db.collection('users').doc(documents.first.id).set(
        {
          'user_id': userId,
          'user_token': userToken,
          'fcm_token': fcmToken,
          'chat_history_update': uuid.v1(),
        },
      );
    } else {
      await db.collection('users').add(
        {
          'user_id': userId,
          'user_token': userToken,
          'fcm_token': fcmToken,
          'chat_history_update': uuid.v1(),
        },
      );
    }
    return "successfully added";
  }

// send fcm notification for one to one user
  Future<void> sendFCM(String userId, String title, String body, User user,
      AuthModel authData, ChatUserInfoModel currentUserInfo) async {
    List fcmToken = [];
    final QuerySnapshot result = await FirebaseFirestore.instance
        .collection('users')
        .where('user_id', whereIn: userId.split(',')) //userId.split(',')
        // .limit(1)
        .get();

    if (result.docs.length == 0) {
      fcmToken = ["FCM TOKEN not found..."];
    } else {
      result.docs.forEach((doc) => fcmToken.add(doc.get('fcm_token')));

      // fcmToken = result.docs.first.get('fcm_token');
    }

    try {
      await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': _fcmServerKey
        },
        body: jsonEncode({
          "registration_ids": fcmToken,
          "collapse_key": "type_a",
          "priority": "high",
          "notification": {
            "title": title,
            "body": body,
            "sound": "default",
            "click_action": "FLUTTER_NOTIFICATION_CLICK",
          },
          "data": {
            "authData": authData,
            "user": user,
            "currentUser": currentUserInfo,
          },
          "apns": {
            "headers": {
              'apns-priority': '10',
            },
            "payload": {
              "aps": {
                "contentAvailable": true,
                "sound": 'default',
              }
            }
          },
          "android": {
            "priority": 'high',
            "notification": {
              "sound": 'default',
            }
          },
        }),
      );
      print('FCM request for device sent!');
    } catch (e) {
      print('FCM failed to send messages ($e)');
    }
  }

  // chat history refresh method through fcm
  Future<String> updateFirebaseHistory(
    String userId,
    String userToken,
  ) async {
    final QuerySnapshot result = await FirebaseFirestore.instance
        .collection('userChatHistory')
        .where('user_id', isEqualTo: userId)
        .limit(1)
        .get();
    final List<DocumentSnapshot> documents = result.docs;
    if (documents.length >= 1) {
      await db.collection('userChatHistory').doc(documents.first.id).set(
        {
          'user_id': userId,
          'user_token': userToken,
          'chat_history_update': uuid.v1(),
        },
      );
    } else {
      await db.collection('userChatHistory').add(
        {
          'user_id': userId,
          'user_token': userToken,
          'chat_history_update': uuid.v1(),
        },
      );
    }
    return "successfully added";
  }
}
