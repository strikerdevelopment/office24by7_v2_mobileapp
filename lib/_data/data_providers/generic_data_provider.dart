import 'dart:async';
import 'dart:convert';
import 'package:office24by7_v2/_data/api_calls/api_call.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

class GenericAPIClient {
  final baseUrl = Environment.baseUrl;
  GenericAPIClient();

  // Create Reminder to lead data provider
  Future<dynamic> genericAPIdata(String token, GenericModel formData) async {
    final url = '$baseUrl/common/generic/getgenericsp';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "api_name": formData.apiName,
      "CountryID": formData.countryID,
      "fieldid": formData.fieldid,
      "parent_field_option_id": formData.parentFieldOptionId.toString(),
      "auth_token": token,
      "version_number": formData.versionNumber,
      "category": formData.category,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGenericData,
    );

    dynamic data = await apiCall.initiateAPI();
    return data;
  }

  dynamic successGenericData(response, APICall apiCall) {
    switch (json.decode(apiCall.body)["api_name"]) {
      case 'ListStatesGet':
        final jsonData = json.decode(response.body);
        return jsonData;
        break;
      case 'CustomFieldOptionsGet':
        final jsonData = json.decode(response.body);
        return dependencyFieldsModelFromJson(jsonData);
        break;
      case 'PolUserauth':
        final jsonData = json.decode(response.body)[0];
        return authModelFromJson(json.encode(jsonData));
        break;
      case 'PolOverviewOnFeedback':
        if (json.decode(apiCall.body)['category'] == "") {
          final jsonData = json.decode(response.body)['feedback'];
          return formatePartyChart(jsonData);
        } else {
          final jsonData = json.decode(response.body)['feedback'];
          return formateMandalChart(jsonData);
        }

        break;
      default:
        return "Not Matching any API name";
    }
  }
}
