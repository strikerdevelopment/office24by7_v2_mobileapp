import 'dart:async';
import 'dart:convert';

import 'package:office24by7_v2/_data/api_calls/api_call.dart';

import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

class GlobalApiClient {
  final baseUrl = Environment.baseUrl;

  GlobalApiClient();

  Future<List<Country>> countryData(String token) async {
    final url = '$baseUrl/mobileapp/contacts/get_countries';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCountryData,
    );

    List<Country> data = await apiCall.initiateAPI();
    return data;
  }

  List<Country> successCountryData(response, apiInput) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }

    final arrayData = json.decode(response.body)['response'] as List;
    return arrayData.map((e) => Country.fromJson(e)).toList();
  }

  Future<List<StateModel>> stateData(String token, String country) async {
    final url = '$baseUrl/mobileapp/contacts/get_states';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "country": country,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successStateData,
    );

    List<StateModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<StateModel> successStateData(response, apiInput) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData = json.decode(response.body)['response'] as List;
    return arrayData.map((e) => StateModel.fromJson(e)).toList();
  }

  Future<List<City>> cityData(String token, String state) async {
    final url = '$baseUrl/mobileapp/contacts/get_cities';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "state": state,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCityData,
    );

    List<City> data = await apiCall.initiateAPI();
    return data;
  }

  List<City> successCityData(response, apiInput) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData = json.decode(response.body)['response'] as List;
    return arrayData.map((e) => City.fromJson(e)).toList();
  }

  Future<List<String>> countryFlags(String token) async {
    final url = '$baseUrl/mobileapp/contacts/get_country_codes';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successCountryFlags,
    );

    List<String> data = await apiCall.initiateAPI();
    return data;
  }

  List<String> successCountryFlags(response, apiInput) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }

    final arrayData = jsonData['response'] as List;

    return arrayData.map((e) => e as String).toList();
  }
}
