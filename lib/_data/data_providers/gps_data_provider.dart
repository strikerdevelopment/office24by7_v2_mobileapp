import 'dart:async';
import 'dart:convert';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

class GpsAPIClient {
  final baseUrl = Environment.baseUrl;
  final googleApi = Environment.googleMapAPI;
  GpsAPIClient();

  // gpsHierarchyUsers api
  Future<List<GpsHierarchyUsersModel>> gpsHierarchyUsers(String token) async {
    final url = '$baseUrl/mobileapp/API/getUsersOnHierarchy';

    final body = jsonEncode(<String, dynamic>{
      "key_Token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGpsHierarchyUsers,
    );

    List<GpsHierarchyUsersModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<GpsHierarchyUsersModel> successGpsHierarchyUsers(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['statusMessage']);
    }

    return gpsHierarchyUsersModelFromJson(json.encode(jsonData["response"]));
  }

  // GpsUserEventType api
  Future<List<GpsUserEventTypeModel>> gpsUserEventType(String token) async {
    final url = '$baseUrl/mobileapp/API/getMobile_UserEventTypes';

    final body = jsonEncode(<String, dynamic>{
      "key_Token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGpsUserEventType,
    );

    List<GpsUserEventTypeModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<GpsUserEventTypeModel> successGpsUserEventType(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['statusMessage']);
    }

    return gpsUserEventTypeModelFromJson(json.encode(jsonData["response"]));
  }

  // GpsUserLocation api
  Future<List<GpsUserLocationModel>> gpsUserLocation(
      String token, String users, String fromDate, String toDate) async {
    final url = '$baseUrl/mobileapp/API/getMobile_geoLocation';

    final body = jsonEncode(<String, dynamic>{
      "key_Token": token,
      "users": users,
      "from_Date": fromDate,
      "to_Date": toDate,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGpsUserLocation,
    );

    List<GpsUserLocationModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<GpsUserLocationModel> successGpsUserLocation(response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['statusMessage']);
    }

    return gpsUserLocationModelFromJson(json.encode(jsonData["response"]));
  }

  // GpsLocationHistory api
  Future<List<GpsLocationHistoryModel>> gpsLocationHistory(String token,
      String users, String fromDate, String toDate, String eventType) async {
    final url = '$baseUrl/mobileapp/API/getMobile_GeoLocationHistory';

    final body = jsonEncode(<String, dynamic>{
      "key_Token": token,
      "users": users,
      "from_Date": fromDate,
      "to_Date": toDate,
      "eventType": eventType,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGpsLocationHistory,
    );

    List<GpsLocationHistoryModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<GpsLocationHistoryModel> successGpsLocationHistory(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['statusMessage']);
    }

    return gpsLocationHistoryModelFromJson(json.encode(jsonData["response"]));
  }

  // addMobile_geoLocation api
  Future<String> gpsAddLocation(
    String token,
    String latitude,
    String longitude,
    String eventType,
    String deviceId,
    String phoneNumber,
  ) async {
    final url = '$baseUrl/mobileapp/API/addMobile_geoLocation';

    final body = jsonEncode(<String, dynamic>{
      "key_Token": token,
      "latitude": latitude,
      "longitude": longitude,
      "eventType": eventType,
      "device_id": deviceId,
      "phoneNumber": phoneNumber,
      "is_gps_enable": "1",
      "is_network_enable": "1",
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successGpsAddLocation,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successGpsAddLocation(response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (jsonData['statusCode'] != 200) {
      throw (jsonData['statusMessage']);
    }

    return jsonData['response']['statusMessage'];
  }

  // Google api location fetch api
  Future<Directions> gpsGoogleDirection(
    double originLat,
    double originLong,
    double destinationLat,
    double destinationLong,
  ) async {
    final url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=$originLat, $originLong&destination=$destinationLat, $destinationLong&key=$googleApi';

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      successCallBack: successGpsGoogleDirection,
    );

    Directions data = await apiCall.initiateAPI();
    return data;
  }

  Directions successGpsGoogleDirection(response, APICall apiCall) {
    final jsonData = json.decode(response.body);

    if (response.statusCode == 200) {
      return Directions.fromMap(jsonData);
    }
    return null;
  }
}
