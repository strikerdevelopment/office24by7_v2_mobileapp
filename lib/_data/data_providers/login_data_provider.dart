import 'dart:async';
import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:office24by7_v2/_data/api_calls/api_call.dart';

import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/login/login_info.dart';
import 'package:office24by7_v2/_models/models.dart';

class LoginApiClient {
  final baseUrl = Environment.baseUrl;
  final http.Client httpClient;

  LoginApiClient({@required this.httpClient}) : assert(httpClient != null);

  Future<OtpInfo> loginIdVerification(String loignID) async {
    final url = '$baseUrl/mobileapp/Auth/authenticate?login_id=$loignID';

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      successCallBack: successLoginIdVerification,
    );

    OtpInfo lstOtpInfo = await apiCall.initiateAPI();
    return lstOtpInfo;
  }

  Object successLoginIdVerification(response, apiInput) {
    final otpJson = jsonDecode(response.body);
    if (otpJson['statusCode'] == 200) {
      return OtpInfo.fromJson(otpJson);
    } else if (otpJson['statusCode'] == 202) {
      throw (otpJson['statusMessage']);
    } else if (otpJson['statusCode'] == 302) {
      throw (otpJson['statusMessage']);
    } else if (otpJson['statusCode'] == 301) {
      throw (otpJson['statusMessage']);
    } else {
      throw ("Invalid User ID, Please Contact to Support Team...");
    }
  }

  Future<LoginInfo> otpVerification(OtpInfo otpInfo) async {
    final url =
        '$baseUrl/mobileapp/Auth/MobileOTP_Check?mobileotp=${otpInfo.otpcode}&otp_type=2&user_id=${otpInfo.userid}';

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      successCallBack: successOtpVerification,
    );

    LoginInfo data = await apiCall.initiateAPI();
    return data;
  }

  LoginInfo successOtpVerification(response, apiInput) {
    final loginJson = jsonDecode(response.body);
    if (loginJson['statusCode'] == 400) {
      throw Exception(loginJson['statusMessage']);
    }
    return loginInfoFromJson(jsonEncode(loginJson['data']));
  }

  Future<String> resendOtp(OtpInfo otpInfo) async {
    final url =
        '$baseUrl/mobileapp/Auth/resetbutton_click?login_id=${otpInfo.loginid}&otp_type=2&user_id=${otpInfo.userid}';

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      successCallBack: successResendOtp,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successResendOtp(response, apiInput) {
    final jsonData = jsonDecode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw Exception(jsonData['statusMessage']);
    }
    return "OTP reset successfully";
  }

  Future<dynamic> logout(String token, String session) async {
    final url =
        '$baseUrl/mobileapp/Auth/logout_user?user_auth_token=$token&session_name=$session';

    APICall apiCall =
        APICall(method: 'POST', url: url, successCallBack: successLogout);

    final data = await apiCall.initiateAPI();
    return data;
  }

  successLogout(response, apiInput) {
    final otpJson = jsonDecode(response.body);
    return otpJson;
  }

  Future<dynamic> forceLogout(String loginId) async {
    final url = '$baseUrl/mobileapp/Auth/userforcelogout?login_id=$loginId';

    APICall apiCall =
        APICall(method: 'POST', url: url, successCallBack: successForceLogout);

    final data = await apiCall.initiateAPI();
    return data;
  }

  successForceLogout(response, apiInput) {
    final data = jsonDecode(response.body);
    if (data['statusCode'] == 200) {
      return data['statusMessage'];
    } else {
      return data['statusMessage'];
    }
  }

  Future<UserInfo> userInfo(String token) async {
    final url = '$baseUrl/mobileapp/Auth/myprofile?user_auth_token=$token';

    APICall apiCall =
        APICall(method: 'POST', url: url, successCallBack: successUserInfo);

    final data = await apiCall.initiateAPI();
    return data;
  }

  UserInfo successUserInfo(response, apiInput) {
    final data = jsonDecode(response.body);
    return UserInfo(
        userInfo: UserInfoClass.fromJson(data['User_info']),
        agentSettings: data['Agent_Settings']);
  }
}
