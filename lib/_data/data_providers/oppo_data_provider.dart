import 'dart:async';
import 'dart:convert';
import 'package:office24by7_v2/_data/api_calls/api_call.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';

class OppoAPIClient {
  final baseUrl = Environment.baseUrl;
  OppoAPIClient();

  // Get Header Data data provider
  Future<List<OppoHeader>> oppoHeaders(String token, String displayAs) async {
    final url = '$baseUrl/mobileapp/Opportunities/get_lead_headers';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "display_as": displayAs,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoHeader,
    );

    List<OppoHeader> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoHeader> successOppoHeader(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }

    switch (jsonDecode(apiCall.body)['display_as']) {
      case "status":
        final headerData = json.decode(response.body)['response'] as List;
        return headerData
            .map((e) => OppoHeader(
                  headerId: e['status_id'],
                  headerDesc: e['status_desc'],
                  headerOrder: e['status_order'],
                ))
            .toList();
        break;
      case "division":
        final headerData =
            json.decode(response.body)['response']['Division'] as List;
        return headerData
            .map((e) => OppoHeader(
                  headerId: e['division_id'],
                  headerDesc: e['division_name'],
                ))
            .toList();
        break;
      case "users":
        final headerData = json.decode(response.body)['response'] as List;
        return headerData
            .map((e) => OppoHeader(
                  headerId: e['user_id'],
                  headerDesc: e['user_name'],
                ))
            .toList();
        break;
      case "priority":
        final headerData = json.decode(response.body)['response'] as List;
        return headerData
            .map((e) => OppoHeader(
                  headerId: e['priority_id'],
                  headerDesc: e['priority_desc'],
                ))
            .toList();
        break;
      case "forms":
        final headerData = json.decode(response.body)['response'] as List;
        return headerData
            .map((e) => OppoHeader(
                  headerId: e['form_id'],
                  headerDesc: e['form_desc'],
                ))
            .toList();
        break;
      case "source":
        final headerData = json.decode(response.body)['response'] as List;
        return headerData
            .map((e) => OppoHeader(
                  headerId: e['source_id'],
                  headerDesc: e['source_desc'],
                ))
            .toList();
        break;
      default:
        throw ("Unknown Header pushed...");
    }
  }

  // Get tab load Data data provider
  Future<List<OppoLoadData>> oppoLoadData(
    String token,
    String displayType,
    String displayAs,
    String displayId,
    String dateType,
    String fromDate,
    String toDate,
    String searchValue,
    String assignedTo,
    int offSet,
    int limit,
  ) async {
    final url = '$baseUrl/mobileapp/Opportunities/getrecordsdata';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "display_type": displayType,
      "offset": offSet,
      "limit": limit,
      "display_as": displayAs,
      "search_division_id": displayAs == "division" ? displayId : "",
      "search_source_id": displayAs == "source" ? displayId : "",
      "custom_form_id": displayAs == "forms" ? displayId : "",
      "search_priority_id": displayAs == "priority" ? displayId : "",
      "search_stage_id": displayAs == "stage" ? displayId : "",
      "search_status_id": displayAs == "status" ? displayId : "",
      "search_date_from": fromDate,
      "search_date_to": toDate,
      "global_search_value": searchValue,
      "search_date_type": dateType,
      "search_assigned_to": assignedTo,
      "search_location": ""
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoLoadData,
    );

    List<OppoLoadData> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoLoadData> successOppoLoadData(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return oppoLoadDataFromJson(json.encode(jsonData["response"]));
  }

  // Get Division dropdown data provider
  Future<List<OppoDivisionDd>> oppoDivisionDD(token) async {
    final url = '$baseUrl/mobileapp/Opportunities/validateuser';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDivisionDD,
    );

    List<OppoDivisionDd> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDivisionDd> successOppoDivisionDD(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['divisions'] as List;
    return arrayData.map((e) => OppoDivisionDd.fromJson(e)).toList();
  }

  // Get Division based form  data provider
  Future<List<OppoDivisionFormDd>> oppoDivisionFormDD(
      String token, String divisionID) async {
    final url = '$baseUrl/mobileapp/Opportunities/getsalesforms';
    final body = jsonEncode(
        <String, String>{"user_auth_token": token, "division_id": divisionID});

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDivisionFormDD,
    );

    List<OppoDivisionFormDd> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDivisionFormDd> successOppoDivisionFormDD(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['division_forms'] as List;
    return arrayData.map((e) => OppoDivisionFormDd.fromJson(e)).toList();
  }

  // Create note to lead data provider
  Future<String> oppoCreateNote(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/addleadnote';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoCreateNote,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successOppoCreateNote(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Get Division based User data provider
  Future<List<OppoDivisionUserDd>> oppoDivisionUserDD(
      String token, String divisionID) async {
    final url = '$baseUrl/mobileapp/Opportunities/getdivisionusers';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "division_id": divisionID,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDivisionUserDD,
    );

    List<OppoDivisionUserDd> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDivisionUserDd> successOppoDivisionUserDD(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['division_users'] as List;
    return arrayData.map((e) => OppoDivisionUserDd.fromJson(e)).toList();
  }

  // Create Reassing to lead data provider
  Future<String> oppoCreateReassing(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/leadreassigned';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoCreateReassign,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successOppoCreateReassign(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusMessage'] != "Success") {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Get Division based User data provider
  Future<List<OppoLeadActivityModel>> oppoLeadActivity(
      String token,
      String leadID,
      String activityType,
      String fromDate,
      String toDate) async {
    final url = '$baseUrl/mobileapp/Opportunities/leadactivities';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "lead_id": leadID,
      "activity_type": activityType,
      "from_date": fromDate,
      "to_date": toDate,
      "user_id": "",
      "division_id": ""
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoLeadActivity,
    );

    List<OppoLeadActivityModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoLeadActivityModel> successOppoLeadActivity(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData = json.decode(response.body)['response'] as List;
    return arrayData.map((e) => OppoLeadActivityModel.fromJson(e)).toList();
  }

  // Create Transfer to lead data provider
  Future<String> oppoLeadTransfer(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/updateleadtransfer';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoLeadTransfer,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successOppoLeadTransfer(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Get Division based Source data provider
  Future<List<OppoDivisionSourceDd>> oppoDivisionSourceDd(
      String token, String divisionID) async {
    final url = '$baseUrl/mobileapp/Opportunities/getsourcemanual';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "division_id": divisionID,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDivisionSourceDd,
    );

    List<OppoDivisionSourceDd> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDivisionSourceDd> successOppoDivisionSourceDd(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData = json.decode(response.body)['response']['Sources'] as List;
    return arrayData.map((e) => OppoDivisionSourceDd.fromJson(e)).toList();
  }

  // Get Division based Statuses data provider
  Future<List<OppoDivisionStatusesDd>> oppoDivisionStatusesDd(
      String token, String divisionID) async {
    final url = '$baseUrl/mobileapp/Opportunities/getdivisionstatuses';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "division_id": divisionID,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDivisionStatusesDd,
    );

    List<OppoDivisionStatusesDd> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDivisionStatusesDd> successOppoDivisionStatusesDd(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['division_statuses'] as List;
    return arrayData.map((e) => OppoDivisionStatusesDd.fromJson(e)).toList();
  }

  // Get Division based Stages data provider
  Future<List<OppoDivisionStagesDd>> oppoDivisionStagesDd(
      String token, String divisionID, String statusId) async {
    final url = '$baseUrl/mobileapp/Opportunities/getdivisionstages';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "division_id": divisionID,
      "status_id": statusId
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDivisionStagesDd,
    );

    List<OppoDivisionStagesDd> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDivisionStagesDd> successOppoDivisionStagesDd(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['division_stages'] as List;
    return arrayData.map((e) => OppoDivisionStagesDd.fromJson(e)).toList();
  }

  // Get Division based Priority data provider
  Future<List<OppoDivisionPriorityDd>> oppoDivisionPriorityDd(
      String token, String divisionID) async {
    final url = '$baseUrl/mobileapp/Opportunities/getdivisionpriorities';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "division_id": divisionID,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDivisionPriorityDd,
    );

    List<OppoDivisionPriorityDd> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDivisionPriorityDd> successOppoDivisionPriorityDd(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['division_priorities'] as List;
    return arrayData.map((e) => OppoDivisionPriorityDd.fromJson(e)).toList();
  }

  // Get Form based Fields data provider
  Future<List<OppoFormFieldsModel>> oppoFormFields(
      String token, String formID) async {
    final url = '$baseUrl/mobileapp/Opportunities/get_lead_FormFields';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
      "form_id": formID,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoFormFields,
    );

    List<OppoFormFieldsModel> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoFormFieldsModel> successOppoFormFields(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }

    return oppoFormFieldsModelFromJson(
        json.encode(jsonData['response']['field_ids']));
  }

  // Get Sender Ids data provider
  Future<List<OppoSenderId>> oppoSenderId(String token) async {
    final url = '$baseUrl/mobileapp/Opportunities/getsendernames';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoSenderId,
    );

    List<OppoSenderId> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoSenderId> successOppoSenderId(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['Sender_IDs'] as List;
    return arrayData.map((e) => OppoSenderId.fromJson(e)).toList();
  }

  // Get Email Ids data provider
  Future<List<OppoEmailId>> oppoEmailId(String token) async {
    final url = '$baseUrl/mobileapp/Opportunities/getemailids';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoEmailId,
    );

    List<OppoEmailId> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoEmailId> successOppoEmailId(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['Email_IDs'] as List;
    return arrayData.map((e) => OppoEmailId.fromJson(e)).toList();
  }

  // Get Date type data provider
  Future<List<OppoDateType>> oppoDateType(String token) async {
    final url = '$baseUrl/mobileapp/Opportunities/get_dateselection';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoDateType,
    );

    List<OppoDateType> data = await apiCall.initiateAPI();
    return data;
  }

  List<OppoDateType> successOppoDateType(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    final arrayData =
        json.decode(response.body)['response']['dateformat'] as List;
    return arrayData.map((e) => OppoDateType.fromJson(e)).toList();
  }

  // Create Meeting to lead data provider
  Future<String> oppoAddMeeting(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/add_Lead_meeting';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoAddMeeting,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successOppoAddMeeting(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }
    return jsonData['response'];
  }

  // Create Reminder to lead data provider
  Future<String> oppoAddReminder(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/add_Lead_reminder';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoAddReminder,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successOppoAddReminder(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response'].toString());
    }
    return jsonData['response'];
  }

  // Create Email to lead data provider
  Future<dynamic> oppoAddEmail(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/sendEmail';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      ...formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoAddEmail,
    );

    dynamic data = await apiCall.initiateAPI();
    return data;
  }

  dynamic successOppoAddEmail(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to send email, Please check email id");
    }
    if (jsonData['response']['error_text'] != 'Success') {
      throw ("Failed to send email, Please check email id");
    }
    return "Your mail has been sent successfuly.";
  }

  // Get Date type data provider
  Future<List<dynamic>> oppoLeadActivityFilterType(String token) async {
    final url = '$baseUrl/mobileapp/Opportunities/activity_types';
    final body = jsonEncode(<String, String>{
      "user_auth_token": token,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoLeadActivityFilterType,
    );

    List<dynamic> data = await apiCall.initiateAPI();
    return data;
  }

  List<dynamic> successOppoLeadActivityFilterType(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw ("Failed to Load");
    }

    final arrayData = json.decode(response.body)['response'] as List;
    return arrayData;
  }

  // Create Reminder to lead data provider
  Future<String> oppoAddLead(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/addlead';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "leaddata": formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoAddLead,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successOppoAddLead(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return jsonData['response'];
  }

  // edit to lead data provider
  Future<String> oppoEditLead(String token, Map formData) async {
    final url = '$baseUrl/mobileapp/Opportunities/editlead';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "leaddata": formData,
    });

    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successOppoEditLead,
    );

    String data = await apiCall.initiateAPI();
    return data;
  }

  String successOppoEditLead(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return jsonData['response'];
  }
}
