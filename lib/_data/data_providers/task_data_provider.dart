import 'dart:convert';

import 'package:office24by7_v2/_data/api_calls/api_call.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/_models/task_model/task_division_form.dart';
import 'package:office24by7_v2/_models/task_model/task_priorities.dart';

class TaskAPIClient {
  final baseUrl = Environment.baseUrl;
  TaskAPIClient();

  // Task Division Provider
  Future<List<TaskDivision>> taskDivision(String token) async {
    final url = '$baseUrl/mobileapp/task/getdivisions';

    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskDivision,
    );

    List<TaskDivision> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskDivision> successTaskDivision(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskDivisionFromJson(json.encode(jsonData["response"]['divisions']));
  }

//  Task User Division Provider

  Future<List<TaskUserDivision>> taskUserDivision(
      String token, int divisionId) async {
    final url = '$baseUrl/mobileapp/task/getdivisionusers';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "division_id": divisionId,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskUserDivision,
    );

    List<TaskUserDivision> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskUserDivision> successTaskUserDivision(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskUserDivisionFromJson(
        json.encode(jsonData["response"]['division_users']));
  }

  // Task Source Provider

  Future<List<TaskSource>> taskSource(String token, int divisionId) async {
    final url = '$baseUrl/mobileapp/task/getsourcemanual';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "division_id": divisionId,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskSource,
    );

    List<TaskSource> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskSource> successTaskSource(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskSourceFromJson(json.encode(jsonData["response"]['Sources']));
  }

  // Task Division Status Provider

  Future<List<TaskDivisionStatus>> taskDivisionStatus(String token) async {
    final url = '$baseUrl/mobileapp/task/getdivisionstatuses';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskDivisionStatus,
    );

    List<TaskDivisionStatus> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskDivisionStatus> successTaskDivisionStatus(
      response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskDivisionStatusFromJson(json.encode(jsonData["response"]));
  }

  // Task Form Fields Provider

  Future<List<TaskFormFields>> taskFormFields(String token, int formId) async {
    final url = '$baseUrl/mobileapp/task/getFormFields';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "formid": formId,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskFormFields,
    );

    List<TaskFormFields> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskFormFields> successTaskFormFields(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskFormFieldsFromJson(
        json.encode(jsonData["response"]["field_ids"]));
  }
// Task Division Form Provider

  Future<List<TaskDivisionForm>> taskDivisionForm(
      String token, int divisionId) async {
    final url = '$baseUrl/mobileapp/task/gettasksforms';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "division_id": divisionId,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskDivisionForm,
    );

    List<TaskDivisionForm> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskDivisionForm> successTaskDivisionForm(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskDivisionFormFromJson(
        json.encode(jsonData["response"]["division_forms"]));
  }

  // Task Countries Provider

  Future<List<TaskCountries>> taskCountries(String token, int countryId) async {
    final url = '$baseUrl/mobileapp/task/getcountries';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "country_id": countryId,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskCountries,
    );

    List<TaskCountries> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskCountries> successTaskCountries(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskCountriesFromJson(json.encode(jsonData["response"]));
  }

  // Task Cities Provider

  Future<List<TaskCities>> taskCities(String token, int state) async {
    final url = '$baseUrl/mobileapp/task/getcities';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "state": state,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskCities,
    );

    List<TaskCities> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskCities> successTaskCities(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskCitiesFromJson(json.encode(jsonData["response"]));
  }

  // Task Records Provider

  Future<TaskDataModel> taskRecords(
      String token,
      String displayType,
      String searchStatusId,
      String searchDateFrom,
      String searchDateTo,
      String searchDuration,
      String displayAs,
      String searchassignedTo,
      String globalSearchValue,
      String offset,
      String limit) async {
    final url = '$baseUrl/mobileapp/task/getrecords';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "display_type": displayType,
      "search_status_id": searchStatusId,
      "search_date_from": searchDateFrom,
      "search_date_to": searchDateTo,
      "searchduration": searchDuration,
      "display_as": displayAs,
      "search_assigned_to": searchassignedTo,
      "global_search_value": globalSearchValue,
      "offset": offset,
      "limit": limit
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskRecords,
    );

    TaskDataModel data = await apiCall.initiateAPI();
    return data;
  }

  TaskDataModel successTaskRecords(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    // if (jsonData['statusCode'] != 200) {
    //   throw (jsonData['response']);
    // }
    return taskDataModelFromJson(json.encode(jsonData));
  }

  // Task priorities provider
  Future<List<TaskPriorities>> taskPriorities(
      String token, int divisionId) async {
    final url = '$baseUrl/mobileapp/task/getdivisionpriorities';
    final body = jsonEncode(<String, dynamic>{
      "user_auth_token": token,
      "division_id": divisionId,
    });
    APICall apiCall = APICall(
      method: 'POST',
      url: url,
      body: body,
      successCallBack: successTaskPriorities,
    );

    List<TaskPriorities> data = await apiCall.initiateAPI();
    return data;
  }

  List<TaskPriorities> successTaskPriorities(response, APICall apiCall) {
    final jsonData = json.decode(response.body);
    if (jsonData['statusCode'] != 200) {
      throw (jsonData['response']);
    }
    return taskPrioritiesFromJson(
        json.encode(jsonData["response"]["division_priorities"]));
  }
}
