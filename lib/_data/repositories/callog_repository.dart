import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';

class CallogRepository {
  final CallogAPIClient callogAPIClient;
  CallogRepository({
    @required this.callogAPIClient,
  }) : assert(callogAPIClient != null);

  Future<String> getClicktoCall(String token, String phoneNumber,
      String callerId, String leadId, String contactId, String callId) async {
    final data = await callogAPIClient.clicktoCall(
        token, phoneNumber, callerId, leadId, contactId, callId);
    return data;
  }

  Future<List<CallerIdModel>> getCallerIds(String token) async {
    final data = await callogAPIClient.callerIds(token);
    return data;
  }

  Future<List<CallsLogModel>> getCallsLog(
      String token, int limit, int offset) async {
    final data = await callogAPIClient.callsLog(token, limit, offset);
    return data;
  }

  Future<List<CallsLogHistoryModel>> getCallsLogHistory(
      String token, String callerPhoneNumber, int limit, int offset) async {
    final data = await callogAPIClient.callsLogHistory(
        token, callerPhoneNumber, limit, offset);
    return data;
  }
}
