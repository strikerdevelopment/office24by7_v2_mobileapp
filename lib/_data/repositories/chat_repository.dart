import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';

class ChatRepository {
  final ChatAPIClient chatAPIClient;
  ChatRepository({
    @required this.chatAPIClient,
  }) : assert(chatAPIClient != null);

  Future<String> getAddLiveChatSession(String token, String session) async {
    final data = await chatAPIClient.addLiveChatSession(token, session);
    return data;
  }

  Future<String> getSendMsgAnnouce(
      String token,
      String groupId,
      String groupUniqueId,
      String groupType,
      String msg,
      String msgReferenceId) async {
    final data = await chatAPIClient.sendMsgAnnouce(
        token, groupId, groupUniqueId, groupType, msg, msgReferenceId);
    return data;
  }

  Future<String> getUploadFilesAnnouce(
    String token,
    String groupId,
    String groupUniqueId,
    String groupType,
    List getFiles,
    String msgReferenceId,
  ) async {
    final data = await chatAPIClient.uploadFilesAnnouce(
        token, groupId, groupUniqueId, groupType, getFiles, msgReferenceId);
    return data;
  }

  Future<List<ChatHistoryModel>> getAnnounChatHistory(
      String token,
      String groupId,
      String groupUniqueId,
      String groupType,
      int offset) async {
    final data = await chatAPIClient.announChatHistory(
        token, groupId, groupUniqueId, groupType, offset);
    return data;
  }

  Future<String> getDeleteMsgAnnouce(String token, List chatTextLogId,
      String groupId, String groupUniqueId, String groupType) async {
    final data = await chatAPIClient.deleteMsgAnnouce(
        token, chatTextLogId, groupId, groupUniqueId, groupType);
    return data;
  }

  Future<ChatUsersModel> getAllUsers(String token, String type,
      String searchTerm, String isHierarchyUsers) async {
    final data =
        await chatAPIClient.allUsers(token, type, searchTerm, isHierarchyUsers);
    return data;
  }

  Future<List<ChatHistoryModel>> getChatHistoryDetails(
      String token, String fromLoginId, String toLoginId, int offset) async {
    final data = await chatAPIClient.chatHistoryDetails(
        token, fromLoginId, toLoginId, offset);
    return data;
  }

  Future<String> getAddChartGroup(String token, Map formData,
      List<String> groupMembers, String groupId, String groupUniqueId) async {
    final data = await chatAPIClient.addChartGroup(
        token, formData, groupMembers, groupId, groupUniqueId);
    return data;
  }

  Future<String> getClearUserChatHistory(
      String token, String fromLoginId, String toLoginId) async {
    final data =
        await chatAPIClient.clearUserChatHistory(token, fromLoginId, toLoginId);
    return data;
  }

  Future<String> getsendTextMessage(String token, String fromLoginId,
      String toLoginId, Map formData, String msgReferenceId) async {
    final data = await chatAPIClient.sendTextMessage(
        token, fromLoginId, toLoginId, formData, msgReferenceId);
    return data;
  }

  Future<String> getUploadUserChat(String token, List files, String fromLoginId,
      String toLoginId, String msgReferenceId) async {
    final data = await chatAPIClient.uploadUserChat(
        token, files, fromLoginId, toLoginId, msgReferenceId);
    return data;
  }

  Future<ChatUserInfoModel> getChatUserInfo(String token, String userId) async {
    final data = await chatAPIClient.chatUserInfo(token, userId);
    return data;
  }

  Future<String> getDeleteUserMessages(String token, List chatTextLogId,
      String fromLoginId, String toLoginId) async {
    final data = await chatAPIClient.deleteUserMessages(
        token, chatTextLogId, fromLoginId, toLoginId);
    return data;
  }

  Future<String> getMessageForward(String token, String fromLoginId,
      List chatTextLogId, Map recipients) async {
    final data = await chatAPIClient.messageForward(
        token, fromLoginId, chatTextLogId, recipients);
    return data;
  }

  Future<String> getDeleteGroup(String token, String groupId,
      String groupUniqueId, String groupType) async {
    final data = await chatAPIClient.deleteGroup(
        token, groupId, groupUniqueId, groupType);
    return data;
  }

  Future<String> getClearGroupChatHistory(String token, String groupId,
      String groupUniqueId, String groupType) async {
    final data = await chatAPIClient.clearGroupChatHistory(
        token, groupId, groupUniqueId, groupType);
    return data;
  }

  Future<ChatGroupInfoModel> getChatGroupInfo(
      String token, String groupId, String groupUniqueId) async {
    final data =
        await chatAPIClient.chatGroupInfo(token, groupId, groupUniqueId);
    return data;
  }

  Future<String> getBlockUser(
      String token, String createdBy, String userId) async {
    final data = await chatAPIClient.blockUser(token, createdBy, userId);
    return data;
  }

  Future<RefreshUserHistoryModel> getRefreshUserChatHistory(
      String token, String fromLoginId, String toLoginId) async {
    final data = await chatAPIClient.refreshUserChatHistory(
        token, fromLoginId, toLoginId);
    return data;
  }

  Future<RefreshGroupHistoryModel> getRefreshGroupChatHistory(String token,
      String groupId, String groupUniqueId, String groupType) async {
    final data = await chatAPIClient.refreshGroupChatHistory(
        token, groupId, groupUniqueId, groupType);
    return data;
  }

  Future<String> getLiveUserStatus(String token, String status) async {
    final data = await chatAPIClient.liveUserStatus(token, status);
    return data;
  }

  Future<String> getGroupChatIds(String token, String groupId,
      String groupUniqueId, String groupType) async {
    final data = await chatAPIClient.groupChatIds(
        token, groupId, groupUniqueId, groupType);
    return data;
  }
}
