import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';

class ContactRepository {
  final ContactAPIClient contactAPIClient;
  ContactRepository({
    @required this.contactAPIClient,
  }) : assert(contactAPIClient != null);

  // ignore: missing_return
  Future<List<dynamic>> getContactsData(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final data = await contactAPIClient.getContacts(
        offset, limit, filterHeader, token, filterOption);
    return data;
  }

  Future<List<ContactGroupList>> getContactGroupList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final data = await contactAPIClient.contactGroupList(
        offset, limit, filterHeader, token, filterOption);
    return data;
  }

  Future<List<ContactSourceList>> getContactSourceList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final data = await contactAPIClient.contactSourceList(
        offset, limit, filterHeader, token, filterOption);
    return data;
  }

  Future<List<ContactCompanyList>> getContactCompanyList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final data = await contactAPIClient.contactCompanyList(
        offset, limit, filterHeader, token, filterOption);
    return data;
  }

  Future<List<ContactListList>> getContactListList(int offset, int limit,
      String filterHeader, String token, String filterOption) async {
    final data = await contactAPIClient.contactListList(
        offset, limit, filterHeader, token, filterOption);
    return data;
  }

  Future<ContactDetailsModel> getContactDetails(
      String token, String contactId) async {
    ContactDetailsModel data =
        await contactAPIClient.getContactInfo(token, contactId);
    return data;
  }

  Future<ContactAddCheck> getcontactAddCheck(String token) async {
    ContactAddCheck data = await contactAPIClient.contactAddCheck(token);
    return data;
  }

  Future<List<ContactFormLoad>> getcontactFormLoad(String token) async {
    List<ContactFormLoad> data = await contactAPIClient.contactFormLoad(token);
    return data;
  }

  Future<List<ContactSource>> getContactSource(String token) async {
    List<ContactSource> data = await contactAPIClient.contactSource(token);
    return data;
  }

  Future<List<ContactGroup>> getContactGroup(String token) async {
    List<ContactGroup> data = await contactAPIClient.contactGroup(token);
    return data;
  }

  Future<List<GetMoreFields>> getGetMoreFiels(
      String token, String fieldType) async {
    List<GetMoreFields> data =
        await contactAPIClient.getMoreFields(token, fieldType);
    return data;
  }

  Future<dynamic> getAddContact(String token, Map formData) async {
    dynamic data = await contactAPIClient.addContact(token, formData);
    return data;
  }

  Future<List<GetContactSenderId>> loadgetcontactSenderID(String token) async {
    List<GetContactSenderId> data =
        await contactAPIClient.getContactSenderID(token);
    return data;
  }

  Future<List<GetContactEmailId>> loadgetcontactEmailID(String token) async {
    List<GetContactEmailId> data =
        await contactAPIClient.getContactEmailID(token);
    return data;
  }

  Future<String> getAddContactMeeting(String token, Map formData) async {
    String data = await contactAPIClient.addContactMeeting(token, formData);
    return data;
  }

  Future<List<GlobalHistoryModel>> getContactHistoryData(
      String token, String contactID) async {
    List<GlobalHistoryModel> data =
        await contactAPIClient.contactHistoryData(token, contactID);
    return data;
  }

  Future<List<ContactListDropdown>> getContactListDropdown(String token) async {
    List<ContactListDropdown> data =
        await contactAPIClient.contactListDropdown(token);
    return data;
  }

  Future<String> getAddContactToList(String token, Map formData) async {
    String data = await contactAPIClient.addContactToList(token, formData);
    return data;
  }

  Future<List<ContactHirarchyUser>> getContactHirarchyUser(String token) async {
    List<ContactHirarchyUser> data =
        await contactAPIClient.contactHirarchyUser(token);
    return data;
  }

  Future<String> getContactAddToReassign(String token, Map formData) async {
    String data = await contactAPIClient.contactAddToReassign(token, formData);
    return data;
  }

  Future<String> getContactDelete(String token, Map formData) async {
    String data = await contactAPIClient.contactDelete(token, formData);
    return data;
  }

  Future<Map> getContactDetailsForForm(String token, String contactID) async {
    Map data = await contactAPIClient.contactDetailsForForm(token, contactID);
    return data;
  }

  Future<dynamic> getEditContact(String token, Map formData) async {
    dynamic data = await contactAPIClient.editContact(token, formData);
    return data;
  }

  Future<List<ContactList>> getContactsDuplicate(
      String token, String cId) async {
    dynamic data = await contactAPIClient.contactsDuplicate(token, cId);
    return data;
  }

  Future<Map> getCreateList(String token, Map formData) async {
    final data = await contactAPIClient.createList(token, formData);
    return data;
  }

  Future<List<ContactSenderIds>> getContactsSenderIds(String token) async {
    final data = await contactAPIClient.contactsSenderIds(token);
    return data;
  }

  Future<List<dynamic>> getContactsSMSType(String token) async {
    final data = await contactAPIClient.contactsSMSType(token);
    return data;
  }

  Future<String> getContactSendSMS(String token, Map formData) async {
    String data = await contactAPIClient.contactSendSMS(token, formData);
    return data;
  }

  Future<List<ContactFromEmail>> getContactsFromEmail(String token) async {
    final data = await contactAPIClient.contactsFromEmail(token);
    return data;
  }

  Future<String> getContactSendEmail(String token, Map formData) async {
    String data = await contactAPIClient.contactSendEmail(token, formData);
    return data;
  }

  Future<List<ContactVoiceCallerIds>> getContactVoiceCallerIds(
      String token) async {
    final data = await contactAPIClient.contactVoiceCallerIds(token);
    return data;
  }

  Future<List<ContactVoiceAudioFiles>> getContactVoiceAudioFiles(
      String token) async {
    final data = await contactAPIClient.contactVoiceAudioFiles(token);
    return data;
  }

  Future<String> getContactSendVoice(String token, Map formData) async {
    String data = await contactAPIClient.contactSendVoice(token, formData);
    return data;
  }

  Future<String> getCreateGroup(String token, Map formData) async {
    String data = await contactAPIClient.createGroup(token, formData);
    return data;
  }

  Future<String> getEditGroup(String token, Map formData) async {
    String data = await contactAPIClient.editGroup(token, formData);
    return data;
  }

  Future<String> getGroupDelete(String token, Map formData) async {
    String data = await contactAPIClient.groupDelete(token, formData);
    return data;
  }

  Future<List<ContactList>> getGroupContactsView(int offset, int limit,
      String filterHeader, String token, String id) async {
    final data = await contactAPIClient.groupContactsView(
        offset, limit, filterHeader, token, id);
    return data;
  }

  Future<List<dynamic>> getContactsGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final data = await contactAPIClient.contactsGlobalSearch(
        offset, limit, filterHeader, token, keyword);
    return data;
  }

  Future<List<ContactGroupList>> getGroupGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final data = await contactAPIClient.groupGlobalSearch(
        offset, limit, filterHeader, token, keyword);
    return data;
  }

  Future<List<ContactSourceList>> getSourceGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final data = await contactAPIClient.sourceGlobalSearch(
        offset, limit, filterHeader, token, keyword);
    return data;
  }

  Future<List<ContactCompanyList>> getCompanyGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final data = await contactAPIClient.companyGlobalSearch(
        offset, limit, filterHeader, token, keyword);
    return data;
  }

  Future<List<ContactListList>> getListGlobalSearch(int offset, int limit,
      String filterHeader, String token, String keyword) async {
    final data = await contactAPIClient.listGlobalSearch(
        offset, limit, filterHeader, token, keyword);
    return data;
  }

  Future<String> getListDelete(String token, Map formData) async {
    String data = await contactAPIClient.listDelete(token, formData);
    return data;
  }

  Future<String> getListEdit(String token, Map formData) async {
    String data = await contactAPIClient.listEdit(token, formData);
    return data;
  }
}
