import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';

class FileDownloadRepository {
  final FileDownloadAPIClient fileDownloadAPIClient;
  FileDownloadRepository({
    @required this.fileDownloadAPIClient,
  }) : assert(fileDownloadAPIClient != null);

  Future<FileDownload> getFileDownloads(ChatFileInfo fileInfo) async {
    final data = await fileDownloadAPIClient.fileDownloads(fileInfo);
    return data;
  }
}
