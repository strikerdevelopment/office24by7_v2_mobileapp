import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';

class FirebaseRepository {
  final FirebaseAPIClient firebaseAPIClient;
  FirebaseRepository({
    @required this.firebaseAPIClient,
  }) : assert(firebaseAPIClient != null);

  Future<String> getAddUserCollection(
      String userId, String userToken, String fcmToken) async {
    final data =
        await firebaseAPIClient.addUserCollection(userId, userToken, fcmToken);
    return data;
  }

  Future<void> getSendFCM(String userId, String title, String body, User user,
      AuthModel authData, ChatUserInfoModel currentUserInfo) async {
    final data = await firebaseAPIClient.sendFCM(
        userId, title, body, user, authData, currentUserInfo);
    return data;
  }

  Future<String> getUpdateFirebaseHistory(
      String userId, String userToken) async {
    final data =
        await firebaseAPIClient.updateFirebaseHistory(userId, userToken);
    return data;
  }
}
