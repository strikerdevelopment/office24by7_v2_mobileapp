import 'package:meta/meta.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';

class GenericRepository {
  final GenericAPIClient genericAPIClient;

  GenericRepository({
    @required this.genericAPIClient,
  }) : assert(genericAPIClient != null);

  Future<dynamic> getGenericAPIdata(String token, GenericModel formData) async {
    final data = await genericAPIClient.genericAPIdata(token, formData);
    return data;
  }
}
