import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';

class GlobalRepository {
  final GlobalApiClient globalApiClient;
  GlobalRepository({
    @required this.globalApiClient,
  }) : assert(globalApiClient != null);

  // ignore: missing_return
  Future<List<Country>> getCountryData(String token) async {
    final data = await globalApiClient.countryData(token);
    return data;
  }

  // ignore: missing_return
  Future<List<StateModel>> getStateData(String token, String country) async {
    final data = await globalApiClient.stateData(token, country);
    return data;
  }

  Future<List<City>> getCityData(String token, String state) async {
    final data = await globalApiClient.cityData(token, state);
    return data;
  }

  Future<List<String>> getCountryFlags(String token) async {
    final data = await globalApiClient.countryFlags(token);
    return data;
  }
}
