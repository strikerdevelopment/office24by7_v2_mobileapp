import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/gps_model/gps_model.dart';

class GpsRepository {
  final GpsAPIClient gpsAPIClient;
  GpsRepository({
    @required this.gpsAPIClient,
  }) : assert(gpsAPIClient != null);

  Future<List<GpsHierarchyUsersModel>> getGpsHierarchyUsers(
      String token) async {
    final data = await gpsAPIClient.gpsHierarchyUsers(token);
    return data;
  }

  Future<List<GpsUserEventTypeModel>> getGpsUserEventType(String token) async {
    final data = await gpsAPIClient.gpsUserEventType(token);
    return data;
  }

  Future<List<GpsUserLocationModel>> getGpsUserLocation(
      String token, String users, String fromDate, String toDate) async {
    final data =
        await gpsAPIClient.gpsUserLocation(token, users, fromDate, toDate);
    return data;
  }

  Future<List<GpsLocationHistoryModel>> getGpsLocationHistory(String token,
      String users, String fromDate, String toDate, String eventType) async {
    final data = await gpsAPIClient.gpsLocationHistory(
        token, users, fromDate, toDate, eventType);
    return data;
  }

  Future<String> getGpsAddLocation(
      String token,
      String latitude,
      String longitude,
      String eventType,
      String deviceId,
      String phoneNumber) async {
    final data = await gpsAPIClient.gpsAddLocation(
        token, latitude, longitude, eventType, deviceId, phoneNumber);
    return data;
  }

  Future<Directions> getGpsGoogleDirection(
    double originLat,
    double originLong,
    double destinationLat,
    double destinationLong,
  ) async {
    final data = await gpsAPIClient.gpsGoogleDirection(
        originLat, originLong, destinationLat, destinationLong);
    return data;
  }
}
