import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/login/login.dart';
import 'package:office24by7_v2/_models/login/login_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginRepository {
  final LoginApiClient loginApiClient;
  LoginRepository({
    @required this.loginApiClient,
  }) : assert(loginApiClient != null);

  OtpInfo otpInfo;

  // ignore: missing_return
  Future<OtpInfo> getLoginData(String loginId) async {
    otpInfo = await loginApiClient.loginIdVerification(loginId);
    return otpInfo;
  }

  Future<dynamic> getLogout(String token, String session) async {
    final logoutData = await loginApiClient.logout(token, session);
    return logoutData;
  }

  Future<dynamic> getForceLogout(String loginId) async {
    final data = await loginApiClient.forceLogout(loginId);
    await deleteToken();
    return data;
  }

  Future<dynamic> getOtpData(OtpInfo otpInfo) async {
    final LoginInfo loginInfo = await loginApiClient.otpVerification(otpInfo);
    return loginInfo;
  }

  Future<String> getResendOtp(OtpInfo otpInfo) async {
    final String data = await loginApiClient.resendOtp(otpInfo);
    return data;
  }

  Future<UserInfo> getUserInfo(String token) async {
    final UserInfo userInfo = await loginApiClient.userInfo(token);
    return userInfo;
  }

  Future<void> deleteToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // await Future.delayed(Duration(seconds: 1));
    prefs.remove("userToken");
    prefs.remove("userSession");
    prefs.remove("UserId");
    prefs.remove("userRole");

    print("userToken deleted...");

    return;
  }

  Future<void> persistToken(LoginInfo loginInfo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // await Future.delayed(Duration(seconds: 1));
    print("token updated...");

    await prefs.setString("userToken", loginInfo.userAuthToken);
    await prefs.setString("userSession", loginInfo.sessionName);
    await prefs.setString("UserId", loginInfo.userId);
    await prefs.setString("userRole", loginInfo.userRole);

    return;
  }

  Future<bool> hasToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String hasUserToken = prefs.getString("userToken");
    await Future.delayed(Duration(seconds: 2));
    print(hasUserToken);

    if (hasUserToken == null) {
      return false;
    } else {
      return true;
    }
  }
}
