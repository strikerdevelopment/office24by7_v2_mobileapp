import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';

class OppoRepository {
  final OppoAPIClient oppoAPIClient;
  OppoRepository({
    @required this.oppoAPIClient,
  }) : assert(oppoAPIClient != null);

  // ignore: missing_return
  Future<List<OppoHeader>> getOppoHeaders(
      String token, String displayAs) async {
    final data = await oppoAPIClient.oppoHeaders(token, displayAs);
    return data;
  }

  Future<List<OppoLoadData>> getOppoLoadData(
      String token,
      String displayType,
      String displayAs,
      String displayId,
      String dateType,
      String fromDate,
      String toDate,
      String searchValue,
      String assignedTo,
      int offSet,
      int limit) async {
    final data = await oppoAPIClient.oppoLoadData(
        token,
        displayType,
        displayAs,
        displayId,
        dateType,
        fromDate,
        toDate,
        searchValue,
        assignedTo,
        offSet,
        limit);
    return data;
  }

  Future<List<OppoDivisionDd>> getOppoDivisionDD(String token) async {
    final data = await oppoAPIClient.oppoDivisionDD(token);
    return data;
  }

  Future<List<OppoDivisionFormDd>> getOppoDivisionFormDD(
      String token, String divisionID) async {
    final data = await oppoAPIClient.oppoDivisionFormDD(token, divisionID);
    return data;
  }

  Future<String> getOppoCreateNote(String token, Map formData) async {
    final data = await oppoAPIClient.oppoCreateNote(token, formData);
    return data;
  }

  Future<List<OppoDivisionUserDd>> getOppoDivisionUserDD(
      String token, String divisionID) async {
    final data = await oppoAPIClient.oppoDivisionUserDD(token, divisionID);
    return data;
  }

  Future<String> getOppoCreateReassing(String token, Map formData) async {
    final data = await oppoAPIClient.oppoCreateReassing(token, formData);
    return data;
  }

  Future<List<OppoLeadActivityModel>> getOppoLeadActivity(
      String token,
      String leadID,
      String activityType,
      String fromDate,
      String toDate) async {
    final data = await oppoAPIClient.oppoLeadActivity(
        token, leadID, activityType, fromDate, toDate);
    return data;
  }

  Future<String> getOppoLeadTransfer(String token, Map formData) async {
    final data = await oppoAPIClient.oppoLeadTransfer(token, formData);
    return data;
  }

  Future<List<OppoDivisionSourceDd>> getOppoDivisionSourceDd(
      String token, String divisionID) async {
    final data = await oppoAPIClient.oppoDivisionSourceDd(token, divisionID);
    return data;
  }

  Future<List<OppoDivisionStatusesDd>> getOppoDivisionStatusesDd(
      String token, String divisionID) async {
    final data = await oppoAPIClient.oppoDivisionStatusesDd(token, divisionID);
    return data;
  }

  Future<List<OppoDivisionStagesDd>> getOppoDivisionStagesDd(
      String token, String divisionID, String statusID) async {
    final data =
        await oppoAPIClient.oppoDivisionStagesDd(token, divisionID, statusID);
    return data;
  }

  Future<List<OppoDivisionPriorityDd>> getOppoDivisionPriorityDd(
      String token, String divisionID) async {
    final data = await oppoAPIClient.oppoDivisionPriorityDd(token, divisionID);
    return data;
  }

  Future<List<OppoFormFieldsModel>> getOppoFormFields(
      String token, String formID) async {
    final data = await oppoAPIClient.oppoFormFields(token, formID);
    return data;
  }

  Future<List<OppoSenderId>> getOppoSenderId(String token) async {
    final data = await oppoAPIClient.oppoSenderId(token);
    return data;
  }

  Future<List<OppoEmailId>> getOppoEmailId(String token) async {
    final data = await oppoAPIClient.oppoEmailId(token);
    return data;
  }

  Future<List<OppoDateType>> getOppoDateType(String token) async {
    final data = await oppoAPIClient.oppoDateType(token);
    return data;
  }

  Future<String> getOppoAddMeeting(String token, Map formData) async {
    final data = await oppoAPIClient.oppoAddMeeting(token, formData);
    return data;
  }

  Future<String> getOppoAddReminder(String token, Map formData) async {
    final data = await oppoAPIClient.oppoAddReminder(token, formData);
    return data;
  }

  Future<dynamic> getOppoAddEmail(String token, Map formData) async {
    final data = await oppoAPIClient.oppoAddEmail(token, formData);
    return data;
  }

  Future<List<dynamic>> getOppoLeadActivityFilterType(String token) async {
    final data = await oppoAPIClient.oppoLeadActivityFilterType(token);
    return data;
  }

  Future<String> getOppoAddLead(String token, Map formData) async {
    final data = await oppoAPIClient.oppoAddLead(token, formData);
    return data;
  }

  Future<String> getOppoEditLead(String token, Map formData) async {
    final data = await oppoAPIClient.oppoEditLead(token, formData);
    return data;
  }
}
