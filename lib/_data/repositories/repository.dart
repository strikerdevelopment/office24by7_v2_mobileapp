export 'login_repository.dart';
export 'contact_repository.dart';
export 'oppo_repository.dart';
export 'global_repository.dart';
export 'chat_repository.dart';
export 'generic_repository.dart';
export 'file_download_repository.dart';
export 'firebase_repository.dart';
export 'callog_repository.dart';
export 'gps_repository.dart';
export 'task_repository.dart';
