import 'package:office24by7_v2/_data/data_providers/task_data_provider.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/_models/task_model/task_division_form.dart';
import 'package:office24by7_v2/_models/task_model/task_priorities.dart';

class TaskRepository {
  final TaskAPIClient taskAPIClient;
  TaskRepository({this.taskAPIClient}) : assert(taskAPIClient != null);
  Future<List<TaskDivision>> getTaskDivision(String token) async {
    final data = await taskAPIClient.taskDivision(token);
    return data;
  }

  Future<List<TaskUserDivision>> getTaskUserDivision(
      String token, int divisionId) async {
    final data = await taskAPIClient.taskUserDivision(token, divisionId);
    return data;
  }

  Future<List<TaskSource>> getTaskSource(String token, int divisionId) async {
    final data = await taskAPIClient.taskSource(token, divisionId);
    return data;
  }

  Future<List<TaskDivisionStatus>> getTaskDivisionStatus(String token) async {
    final data = await taskAPIClient.taskDivisionStatus(token);
    return data;
  }

  Future<List<TaskFormFields>> getTaskFormFields(
      String token, int formId) async {
    final data = await taskAPIClient.taskFormFields(token, formId);
    return data;
  }

  Future<List<TaskDivisionForm>> getTaskDivisionForm(
      String token, int divisionId) async {
    final data = await taskAPIClient.taskDivisionForm(token, divisionId);
    return data;
  }

  Future<List<TaskCountries>> getTaskCountries(
      String token, int countryId) async {
    final data = await taskAPIClient.taskCountries(token, countryId);
    return data;
  }

  Future<List<TaskCities>> getTaskCities(String token, int state) async {
    final data = await taskAPIClient.taskCities(token, state);
    return data;
  }

  Future<List<TaskPriorities>> getTaskPriorities(
      String token, int divisionId) async {
    final data = await taskAPIClient.taskPriorities(token, divisionId);
    return data;
  }

  Future<TaskDataModel> getTaskRecords(
    String token,
    String displayType,
    String searchStatusId,
    String searchDateFrom,
    String searchDateTo,
    String searchDuration,
    String displayAs,
    String searchassignedTo,
    String globalSearchValue,
    String offset,
    String limit,
  ) async {
    final data = await taskAPIClient.taskRecords(
      token,
      displayType,
      searchStatusId,
      searchDateFrom,
      searchDateTo,
      searchDuration,
      displayAs,
      searchassignedTo,
      globalSearchValue,
      offset.toString(),
      limit.toString(),
    );
    return data;
  }
}
