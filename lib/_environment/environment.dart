import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Environment {
  static String baseUrl = "https://app.office24by7.com/v1";
  // static String baseUrl = "https://stage.office24by7.com/v1";
  // static String baseUrl = "https://feature5.office24by7.com/v1";
  // static String baseUrl = "https://4de0ead7a5c7.ngrok.io/bitbucket_v2/php/v1";
  static String cdnUrl = "assets/img";

  static String googleMapAPI = "AIzaSyB2OsLLYaS8Gfkg0dczo2DwDdwiqsK1wBQ";

  static String fcmServerKey =
      "key=AAAAr8XqqPw:APA91bEEN6cXCbrZZ1HpX6U--7Y7lX-8JfmchP0ULxIR6oB1db4sWGNa5Jeu8EY9SVLpUBi_cq-znBya0EHmfNUfzhwoamHgRUFvdg7WMum1No3Nlp_0eGC52flnn9Au9dGB-byNQW6q";

  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getString("userToken"));
    return prefs.getString("userToken");
  }

  Future<String> getSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userSession");
  }

  Future<String> getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("UserId");
  }

  Future<String> getUserRole() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userRole");
  }

  Future<String> fcmToken() async {
    String token = await FirebaseMessaging.instance.getToken();
    print(token);
    return token;
  }

  static int dataDisplayCount = 20;
}
