// To parse this JSON data, do
//
//     final callerIdModel = callerIdModelFromJson(jsonString);

import 'dart:convert';

List<CallerIdModel> callerIdModelFromJson(String str) =>
    List<CallerIdModel>.from(
        json.decode(str).map((x) => CallerIdModel.fromJson(x)));

String callerIdModelToJson(List<CallerIdModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CallerIdModel {
  CallerIdModel({
    this.didNumber,
    this.altNumber,
    this.calleridNum,
  });

  String didNumber;
  String altNumber;
  String calleridNum;

  factory CallerIdModel.fromJson(Map<String, dynamic> json) => CallerIdModel(
        didNumber: json["did_number"] == null ? null : json["did_number"],
        altNumber: json["alt_number"] == null ? null : json["alt_number"],
        calleridNum: json["callerid_num"] == null ? null : json["callerid_num"],
      );

  Map<String, dynamic> toJson() => {
        "did_number": didNumber == null ? null : didNumber,
        "alt_number": altNumber == null ? null : altNumber,
        "callerid_num": calleridNum == null ? null : calleridNum,
      };
}
