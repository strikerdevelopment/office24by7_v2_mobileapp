// To parse this JSON data, do
//
//     final callsLogHistoryModel = callsLogHistoryModelFromJson(jsonString);

import 'dart:convert';

List<CallsLogHistoryModel> callsLogHistoryModelFromJson(String str) =>
    List<CallsLogHistoryModel>.from(
        json.decode(str).map((x) => CallsLogHistoryModel.fromJson(x)));

String callsLogHistoryModelToJson(List<CallsLogHistoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CallsLogHistoryModel {
  CallsLogHistoryModel({
    this.callId,
    this.callType,
    this.maskingCallerNumber,
    this.callerNumber,
    this.callTime,
    this.callerStatus,
    this.agentStatus,
    this.totalCallDuration,
    this.conversationDuration,
    this.recordingFile,
    this.obdConnected,
    this.obdCallerMissed,
    this.obdAgentMissed,
    this.ibdConnected,
    this.ibdAgentMissed,
    this.ibdCallerMissed,
  });

  String callId;
  String callType;
  String maskingCallerNumber;
  String callerNumber;
  DateTime callTime;
  String callerStatus;
  String agentStatus;
  String totalCallDuration;
  String conversationDuration;
  String recordingFile;
  int obdConnected;
  int obdCallerMissed;
  int obdAgentMissed;
  int ibdConnected;
  int ibdAgentMissed;
  int ibdCallerMissed;

  factory CallsLogHistoryModel.fromJson(Map<String, dynamic> json) =>
      CallsLogHistoryModel(
        callId: json["call_id"] == null ? null : json["call_id"],
        callType: json["call_type"] == null ? null : json["call_type"],
        maskingCallerNumber: json["masking_caller_number"] == null
            ? null
            : json["masking_caller_number"],
        callerNumber:
            json["caller_number"] == null ? null : json["caller_number"],
        callTime: json["call_time"] == null
            ? null
            : DateTime.parse(json["call_time"]),
        callerStatus:
            json["caller_status"] == null ? null : json["caller_status"],
        agentStatus: json["agent_status"] == null ? null : json["agent_status"],
        totalCallDuration: json["total_call_duration"] == null
            ? null
            : json["total_call_duration"],
        conversationDuration: json["conversation_duration"] == null
            ? null
            : json["conversation_duration"],
        recordingFile:
            json["recording_file"] == null ? null : json["recording_file"],
        obdConnected:
            json["obd_connected"] == null ? null : json["obd_connected"],
        obdCallerMissed: json["obd_caller_missed"] == null
            ? null
            : json["obd_caller_missed"],
        obdAgentMissed:
            json["obd_agent_missed"] == null ? null : json["obd_agent_missed"],
        ibdConnected:
            json["ibd_connected"] == null ? null : json["ibd_connected"],
        ibdAgentMissed:
            json["ibd_agent_missed"] == null ? null : json["ibd_agent_missed"],
        ibdCallerMissed: json["ibd_caller_missed"] == null
            ? null
            : json["ibd_caller_missed"],
      );

  Map<String, dynamic> toJson() => {
        "call_id": callId == null ? null : callId,
        "call_type": callType == null ? null : callType,
        "masking_caller_number":
            maskingCallerNumber == null ? null : maskingCallerNumber,
        "caller_number": callerNumber == null ? null : callerNumber,
        "call_time": callTime == null ? null : callTime.toIso8601String(),
        "caller_status": callerStatus == null ? null : callerStatus,
        "agent_status": agentStatus == null ? null : agentStatus,
        "total_call_duration":
            totalCallDuration == null ? null : totalCallDuration,
        "conversation_duration":
            conversationDuration == null ? null : conversationDuration,
        "recording_file": recordingFile == null ? null : recordingFile,
        "obd_connected": obdConnected == null ? null : obdConnected,
        "obd_caller_missed": obdCallerMissed == null ? null : obdCallerMissed,
        "obd_agent_missed": obdAgentMissed == null ? null : obdAgentMissed,
        "ibd_connected": ibdConnected == null ? null : ibdConnected,
        "ibd_agent_missed": ibdAgentMissed == null ? null : ibdAgentMissed,
        "ibd_caller_missed": ibdCallerMissed == null ? null : ibdCallerMissed,
      };
}
