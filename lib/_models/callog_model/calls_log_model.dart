// To parse this JSON data, do
//
//     final callsLogModel = callsLogModelFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/random_colors/random_color.dart';

List<CallsLogModel> callsLogModelFromJson(String str) =>
    List<CallsLogModel>.from(
        json.decode(str).map((x) => CallsLogModel.fromJson(x)));

String callsLogModelToJson(List<CallsLogModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CallsLogModel {
  CallsLogModel({
    this.callId,
    this.callType,
    this.maskingCallerNumber,
    this.callerNumber,
    this.callTime,
    this.callStatus,
    this.callerStatus,
    this.agentStatus,
    this.totalCallDuration,
    this.conversationDuration,
    this.source,
    this.agentId,
    this.obdConnected,
    this.obdCallerMissed,
    this.obdAgentMissed,
    this.ibdConnected,
    this.ibdAgentMissed,
    this.ibdCallerMissed,
    this.color,
  });

  String callId;
  String callType;
  String maskingCallerNumber;
  String callerNumber;
  DateTime callTime;
  String callStatus;
  String callerStatus;
  String agentStatus;
  String totalCallDuration;
  String conversationDuration;
  String source;
  String agentId;
  int obdConnected;
  int obdCallerMissed;
  int obdAgentMissed;
  int ibdConnected;
  int ibdAgentMissed;
  int ibdCallerMissed;
  Color color;

  factory CallsLogModel.fromJson(Map<String, dynamic> json) => CallsLogModel(
        callId: json["call_id"] == null ? null : json["call_id"],
        callType: json["call_type"] == null ? null : json["call_type"],
        maskingCallerNumber: json["masking_caller_number"] == null
            ? null
            : json["masking_caller_number"],
        callerNumber:
            json["caller_number"] == null ? null : json["caller_number"],
        callTime: json["call_time"] == null
            ? null
            : DateTime.parse(json["call_time"]),
        callStatus: json["call_status"] == null ? null : json["call_status"],
        callerStatus:
            json["caller_status"] == null ? null : json["caller_status"],
        agentStatus: json["agent_status"] == null ? null : json["agent_status"],
        totalCallDuration: json["total_call_duration"] == null
            ? null
            : json["total_call_duration"],
        conversationDuration: json["conversation_duration"] == null
            ? null
            : json["conversation_duration"],
        source: json["source"] == null ? null : json["source"],
        agentId: json["agent_id"] == null ? null : json["agent_id"],
        obdConnected:
            json["obd_connected"] == null ? null : json["obd_connected"],
        obdCallerMissed: json["obd_caller_missed"] == null
            ? null
            : json["obd_caller_missed"],
        obdAgentMissed:
            json["obd_agent_missed"] == null ? null : json["obd_agent_missed"],
        ibdConnected:
            json["ibd_connected"] == null ? null : json["ibd_connected"],
        ibdAgentMissed:
            json["ibd_agent_missed"] == null ? null : json["ibd_agent_missed"],
        ibdCallerMissed: json["ibd_caller_missed"] == null
            ? null
            : json["ibd_caller_missed"],
        color: randomColor(),
      );

  Map<String, dynamic> toJson() => {
        "call_id": callId == null ? null : callId,
        "call_type": callType == null ? null : callType,
        "masking_caller_number":
            maskingCallerNumber == null ? null : maskingCallerNumber,
        "caller_number": callerNumber == null ? null : callerNumber,
        "call_time": callTime == null ? null : callTime.toIso8601String(),
        "call_status": callStatus == null ? null : callStatus,
        "caller_status": callerStatus == null ? null : callerStatus,
        "agent_status": agentStatus == null ? null : agentStatus,
        "total_call_duration":
            totalCallDuration == null ? null : totalCallDuration,
        "conversation_duration":
            conversationDuration == null ? null : conversationDuration,
        "source": source == null ? null : source,
        "agent_id": agentId == null ? null : agentId,
        "obd_connected": obdConnected == null ? null : obdConnected,
        "obd_caller_missed": obdCallerMissed == null ? null : obdCallerMissed,
        "obd_agent_missed": obdAgentMissed == null ? null : obdAgentMissed,
        "ibd_connected": ibdConnected == null ? null : ibdConnected,
        "ibd_agent_missed": ibdAgentMissed == null ? null : ibdAgentMissed,
        "ibd_caller_missed": ibdCallerMissed == null ? null : ibdCallerMissed,
      };
}
