class CallsLogTabModel {
  String iconName;
  String img;
  String tabId;
  String activeimg;

  CallsLogTabModel({this.tabId, this.iconName, this.img, this.activeimg});
}

List<CallsLogTabModel> callsLogTabData = [
  CallsLogTabModel(
    tabId: "call",
    iconName: "Call",
    img: "/mobile_app_v2/o_call_purple.png",
    activeimg: "/mobile_app_v2/o_call_white.png",
  ),
  // CallsLogTabModel(
  //   tabId: "text",
  //   iconName: "Text",
  //   img: "/mobile_app_v2/o_text_purple.png",
  //   activeimg: "/mobile_app_v2/o_text_white.png",
  // ),
  // CallsLogTabModel(
  //   tabId: "email",
  //   iconName: "Email",
  //   img: "/mobile_app_v2/o_email_purple.png",
  //   activeimg: "/mobile_app_v2/o_email_white.png",
  // ),
  // CallsLogTabModel(
  //   tabId: "task",
  //   iconName: "Task",
  //   img: "/mobile_app_v2/o_task_purple.png",
  //   activeimg: "/mobile_app_v2/o_task_white.png",
  // ),
];
