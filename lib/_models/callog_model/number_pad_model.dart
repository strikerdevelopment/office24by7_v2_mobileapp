class NumberPad {
  dynamic id;
  dynamic number;
  NumberPad({this.id, this.number});
}

List<NumberPad> numberPadData = [
  NumberPad(id: 1, number: 1),
  NumberPad(id: 2, number: 2),
  NumberPad(id: 3, number: 3),
  NumberPad(id: 4, number: 4),
  NumberPad(id: 5, number: 5),
  NumberPad(id: 6, number: 6),
  NumberPad(id: 7, number: 7),
  NumberPad(id: 8, number: 8),
  NumberPad(id: 9, number: 9),
  NumberPad(id: '', number: 'c'),
  NumberPad(id: 0, number: 0),
  NumberPad(id: 'call', number: 'c'),
];
