// To parse this JSON data, do
//
//     final announcementChatHistory = announcementChatHistoryFromJson(jsonString);

import 'dart:convert';

List<AnnouncementChatHistory> announcementChatHistoryFromJson(String str) =>
    List<AnnouncementChatHistory>.from(
        json.decode(str).map((x) => AnnouncementChatHistory.fromJson(x)));

String announcementChatHistoryToJson(List<AnnouncementChatHistory> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AnnouncementChatHistory {
  AnnouncementChatHistory({
    this.id,
    this.date,
    this.historyFields,
  });

  int id;
  String date;
  List<HistoryField> historyFields;

  factory AnnouncementChatHistory.fromJson(Map<String, dynamic> json) =>
      AnnouncementChatHistory(
        id: json["id"],
        date: json["date"],
        historyFields: List<HistoryField>.from(
            json["historyFields"].map((x) => HistoryField.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "date": date,
        "historyFields":
            List<dynamic>.from(historyFields.map((x) => x.toJson())),
      };
}

class HistoryField {
  HistoryField({
    this.time,
    this.color,
    this.rightdescription,
  });

  String time;
  String color;
  List<Rightdescription> rightdescription;

  factory HistoryField.fromJson(Map<String, dynamic> json) => HistoryField(
        time: json["time"],
        color: json["color"],
        rightdescription: List<Rightdescription>.from(
            json["rightdescription"].map((x) => Rightdescription.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "time": time,
        "color": color,
        "rightdescription":
            List<dynamic>.from(rightdescription.map((x) => x.toJson())),
      };
}

class Rightdescription {
  Rightdescription({
    this.chatTextLogId,
    this.fileInfo,
    this.msg,
    this.msgReferenceId,
    this.referenceMsg,
    this.referenceFileInfo,
    this.refFromName,
  });

  String chatTextLogId;
  List<FileInfo> fileInfo;
  dynamic msg;
  String msgReferenceId;
  String referenceMsg;
  List<dynamic> referenceFileInfo;
  dynamic refFromName;

  factory Rightdescription.fromJson(Map<String, dynamic> json) =>
      Rightdescription(
        chatTextLogId: json["chat_text_log_id"],
        fileInfo: List<FileInfo>.from(
            json["file_info"].map((x) => FileInfo.fromJson(x))),
        msg: json["msg"],
        msgReferenceId: json["msg_reference_id"],
        referenceMsg: json["reference_msg"],
        referenceFileInfo:
            List<dynamic>.from(json["reference_file_info"].map((x) => x)),
        refFromName: json["ref_from_name"],
      );

  Map<String, dynamic> toJson() => {
        "chat_text_log_id": chatTextLogId,
        "file_info": List<dynamic>.from(fileInfo.map((x) => x.toJson())),
        "msg": msg,
        "msg_reference_id": msgReferenceId,
        "reference_msg": referenceMsg,
        "reference_file_info":
            List<dynamic>.from(referenceFileInfo.map((x) => x)),
        "ref_from_name": refFromName,
      };
}

class FileInfo {
  FileInfo({
    this.type,
    this.size,
    this.src,
    this.thumb,
    this.filepath,
    this.fileName,
  });

  String type;
  int size;
  String src;
  String thumb;
  String filepath;
  String fileName;

  factory FileInfo.fromJson(Map<String, dynamic> json) => FileInfo(
        type: json["type"],
        size: json["size"] == null ? null : json["size"],
        src: json["src"],
        thumb: json["thumb"],
        filepath: json["filepath"],
        fileName: json["file_name"] == null ? null : json["file_name"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "size": size == null ? null : size,
        "src": src,
        "thumb": thumb,
        "filepath": filepath,
        "file_name": fileName == null ? null : fileName,
      };
}
