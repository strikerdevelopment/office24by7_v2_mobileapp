// To parse this JSON data, do
//
//     final chatGroupInfoModel = chatGroupInfoModelFromJson(jsonString);

import 'dart:convert';

import 'package:office24by7_v2/_models/chat_model/chat_model.dart';

ChatGroupInfoModel chatGroupInfoModelFromJson(String str) =>
    ChatGroupInfoModel.fromJson(json.decode(str));

String chatGroupInfoModelToJson(ChatGroupInfoModel data) =>
    json.encode(data.toJson());

class ChatGroupInfoModel {
  ChatGroupInfoModel({
    this.chatGroupId,
    this.groupUniqueId,
    this.groupType,
    this.groupName,
    this.groupDescription,
    this.orgId,
    this.companyId,
    this.isActive,
    this.createdDatetime,
    this.createdBy,
    this.groupCreatedBy,
    this.modifiedDatetime,
    this.modifiedBy,
    this.createdDate,
    this.groupUsers,
    this.users,
  });

  String chatGroupId;
  String groupUniqueId;
  String groupType;
  String groupName;
  dynamic groupDescription;
  String orgId;
  String companyId;
  String isActive;
  DateTime createdDatetime;
  String createdBy;
  String groupCreatedBy;
  String modifiedDatetime;
  String modifiedBy;
  String createdDate;
  String groupUsers;
  List<User> users;

  factory ChatGroupInfoModel.fromJson(Map<String, dynamic> json) =>
      ChatGroupInfoModel(
        chatGroupId: json["chat_group_id"],
        groupUniqueId: json["group_unique_id"],
        groupType: json["group_type"],
        groupName: json["group_name"],
        groupDescription: json["group_description"],
        orgId: json["org_id"],
        companyId: json["company_id"],
        isActive: json["is_active"],
        createdDatetime: DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"],
        groupCreatedBy: json["group_created_by"],
        modifiedDatetime: json["modified_datetime"],
        modifiedBy: json["modified_by"],
        createdDate: json["created_date"],
        groupUsers: json["group_users"],
        users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "chat_group_id": chatGroupId,
        "group_unique_id": groupUniqueId,
        "group_type": groupType,
        "group_name": groupName,
        "group_description": groupDescription,
        "org_id": orgId,
        "company_id": companyId,
        "is_active": isActive,
        "created_datetime": createdDatetime.toIso8601String(),
        "created_by": createdBy,
        "group_created_by": groupCreatedBy,
        "modified_datetime": modifiedDatetime,
        "modified_by": modifiedBy,
        "created_date": createdDate,
        "group_users": groupUsers,
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
      };
}
