// To parse this JSON data, do
//
//     final chatHistoryModel = chatHistoryModelFromJson(jsonString);

import 'dart:convert';

List<ChatHistoryModel> chatHistoryModelFromJson(String str) =>
    List<ChatHistoryModel>.from(
        json.decode(str).map((x) => ChatHistoryModel.fromJson(x)));

String chatHistoryModelToJson(List<ChatHistoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ChatHistoryModel {
  ChatHistoryModel({
    this.time,
    this.chatTextLogId,
    this.toLoginId,
    this.fromLoginId,
    this.cardPosition,
    this.date,
    this.fileInfo,
    this.msg,
    this.name,
    this.color,
    this.toName,
    this.msgReferenceId,
    this.referenceMsg,
    this.referenceFileInfo,
    this.refToName,
    this.refFromName,
  });

  String time;
  String chatTextLogId;
  String toLoginId;
  String fromLoginId;
  String cardPosition;
  String date;
  List<ChatFileInfo> fileInfo;
  dynamic msg;
  String name;
  String color;
  String toName;
  String msgReferenceId;
  String referenceMsg;
  List<ChatFileInfo> referenceFileInfo;
  String refToName;
  String refFromName;

  factory ChatHistoryModel.fromJson(Map<String, dynamic> json) =>
      ChatHistoryModel(
        time: json["time"] == null ? null : json["time"],
        chatTextLogId:
            json["chat_text_log_id"] == null ? null : json["chat_text_log_id"],
        toLoginId: json["to_login_id"] == null ? null : json["to_login_id"],
        fromLoginId:
            json["from_login_id"] == null ? null : json["from_login_id"],
        cardPosition:
            json["card_position"] == null ? null : json["card_position"],
        date: json["date"] == null ? null : json["date"],
        fileInfo: json["file_info"] == null
            ? null
            : List<ChatFileInfo>.from(
                json["file_info"].map((x) => ChatFileInfo.fromJson(x))),
        msg: json["msg"],
        name: json["name"] == null ? null : json["name"],
        color: json["color"] == null ? null : json["color"],
        toName: json["to_name"] == null ? null : json["to_name"],
        msgReferenceId:
            json["msg_reference_id"] == null ? null : json["msg_reference_id"],
        referenceMsg:
            json["reference_msg"] == null ? null : json["reference_msg"],
        referenceFileInfo: json["reference_file_info"] == null
            ? null
            : List<ChatFileInfo>.from(json["reference_file_info"]
                .map((x) => ChatFileInfo.fromJson(x))),
        refToName: json["ref_to_name"] == null ? null : json["ref_to_name"],
        refFromName:
            json["ref_from_name"] == null ? null : json["ref_from_name"],
      );

  Map<String, dynamic> toJson() => {
        "time": time == null ? null : time,
        "chat_text_log_id": chatTextLogId == null ? null : chatTextLogId,
        "to_login_id": toLoginId == null ? null : toLoginId,
        "from_login_id": fromLoginId == null ? null : fromLoginId,
        "card_position": cardPosition == null ? null : cardPosition,
        "date": date == null ? null : date,
        "file_info": fileInfo == null
            ? null
            : List<dynamic>.from(fileInfo.map((x) => x.toJson())),
        "msg": msg,
        "name": name == null ? null : name,
        "color": color == null ? null : color,
        "to_name": toName == null ? null : toName,
        "msg_reference_id": msgReferenceId == null ? null : msgReferenceId,
        "reference_msg": referenceMsg == null ? null : referenceMsg,
        "reference_file_info": referenceFileInfo == null
            ? null
            : List<dynamic>.from(referenceFileInfo.map((x) => x.toJson())),
        "ref_to_name": refToName == null ? null : refToName,
        "ref_from_name": refFromName == null ? null : refFromName,
      };
}

class ChatFileInfo {
  ChatFileInfo({
    this.type,
    this.size,
    this.src,
    this.fileName,
    this.thumb,
    this.filepath,
  });

  String type;
  int size;
  String src;
  String fileName;
  String thumb;
  String filepath;

  factory ChatFileInfo.fromJson(Map<String, dynamic> json) => ChatFileInfo(
        type: json["type"] == null ? null : json["type"],
        size: json["size"] == null ? null : json["size"],
        src: json["src"] == null ? null : json["src"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        thumb: json["thumb"] == null ? null : json["thumb"],
        filepath: json["filepath"] == null ? null : json["filepath"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "size": size == null ? null : size,
        "src": src == null ? null : src,
        "file_name": fileName == null ? null : fileName,
        "thumb": thumb == null ? null : thumb,
        "filepath": filepath == null ? null : filepath,
      };
}
