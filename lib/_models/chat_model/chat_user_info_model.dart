// To parse this JSON data, do
//
//     final chatUserInfoModel = chatUserInfoModelFromJson(jsonString);

import 'dart:convert';

ChatUserInfoModel chatUserInfoModelFromJson(String str) =>
    ChatUserInfoModel.fromJson(json.decode(str));

String chatUserInfoModelToJson(ChatUserInfoModel data) =>
    json.encode(data.toJson());

class ChatUserInfoModel {
  ChatUserInfoModel({
    this.userId,
    this.loginId,
    this.passWord,
    this.userRole,
    this.firstName,
    this.middleName,
    this.lastName,
    this.mobileNumber,
    this.emailId,
    this.empId,
    this.dob,
    this.doj,
    this.profileImage,
    this.orgId,
    this.orgName,
    this.companyId,
    this.companyName,
    this.branchId,
    this.branchName,
    this.deptId,
    this.deptName,
    this.desigId,
    this.desigName,
    this.skillIDs,
    this.languageIDs,
    this.groupId,
    this.groupName,
    this.contactAddress,
    this.countryId,
    this.countryName,
    this.stateId,
    this.stateName,
    this.cityId,
    this.cityName,
    this.pincode,
    this.registeredDateTime,
    this.createdBy,
    this.userType,
    this.lastLoginDateTime,
    this.lastLogoutDateTime,
    this.isMultiLogin,
    this.isSecureLogin,
    this.isLocked,
    this.invalidAttempts,
    this.invalidAttemptDate,
    this.isSendInvoice,
    this.isAvailable,
    this.isAllowDynamicNumber,
    this.assignedCfgFormId,
    this.assignedLeadCfgFormId,
    this.userAuthToken,
    this.isActive,
    this.isDelete,
    this.mverify,
    this.everify,
    this.name,
    this.registeredDate,
    this.role,
    this.chatUserInfoModelUserId,
    this.liveStatus,
    this.network,
    this.color,
    this.status,
    this.reportUser,
  });

  String userId;
  String loginId;
  String passWord;
  String userRole;
  String firstName;
  String middleName;
  String lastName;
  String mobileNumber;
  String emailId;
  String empId;
  String dob;
  String doj;
  String profileImage;
  String orgId;
  String orgName;
  String companyId;
  String companyName;
  String branchId;
  String branchName;
  String deptId;
  String deptName;
  String desigId;
  String desigName;
  String skillIDs;
  String languageIDs;
  String groupId;
  String groupName;
  String contactAddress;
  String countryId;
  String countryName;
  String stateId;
  String stateName;
  String cityId;
  String cityName;
  String pincode;
  String registeredDateTime;
  String createdBy;
  String userType;
  String lastLoginDateTime;
  String lastLogoutDateTime;
  String isMultiLogin;
  String isSecureLogin;
  String isLocked;
  String invalidAttempts;
  String invalidAttemptDate;
  String isSendInvoice;
  String isAvailable;
  String isAllowDynamicNumber;
  String assignedCfgFormId;
  String assignedLeadCfgFormId;
  String userAuthToken;
  String isActive;
  String isDelete;
  String mverify;
  String everify;
  String name;
  String registeredDate;
  String role;
  String chatUserInfoModelUserId;
  String liveStatus;
  String network;
  String color;
  Status status;
  String reportUser;

  factory ChatUserInfoModel.fromJson(Map<String, dynamic> json) =>
      ChatUserInfoModel(
        userId: json["User_ID"],
        loginId: json["Login_ID"],
        passWord: json["PassWord"],
        userRole: json["User_Role"],
        firstName: json["First_Name"],
        middleName: json["Middle_Name"],
        lastName: json["Last_Name"],
        mobileNumber: json["Mobile_Number"],
        emailId: json["Email_ID"],
        empId: json["Emp_ID"],
        dob: json["DOB"],
        doj: json["DOJ"],
        profileImage: json["Profile_Image"],
        orgId: json["Org_ID"],
        orgName: json["Org_Name"],
        companyId: json["Company_ID"],
        companyName: json["Company_Name"],
        branchId: json["Branch_ID"],
        branchName: json["Branch_Name"],
        deptId: json["Dept_ID"],
        deptName: json["Dept_Name"],
        desigId: json["Desig_ID"],
        desigName: json["Desig_Name"],
        skillIDs: json["Skill_IDs"],
        languageIDs: json["Language_IDs"],
        groupId: json["Group_ID"],
        groupName: json["Group_Name"],
        contactAddress: json["Contact_Address"],
        countryId: json["Country_Id"],
        countryName: json["Country_Name"],
        stateId: json["State_Id"],
        stateName: json["State_Name"],
        cityId: json["City_Id"],
        cityName: json["City_Name"],
        pincode: json["Pincode"],
        registeredDateTime: json["Registered_DateTime"],
        createdBy: json["Created_By"],
        userType: json["User_Type"],
        lastLoginDateTime: json["LastLogin_DateTime"],
        lastLogoutDateTime: json["LastLogout_DateTime"],
        isMultiLogin: json["Is_MultiLogin"],
        isSecureLogin: json["Is_SecureLogin"],
        isLocked: json["Is_Locked"],
        invalidAttempts: json["Invalid_Attempts"],
        invalidAttemptDate: json["Invalid_Attempt_Date"],
        isSendInvoice: json["Is_Send_Invoice"],
        isAvailable: json["Is_Available"],
        isAllowDynamicNumber: json["Is_Allow_Dynamic_Number"],
        assignedCfgFormId: json["Assigned_CFGFormID"],
        assignedLeadCfgFormId: json["Assigned_LeadCFGFormID"],
        userAuthToken: json["User_Auth_Token"],
        isActive: json["Is_Active"],
        isDelete: json["Is_Delete"],
        mverify: json["mverify"],
        everify: json["everify"],
        name: json["name"],
        registeredDate: json["registered_date"],
        role: json["role"],
        chatUserInfoModelUserId: json["user_id"],
        liveStatus: json["live_status"],
        network: json["network"],
        color: json["color"],
        status: Status.fromJson(json["status"]),
        reportUser: json["report_user"],
      );

  Map<String, dynamic> toJson() => {
        "User_ID": userId,
        "Login_ID": loginId,
        "PassWord": passWord,
        "User_Role": userRole,
        "First_Name": firstName,
        "Middle_Name": middleName,
        "Last_Name": lastName,
        "Mobile_Number": mobileNumber,
        "Email_ID": emailId,
        "Emp_ID": empId,
        "DOB": dob,
        "DOJ": doj,
        "Profile_Image": profileImage,
        "Org_ID": orgId,
        "Org_Name": orgName,
        "Company_ID": companyId,
        "Company_Name": companyName,
        "Branch_ID": branchId,
        "Branch_Name": branchName,
        "Dept_ID": deptId,
        "Dept_Name": deptName,
        "Desig_ID": desigId,
        "Desig_Name": desigName,
        "Skill_IDs": skillIDs,
        "Language_IDs": languageIDs,
        "Group_ID": groupId,
        "Group_Name": groupName,
        "Contact_Address": contactAddress,
        "Country_Id": countryId,
        "Country_Name": countryName,
        "State_Id": stateId,
        "State_Name": stateName,
        "City_Id": cityId,
        "City_Name": cityName,
        "Pincode": pincode,
        "Registered_DateTime": registeredDateTime,
        "Created_By": createdBy,
        "User_Type": userType,
        "LastLogin_DateTime": lastLoginDateTime,
        "LastLogout_DateTime": lastLogoutDateTime,
        "Is_MultiLogin": isMultiLogin,
        "Is_SecureLogin": isSecureLogin,
        "Is_Locked": isLocked,
        "Invalid_Attempts": invalidAttempts,
        "Invalid_Attempt_Date": invalidAttemptDate,
        "Is_Send_Invoice": isSendInvoice,
        "Is_Available": isAvailable,
        "Is_Allow_Dynamic_Number": isAllowDynamicNumber,
        "Assigned_CFGFormID": assignedCfgFormId,
        "Assigned_LeadCFGFormID": assignedLeadCfgFormId,
        "User_Auth_Token": userAuthToken,
        "Is_Active": isActive,
        "Is_Delete": isDelete,
        "mverify": mverify,
        "everify": everify,
        "name": name,
        "registered_date": registeredDate,
        "role": role,
        "user_id": chatUserInfoModelUserId,
        "live_status": liveStatus,
        "network": network,
        "color": color,
        "status": status.toJson(),
        "report_user": reportUser,
      };
}

class Status {
  Status({
    this.name,
    this.color,
    this.network,
  });

  String name;
  String color;
  String network;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        name: json["name"],
        color: json["color"],
        network: json["network"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "color": color,
        "network": network,
      };
}
