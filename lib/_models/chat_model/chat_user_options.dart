import 'package:flutter/material.dart';

class ChatUserOptions {
  String id;
  String name;
  String img;

  ChatUserOptions({this.id, this.name, this.img});
}

List<ChatUserOptions> chatUserOptionData = [
  ChatUserOptions(
      id: 'new_group',
      name: 'New Group',
      img: '/mobile_app_v2/o_option_group.png'),
  ChatUserOptions(
      id: 'new_announcement',
      name: 'New Announcement',
      img: '/mobile_app_v2/o_option_voice.png'),
  ChatUserOptions(
      id: 'settings',
      name: 'Settings',
      img: '/mobile_app_v2/o_option_settings.png'),
];

List<ChatUserOptions> chatHistoryOptionData = [
  ChatUserOptions(
      id: 'view_details',
      name: 'View Details',
      img: '/mobile_app_v2/o_option_view.png'),
  ChatUserOptions(
      id: 'clear_history',
      name: 'Clear History',
      img: '/mobile_app_v2/o_option_clear.png'),
  ChatUserOptions(
      id: 'block', name: 'Block', img: '/mobile_app_v2/o_option_block.png'),
];

List<ChatUserOptions> chatAnnounHistoryOptionData = [
  ChatUserOptions(
      id: 'view_details',
      name: 'Group Info',
      img: '/mobile_app_v2/o_option_view.png'),
  ChatUserOptions(
      id: 'clear_history',
      name: 'Clear History',
      img: '/mobile_app_v2/o_option_clear.png'),
  ChatUserOptions(
      id: 'delete', name: 'Delete', img: '/mobile_app_v2/o_option_delete.png'),
];

class ChatUserStatusOptions {
  String id;
  String name;
  Color color;

  ChatUserStatusOptions({this.id, this.name, this.color});
}

List<ChatUserStatusOptions> chatUserStatusData = [
  ChatUserStatusOptions(
    id: '1',
    name: 'Online',
    color: Colors.green,
  ),
  ChatUserStatusOptions(
    id: '2',
    name: 'Offline',
    color: Colors.grey,
  ),
  ChatUserStatusOptions(
    id: '3',
    name: 'Away',
    color: Colors.amber,
  ),
];
