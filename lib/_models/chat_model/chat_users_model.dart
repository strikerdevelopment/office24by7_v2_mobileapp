// To parse this JSON data, do
//
//     final chatUsersModel = chatUsersModelFromJson(jsonString);

import 'dart:convert';

ChatUsersModel chatUsersModelFromJson(String str) =>
    ChatUsersModel.fromJson(json.decode(str));

String chatUsersModelToJson(ChatUsersModel data) => json.encode(data.toJson());

class ChatUsersModel {
  ChatUsersModel({
    this.cnt,
    this.users,
  });

  int cnt;
  List<User> users;

  factory ChatUsersModel.fromJson(Map<String, dynamic> json) => ChatUsersModel(
        cnt: json["cnt"],
        users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "cnt": cnt,
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
      };
}

class User {
  User({
    this.name,
    this.createdDate,
    this.designation,
    this.description,
    this.network,
    this.isSelected,
    this.phone,
    this.group,
    this.img,
    this.i,
    this.chatGroupId,
    this.groupUniqueId,
    this.createdBy,
    this.groupType,
    this.icons,
    this.loginId,
    this.userId,
    this.firstName,
    this.lastName,
    this.email,
    this.color,
    this.liveStatus,
    this.sessionName,
  });

  String name;
  String createdDate;
  String designation;
  String description;
  String network;
  bool isSelected;
  String phone;
  String group;
  String img;
  dynamic i;
  String chatGroupId;
  String groupUniqueId;
  String createdBy;
  String groupType;
  String icons;
  String loginId;
  String userId;
  String firstName;
  String lastName;
  String email;
  String color;
  dynamic liveStatus;
  String sessionName;

  factory User.fromJson(Map<String, dynamic> json) => User(
        name: json["name"],
        createdDate: json["created_date"] == null ? null : json["created_date"],
        designation: json["designation"],
        description: json["description"],
        network: json["network"],
        isSelected: json["isSelected"],
        phone: json["phone"],
        group: json["group"] == null ? null : json["group"],
        img: json["img"],
        i: json["i"],
        chatGroupId:
            json["chat_group_id"] == null ? null : json["chat_group_id"],
        groupUniqueId:
            json["group_unique_id"] == null ? null : json["group_unique_id"],
        createdBy: json["created_by"] == null ? null : json["created_by"],
        groupType: json["group_type"] == null ? null : json["group_type"],
        icons: json["icons"] == null ? null : json["icons"],
        loginId: json["login_id"] == null ? null : json["login_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        email: json["email"] == null ? null : json["email"],
        color: json["color"] == null ? null : json["color"],
        liveStatus: json["live_status"],
        sessionName: json["session_name"] == null ? null : json["session_name"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "created_date": createdDate == null ? null : createdDate,
        "designation": designation,
        "description": description,
        "network": network,
        "isSelected": isSelected,
        "phone": phone,
        "group": group == null ? null : group,
        "img": img,
        "i": i,
        "chat_group_id": chatGroupId == null ? null : chatGroupId,
        "group_unique_id": groupUniqueId == null ? null : groupUniqueId,
        "created_by": createdBy == null ? null : createdBy,
        "group_type": groupType == null ? null : groupType,
        "icons": icons == null ? null : icons,
        "login_id": loginId == null ? null : loginId,
        "user_id": userId == null ? null : userId,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "color": color == null ? null : color,
        "live_status": liveStatus,
        "session_name": sessionName == null ? null : sessionName,
      };
}

User chatUserFromJson(String str) => User.fromJson(json.decode(str));
