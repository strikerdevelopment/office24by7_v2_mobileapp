// To parse this JSON data, do
//
//     final refreshGroupHistoryModel = refreshGroupHistoryModelFromJson(jsonString);

import 'dart:convert';

RefreshGroupHistoryModel refreshGroupHistoryModelFromJson(String str) =>
    RefreshGroupHistoryModel.fromJson(json.decode(str));

String refreshGroupHistoryModelToJson(RefreshGroupHistoryModel data) =>
    json.encode(data.toJson());

class RefreshGroupHistoryModel {
  RefreshGroupHistoryModel({
    this.cnt,
    this.chatHistory,
  });

  int cnt;
  List<dynamic> chatHistory;

  factory RefreshGroupHistoryModel.fromJson(Map<String, dynamic> json) =>
      RefreshGroupHistoryModel(
        cnt: json["cnt"] == null ? null : json["cnt"],
        chatHistory: json["chat_history"] == null
            ? null
            : List<dynamic>.from(json["chat_history"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "cnt": cnt == null ? null : cnt,
        "chat_history": chatHistory == null
            ? null
            : List<dynamic>.from(chatHistory.map((x) => x)),
      };
}
