// To parse this JSON data, do
//
//     final refreshUserHistoryModel = refreshUserHistoryModelFromJson(jsonString);

import 'dart:convert';

RefreshUserHistoryModel refreshUserHistoryModelFromJson(String str) =>
    RefreshUserHistoryModel.fromJson(json.decode(str));

String refreshUserHistoryModelToJson(RefreshUserHistoryModel data) =>
    json.encode(data.toJson());

class RefreshUserHistoryModel {
  RefreshUserHistoryModel({
    this.blockedUserInfo,
    this.cnt,
    this.chatHistory,
  });

  BlockedUserInfo blockedUserInfo;
  int cnt;
  List<ChatHistory> chatHistory;

  factory RefreshUserHistoryModel.fromJson(Map<String, dynamic> json) =>
      RefreshUserHistoryModel(
        blockedUserInfo: json["blocked_user_info"] == null
            ? null
            : BlockedUserInfo.fromJson(json["blocked_user_info"]),
        cnt: json["cnt"] == null ? null : json["cnt"],
        chatHistory: json["chat_history"] == null
            ? null
            : List<ChatHistory>.from(
                json["chat_history"].map((x) => ChatHistory.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "blocked_user_info":
            blockedUserInfo == null ? null : blockedUserInfo.toJson(),
        "cnt": cnt == null ? null : cnt,
        "chat_history": chatHistory == null
            ? null
            : List<dynamic>.from(chatHistory.map((x) => x.toJson())),
      };
}

class BlockedUserInfo {
  BlockedUserInfo({
    this.cnt,
    this.data,
  });

  int cnt;
  dynamic data;

  factory BlockedUserInfo.fromJson(Map<String, dynamic> json) =>
      BlockedUserInfo(
        cnt: json["cnt"] == null ? null : json["cnt"],
        data: json["data"] == null ? null : json["data"],
      );

  Map<String, dynamic> toJson() => {
        "cnt": cnt == null ? null : cnt,
        // "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.chatBlockedId,
    this.userId,
    this.createdBy,
    this.updatedBy,
    this.companyId,
    this.orgId,
    this.createdDatetime,
    this.updatedDatetime,
  });

  String chatBlockedId;
  String userId;
  String createdBy;
  dynamic updatedBy;
  String companyId;
  String orgId;
  DateTime createdDatetime;
  dynamic updatedDatetime;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        chatBlockedId:
            json["chat_blocked_id"] == null ? null : json["chat_blocked_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        createdBy: json["created_by"] == null ? null : json["created_by"],
        updatedBy: json["updated_by"],
        companyId: json["company_id"] == null ? null : json["company_id"],
        orgId: json["org_id"] == null ? null : json["org_id"],
        createdDatetime: json["created_datetime"] == null
            ? null
            : DateTime.parse(json["created_datetime"]),
        updatedDatetime: json["updated_datetime"],
      );

  Map<String, dynamic> toJson() => {
        "chat_blocked_id": chatBlockedId == null ? null : chatBlockedId,
        "user_id": userId == null ? null : userId,
        "created_by": createdBy == null ? null : createdBy,
        "updated_by": updatedBy,
        "company_id": companyId == null ? null : companyId,
        "org_id": orgId == null ? null : orgId,
        "created_datetime":
            createdDatetime == null ? null : createdDatetime.toIso8601String(),
        "updated_datetime": updatedDatetime,
      };
}

class ChatHistory {
  ChatHistory({
    this.time,
    this.chatTextLogId,
    this.toLoginId,
    this.fromLoginId,
    this.date,
    this.cardPosition,
    this.fileInfo,
    this.msg,
    this.name,
    this.color,
    this.toName,
    this.msgReferenceId,
    this.referenceMsg,
    this.referenceFileInfo,
    this.refToName,
    this.refFromName,
  });

  String time;
  String chatTextLogId;
  String toLoginId;
  String fromLoginId;
  DateTime date;
  String cardPosition;
  List<dynamic> fileInfo;
  String msg;
  String name;
  String color;
  String toName;
  String msgReferenceId;
  String referenceMsg;
  List<dynamic> referenceFileInfo;
  String refToName;
  String refFromName;

  factory ChatHistory.fromJson(Map<String, dynamic> json) => ChatHistory(
        time: json["time"] == null ? null : json["time"],
        chatTextLogId:
            json["chat_text_log_id"] == null ? null : json["chat_text_log_id"],
        toLoginId: json["to_login_id"] == null ? null : json["to_login_id"],
        fromLoginId:
            json["from_login_id"] == null ? null : json["from_login_id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        cardPosition:
            json["card_position"] == null ? null : json["card_position"],
        fileInfo: json["file_info"] == null
            ? null
            : List<dynamic>.from(json["file_info"].map((x) => x)),
        msg: json["msg"] == null ? null : json["msg"],
        name: json["name"] == null ? null : json["name"],
        color: json["color"] == null ? null : json["color"],
        toName: json["to_name"] == null ? null : json["to_name"],
        msgReferenceId:
            json["msg_reference_id"] == null ? null : json["msg_reference_id"],
        referenceMsg:
            json["reference_msg"] == null ? null : json["reference_msg"],
        referenceFileInfo: json["reference_file_info"] == null
            ? null
            : List<dynamic>.from(json["reference_file_info"].map((x) => x)),
        refToName: json["ref_to_name"] == null ? null : json["ref_to_name"],
        refFromName:
            json["ref_from_name"] == null ? null : json["ref_from_name"],
      );

  Map<String, dynamic> toJson() => {
        "time": time == null ? null : time,
        "chat_text_log_id": chatTextLogId == null ? null : chatTextLogId,
        "to_login_id": toLoginId == null ? null : toLoginId,
        "from_login_id": fromLoginId == null ? null : fromLoginId,
        "date": date == null
            ? null
            : "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "card_position": cardPosition == null ? null : cardPosition,
        "file_info": fileInfo == null
            ? null
            : List<dynamic>.from(fileInfo.map((x) => x)),
        "msg": msg == null ? null : msg,
        "name": name == null ? null : name,
        "color": color == null ? null : color,
        "to_name": toName == null ? null : toName,
        "msg_reference_id": msgReferenceId == null ? null : msgReferenceId,
        "reference_msg": referenceMsg == null ? null : referenceMsg,
        "reference_file_info": referenceFileInfo == null
            ? null
            : List<dynamic>.from(referenceFileInfo.map((x) => x)),
        "ref_to_name": refToName == null ? null : refToName,
        "ref_from_name": refFromName == null ? null : refFromName,
      };
}
