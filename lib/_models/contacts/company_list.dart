// To parse this JSON data, do
//
//     final contactCompanyList = contactCompanyListFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

List<ContactCompanyList> contactCompanyListFromJson(String str) =>
    List<ContactCompanyList>.from(
        json.decode(str).map((x) => ContactCompanyList.fromJson(x)));

String contactCompanyListToJson(List<ContactCompanyList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactCompanyList {
  ContactCompanyList({
    this.companyId,
    this.cId,
    this.name,
    this.state,
    this.createDate,
    this.modifyDate,
    this.count,
    this.spoc,
    this.filterHeader,
    this.color,
  });

  String companyId;
  String cId;
  String name;
  String state;
  String createDate;
  String modifyDate;
  int count;
  String spoc;
  String filterHeader;
  Color color;

  factory ContactCompanyList.fromJson(Map<String, dynamic> json) =>
      ContactCompanyList(
          companyId: json["company_id"] == null ? null : json["company_id"],
          cId: json["c_id"] == null ? null : json["c_id"],
          name: json["name"] == null ? null : json["name"],
          state: json["state"] == null ? null : json["state"],
          createDate: json["create_date"] == null ? null : json["create_date"],
          modifyDate: json["modify_date"] == null ? null : json["modify_date"],
          count: json["count"] == null ? null : json["count"],
          spoc: json["spoc"] == null ? null : json["spoc"],
          filterHeader:
              json["filter_header"] == null ? null : json["filter_header"],
          color: randomColor());

  Map<String, dynamic> toJson() => {
        "company_id": companyId == null ? null : companyId,
        "c_id": cId == null ? null : cId,
        "name": name == null ? null : name,
        "state": state == null ? null : state,
        "create_date": createDate == null ? null : createDate,
        "modify_date": modifyDate == null ? null : modifyDate,
        "count": count == null ? null : count,
        "spoc": spoc == null ? null : spoc,
        "filter_header": filterHeader == null ? null : filterHeader,
      };
}
