// To parse this JSON data, do
//
//     final contactAddCheck = contactAddCheckFromJson(jsonString);

import 'dart:convert';

ContactAddCheck contactAddCheckFromJson(String str) =>
    ContactAddCheck.fromJson(json.decode(str));

String contactAddCheckToJson(ContactAddCheck data) =>
    json.encode(data.toJson());

class ContactAddCheck {
  ContactAddCheck({
    this.formId,
  });

  String formId;

  factory ContactAddCheck.fromJson(Map<String, dynamic> json) =>
      ContactAddCheck(
        formId: json["form_id"],
      );

  Map<String, dynamic> toJson() => {
        "form_id": formId,
      };
}
