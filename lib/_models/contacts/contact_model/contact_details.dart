import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/_models/models.dart';

// ignore: must_be_immutable
class ContactDetailsModel extends Equatable {
  final String contactId;
  final String firstName;
  final String lastName;
  final String company;
  final String phoneNumber;
  String email;
  String service;
  String source;
  String group;
  String createdBy;

  ContactDetailsModel(
      {@required this.contactId,
      @required this.firstName,
      @required this.lastName,
      @required this.phoneNumber,
      this.company,
      this.email,
      this.service,
      this.source,
      this.group,
      this.createdBy});

  @override
  List<Object> get props => [];

  static ContactDetailsModel fromJson(dynamic json) {
    return ContactDetailsModel(
      contactId: textHandler(json['contact_id']),
      firstName: textHandler(json['firstname']),
      lastName: textHandler(json['lastname']),
      company: textHandler(json['company_name']),
      phoneNumber: textHandler(json['mobile']),
      email: textHandler(json['email']),
      service: textHandler(json['services']),
      source: textHandler(json['source']),
      group: textHandler(json['group']),
      createdBy: textHandler(json['created_by']),
    );
  }
}
