class ContactDetailsTabs {
  String iconName;
  String img;
  String tabId;
  String activeimg;

  ContactDetailsTabs({this.tabId, this.iconName, this.img, this.activeimg});
}

List<ContactDetailsTabs> contactDetailsTabs = [
  ContactDetailsTabs(
    tabId: "call",
    iconName: "Call",
    img: "/mobile_app_v2/o_call_purple.png",
    activeimg: "/mobile_app_v2/o_call_white.png",
  ),
  ContactDetailsTabs(
    tabId: "text",
    iconName: "Text",
    img: "/mobile_app_v2/o_text_purple.png",
    activeimg: "/mobile_app_v2/o_text_white.png",
  ),
  ContactDetailsTabs(
    tabId: "email",
    iconName: "Email",
    img: "/mobile_app_v2/o_email_purple.png",
    activeimg: "/mobile_app_v2/o_email_white.png",
  ),
  // ContactDetailsTabs(
  //   tabId: "task",
  //   iconName: "Task",
  //   img: "/mobile_app_v2/o_task_purple.png",
  //   activeimg: "/mobile_app_v2/o_task_white.png",
  // ),
  // ContactDetailsTabs(
  //   tabId: "note",
  //   iconName: "Note",
  //   img: "/mobile_app_v2/o_note_purple.png",
  //   activeimg: "/mobile_app_v2/o_note_white.png",
  // ),
  // ContactDetailsTabs(
  //   tabId: "lead",
  //   iconName: "Lead",
  //   img: "/mobile_app_v2/o_lead_purple.png",
  //   activeimg: "/mobile_app_v2/o_lead_white.png",
  // ),
  // ContactDetailsTabs(
  //   tabId: "ticket",
  //   iconName: "Ticket",
  //   img: "/mobile_app_v2/o_ticket_purple.png",
  //   activeimg: "/mobile_app_v2/o_ticket_white.png",
  // ),
  ContactDetailsTabs(
    tabId: "history",
    iconName: "History",
    img: "/mobile_app_v2/o_history_purple.png",
    activeimg: "/mobile_app_v2/o_history_white.png",
  ),
];

List<ContactDetailsTabs> contactHistoryTabs = [
  ContactDetailsTabs(
    tabId: "call",
    iconName: "Call",
    img: "/mobile_app_v2/o_call_purple.png",
    activeimg: "/mobile_app_v2/o_call_white.png",
  ),
  ContactDetailsTabs(
    tabId: "text",
    iconName: "Text",
    img: "/mobile_app_v2/o_text_purple.png",
    activeimg: "/mobile_app_v2/o_text_white.png",
  ),
  ContactDetailsTabs(
    tabId: "email",
    iconName: "Email",
    img: "/mobile_app_v2/o_email_purple.png",
    activeimg: "/mobile_app_v2/o_email_white.png",
  ),
  // ContactDetailsTabs(
  //   tabId: "task",
  //   iconName: "Task",
  //   img: "/mobile_app_v2/o_task_purple.png",
  //   activeimg: "/mobile_app_v2/o_task_white.png",
  // ),
];
