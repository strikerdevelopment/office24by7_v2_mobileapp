// To parse this JSON data, do
//
//     final contactFromEmail = contactFromEmailFromJson(jsonString);

import 'dart:convert';

List<ContactFromEmail> contactFromEmailFromJson(String str) =>
    List<ContactFromEmail>.from(
        json.decode(str).map((x) => ContactFromEmail.fromJson(x)));

String contactFromEmailToJson(List<ContactFromEmail> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactFromEmail {
  ContactFromEmail({
    this.seId,
    this.fromMail,
  });

  String seId;
  String fromMail;

  factory ContactFromEmail.fromJson(Map<String, dynamic> json) =>
      ContactFromEmail(
        seId: json["SE_ID"] == null ? null : json["SE_ID"],
        fromMail: json["FromMail"] == null ? null : json["FromMail"],
      );

  Map<String, dynamic> toJson() => {
        "SE_ID": seId == null ? null : seId,
        "FromMail": fromMail == null ? null : fromMail,
      };
}
