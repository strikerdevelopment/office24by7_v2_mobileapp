// To parse this JSON data, do
//
//     final getContactEmailId = getContactEmailIdFromJson(jsonString);

import 'dart:convert';

List<GetContactEmailId> getContactEmailIdFromJson(String str) =>
    List<GetContactEmailId>.from(
        json.decode(str).map((x) => GetContactEmailId.fromJson(x)));

String getContactEmailIdToJson(List<GetContactEmailId> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetContactEmailId {
  GetContactEmailId({
    this.seId,
    this.fromMail,
  });

  String seId;
  String fromMail;

  factory GetContactEmailId.fromJson(Map<String, dynamic> json) =>
      GetContactEmailId(
        seId: json["SE_ID"] == null ? null : json["SE_ID"],
        fromMail: json["FromMail"] == null ? null : json["FromMail"],
      );

  Map<String, dynamic> toJson() => {
        "SE_ID": seId == null ? null : seId,
        "FromMail": fromMail == null ? null : fromMail,
      };
}
