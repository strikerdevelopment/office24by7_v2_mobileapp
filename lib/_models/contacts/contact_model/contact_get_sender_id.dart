// To parse this JSON data, do
//
//     final getContactSenderId = getContactSenderIdFromJson(jsonString);

import 'dart:convert';

List<GetContactSenderId> getContactSenderIdFromJson(String str) =>
    List<GetContactSenderId>.from(
        json.decode(str).map((x) => GetContactSenderId.fromJson(x)));

String getContactSenderIdToJson(List<GetContactSenderId> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetContactSenderId {
  GetContactSenderId({
    this.sId,
    this.senderId,
  });

  String sId;
  String senderId;

  factory GetContactSenderId.fromJson(Map<String, dynamic> json) =>
      GetContactSenderId(
        sId: json["S_ID"] == null ? null : json["S_ID"],
        senderId: json["Sender_ID"] == null ? null : json["Sender_ID"],
      );

  Map<String, dynamic> toJson() => {
        "S_ID": sId == null ? null : sId,
        "Sender_ID": senderId == null ? null : senderId,
      };
}
