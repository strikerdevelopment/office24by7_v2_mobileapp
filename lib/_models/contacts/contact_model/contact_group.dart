// To parse this JSON data, do
//
//     final contactGroup = contactGroupFromJson(jsonString);

import 'dart:convert';

List<ContactGroup> contactGroupFromJson(String str) => List<ContactGroup>.from(
    json.decode(str).map((x) => ContactGroup.fromJson(x)));

String contactGroupToJson(List<ContactGroup> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactGroup {
  ContactGroup({
    this.contactGroupId,
    this.contactGroupDesc,
    this.isActive,
    this.contactEventId,
    this.isAuthenticate,
    this.authenticatePswd,
    this.isUpdate,
    this.isRemove,
    this.isDelete,
    this.isCreate,
    this.isCopy,
    this.isReadAndWrite,
    this.isView,
    this.companyId,
    this.orgId,
    this.createdDatetime,
    this.createdBy,
    this.modifiedDatetime,
    this.modifiedBy,
  });

  String contactGroupId;
  String contactGroupDesc;
  String isActive;
  dynamic contactEventId;
  String isAuthenticate;
  String authenticatePswd;
  String isUpdate;
  dynamic isRemove;
  String isDelete;
  String isCreate;
  String isCopy;
  String isReadAndWrite;
  String isView;
  String companyId;
  String orgId;
  DateTime createdDatetime;
  String createdBy;
  DateTime modifiedDatetime;
  String modifiedBy;

  factory ContactGroup.fromJson(Map<String, dynamic> json) => ContactGroup(
        contactGroupId: json["contact_group_id"],
        contactGroupDesc: json["contact_group_desc"],
        isActive: json["is_active"],
        contactEventId: json["contact_event_id"],
        isAuthenticate: json["Is_Authenticate"],
        authenticatePswd: json["Authenticate_PSWD"],
        isUpdate: json["Is_Update"],
        isRemove: json["Is_Remove"],
        isDelete: json["Is_Delete"],
        isCreate: json["Is_Create"],
        isCopy: json["Is_Copy"],
        isReadAndWrite: json["Is_Read_And_Write"],
        isView: json["Is_View"],
        companyId: json["company_id"],
        orgId: json["org_id"],
        createdDatetime: DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"],
        modifiedDatetime: DateTime.parse(json["modified_datetime"]),
        modifiedBy: json["modified_by"],
      );

  Map<String, dynamic> toJson() => {
        "contact_group_id": contactGroupId,
        "contact_group_desc": contactGroupDesc,
        "is_active": isActive,
        "contact_event_id": contactEventId,
        "Is_Authenticate": isAuthenticate,
        "Authenticate_PSWD": authenticatePswd,
        "Is_Update": isUpdate,
        "Is_Remove": isRemove,
        "Is_Delete": isDelete,
        "Is_Create": isCreate,
        "Is_Copy": isCopy,
        "Is_Read_And_Write": isReadAndWrite,
        "Is_View": isView,
        "company_id": companyId,
        "org_id": orgId,
        "created_datetime": createdDatetime.toIso8601String(),
        "created_by": createdBy,
        "modified_datetime": modifiedDatetime.toIso8601String(),
        "modified_by": modifiedBy,
      };
}
