// To parse this JSON data, do
//
//     final contactList = contactListFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

List<ContactList> contactListFromJson(String str) => List<ContactList>.from(
    json.decode(str).map((x) => ContactList.fromJson(x)));

String contactListToJson(List<ContactList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactList {
  ContactList({
    this.filterHeader,
    this.state,
    this.contactId,
    this.cId,
    this.groupId,
    this.contact,
    this.contactReferenceId,
    this.phoneCode,
    this.listContactUniqueId,
    this.displayStatus,
    this.isLead,
    this.isoCode,
    this.duplicateCount,
    this.contactCat,
    this.authenticatePswd,
    this.isAuthenticate,
    this.isView,
    this.isUpdate,
    this.isRemove,
    this.isDelete,
    this.isCreate,
    this.isCopy,
    this.isReadAndWrite,
    this.group,
    this.email,
    this.mobile,
    this.contactNumber,
    this.phoneNumber,
    this.companyName,
    this.firstname,
    this.orgin,
    this.assignedTo,
    this.lastname,
    this.color,
  });

  String filterHeader;
  String state;
  String contactId;
  String cId;
  String groupId;
  String contact;
  String contactReferenceId;
  String phoneCode;
  String listContactUniqueId;
  dynamic displayStatus;
  String isLead;
  String isoCode;
  dynamic duplicateCount;
  String contactCat;
  dynamic authenticatePswd;
  dynamic isAuthenticate;
  String isView;
  String isUpdate;
  dynamic isRemove;
  dynamic isDelete;
  String isCreate;
  String isCopy;
  String isReadAndWrite;
  String group;
  String email;
  dynamic mobile;
  dynamic contactNumber;
  dynamic phoneNumber;
  String companyName;
  String firstname;
  String orgin;
  String assignedTo;
  String lastname;
  Color color;

  factory ContactList.fromJson(Map<String, dynamic> json) => ContactList(
        filterHeader:
            json["filter_header"] == null ? null : json["filter_header"],
        state: json["state"] == null ? null : json["state"],
        contactId: json["contact_id"] == null ? null : json["contact_id"],
        cId: json["c_id"] == null ? null : json["c_id"],
        groupId: json["group_id"] == null ? null : json["group_id"],
        contact: json["contact"] == null ? null : json["contact"],
        contactReferenceId: json["contact_reference_id"] == null
            ? null
            : json["contact_reference_id"],
        phoneCode: json["phone_code"] == null ? null : json["phone_code"],
        listContactUniqueId: json["list_contact_unique_id"] == null
            ? null
            : json["list_contact_unique_id"],
        displayStatus:
            json["display_status"] == null ? null : json["display_status"],
        isLead: json["is_lead"] == null ? null : json["is_lead"],
        isoCode: json["iso_code"] == null ? null : json["iso_code"],
        duplicateCount:
            json["duplicate_count"] == null ? null : json["duplicate_count"],
        contactCat: json["contact_cat"] == null ? null : json["contact_cat"],
        authenticatePswd: json["authenticate_pswd"] == null
            ? null
            : json["authenticate_pswd"],
        isAuthenticate:
            json["is_authenticate"] == null ? null : json["is_authenticate"],
        isView: json["is_view"] == null ? null : json["is_view"],
        isUpdate: json["is_update"] == null ? null : json["is_update"],
        isRemove: json["is_remove"] == null ? null : json["is_remove"],
        isDelete: json["is_delete"] == null ? null : json["is_delete"],
        isCreate: json["is_create"] == null ? null : json["is_create"],
        isCopy: json["is_copy"] == null ? null : json["is_copy"],
        isReadAndWrite: json["is_read_and_write"] == null
            ? null
            : json["is_read_and_write"],
        group: json["group"] == null ? null : json["group"],
        email: json["email"] == null ? null : json["email"],
        mobile: json["mobile"] == null ? null : json["mobile"],
        contactNumber:
            json["contact_number"] == null ? null : json["contact_number"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        companyName: json["company_name"] == "" ? "null" : json["company_name"],
        firstname: json["firstname"] == null ? null : json["firstname"],
        orgin: json["orgin"] == null ? null : json["orgin"],
        assignedTo: json["assigned_to"] == null ? null : json["assigned_to"],
        lastname: json["lastname"] == null ? null : json["lastname"],
        color: randomColor(),
      );

  Map<String, dynamic> toJson() => {
        "filter_header": filterHeader == null ? null : filterHeader,
        "state": state == null ? null : state,
        "contact_id": contactId == null ? null : contactId,
        "c_id": cId == null ? null : cId,
        "group_id": groupId == null ? null : groupId,
        "contact": contact == null ? null : contact,
        "contact_reference_id":
            contactReferenceId == null ? null : contactReferenceId,
        "phone_code": phoneCode == null ? null : phoneCode,
        "list_contact_unique_id":
            listContactUniqueId == null ? null : listContactUniqueId,
        "display_status": displayStatus == null ? null : displayStatus,
        "is_lead": isLead == null ? null : isLead,
        "iso_code": isoCode == null ? null : isoCode,
        "duplicate_count": duplicateCount == null ? null : duplicateCount,
        "contact_cat": contactCat == null ? null : contactCat,
        "authenticate_pswd": authenticatePswd == null ? null : authenticatePswd,
        "is_authenticate": isAuthenticate == null ? null : isAuthenticate,
        "is_view": isView == null ? null : isView,
        "is_update": isUpdate == null ? null : isUpdate,
        "is_remove": isRemove == null ? null : isRemove,
        "is_delete": isDelete == null ? null : isDelete,
        "is_create": isCreate == null ? null : isCreate,
        "is_copy": isCopy == null ? null : isCopy,
        "is_read_and_write": isReadAndWrite == null ? null : isReadAndWrite,
        "group": group == null ? null : group,
        "email": email == null ? null : email,
        "mobile": mobile == null ? null : mobile,
        "contact_number": contactNumber == null ? null : contactNumber,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "company_name": companyName == null ? null : companyName,
        "firstname": firstname == null ? null : firstname,
        "orgin": orgin == null ? null : orgin,
        "assigned_to": assignedTo == null ? null : assignedTo,
        "lastname": lastname == null ? null : lastname,
      };
}
