// To parse this JSON data, do
//
//     final contactListDropdown = contactListDropdownFromJson(jsonString);

import 'dart:convert';

List<ContactListDropdown> contactListDropdownFromJson(String str) =>
    List<ContactListDropdown>.from(
        json.decode(str).map((x) => ContactListDropdown.fromJson(x)));

String contactListDropdownToJson(List<ContactListDropdown> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactListDropdown {
  ContactListDropdown({
    this.contactListId,
    this.listName,
  });

  String contactListId;
  String listName;

  factory ContactListDropdown.fromJson(Map<String, dynamic> json) =>
      ContactListDropdown(
        contactListId: json["contact_list_id"],
        listName: json["list_name"],
      );

  Map<String, dynamic> toJson() => {
        "contact_list_id": contactListId,
        "list_name": listName,
      };
}
