class CardContactOptions {
  String id;
  String img;
  String title;
  CardContactOptions({this.id, this.img, this.title});
}

List<CardContactOptions> cardContactOptions = [
  CardContactOptions(
      id: "contactDetails",
      img: "/mobile_app_v2/o_option_contact_details.png",
      title: "Contact Details"),
  CardContactOptions(
      id: "contactHistory",
      img: "/mobile_app_v2/o_option_history.png",
      title: "Contact History"),
  CardContactOptions(
      id: "meeting",
      img: "/mobile_app_v2/o_option_meeting.png",
      title: "Meeting"),
  CardContactOptions(
      id: "task", img: "/mobile_app_v2/o_option_task.png", title: "Task"),
  CardContactOptions(
      id: "addToLeads",
      img: "/mobile_app_v2/o_option_add_to_lead.png",
      title: "Add to Leads"),
  CardContactOptions(
      id: "addToList",
      img: "/mobile_app_v2/o_option_add_to_list.png",
      title: "Add to List"),
  CardContactOptions(
      id: "reassignContact",
      img: "/mobile_app_v2/o_option_reassign.png",
      title: "Reassign Contact"),
  CardContactOptions(
      id: "deleteContact",
      img: "/mobile_app_v2/o_option_delete.png",
      title: "Delete Contact"),
  CardContactOptions(
      id: "copyContact",
      img: "/mobile_app_v2/o_option_copy.png",
      title: "Copy Contact"),
  CardContactOptions(
      id: "editContact",
      img: "/mobile_app_v2/o_option_edit.png",
      title: "Edit Contact"),
  CardContactOptions(
      id: "viewDuplicate",
      img: "/mobile_app_v2/o_option_duplicate.png",
      title: "View Duplicate"),
];

List<CardContactOptions> cardContactGlobalOptions = [
  CardContactOptions(
      id: "deleteContact",
      img: "/mobile_app_v2/o_option_delete.png",
      title: "Delete Contact"),
  CardContactOptions(
      id: "reassignContact",
      img: "/mobile_app_v2/o_option_reassign.png",
      title: "Reassign Contact"),
  CardContactOptions(
      id: "newList",
      img: "/mobile_app_v2/o_option_add_list.png",
      title: "New List"),
  CardContactOptions(
      id: "addToList",
      img: "/mobile_app_v2/o_option_add_to_list.png",
      title: "Add to List"),
  // CardContactOptions(
  //     id: "dublicateFilter",
  //     img: "/mobile_app_v2/o_option_duplicate.png",
  //     title: "Dublicate Filter"),
  CardContactOptions(
      id: "sms", img: "/mobile_app_v2/o_option_sms.png", title: "SMS Campaign"),
  CardContactOptions(
      id: "email",
      img: "/mobile_app_v2/o_option_email.png",
      title: "Email Campaign"),
  CardContactOptions(
      id: "voice",
      img: "/mobile_app_v2/o_option_voice.png",
      title: "Voice Campaign"),
];

List<CardContactOptions> cardGroupGlobalOptions = [
  CardContactOptions(
      id: "deleteGroup",
      img: "/mobile_app_v2/o_option_delete.png",
      title: "Delete Group(s)"),
  CardContactOptions(
      id: "newList",
      img: "/mobile_app_v2/o_option_add_list.png",
      title: "New List"),
  CardContactOptions(
      id: "addToList",
      img: "/mobile_app_v2/o_option_add_to_list.png",
      title: "Add to List"),
  CardContactOptions(
      id: "sms", img: "/mobile_app_v2/o_option_sms.png", title: "SMS Campaign"),
  CardContactOptions(
      id: "email",
      img: "/mobile_app_v2/o_option_email.png",
      title: "Email Campaign"),
  CardContactOptions(
      id: "voice",
      img: "/mobile_app_v2/o_option_voice.png",
      title: "Voice Campaign"),
];

List<CardContactOptions> cardGroupOptions = [
  CardContactOptions(
      id: "viewContacts",
      img: "/mobile_app_v2/o_option_contact_details.png",
      title: "View Contacts"),
  CardContactOptions(
      id: "deleteGroup",
      img: "/mobile_app_v2/o_option_delete.png",
      title: "Delete Group"),
  CardContactOptions(
      id: "editGroup",
      img: "/mobile_app_v2/o_option_edit.png",
      title: "Edit Group"),
];

List<CardContactOptions> cardSourceOptions = [
  CardContactOptions(
      id: "viewContacts",
      img: "/mobile_app_v2/o_option_contact_details.png",
      title: "View Contacts"),
  CardContactOptions(
      id: "addToList",
      img: "/mobile_app_v2/o_option_add_to_list.png",
      title: "Add to List"),
];

List<CardContactOptions> cardSourceGlobalOptions = [
  CardContactOptions(
      id: "newList",
      img: "/mobile_app_v2/o_option_add_list.png",
      title: "New List"),
  CardContactOptions(
      id: "addToList",
      img: "/mobile_app_v2/o_option_add_to_list.png",
      title: "Add to List"),
  CardContactOptions(
      id: "sms", img: "/mobile_app_v2/o_option_sms.png", title: "SMS Campaign"),
  CardContactOptions(
      id: "email",
      img: "/mobile_app_v2/o_option_email.png",
      title: "Email Campaign"),
  CardContactOptions(
      id: "voice",
      img: "/mobile_app_v2/o_option_voice.png",
      title: "Voice Campaign"),
];

List<CardContactOptions> cardCompanyOptions = [
  CardContactOptions(
      id: "viewContacts",
      img: "/mobile_app_v2/o_option_contact_details.png",
      title: "View Contacts"),
];

List<CardContactOptions> cardCompanyGlobalOptions = [
  CardContactOptions(
      id: "newList",
      img: "/mobile_app_v2/o_option_add_list.png",
      title: "New List"),
  CardContactOptions(
      id: "addToList",
      img: "/mobile_app_v2/o_option_add_to_list.png",
      title: "Add to List"),
  CardContactOptions(
      id: "sms", img: "/mobile_app_v2/o_option_sms.png", title: "SMS Campaign"),
  CardContactOptions(
      id: "email",
      img: "/mobile_app_v2/o_option_email.png",
      title: "Email Campaign"),
  CardContactOptions(
      id: "voice",
      img: "/mobile_app_v2/o_option_voice.png",
      title: "Voice Campaign"),
];

List<CardContactOptions> cardListOptions = [
  CardContactOptions(
      id: "viewContacts",
      img: "/mobile_app_v2/o_option_contact_details.png",
      title: "View Contacts"),
  CardContactOptions(
      id: "deleteList",
      img: "/mobile_app_v2/o_option_delete.png",
      title: "Delete List"),
  CardContactOptions(
      id: "editList",
      img: "/mobile_app_v2/o_option_edit.png",
      title: "Edit List"),
];

List<CardContactOptions> cardListGlobalOptions = [
  CardContactOptions(
      id: "newList",
      img: "/mobile_app_v2/o_option_add_list.png",
      title: "New List"),
  CardContactOptions(
      id: "addToList",
      img: "/mobile_app_v2/o_option_add_to_list.png",
      title: "Add to List"),
  CardContactOptions(
      id: "sms", img: "/mobile_app_v2/o_option_sms.png", title: "SMS Campaign"),
  CardContactOptions(
      id: "email",
      img: "/mobile_app_v2/o_option_email.png",
      title: "Email Campaign"),
  CardContactOptions(
      id: "voice",
      img: "/mobile_app_v2/o_option_voice.png",
      title: "Voice Campaign"),
];
