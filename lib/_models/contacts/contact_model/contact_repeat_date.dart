// To parse this JSON data, do
//
//     final contactRepeatDate = contactRepeatDateFromJson(jsonString);

import 'dart:convert';

List<ContactRepeatDate> contactRepeatDateFromJson(String str) =>
    List<ContactRepeatDate>.from(
        json.decode(str).map((x) => ContactRepeatDate.fromJson(x)));

String contactRepeatDateToJson(List<ContactRepeatDate> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactRepeatDate {
  ContactRepeatDate({
    this.type,
    this.name,
  });

  String type;
  String name;

  factory ContactRepeatDate.fromJson(Map<String, dynamic> json) =>
      ContactRepeatDate(
        type: json["type"] == null ? null : json["type"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "name": name == null ? null : name,
      };
}

List<ContactRepeatDate> contactRepeatDate = [
  ContactRepeatDate(type: "Doesnotrepeat", name: "Does not repeat"),
  ContactRepeatDate(type: "Daily", name: "Daily"),
  ContactRepeatDate(type: "Weekly", name: "Weekly on Friday"),
  ContactRepeatDate(
      type: "Alternate_Weekly", name: "Alternate weekly on Friday"),
  ContactRepeatDate(type: "Monthly", name: "Monthly on 18"),
  ContactRepeatDate(
      type: "Every_weekday", name: "Every weekday (Monday to Firday)"),
  // ContactRepeatDate(type: "custom", name: "custom"),
];

List<ContactRepeatDate> contactEdson = [
  ContactRepeatDate(type: "endson", name: "On"),
  ContactRepeatDate(type: "endsafter", name: "After"),
];
