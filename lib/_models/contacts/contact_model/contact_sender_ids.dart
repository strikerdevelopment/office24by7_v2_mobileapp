// To parse this JSON data, do
//
//     final contactSenderIds = contactSenderIdsFromJson(jsonString);

import 'dart:convert';

List<ContactSenderIds> contactSenderIdsFromJson(String str) =>
    List<ContactSenderIds>.from(
        json.decode(str).map((x) => ContactSenderIds.fromJson(x)));

String contactSenderIdsToJson(List<ContactSenderIds> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactSenderIds {
  ContactSenderIds({
    this.sId,
    this.senderId,
  });

  String sId;
  String senderId;

  factory ContactSenderIds.fromJson(Map<String, dynamic> json) =>
      ContactSenderIds(
        sId: json["S_ID"] == null ? null : json["S_ID"],
        senderId: json["Sender_ID"] == null ? null : json["Sender_ID"],
      );

  Map<String, dynamic> toJson() => {
        "S_ID": sId == null ? null : sId,
        "Sender_ID": senderId == null ? null : senderId,
      };
}
