// To parse this JSON data, do
//
//     final contactSource = contactSourceFromJson(jsonString);

import 'dart:convert';

List<ContactSource> contactSourceFromJson(String str) =>
    List<ContactSource>.from(
        json.decode(str).map((x) => ContactSource.fromJson(x)));

String contactSourceToJson(List<ContactSource> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactSource {
  ContactSource({
    this.contactSourceId,
    this.contactSourceDesc,
    this.sourceOrder,
    this.sourceTypeId,
    this.sourceTypeValue,
    this.isActive,
    this.companyId,
    this.orgId,
    this.createdDatetime,
    this.createdBy,
    this.modifiedDatetime,
    this.modifiedBy,
  });

  String contactSourceId;
  String contactSourceDesc;
  String sourceOrder;
  String sourceTypeId;
  String sourceTypeValue;
  String isActive;
  String companyId;
  String orgId;
  DateTime createdDatetime;
  String createdBy;
  DateTime modifiedDatetime;
  String modifiedBy;

  factory ContactSource.fromJson(Map<String, dynamic> json) => ContactSource(
        contactSourceId: json["contact_source_id"],
        contactSourceDesc: json["contact_source_desc"],
        sourceOrder: json["source_order"],
        sourceTypeId: json["source_type_id"],
        sourceTypeValue: json["source_type_value"],
        isActive: json["is_active"],
        companyId: json["company_id"],
        orgId: json["org_id"],
        createdDatetime: DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"],
        modifiedDatetime: DateTime.parse(json["modified_datetime"]),
        modifiedBy: json["modified_by"],
      );

  Map<String, dynamic> toJson() => {
        "contact_source_id": contactSourceId,
        "contact_source_desc": contactSourceDesc,
        "source_order": sourceOrder,
        "source_type_id": sourceTypeId,
        "source_type_value": sourceTypeValue,
        "is_active": isActive,
        "company_id": companyId,
        "org_id": orgId,
        "created_datetime": createdDatetime.toIso8601String(),
        "created_by": createdBy,
        "modified_datetime": modifiedDatetime.toIso8601String(),
        "modified_by": modifiedBy,
      };
}
