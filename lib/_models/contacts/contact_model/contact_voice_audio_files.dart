// To parse this JSON data, do
//
//     final contactVoiceAudioFiles = contactVoiceAudioFilesFromJson(jsonString);

import 'dart:convert';

List<ContactVoiceAudioFiles> contactVoiceAudioFilesFromJson(String str) =>
    List<ContactVoiceAudioFiles>.from(
        json.decode(str).map((x) => ContactVoiceAudioFiles.fromJson(x)));

String contactVoiceAudioFilesToJson(List<ContactVoiceAudioFiles> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactVoiceAudioFiles {
  ContactVoiceAudioFiles({
    this.id,
    this.ivrDesc,
    this.ivrFilename,
    this.ivrLocation,
    this.fileType,
  });

  String id;
  String ivrDesc;
  String ivrFilename;
  String ivrLocation;
  ContactFileType fileType;

  factory ContactVoiceAudioFiles.fromJson(Map<String, dynamic> json) =>
      ContactVoiceAudioFiles(
        id: json["ID"] == null ? null : json["ID"],
        ivrDesc: json["ivr_desc"] == null ? null : json["ivr_desc"],
        ivrFilename: json["ivr_filename"] == null ? null : json["ivr_filename"],
        ivrLocation: json["ivr_location"] == null ? null : json["ivr_location"],
        fileType: json["file_type"] == null
            ? null
            : fileTypeValues.map[json["file_type"]],
      );

  Map<String, dynamic> toJson() => {
        "ID": id == null ? null : id,
        "ivr_desc": ivrDesc == null ? null : ivrDesc,
        "ivr_filename": ivrFilename == null ? null : ivrFilename,
        "ivr_location": ivrLocation == null ? null : ivrLocation,
        "file_type": fileType == null ? null : fileTypeValues.reverse[fileType],
      };
}

enum ContactFileType { F }

final fileTypeValues = EnumValues({"F": ContactFileType.F});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
