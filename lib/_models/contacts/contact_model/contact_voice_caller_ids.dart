// To parse this JSON data, do
//
//     final contactVoiceCallerIds = contactVoiceCallerIdsFromJson(jsonString);

import 'dart:convert';

List<ContactVoiceCallerIds> contactVoiceCallerIdsFromJson(String str) =>
    List<ContactVoiceCallerIds>.from(
        json.decode(str).map((x) => ContactVoiceCallerIds.fromJson(x)));

String contactVoiceCallerIdsToJson(List<ContactVoiceCallerIds> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactVoiceCallerIds {
  ContactVoiceCallerIds({
    this.id,
    this.calleridNum,
    this.didNumber,
  });

  String id;
  String calleridNum;
  String didNumber;

  factory ContactVoiceCallerIds.fromJson(Map<String, dynamic> json) =>
      ContactVoiceCallerIds(
        id: json["ID"] == null ? null : json["ID"],
        calleridNum: json["callerid_num"] == null ? null : json["callerid_num"],
        didNumber: json["did_number"] == null ? null : json["did_number"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id == null ? null : id,
        "callerid_num": calleridNum == null ? null : calleridNum,
        "did_number": didNumber == null ? null : didNumber,
      };
}
