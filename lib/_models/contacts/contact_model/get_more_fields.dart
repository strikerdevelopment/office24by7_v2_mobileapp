// To parse this JSON data, do
//
//     final getMoreFields = getMoreFieldsFromJson(jsonString);

import 'dart:convert';

List<GetMoreFields> getMoreFieldsFromJson(String str) =>
    List<GetMoreFields>.from(
        json.decode(str).map((x) => GetMoreFields.fromJson(x)));

String getMoreFieldsToJson(List<GetMoreFields> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GetMoreFields {
  GetMoreFields({
    this.fieldId,
    this.fieldFor,
    this.fieldName,
    this.fieldLabel,
    this.fieldDesc,
    this.fieldType,
    this.fieldDefaultValue,
    this.fieldProperties,
    this.isRequired,
    this.validationType,
    this.isDisplayGridview,
    this.isDisplayTableview,
    this.isDependent,
    this.chieldFieldId,
    this.isSystemField,
    this.isStandardField,
    this.orgId,
    this.isActive,
    this.createdDatetime,
    this.createdBy,
    this.modifiedDatetime,
    this.modifiedBy,
    this.userFor,
    this.attributeType,
  });

  String fieldId;
  String fieldFor;
  String fieldName;
  String fieldLabel;
  String fieldDesc;
  String fieldType;
  String fieldDefaultValue;
  String fieldProperties;
  String isRequired;
  String validationType;
  String isDisplayGridview;
  String isDisplayTableview;
  String isDependent;
  String chieldFieldId;
  String isSystemField;
  String isStandardField;
  String orgId;
  String isActive;
  DateTime createdDatetime;
  String createdBy;
  DateTime modifiedDatetime;
  String modifiedBy;
  String userFor;
  String attributeType;

  factory GetMoreFields.fromJson(Map<String, dynamic> json) => GetMoreFields(
        fieldId: json["field_id"],
        fieldFor: json["field_for"],
        fieldName: json["field_name"],
        fieldLabel: json["field_label"],
        fieldDesc: json["field_desc"],
        fieldType: json["field_type"],
        fieldDefaultValue: json["field_default_value"],
        fieldProperties: json["field_properties"],
        isRequired: json["is_required"],
        validationType: json["validation_type"],
        isDisplayGridview: json["is_display_gridview"],
        isDisplayTableview: json["is_display_tableview"],
        isDependent: json["is_dependent"],
        chieldFieldId: json["chield_field_id"],
        isSystemField: json["is_system_field"],
        isStandardField: json["is_standard_field"],
        orgId: json["org_id"],
        isActive: json["is_active"],
        createdDatetime: DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"],
        modifiedDatetime: DateTime.parse(json["modified_datetime"]),
        modifiedBy: json["modified_by"],
        userFor: json["user_for"],
        attributeType: json["attribute_type"],
      );

  Map<String, dynamic> toJson() => {
        "field_id": fieldId,
        "field_for": fieldFor,
        "field_name": fieldName,
        "field_label": fieldLabel,
        "field_desc": fieldDesc,
        "field_type": fieldType,
        "field_default_value": fieldDefaultValue,
        "field_properties": fieldProperties,
        "is_required": isRequired,
        "validation_type": validationType,
        "is_display_gridview": isDisplayGridview,
        "is_display_tableview": isDisplayTableview,
        "is_dependent": isDependent,
        "chield_field_id": chieldFieldId,
        "is_system_field": isSystemField,
        "is_standard_field": isStandardField,
        "org_id": orgId,
        "is_active": isActive,
        "created_datetime": createdDatetime.toIso8601String(),
        "created_by": createdBy,
        "modified_datetime": modifiedDatetime.toIso8601String(),
        "modified_by": modifiedBy,
        "user_for": userFor,
        "attribute_type": attributeType,
      };
}
