// To parse this JSON data, do
//
//     final contactGroupList = contactGroupListFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

List<ContactGroupList> contactGroupListFromJson(String str) =>
    List<ContactGroupList>.from(
        json.decode(str).map((x) => ContactGroupList.fromJson(x)));

String contactGroupListToJson(List<ContactGroupList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactGroupList {
  ContactGroupList({
    this.groupId,
    this.gId,
    this.name,
    this.isAuthenticate,
    this.authenticatePswd,
    this.isUpdate,
    this.isRemove,
    this.isDelete,
    this.isCreate,
    this.isCopy,
    this.isReadAndWrite,
    this.isDisabale,
    this.state,
    this.createDate,
    this.modifyDate,
    this.count,
    this.spoc,
    this.createdBy,
    this.filterHeader,
    this.isView,
    this.color,
  });

  String groupId;
  String gId;
  String name;
  String isAuthenticate;
  String authenticatePswd;
  String isUpdate;
  String isRemove;
  String isDelete;
  String isCreate;
  String isCopy;
  String isReadAndWrite;
  String isDisabale;
  String state;
  String createDate;
  String modifyDate;
  dynamic count;
  String spoc;
  String createdBy;
  String filterHeader;
  String isView;
  Color color;

  factory ContactGroupList.fromJson(Map<String, dynamic> json) =>
      ContactGroupList(
        groupId: json["group_id"] == null ? null : json["group_id"],
        gId: json["g_id"] == null ? null : json["g_id"],
        name: json["name"] == null ? null : json["name"],
        isAuthenticate:
            json["Is_Authenticate"] == null ? null : json["Is_Authenticate"],
        authenticatePswd: json["Authenticate_PSWD"] == null
            ? null
            : json["Authenticate_PSWD"],
        isUpdate: json["Is_Update"] == null ? null : json["Is_Update"],
        isRemove: json["Is_Remove"] == null ? null : json["Is_Remove"],
        isDelete: json["Is_Delete"] == null ? null : json["Is_Delete"],
        isCreate: json["Is_Create"] == null ? null : json["Is_Create"],
        isCopy: json["Is_Copy"] == null ? null : json["Is_Copy"],
        isReadAndWrite: json["Is_Read_And_Write"] == null
            ? null
            : json["Is_Read_And_Write"],
        isDisabale: json["is_disabale"] == null ? null : json["is_disabale"],
        state: json["state"] == null ? null : json["state"],
        createDate: json["create_date"] == null ? null : json["create_date"],
        modifyDate: json["modify_date"] == null ? null : json["modify_date"],
        count: json["count"],
        spoc: json["spoc"] == null ? null : json["spoc"],
        createdBy: json["created_by"] == null ? null : json["created_by"],
        filterHeader:
            json["filter_header"] == null ? null : json["filter_header"],
        isView: json["Is_View"] == null ? null : json["Is_View"],
        color: randomColor(),
      );

  Map<String, dynamic> toJson() => {
        "group_id": groupId == null ? null : groupId,
        "g_id": gId == null ? null : gId,
        "name": name == null ? null : name,
        "Is_Authenticate": isAuthenticate == null ? null : isAuthenticate,
        "Authenticate_PSWD": authenticatePswd == null ? null : authenticatePswd,
        "Is_Update": isUpdate == null ? null : isUpdate,
        "Is_Remove": isRemove == null ? null : isRemove,
        "Is_Delete": isDelete == null ? null : isDelete,
        "Is_Create": isCreate == null ? null : isCreate,
        "Is_Copy": isCopy == null ? null : isCopy,
        "Is_Read_And_Write": isReadAndWrite == null ? null : isReadAndWrite,
        "is_disabale": isDisabale == null ? null : isDisabale,
        "state": state == null ? null : state,
        "create_date": createDate == null ? null : createDate,
        "modify_date": modifyDate == null ? null : modifyDate,
        "count": count,
        "spoc": spoc == null ? null : spoc,
        "created_by": createdBy == null ? null : createdBy,
        "filter_header": filterHeader == null ? null : filterHeader,
        "Is_View": isView == null ? null : isView,
      };
}
