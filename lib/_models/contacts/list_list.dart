// To parse this JSON data, do
//
//     final contactListList = contactListListFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

List<ContactListList> contactListListFromJson(String str) =>
    List<ContactListList>.from(
        json.decode(str).map((x) => ContactListList.fromJson(x)));

String contactListListToJson(List<ContactListList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactListList {
  ContactListList({
    this.listId,
    this.lId,
    this.name,
    this.state,
    this.createDate,
    this.modifyDate,
    this.count,
    this.spoc,
    this.filterHeader,
    this.createdBy,
    this.color,
  });

  String listId;
  String lId;
  String name;
  String state;
  String createDate;
  String modifyDate;
  int count;
  String spoc;
  String filterHeader;
  String createdBy;
  Color color;

  factory ContactListList.fromJson(Map<String, dynamic> json) =>
      ContactListList(
        listId: json["list_id"] == null ? null : json["list_id"],
        lId: json["l_id"] == null ? null : json["l_id"],
        name: json["name"] == null ? null : json["name"],
        state: json["state"] == null ? null : json["state"],
        createDate: json["create_date"] == null ? null : json["create_date"],
        modifyDate: json["modify_date"] == null ? null : json["modify_date"],
        count: json["count"] == null ? null : json["count"],
        spoc: json["spoc"] == null ? null : json["spoc"],
        filterHeader:
            json["filter_header"] == null ? null : json["filter_header"],
        createdBy: json["created_by"] == null ? null : json["created_by"],
        color: randomColor(),
      );

  Map<String, dynamic> toJson() => {
        "list_id": listId == null ? null : listId,
        "l_id": lId == null ? null : lId,
        "name": name == null ? null : name,
        "state": state == null ? null : state,
        "create_date": createDate == null ? null : createDate,
        "modify_date": modifyDate == null ? null : modifyDate,
        "count": count == null ? null : count,
        "spoc": spoc == null ? null : spoc,
        "filter_header": filterHeader == null ? null : filterHeader,
        "created_by": createdBy == null ? null : createdBy,
      };
}
