// To parse this JSON data, do
//
//     final contactSourceList = contactSourceListFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

List<ContactSourceList> contactSourceListFromJson(String str) =>
    List<ContactSourceList>.from(
        json.decode(str).map((x) => ContactSourceList.fromJson(x)));

String contactSourceListToJson(List<ContactSourceList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContactSourceList {
  ContactSourceList({
    this.sourceId,
    this.sId,
    this.name,
    this.state,
    this.createDate,
    this.modifyDate,
    this.count,
    this.origin,
    this.originEn,
    this.spoc,
    this.filterHeader,
    this.color,
  });

  String sourceId;
  String sId;
  String name;
  String state;
  String createDate;
  String modifyDate;
  dynamic count;
  String origin;
  String originEn;
  String spoc;
  String filterHeader;
  Color color;

  factory ContactSourceList.fromJson(Map<String, dynamic> json) =>
      ContactSourceList(
        sourceId: json["source_id"] == null ? null : json["source_id"],
        sId: json["s_id"] == null ? null : json["s_id"],
        name: json["name"] == null ? null : json["name"],
        state: json["state"] == null ? null : json["state"],
        createDate: json["create_date"] == null ? null : json["create_date"],
        modifyDate: json["modify_date"] == null ? null : json["modify_date"],
        count: json["count"],
        origin: json["origin"] == null ? null : json["origin"],
        originEn: json["origin_en"] == null ? null : json["origin_en"],
        spoc: json["spoc"] == null ? null : json["spoc"],
        filterHeader:
            json["filter_header"] == null ? null : json["filter_header"],
        color: randomColor(),
      );

  Map<String, dynamic> toJson() => {
        "source_id": sourceId == null ? null : sourceId,
        "s_id": sId == null ? null : sId,
        "name": name == null ? null : name,
        "state": state == null ? null : state,
        "create_date": createDate == null ? null : createDate,
        "modify_date": modifyDate == null ? null : modifyDate,
        "count": count,
        "origin": origin == null ? null : origin,
        "origin_en": originEn == null ? null : originEn,
        "spoc": spoc == null ? null : spoc,
        "filter_header": filterHeader == null ? null : filterHeader,
      };
}
