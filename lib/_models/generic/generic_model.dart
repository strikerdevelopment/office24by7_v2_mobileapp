class GenericModel {
  String apiName;
  int countryID;
  String fieldid;
  String parentFieldOptionId;
  String versionNumber;
  String category;

  GenericModel({
    this.apiName,
    this.countryID,
    this.fieldid,
    this.parentFieldOptionId,
    this.versionNumber,
    this.category,
  });
}
