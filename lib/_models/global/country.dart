// To parse this JSON data, do
//
//     final country = countryFromJson(jsonString);

import 'dart:convert';

List<Country> countryFromJson(String str) =>
    List<Country>.from(json.decode(str).map((x) => Country.fromJson(x)));

String countryToJson(List<Country> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Country {
  Country({
    this.id,
    this.name,
    this.phonecode,
    this.phoneNumberLength,
    this.currency,
  });

  String id;
  String name;
  String phonecode;
  String phoneNumberLength;
  String currency;

  factory Country.fromJson(Map<String, dynamic> json) => Country(
        id: json["id"],
        name: json["name"],
        phonecode: json["phonecode"],
        phoneNumberLength: json["phone_number_length"],
        currency: json["currency"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phonecode": phonecode,
        "phone_number_length": phoneNumberLength,
        "currency": currency,
      };
}
