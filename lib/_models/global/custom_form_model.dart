// To parse this JSON data, do
//
//     final customeFormModel = customeFormModelFromJson(jsonString);

import 'dart:convert';

List<CustomeFormModel> customeFormModelFromJson(str) =>
    List<CustomeFormModel>.from(str.map((x) => CustomeFormModel.fromJson(x)));

String customeFormModelToJson(List<CustomeFormModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CustomeFormModel {
  CustomeFormModel({
    this.formId,
    this.fieldId,
    this.fieldName,
    this.fieldLabel,
    this.fieldDesc,
    this.fieldType,
    this.fieldOrder,
    this.fieldDefaultValue,
    this.isRequired,
    this.validationType,
    this.isDisplayGridview,
    this.isSystemField,
    this.isStandardField,
    this.isDependent,
    this.chieldFieldId,
    this.isParentDependent,
  });

  String formId;
  String fieldId;
  String fieldName;
  String fieldLabel;
  String fieldDesc;
  String fieldType;
  String fieldOrder;
  String fieldDefaultValue;
  String isRequired;
  String validationType;
  String isDisplayGridview;
  String isSystemField;
  String isStandardField;
  String isDependent;
  String chieldFieldId;
  String isParentDependent;

  factory CustomeFormModel.fromJson(Map<String, dynamic> json) =>
      CustomeFormModel(
        formId: json["form_id"],
        fieldId: json["field_id"],
        fieldName: json["field_name"],
        fieldLabel: json["field_label"],
        fieldDesc: json["field_desc"],
        fieldType: json["field_type"],
        fieldOrder: json["field_order"],
        fieldDefaultValue: json["field_default_value"],
        isRequired: json["is_required"],
        validationType: json["validation_type"],
        isDisplayGridview: json["is_display_gridview"],
        isSystemField: json["is_system_field"],
        isStandardField: json["is_standard_field"],
        isDependent: json["is_dependent"],
        chieldFieldId: json["chield_field_id"],
        isParentDependent: json["isDependent"],
      );

  Map<String, dynamic> toJson() => {
        "form_id": formId,
        "field_id": fieldId,
        "field_name": fieldName,
        "field_label": fieldLabel,
        "field_desc": fieldDesc,
        "field_type": fieldType,
        "field_order": fieldOrder,
        "field_default_value": fieldDefaultValue,
        "is_required": isRequired,
        "validation_type": validationType,
        "is_display_gridview": isDisplayGridview,
        "is_system_field": isSystemField,
        "is_standard_field": isStandardField,
        "is_dependent": isDependent,
        "chield_field_id": chieldFieldId,
        "isDependent": isParentDependent,
      };
}
