// To parse this JSON data, do
//
//     final dependencyFieldsModel = dependencyFieldsModelFromJson(jsonString);

import 'dart:convert';

List<DependencyFieldsModel> dependencyFieldsModelFromJson(str) =>
    List<DependencyFieldsModel>.from(
        str.map((x) => DependencyFieldsModel.fromJson(x)));

String dependencyFieldsModelToJson(List<DependencyFieldsModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DependencyFieldsModel {
  DependencyFieldsModel({
    this.fieldOptionId,
    this.optionKey,
    this.optionValue,
    this.fieldId,
  });

  String fieldOptionId;
  String optionKey;
  String optionValue;
  String fieldId;

  factory DependencyFieldsModel.fromJson(Map<String, dynamic> json) =>
      DependencyFieldsModel(
        fieldOptionId: json["field_option_id"],
        optionKey: json["option_key"],
        optionValue: json["option_value"],
        fieldId: json["field_id"],
      );

  Map<String, dynamic> toJson() => {
        "field_option_id": fieldOptionId,
        "option_key": optionKey,
        "option_value": optionValue,
        "field_id": fieldId,
      };
}
