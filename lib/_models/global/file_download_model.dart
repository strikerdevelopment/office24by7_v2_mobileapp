class FileDownload {
  bool downloading;
  String progressString;
  double progressDouble;
  String filePath;

  FileDownload({
    this.downloading,
    this.progressString,
    this.progressDouble,
    this.filePath,
  });
}
