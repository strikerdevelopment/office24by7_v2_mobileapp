// To parse this JSON data, do
//
//     final globalHistoryModel = globalHistoryModelFromJson(jsonString);

import 'dart:convert';

List<GlobalHistoryModel> globalHistoryModelFromJson(String str) =>
    List<GlobalHistoryModel>.from(
        json.decode(str).map((x) => GlobalHistoryModel.fromJson(x)));

String globalHistoryModelToJson(List<GlobalHistoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GlobalHistoryModel {
  GlobalHistoryModel({
    this.historydate,
    this.historyrepeat,
  });

  String historydate;
  List<Historyrepeat> historyrepeat;

  factory GlobalHistoryModel.fromJson(Map<String, dynamic> json) =>
      GlobalHistoryModel(
        historydate: json["historydate"],
        historyrepeat: List<Historyrepeat>.from(
            json["historyrepeat"].map((x) => Historyrepeat.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "historydate": historydate,
        "historyrepeat":
            List<dynamic>.from(historyrepeat.map((x) => x.toJson())),
      };
}

class Historyrepeat {
  Historyrepeat({
    this.activityId,
    this.time,
    this.type,
    this.addedBy,
    this.spoc,
    this.typeimg,
    this.description,
  });

  String activityId;
  String time;
  String type;
  String addedBy;
  String spoc;
  String typeimg;
  String description;

  factory Historyrepeat.fromJson(Map<String, dynamic> json) => Historyrepeat(
        activityId: json["activity_id"],
        time: json["time"],
        type: json["type"],
        addedBy: json["added_by"],
        spoc: json["spoc"],
        typeimg: json["typeimg"],
        description: json["Description"],
      );

  Map<String, dynamic> toJson() => {
        "activity_id": activityId,
        "time": time,
        "type": type,
        "added_by": addedBy,
        "spoc": spoc,
        "typeimg": typeimg,
        "Description": description,
      };
}

List<GlobalHistoryModel> contactHistoryData = [
  GlobalHistoryModel(
    historydate: "September 21, 2020",
    historyrepeat: [
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
    ],
  ),
  GlobalHistoryModel(
    historydate: "September 21, 2020",
    historyrepeat: [
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
    ],
  ),
  GlobalHistoryModel(
    historydate: "September 21, 2020",
    historyrepeat: [
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
    ],
  ),
  GlobalHistoryModel(
    historydate: "September 21, 2020",
    historyrepeat: [
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
    ],
  ),
  GlobalHistoryModel(
    historydate: "September 21, 2020",
    historyrepeat: [
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
      Historyrepeat(
        activityId: "523446",
        time: "10:29 PM",
        type: "Meeting",
        addedBy: "Santosh Naagula",
        spoc: "Santosh Naagula",
        typeimg:
            "https://uat.office24by7.com/cdn/assets/img/contacts/c_cs5_history_meeting.svg",
        description:
            "afadfadf123456789 meeting created with description 123456",
      ),
    ],
  ),
];
