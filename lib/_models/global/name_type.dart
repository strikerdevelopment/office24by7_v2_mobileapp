class TypeName {
  final String type;
  final String name;
  const TypeName({this.type, this.name});
}

List<TypeName> weekData = [
  TypeName(type: "Mon", name: "Monday"),
  TypeName(type: "Tue", name: "Tuesday"),
  TypeName(type: "Wed", name: "Wednesday"),
  TypeName(type: "Thu", name: "Thursday"),
  TypeName(type: "Fri", name: "Friday"),
  TypeName(type: "Sat", name: "Saturday"),
  TypeName(type: "Sun", name: "Sunday"),
];

List<TypeName> dateTypeDd = [
  TypeName(type: "created_datetime", name: "Created Date"),
  TypeName(type: "modified_date", name: "Modified Date"),
];

List<TypeName> displayAs = [
  TypeName(type: "status", name: "Status"),
  TypeName(type: "division", name: "Divisions"),
  TypeName(type: "users", name: "User"),
  TypeName(type: "priority", name: "Priority"),
  TypeName(type: "forms", name: "Form"),
  TypeName(type: "source", name: "Source"),
];

List<TypeName> typeOfLead = [
  TypeName(type: "all", name: "All"),
  TypeName(type: "assigned_to_me", name: "Assigned to me"),
  TypeName(type: "assigned_by_me", name: "Assinged by me"),
  TypeName(type: "unassigned", name: "Unassigned"),
];
