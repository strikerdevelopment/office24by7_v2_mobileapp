// To parse this JSON data, do
//
//     final gpsHierarchyUsersModel = gpsHierarchyUsersModelFromJson(jsonString);

import 'dart:convert';

List<GpsHierarchyUsersModel> gpsHierarchyUsersModelFromJson(String str) =>
    List<GpsHierarchyUsersModel>.from(
        json.decode(str).map((x) => GpsHierarchyUsersModel.fromJson(x)));

String gpsHierarchyUsersModelToJson(List<GpsHierarchyUsersModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GpsHierarchyUsersModel {
  GpsHierarchyUsersModel({
    this.userId,
    this.loginId,
    this.userRole,
    this.firstName,
    this.lastName,
    this.mobileNumber,
    this.reportUserId,
    this.orgId,
    this.companyId,
    this.id,
    this.name,
  });

  String userId;
  String loginId;
  String userRole;
  String firstName;
  String lastName;
  String mobileNumber;
  dynamic reportUserId;
  String orgId;
  String companyId;
  String id;
  String name;

  factory GpsHierarchyUsersModel.fromJson(Map<String, dynamic> json) =>
      GpsHierarchyUsersModel(
        userId: json["User_ID"] == null ? null : json["User_ID"],
        loginId: json["Login_ID"] == null ? null : json["Login_ID"],
        userRole: json["User_Role"] == null ? null : json["User_Role"],
        firstName: json["First_Name"] == null ? null : json["First_Name"],
        lastName: json["Last_Name"] == null ? null : json["Last_Name"],
        mobileNumber:
            json["Mobile_Number"] == null ? null : json["Mobile_Number"],
        reportUserId: json["Report_User_ID"],
        orgId: json["Org_ID"] == null ? null : json["Org_ID"],
        companyId: json["Company_ID"] == null ? null : json["Company_ID"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "User_ID": userId == null ? null : userId,
        "Login_ID": loginId == null ? null : loginId,
        "User_Role": userRole == null ? null : userRole,
        "First_Name": firstName == null ? null : firstName,
        "Last_Name": lastName == null ? null : lastName,
        "Mobile_Number": mobileNumber == null ? null : mobileNumber,
        "Report_User_ID": reportUserId,
        "Org_ID": orgId == null ? null : orgId,
        "Company_ID": companyId == null ? null : companyId,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}
