// To parse this JSON data, do
//
//     final gpsLocationHistoryModel = gpsLocationHistoryModelFromJson(jsonString);

import 'dart:convert';

List<GpsLocationHistoryModel> gpsLocationHistoryModelFromJson(String str) =>
    List<GpsLocationHistoryModel>.from(
        json.decode(str).map((x) => GpsLocationHistoryModel.fromJson(x)));

String gpsLocationHistoryModelToJson(List<GpsLocationHistoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GpsLocationHistoryModel {
  GpsLocationHistoryModel({
    this.id,
    this.date,
    this.historyFields,
  });

  int id;
  DateTime date;
  List<GpsHistoryField> historyFields;

  factory GpsLocationHistoryModel.fromJson(Map<String, dynamic> json) =>
      GpsLocationHistoryModel(
        id: json["id"] == null ? null : json["id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        historyFields: json["historyFields"] == null
            ? null
            : List<GpsHistoryField>.from(
                json["historyFields"].map((x) => GpsHistoryField.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "date": date == null
            ? null
            : "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "historyFields": historyFields == null
            ? null
            : List<dynamic>.from(historyFields.map((x) => x.toJson())),
      };
}

class GpsHistoryField {
  GpsHistoryField({
    this.img,
    this.type,
    this.time,
    this.heading,
    this.user,
    this.description,
  });

  String img;
  String type;
  String time;
  String heading;
  String user;
  String description;

  factory GpsHistoryField.fromJson(Map<String, dynamic> json) =>
      GpsHistoryField(
        img: json["img"] == null ? null : json["img"],
        type: json["type"] == null ? null : json["type"],
        time: json["time"] == null ? null : json["time"],
        heading: json["heading"] == null ? null : json["heading"],
        user: json["user"] == null ? null : json["user"],
        description: json["description"] == null ? null : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "img": img == null ? null : img,
        "type": type == null ? null : type,
        "time": time == null ? null : time,
        "heading": heading == null ? null : heading,
        "user": user == null ? null : user,
        "description": description == null ? null : description,
      };
}
