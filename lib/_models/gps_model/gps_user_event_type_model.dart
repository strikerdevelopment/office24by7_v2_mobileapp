// To parse this JSON data, do
//
//     final gpsUserEventTypeModel = gpsUserEventTypeModelFromJson(jsonString);

import 'dart:convert';

List<GpsUserEventTypeModel> gpsUserEventTypeModelFromJson(String str) =>
    List<GpsUserEventTypeModel>.from(
        json.decode(str).map((x) => GpsUserEventTypeModel.fromJson(x)));

String gpsUserEventTypeModelToJson(List<GpsUserEventTypeModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GpsUserEventTypeModel {
  GpsUserEventTypeModel({
    this.id,
    this.name,
  });

  dynamic id;
  String name;

  factory GpsUserEventTypeModel.fromJson(Map<String, dynamic> json) =>
      GpsUserEventTypeModel(
        id: json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name == null ? null : name,
      };
}
