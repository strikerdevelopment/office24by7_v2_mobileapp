// To parse this JSON data, do
//
//     final gpsUserEventsModel = gpsUserEventsModelFromJson(jsonString);

import 'dart:convert';

List<GpsUserEventsModel> gpsUserEventsModelFromJson(String str) =>
    List<GpsUserEventsModel>.from(
        json.decode(str).map((x) => GpsUserEventsModel.fromJson(x)));

String gpsUserEventsModelToJson(List<GpsUserEventsModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GpsUserEventsModel {
  GpsUserEventsModel({
    this.id,
    this.name,
  });

  int id;
  String name;

  factory GpsUserEventsModel.fromJson(Map<String, dynamic> json) =>
      GpsUserEventsModel(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}
