// To parse this JSON data, do
//
//     final gpsUserLocationModel = gpsUserLocationModelFromJson(jsonString);

import 'dart:convert';

import 'package:intl/intl.dart';

List<GpsUserLocationModel> gpsUserLocationModelFromJson(String str) =>
    List<GpsUserLocationModel>.from(
        json.decode(str).map((x) => GpsUserLocationModel.fromJson(x)));

String gpsUserLocationModelToJson(List<GpsUserLocationModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GpsUserLocationModel {
  GpsUserLocationModel({
    this.gpsLocationId,
    this.gpsTime,
    this.latitude,
    this.longitude,
    this.delayTime,
  });

  String gpsLocationId;
  String gpsTime;
  double latitude;
  double longitude;
  String delayTime;

  factory GpsUserLocationModel.fromJson(Map<String, dynamic> json) =>
      GpsUserLocationModel(
        gpsLocationId:
            json["GPSLocationID"] == null ? null : json["GPSLocationID"],
        gpsTime: json["gpsTime"] == null
            ? null
            : DateFormat('MMM dd, yyyy, kk:mm')
                .format(DateTime.parse(json["gpsTime"])),
        latitude:
            json["latitude"] == null ? null : double.parse(json["latitude"]),
        longitude:
            json["longitude"] == null ? null : double.parse(json["longitude"]),
        delayTime: json["delayTime"] == null ? null : json["delayTime"],
      );

  Map<String, dynamic> toJson() => {
        "GPSLocationID": gpsLocationId == null ? null : gpsLocationId,
        "gpsTime": gpsTime == null ? null : gpsTime,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "delayTime": delayTime == null ? null : delayTime,
      };
}
