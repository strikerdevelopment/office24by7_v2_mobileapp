// To parse this JSON data, do
//
//     final authModel = authModelFromJson(jsonString);

import 'dart:convert';

AuthModel authModelFromJson(String str) => AuthModel.fromJson(json.decode(str));

String authModelToJson(AuthModel data) => json.encode(data.toJson());

class AuthModel {
  AuthModel({
    this.isActive,
    this.isDelete,
    this.isLocked,
    this.isAvailable,
    this.mobileNumber,
    this.loginId,
    this.isProfileExist,
    this.version,
    this.releaseInformation,
    this.planExpired,
    this.alreadyLoggedIn,
    this.isOverviewEnabled,
    this.isFeedbackDivisionEnabled,
    this.isVoterDivisionEnabled,
    this.isChatEnabled,
  });

  String isActive;
  String isDelete;
  String isLocked;
  String isAvailable;
  String mobileNumber;
  String loginId;
  String isProfileExist;
  String version;
  String releaseInformation;
  String planExpired;
  String alreadyLoggedIn;
  String isOverviewEnabled;
  String isFeedbackDivisionEnabled;
  String isVoterDivisionEnabled;
  String isChatEnabled;

  factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        isActive: json["Is_Active"],
        isDelete: json["Is_Delete"],
        isLocked: json["Is_Locked"],
        isAvailable: json["Is_Available"],
        mobileNumber: json["Mobile_Number"],
        loginId: json["Login_ID"],
        isProfileExist: json["is_profile_exist"],
        version: json["Version"],
        releaseInformation: json["ReleaseInformation"],
        planExpired: json["PlanExpired"],
        alreadyLoggedIn: json["AlreadyLoggedIn"],
        isOverviewEnabled: json["IsOverviewEnabled"],
        isFeedbackDivisionEnabled: json["IsFeedbackDivisionEnabled"],
        isVoterDivisionEnabled: json["IsVoterDivisionEnabled"],
        isChatEnabled: json["IsChatEnabled"],
      );

  Map<String, dynamic> toJson() => {
        "Is_Active": isActive,
        "Is_Delete": isDelete,
        "Is_Locked": isLocked,
        "Is_Available": isAvailable,
        "Mobile_Number": mobileNumber,
        "Login_ID": loginId,
        "is_profile_exist": isProfileExist,
        "Version": version,
        "ReleaseInformation": releaseInformation,
        "PlanExpired": planExpired,
        "AlreadyLoggedIn": alreadyLoggedIn,
        "IsOverviewEnabled": isOverviewEnabled,
        "IsFeedbackDivisionEnabled": isFeedbackDivisionEnabled,
        "IsVoterDivisionEnabled": isVoterDivisionEnabled,
        "IsChatEnabled": isChatEnabled,
      };
}
