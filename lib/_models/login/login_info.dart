// To parse this JSON data, do
//
//     final loginInfo = loginInfoFromJson(jsonString);

import 'dart:convert';

LoginInfo loginInfoFromJson(String str) => LoginInfo.fromJson(json.decode(str));

String loginInfoToJson(LoginInfo data) => json.encode(data.toJson());

class LoginInfo {
  LoginInfo({
    this.loginId,
    this.userId,
    this.noAttempts,
    this.noAttemptsReset,
    this.otpLastInsertid,
    this.firstName,
    this.middleName,
    this.lastName,
    this.mobileNumber,
    this.emailId,
    this.orgId,
    this.orgName,
    this.companyId,
    this.companyName,
    this.userAuthToken,
    this.sessionName,
    this.userRole,
  });

  String loginId;
  String userId;
  int noAttempts;
  int noAttemptsReset;
  String otpLastInsertid;
  String firstName;
  String middleName;
  String lastName;
  String mobileNumber;
  String emailId;
  String orgId;
  String orgName;
  String companyId;
  String companyName;
  String userAuthToken;
  String sessionName;
  String userRole;

  factory LoginInfo.fromJson(Map<String, dynamic> json) => LoginInfo(
        loginId: json["Login_ID"],
        userId: json["User_ID"],
        noAttempts: json["No_Attempts"],
        noAttemptsReset: json["No_Attempts_Reset"],
        otpLastInsertid: json["Otp_last_insertid"],
        firstName: json["First_Name"],
        middleName: json["Middle_Name"],
        lastName: json["Last_Name"],
        mobileNumber: json["Mobile_Number"],
        emailId: json["Email_ID"],
        orgId: json["Org_ID"],
        orgName: json["Org_Name"],
        companyId: json["Company_ID"],
        companyName: json["Company_Name"],
        userAuthToken: json["User_Auth_Token"],
        sessionName: json["session_name"],
        userRole: json["User_Role"],
      );

  Map<String, dynamic> toJson() => {
        "Login_ID": loginId,
        "User_ID": userId,
        "No_Attempts": noAttempts,
        "No_Attempts_Reset": noAttemptsReset,
        "Otp_last_insertid": otpLastInsertid,
        "First_Name": firstName,
        "Middle_Name": middleName,
        "Last_Name": lastName,
        "Mobile_Number": mobileNumber,
        "Email_ID": emailId,
        "Org_ID": orgId,
        "Org_Name": orgName,
        "Company_ID": companyId,
        "Company_Name": companyName,
        "User_Auth_Token": userAuthToken,
        "session_name": sessionName,
        "User_Role": userRole,
      };
}
