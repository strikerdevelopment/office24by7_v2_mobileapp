// To parse this JSON data, do
//
//     final otpInfo = otpInfoFromJson(jsonString);

import 'dart:convert';

OtpInfo otpInfoFromJson(String str) => OtpInfo.fromJson(json.decode(str));

String otpInfoToJson(OtpInfo data) => json.encode(data.toJson());

class OtpInfo {
  OtpInfo({
    this.statusCode,
    this.statusMessage,
    this.otpcode,
    this.userid,
    this.loginid,
    this.otpInsertid,
    this.isAgents,
    this.isverson2,
    this.companyplan,
    this.userplan,
    this.userservices,
    this.mobile,
    this.sessionName,
    this.userAuthToken,
    this.expirytime,
    this.otpType,
    this.userRole,
  });

  int statusCode;
  String statusMessage;
  String otpcode;
  String userid;
  String loginid;
  int otpInsertid;
  int isAgents;
  int isverson2;
  int companyplan;
  int userplan;
  int userservices;
  String mobile;
  String sessionName;
  String userAuthToken;
  String expirytime;
  String otpType;
  String userRole;

  factory OtpInfo.fromJson(Map<String, dynamic> json) => OtpInfo(
        statusCode: json["statusCode"] == null ? null : json["statusCode"],
        statusMessage:
            json["statusMessage"] == null ? null : json["statusMessage"],
        otpcode: json["otpcode"] == null ? null : json["otpcode"],
        userid: json["userid"] == null ? null : json["userid"],
        loginid: json["loginid"] == null ? null : json["loginid"],
        otpInsertid: json["otp_insertid"] == null ? null : json["otp_insertid"],
        isAgents: json["is_agents"] == null ? null : json["is_agents"],
        isverson2: json["isverson2"] == null ? null : json["isverson2"],
        companyplan: json["companyplan"] == null ? null : json["companyplan"],
        userplan: json["userplan"] == null ? null : json["userplan"],
        userservices:
            json["userservices"] == null ? null : json["userservices"],
        mobile: json["mobile"] == null ? null : json["mobile"],
        sessionName: json["session_name"] == null ? null : json["session_name"],
        userAuthToken:
            json["user_auth_token"] == null ? null : json["user_auth_token"],
        expirytime: json["expirytime"] == null ? null : json["expirytime"],
        otpType: json["otp_type"] == null ? null : json["otp_type"],
        userRole: json["User_Role"] == null ? null : json["User_Role"],
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode == null ? null : statusCode,
        "statusMessage": statusMessage == null ? null : statusMessage,
        "otpcode": otpcode == null ? null : otpcode,
        "userid": userid == null ? null : userid,
        "loginid": loginid == null ? null : loginid,
        "otp_insertid": otpInsertid == null ? null : otpInsertid,
        "is_agents": isAgents == null ? null : isAgents,
        "isverson2": isverson2 == null ? null : isverson2,
        "companyplan": companyplan == null ? null : companyplan,
        "userplan": userplan == null ? null : userplan,
        "userservices": userservices == null ? null : userservices,
        "mobile": mobile == null ? null : mobile,
        "session_name": sessionName == null ? null : sessionName,
        "user_auth_token": userAuthToken == null ? null : userAuthToken,
        "expirytime": expirytime == null ? null : expirytime,
        "otp_type": otpType == null ? null : otpType,
        "User_Role": userRole == null ? null : userRole,
      };
}
