// To parse this JSON data, do
//
//     final userInfo = userInfoFromJson(jsonString);

import 'dart:convert';

UserInfo userInfoFromJson(String str) => UserInfo.fromJson(json.decode(str));

String userInfoToJson(UserInfo data) => json.encode(data.toJson());

class UserInfo {
  UserInfo({
    this.userInfo,
    this.agentSettings,
  });

  UserInfoClass userInfo;
  Map<String, dynamic> agentSettings;

  factory UserInfo.fromJson(Map<String, dynamic> json) => UserInfo(
        userInfo: json["User_info"] == null
            ? null
            : UserInfoClass.fromJson(json["User_info"]),
        agentSettings: json["Agent_Settings"] == null
            ? null
            : Map.from(json["Agent_Settings"]).map(
                (k, v) => MapEntry<String, String>(k, v == null ? null : v)),
      );

  Map<String, dynamic> toJson() => {
        "User_info": userInfo == null ? null : userInfo.toJson(),
        "Agent_Settings": agentSettings == null
            ? null
            : Map.from(agentSettings).map(
                (k, v) => MapEntry<String, dynamic>(k, v == null ? null : v)),
      };
}

class UserInfoClass {
  UserInfoClass({
    this.userId,
    this.loginId,
    this.firstName,
    this.lastName,
    this.dob,
    this.mobileNumber,
    this.emailId,
    this.userRole,
    this.profileImage,
    this.registeredDateTime,
    this.doj,
    this.orgId,
    this.orgName,
    this.companyId,
    this.companyName,
    this.desigId,
    this.desigName,
    this.contactAddress,
    this.cityId,
    this.cityName,
    this.countryId,
    this.countryName,
    this.pincode,
    this.branchId,
    this.branchName,
    this.userRoleName,
    this.reportingTo,
    this.reportingUsername,
  });

  String userId;
  String loginId;
  String firstName;
  String lastName;
  String dob;
  String mobileNumber;
  String emailId;
  String userRole;
  String profileImage;
  DateTime registeredDateTime;
  String doj;
  String orgId;
  String orgName;
  String companyId;
  String companyName;
  String desigId;
  dynamic desigName;
  String contactAddress;
  String cityId;
  String cityName;
  String countryId;
  String countryName;
  String pincode;
  String branchId;
  String branchName;
  String userRoleName;
  String reportingTo;
  String reportingUsername;

  factory UserInfoClass.fromJson(Map<String, dynamic> json) => UserInfoClass(
        userId: json["User_ID"] == null ? null : json["User_ID"],
        loginId: json["Login_ID"] == null ? null : json["Login_ID"],
        firstName: json["First_Name"] == null ? null : json["First_Name"],
        lastName: json["Last_Name"] == null ? null : json["Last_Name"],
        dob: json["DOB"] == null ? null : json["DOB"],
        mobileNumber:
            json["Mobile_Number"] == null ? null : json["Mobile_Number"],
        emailId: json["Email_ID"] == null ? null : json["Email_ID"],
        userRole: json["User_Role"] == null ? null : json["User_Role"],
        profileImage:
            json["Profile_Image"] == null ? null : json["Profile_Image"],
        registeredDateTime: json["Registered_DateTime"] == null
            ? null
            : DateTime.parse(json["Registered_DateTime"]),
        doj: json["DOJ"] == null ? null : json["DOJ"],
        orgId: json["Org_ID"] == null ? null : json["Org_ID"],
        orgName: json["Org_Name"] == null ? null : json["Org_Name"],
        companyId: json["Company_ID"] == null ? null : json["Company_ID"],
        companyName: json["Company_Name"] == null ? null : json["Company_Name"],
        desigId: json["Desig_ID"] == null ? null : json["Desig_ID"],
        desigName: json["Desig_Name"],
        contactAddress:
            json["Contact_Address"] == null ? null : json["Contact_Address"],
        cityId: json["City_Id"] == null ? null : json["City_Id"],
        cityName: json["City_Name"] == null ? null : json["City_Name"],
        countryId: json["Country_Id"] == null ? null : json["Country_Id"],
        countryName: json["Country_Name"] == null ? null : json["Country_Name"],
        pincode: json["Pincode"] == null ? null : json["Pincode"],
        branchId: json["Branch_ID"] == null ? null : json["Branch_ID"],
        branchName: json["Branch_Name"] == null ? null : json["Branch_Name"],
        userRoleName:
            json["User_Role_Name"] == null ? null : json["User_Role_Name"],
        reportingTo: json["reporting_to"] == null ? null : json["reporting_to"],
        reportingUsername: json["reporting_username"] == null
            ? null
            : json["reporting_username"],
      );

  Map<String, dynamic> toJson() => {
        "User_ID": userId == null ? null : userId,
        "Login_ID": loginId == null ? null : loginId,
        "First_Name": firstName == null ? null : firstName,
        "Last_Name": lastName == null ? null : lastName,
        "DOB": dob == null ? null : dob,
        "Mobile_Number": mobileNumber == null ? null : mobileNumber,
        "Email_ID": emailId == null ? null : emailId,
        "User_Role": userRole == null ? null : userRole,
        "Profile_Image": profileImage == null ? null : profileImage,
        "Registered_DateTime": registeredDateTime == null
            ? null
            : registeredDateTime.toIso8601String(),
        "DOJ": doj == null ? null : doj,
        "Org_ID": orgId == null ? null : orgId,
        "Org_Name": orgName == null ? null : orgName,
        "Company_ID": companyId == null ? null : companyId,
        "Company_Name": companyName == null ? null : companyName,
        "Desig_ID": desigId == null ? null : desigId,
        "Desig_Name": desigName,
        "Contact_Address": contactAddress == null ? null : contactAddress,
        "City_Id": cityId == null ? null : cityId,
        "City_Name": cityName == null ? null : cityName,
        "Country_Id": countryId == null ? null : countryId,
        "Country_Name": countryName == null ? null : countryName,
        "Pincode": pincode == null ? null : pincode,
        "Branch_ID": branchId == null ? null : branchId,
        "Branch_Name": branchName == null ? null : branchName,
        "User_Role_Name": userRoleName == null ? null : userRoleName,
        "reporting_to": reportingTo == null ? null : reportingTo,
        "reporting_username":
            reportingUsername == null ? null : reportingUsername,
      };
}
