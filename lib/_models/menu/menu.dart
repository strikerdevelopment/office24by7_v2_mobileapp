class Menu {
  final String pageid;
  final String img;
  final String acitveImg;
  final String title;
  final String router;

  const Menu({
    this.pageid,
    this.img,
    this.title,
    this.router,
    this.acitveImg,
  });
}

List<Menu> menuList = [
  // Menu(
  //   pageid: 'call_log',
  //   acitveImg: "/mobile_app_v2/o_call_log_white_menu.png",
  //   img: "/mobile_app_v2/o_call_log_blue_menu.png",
  //   title: "Call Log",
  //   router: "/call_log",
  // ),
  // Menu(
  //   pageid: 'dash_boards',
  //   acitveImg: "/mobile_app_v2/o_dashboard_white_menu.png",
  //   img: "/mobile_app_v2/o_dashboard_blue_menu.png",
  //   title: "Dash Boards",
  //   router: "/dash_boards",
  // ),
  // Menu(
  // pageid: 'calendar',
  //   acitveImg: "/mobile_app_v2/o_calender_white_menu.png",
  //   img: "/mobile_app_v2/o_calender_blue_menu.png",
  //   title: "Calendar",
  //   router: "/calendar",
  // ),
  Menu(
    pageid: 'contacts',
    acitveImg: "/mobile_app_v2/o_contact_white_menu.png",
    img: "/mobile_app_v2/o_contact_blue_menu.png",
    title: "Contacts",
    router: "/contacts",
  ),
  // Menu(
  //   pageid: 'dialer',
  //   acitveImg: "/mobile_app_v2/o_dialer_white_menu.png",
  //   img: "/mobile_app_v2/o_dialer_blue_menu.png",
  //   title: "Dialer (Click to Call)",
  //   router: "/dialer",
  // ),
  Menu(
    pageid: 'task_manager',
    acitveImg: "/mobile_app_v2/o_task_white_menu.png",
    img: "/mobile_app_v2/o_task_blue_menu.png",
    title: "Task Manager",
    router: "/task_manager",
  ),
  // Menu(
  //   pageid: 'opportunities',
  //   acitveImg: "/mobile_app_v2/o_opportunities_white_menu.png",
  //   img: "/mobile_app_v2/o_opportunities_blue_menu.png",
  //   title: "Opportunities",
  //   router: "/opportunities",
  // ),
  // Menu(
  // pageid: 'gps_tracker',
  //   acitveImg: "/mobile_app_v2/o_gps_white_menu.png",
  //   img: "/mobile_app_v2/o_gps_blue_menu.png",
  //   title: "GPS Tracker",
  //   router: "/gps_tracker",
  // ),
  // Menu(
  //   pageid: 'ticketing_manager',
  //   acitveImg: "/mobile_app_v2/o_ticket_white_menu.png",
  //   img: "/mobile_app_v2/o_ticket_blue_menu.png",
  //   title: "Ticketing Manager",
  //   router: "/ticketing_manager",
  // ),
  // Menu(
  // pageid: 'chat',
  //   acitveImg: "/mobile_app_v2/o_chat_white_menu.png",
  //   img: "/mobile_app_v2/o_chat_blue_menu.png",
  //   title: "Chat",
  //   router: "/chat",
  // ),
  // Menu(
  //   pageid: 'performance',
  //   acitveImg: "/mobile_app_v2/o_performance_white_menu.png",
  //   img: "/mobile_app_v2/o_performance_blue_menu.png",
  //   title: "Performance",
  //   router: "/performance",
  // ),
  Menu(
    pageid: 'logout',
    acitveImg: "/mobile_app_v2/o_logout_white_menu.png",
    img: "/mobile_app_v2/o_logout_blue_menu.png",
    title: "Logout",
    router: "/logout",
  ),
];
