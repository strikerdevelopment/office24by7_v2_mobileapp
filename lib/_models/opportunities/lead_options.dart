class CardLeadOptions {
  String id;
  String img;
  String title;
  CardLeadOptions({this.id, this.img, this.title});
}

List<CardLeadOptions> cardLeadOptions = [
  CardLeadOptions(
      id: "leadDetails",
      img: "/mobile_app_v2/o_option_lead_details.png",
      title: "Lead Details"),
  CardLeadOptions(
      id: "leadReassign",
      img: "/mobile_app_v2/o_option_reassign.png",
      title: "Lead Reassign"),
  CardLeadOptions(
      id: "leadTransfer",
      img: "/mobile_app_v2/o_option_transfer.png",
      title: "Lead Transfer"),
  CardLeadOptions(
      id: "note", img: "/mobile_app_v2/o_option_note.png", title: "Note"),
  CardLeadOptions(
      id: "task", img: "/mobile_app_v2/o_option_task.png", title: "Task"),
  CardLeadOptions(
      id: "meeting",
      img: "/mobile_app_v2/o_option_meeting.png",
      title: "Meeting"),
  CardLeadOptions(
      id: "reminder",
      img: "/mobile_app_v2/o_option_reminder.png",
      title: "Reminder"),
  CardLeadOptions(
      id: "editLead",
      img: "/mobile_app_v2/o_option_edit.png",
      title: "Edit Lead"),
];
