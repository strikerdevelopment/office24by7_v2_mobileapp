// To parse this JSON data, do
//
//     final oppoDateType = oppoDateTypeFromJson(jsonString);

import 'dart:convert';

List<OppoDateType> oppoDateTypeFromJson(String str) => List<OppoDateType>.from(
    json.decode(str).map((x) => OppoDateType.fromJson(x)));

String oppoDateTypeToJson(List<OppoDateType> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDateType {
  OppoDateType({
    this.type,
    this.name,
  });

  String type;
  String name;

  factory OppoDateType.fromJson(Map<String, dynamic> json) => OppoDateType(
        type: json["type"] == null ? null : json["type"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "name": name == null ? null : name,
      };
}
