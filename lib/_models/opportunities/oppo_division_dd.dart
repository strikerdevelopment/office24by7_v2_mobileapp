// To parse this JSON data, do
//
//     final oppoDivisionDd = oppoDivisionDdFromJson(jsonString);

import 'dart:convert';

List<OppoDivisionDd> oppoDivisionDdFromJson(String str) =>
    List<OppoDivisionDd>.from(
        json.decode(str).map((x) => OppoDivisionDd.fromJson(x)));

String oppoDivisionDdToJson(List<OppoDivisionDd> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDivisionDd {
  OppoDivisionDd({
    this.divisionId,
    this.divisionName,
    this.privilegeLevel,
    this.isHierarchy,
    this.isTeam,
    this.isDivision,
  });

  String divisionId;
  String divisionName;
  String privilegeLevel;
  dynamic isHierarchy;
  dynamic isTeam;
  dynamic isDivision;

  factory OppoDivisionDd.fromJson(Map<String, dynamic> json) => OppoDivisionDd(
        divisionId: json["division_id"],
        divisionName: json["division_name"],
        privilegeLevel: json["privilege_level"],
        isHierarchy: json["is_hierarchy"],
        isTeam: json["is_team"],
        isDivision: json["is_division"],
      );

  Map<String, dynamic> toJson() => {
        "division_id": divisionId,
        "division_name": divisionName,
        "privilege_level": privilegeLevel,
        "is_hierarchy": isHierarchy,
        "is_team": isTeam,
        "is_division": isDivision,
      };
}
