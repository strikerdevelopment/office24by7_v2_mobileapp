// To parse this JSON data, do
//
//     final oppoDivisionFormDd = oppoDivisionFormDdFromJson(jsonString);

import 'dart:convert';

List<OppoDivisionFormDd> oppoDivisionFormDdFromJson(String str) =>
    List<OppoDivisionFormDd>.from(
        json.decode(str).map((x) => OppoDivisionFormDd.fromJson(x)));

String oppoDivisionFormDdToJson(List<OppoDivisionFormDd> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDivisionFormDd {
  OppoDivisionFormDd({
    this.formId,
    this.formDesc,
  });

  String formId;
  String formDesc;

  factory OppoDivisionFormDd.fromJson(Map<String, dynamic> json) =>
      OppoDivisionFormDd(
        formId: json["form_id"],
        formDesc: json["form_desc"],
      );

  Map<String, dynamic> toJson() => {
        "form_id": formId,
        "form_desc": formDesc,
      };
}
