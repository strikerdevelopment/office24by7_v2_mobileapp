// To parse this JSON data, do
//
//     final oppoDivisionPriorityDd = oppoDivisionPriorityDdFromJson(jsonString);

import 'dart:convert';

List<OppoDivisionPriorityDd> oppoDivisionPriorityDdFromJson(String str) =>
    List<OppoDivisionPriorityDd>.from(
        json.decode(str).map((x) => OppoDivisionPriorityDd.fromJson(x)));

String oppoDivisionPriorityDdToJson(List<OppoDivisionPriorityDd> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDivisionPriorityDd {
  OppoDivisionPriorityDd({
    this.priorityId,
    this.priorityDesc,
    this.priorityOrder,
    this.backGroundColor,
    this.fontColor,
  });

  String priorityId;
  String priorityDesc;
  String priorityOrder;
  String backGroundColor;
  String fontColor;

  factory OppoDivisionPriorityDd.fromJson(Map<String, dynamic> json) =>
      OppoDivisionPriorityDd(
        priorityId: json["priority_id"] == null ? null : json["priority_id"],
        priorityDesc:
            json["priority_desc"] == null ? null : json["priority_desc"],
        priorityOrder:
            json["priority_order"] == null ? null : json["priority_order"],
        backGroundColor: json["back_ground_color"] == null
            ? null
            : json["back_ground_color"],
        fontColor: json["font_color"] == null ? null : json["font_color"],
      );

  Map<String, dynamic> toJson() => {
        "priority_id": priorityId == null ? null : priorityId,
        "priority_desc": priorityDesc == null ? null : priorityDesc,
        "priority_order": priorityOrder == null ? null : priorityOrder,
        "back_ground_color": backGroundColor == null ? null : backGroundColor,
        "font_color": fontColor == null ? null : fontColor,
      };
}
