// To parse this JSON data, do
//
//     final oppoDivisionSourceDd = oppoDivisionSourceDdFromJson(jsonString);

import 'dart:convert';

List<OppoDivisionSourceDd> oppoDivisionSourceDdFromJson(String str) =>
    List<OppoDivisionSourceDd>.from(
        json.decode(str).map((x) => OppoDivisionSourceDd.fromJson(x)));

String oppoDivisionSourceDdToJson(List<OppoDivisionSourceDd> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDivisionSourceDd {
  OppoDivisionSourceDd({
    this.sourceTypeId,
    this.sourceId,
    this.sourceDesc,
    this.sourceOrder,
    this.sourceTypeName,
  });

  String sourceTypeId;
  String sourceId;
  String sourceDesc;
  String sourceOrder;
  String sourceTypeName;

  factory OppoDivisionSourceDd.fromJson(Map<String, dynamic> json) =>
      OppoDivisionSourceDd(
        sourceTypeId:
            json["source_type_id"] == null ? null : json["source_type_id"],
        sourceId: json["source_id"] == null ? null : json["source_id"],
        sourceDesc: json["source_desc"] == null ? null : json["source_desc"],
        sourceOrder: json["source_order"] == null ? null : json["source_order"],
        sourceTypeName:
            json["source_type_name"] == null ? null : json["source_type_name"],
      );

  Map<String, dynamic> toJson() => {
        "source_type_id": sourceTypeId == null ? null : sourceTypeId,
        "source_id": sourceId == null ? null : sourceId,
        "source_desc": sourceDesc == null ? null : sourceDesc,
        "source_order": sourceOrder == null ? null : sourceOrder,
        "source_type_name": sourceTypeName == null ? null : sourceTypeName,
      };
}
