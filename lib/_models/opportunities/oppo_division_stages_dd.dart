// To parse this JSON data, do
//
//     final oppoDivisionStagesDd = oppoDivisionStagesDdFromJson(jsonString);

import 'dart:convert';

List<OppoDivisionStagesDd> oppoDivisionStagesDdFromJson(String str) =>
    List<OppoDivisionStagesDd>.from(
        json.decode(str).map((x) => OppoDivisionStagesDd.fromJson(x)));

String oppoDivisionStagesDdToJson(List<OppoDivisionStagesDd> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDivisionStagesDd {
  OppoDivisionStagesDd({
    this.stageId,
    this.stageDesc,
    this.stageOrder,
    this.statusId,
  });

  String stageId;
  String stageDesc;
  String stageOrder;
  String statusId;

  factory OppoDivisionStagesDd.fromJson(Map<String, dynamic> json) =>
      OppoDivisionStagesDd(
        stageId: json["stage_id"] == null ? null : json["stage_id"],
        stageDesc: json["stage_desc"] == null ? null : json["stage_desc"],
        stageOrder: json["stage_order"] == null ? null : json["stage_order"],
        statusId: json["status_id"] == null ? null : json["status_id"],
      );

  Map<String, dynamic> toJson() => {
        "stage_id": stageId == null ? null : stageId,
        "stage_desc": stageDesc == null ? null : stageDesc,
        "stage_order": stageOrder == null ? null : stageOrder,
        "status_id": statusId == null ? null : statusId,
      };
}
