// To parse this JSON data, do
//
//     final oppoDivisionStatusesDd = oppoDivisionStatusesDdFromJson(jsonString);

import 'dart:convert';

List<OppoDivisionStatusesDd> oppoDivisionStatusesDdFromJson(String str) =>
    List<OppoDivisionStatusesDd>.from(
        json.decode(str).map((x) => OppoDivisionStatusesDd.fromJson(x)));

String oppoDivisionStatusesDdToJson(List<OppoDivisionStatusesDd> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDivisionStatusesDd {
  OppoDivisionStatusesDd({
    this.statusId,
    this.statusDesc,
    this.statusOrder,
  });

  String statusId;
  String statusDesc;
  String statusOrder;

  factory OppoDivisionStatusesDd.fromJson(Map<String, dynamic> json) =>
      OppoDivisionStatusesDd(
        statusId: json["status_id"] == null ? null : json["status_id"],
        statusDesc: json["status_desc"] == null ? null : json["status_desc"],
        statusOrder: json["status_order"] == null ? null : json["status_order"],
      );

  Map<String, dynamic> toJson() => {
        "status_id": statusId == null ? null : statusId,
        "status_desc": statusDesc == null ? null : statusDesc,
        "status_order": statusOrder == null ? null : statusOrder,
      };
}
