// To parse this JSON data, do
//
//     final oppoDivisionUserDd = oppoDivisionUserDdFromJson(jsonString);

import 'dart:convert';

List<OppoDivisionUserDd> oppoDivisionUserDdFromJson(String str) =>
    List<OppoDivisionUserDd>.from(
        json.decode(str).map((x) => OppoDivisionUserDd.fromJson(x)));

String oppoDivisionUserDdToJson(List<OppoDivisionUserDd> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoDivisionUserDd {
  OppoDivisionUserDd({
    this.userId,
    this.userName,
    this.roleId,
  });

  String userId;
  String userName;
  String roleId;

  factory OppoDivisionUserDd.fromJson(Map<String, dynamic> json) =>
      OppoDivisionUserDd(
        userId: json["user_id"],
        userName: json["user_name"],
        roleId: json["role_id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "user_name": userName,
        "role_id": roleId,
      };
}
