// To parse this JSON data, do
//
//     final oppoEmailId = oppoEmailIdFromJson(jsonString);

import 'dart:convert';

List<OppoEmailId> oppoEmailIdFromJson(String str) => List<OppoEmailId>.from(
    json.decode(str).map((x) => OppoEmailId.fromJson(x)));

String oppoEmailIdToJson(List<OppoEmailId> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoEmailId {
  OppoEmailId({
    this.seId,
    this.fromMail,
    this.replyTo,
    this.cc,
    this.bcc,
    this.orgId,
    this.companyId,
    this.branchId,
    this.createdDateTime,
    this.createdBy,
    this.isActive,
    this.isDelete,
  });

  String seId;
  String fromMail;
  String replyTo;
  String cc;
  String bcc;
  String orgId;
  String companyId;
  String branchId;
  DateTime createdDateTime;
  String createdBy;
  String isActive;
  String isDelete;

  factory OppoEmailId.fromJson(Map<String, dynamic> json) => OppoEmailId(
        seId: json["SE_ID"] == null ? null : json["SE_ID"],
        fromMail: json["FromMail"] == null ? null : json["FromMail"],
        replyTo: json["Reply_To"] == null ? null : json["Reply_To"],
        cc: json["CC"] == null ? null : json["CC"],
        bcc: json["BCC"] == null ? null : json["BCC"],
        orgId: json["Org_ID"] == null ? null : json["Org_ID"],
        companyId: json["Company_ID"] == null ? null : json["Company_ID"],
        branchId: json["Branch_ID"] == null ? null : json["Branch_ID"],
        createdDateTime: json["Created_DateTime"] == null
            ? null
            : DateTime.parse(json["Created_DateTime"]),
        createdBy: json["Created_By"] == null ? null : json["Created_By"],
        isActive: json["Is_Active"] == null ? null : json["Is_Active"],
        isDelete: json["Is_Delete"] == null ? null : json["Is_Delete"],
      );

  Map<String, dynamic> toJson() => {
        "SE_ID": seId == null ? null : seId,
        "FromMail": fromMail == null ? null : fromMail,
        "Reply_To": replyTo == null ? null : replyTo,
        "CC": cc == null ? null : cc,
        "BCC": bcc == null ? null : bcc,
        "Org_ID": orgId == null ? null : orgId,
        "Company_ID": companyId == null ? null : companyId,
        "Branch_ID": branchId == null ? null : branchId,
        "Created_DateTime":
            createdDateTime == null ? null : createdDateTime.toIso8601String(),
        "Created_By": createdBy == null ? null : createdBy,
        "Is_Active": isActive == null ? null : isActive,
        "Is_Delete": isDelete == null ? null : isDelete,
      };
}
