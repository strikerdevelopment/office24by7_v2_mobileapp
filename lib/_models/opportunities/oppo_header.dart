// To parse this JSON data, do
//
//     final oppoHeader = oppoHeaderFromJson(jsonString);

import 'dart:convert';

List<OppoHeader> oppoHeaderFromJson(String str) =>
    List<OppoHeader>.from(json.decode(str).map((x) => OppoHeader.fromJson(x)));

String oppoHeaderToJson(List<OppoHeader> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoHeader {
  OppoHeader({
    this.headerId,
    this.headerDesc,
    this.headerOrder,
  });

  String headerId;
  String headerDesc;
  String headerOrder;

  factory OppoHeader.fromJson(Map<String, dynamic> json) => OppoHeader(
        headerId: json["header_id"] == null ? null : json["header_id"],
        headerDesc: json["header_desc"] == null ? null : json["header_desc"],
        headerOrder: json["header_order"] == null ? null : json["header_order"],
      );

  Map<String, dynamic> toJson() => {
        "header_id": headerId == null ? null : headerId,
        "header_desc": headerDesc == null ? null : headerDesc,
        "header_order": headerOrder == null ? null : headerOrder,
      };
}
