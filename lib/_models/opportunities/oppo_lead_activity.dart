// To parse this JSON data, do
//
//     final oppoLeadActivityModel = oppoLeadActivityModelFromJson(jsonString);

import 'dart:convert';

List<OppoLeadActivityModel> oppoLeadActivityModelFromJson(String str) =>
    List<OppoLeadActivityModel>.from(
        json.decode(str).map((x) => OppoLeadActivityModel.fromJson(x)));

String oppoLeadActivityModelToJson(List<OppoLeadActivityModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoLeadActivityModel {
  OppoLeadActivityModel({
    this.activitydate,
    this.data,
  });

  DateTime activitydate;
  List<Datum> data;

  factory OppoLeadActivityModel.fromJson(Map<dynamic, dynamic> json) =>
      OppoLeadActivityModel(
        activitydate: json["activitydate"] == null
            ? null
            : DateTime.parse(json["activitydate"]),
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<dynamic, dynamic> toJson() => {
        "activitydate": activitydate == null
            ? null
            : "${activitydate.year.toString().padLeft(4, '0')}-${activitydate.month.toString().padLeft(2, '0')}-${activitydate.day.toString().padLeft(2, '0')}",
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.leadActivityId,
    this.mydate,
    this.activityType,
    this.activityStatus,
    this.activityDesc,
    this.leadId,
    this.createdDatetime,
    this.createdBy,
    this.referenceTable,
    this.referenceKey,
    this.userName,
    this.leadAttachments,
    this.leadFirstName,
    this.leadLastName,
    this.leadNotes,
    this.voiceCall,
    //this.mobileVoiceCall,
  });

  String leadActivityId;
  DateTime mydate;
  String activityType;
  String activityStatus;
  String activityDesc;
  String leadId;
  DateTime createdDatetime;
  String createdBy;
  String referenceTable;
  String referenceKey;
  String userName;
  List<dynamic> leadAttachments;
  String leadFirstName;
  String leadLastName;
  List<LeadNote> leadNotes;
  List<dynamic> voiceCall;
  //List<List<dynamic>> mobileVoiceCall;

  factory Datum.fromJson(Map<dynamic, dynamic> json) => Datum(
        leadActivityId:
            json["lead_activity_id"] == null ? null : json["lead_activity_id"],
        mydate: json["mydate"] == null ? null : DateTime.parse(json["mydate"]),
        activityType:
            json["activity_type"] == null ? null : json["activity_type"],
        activityStatus:
            json["activity_status"] == null ? null : json["activity_status"],
        activityDesc:
            json["activity_desc"] == null ? null : json["activity_desc"],
        leadId: json["lead_id"] == null ? null : json["lead_id"],
        createdDatetime: json["created_datetime"] == null
            ? null
            : DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"] == null ? null : json["created_by"],
        referenceTable:
            json["reference_table"] == null ? null : json["reference_table"],
        referenceKey:
            json["reference_key"] == null ? null : json["reference_key"],
        userName: json["user_name"] == null ? null : json["user_name"],
        leadAttachments: json["lead_attachments"] == null
            ? null
            : List<dynamic>.from(json["lead_attachments"].map((x) => x)),
        leadFirstName:
            json["lead_first_name"] == null ? null : json["lead_first_name"],
        leadLastName:
            json["lead_last_name"] == null ? null : json["lead_last_name"],
        leadNotes: json["lead_notes"] == null
            ? null
            : List<LeadNote>.from(
                json["lead_notes"].map((x) => LeadNote.fromJson(x))),
        voiceCall: json["voice_call"] == null
            ? null
            : List<dynamic>.from(json["voice_call"].map((x) => x)),
        // mobileVoiceCall: json["mobile_voice_call"] == null
        //     ? null
        //     : List<List<dynamic>>.from(json["mobile_voice_call"]
        //         .map((x) => List<dynamic>.from(x.map((x) => x)))),
      );

  Map<dynamic, dynamic> toJson() => {
        "lead_activity_id": leadActivityId == null ? null : leadActivityId,
        "mydate": mydate == null
            ? null
            : "${mydate.year.toString().padLeft(4, '0')}-${mydate.month.toString().padLeft(2, '0')}-${mydate.day.toString().padLeft(2, '0')}",
        "activity_type": activityType == null ? null : activityType,
        "activity_status": activityStatus == null ? null : activityStatus,
        "activity_desc": activityDesc == null ? null : activityDesc,
        "lead_id": leadId == null ? null : leadId,
        "created_datetime":
            createdDatetime == null ? null : createdDatetime.toIso8601String(),
        "created_by": createdBy == null ? null : createdBy,
        "reference_table": referenceTable == null ? null : referenceTable,
        "reference_key": referenceKey == null ? null : referenceKey,
        "user_name": userName == null ? null : userName,
        "lead_attachments": leadAttachments == null
            ? null
            : List<dynamic>.from(leadAttachments.map((x) => x)),
        "lead_first_name": leadFirstName == null ? null : leadFirstName,
        "lead_last_name": leadLastName == null ? null : leadLastName,
        "lead_notes": leadNotes == null
            ? null
            : List<dynamic>.from(leadNotes.map((x) => x.toJson())),
        "voice_call": voiceCall == null
            ? null
            : List<dynamic>.from(voiceCall.map((x) => x)),
        // "mobile_voice_call": mobileVoiceCall == null
        //     ? null
        //     : List<dynamic>.from(mobileVoiceCall
        //         .map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}

class LeadNote {
  LeadNote({
    this.noteDesc,
    this.leadId,
    this.activityId,
    this.createdDatetime,
    this.createdBy,
  });

  String noteDesc;
  String leadId;
  String activityId;
  DateTime createdDatetime;
  String createdBy;

  factory LeadNote.fromJson(Map<String, dynamic> json) => LeadNote(
        noteDesc: json["note_desc"] == null ? null : json["note_desc"],
        leadId: json["lead_id"] == null ? null : json["lead_id"],
        activityId: json["activity_id"] == null ? null : json["activity_id"],
        createdDatetime: json["created_datetime"] == null
            ? null
            : DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"] == null ? null : json["created_by"],
      );

  Map<String, dynamic> toJson() => {
        "note_desc": noteDesc == null ? null : noteDesc,
        "lead_id": leadId == null ? null : leadId,
        "activity_id": activityId == null ? null : activityId,
        "created_datetime":
            createdDatetime == null ? null : createdDatetime.toIso8601String(),
        "created_by": createdBy == null ? null : createdBy,
      };
}
