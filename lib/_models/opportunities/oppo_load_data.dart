// To parse this JSON data, do
//
//     final oppoLoadData = oppoLoadDataFromJson(jsonString);

import 'dart:convert';

List<OppoLoadData> oppoLoadDataFromJson(String str) => List<OppoLoadData>.from(
    json.decode(str).map((x) => OppoLoadData.fromJson(x)));

String oppoLoadDataToJson(List<OppoLoadData> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoLoadData {
  OppoLoadData({
    this.leadId,
    this.contactId,
    this.categoryId,
    this.sourceTypeId,
    this.sourceId,
    this.divisionId,
    this.formId,
    this.statusId,
    this.stageId,
    this.priorityId,
    this.assignedTo,
    this.assignedDatetime,
    this.createdBy,
    this.createdDatetime,
    this.modifiedDatetime,
    this.fullName,
    this.firstName,
    this.lastName,
    this.isocode,
    this.phoneCode,
    this.phoneNumber,
    this.withoutnumbermasking,
    this.emailId,
    this.budget,
    this.location,
    this.website,
    this.leadScore,
    this.customerScore,
    this.leadMembers,
    this.modifiedBy,
    this.country,
    this.state,
    this.city,
    this.divisionName,
    this.sourceTypeDesc,
    this.sourceDesc,
    this.statusDesc,
    this.stageDesc,
    this.priorityDesc,
    this.priorityBgcolor,
    this.priorityFontcolor,
    this.formDesc,
    this.assignedToUser,
    this.createdByUser,
    this.modifiedByUser,
    this.leadAttachments,
    this.dateAndTime,
    this.jsonData,
  });

  String leadId;
  String contactId;
  String categoryId;
  String sourceTypeId;
  String sourceId;
  String divisionId;
  String formId;
  String statusId;
  String stageId;
  String priorityId;
  String assignedTo;
  DateTime assignedDatetime;
  String createdBy;
  DateTime createdDatetime;
  String modifiedDatetime;
  String fullName;
  String firstName;
  String lastName;
  String isocode;
  String phoneCode;
  String phoneNumber;
  String withoutnumbermasking;
  String emailId;
  String budget;
  String location;
  String website;
  String leadScore;
  String customerScore;
  String leadMembers;
  String modifiedBy;
  String country;
  String state;
  String city;
  String divisionName;
  String sourceTypeDesc;
  String sourceDesc;
  String statusDesc;
  String stageDesc;
  String priorityDesc;
  String priorityBgcolor;
  String priorityFontcolor;
  String formDesc;
  String assignedToUser;
  String createdByUser;
  String modifiedByUser;
  List<LeadAttachment> leadAttachments;
  String dateAndTime;
  Map<String, dynamic> jsonData;

  factory OppoLoadData.fromJson(Map<String, dynamic> json) => OppoLoadData(
        leadId: json["lead_id"],
        contactId: json["contact_id"],
        categoryId: json["category_id"],
        sourceTypeId: json["source_type_id"],
        sourceId: json["source_id"],
        divisionId: json["division_id"],
        formId: json["form_id"],
        statusId: json["status_id"],
        stageId: json["stage_id"],
        priorityId: json["priority_id"],
        assignedTo: json["assigned_to"],
        assignedDatetime: DateTime.parse(json["assigned_datetime"]),
        createdBy: json["created_by"],
        createdDatetime: DateTime.parse(json["created_datetime"]),
        modifiedDatetime: json["modified_datetime"],
        fullName: json["full_name"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        isocode: json["isocode"],
        phoneCode: json["phone_code"],
        phoneNumber: json["phone_number"],
        withoutnumbermasking: json["withoutnumbermasking"],
        emailId: json["email_id"],
        budget: json["budget"],
        location: json["location"],
        website: json["website"] == null ? null : json["website"],
        leadScore: json["lead_score"],
        customerScore: json["customer_score"],
        leadMembers: json["lead_members"],
        modifiedBy: json["modified_by"],
        country: json["country"],
        state: json["state"],
        city: json["city"],
        divisionName: json["division_name"],
        sourceTypeDesc: json["source_type_desc"],
        sourceDesc: json["source_desc"],
        statusDesc: json["status_desc"],
        stageDesc: json["stage_desc"],
        priorityDesc: json["priority_desc"],
        priorityBgcolor: json["priority_bgcolor"],
        priorityFontcolor: json["priority_fontcolor"],
        formDesc: json["form_desc"],
        assignedToUser: json["assigned_to_user"],
        createdByUser: json["created_by_user"],
        modifiedByUser: json["modified_by_user"],
        leadAttachments: json["lead_attachments"] == null
            ? null
            : List<LeadAttachment>.from(json["lead_attachments"]
                .map((x) => LeadAttachment.fromJson(x))),
        dateAndTime:
            json["date_and_time"] == null ? null : json["date_and_time"],
        jsonData: json,
      );

  Map<String, dynamic> toJson() => {
        "lead_id": leadId,
        "contact_id": contactId,
        "category_id": categoryId,
        "source_type_id": sourceTypeId,
        "source_id": sourceId,
        "division_id": divisionId,
        "form_id": formId,
        "status_id": statusId,
        "stage_id": stageId,
        "priority_id": priorityId,
        "assigned_to": assignedTo,
        "assigned_datetime": assignedDatetime.toIso8601String(),
        "created_by": createdBy,
        "created_datetime": createdDatetime.toIso8601String(),
        "modified_datetime": modifiedDatetime,
        "full_name": fullName,
        "first_name": firstName,
        "last_name": lastName,
        "isocode": isocode,
        "phone_code": phoneCode,
        "phone_number": phoneNumber,
        "withoutnumbermasking": withoutnumbermasking,
        "email_id": emailId,
        "budget": budget,
        "location": location,
        "website": website == null ? null : website,
        "lead_score": leadScore,
        "customer_score": customerScore,
        "lead_members": leadMembers,
        "modified_by": modifiedBy,
        "country": country,
        "state": state,
        "city": city,
        "division_name": divisionName,
        "source_type_desc": sourceTypeDesc,
        "source_desc": sourceDesc,
        "status_desc": statusDesc,
        "stage_desc": stageDesc,
        "priority_desc": priorityDesc,
        "priority_bgcolor": priorityBgcolor,
        "priority_fontcolor": priorityFontcolor,
        "form_desc": formDesc,
        "assigned_to_user": assignedToUser,
        "created_by_user": createdByUser,
        "modified_by_user": modifiedByUser,
        "lead_attachments": leadAttachments == null
            ? null
            : List<dynamic>.from(leadAttachments.map((x) => x.toJson())),
        "date_and_time": dateAndTime == null ? null : dateAndTime,
        "jsonData": json
      };
}

class LeadAttachment {
  LeadAttachment({
    this.attachmentId,
    this.attachmentPath,
    this.attachmentFilename,
    this.attachmentSize,
    this.createdDatetime,
    this.createdBy,
    this.leadId,
  });

  String attachmentId;
  String attachmentPath;
  String attachmentFilename;
  String attachmentSize;
  DateTime createdDatetime;
  String createdBy;
  String leadId;

  factory LeadAttachment.fromJson(Map<String, dynamic> json) => LeadAttachment(
        attachmentId: json["attachment_id"],
        attachmentPath: json["attachment_path"],
        attachmentFilename: json["attachment_filename"],
        attachmentSize: json["attachment_size"],
        createdDatetime: DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"],
        leadId: json["lead_id"],
      );

  Map<String, dynamic> toJson() => {
        "attachment_id": attachmentId,
        "attachment_path": attachmentPath,
        "attachment_filename": attachmentFilename,
        "attachment_size": attachmentSize,
        "created_datetime": createdDatetime.toIso8601String(),
        "created_by": createdBy,
        "lead_id": leadId,
      };
}
