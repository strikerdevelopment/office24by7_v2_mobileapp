// To parse this JSON data, do
//
//     final oppoSenderId = oppoSenderIdFromJson(jsonString);

import 'dart:convert';

List<OppoSenderId> oppoSenderIdFromJson(String str) => List<OppoSenderId>.from(
    json.decode(str).map((x) => OppoSenderId.fromJson(x)));

String oppoSenderIdToJson(List<OppoSenderId> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OppoSenderId {
  OppoSenderId({
    this.sId,
    this.senderId,
    this.userId,
    this.orgId,
    this.companyId,
    this.createdDateTime,
    this.createdBy,
    this.isActive,
    this.isDelete,
    this.isBlock,
  });

  String sId;
  String senderId;
  String userId;
  String orgId;
  String companyId;
  DateTime createdDateTime;
  String createdBy;
  String isActive;
  String isDelete;
  String isBlock;

  factory OppoSenderId.fromJson(Map<String, dynamic> json) => OppoSenderId(
        sId: json["S_ID"] == null ? null : json["S_ID"],
        senderId: json["Sender_ID"] == null ? null : json["Sender_ID"],
        userId: json["User_ID"] == null ? null : json["User_ID"],
        orgId: json["Org_ID"] == null ? null : json["Org_ID"],
        companyId: json["Company_ID"] == null ? null : json["Company_ID"],
        createdDateTime: json["Created_DateTime"] == null
            ? null
            : DateTime.parse(json["Created_DateTime"]),
        createdBy: json["Created_By"] == null ? null : json["Created_By"],
        isActive: json["Is_Active"] == null ? null : json["Is_Active"],
        isDelete: json["Is_Delete"] == null ? null : json["Is_Delete"],
        isBlock: json["Is_Block"] == null ? null : json["Is_Block"],
      );

  Map<String, dynamic> toJson() => {
        "S_ID": sId == null ? null : sId,
        "Sender_ID": senderId == null ? null : senderId,
        "User_ID": userId == null ? null : userId,
        "Org_ID": orgId == null ? null : orgId,
        "Company_ID": companyId == null ? null : companyId,
        "Created_DateTime":
            createdDateTime == null ? null : createdDateTime.toIso8601String(),
        "Created_By": createdBy == null ? null : createdBy,
        "Is_Active": isActive == null ? null : isActive,
        "Is_Delete": isDelete == null ? null : isDelete,
        "Is_Block": isBlock == null ? null : isBlock,
      };
}
