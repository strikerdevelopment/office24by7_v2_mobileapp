import 'dart:convert';

import 'package:office24by7_v2/presention/styles/color_codes.dart';

Map formateMandalChart(data) {
  Map chartData = {"xaxis": [], "series": []};

  List<dynamic> frmData = [];
  List<dynamic> frmxAxiz = [];
  if (data == null) {
    frmData = [];
    frmxAxiz = [];
  } else {
    data.forEach(
      (e) => {
        frmxAxiz.add(e['XName']),
        frmData.add({"name": e["YName"], "data": [], "type": 'bar'})
      },
    );
  }

  List<dynamic> uniqevalues = uniqueValues(frmData);
  if (data == null) {
    frmData = [];
    frmxAxiz = [];
  } else {
    data.forEach((e) => {
          uniqevalues.forEach((dynamic uniqueValue) => {
                if (uniqueValue['name'] == e['YName'])
                  {uniqueValue['data'].add(int.parse(e['Points']))}
              })
        });
  }
  chartData["series"] = uniqevalues;
  chartData["xaxis"] = frmxAxiz.toSet().toList();

  return chartData;
}

Map formatePartyChart(data) {
  Map chartData = {"xaxis": [], "series": []};

  List<dynamic> frmData = [];
  List<dynamic> frmxAxiz = [];

  int i = 0;
  if (data == null) {
    frmData = [];
    frmxAxiz = [];
  } else {
    data.forEach(
      (e) => {
        frmxAxiz.add(e['XName']),
        frmData.add({
          "value": int.parse(e['Points']),
          "itemStyle": {"color": color_codes[i++]},
          "label": {"show": true},
        }),
      },
    );
  }

  chartData["series"] = frmData;
  chartData["xaxis"] = frmxAxiz;

  return chartData;
}

List<dynamic> uniqueValues(myList) {
// convert each item to a string by using JSON encoding
  final jsonList = myList.map((item) => jsonEncode(item)).toList();

  // using toSet - toList strategy
  final uniqueJsonList = jsonList.toSet().toList();

  // convert each item back to the original form using JSON decoding
  final result = uniqueJsonList.map((item) => jsonDecode(item)).toList();

  return result;
}
