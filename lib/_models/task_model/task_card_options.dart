class TaskCardOptions {
  String id;
  String img;
  String title;
  TaskCardOptions({this.id, this.img, this.title});
}

List<TaskCardOptions> taskCardOptions = [
  TaskCardOptions(
    id: "editTask",
    title: "Edit Task",
    img: "/mobile_app_v2/o_option_edit.png",
  ),
  TaskCardOptions(
    id: "addTask",
    title: "Add Task",
    img: "/mobile_app_v2/o_option_edit.png",
  ),
  TaskCardOptions(
    id: "taskReassign",
    title: "Task Reassign",
    img: "/mobile_app_v2/o_option_reassign.png",
  ),
  TaskCardOptions(
    id: "taskTransfer",
    title: "Task Transfer",
    img: "/mobile_app_v2/o_option_transfer.png",
  ),
  TaskCardOptions(
    id: "tasknote",
    title: "Note",
    img: "/mobile_app_v2/o_option_note.png",
  ),
  TaskCardOptions(
    id: "taskhistory",
    title: "Task History",
    img: "/mobile_app_v2/o_option_task.png",
  ),
  TaskCardOptions(
    id: "taskReminder",
    title: "Reminder",
    img: "/mobile_app_v2/o_option_contact_details.png",
  ),
  TaskCardOptions(
    id: "taskAttachment",
    title: "Attachment",
    img: "/mobile_app_v2/o_option_contact_details.png",
  ),
  TaskCardOptions(
    id: "Subtask",
    title: "Subtask",
    img: "/mobile_app_v2/o_option_task.png",
  ),
];
