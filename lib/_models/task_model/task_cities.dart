// To parse this JSON data, do
//
//     final taskCities = taskCitiesFromJson(jsonString);

import 'dart:convert';

List<TaskCities> taskCitiesFromJson(String str) =>
    List<TaskCities>.from(json.decode(str).map((x) => TaskCities.fromJson(x)));

String taskCitiesToJson(List<TaskCities> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskCities {
  TaskCities({
    this.id,
    this.name,
    this.countryId,
    this.stateId,
  });

  String id;
  String name;
  String countryId;
  String stateId;

  factory TaskCities.fromJson(Map<String, dynamic> json) => TaskCities(
        id: json["id"],
        name: json["name"],
        countryId: json["country_id"],
        stateId: json["state_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "country_id": countryId,
        "state_id": stateId,
      };
}
