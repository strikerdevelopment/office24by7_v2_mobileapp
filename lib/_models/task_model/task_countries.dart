// To parse this JSON data, do
//
//     final taskCountries = taskCountriesFromJson(jsonString);

import 'dart:convert';

List<TaskCountries> taskCountriesFromJson(String str) =>
    List<TaskCountries>.from(
        json.decode(str).map((x) => TaskCountries.fromJson(x)));

String taskCountriesToJson(List<TaskCountries> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskCountries {
  TaskCountries({
    this.id,
    this.name,
    this.phonecode,
    this.phoneNumberLength,
    this.currency,
  });

  String id;
  String name;
  String phonecode;
  String phoneNumberLength;
  String currency;

  factory TaskCountries.fromJson(Map<String, dynamic> json) => TaskCountries(
        id: json["id"],
        name: json["name"],
        phonecode: json["phonecode"],
        phoneNumberLength: json["phone_number_length"],
        currency: json["currency"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phonecode": phonecode,
        "phone_number_length": phoneNumberLength,
        "currency": currency,
      };
}
