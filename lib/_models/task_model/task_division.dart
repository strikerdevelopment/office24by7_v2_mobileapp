// To parse this JSON data, do
//
//     final taskDivision = taskDivisionFromJson(jsonString);

import 'dart:convert';

List<TaskDivision> taskDivisionFromJson(String str) => List<TaskDivision>.from(
    (json.decode(str)).map((x) => TaskDivision.fromJson(x)));

String taskDivisionToJson(List<TaskDivision> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskDivision {
  TaskDivision({
    this.divisionId,
    this.divisionName,
    this.privilegeLevel,
    this.isHierarchy,
    this.isTeam,
    this.isDivision,
    this.divisionTotalUsers,
    this.divisionUsers,
  });

  String divisionId;
  String divisionName;
  String privilegeLevel;
  dynamic isHierarchy;
  dynamic isTeam;
  dynamic isDivision;
  List<DivisionUser> divisionTotalUsers;
  List<DivisionUser> divisionUsers;

  factory TaskDivision.fromJson(Map<String, dynamic> json) => TaskDivision(
        divisionId: json["division_id"],
        divisionName: json["division_name"],
        privilegeLevel: json["privilege_level"],
        isHierarchy: json["is_hierarchy"],
        isTeam: json["is_team"],
        isDivision: json["is_division"],
        divisionTotalUsers: List<DivisionUser>.from(
            json["division_total_users"].map((x) => DivisionUser.fromJson(x))),
        divisionUsers: List<DivisionUser>.from(
            json["division_users"].map((x) => DivisionUser.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "division_id": divisionId,
        "division_name": divisionName,
        "privilege_level": privilegeLevel,
        "is_hierarchy": isHierarchy,
        "is_team": isTeam,
        "is_division": isDivision,
        "division_total_users":
            List<dynamic>.from(divisionTotalUsers.map((x) => x.toJson())),
        "division_users":
            List<dynamic>.from(divisionUsers.map((x) => x.toJson())),
      };
}

class DivisionUser {
  DivisionUser({
    this.userId,
    this.userName,
    this.roleId,
  });

  String userId;
  String userName;
  String roleId;

  factory DivisionUser.fromJson(Map<String, dynamic> json) => DivisionUser(
        userId: json["user_id"],
        userName: json["user_name"],
        roleId: json["role_id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "user_name": userName,
        "role_id": roleId,
      };
}
