// To parse this JSON data, do
//
//     final taskDivisionForm = taskDivisionFormFromJson(jsonString);

import 'dart:convert';

List<TaskDivisionForm> taskDivisionFormFromJson(String str) =>
    List<TaskDivisionForm>.from(
        json.decode(str).map((x) => TaskDivisionForm.fromJson(x)));

String taskDivisionFormToJson(List<TaskDivisionForm> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskDivisionForm {
  TaskDivisionForm({
    this.formId,
    this.formDesc,
  });

  String formId;
  String formDesc;

  factory TaskDivisionForm.fromJson(Map<String, dynamic> json) =>
      TaskDivisionForm(
        formId: json["form_id"],
        formDesc: json["form_desc"],
      );

  Map<String, dynamic> toJson() => {
        "form_id": formId,
        "form_desc": formDesc,
      };
}
