// To parse this JSON data, do
//
//     final taskDivisionStatus = taskDivisionStatusFromJson(jsonString);

import 'dart:convert';

List<TaskDivisionStatus> taskDivisionStatusFromJson(String str) =>
    List<TaskDivisionStatus>.from(
        json.decode(str).map((x) => TaskDivisionStatus.fromJson(x)));

String taskDivisionStatusToJson(List<TaskDivisionStatus> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskDivisionStatus {
  TaskDivisionStatus({
    this.statusId,
    this.statusDesc,
    this.statusOrder,
    this.fontColor,
    this.backgroundColor,
    this.leadCnt,
  });

  String statusId;
  String statusDesc;
  String statusOrder;
  String fontColor;
  String backgroundColor;
  String leadCnt;

  factory TaskDivisionStatus.fromJson(Map<String, dynamic> json) =>
      TaskDivisionStatus(
        statusId: json["status_id"],
        statusDesc: json["status_desc"],
        statusOrder: json["status_order"],
        fontColor: json["font_color"],
        backgroundColor: json["background_color"],
        leadCnt: json["lead_cnt"],
      );

  Map<String, dynamic> toJson() => {
        "status_id": statusId,
        "status_desc": statusDesc,
        "status_order": statusOrder,
        "font_color": fontColor,
        "background_color": backgroundColor,
        "lead_cnt": leadCnt,
      };
}
