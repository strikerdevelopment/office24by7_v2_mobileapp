// To parse this JSON data, do
//
//     final taskFormFields = taskFormFieldsFromJson(jsonString);

import 'dart:convert';

List<TaskFormFields> taskFormFieldsFromJson(String str) =>
    List<TaskFormFields>.from(
        json.decode(str).map((x) => TaskFormFields.fromJson(x)));

String taskFormFieldsToJson(List<TaskFormFields> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskFormFields {
  TaskFormFields({
    this.formId,
    this.fieldId,
    this.fieldName,
    this.fieldLabel,
    this.fieldDesc,
    this.fieldType,
    this.fieldOrder,
    this.fieldDefaultValue,
    this.isRequired,
    this.validationType,
    this.isDisplayGridview,
    this.isSystemField,
    this.isStandardField,
    this.isDependent,
    this.chieldFieldId,
  });

  String formId;
  String fieldId;
  String fieldName;
  String fieldLabel;
  String fieldDesc;
  String fieldType;
  String fieldOrder;
  String fieldDefaultValue;
  String isRequired;
  String validationType;
  String isDisplayGridview;
  String isSystemField;
  String isStandardField;
  String isDependent;
  String chieldFieldId;

  factory TaskFormFields.fromJson(Map<String, dynamic> json) => TaskFormFields(
        formId: json["form_id"],
        fieldId: json["field_id"],
        fieldName: json["field_name"],
        fieldLabel: json["field_label"],
        fieldDesc: json["field_desc"],
        fieldType: json["field_type"],
        fieldOrder: json["field_order"],
        fieldDefaultValue: json["field_default_value"],
        isRequired: json["is_required"],
        validationType: json["validation_type"],
        isDisplayGridview: json["is_display_gridview"],
        isSystemField: json["is_system_field"],
        isStandardField: json["is_standard_field"],
        isDependent: json["is_dependent"],
        chieldFieldId: json["chield_field_id"],
      );

  Map<String, dynamic> toJson() => {
        "form_id": formId,
        "field_id": fieldId,
        "field_name": fieldName,
        "field_label": fieldLabel,
        "field_desc": fieldDesc,
        "field_type": fieldType,
        "field_order": fieldOrder,
        "field_default_value": fieldDefaultValue,
        "is_required": isRequired,
        "validation_type": validationType,
        "is_display_gridview": isDisplayGridview,
        "is_system_field": isSystemField,
        "is_standard_field": isStandardField,
        "is_dependent": isDependent,
        "chield_field_id": chieldFieldId,
      };
}
