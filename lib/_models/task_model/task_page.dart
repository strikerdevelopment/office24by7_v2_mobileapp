import 'package:flutter/material.dart';

class TaskModel {
  String id;
  String title;
  String subtitle;
  Icon icon;
  Icon button;

  TaskModel({this.id, this.title, this.subtitle, this.icon, this.button});
}

List<TaskModel> taskListData = [
  TaskModel(
    id: "1",
    title: "Share the workload",
    subtitle: '04/06/2020 | Jason david',
    icon: Icon(Icons.chevron_right_outlined),
    button: Icon(Icons.assignment_outlined),
  ),
  TaskModel(
    id: "2",
    title: "see tangible progress",
    subtitle: '02/06/2020 | jason david',
    icon: Icon(Icons.chevron_right_outlined),
    button: Icon(Icons.assignment_outlined),
  ),
  TaskModel(
    id: "3",
    title: "Connect todoist to your world",
    subtitle: '25/05/2020 | willson cook',
    icon: Icon(Icons.chevron_right_outlined),
    button: Icon(Icons.assignment_outlined),
  ),
  TaskModel(
    id: "4",
    title: "Templates to get you started",
    subtitle: '16/0/2020 | jason david',
    icon: Icon(Icons.chevron_right_outlined),
    button: Icon(Icons.assignment_outlined),
  ),
  TaskModel(
    id: "5",
    title: "Keep it all together with todoist",
    subtitle: '04/06/2020 | thomas cook',
    icon: Icon(Icons.chevron_right_outlined),
    button: Icon(Icons.assignment_outlined),
  ),
  TaskModel(
    id: "6",
    title: "Start each day feeling calm and in control",
    subtitle: '08/05/2020 | jason david',
    icon: Icon(Icons.chevron_right_outlined),
    button: Icon(Icons.assignment_outlined),
  ),
  TaskModel(
    id: "7",
    title: "Focus your energy on the right things",
    subtitle: '04/06/2020 | jason david',
    icon: Icon(Icons.chevron_right_outlined),
    button: Icon(Icons.assignment_outlined),
  ),
];
