// To parse this JSON data, do
//
//     final taskPriorities = taskPrioritiesFromJson(jsonString);

import 'dart:convert';

List<TaskPriorities> taskPrioritiesFromJson(String str) =>
    List<TaskPriorities>.from(
        json.decode(str).map((x) => TaskPriorities.fromJson(x)));

String taskPrioritiesToJson(List<TaskPriorities> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskPriorities {
  TaskPriorities({
    this.priorityId,
    this.priorityDesc,
    this.priorityOrder,
    this.priorityBgcolour,
    this.priorityFontcolour,
  });

  String priorityId;
  String priorityDesc;
  String priorityOrder;
  String priorityBgcolour;
  String priorityFontcolour;

  factory TaskPriorities.fromJson(Map<String, dynamic> json) => TaskPriorities(
        priorityId: json["priority_id"],
        priorityDesc: json["priority_desc"],
        priorityOrder: json["priority_order"],
        priorityBgcolour: json["priority_bgcolour"],
        priorityFontcolour: json["priority_fontcolour"],
      );

  Map<String, dynamic> toJson() => {
        "priority_id": priorityId,
        "priority_desc": priorityDesc,
        "priority_order": priorityOrder,
        "priority_bgcolour": priorityBgcolour,
        "priority_fontcolour": priorityFontcolour,
      };
}
