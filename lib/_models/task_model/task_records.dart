// To parse this JSON data, do
//
//     final taskDataModel = taskDataModelFromJson(jsonString);

import 'dart:convert';

TaskDataModel taskDataModelFromJson(str) =>
    TaskDataModel.fromJson(json.decode(str));

String taskDataModelToJson(TaskDataModel data) => json.encode(data.toJson());

class TaskDataModel {
  TaskDataModel({
    this.status,
    this.taskData,
    this.allRecords,
    this.dropdownlist,
    this.totalallRecords,
  });

  List<TaskStatus> status;
  List<TaskDatum> taskData;
  dynamic allRecords;
  List<TaskDropdownlist> dropdownlist;
  dynamic totalallRecords;

  factory TaskDataModel.fromJson(Map<String, dynamic> json) => TaskDataModel(
        status: json["Status"] == null
            ? null
            : List<TaskStatus>.from(
                json["Status"].map((x) => TaskStatus.fromJson(x))),
        taskData: List<TaskDatum>.from(
            json["Task_Data"].map((x) => TaskDatum.fromJson(x))),
        allRecords: json["all_records"],
        dropdownlist: List<TaskDropdownlist>.from(json["dropdownlist"]
            .map((x) => x == 0 ? null : TaskDropdownlist.fromJson(x))),
        totalallRecords: json["totalall_records"],
      );

  Map<String, dynamic> toJson() => {
        "TaskStatus": List<dynamic>.from(status.map((x) => x.toJson())),
        "Task_Data": List<dynamic>.from(taskData.map((x) => x.toJson())),
        "all_records": allRecords,
        "dropdownlist": List<dynamic>.from(dropdownlist.map((x) => x)),
        "totalall_records": totalallRecords,
      };
}

class TaskStatus {
  TaskStatus({
    this.statusId,
    this.statusDesc,
    this.statusOrder,
    this.fontColor,
    this.backgroundColor,
    this.leadCnt,
    this.header,
    this.data,
    this.finalstatusDesc,
    this.anchorlinkStatus,
  });

  String statusId;
  String statusDesc;
  String statusOrder;
  String fontColor;
  String backgroundColor;
  String leadCnt;
  Header header;
  dynamic data;
  String finalstatusDesc;
  bool anchorlinkStatus;

  factory TaskStatus.fromJson(Map<String, dynamic> json) => TaskStatus(
        statusId: json["status_id"],
        statusDesc: json["status_desc"],
        statusOrder: json["status_order"],
        fontColor: json["font_color"],
        backgroundColor: json["background_color"],
        leadCnt: json["lead_cnt"],
        header: json["header"] == null ? null : Header.fromJson(json["header"]),
        data: json["data"],
        finalstatusDesc:
            json["finalstatus_desc"] == null ? null : json["finalstatus_desc"],
        anchorlinkStatus: json["anchorlink_status"] == null
            ? null
            : json["anchorlink_status"],
      );

  Map<String, dynamic> toJson() => {
        "status_id": statusId,
        "status_desc": statusDesc,
        "status_order": statusOrder,
        "font_color": fontColor,
        "background_color": backgroundColor,
        "lead_cnt": leadCnt,
        "header": header == null ? null : header.toJson(),
        "data": data,
        "finalstatus_desc": finalstatusDesc == null ? null : finalstatusDesc,
        "anchorlink_status": anchorlinkStatus == null ? null : anchorlinkStatus,
      };
}

class TaskDatum {
  TaskDatum({
    this.taskId,
    this.taskType,
    this.taskTitle,
    this.taskDesc,
    this.scheduleDate,
    this.startDatetime,
    this.endDatetime,
    this.tatDuration,
    this.assignedTo,
    this.country,
    this.state,
    this.city,
    this.isApproval,
    this.isApproved,
    this.approvedDatetime,
    this.isRejected,
    this.rejectedDatetime,
    this.isFollowup,
    this.reminderId,
    this.isMeeting,
    this.eventId,
    this.categoryId,
    this.sourceTypeId,
    this.sourceId,
    this.divisionId,
    this.formId,
    this.statusId,
    this.stageId,
    this.priorityId,
    this.uniqueId,
    this.contactId,
    this.callId,
    this.ticketId,
    this.leadId,
    this.isCompleted,
    this.createdDatetime,
    this.createdBy,
    this.modifiedDatetime,
    this.modifiedBy,
    this.parentTaskId,
    this.isIndependent,
    this.divisionName,
    this.sourceTypeDesc,
    this.sourceDesc,
    this.statusDesc,
    this.stageDesc,
    this.priorityDesc,
    this.priorityBgcolor,
    this.priorityFontcolor,
    this.formDesc,
    this.assignedToUser,
    this.createdByUser,
    this.modifiedByUser,
    this.subTasks,
    this.subTaskIds,
    this.statusBgcolor,
    this.statusFontColor,
    this.branch,
  });

  String taskId;
  String taskType;
  String taskTitle;
  String taskDesc;
  DateTime scheduleDate;
  dynamic startDatetime;
  dynamic endDatetime;
  String tatDuration;
  String assignedTo;
  String country;
  String state;
  String city;
  String isApproval;
  String isApproved;
  String approvedDatetime;
  String isRejected;
  dynamic rejectedDatetime;
  String isFollowup;
  String reminderId;
  String isMeeting;
  String eventId;
  String categoryId;
  String sourceTypeId;
  String sourceId;
  String divisionId;
  String formId;
  String statusId;
  String stageId;
  String priorityId;
  String uniqueId;
  String contactId;
  String callId;
  String ticketId;
  String leadId;
  String isCompleted;
  DateTime createdDatetime;
  String createdBy;
  DateTime modifiedDatetime;
  String modifiedBy;
  String parentTaskId;
  String isIndependent;
  String divisionName;
  dynamic sourceTypeDesc;
  String sourceDesc;
  dynamic statusDesc;
  String stageDesc;
  String priorityDesc;
  String priorityBgcolor;
  String priorityFontcolor;
  String formDesc;
  String assignedToUser;
  String createdByUser;
  String modifiedByUser;
  dynamic subTasks;
  String subTaskIds;
  dynamic statusBgcolor;
  dynamic statusFontColor;
  String branch;

  factory TaskDatum.fromJson(Map<String, dynamic> json) => TaskDatum(
        taskId: json["task_id"],
        taskType: json["task_type"],
        taskTitle: json["task_title"],
        taskDesc: json["task_desc"],
        scheduleDate: DateTime.parse(json["schedule_date"]),
        startDatetime: json["start_datetime"],
        endDatetime: json["end_datetime"],
        tatDuration: json["tat_duration"],
        assignedTo: json["assigned_to"],
        country: json["country"],
        state: json["state"],
        city: json["city"],
        isApproval: json["is_approval"],
        isApproved: json["is_approved"],
        approvedDatetime: json["approved_datetime"],
        isRejected: json["is_rejected"],
        rejectedDatetime: json["rejected_datetime"],
        isFollowup: json["is_followup"],
        reminderId: json["reminder_id"],
        isMeeting: json["is_meeting"],
        eventId: json["event_id"],
        categoryId: json["category_id"],
        sourceTypeId: json["source_type_id"],
        sourceId: json["source_id"],
        divisionId: json["division_id"],
        formId: json["form_id"],
        statusId: json["status_id"],
        stageId: json["stage_id"],
        priorityId: json["priority_id"],
        uniqueId: json["unique_id"],
        contactId: json["contact_id"],
        callId: json["call_id"],
        ticketId: json["ticket_id"],
        leadId: json["lead_id"],
        isCompleted: json["is_completed"],
        createdDatetime: DateTime.parse(json["created_datetime"]),
        createdBy: json["created_by"],
        modifiedDatetime: DateTime.parse(json["modified_datetime"]),
        modifiedBy: json["modified_by"],
        parentTaskId: json["parent_task_id"],
        isIndependent: json["is_independent"],
        divisionName: json["division_name"],
        sourceTypeDesc: json["source_type_desc"],
        sourceDesc: json["source_desc"],
        statusDesc: json["status_desc"],
        stageDesc: json["stage_desc"],
        priorityDesc: json["priority_desc"],
        priorityBgcolor: json["priority_bgcolor"],
        priorityFontcolor: json["priority_fontcolor"],
        formDesc: json["form_desc"],
        assignedToUser: json["assigned_to_user"],
        createdByUser: json["created_by_user"],
        modifiedByUser: json["modified_by_user"],
        subTasks: json["sub_tasks"],
        subTaskIds: json["sub_task_ids"],
        statusBgcolor: json["status_bgcolor"],
        statusFontColor: json["status_font_color"],
        branch: json["branch"],
      );

  Map<String, dynamic> toJson() => {
        "task_id": taskId,
        "task_type": taskType,
        "task_title": taskTitle,
        "task_desc": taskDesc,
        "schedule_date": scheduleDate.toIso8601String(),
        "start_datetime": startDatetime,
        "end_datetime": endDatetime,
        "tat_duration": tatDuration,
        "assigned_to": assignedTo,
        "country": country,
        "state": state,
        "city": city,
        "is_approval": isApproval,
        "is_approved": isApproved,
        "approved_datetime": approvedDatetime,
        "is_rejected": isRejected,
        "rejected_datetime": rejectedDatetime,
        "is_followup": isFollowup,
        "reminder_id": reminderId,
        "is_meeting": isMeeting,
        "event_id": eventId,
        "category_id": categoryId,
        "source_type_id": sourceTypeId,
        "source_id": sourceId,
        "division_id": divisionId,
        "form_id": formId,
        "status_id": statusId,
        "stage_id": stageId,
        "priority_id": priorityId,
        "unique_id": uniqueId,
        "contact_id": contactId,
        "call_id": callId,
        "ticket_id": ticketId,
        "lead_id": leadId,
        "is_completed": isCompleted,
        "created_datetime": createdDatetime.toIso8601String(),
        "created_by": createdBy,
        "modified_datetime": modifiedDatetime.toIso8601String(),
        "modified_by": modifiedBy,
        "parent_task_id": parentTaskId,
        "is_independent": isIndependent,
        "division_name": divisionName,
        "source_type_desc": sourceTypeDesc,
        "source_desc": sourceDesc,
        "status_desc": statusDesc,
        "stage_desc": stageDesc,
        "priority_desc": priorityDesc,
        "priority_bgcolor": priorityBgcolor,
        "priority_fontcolor": priorityFontcolor,
        "form_desc": formDesc,
        "assigned_to_user": assignedToUser,
        "created_by_user": createdByUser,
        "modified_by_user": modifiedByUser,
        "sub_tasks": subTasks,
        "sub_task_ids": subTaskIds,
        "status_bgcolor": statusBgcolor,
        "status_font_color": statusFontColor,
        "branch": branch,
      };
}

class TaskDropdownlist {
  TaskDropdownlist({
    this.statusId,
    this.statusDesc,
    this.statusOrder,
    this.fontColor,
    this.backgroundColor,
    this.leadCnt,
    this.header,
    this.finalstatusDesc,
    this.anchorlinkStatus,
  });

  String statusId;
  String statusDesc;
  String statusOrder;
  String fontColor;
  String backgroundColor;
  String leadCnt;
  Header header;
  String finalstatusDesc;
  bool anchorlinkStatus;

  factory TaskDropdownlist.fromJson(Map<String, dynamic> json) =>
      TaskDropdownlist(
        statusId: json["status_id"],
        statusDesc: json["status_desc"],
        statusOrder: json["status_order"],
        fontColor: json["font_color"],
        backgroundColor: json["background_color"],
        leadCnt: json["lead_cnt"],
        header: json["header"] == null ? null : Header.fromJson(json["header"]),
        finalstatusDesc: json["finalstatus_desc"],
        anchorlinkStatus: json["anchorlink_status"],
      );

  Map<String, dynamic> toJson() => {
        "status_id": statusId,
        "status_desc": statusDesc,
        "status_order": statusOrder,
        "font_color": fontColor,
        "background_color": backgroundColor,
        "lead_cnt": leadCnt,
        "header": header == null ? null : header.toJson(),
        "finalstatus_desc": finalstatusDesc,
        "anchorlink_status": anchorlinkStatus,
      };
}

class Header {
  Header({
    this.totalRecords,
  });

  dynamic totalRecords;

  factory Header.fromJson(Map<String, dynamic> json) => Header(
        totalRecords: json["total_records"],
      );

  Map<String, dynamic> toJson() => {
        "total_records": totalRecords,
      };
}
