// To parse this JSON data, do
//
//     final taskSource = taskSourceFromJson(jsonString);

import 'dart:convert';

List<TaskSource> taskSourceFromJson(String str) =>
    List<TaskSource>.from(json.decode(str).map((x) => TaskSource.fromJson(x)));

String taskSourceToJson(List<TaskSource> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskSource {
  TaskSource({
    this.sourceTypeId,
    this.sourceId,
    this.sourceDesc,
    this.sourceOrder,
    this.sourceTypeName,
  });

  String sourceTypeId;
  String sourceId;
  String sourceDesc;
  String sourceOrder;
  String sourceTypeName;

  factory TaskSource.fromJson(Map<String, dynamic> json) => TaskSource(
        sourceTypeId: json["source_type_id"],
        sourceId: json["source_id"],
        sourceDesc: json["source_desc"],
        sourceOrder: json["source_order"],
        sourceTypeName: json["source_type_name"],
      );

  Map<String, dynamic> toJson() => {
        "source_type_id": sourceTypeId,
        "source_id": sourceId,
        "source_desc": sourceDesc,
        "source_order": sourceOrder,
        "source_type_name": sourceTypeName,
      };
}
