// To parse this JSON data, do
//
//     final taskUserDivision = taskUserDivisionFromJson(jsonString);

import 'dart:convert';

List<TaskUserDivision> taskUserDivisionFromJson(String str) =>
    List<TaskUserDivision>.from(
        json.decode(str).map((x) => TaskUserDivision.fromJson(x)));

String taskUserDivisionToJson(List<TaskUserDivision> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TaskUserDivision {
  TaskUserDivision({
    this.userId,
    this.userName,
    this.roleId,
  });

  String userId;
  String userName;
  String roleId;

  factory TaskUserDivision.fromJson(Map<String, dynamic> json) =>
      TaskUserDivision(
        userId: json["user_id"],
        userName: json["user_name"],
        roleId: json["role_id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "user_name": userName,
        "role_id": roleId,
      };
}
