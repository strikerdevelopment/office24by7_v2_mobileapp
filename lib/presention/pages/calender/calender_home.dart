import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
// import 'package:sticky_headers/sticky_headers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CalenderHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNotLeading(title: "Calendar Page"),
            BodyInner(
              page: FirebaseDB(),
            ),
          ],
        ),
      ),
    );
  }
}

class FirebaseDB extends StatefulWidget {
  const FirebaseDB({
    Key key,
  }) : super(key: key);

  @override
  _FirebaseDBState createState() => _FirebaseDBState();
}

class _FirebaseDBState extends State<FirebaseDB> {
  FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Text("data");
  }
}


                
// List.generate(
//   50,
//   (index) => StickyHeader(
//     header: Container(
//       width: double.infinity,
//       padding: EdgeInsets.all(10),
//       child: Text(
//         "stickyHeader $index",
//         textAlign: TextAlign.center,
//       ),
//       color: Colors.green,
//     ),
//     content: Container(
//       height: 250,
//       color: Colors.green[100],
//     ),
//   ),
// ),

// class MySliverAppBar extends SliverPersistentHeaderDelegate {
//   final double expandedHeight;
//   MySliverAppBar({@required this.expandedHeight});

//   @override
//   Widget build(
//       BuildContext context, double shrinkOffset, bool overlapsContent) {
//     return Stack(
//       fit: StackFit.expand,
//       clipBehavior: Clip.none,
//       children: [
//         Image.network(
//           "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
//           fit: BoxFit.cover,
//         ),
//         Center(
//           child: Opacity(
//             opacity: shrinkOffset / expandedHeight,
//             child: Text(
//               "MySliverAppBar",
//               style: TextStyle(
//                 color: Colors.white,
//                 fontWeight: FontWeight.w700,
//                 fontSize: 23,
//               ),
//             ),
//           ),
//         ),
//         Positioned(
//           top: expandedHeight / 2 - shrinkOffset,
//           left: MediaQuery.of(context).size.width / 4,
//           child: Opacity(
//             opacity: (1 - shrinkOffset / expandedHeight),
//             child: Card(
//               elevation: 10,
//               child: SizedBox(
//                 height: expandedHeight,
//                 width: MediaQuery.of(context).size.width / 2,
//                 child: FlutterLogo(),
//               ),
//             ),
//           ),
//         ),
//       ],
//     );
//   }

//   @override
//   double get maxExtent => expandedHeight;

//   @override
//   double get minExtent => kToolbarHeight;

//   @override
//   bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
// }
