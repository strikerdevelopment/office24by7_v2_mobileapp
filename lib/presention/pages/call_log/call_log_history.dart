import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/callog_model/callog.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

CallogRepository _callogRepository =
    CallogRepository(callogAPIClient: CallogAPIClient());

CallsLogHistoryBloc _callsLogHistoryBloc;

class CallsLogHistory extends StatelessWidget {
  final CallsLogModel callsLogData;

  CallsLogHistory({Key key, @required this.callsLogData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              CallsLogHistoryBloc(callogRepository: _callogRepository)
                ..add(CallsLogHistoryPressed(
                    callerPhoneNumber: callsLogData.callerNumber)),
        ),
        BlocProvider(
            create: (context) =>
                ClickToCallBloc(callogRepository: _callogRepository)),
      ],
      child: Scaffold(
        body: Container(
          color: bodyBG,
          child: Stack(
            children: [
              AppbarCurve(),
              AppbarNormal(
                title: "Call History ",
              ),
              BodyInner(
                  page: Column(
                children: [
                  Container(
                    padding: tabBodyPadd,
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(250),
                            ),
                            child: Container(
                              decoration: roundShadow,
                              height: 50,
                              width: 50,
                              child: ImageLoad(
                                  img: '/mobile_app_v2/o_profile_pic.png'),
                            ),
                          ),
                        ),
                        Container(
                          width: 120,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              profileText(callsLogData.maskingCallerNumber, 16),
                              customText(
                                  callsLogData.maskingCallerNumber, 14, black)
                            ],
                          ),
                        ),
                        SizedBox(width: 15),
                        Expanded(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: callsLogTabData.map((historyData) {
                                return BlocListener<ClickToCallBloc,
                                    ClickToCallState>(
                                  listener: (context, state) {
                                    if (state is ClickToCallSuccess) {
                                      successAlert(context, state.stateData);
                                    }
                                    if (state is ClickToCallFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<ClickToCallBloc,
                                      ClickToCallState>(
                                    builder: (context, state) {
                                      if (state is ClickToCallInProgress) {
                                        return LoaderSmallIndicator();
                                      }
                                      return FloatingCallsLogTabButton(
                                        data: historyData,
                                        onPressed: () {
                                          print("object");
                                          BlocProvider.of<ClickToCallBloc>(
                                                  context)
                                              .add(ClickToCallPressed(
                                                  phoneNumber:
                                                      callsLogData.callerNumber,
                                                  callId: callsLogData.callId));
                                        },
                                      );
                                    },
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(width: 1, color: textGray)),
                    ),
                  ),
                  BlocBuilder<CallsLogHistoryBloc, CallsLogHistoryState>(
                    builder: (context, state) {
                      if (state is CallsLogHistorySuccess) {
                        return state.stateData.isEmpty
                            ? Center(
                                child: NoDataFound(
                                message: "No Calls Log Data Found...",
                              ))
                            : Expanded(
                                child: CallsLogHistoryData(
                                    state: state,
                                    callerPhoneNumber:
                                        callsLogData.callerNumber),
                              );
                      }
                      if (state is CallsLogHistoryFailure) {
                        return Center(
                            child: FailedToLoad(message: state.error));
                      }
                      return LoaderIndicator();
                    },
                  ),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}

class CallsLogHistoryData extends StatefulWidget {
  final CallsLogHistorySuccess state;
  final String callerPhoneNumber;

  const CallsLogHistoryData({Key key, this.state, this.callerPhoneNumber})
      : super(key: key);

  @override
  _CallsLogHistoryDataState createState() => _CallsLogHistoryDataState();
}

class _CallsLogHistoryDataState extends State<CallsLogHistoryData> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _callsLogHistoryBloc = BlocProvider.of<CallsLogHistoryBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.all(10),
      itemBuilder: (BuildContext context, int index) {
        bool last = widget.state.stateData.length == (index + 1);
        print(widget.state.currentState.length);
        return index >= widget.state.stateData.length
            ? widget.state.currentState.length >= Environment.dataDisplayCount
                ? LoaderSmallIndicator()
                : Container()
            : CallsLogHistoryCard(
                data: widget.state.stateData[index], last: last);
      },
      itemCount: widget.state.hasReachedMax
          ? widget.state.stateData.length
          : widget.state.stateData.length + 1,
      controller: _scrollController,
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _callsLogHistoryBloc.add(
          CallsLogHistoryPressed(callerPhoneNumber: widget.callerPhoneNumber));
    }
  }
}

class CallsLogHistoryCard extends StatelessWidget {
  final CallsLogHistoryModel data;
  final bool last;

  const CallsLogHistoryCard({Key key, this.data, this.last}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: last ? EdgeInsets.only(bottom: 80) : EdgeInsets.only(bottom: 15),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.only(right: 10),
            child: Center(
              child: callStatusSymbol(data),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: bodyBG,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("${formateCallsLogDT(data.callTime)}"),
                SizedBox(height: 3),
                Row(
                  children: [
                    Text(data.maskingCallerNumber,
                        style: TextStyle(fontSize: 10)),
                    SizedBox(width: 15),
                    Text(data.conversationDuration,
                        style: TextStyle(fontSize: 10)),
                  ],
                ),
              ],
            ),
          ),
          Container(
            width: 30,
            height: 30,
            margin: EdgeInsets.only(left: 15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: bodyBG,
            ),
            child: IconButton(
              icon: Icon(Icons.play_arrow),
              iconSize: 14,
              onPressed: () {
                audioPlayeBottomSheet(context, data.recordingFile);
              },
            ),
          ),
        ],
      ),
    );
  }

  Icon callStatusSymbol(CallsLogHistoryModel data) {
    if (data.callType == 'Outgoing' && data.obdConnected == 1) {
      return Icon(
        Icons.call_made,
        color: green,
      );
    } else if (data.callType == 'Outgoing' && data.obdCallerMissed == 1) {
      return Icon(
        Icons.call_made,
        color: red,
      );
    } else if (data.callType == 'Outgoing' && data.obdAgentMissed == 1) {
      return Icon(
        Icons.call_missed,
        color: red,
      );
    } else if (data.callType == 'Incoming' && data.ibdConnected == 1) {
      return Icon(
        Icons.call_received,
        color: green,
      );
    } else if (data.callType == 'Incoming' && data.ibdAgentMissed == 1) {
      return Icon(
        Icons.call_missed,
        color: green,
      );
    } else if (data.callType == 'Incoming' && data.ibdCallerMissed == 1) {
      return Icon(
        Icons.call_made,
        color: red,
      );
    } else {
      // print(data.);
      return Icon(
        Icons.help,
      );
    }
  }
}
