import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/call_log/call_log.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

CallogRepository _callogRepository =
    CallogRepository(callogAPIClient: CallogAPIClient());

class CallLogHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNotLeading(title: "Call Log"),
            BodyInner(
              page: BlocProvider( 
                create: (context) =>
                    CallsLogBloc(callogRepository: _callogRepository)
                      ..add(CallsLogPressed()),
                child: RefreshIndicator(
                    onRefresh: () async {
                      callsLogBloc.add(CallsLogPressed());
                    },
                    color: purple,
                    child: CallsLogBody()),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingButtonCustome(
        icon: Icon(Icons.dialpad),
        onPressed: () {
          dialPad(context).then((value) {
            if (value['status'] == 'success') {
              successAlert(context, value['message']);
            } else {
              errorAlert(context, value['message']);
            }
          });
        },
      ),
    );
  }
}
