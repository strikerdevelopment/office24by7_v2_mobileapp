import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/call_log/call_log.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

CallsLogBloc callsLogBloc;

class CallsLogBody extends StatelessWidget {
  const CallsLogBody({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CallsLogBloc, CallsLogState>(
      builder: (context, state) {
        if (state is CallsLogSuccess) {
          return state.stateData.isEmpty
              ? Center(
                  child: NoDataFound(
                  message: "No Calls Log Data Found...",
                ))
              : CallsLogData(state: state);
        }
        if (state is CallsLogFailure) {
          return Center(child: FailedToLoad(message: state.error));
        }
        return LoaderIndicator();
      },
    );
  }
}

class CallsLogData extends StatefulWidget {
  final CallsLogSuccess state;

  const CallsLogData({Key key, this.state}) : super(key: key);

  @override
  _CallsLogDataState createState() => _CallsLogDataState();
}

class _CallsLogDataState extends State<CallsLogData> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    callsLogBloc = BlocProvider.of<CallsLogBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.all(10),
      itemBuilder: (BuildContext context, int index) {
        bool last = widget.state.stateData.length == (index + 1);
        print(widget.state.currentState.length);
        return index >= widget.state.stateData.length
            ? widget.state.currentState.length >= Environment.dataDisplayCount
                ? LoaderSmallIndicator()
                : Container()
            : CallsLogCard(data: widget.state.stateData[index], last: last);
      },
      itemCount: widget.state.hasReachedMax
          ? widget.state.stateData.length
          : widget.state.stateData.length + 1,
      controller: _scrollController,
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      callsLogBloc.add(CallsLogPressed());
    }
  }
}

class CallsLogCard extends StatelessWidget {
  final CallsLogModel data;
  final bool last;

  const CallsLogCard({Key key, this.data, this.last}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: last ? EdgeInsets.only(bottom: 80) : EdgeInsets.only(bottom: 15),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.only(right: 10),
            child: Center(
              child: ImageLoad(
                  img: "/mobile_app_v2/single_contact.png", width: 35),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: data.color.withOpacity(0.3),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(data.maskingCallerNumber == null
                    ? "- -"
                    : "${data.maskingCallerNumber}"),
                SizedBox(height: 3),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 5),
                          child: callStatusSymbol(data),
                        ),
                        // Text(data.callerNumber, style: TextStyle(fontSize: 10)),
                        Text("${formateCallsLogDT(data.callTime)}",
                            style: TextStyle(fontSize: 10)),
                      ],
                    ),
                    // Text("${data.conversationDuration}",
                    //     style: TextStyle(fontSize: 10)),
                  ],
                ),
              ],
            ),
          ),
          Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.only(left: 15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: bodyBG,
            ),
            child: IconButton(
              icon: Icon(Icons.chevron_right),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CallsLogHistory(callsLogData: data),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Icon callStatusSymbol(CallsLogModel data) {
    if (data.callType == 'Outgoing' && data.obdConnected == 1) {
      return Icon(
        Icons.call_made,
        color: green,
        size: 12,
      );
    } else if (data.callType == 'Outgoing' && data.obdCallerMissed == 1) {
      return Icon(
        Icons.call_made,
        color: red,
        size: 12,
      );
    } else if (data.callType == 'Outgoing' && data.obdAgentMissed == 1) {
      return Icon(
        Icons.call_missed,
        color: red,
        size: 12,
      );
    } else if (data.callType == 'Incoming' && data.ibdConnected == 1) {
      return Icon(
        Icons.call_received,
        color: green,
        size: 12,
      );
    } else if (data.callType == 'Incoming' && data.ibdAgentMissed == 1) {
      return Icon(
        Icons.call_missed,
        color: green,
        size: 12,
      );
    } else if (data.callType == 'Incoming' && data.ibdCallerMissed == 1) {
      return Icon(
        Icons.call_made,
        color: red,
        size: 12,
      );
    } else {
      return Icon(
        Icons.help,
        size: 12,
      );
    }
  }
}
