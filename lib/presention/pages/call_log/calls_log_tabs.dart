import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/opportunities/oppo_task.dart';
import 'package:office24by7_v2/presention/pages/opportunities/opportunities.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class CallsLogTabs extends StatelessWidget {
  final CallsLogTabModel callsLogTabData;
  final CallsLogModel data;

  const CallsLogTabs({Key key, this.callsLogTabData, this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10),
      child: FloatingCallsLogTabButton(
        data: callsLogTabData,
        onPressed: () {
          switch (callsLogTabData.tabId) {
            case 'call':
              // oppoCall(context);
              break;
            case 'text':
              oppoSMS(context);
              break;
            case 'email':
              // oppoEmail(context, data);
              break;
            case 'task':
              oppoTask(context);
              break;
            // case 'note':
            //   contactNote(context, contactList);
            //   break;
            // case 'lead':
            //   contactAddToLead(context, contactList);
            //   break;
            // case 'ticket':
            //   contactTicket(context, contactList);
            //   break;
            // case 'history':
            //   contactCall(context, contactList);
            //   break;
          }
          print(callsLogTabData.tabId);
        },
      ),
    );
  }
}
