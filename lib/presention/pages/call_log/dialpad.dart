import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

CallogRepository _callogRepository =
    CallogRepository(callogAPIClient: CallogAPIClient());

Future dialPad(BuildContext context) {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                ClickToCallBloc(callogRepository: _callogRepository),
          ),
          BlocProvider(
            create: (context) =>
                CallerIdsBloc(callogRepository: _callogRepository)
                  ..add(CallerIdsPressed()),
          ),
        ],
        child: ModalBox(
          modalHieght: 350,
          modalHeader: ModalHeader(
              img: "/mobile_app_v2/o_call_modal.png", title: "DialPad"),
          modalBody: Expanded(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
              child: ListView(children: [
                FormBuilder(
                  key: _fbKey,
                  autovalidateMode: AutovalidateMode.always,
                  initialValue: {},
                  child: Column(
                    children: [
                      fieldDistance,
                      BlocBuilder<CallerIdsBloc, CallerIdsState>(
                        builder: (context, state) {
                          if (state is CallerIdsSuccess) {
                            return FormBuilderDropdown(
                              name: 'caller_id',
                              decoration: formGlobal("Select Caller ID"),
                              validator: FormBuilderValidators.compose(
                                  [FormBuilderValidators.required(context)]),
                              items: state.stateData
                                  .map((callerid) => DropdownMenuItem(
                                        value: callerid.calleridNum,
                                        child: Text(callerid.calleridNum),
                                      ))
                                  .toList(),
                            );
                          }
                          if (state is CallerIdsFailure) {
                            return FormBuilderDropdown(
                              name: 'caller_id',
                              decoration: formGlobal("No Caller IDs Found"),
                              enabled: false,
                              items: [],
                              validator: FormBuilderValidators.compose(
                                [
                                  FormBuilderValidators.required(context),
                                ],
                              ),
                            );
                          }
                          return LoaderSmallIndicator();
                        },
                      ),
                      fieldDistance,
                      FormBuilderTextField(
                        name: "phone_number",
                        decoration: formGlobal("Phone Number"),
                        keyboardType: TextInputType.number,
                        validator: (val) {
                          String pattern = r'^[0-9\.\-\/]+$';
                          // r'^(\+91[\-\s]?)?[0]?(91)?[123456789]\d{9}$';
                          RegExp regExp = new RegExp(pattern);
                          if ((val == null || val.isEmpty)) {
                            return 'Please enter only numeric characters';
                          }
                          // if ((val.length != 10)) {
                          //   return 'Please enter 10 digit mobile number';
                          // }
                          if (!regExp.hasMatch(val)) {
                            return 'Please enter only Numbers characters';
                          }
                          return null;
                        },
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          ),
          modalFooter: BlocListener<ClickToCallBloc, ClickToCallState>(
            listener: (context, state) {
              if (state is ClickToCallSuccess) {
                Navigator.pop(
                    context, {"status": "success", "message": state.stateData});
              }
              if (state is ClickToCallFailure) {
                Navigator.pop(
                    context, {"status": "failure", "message": state.error});
              }
            },
            child: BlocBuilder<ClickToCallBloc, ClickToCallState>(
              builder: (context, state) {
                return state is ClickToCallInProgress
                    ? LoaderSmallIndicator()
                    : ModalFooter(
                        firstButton: "Cancel",
                        firstFun: () {
                          Navigator.pop(context);
                        },
                        secondButton: "Call",
                        secondFun: () async {
                          if (_fbKey.currentState.saveAndValidate()) {
                            Map formData = _fbKey.currentState.value;
                            BlocProvider.of<ClickToCallBloc>(context).add(
                                ClickToCallPressed(
                                    callerId: formData['caller_id'],
                                    phoneNumber: formData['phone_number']));
                            print(_fbKey.currentState.value);
                          }
                          //Navigator.pop(context);
                        },
                      );
              },
            ),
          ),
        ),
      );
    },
  );
}
