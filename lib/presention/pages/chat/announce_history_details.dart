import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:file_picker/file_picker.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());
FirebaseRepository _firebaseRepository =
    FirebaseRepository(firebaseAPIClient: FirebaseAPIClient());

AnnounChatHistoryBloc announChatHistoryBloc;
MultiSelectAnnounDataBloc multiSelectAnnounDataBloc;

class AnnounChatHistoryDetails extends StatefulWidget {
  final User user;
  final AuthModel authData;
  const AnnounChatHistoryDetails({this.authData, this.user, Key key})
      : super(key: key);

  @override
  _AnnounChatHistoryDetailsState createState() =>
      _AnnounChatHistoryDetailsState();
}

class _AnnounChatHistoryDetailsState extends State<AnnounChatHistoryDetails> {
  Timer _clockTimer;
  @override
  void initState() {
    _clockTimer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      BlocProvider.of<RefreshGroupHistoryBloc>(context)
        ..add(RefreshGroupHistoryPressed(
            groupId: widget.user.chatGroupId,
            groupUniqueId: widget.user.groupUniqueId,
            groupType: widget.user.groupType));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              AnnounChatHistoryBloc(chatRepository: _chatRepository)
                ..add(AnnounChatHistoryPressed(
                  offset: 0,
                  groupId: widget.user.chatGroupId,
                  groupUniqueId: widget.user.groupUniqueId,
                  groupType: widget.user.groupType,
                )),
        ),
        BlocProvider(
          create: (context) => SendMsgAnnouceBloc(
              chatRepository: _chatRepository,
              firebaseRepository: _firebaseRepository),
        ),
        BlocProvider(
          create: (context) => UploadFilesAnnouceBloc(
              chatRepository: _chatRepository,
              firebaseRepository: _firebaseRepository),
        ),
        BlocProvider(create: (context) => MultiSelectAnnounDataBloc()),
      ],
      child: Scaffold(
        body: Container(
          color: bodyBG,
          child: Stack(
            children: [
              AppbarCurve(),
              BlocBuilder<MultiSelectAnnounDataBloc,
                  MultiSelectAnnounDataState>(builder: (context, state) {
                if (state is MSDannounTextLogIdSuccess) {
                  return ChatAnnounMultiSelectMessage(
                    user: widget.user,
                    chatTextLogId: state.chatTextLogId,
                    shareData: state.singleChatData,
                  );
                }
                return AppbarNormalChatHistory(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${widget.user.name} "),
                      Text(" ${widget.user.phone}",
                          style: TextStyle(fontSize: 11)),
                    ],
                  ),
                  actions: [
                    // IconButton(
                    //     icon: Icon(Icons.refresh),
                    //     onPressed: () {
                    //       announChatHistoryBloc.add(AnnounChatHistoryRefresh(
                    //           offset: 0,
                    //           groupId: widget.user.chatGroupId,
                    //           groupUniqueId: widget.user.groupUniqueId,
                    //           groupType: widget.user.groupType));
                    //     }),
                    PopupMenuButton(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.more_vert),
                      ),
                      onSelected: (value) {
                        switch (value) {
                          case 'clear_history':
                            chatClearAnnounHistory(context, widget.user)
                                .then((value) {
                              if (value["status"] == "success") {
                                announChatHistoryBloc.add(
                                    AnnounChatHistoryRefresh(
                                        offset: 0,
                                        groupId: widget.user.chatGroupId,
                                        groupUniqueId:
                                            widget.user.groupUniqueId,
                                        groupType: widget.user.groupType));
                                successAlert(context, value["message"]);
                              } else {
                                errorAlert(context, value["message"]);
                              }
                            });
                            break;
                          case 'delete':
                            deleteGroup(context, widget.user).then((value) {
                              if (value["status"] == "success") {
                                announChatHistoryBloc.add(
                                    AnnounChatHistoryRefresh(
                                        offset: 0,
                                        groupId: widget.user.chatGroupId,
                                        groupUniqueId:
                                            widget.user.groupUniqueId,
                                        groupType: widget.user.groupType));
                                successAlert(context, value["message"]);
                              } else {
                                errorAlert(context, value["message"]);
                              }
                            });
                            break;
                          default:
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ChatGroupInfo(user: widget.user),
                              ),
                            );
                        }
                        // print(value);
                      },
                      itemBuilder: (context) {
                        return chatAnnounHistoryOptionData.map((e) {
                          return PopupMenuItem(
                            value: e.id,
                            child: Row(
                              children: [
                                ImageLoad(img: e.img, width: 25),
                                SizedBox(width: 15),
                                Text(e.name),
                              ],
                            ),
                          );
                        }).toList();
                      },
                    ),
                  ],
                );
              }),
              BodyInner(
                page: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    BlocBuilder<AnnounChatHistoryBloc, AnnounChatHistoryState>(
                      builder: (context, state) {
                        if (state is AnnounChatHistorySuccess) {
                          return BlocListener<RefreshGroupHistoryBloc,
                              RefreshGroupHistoryState>(
                            listener: (context, state) {
                              if (state is RefreshGroupHistorySuccess) {
                                print(state.stateData.cnt);
                                if (state.stateData.cnt != 0) {
                                  announChatHistoryBloc.add(
                                      AnnounChatHistoryRefresh(
                                          offset: 0,
                                          groupId: widget.user.chatGroupId,
                                          groupUniqueId:
                                              widget.user.groupUniqueId,
                                          groupType: widget.user.groupType));
                                }
                              }
                            },
                            child: ChatAnnounBody(
                                chatData: state, user: widget.user),
                          );
                        }
                        if (state is AnnounChatHistoryFailure) {
                          return Expanded(
                            child: ListView(
                              children: [
                                FailedToLoad(message: state.error),
                              ],
                            ),
                          );
                        }
                        return Expanded(
                          child: ListView(
                            children: [
                              LoaderSmallIndicator(),
                            ],
                          ),
                        );
                      },
                    ),
                    widget.authData.isChatEnabled == "1"
                        ? BlocListener<MultiSelectAnnounDataBloc,
                            MultiSelectAnnounDataState>(listener: (context, state) {
                            if (state is MSDannounTextLogIdSuccess) {
                              // print(state.isReply);
                            }
                          }, child: BlocBuilder<MultiSelectAnnounDataBloc,
                                MultiSelectAnnounDataState>(
                            builder: (context, state) {
                            if (state is MSDannounTextLogIdSuccess) {
                              print("$state   asdfasdf asdfasdf");
                              return Column(
                                children: [
                                  state.isReply
                                      ? ForwardAnnounDisplay(
                                          historyData: state.singleChatData,
                                        )
                                      : Container(),
                                  ChatAnnounFooter(
                                      user: widget.user,
                                      singleChatData: state.singleChatData,
                                      authData: widget.authData),
                                ],
                              );
                            }
                            return ChatAnnounFooter(
                                user: widget.user, authData: widget.authData);
                          }))
                        : AlertDangerBox(
                            child: Text(
                                "Note*: You don't have plan to send messages, please contact to support team"),
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _clockTimer.cancel();
    super.dispose();
  }
}

class ForwardAnnounDisplay extends StatelessWidget {
  final ChatHistoryModel historyData;
  const ForwardAnnounDisplay({Key key, this.historyData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // String fileName = historyData.referenceMsg != null
    //     ? historyData.referenceMsg.split('.').last
    //     : "unknown";

    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(6),
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        color: black.withOpacity(0.1),
        borderRadius: BorderRadius.circular(4),
      ),
      child: historyData.fileInfo.isEmpty
          ? Text(
              "${historyData.msg}",
            )
          : Wrap(
              spacing: 2,
              children: historyData.fileInfo
                  .map((ChatFileInfo file) => ChatDisplayFiles(
                      fileInfo: file,
                      referenceFileInfo: historyData.fileInfo,
                      width: 30))
                  .toList(),
            ),
    );
  }
}

class ChatAnnounBody extends StatefulWidget {
  final User user;
  final AnnounChatHistorySuccess chatData;
  const ChatAnnounBody({Key key, this.chatData, this.user}) : super(key: key);

  @override
  _ChatAnnounBodyState createState() => _ChatAnnounBodyState();
}

class _ChatAnnounBodyState extends State<ChatAnnounBody> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  MultiSelectController mlController = MultiSelectController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    multiSelectAnnounDataBloc =
        BlocProvider.of<MultiSelectAnnounDataBloc>(context);
    announChatHistoryBloc = BlocProvider.of<AnnounChatHistoryBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return widget.chatData.stateData.isEmpty ||
            widget.chatData.stateData == null
        ? Expanded(
            child: Center(
              child: ListView(
                shrinkWrap: true,
                children: [
                  NoDataFound(message: "No Data Found..."),
                ],
              ),
            ),
          )
        : Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(0),
              reverse: true,
              shrinkWrap: false,
              itemCount: widget.chatData.hasReachedMax
                  ? widget.chatData.stateData.length
                  : widget.chatData.stateData.length + 1,
              controller: _scrollController,
              itemBuilder: (context, index) {
                if (index >= widget.chatData.stateData.length) {
                  if (widget.chatData.currentState.length >=
                      Environment.dataDisplayCount) {
                    return LoaderSmallIndicator();
                  } else {
                    return Container();
                  }
                } else {
                  String presentDate = widget.chatData.stateData[index].date;
                  String futureDate =
                      index + 1 >= widget.chatData.stateData.length
                          ? "santosh"
                          : widget.chatData.stateData[index + 1].date;
                  return Column(
                    children: [
                      StickyHeader(
                        header: futureDate != presentDate
                            ? Container(
                                alignment: Alignment.center,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: bodyBG,
                                  ),
                                  margin: EdgeInsets.only(top: 5, bottom: 5),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 3),
                                  child: Text(
                                      chatHistoryDateFormate(widget
                                          .chatData.stateData[index].date),
                                      style: TextStyle(fontSize: 10)),
                                ),
                              )
                            : Container(),
                        content: BlocListener<MultiSelectAnnounDataBloc,
                            MultiSelectAnnounDataState>(
                          listener: (context, state) {
                            if (state is MultiSelectAnnounDataInitial) {
                              setState(() {
                                mlController.deselectAll();
                              });
                            }
                          },
                          child: MultiSelectItem(
                            isSelecting: mlController.isSelecting,
                            onSelected: () {
                              List selectedChatTextLogId = [];
                              ChatHistoryModel singleChatMsg;
                              setState(() {
                                mlController.toggle(index);
                                mlController.selectedIndexes.map((e) {
                                  selectedChatTextLogId.add(widget
                                      .chatData.stateData[e].chatTextLogId);
                                }).toList();

                                if (mlController.selectedIndexes.length == 1) {
                                  singleChatMsg = widget.chatData.stateData[
                                      mlController.selectedIndexes[0]];
                                }
                              });
                              selectedChatTextLogId.isEmpty
                                  ? multiSelectAnnounDataBloc
                                      .add(MSDannounTextLogIdClose())
                                  : mlController.selectedIndexes.length == 1
                                      ? multiSelectAnnounDataBloc
                                          .add(MSDannounTextLogIdPressed(
                                          isReply: false,
                                          chatTextLogId: selectedChatTextLogId,
                                          singleChatData: singleChatMsg,
                                        ))
                                      : multiSelectAnnounDataBloc
                                          .add(MSDannounTextLogIdPressed(
                                          isReply: false,
                                          chatTextLogId: selectedChatTextLogId,
                                          singleChatData: null,
                                        ));
                            },
                            child: Container(
                              color: mlController.isSelected(index)
                                  ? dartpurple.withOpacity(0.2)
                                  : transparent,
                              width: double.infinity,
                              margin: EdgeInsets.only(bottom: 2),
                              child: ChatBubbleBody(
                                  historyData: widget.chatData.stateData[index],
                                  chatType: widget.user.groupType),
                            ),
                          ),
                        ),
                      )
                    ],
                  );
                }
              },
            ),
          );
  }

  String chatHistoryDateFormate(String date) {
    DateTime dateType = DateTime.parse(date);
    String formattedDate = DateFormat('dd MMM, yyyy').format(dateType);

    return formattedDate;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      announChatHistoryBloc.add(AnnounChatHistoryPressed(
          groupId: widget.user.chatGroupId,
          groupUniqueId: widget.user.groupUniqueId,
          groupType: widget.user.groupType));
    }
  }
}

class ChatAnnounFooter extends StatefulWidget {
  final User user;
  final AuthModel authData;
  final ChatHistoryModel singleChatData;
  const ChatAnnounFooter(
      {Key key, this.user, this.authData, this.singleChatData})
      : super(key: key);

  @override
  _ChatAnnounFooterState createState() => _ChatAnnounFooterState();
}

class _ChatAnnounFooterState extends State<ChatAnnounFooter> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  bool inputHidden = true;
  bool validationCheck = false;

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: _fbKey,
      autovalidateMode: AutovalidateMode.disabled,
      child: Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.only(bottom: Platform.isIOS ? 15 : 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Stack(
                children: [
                  FormBuilderTextField(
                    autovalidateMode: validationCheck
                        ? AutovalidateMode.onUserInteraction
                        : AutovalidateMode.disabled,
                    name: "message",
                    decoration: chatTextInput("Type Message..."),
                    enabled: inputHidden,
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(context),
                    ]),
                    onChanged: (value) {
                      setState(() {
                        validationCheck = true;
                      });
                    },
                  ),
                  Positioned(
                    right: 2,
                    top: 1,
                    child: BlocListener<UploadFilesAnnouceBloc,
                        UploadFilesAnnouceState>(
                      listener: (context, state) {
                        if (state is UploadFilesAnnouceFailure) {
                          return errorAlert(context, state.error);
                        }
                        if (state is UploadFilesAnnouceSuccess) {
                          announChatHistoryBloc.add(AnnounChatHistoryRefresh(
                              offset: 0,
                              groupId: widget.user.chatGroupId,
                              groupUniqueId: widget.user.groupUniqueId,
                              groupType: widget.user.groupType));
                          multiSelectAnnounDataBloc
                              .add(MSDannounTextLogIdClose());
                        }
                      },
                      child: BlocBuilder<UploadFilesAnnouceBloc,
                          UploadFilesAnnouceState>(
                        builder: (context, state) {
                          if (state is UploadFilesAnnouceInProgress) {
                            return LoaderSmallIndicator();
                          }
                          return RaisedChatGradientButton(
                            child: Icon(Icons.attach_file, color: purple),
                            width: 45,
                            onPressed: () => uploadAnnounFilterfiles(
                                context,
                                widget.user,
                                widget.singleChatData,
                                widget.authData),
                          );
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            BlocListener<SendMsgAnnouceBloc, SendMsgAnnouceState>(
              listener: (context, state) {
                if (state is SendMsgAnnouceFailure) {
                  return errorAlert(context, state.error);
                }
                if (state is SendMsgAnnouceSuccess) {
                  announChatHistoryBloc.add(AnnounChatHistoryRefresh(
                      offset: 0,
                      groupId: widget.user.chatGroupId,
                      groupUniqueId: widget.user.groupUniqueId,
                      groupType: widget.user.groupType));
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  setState(() {
                    _fbKey.currentState.reset();
                  });
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  multiSelectAnnounDataBloc.add(MSDannounTextLogIdClose());
                  setState(() {
                    _fbKey.currentState.reset();
                    inputHidden = true;
                    validationCheck = false;
                  });
                }
                if (state is SendMsgAnnouceInProgress) {
                  setState(() {
                    inputHidden = false;
                  });
                }
              },
              child: BlocBuilder<SendMsgAnnouceBloc, SendMsgAnnouceState>(
                builder: (context, state) {
                  if (state is SendMsgAnnouceInProgress) {
                    return Container(
                      margin: EdgeInsets.only(left: 5, top: 2),
                      child: LoaderSmallIndicator(),
                    );
                  }
                  return Container(
                    margin: EdgeInsets.only(left: 5, top: 2),
                    child: RaisedGradientButton(
                      child: Icon(Icons.arrow_forward, color: white),
                      gradient: LinearGradient(colors: purpleTtL),
                      width: 45,
                      onPressed: () {
                        if (_fbKey.currentState.saveAndValidate()) {
                          Map formData = _fbKey.currentState.value;
                          BlocProvider.of<SendMsgAnnouceBloc>(context)
                              .add(SendMsgAnnoucePressed(
                            user: widget.user,
                            authData: widget.authData,
                            msg: formData["message"],
                            msgReferenceId: widget.singleChatData == null
                                ? "0"
                                : widget.singleChatData.chatTextLogId,
                          ));
                        }
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ChatAnnounMultiSelectMessage extends StatefulWidget {
  final User user;
  final ChatHistoryModel shareData;
  final List chatTextLogId;
  ChatAnnounMultiSelectMessage(
      {Key key, this.user, this.shareData, this.chatTextLogId})
      : super(key: key);

  @override
  _ChatAnnounMultiSelectMessageState createState() =>
      _ChatAnnounMultiSelectMessageState();
}

class _ChatAnnounMultiSelectMessageState
    extends State<ChatAnnounMultiSelectMessage> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              DeleteMsgAnnouceBloc(chatRepository: _chatRepository),
        ),
      ],
      child: AppbarChatHistoryMultSelect(
        onPressed: () {
          BlocProvider.of<MultiSelectAnnounDataBloc>(context)
              .add(MSDannounTextLogIdClose());
        },
        child: Text("${widget.chatTextLogId.length}"),
        actions: [
          widget.chatTextLogId.length == 1
              ? IconButton(
                  color: white,
                  icon: Icon(Icons.share),
                  onPressed: () => shareContent(context, widget.shareData),
                )
              : Container(),
          widget.chatTextLogId.length == 1
              ? IconButton(
                  color: white,
                  icon: Icon(Icons.reply),
                  onPressed: () {
                    BlocProvider.of<MultiSelectAnnounDataBloc>(context)
                        .add(MSDannounTextLogIdPressed(
                      isReply: true,
                      chatTextLogId: widget.chatTextLogId,
                      singleChatData: widget.shareData,
                    ));
                  },
                )
              : Container(),
          IconButton(
            color: white,
            icon: Icon(Icons.send),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChatUserMessageForward(
                      chatTextLogId: widget.chatTextLogId),
                ),
              );
            },
          ),
          BlocListener<DeleteMsgAnnouceBloc, DeleteMsgAnnouceState>(
            listener: (context, state) {
              if (state is DeleteMsgAnnouceSuccess) {
                successAlert(context, state.stateData);
                announChatHistoryBloc.add(AnnounChatHistoryRefresh(
                    offset: 0,
                    groupId: widget.user.chatGroupId,
                    groupUniqueId: widget.user.groupUniqueId,
                    groupType: widget.user.groupType));
                BlocProvider.of<MultiSelectAnnounDataBloc>(context)
                    .add(MSDannounTextLogIdClose());
              }
              if (state is DeleteMsgAnnouceFailure) {
                errorAlert(context, state.error);
              }
            },
            child: BlocBuilder<DeleteMsgAnnouceBloc, DeleteMsgAnnouceState>(
              builder: (context, state) {
                return IconButton(
                  color: white,
                  icon: state is DeleteMsgAnnouceInProgress
                      ? WhiteLoaderSmallIndicator()
                      : Icon(Icons.delete),
                  onPressed: () {
                    BlocProvider.of<DeleteMsgAnnouceBloc>(context)
                        .add(DeleteMsgAnnoucePressed(
                      chatTextLogId: widget.chatTextLogId,
                      groupId: widget.user.chatGroupId,
                      groupUniqueId: widget.user.groupUniqueId,
                      groupType: widget.user.groupType,
                    ));
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

List<File> announFiles;

Future uploadAnnounFilterfiles(BuildContext context, User user,
    ChatHistoryModel singleMsgData, AuthModel authData) async {
  FilePickerResult result = await FilePicker.platform.pickFiles(
    allowMultiple: true,
    type: FileType.custom,
    allowedExtensions: [
      'doc',
      'docx',
      'pdf',
      'xls',
      'xlsx',
      'jpg',
      'jpeg',
      'png',
      'php',
      'txt',
      'sql',
      'java',
      'ts',
      'html',
      'mp4',
      'mp3',
      'mpeg',
      'wav',
      'css'
    ],
  );
  if (result != null) {
    // await uploadFiles(result.paths);
    // print(result.paths);
    BlocProvider.of<UploadFilesAnnouceBloc>(context).add(
        UploadFilesAnnoucePressed(
            user: user,
            authData: authData,
            files: result.paths,
            msgReferenceId:
                singleMsgData == null ? "0" : singleMsgData.chatTextLogId));
  } else {
    // User canceled the picker
  }
}
