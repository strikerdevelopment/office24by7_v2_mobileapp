import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

Future chatBlocUser(BuildContext context, User user, int isBlockUser) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return BlocProvider(
            create: (context) =>
                ChatBlockUserBloc(chatRepository: _chatRepository),
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_clear.png",
                  title: isBlockUser == 0 ? "Block User" : "Unblock User"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    Column(
                      children: [
                        Text(
                            isBlockUser == 0
                                ? "Are you sure you want to block user..."
                                : "Are you sure you want to unblock user...",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.center),
                        fieldDistance,
                      ],
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<ChatBlockUserBloc, ChatBlockUserState>(
                listener: (context, state) {
                  if (state is ChatBlockUserSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "message": state.stateData});
                  }
                  if (state is ChatBlockUserFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "message": state.error});
                  }
                },
                child: BlocBuilder<ChatBlockUserBloc, ChatBlockUserState>(
                  builder: (context, state) {
                    return state is ChatBlockUserInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              BlocProvider.of<ChatBlockUserBloc>(context).add(
                                  ChatBlockUserPressed(userId: user.userId));
                            });
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
