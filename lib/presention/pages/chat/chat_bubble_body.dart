import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/chat/document_preview.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ChatBubbleBody extends StatelessWidget {
  final ChatHistoryModel historyData;
  final String chatType;
  const ChatBubbleBody({Key key, this.historyData, this.chatType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChatCustomBubble(
      color:
          historyData.cardPosition == 'right' ? chatRightColor : chatLeftColor,
      isSender: historyData.cardPosition == 'right' ? true : false,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: 50,
        ),
        child: Container(
          padding: EdgeInsets.only(bottom: 15),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              historyData.fileInfo.isEmpty
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        chatType == "0"
                            ? customText(historyData.name, 12, purple)
                            : Container(width: 100),
                        historyData.referenceMsg == ""
                            ? Container(width: 0)
                            : replyMessageDisplay(historyData),
                        Text("${historyData.msg}"),
                      ],
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        chatType == "0"
                            ? customText(historyData.name, 12, purple)
                            : Container(width: 100),
                        historyData.referenceMsg == ""
                            ? Container(width: 0)
                            : replyMessageDisplay(historyData),
                        SizedBox(height: 4),
                        Wrap(
                          spacing: 2,
                          children: historyData.fileInfo
                              .map((ChatFileInfo file) => ChatDisplayFiles(
                                  fileInfo: file,
                                  referenceFileInfo: historyData.fileInfo))
                              .toList(),
                        ),
                      ],
                    ),
              Positioned(
                bottom: -15,
                right: 0,
                child: customTextRight("${historyData.time}", 10, textGray),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container replyMessageDisplay(ChatHistoryModel historyData) {
    // String fileName = historyData.referenceMsg != null
    //     ? historyData.referenceMsg.split('.').last
    //     : "unknown";

    return Container(
        padding: EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: black.withOpacity(0.1),
          borderRadius: BorderRadius.circular(4),
        ),
        child: historyData.referenceFileInfo.isEmpty
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  chatType == "0"
                      ? customText(historyData.refFromName, 12, purple)
                      : Container(width: 100),
                  SizedBox(height: 4),
                  Text(
                    "${historyData.referenceMsg}",
                    style: TextStyle(fontSize: 11),
                  ),
                ],
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  chatType == "0"
                      ? customText(historyData.refFromName, 12, purple)
                      : Container(width: 100),
                  SizedBox(height: 4),
                  Wrap(
                    spacing: 2,
                    children: historyData.referenceFileInfo
                        .map((ChatFileInfo file) => ChatDisplayFiles(
                            fileInfo: file,
                            referenceFileInfo: historyData.referenceFileInfo))
                        .toList(),
                  ),
                ],
              ));
  }
}

class ChatDisplayFiles extends StatelessWidget {
  final ChatFileInfo fileInfo;
  final List<ChatFileInfo> referenceFileInfo;
  final double width;

  const ChatDisplayFiles(
      {Key key, this.fileInfo, this.referenceFileInfo, this.width = 80})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DocmentPreview(
              fileInfo: fileInfo,
              referenceFileInfo: referenceFileInfo,
            ),
          ),
        );
      },
      child: getFileExtension(context, fileInfo, width),
    );
  }
}

Widget getFileExtension(
    BuildContext context, ChatFileInfo fileinfo, double width) {
  var fileName =
      fileinfo.fileName != null ? fileinfo.fileName.split('.').last : "unknown";
  // print(fileName);
  switch (fileName) {
    case 'jpg':
      return ImageChatLoad(
        img: fileinfo.src,
        width: width,
      );
      break;
    case 'jpeg':
      return ImageChatLoad(
        img: fileinfo.src,
        width: width,
      );
      break;
    case 'png':
      return ImageChatLoad(
        img: fileinfo.src,
        width: width,
      );
      break;
    default:
      return Column(
        children: [
          Row(
            children: [
              Icon(Icons.file_present),
              Expanded(
                  child: Text(
                fileinfo.fileName != null ? fileinfo.fileName : "unknown",
                overflow: TextOverflow.ellipsis,
              )),
            ],
          ),
        ],
      );
  }
}
