import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

Future chatClearAnnounHistory(BuildContext context, User user) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return BlocProvider(
            create: (context) =>
                ClearGroupChatHistoryBloc(chatRepository: _chatRepository),
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_clear.png",
                  title: "Clear History"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    Column(
                      children: [
                        Text("Are you sure you want to clear history...",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.center),
                        fieldDistance,
                      ],
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<ClearGroupChatHistoryBloc,
                  ClearGroupChatHistoryState>(
                listener: (context, state) {
                  if (state is ClearGroupChatHistorySuccess) {
                    Navigator.pop(context,
                        {"status": "success", "message": state.stateData});
                  }
                  if (state is ClearGroupChatHistoryFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "message": state.error});
                  }
                },
                child: BlocBuilder<ClearGroupChatHistoryBloc,
                    ClearGroupChatHistoryState>(
                  builder: (context, state) {
                    return state is ClearGroupChatHistoryInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "No",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Yes",
                            secondFun: () async {
                              BlocProvider.of<ClearGroupChatHistoryBloc>(
                                      context)
                                  .add(ClearGroupChatHistoryPressed(
                                groupId: user.chatGroupId,
                                groupUniqueId: user.groupUniqueId,
                                groupType: user.groupType,
                              ));
                            });
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
