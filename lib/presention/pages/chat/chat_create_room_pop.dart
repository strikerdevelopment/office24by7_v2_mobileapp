import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_group_info_model.dart';
import 'package:office24by7_v2/presention/pages/chat/chat.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

Future chatCreateRoomPop(BuildContext context, String groupType,
    List<String> selectedChatUsers, ChatGroupInfoModel groupInfo) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

      return BlocProvider(
        create: (context) => AddChatGroupBloc(chatRepository: _chatRepository),
        child: StatefulBuilder(
          builder: (BuildContext context, setState) {
            return ModalBox(
              modalHieght: 350,
              modalHeader: ModalHeader(
                  img: groupType == "1"
                      ? "/mobile_app_v2/o_option_group.png"
                      : "/mobile_app_v2/o_option_voice.png",
                  title: groupType == "1"
                      ? groupInfo == null
                          ? "Create Announcement"
                          : "Update Announcement"
                      : groupInfo == null
                          ? "Create Group"
                          : "Update Group"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        "group_type": groupType,
                        "group_name":
                            groupInfo == null ? null : groupInfo.groupName,
                        "description": groupInfo == null
                            ? null
                            : groupInfo.groupDescription,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: "group_type"),
                          FormBuilderTextField(
                            name: "group_name",
                            decoration: formGlobal("Name"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          SizedBox(height: 15),
                          groupType == "1"
                              ? Container()
                              : FormBuilderTextField(
                                  name: "description",
                                  decoration: formGlobal("Description"),
                                  keyboardType: TextInputType.multiline,
                                  validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context)],
                                  ),
                                ),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<AddChatGroupBloc, AddChatGroupState>(
                listener: (context, state) {
                  if (state is AddChatGroupSuccess) {
                    successAlert(context, state.stateData);
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    chatUsersBloc.add(ChatUsersPressedRefresh(
                        searchTerm: "", isHierarchyUsers: "0"));
                  }
                  if (state is AddChatGroupFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "message": state.error});
                    errorAlert(context, state.error);
                    Navigator.of(context).popUntil((route) => route.isFirst);
                  }
                },
                child: BlocBuilder<AddChatGroupBloc, AddChatGroupState>(
                  builder: (context, state) {
                    return state is AddChatGroupInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<AddChatGroupBloc>(context)
                                    .add(AddChatGroupPressed(
                                  formData: formData,
                                  groupId: groupInfo == null
                                      ? "0"
                                      : groupInfo.chatGroupId,
                                  groupMembers: selectedChatUsers,
                                  groupUniqueId: groupInfo == null
                                      ? "0"
                                      : groupInfo.groupUniqueId,
                                ));
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            );
          },
        ),
      );
    },
  );
}
