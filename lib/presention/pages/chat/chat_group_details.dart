import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/chat/chat.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

class ChatGroupInfo extends StatelessWidget {
  final User user;

  ChatGroupInfo({Key key, this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: user.groupType == "1"
                  ? "Announcement Details"
                  : "Group Details",
            ),
            BodyInner(
              page: Center(
                child: BlocProvider(
                  create: (context) =>
                      ChatGroupInfoBloc(chatRepository: _chatRepository)
                        ..add(ChatGroupInfoPressed(
                            groupId: user.chatGroupId,
                            groupUniqueId: user.groupUniqueId)),
                  child: BlocBuilder<ChatGroupInfoBloc, ChatGroupInfoState>(
                    builder: (context, state) {
                      if (state is ChatGroupInfoFailure) {
                        FailedToLoad(message: state.error);
                      }
                      if (state is ChatGroupInfoSuccess) {
                        return ChatGroupInfoBody(userinfo: state.stateData);
                      }
                      return LoaderSmallIndicator();
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: BlocProvider(
        create: (context) => ChatGroupInfoBloc(chatRepository: _chatRepository)
          ..add(ChatGroupInfoPressed(
              groupId: user.chatGroupId, groupUniqueId: user.groupUniqueId)),
        child: BlocBuilder<ChatGroupInfoBloc, ChatGroupInfoState>(
          builder: (context, state) {
            if (state is ChatGroupInfoFailure) {
              FailedToLoad(message: state.error);
            }
            if (state is ChatGroupInfoSuccess) {
              return FloatingButtonCustome(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChatUserRoomAdd(
                                groupType: user.groupType,
                                selectedUserIds:
                                    listOfUserIds(state.stateData.users),
                                groupInfo: state.stateData,
                              )));
                },
              );
            }
            return FloatingButtonCustome(
              icon: Icon(Icons.edit),
            );
          },
        ),
      ),
    );
  }
}

List<String> listOfUserIds(List<User> users) {
  List<String> selectedIds = [];

  users.map((e) => selectedIds.add(e.userId)).toList();

  return selectedIds;
}

class ChatGroupInfoBody extends StatelessWidget {
  final ChatGroupInfoModel userinfo;

  ChatGroupInfoBody({Key key, this.userinfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: tabBodyPadd,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(250),
                  ),
                  child: Container(
                    decoration: roundShadow,
                    height: 50,
                    width: 50,
                    child: ImageLoad(img: '/mobile_app_v2/o_profile_pic.png'),
                  ),
                ),
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    profileText("${userinfo.groupName}", 16),
                    customText("Created ${userinfo.createdDate}", 14, black),
                    customText(
                        "Admin :  ${userinfo.groupCreatedBy}", 10, black),
                  ],
                ),
              ),
              // SizedBox(width: 15),
              // Expanded(
              //   child: SingleChildScrollView(
              //     scrollDirection: Axis.horizontal,
              //     child: Row(
              //       children: oppoHistoryTabs.map((historyData) {
              //         return OppotActivityData(
              //           oppoDetailsTabs: historyData,
              //           data: data,
              //         );
              //       }).toList(),
              //     ),
              //   ),
              // )
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: textGray)),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 10),
          child: Text("No. of Users : ${userinfo.users.length}"),
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.all(10),
            children: userinfo.users
                .map((user) => ChatUserCard(userDetails: user))
                .toList(),
          ),
        ),
      ],
    );
  }
}
