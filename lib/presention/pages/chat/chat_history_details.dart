import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:file_picker/file_picker.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());
FirebaseRepository _firebaseRepository =
    FirebaseRepository(firebaseAPIClient: FirebaseAPIClient());

ChatHistoryBloc chatHistoryBloc;
MultiSelectDataBloc multiSelectDataBloc;

int isBlockUser = 0;
bool isBlockUserHidden = false;

class ChatHistoryDetails extends StatefulWidget {
  final User user;
  final AuthModel authData;
  const ChatHistoryDetails({this.authData, this.user, Key key})
      : super(key: key);

  @override
  _ChatHistoryDetailsState createState() => _ChatHistoryDetailsState();
}

class _ChatHistoryDetailsState extends State<ChatHistoryDetails> {
  Timer _clockTimer;

  @override
  void initState() {
    _clockTimer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      BlocProvider.of<RefreshUserHistoryBloc>(context)
        ..add(RefreshUserHistoryPressed(toUserId: widget.user.userId));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ChatHistoryBloc(chatRepository: _chatRepository)
            ..add(
              ChatHistoryPressed(toLoginId: widget.user.userId, offset: 0),
            ),
        ),
        BlocProvider(
          create: (context) => SendTextMessageBloc(
              chatRepository: _chatRepository,
              firebaseRepository: _firebaseRepository),
        ),
        BlocProvider(
            create: (context) => UploadUserChatBloc(
                chatRepository: _chatRepository,
                firebaseRepository: _firebaseRepository)),
        BlocProvider(
            create: (context) =>
                ChatBlockUserBloc(chatRepository: _chatRepository)),
        BlocProvider(create: (context) => MultiSelectDataBloc()),
      ],
      child: Scaffold(
        body: Container(
          color: bodyBG,
          child: Stack(
            children: [
              AppbarCurve(),
              BlocBuilder<MultiSelectDataBloc, MultiSelectDataState>(
                  builder: (context, state) {
                if (state is MSDchatTextLogIdSuccess) {
                  return ChatMultiSelectMessage(
                    user: widget.user,
                    chatTextLogId: state.chatTextLogId,
                    shareData: state.singleChatData,
                  );
                }
                return AppbarNormalChatHistory(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${widget.user.name} "),
                      Text(" ${widget.user.phone}",
                          style: TextStyle(fontSize: 11)),
                    ],
                  ),
                  actions: [
                    // IconButton(
                    //     icon: Icon(Icons.refresh),
                    //     onPressed: () {
                    //       chatHistoryBloc.add(ChatHistoryRefresh(
                    //           offset: 0, toLoginId: widget.user.userId));
                    //     }),
                    PopupMenuButton(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.more_vert),
                      ),
                      onSelected: (value) {
                        switch (value) {
                          case 'clear_history':
                            chatClearHistory(context, widget.user)
                                .then((value) {
                              if (value["status"] == "success") {
                                chatHistoryBloc.add(ChatHistoryRefresh(
                                    offset: 0, toLoginId: widget.user.userId));
                                successAlert(context, value["message"]);
                              } else {
                                errorAlert(context, value["message"]);
                              }
                            });
                            break;
                          case 'block':
                            chatBlocUser(context, widget.user, isBlockUser)
                                .then((value) {
                              if (value["status"] == "success") {
                                chatHistoryBloc.add(ChatHistoryRefresh(
                                    offset: 0, toLoginId: widget.user.userId));
                                successAlert(context, value["message"]);
                              } else {
                                errorAlert(context, value["message"]);
                              }
                            });
                            break;
                          default:
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ChatUserInfo(user: widget.user),
                              ),
                            );
                        }
                        // print(value);
                      },
                      itemBuilder: (context) {
                        return chatHistoryOptionData.map((e) {
                          return e.id == "block"
                              ? PopupMenuItem(
                                  value: e.id,
                                  enabled: isBlockUserHidden ? false : true,
                                  child: Row(
                                    children: [
                                      ImageLoad(img: e.img, width: 25),
                                      SizedBox(width: 15),
                                      isBlockUser == 0
                                          ? Text("Block")
                                          : Text("Un Block"),
                                    ],
                                  ),
                                )
                              : PopupMenuItem(
                                  value: e.id,
                                  child: Row(
                                    children: [
                                      ImageLoad(img: e.img, width: 25),
                                      SizedBox(width: 15),
                                      Text('${e.name}'),
                                    ],
                                  ),
                                );
                        }).toList();
                      },
                    ),
                  ],
                );
              }),
              BodyInner(
                page: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    BlocBuilder<ChatHistoryBloc, ChatHistoryState>(
                      builder: (context, state) {
                        if (state is ChatHistorySuccess) {
                          return BlocListener<RefreshUserHistoryBloc,
                              RefreshUserHistoryState>(
                            listener: (context, state) {
                              if (state is RefreshUserHistorySuccess) {
                                if (state.stateData.cnt != 0) {
                                  chatHistoryBloc.add(ChatHistoryRefresh(
                                      offset: 0,
                                      toLoginId: widget.user.userId));
                                }
                                setState(() {
                                  isBlockUser =
                                      state.stateData.blockedUserInfo.cnt;
                                  isBlockUserHidden = state.stateData
                                              .blockedUserInfo.data.length ==
                                          0
                                      ? false
                                      : state.stateData.blockedUserInfo
                                                  .data["created_by"] ==
                                              widget.user.userId
                                          ? true
                                          : false;
                                });
                              }
                            },
                            child: ChatBody(chatData: state, user: widget.user),
                          );
                        }
                        if (state is ChatHistoryFailure) {
                          return Expanded(
                            child: ListView(
                              children: [
                                FailedToLoad(message: state.error),
                              ],
                            ),
                          );
                        }
                        return Expanded(
                          child: ListView(
                            children: [
                              LoaderSmallIndicator(),
                            ],
                          ),
                        );
                      },
                    ),
                    widget.authData.isChatEnabled == "1"
                        ? BlocListener<MultiSelectDataBloc,
                            MultiSelectDataState>(listener: (context, state) {
                            if (state is MSDchatTextLogIdSuccess) {
                              // print(state.isReply);
                            }
                          }, child: BlocBuilder<MultiSelectDataBloc,
                            MultiSelectDataState>(builder: (context, state) {
                            if (state is MSDchatTextLogIdSuccess) {
                              print("   asdfasdf asdfasdf");
                              return Column(
                                children: [
                                  state.isReply
                                      ? ForwardDisplay(
                                          historyData: state.singleChatData,
                                        )
                                      : Container(),
                                  ChatFooter(
                                      user: widget.user,
                                      authData: widget.authData,
                                      singleChatData: state.singleChatData),
                                ],
                              );
                            }
                            return isBlockUser == 0
                                ? ChatFooter(
                                    user: widget.user,
                                    authData: widget.authData)
                                : AlertDangerBox(
                                    child: Text(
                                        "Note*: You can't send message untill, Un block the user."),
                                  );
                          }))
                        : AlertDangerBox(
                            child: Text(
                                "Note*: You don't have plan to send messages, please contact to support team"),
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _clockTimer.cancel();
    super.dispose();
  }
}

class ForwardDisplay extends StatelessWidget {
  final ChatHistoryModel historyData;
  const ForwardDisplay({Key key, this.historyData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // String fileName = historyData.referenceMsg != null
    //     ? historyData.referenceMsg.split('.').last
    //     : "unknown";

    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(6),
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        color: black.withOpacity(0.1),
        borderRadius: BorderRadius.circular(4),
      ),
      child: historyData.fileInfo.isEmpty
          ? Text(
              "${historyData.msg}",
            )
          : Wrap(
              spacing: 2,
              children: historyData.fileInfo
                  .map((ChatFileInfo file) => ChatDisplayFiles(
                      fileInfo: file,
                      referenceFileInfo: historyData.fileInfo,
                      width: 30))
                  .toList(),
            ),
    );
  }
}

class ChatBody extends StatefulWidget {
  final User user;

  final ChatHistorySuccess chatData;
  const ChatBody({Key key, this.chatData, this.user}) : super(key: key);

  @override
  _ChatBodyState createState() => _ChatBodyState();
}

class _ChatBodyState extends State<ChatBody> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  MultiSelectController mlController = MultiSelectController();

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    multiSelectDataBloc = BlocProvider.of<MultiSelectDataBloc>(context);
    chatHistoryBloc = BlocProvider.of<ChatHistoryBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.chatData.stateData.isEmpty ||
            widget.chatData.stateData == null
        ? Expanded(
            child: Center(
              child: ListView(
                shrinkWrap: true,
                children: [
                  NoDataFound(message: "No Data Found..."),
                ],
              ),
            ),
          )
        : Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(0),
              reverse: true,
              shrinkWrap: false,
              itemCount: widget.chatData.hasReachedMax
                  ? widget.chatData.stateData.length
                  : widget.chatData.stateData.length + 1,
              controller: _scrollController,
              itemBuilder: (context, index) {
                if (index >= widget.chatData.stateData.length) {
                  if (widget.chatData.currentState.length >=
                      Environment.dataDisplayCount) {
                    return LoaderSmallIndicator();
                  } else {
                    return Container();
                  }
                } else {
                  String presentDate = widget.chatData.stateData[index].date;
                  String futureDate =
                      index + 1 >= widget.chatData.stateData.length
                          ? "santosh"
                          : widget.chatData.stateData[index + 1].date;
                  return Column(
                    children: [
                      StickyHeader(
                        header: futureDate != presentDate
                            ? Container(
                                alignment: Alignment.center,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: bodyBG,
                                  ),
                                  margin: EdgeInsets.only(top: 5, bottom: 5),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 3),
                                  child: Text(
                                      chatHistoryDateFormate(widget
                                          .chatData.stateData[index].date),
                                      style: TextStyle(fontSize: 10)),
                                ),
                              )
                            : Container(),
                        content: BlocListener<MultiSelectDataBloc,
                            MultiSelectDataState>(
                          listener: (context, state) {
                            if (state is MultiSelectDataInitial) {
                              setState(() {
                                mlController.deselectAll();
                              });
                            }
                          },
                          child: MultiSelectItem(
                            isSelecting: mlController.isSelecting,
                            onSelected: () {
                              List selectedChatTextLogId = [];
                              ChatHistoryModel singleChatMsg;
                              setState(() {
                                mlController.toggle(index);
                                mlController.selectedIndexes.map((e) {
                                  selectedChatTextLogId.add(widget
                                      .chatData.stateData[e].chatTextLogId);
                                }).toList();

                                if (mlController.selectedIndexes.length == 1) {
                                  singleChatMsg = widget.chatData.stateData[
                                      mlController.selectedIndexes[0]];
                                }
                              });
                              selectedChatTextLogId.isEmpty
                                  ? multiSelectDataBloc
                                      .add(MSDchatTextLogIdClose())
                                  : mlController.selectedIndexes.length == 1
                                      ? multiSelectDataBloc
                                          .add(MSDchatTextLogIdPressed(
                                          isReply: false,
                                          chatTextLogId: selectedChatTextLogId,
                                          singleChatData: singleChatMsg,
                                        ))
                                      : multiSelectDataBloc
                                          .add(MSDchatTextLogIdPressed(
                                          isReply: false,
                                          chatTextLogId: selectedChatTextLogId,
                                          singleChatData: null,
                                        ));
                            },
                            child: Container(
                              color: mlController.isSelected(index)
                                  ? dartpurple.withOpacity(0.2)
                                  : transparent,
                              width: double.infinity,
                              margin: EdgeInsets.only(bottom: 2),
                              child: ChatBubbleBody(
                                  historyData:
                                      widget.chatData.stateData[index]),
                            ),
                          ),
                        ),
                      )
                    ],
                  );
                }
              },
            ),
          );
  }

  String chatHistoryDateFormate(String date) {
    DateTime dateType = DateTime.parse(date);
    String formattedDate = DateFormat('dd MMM, yyyy').format(dateType);

    return formattedDate;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      chatHistoryBloc.add(ChatHistoryPressed(toLoginId: widget.user.userId));
    }
  }
}

class ChatFooter extends StatefulWidget {
  final User user;
  final AuthModel authData;
  final ChatHistoryModel singleChatData;
  const ChatFooter({Key key, this.user, this.singleChatData, this.authData})
      : super(key: key);

  @override
  _ChatFooterState createState() => _ChatFooterState();
}

class _ChatFooterState extends State<ChatFooter> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  bool inputHidden = true;
  bool validationCheck = false;

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: _fbKey,
      autovalidateMode: AutovalidateMode.disabled,
      child: Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.only(bottom: Platform.isIOS ? 15 : 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Stack(
                children: [
                  FormBuilderTextField(
                    autovalidateMode: validationCheck
                        ? AutovalidateMode.onUserInteraction
                        : AutovalidateMode.disabled,
                    name: "message",
                    enabled: inputHidden,
                    autofocus: false,
                    decoration: chatTextInput("Type Message..."),
                    keyboardType: TextInputType.multiline,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(context),
                    ]),
                    maxLines: null,
                    onChanged: (value) {
                      setState(() {
                        validationCheck = true;
                      });
                    },
                  ),
                  Positioned(
                    right: 2,
                    top: 1,
                    child:
                        BlocListener<UploadUserChatBloc, UploadUserChatState>(
                      listener: (context, state) {
                        if (state is UploadUserChatFailure) {
                          return errorAlert(context, state.error);
                        }
                        if (state is UploadUserChatSuccess) {
                          chatHistoryBloc.add(ChatHistoryRefresh(
                              offset: 0, toLoginId: widget.user.userId));
                          multiSelectDataBloc.add(MSDchatTextLogIdClose());
                        }
                      },
                      child:
                          BlocBuilder<UploadUserChatBloc, UploadUserChatState>(
                        builder: (context, state) {
                          if (state is UploadUserChatInProgress) {
                            return LoaderSmallIndicator();
                          }
                          return RaisedChatGradientButton(
                            child: Icon(Icons.attach_file, color: purple),
                            width: 45,
                            onPressed: () => uploadFilterfiles(
                                context,
                                widget.user,
                                widget.singleChatData,
                                widget.authData),
                          );
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            BlocListener<SendTextMessageBloc, SendTextMessageState>(
              listener: (context, state) {
                if (state is SendTextMessageFailure) {
                  return errorAlert(context, state.error);
                }
                if (state is SendTextMessageSuccess) {
                  // fcmRefreshHistory(context, widget.user.userId);
                  chatHistoryBloc.add(ChatHistoryRefresh(
                      offset: 0, toLoginId: widget.user.userId));
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  setState(() {
                    _fbKey.currentState.reset();
                    inputHidden = true;
                    validationCheck = false;
                  });
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  multiSelectDataBloc.add(MSDchatTextLogIdClose());
                }
                if (state is SendTextMessageInProgress) {
                  setState(() {
                    inputHidden = false;
                  });
                }
              },
              child: BlocBuilder<SendTextMessageBloc, SendTextMessageState>(
                builder: (context, state) {
                  if (state is SendTextMessageInProgress) {
                    return Container(
                      margin: EdgeInsets.only(left: 5, top: 2),
                      child: LoaderSmallIndicator(),
                    );
                  }
                  return Container(
                      margin: EdgeInsets.only(left: 5, top: 2),
                      child: RaisedGradientButton(
                        child: Icon(Icons.arrow_forward, color: white),
                        gradient: LinearGradient(colors: purpleTtL),
                        width: 45,
                        onPressed: () {
                          if (_fbKey.currentState.saveAndValidate()) {
                            Map formData = _fbKey.currentState.value;
                            BlocProvider.of<SendTextMessageBloc>(context).add(
                              SendTextMessagePressed(
                                  formData: formData,
                                  user: widget.user,
                                  authData: widget.authData,
                                  msgReferenceId: widget.singleChatData == null
                                      ? "0"
                                      : widget.singleChatData.chatTextLogId),
                            );
                          }
                        },
                      )
                      // : RaisedGradientButton(
                      //     child: Icon(Icons.arrow_forward, color: black),
                      //     gradient: LinearGradient(colors: grayTtL),
                      //     width: 45,
                      //     onPressed: null,
                      //   ),
                      );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ChatMultiSelectMessage extends StatefulWidget {
  final User user;
  final ChatHistoryModel shareData;
  final List chatTextLogId;
  ChatMultiSelectMessage(
      {Key key, this.user, this.shareData, this.chatTextLogId})
      : super(key: key);

  @override
  _ChatMultiSelectMessageState createState() => _ChatMultiSelectMessageState();
}

class _ChatMultiSelectMessageState extends State<ChatMultiSelectMessage> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              DeleteUserMessagesBloc(chatRepository: _chatRepository),
        ),
      ],
      child: AppbarChatHistoryMultSelect(
        onPressed: () {
          BlocProvider.of<MultiSelectDataBloc>(context)
              .add(MSDchatTextLogIdClose());
        },
        child: Text("${widget.chatTextLogId.length}"),
        actions: [
          widget.chatTextLogId.length == 1
              ? IconButton(
                  color: white,
                  icon: Icon(Icons.share),
                  onPressed: () => shareContent(context, widget.shareData),
                )
              : Container(),
          widget.chatTextLogId.length == 1
              ? IconButton(
                  color: white,
                  icon: Icon(Icons.reply),
                  onPressed: () {
                    BlocProvider.of<MultiSelectDataBloc>(context)
                        .add(MSDchatTextLogIdPressed(
                      isReply: true,
                      chatTextLogId: widget.chatTextLogId,
                      singleChatData: widget.shareData,
                    ));
                  },
                )
              : Container(),
          IconButton(
            color: white,
            icon: Icon(Icons.send),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChatUserMessageForward(
                      chatTextLogId: widget.chatTextLogId),
                ),
              );
            },
          ),
          BlocListener<DeleteUserMessagesBloc, DeleteUserMessagesState>(
            listener: (context, state) {
              if (state is DeleteUserMessagesSuccess) {
                successAlert(context, state.stateData);
                chatHistoryBloc.add(ChatHistoryRefresh(
                    offset: 0, toLoginId: widget.user.userId));
                BlocProvider.of<MultiSelectDataBloc>(context)
                    .add(MSDchatTextLogIdClose());
              }
              if (state is DeleteUserMessagesFailure) {
                errorAlert(context, state.error);
              }
            },
            child: BlocBuilder<DeleteUserMessagesBloc, DeleteUserMessagesState>(
              builder: (context, state) {
                return IconButton(
                  color: white,
                  icon: state is DeleteUserMessagesInProgress
                      ? WhiteLoaderSmallIndicator()
                      : Icon(Icons.delete),
                  onPressed: () {
                    BlocProvider.of<DeleteUserMessagesBloc>(context)
                        .add(DeleteUserMessagesPressed(
                      chatTextLogId: widget.chatTextLogId,
                      toLoginId: widget.user.userId,
                    ));
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

List<File> files;

Future uploadFilterfiles(BuildContext context, User user,
    ChatHistoryModel singleMsgData, AuthModel authData) async {
  FilePickerResult result = await FilePicker.platform.pickFiles(
    allowMultiple: true,
    type: FileType.custom,
    allowedExtensions: [
      'doc',
      'docx',
      'pdf',
      'xls',
      'xlsx',
      'jpg',
      'jpeg',
      'png',
      'php',
      'txt',
      'sql',
      'java',
      'ts',
      'html',
      'mp4',
      'mp3',
      'mpeg',
      'wav',
      'css'
    ],
  );
  if (result != null) {
    // await uploadFiles(result.paths);
    // print(result.paths);
    BlocProvider.of<UploadUserChatBloc>(context).add(UploadUserChatPressed(
        files: result.paths,
        user: user,
        authData: authData,
        msgReferenceId:
            singleMsgData == null ? "0" : singleMsgData.chatTextLogId));
  } else {
    // User canceled the picker
  }
}

// fcmRefreshHistory(context, userId) {
//   Environment _environment = Environment();
//   FirebaseFirestore.instance
//       .collection('userChatHistory')
//       .snapshots()
//       .listen((querySnapshot) async {
//     final String userId = await _environment.getUserId();
//     querySnapshot.docChanges.forEach((change) {
//       if (userId == change.doc.get("user_id")) {
//         print('${change.doc.get("user_id")} changed user id');
//         chatHistoryBloc.add(ChatHistoryRefresh(
//             offset: 0, toLoginId: change.doc.get("user_id")));
//       }
//     });
//   });
// }
