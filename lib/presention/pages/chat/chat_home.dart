import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_user_options.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());
GenericRepository _genericRepository =
    GenericRepository(genericAPIClient: GenericAPIClient());

class ChatHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => AddLiveChatSessionCubit(
                  chatRepository: _chatRepository,
                  genericRepository: _genericRepository)
                ..addChatLiveSessionPressed(),
            ),
            BlocProvider(
              create: (context) =>
                  AnnounChatHistoryBloc(chatRepository: _chatRepository)
                    ..add(AnnounChatHistoryPressed(offset: 0)),
            ),
          ],
          child: Stack(
            children: [
              AppbarCurve(),
              AppbarNotLeading(
                title: "Chat",
                actions: [
                  BlocBuilder<AddLiveChatSessionCubit, AddLiveChatSessionState>(
                    builder: (context, state) {
                      if (state is AddLiveChatSessionSuccess) {
                        return IconButton(
                          icon: Icon(Icons.search),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ChatUserSearch(authData: state.authData),
                              ),
                            );
                          },
                        );
                      }
                      if (state is AddLiveChatSessionFailure) {
                        return IconButton(
                          icon: Icon(Icons.search),
                          onPressed: () {
                            errorAlert(context, "Not a valid request");
                          },
                        );
                      }
                      return Center(
                        child: LoaderSmallIndicator(),
                      );
                    },
                  ),
                  PopupMenuButton(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.more_vert),
                    ),
                    onSelected: (value) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => value == 'settings'
                              ? ChatUserSettings()
                              : value == 'new_group'
                                  ? ChatUserRoomAdd(groupType: "0")
                                  : ChatUserRoomAdd(groupType: "1"),
                        ),
                      );
                      print(value);
                    },
                    itemBuilder: (context) {
                      return chatUserOptionData.map((e) {
                        return PopupMenuItem(
                          value: e.id,
                          child: Row(
                            children: [
                              ImageLoad(img: e.img, width: 25),
                              SizedBox(width: 15),
                              Text(e.name),
                            ],
                          ),
                        );
                      }).toList();
                    },
                  ),
                  // chatUserOptionData
                ],
              ),
              BodyInner(
                page: RefreshIndicator(
                  onRefresh: () async {
                    chatUsersBloc.add(ChatUsersPressedRefresh(
                        searchTerm: "", isHierarchyUsers: "0"));
                  },
                  child: BlocListener<AddLiveChatSessionCubit,
                      AddLiveChatSessionState>(
                    listener: (context, state) {
                      if (state is AddLiveChatSessionFailure) {
                        return errorAlert(context, state.error);
                      }
                    },
                    child: BlocBuilder<AddLiveChatSessionCubit,
                        AddLiveChatSessionState>(
                      builder: (context, state) {
                        if (state is AddLiveChatSessionSuccess) {
                          return ChatUsers(authData: state.authData);
                        }
                        if (state is AddLiveChatSessionFailure) {
                          return FailedToLoad(message: state.error);
                        }
                        return Center(
                          child: LoaderSmallIndicator(),
                        );
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
