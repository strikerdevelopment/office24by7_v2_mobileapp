import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/widgets/loaders/loader_indicator.dart';
import 'package:share/share.dart';

FileDownloadRepository _fileDownloadRepository =
    FileDownloadRepository(fileDownloadAPIClient: FileDownloadAPIClient());

shareContent(BuildContext context, ChatHistoryModel shareData) async {
  showModalBottomSheet(
    context: context,
    builder: (builder) {
      return Container(
        margin: EdgeInsets.only(bottom: Platform.isIOS ? 15 : 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            shareData.fileInfo.isEmpty
                ? ListTile(
                    leading: Icon(Icons.share),
                    title: Text('Share Message'),
                    onTap: () async {
                      await Share.share(shareData.msg);
                      Navigator.pop(context);
                    },
                  )
                : ShareFiles(fileInfo: shareData.fileInfo),
          ],
        ),
      );
    },
  );
}

class ShareFiles extends StatelessWidget {
  final List<ChatFileInfo> fileInfo;
  const ShareFiles({Key key, this.fileInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: fileInfo
          .map(
            (ChatFileInfo file) => BlocProvider(
              create: (context) => ChatFileDownloadBloc(
                  fileDownloadRepository: _fileDownloadRepository)
                ..add(ChatFileDownloadExistPressed(file: file)),
              child: BlocBuilder<ChatFileDownloadBloc, ChatFileDownloadState>(
                builder: (context, state) {
                  if (state is ChatFileDownloadIsExist) {
                    return ListTile(
                      leading: Icon(Icons.file_present),
                      title: Text(" ${file.fileName}",
                          overflow: TextOverflow.ellipsis),
                      trailing: state.isExist
                          ? IconButton(
                              icon: Icon(Icons.share),
                              onPressed: () {
                                BlocProvider.of<ChatFileDownloadBloc>(context)
                                    .add(ChatFileDownloadSharedPressed(
                                        file: file));
                                Navigator.pop(context);
                              },
                            )
                          : IconButton(
                              icon: Icon(Icons.download_outlined),
                              onPressed: () {
                                BlocProvider.of<ChatFileDownloadBloc>(context)
                                    .add(ChatFileDownloadPressed(file: file));
                                BlocProvider.of<ChatFileDownloadBloc>(context)
                                    .add(ChatFileDownloadExistPressed(
                                        file: file));
                              },
                            ),
                    );
                  }
                  return ListTile(
                      leading: Icon(Icons.file_present),
                      title: Text(" ${file.fileName}",
                          overflow: TextOverflow.ellipsis),
                      trailing: LoaderInLineIndicator());
                },
              ),
            ),
          )
          .toList(),
    );
  }
}
