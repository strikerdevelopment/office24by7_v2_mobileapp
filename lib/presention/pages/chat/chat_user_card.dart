import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ChatUserCard extends StatelessWidget {
  final User userDetails;
  ChatUserCard({Key key, this.userDetails}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: transparent,
      elevation: 0,
      margin: EdgeInsets.only(bottom: 0),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            Stack(
              children: [
                Container(
                  width: 40,
                  height: 40,
                  margin: EdgeInsets.only(right: 10),
                  child: Center(
                    child: userDetails.group == null
                        ? Text(StringSplit(name: '${userDetails.name}')
                            .twoLetter())
                        : userDetails.group == 'announcement'
                            ? ImageLoad(
                                img:
                                    "/mobile_app_v2/o_announcement_contact.png",
                                width: 35)
                            : ImageLoad(
                                img: "/mobile_app_v2/o_group_contact.png",
                                width: 35),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: hexColor(userDetails.color),
                  ),
                ),
                RoundCircle(
                    color: userDetails.group == null
                        ? userNetword(userDetails.network)
                        : Colors.transparent,
                    bottom: 0,
                    right: 10),
              ],
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(userDetails.name),
                  Row(
                    children: [
                      Text(userDetails.phone, style: TextStyle(fontSize: 10)),
                      userDetails.description == '' ? Text("") : Text(" | "),
                      Expanded(
                        child: Text("${userDetails.description}",
                            style: TextStyle(fontSize: 10),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            userDetails.i == 0 || userDetails.i == '0'
                ? Container()
                : RoundCircleText(
                    color: purpleTtL,
                    width: 20,
                    height: 20,
                    child: Text("${userDetails.i}",
                        style: TextStyle(fontSize: 10, color: white)),
                  ),
          ],
        ),
      ),
    );
  }

  Color userNetword(type) {
    switch (type) {
      case "online":
        return Colors.green;
        break;
      case "offline":
        return Colors.grey;
        break;
      case "away":
        return Colors.yellow;
        break;
      default:
        return Colors.transparent;
    }
  }
}
