import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ChatUserCardSelect extends StatelessWidget {
  final bool isSelected;
  final User userDetails;
  ChatUserCardSelect({Key key, this.userDetails, this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.only(bottom: 0),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.only(right: 10),
              child: Center(
                child: isSelected
                    ? Icon(Icons.check_rounded, color: white)
                    : userDetails.group == null
                        ? Text(StringSplit(name: '${userDetails.name}')
                            .twoLetter())
                        : userDetails.group == 'announcement'
                            ? ImageLoad(
                                img:
                                    "/mobile_app_v2/o_announcement_contact.png",
                                width: 35)
                            : ImageLoad(
                                img: "/mobile_app_v2/o_group_contact.png",
                                width: 35),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: isSelected ? purple : hexColor(userDetails.color),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(userDetails.name),
                  Row(
                    children: [
                      Text(
                        userDetails.phone,
                        style: TextStyle(fontSize: 10),
                      ),
                      userDetails.description == '' ? Text("") : Text(" | "),
                      Expanded(
                        child: Text("${userDetails.description}",
                            style: TextStyle(fontSize: 10),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
