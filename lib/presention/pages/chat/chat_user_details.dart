import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

class ChatUserInfo extends StatelessWidget {
  final User user;

  ChatUserInfo({Key key, this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "User Details",
            ),
            BodyInner(
              page: Center(
                child: BlocProvider(
                  create: (context) =>
                      ChatUserInfoBloc(chatRepository: _chatRepository)
                        ..add(ChatUserInfoPressed(userId: user.userId)),
                  child: BlocBuilder<ChatUserInfoBloc, ChatUserInfoState>(
                    builder: (context, state) {
                      if (state is ChatUserInfoFailure) {
                        FailedToLoad(message: state.error);
                      }
                      if (state is ChatUserInfoSuccess) {
                        return ChatUserInfoBody(userinfo: state.stateData);
                      }
                      return LoaderSmallIndicator();
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ChatUserInfoBody extends StatelessWidget {
  final ChatUserInfoModel userinfo;

  ChatUserInfoBody({Key key, this.userinfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: tabBodyPadd,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(250),
                  ),
                  child: Container(
                    decoration: roundShadow,
                    height: 50,
                    width: 50,
                    child: ImageLoad(img: '/mobile_app_v2/o_profile_pic.png'),
                  ),
                ),
              ),
              Container(
                width: 220,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    profileText(
                        "${userinfo.firstName} ${userinfo.lastName}", 16),
                    customText(
                        userinfo.orgName == null
                            ? 'Software Developer'
                            : '${userinfo.orgName}',
                        14,
                        black),
                  ],
                ),
              ),
              // SizedBox(width: 15),
              // Expanded(
              //   child: SingleChildScrollView(
              //     scrollDirection: Axis.horizontal,
              //     child: Row(
              //       children: oppoHistoryTabs.map((historyData) {
              //         return OppotActivityData(
              //           oppoDetailsTabs: historyData,
              //           data: data,
              //         );
              //       }).toList(),
              //     ),
              //   ),
              // )
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: textGray)),
          ),
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.all(0),
            children: [
              ListTile(
                title: Text("Email"),
                subtitle: Text('${userinfo.emailId}'),
              ),
              ListTile(
                title: Text("Phone"),
                subtitle: Text('${userinfo.mobileNumber}'),
              ),
              ListTile(
                title: Text("Designation"),
                subtitle: Text('Software Developer'),
              ),
              ListTile(
                title: Text("Date of Joining"),
                subtitle: Text('${userinfo.registeredDate}'),
              ),
              ListTile(
                title: Text("Reporting To"),
                subtitle: Text('${userinfo.reportUser}'),
              ),
              ListTile(
                title: Text("Address"),
                subtitle: Text(userinfo.contactAddress == null
                    ? '--'
                    : '${userinfo.contactAddress}'),
              ),
              ListTile(
                title: Text("City"),
                subtitle: Text(
                    userinfo.cityName == null ? '--' : '${userinfo.cityName}'),
              ),
              ListTile(
                title: Text("Company"),
                subtitle: Text('Striker Soft Solutions'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
