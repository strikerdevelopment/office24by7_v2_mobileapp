import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/chat/chat.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:multi_select_item/multi_select_item.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

String _allUserKeyword = "";

class ChatUserMessageForward extends StatelessWidget {
  final List chatTextLogId;
  ChatUserMessageForward({Key key, @required this.chatTextLogId})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          //color: bodyBG,
          child: MultiBlocProvider(providers: [
        BlocProvider(
          create: (context) => ChatUsersBloc(chatRepository: _chatRepository)
            ..add(
              ChatUsersPressed(
                  searchTerm: _allUserKeyword, isHierarchyUsers: "0"),
            ),
        ),
        BlocProvider(
            create: (context) =>
                MessageForwardBloc(chatRepository: _chatRepository)),
      ], child: ChatAllSelectionBody(chatTextLogId: chatTextLogId))),
    );
  }
}

class ChatAllSelectionBody extends StatefulWidget {
  final List chatTextLogId;
  ChatAllSelectionBody({Key key, @required this.chatTextLogId})
      : super(key: key);

  @override
  _ChatAllSelectionBodyState createState() => _ChatAllSelectionBodyState();
}

List<String> selectedAllUsers = [];
Map selectedRecipients = {"users": [], "groups": []};

class _ChatAllSelectionBodyState extends State<ChatAllSelectionBody> {
  MultiSelectController controller = new MultiSelectController();

  @override
  void initState() {
    super.initState();
    selectedAllUsers = [];
    controller.deselectAll();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppbarCurve(),
        AppbarNormal(
          title: "Forwarded to",
        ),
        BodyInner(
            page: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, left: 10, right: 10),
              child: TextFormField(
                decoration: searchInput("Search"),
                keyboardType: TextInputType.text,
                onChanged: (value) {
                  setState(() {
                    _allUserKeyword = value;
                    controller.deselectAll();
                    selectedAllUsers = [];
                  });
                  BlocProvider.of<ChatUsersBloc>(context).add(ChatUsersPressed(
                      searchTerm: _allUserKeyword, isHierarchyUsers: "0"));
                },
              ),
            ),
            Expanded(
              child: BlocBuilder<ChatUsersBloc, ChatUsersState>(
                builder: (context, state) {
                  if (state is ChatUsersSuccess) {
                    return ListView.builder(
                      padding: EdgeInsets.all(10),
                      itemCount: state.stateData.users.length,
                      itemBuilder: (context, index) {
                        return MultiSelectItem(
                          isSelecting: controller.isSelecting,
                          onSelected: () {
                            setState(() {
                              controller.toggle(index);
                              selectedAllUsers = [];
                              selectedRecipients = {"users": [], "groups": []};
                              controller.selectedIndexes.map((e) {
                                selectedAllUsers
                                    .add(state.stateData.users[e].userId);

                                if (state.stateData.users[e].chatGroupId ==
                                    null) {
                                  selectedRecipients["users"]
                                      .add(state.stateData.users[e].userId);
                                } else {
                                  selectedRecipients["groups"].add(
                                      state.stateData.users[e].chatGroupId);
                                }
                              }).toList();
                            });
                          },
                          child: ChatUserCardSelect(
                            userDetails: state.stateData.users[index],
                            isSelected: controller.isSelected(index),
                          ),
                        );
                      },
                    );
                  }
                  if (state is ChatUsersFailure) {
                    return FailedToLoad(message: state.error);
                  }
                  return LoaderSmallIndicator();
                },
              ),
            ),
          ],
        )),
        selectedAllUsers.isEmpty
            ? Container()
            : Positioned(
                bottom: 0,
                left: -10,
                right: 10,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: purpleTtW,
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.8, 0.8),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                controller.deselectAll();
                                selectedAllUsers = [];
                              });
                            },
                            child: Icon(Icons.highlight_off, color: black)),
                      ),
                      Expanded(
                        child: Text(
                            "You have selected ${selectedAllUsers.length} users"),
                      ),
                      BlocListener<MessageForwardBloc, MessageForwardState>(
                        listener: (context, state) {
                          if (state is MessageForwardSuccess) {
                            successAlert(context, state.stateData);
                            Navigator.popUntil(
                                context, (route) => route.isFirst);
                            chatUsersBloc.add(ChatUsersPressedRefresh(
                                isHierarchyUsers: "0", searchTerm: ""));
                          }
                          if (state is MessageForwardFailure) {
                            errorAlert(context, state.error);
                          }
                        },
                        child: BlocBuilder<MessageForwardBloc,
                            MessageForwardState>(builder: (context, state) {
                          if (state is MessageForwardInProgress) {
                            return LoaderSmallIndicator();
                          }
                          return RoundCircleText(
                            width: 40,
                            height: 40,
                            color: purpleTtL,
                            child: IconButton(
                              color: white,
                              icon: Icon(Icons.arrow_right_alt),
                              onPressed: () {
                                BlocProvider.of<MessageForwardBloc>(context)
                                    .add(MessageForwardPressed(
                                        chatTextLogId: widget.chatTextLogId,
                                        recipients: selectedRecipients));
                              },
                            ),
                          );
                        }),
                      ),
                    ],
                  ),
                ),
              ),
      ],
    );
  }
}
