import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/presention/pages/chat/chat.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:multi_select_item/multi_select_item.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

String _userKeyword = "";

class ChatUserRoomAdd extends StatelessWidget {
  final String groupType;
  final List<String> selectedUserIds;
  final ChatGroupInfoModel groupInfo;
  ChatUserRoomAdd(
      {Key key, @required this.groupType, this.selectedUserIds, this.groupInfo})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        //color: bodyBG,
        child: BlocProvider(
          create: (context) => ChatUsersBloc(chatRepository: _chatRepository)
            ..add(ChatUsersPressed(
                searchTerm: _userKeyword,
                type: 'search',
                isHierarchyUsers: "0")),
          child: ChatUserSelectionBody(
            userdata: chatuserdataLoaded,
            groupType: groupType,
            selectedUserIds: selectedUserIds,
            groupInfo: groupInfo,
          ),
        ),
      ),
    );
  }
}

class ChatUserSelectionBody extends StatefulWidget {
  final String groupType;
  final ChatUsersModel userdata;
  final List<String> selectedUserIds;
  final ChatGroupInfoModel groupInfo;
  ChatUserSelectionBody(
      {Key key,
      @required this.userdata,
      this.groupType,
      this.selectedUserIds,
      this.groupInfo})
      : super(key: key);

  @override
  _ChatUserSelectionBodyState createState() => _ChatUserSelectionBodyState();
}

List<String> selectedChatUsers = [];

class _ChatUserSelectionBodyState extends State<ChatUserSelectionBody> {
  MultiSelectController controller = new MultiSelectController();

  @override
  void initState() {
    super.initState();
    selectedChatUsers = [];
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppbarCurve(),
        AppbarNormal(
          title: "Select Users",
        ),
        BodyInner(
            page: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, left: 10, right: 10),
              child: TextFormField(
                decoration: searchInput("Search"),
                keyboardType: TextInputType.text,
                onChanged: (value) {
                  setState(() {
                    _userKeyword = value;
                    controller.deselectAll();
                    selectedChatUsers = [];
                  });
                  BlocProvider.of<ChatUsersBloc>(context).add(ChatUsersPressed(
                      searchTerm: _userKeyword, isHierarchyUsers: "0"));
                },
              ),
            ),
            Expanded(
              child: BlocListener<ChatUsersBloc, ChatUsersState>(
                listener: (context, state) {
                  if (state is ChatUsersSuccess) {
                    setState(() {
                      selectedChatUsers = widget.selectedUserIds == null
                          ? []
                          : widget.selectedUserIds;
                      controller.selectedIndexes =
                          findIndexs(widget.selectedUserIds, state.stateData);
                    });
                  }
                },
                child: BlocBuilder<ChatUsersBloc, ChatUsersState>(
                  builder: (context, state) {
                    if (state is ChatUsersSuccess) {
                      return state.stateData.users.isEmpty
                          ? NoDataFound(message: "No Users Found...")
                          : ListView.builder(
                              padding: EdgeInsets.all(10),
                              itemCount: state.stateData.users.length,
                              itemBuilder: (context, index) {
                                return state.stateData.users[index].group ==
                                        null
                                    ? MultiSelectItem(
                                        isSelecting: controller.isSelecting,
                                        onSelected: () {
                                          setState(() {
                                            controller.toggle(index);
                                            selectedChatUsers = [];
                                            controller.selectedIndexes
                                                .map((e) =>
                                                    selectedChatUsers.add(state
                                                        .stateData
                                                        .users[e]
                                                        .userId))
                                                .toList();
                                          });
                                        },
                                        child: ChatUserRoomCardSelect(
                                          userDetails:
                                              state.stateData.users[index],
                                          isSelected:
                                              controller.isSelected(index),
                                        ),
                                      )
                                    : Container();
                              },
                            );
                    }
                    if (state is ChatUsersFailure) {
                      return FailedToLoad(message: state.error);
                    }
                    return LoaderSmallIndicator();
                  },
                ),
              ),
            ),
          ],
        )),
        selectedChatUsers.isEmpty
            ? Container()
            : Positioned(
                bottom: 0,
                left: -10,
                right: 10,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: purpleTtW,
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.8, 0.8),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                controller.deselectAll();
                                selectedChatUsers = [];
                              });
                            },
                            child: Icon(Icons.highlight_off, color: black)),
                      ),
                      Expanded(
                        child: Text(
                            "You have selected ${selectedChatUsers.length} users"),
                      ),
                      RoundCircleText(
                        width: 40,
                        height: 40,
                        color: purpleTtL,
                        child: IconButton(
                          color: white,
                          icon: Icon(Icons.arrow_right_alt),
                          onPressed: () {
                            chatCreateRoomPop(context, widget.groupType,
                                selectedChatUsers, widget.groupInfo);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  List<int> findIndexs(List<String> selecteddata, ChatUsersModel chatusers) {
    List<int> selectedIndex = [];
    selecteddata.forEach((user) {
      selectedIndex.add(
          chatusers.users.indexWhere((chatuser) => chatuser.userId == user));
    });
    return selecteddata == null ? [] : selectedIndex;
  }
}

class ChatUserRoomCardSelect extends StatelessWidget {
  final bool isSelected;
  final User userDetails;
  ChatUserRoomCardSelect({Key key, this.userDetails, this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.only(bottom: 0),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.only(right: 10),
              child: Center(
                child: isSelected
                    ? Icon(Icons.check_rounded, color: white)
                    : userDetails.group == null
                        ? Text(StringSplit(name: '${userDetails.name}')
                            .twoLetter())
                        : userDetails.group == 'announcement'
                            ? ImageLoad(
                                img:
                                    "/mobile_app_v2/o_announcement_contact.png",
                                width: 35)
                            : ImageLoad(
                                img: "/mobile_app_v2/o_group_contact.png",
                                width: 35),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: isSelected ? purple : hexColor(userDetails.color),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(userDetails.name),
                  Row(
                    children: [
                      Text(
                        userDetails.phone,
                        style: TextStyle(fontSize: 10),
                      ),
                      userDetails.description == '' ? Text("") : Text(" | "),
                      Expanded(
                        child: Text("${userDetails.description}",
                            style: TextStyle(fontSize: 10),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
