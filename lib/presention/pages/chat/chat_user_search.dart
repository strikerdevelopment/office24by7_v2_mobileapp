import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/login/auth_model.dart';
import 'package:office24by7_v2/presention/pages/chat/chat.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

String _userKeyword = "";

class ChatUserSearch extends StatelessWidget {
  final AuthModel authData;
  const ChatUserSearch({Key key, @required this.authData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: BlocProvider(
          create: (context) =>
              ChatUsersSearchBloc(chatRepository: _chatRepository)
                ..add(ChatUsersPressedSearch(
                    searchTerm: _userKeyword,
                    type: 'search',
                    isHierarchyUsers: "0")),
          child: ChatSearchBody(authData: authData),
        ),
      ),
    );
  }
}

class ChatSearchBody extends StatefulWidget {
  final AuthModel authData;
  ChatSearchBody({Key key, @required this.authData}) : super(key: key);

  @override
  _ChatSearchBodyState createState() => _ChatSearchBodyState();
}

class _ChatSearchBodyState extends State<ChatSearchBody> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppbarCurve(),
        AppbarNormal(
          title: "User Search",
        ),
        BodyInner(
            page: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 10, left: 10, right: 10),
              child: TextFormField(
                decoration: searchInput("Search"),
                keyboardType: TextInputType.text,
                onChanged: (value) {
                  setState(() {
                    _userKeyword = value;
                  });
                  BlocProvider.of<ChatUsersSearchBloc>(context).add(
                      ChatUsersPressedSearch(
                          type: "search",
                          searchTerm: _userKeyword,
                          isHierarchyUsers: "0"));
                },
              ),
            ),
            Expanded(
              child: BlocBuilder<ChatUsersSearchBloc, ChatUsersSearchState>(
                builder: (context, state) {
                  if (state is ChatUsersSearchSuccess) {
                    return ChatListSearchView(
                        chatuserdata: state.stateData,
                        authData: widget.authData);
                  }
                  if (state is ChatUsersSearchFailure) {
                    return FailedToLoad(message: state.error);
                  }
                  return LoaderSmallIndicator();
                },
              ),
            ),
          ],
        )),
      ],
    );
  }
}

class ChatListSearchView extends StatefulWidget {
  final AuthModel authData;
  final ChatUsersModel chatuserdata;
  ChatListSearchView({Key key, this.chatuserdata, this.authData})
      : super(key: key);

  @override
  _ChatListSearchViewState createState() => _ChatListSearchViewState();
}

class _ChatListSearchViewState extends State<ChatListSearchView> {
  @override
  void initState() {
    chatuserdataLoaded = widget.chatuserdata;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.chatuserdata.users.isEmpty
        ? Center(child: NoDataFound(message: "No Users Found..."))
        : ListView(
            padding: EdgeInsets.all(10),
            children: widget.chatuserdata.users
                .map(
                  (User user) => MaterialButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      user.groupType == null
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BlocProvider(
                                  create: (context) => RefreshUserHistoryBloc(
                                      chatRepository: _chatRepository),
                                  child: ChatHistoryDetails(
                                      authData: widget.authData, user: user),
                                ),
                              ),
                            )
                          : Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BlocProvider(
                                  create: (context) => RefreshGroupHistoryBloc(
                                      chatRepository: _chatRepository),
                                  child: AnnounChatHistoryDetails(
                                      authData: widget.authData, user: user),
                                ),
                              ),
                            );
                    },
                    child: ChatUserCard(userDetails: user),
                  ),
                )
                .toList(),
          );
  }
}
