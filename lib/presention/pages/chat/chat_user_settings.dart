import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

ChatUserInfoBloc _chatUserInfoBloc;
LiveUserStatusBloc _liveUserStatusBloc;

class ChatUserSettings extends StatelessWidget {
  ChatUserSettings({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                ChatUserInfoBloc(chatRepository: _chatRepository)
                  ..add(ChatUserInfoPressed()),
          ),
          BlocProvider(
            create: (context) =>
                LiveUserStatusBloc(chatRepository: _chatRepository),
          ),
        ],
        child: BlocListener<LiveUserStatusBloc, LiveUserStatusState>(
          listener: (context, state) {
            if (state is LiveUserStatusSuccess) {
              _chatUserInfoBloc.add(ChatUserInfoPressed());
            }
            if (state is LiveUserStatusFailure) {
              errorAlert(context, state.error);
            }
          },
          child: Container(
            color: bodyBG,
            child: Stack(
              children: [
                AppbarCurve(),
                AppbarNormal(
                  title: "User Details",
                  actions: [
                    PopupMenuButton(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.more_vert),
                      ),
                      onSelected: (value) {
                        _liveUserStatusBloc
                            .add(LiveUserStatusPressed(status: value));
                      },
                      itemBuilder: (context) {
                        return chatUserStatusData.map((e) {
                          return PopupMenuItem(
                            value: e.id,
                            child: Row(
                              children: [
                                Container(
                                  width: 12,
                                  height: 12,
                                  decoration: BoxDecoration(
                                    color: e.color,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                ),
                                SizedBox(width: 10),
                                Text('${e.name}'),
                              ],
                            ),
                          );
                        }).toList();
                      },
                    ),
                  ],
                ),
                BodyInner(
                  page: Center(
                    child: BlocBuilder<ChatUserInfoBloc, ChatUserInfoState>(
                      builder: (context, state) {
                        if (state is ChatUserInfoFailure) {
                          FailedToLoad(message: state.error);
                        }
                        if (state is ChatUserInfoSuccess) {
                          return ChatUserSettingInfoBody(
                              userinfo: state.stateData);
                        }
                        return LoaderSmallIndicator();
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ChatUserSettingInfoBody extends StatefulWidget {
  final ChatUserInfoModel userinfo;

  ChatUserSettingInfoBody({Key key, this.userinfo}) : super(key: key);

  @override
  _ChatUserSettingInfoBodyState createState() =>
      _ChatUserSettingInfoBodyState();
}

class _ChatUserSettingInfoBodyState extends State<ChatUserSettingInfoBody> {
  @override
  void initState() {
    _chatUserInfoBloc = BlocProvider.of<ChatUserInfoBloc>(context);
    _liveUserStatusBloc = BlocProvider.of<LiveUserStatusBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: tabBodyPadd,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(250),
                  ),
                  child: Container(
                    decoration: roundShadow,
                    height: 50,
                    width: 50,
                    child: ImageLoad(img: '/mobile_app_v2/o_profile_pic.png'),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  width: 120,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      profileText(
                          "${widget.userinfo.firstName} ${widget.userinfo.lastName}",
                          16),
                      customText(
                          widget.userinfo.orgName == null
                              ? 'Software Developer'
                              : '${widget.userinfo.orgName}',
                          14,
                          black),
                    ],
                  ),
                ),
              ),
              Container(
                child: Text('${widget.userinfo.status.name}'),
              )
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: textGray)),
          ),
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.all(0),
            children: [
              ListTile(
                title: Text("Email"),
                subtitle: Text("${widget.userinfo.emailId}"),
              ),
              ListTile(
                title: Text("Phone"),
                subtitle: Text("${widget.userinfo.mobileNumber}"),
              ),
              ListTile(
                title: Text("Designation"),
                subtitle: Text(widget.userinfo.desigName == null
                    ? 'Software Developer'
                    : '${widget.userinfo.desigName}'),
              ),
              ListTile(
                title: Text("Date of Joining"),
                subtitle: Text('${widget.userinfo.registeredDate}'),
              ),
              ListTile(
                title: Text("Reporting To"),
                subtitle: Text('${widget.userinfo.reportUser}'),
              ),
              ListTile(
                title: Text("Address"),
                subtitle: Text(widget.userinfo.contactAddress == null
                    ? '--'
                    : '${widget.userinfo.contactAddress}'),
              ),
              ListTile(
                title: Text("City"),
                subtitle: Text(widget.userinfo.cityName == null
                    ? '--'
                    : '${widget.userinfo.cityName}'),
              ),
              ListTile(
                title: Text("Company"),
                subtitle: Text('Striker Soft Solutions'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
