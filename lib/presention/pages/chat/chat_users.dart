import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/chat/chat.dart';
import 'package:office24by7_v2/presention/widgets/data_loaders/data_loaders.dart';
import 'package:office24by7_v2/presention/widgets/loaders/loader_indicator.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

ChatUsersBloc chatUsersBloc;

class ChatUsers extends StatefulWidget {
  final AuthModel authData;
  const ChatUsers({this.authData, Key key}) : super(key: key);

  @override
  _ChatUsersState createState() => _ChatUsersState();
}

class _ChatUsersState extends State<ChatUsers> {
  Timer _clockTimer;
  @override
  void initState() {
    _clockTimer = Timer.periodic(Duration(seconds: 5), (Timer t) {
      chatUsersBloc
          .add(ChatUsersPressedRefresh(searchTerm: "", isHierarchyUsers: "0"));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ChatUsersBloc(chatRepository: _chatRepository)
            ..add(
                ChatUsersPressedRefresh(searchTerm: "", isHierarchyUsers: "0")),
        ),
      ],
      child: BlocBuilder<ChatUsersBloc, ChatUsersState>(
        builder: (context, state) {
          if (state is ChatUsersSuccess) {
            return ChatListView(
                chatuserdata: state.stateData, authData: widget.authData);
          }
          if (state is ChatUsersFailure) {
            return FailedToLoad(message: state.error);
          }
          return LoaderSmallIndicator();
        },
      ),
    );
  }

  @override
  void dispose() {
    _clockTimer.cancel();
    super.dispose();
  }
}

ChatUsersModel chatuserdataLoaded;

class ChatListView extends StatefulWidget {
  final AuthModel authData;
  final ChatUsersModel chatuserdata;
  ChatListView({Key key, this.chatuserdata, this.authData}) : super(key: key);

  @override
  _ChatListViewState createState() => _ChatListViewState();
}

class _ChatListViewState extends State<ChatListView> {
  @override
  void initState() {
    chatUsersBloc = BlocProvider.of<ChatUsersBloc>(context);
    chatuserdataLoaded = widget.chatuserdata;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.chatuserdata.users.isEmpty
        ? Center(child: NoDataFound(message: "No Users Found..."))
        : ListView(
            padding: EdgeInsets.all(10),
            children: widget.chatuserdata.users
                .map(
                  (User user) => MaterialButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      user.groupType == null
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BlocProvider(
                                  create: (context) => RefreshUserHistoryBloc(
                                      chatRepository: _chatRepository),
                                  child: ChatHistoryDetails(
                                      authData: widget.authData, user: user),
                                ),
                              ),
                            )
                          : Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BlocProvider(
                                  create: (context) => RefreshGroupHistoryBloc(
                                      chatRepository: _chatRepository),
                                  child: AnnounChatHistoryDetails(
                                      authData: widget.authData, user: user),
                                ),
                              ),
                            );
                    },
                    child: ChatUserCard(userDetails: user),
                  ),
                )
                .toList(),
          );
  }
}
