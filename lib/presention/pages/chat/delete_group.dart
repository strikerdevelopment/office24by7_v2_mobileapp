import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/chat_model/chat_model.dart';
import 'package:office24by7_v2/presention/pages/chat/chat.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ChatRepository _chatRepository = ChatRepository(chatAPIClient: ChatAPIClient());

Future deleteGroup(BuildContext context, User user) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return BlocProvider(
            create: (context) =>
                DeleteGroupBloc(chatRepository: _chatRepository),
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_clear.png",
                  title: user.groupType == "1"
                      ? "Delete Announcement"
                      : "Delete Group"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    Column(
                      children: [
                        Text(
                            user.groupType == "1"
                                ? "Are you sure you want to Delete Announcement..."
                                : "Are you sure you want to Delete Group...",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.center),
                        fieldDistance,
                      ],
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<DeleteGroupBloc, DeleteGroupState>(
                listener: (context, state) {
                  if (state is DeleteGroupSuccess) {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    successAlert(context, state.stateData);
                    chatUsersBloc.add(ChatUsersPressedRefresh(
                        searchTerm: "", isHierarchyUsers: "0"));
                  }
                  if (state is DeleteGroupFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "message": state.error});
                  }
                },
                child: BlocBuilder<DeleteGroupBloc, DeleteGroupState>(
                  builder: (context, state) {
                    return state is DeleteGroupInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "No",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Yes",
                            secondFun: () async {
                              BlocProvider.of<DeleteGroupBloc>(context)
                                  .add(DeleteGroupPressed(
                                groupId: user.chatGroupId,
                                groupUniqueId: user.groupUniqueId,
                                groupType: user.groupType,
                              ));
                            });
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
