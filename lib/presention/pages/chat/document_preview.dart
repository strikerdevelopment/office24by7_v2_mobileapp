import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/chat_model/chat_history_model.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/widgets/loaders/image_load.dart';

class DocmentPreview extends StatelessWidget {
  final ChatFileInfo fileInfo;
  final List<ChatFileInfo> referenceFileInfo;
  const DocmentPreview(
      {Key key, @required this.fileInfo, this.referenceFileInfo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black.withOpacity(0.5),
      appBar: AppBar(
          // title: Text(fileInfo.fileName),
          // actions: [
          //   IconButton(
          //     icon: Icon(Icons.share),
          //     onPressed: () {},
          //   )
          // ],
          ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: referenceFileInfo == null
              ? [Container(child: Text("No preview available"))]
              : referenceFileInfo
                  .map(
                    (ChatFileInfo file) => Center(
                      child: Container(
                        child: getFileExtensionPreview(context, file, 300),
                      ),
                    ),
                  )
                  .toList(),
        ),
      ),
    );
  }
}

Widget getFileExtensionPreview(
    BuildContext context, ChatFileInfo fileinfo, double width) {
  var fileName =
      fileinfo.fileName != null ? fileinfo.fileName.split('.').last : "unknown";
  // print(fileName);
  switch (fileName) {
    case 'jpg':
      return ImageChatLoad(
        img: fileinfo.src,
        width: width,
      );
      break;
    case 'jpeg':
      return ImageChatLoad(
        img: fileinfo.src,
        width: width,
      );
      break;
    case 'png':
      return ImageChatLoad(
        img: fileinfo.src,
        width: width,
      );
      break;
    default:
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 100),
          Icon(
            Icons.file_present,
            color: white,
          ),
          Expanded(
              child: Text(
            fileinfo.fileName != null ? fileinfo.fileName : "unknown",
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: white),
          )),
        ],
      );
  }
}
