import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ContactAdd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Add Contact",
            ),
            BodyInner(
                page: BlocProvider(
              create: (context) => ContactDetailsForFormBloc(),
              child: AddContactForm(),
            )),
          ],
        ),
      ),
    );
  }
}

class AddContactForm extends StatefulWidget {
  AddContactForm({Key key}) : super(key: key);

  @override
  _AddContactFormState createState() => _AddContactFormState();
}

class _AddContactFormState extends State<AddContactForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  // final ValueChanged _onChanged = (val) => print(val);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: formpadding,
      children: [
        FormBuilder(
          key: _fbKey,
          autovalidateMode: AutovalidateMode.disabled,
          initialValue: {
            // 'date': DateTime.now(),
            // 'gender': 'Male',
          },
          skipDisabled: true,
          child: Column(children: [
            FormBuilderDropdown(
              name: "salutation",
              decoration: formGlobal("Salutation"),
              items: ['MR', 'MRS', 'MISS']
                  .map((gender) =>
                      DropdownMenuItem(value: gender, child: Text("$gender")))
                  .toList(),
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "first_name",
              decoration: formGlobal("First Name"),
              keyboardType: TextInputType.text,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
              ]),
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "last_name",
              decoration: formGlobal("Last Name"),
              keyboardType: TextInputType.text,
              // validators: [
              //   FormBuilderValidators.required(),
              // ],
            ),
            fieldDistance,
            BlocListener<CountryFlagBloc, CountryFlagState>(
                listener: (context, state) {
              if (state is CountryFlagFailure) {
                errorAlert(context, state.error);
              }
            }, child: BlocBuilder<CountryFlagBloc, CountryFlagState>(
              builder: (context, state) {
                if (state is CountryFlagFailure) {
                  return Container();
                }
                if (state is CountryFlagSuccess) {
                  return FormBuilderField(
                    name: "phone_number",
                    builder: (FormFieldState<dynamic> field) {
                      return IntePhoneNumber(
                          field: field, countries: state.stateData);
                    },
                  );
                }
                return LoaderSmallIndicator();
              },
            )),
            fieldDistance,
            FormBuilderTextField(
              name: "email_id",
              decoration: formGlobal("Email"),
              keyboardType: TextInputType.text,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.email(context),
              ]),
            ),
            fieldDistance,
            BlocListener<ContactSourceBloc, ContactSourceState>(
              listener: (context, state) {
                if (state is ContactSourceFailure) {
                  errorAlert(context,
                      "Getting Error in Sources, please contact to Support Team... ");
                }
              },
              child: BlocBuilder<ContactSourceBloc, ContactSourceState>(
                  builder: (context, state) {
                if (state is ContactSourceSuccess) {
                  final List<ContactSource> contactSourceData =
                      state.contactSourceData;

                  return FormBuilderDropdown(
                    name: "contact_source_id",
                    decoration: formGlobal("Select Source"),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: contactSourceData
                        .map((data) => DropdownMenuItem(
                            value: data.contactSourceId,
                            child: Text("${data.contactSourceDesc}")))
                        .toList(),
                  );
                } else {
                  return FormBuilderTextField(
                    name: "Loading",
                    decoration: formGlobal("Loading..."),
                    readOnly: true,
                  );
                }
              }),
            ),
            fieldDistance,
            BlocListener<ContactGroupBloc, ContactGroupState>(
              listener: (context, state) {
                if (state is ContactGroupFailure) {
                  errorAlert(context,
                      "Getting Error in Groups, please contact to Support Team... ");
                }
              },
              child: BlocBuilder<ContactGroupBloc, ContactGroupState>(
                builder: (context, state) {
                  if (state is ContactGroupSuccess) {
                    return FormBuilderDropdown(
                      name: "contact_group_id",
                      decoration: formGlobal("Select Group"),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                      items: state.contactGroupData
                          .map((data) => DropdownMenuItem(
                              value: data.contactGroupId,
                              child: Text("${data.contactGroupDesc}")))
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "contact_group_id",
                      decoration: formGlobal("Loading..."),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            BlocListener<CountryBloc, CountryState>(
              listener: (context, state) {
                if (state is CountryFailure) {
                  errorAlert(context,
                      "Getting Error in Country, please contact to Support Team... ");
                }
              },
              child: BlocBuilder<CountryBloc, CountryState>(
                builder: (context, state) {
                  if (state is CountrySuccess) {
                    List<Country> countryData = state.countryData;
                    Country country;
                    return FormBuilderDropdown(
                      name: "country",
                      onChanged: (value) {
                        BlocProvider.of<StateBloc>(context)
                            .add(StatePressed(country: country.id));
                      },
                      decoration: formGlobal("Select Country"),
                      items: countryData
                          .map(
                            (data) => DropdownMenuItem(
                              value: data.name,
                              onTap: () {
                                setState(() {
                                  country = data;
                                });
                              },
                              child: Text("${data.name}"),
                            ),
                          )
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "country",
                      decoration: formGlobal("Loading..."),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            BlocListener<StateBloc, StateState>(
              listener: (context, state) {
                if (state is StateFailure) {
                  errorAlert(context,
                      "Getting Error in State, please contact to Support Team... ");
                }
              },
              child: BlocBuilder<StateBloc, StateState>(
                builder: (context, state) {
                  if (state is StateSuccess) {
                    List<StateModel> statesData = state.statesData;
                    StateModel statedata;
                    return FormBuilderDropdown(
                      name: "state",
                      decoration: formGlobal("Select State"),
                      onChanged: (value) {
                        BlocProvider.of<CityBloc>(context)
                            .add(CityPressed(state: statedata.id));
                      },
                      items: statesData
                          .map((data) => DropdownMenuItem(
                              value: data.name,
                              onTap: () {
                                setState(() {
                                  statedata = data;
                                });
                              },
                              child: Text("${data.name}")))
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "Loading",
                      decoration: formGlobal("Select State..."),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            BlocListener<CityBloc, CityState>(
              listener: (context, state) {
                if (state is CityFailure) {
                  errorAlert(context,
                      "Getting Error in City, please contact to Support Team... ");
                }
              },
              child: BlocBuilder<CityBloc, CityState>(
                builder: (context, state) {
                  if (state is CitySuccess) {
                    List<City> cityData = state.cityData;
                    return FormBuilderDropdown(
                      name: "city",
                      decoration: formGlobal("Select City"),
                      items: cityData
                          .map((data) => DropdownMenuItem(
                              value: data.name, child: Text("${data.name}")))
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "Loading",
                      decoration: formGlobal("Select City"),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "location",
              decoration: formGlobal("Location"),
              keyboardType: TextInputType.text,
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "company_name",
              decoration: formGlobal("Company Name"),
              keyboardType: TextInputType.text,
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "job_title",
              decoration: formGlobal("Job Title"),
              keyboardType: TextInputType.text,
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "website_1",
              decoration: formGlobal("Website"),
              keyboardType: TextInputType.text,
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "comments",
              decoration: formGlobal("Comments"),
              //keyboardType: TextInputType.text,
            ),
            fieldDistance,
            SizedBox(height: 15),
            BlocProvider(
              create: (context) => ContactAddBloc(
                  contactRepository:
                      ContactRepository(contactAPIClient: ContactAPIClient())),
              child: SubmitButtons(fbKey: _fbKey),
            )
          ]),
        ),
      ],
    );
  }
}

class SubmitButtons extends StatelessWidget {
  const SubmitButtons({Key key, @required GlobalKey<FormBuilderState> fbKey})
      : _fbKey = fbKey,
        super(key: key);

  final GlobalKey<FormBuilderState> _fbKey;

  @override
  Widget build(BuildContext context) {
    return BlocListener<ContactAddBloc, ContactAddState>(
      listener: (context, state) {
        if (state is ContactAddFailure) {
          errorAlert(context, "Failed to Add Contact...");
        }
        if (state is ContactaddSuccess) {
          Navigator.pop(context, "success");
        }
      },
      child: BlocBuilder<ContactAddBloc, ContactAddState>(
        builder: (context, state) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RaisedGradientButton(
                child: customText('Reset', 16, black),
                gradient: LinearGradient(colors: grayTtL),
                width: 100,
                onPressed: () {
                  _fbKey.currentState.reset();
                },
              ),
              SizedBox(width: 10),
              state is ContactAddInProgress
                  ? LoaderIndicator()
                  : RaisedGradientButton(
                      child: buttonwhiteText('Submit'),
                      gradient: LinearGradient(colors: purpleTtL),
                      width: 100,
                      onPressed: () {
                        //Navigator.pushNamed(context, '/contacts');
                        if (_fbKey.currentState.saveAndValidate()) {
                          BlocProvider.of<ContactAddBloc>(context).add(
                            ContactAddPressed(
                              formData: _fbKey.currentState.value,
                            ),
                          );
                          print(_fbKey.currentState.value);
                        }
                      },
                    ),
            ],
          );
        },
      ),
    );
  }
}
