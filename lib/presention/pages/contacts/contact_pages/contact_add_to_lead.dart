import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future contactAddToLead(BuildContext context, ContactList contactInfo) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return ModalBox(
            modalHieght: 300,
            modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_option_add_to_lead.png",
                title: "Add To Lead"),
            modalBody: Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(children: [
                  FormBuilder(
                    key: _fbKey,
                    autovalidateMode: AutovalidateMode.always,
                    initialValue: {
                      'contact_id': contactInfo.contactId,
                      'contact_name': contactInfo.contact,
                    },
                    child: Column(
                      children: [
                        FormBuilderHiddenField(value: 'contact_id'),
                        FormBuilderHiddenField(value: 'contact_name'),
                        Text("Under Development",
                            style: TextStyle(fontSize: 20)),
                        // FormBuilderTextField(
                        //   name: "reminder_title",
                        //   decoration: formGlobal("Title"),
                        //   keyboardType: TextInputType.text,
                        //   validators: [
                        //     FormBuilderValidators.required(),
                        //   ],
                        // ),
                        fieldDistance,
                      ],
                    ),
                  ),
                ]),
              ),
            ),
            modalFooter: ModalFooter(
              firstButton: "Cancel",
              firstFun: () {
                Navigator.pop(context);
              },
              secondButton: "Submit",
              secondFun: () async {
                if (_fbKey.currentState.saveAndValidate()) {
                  Map formData = _fbKey.currentState.value;
                  Navigator.pop(context);
                  print(formData);
                }
                //Navigator.pop(context);
              },
            ),
          );
        },
      );
    },
  );
}
