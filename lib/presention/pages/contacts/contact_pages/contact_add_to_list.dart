import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future contactAddToList(BuildContext context, String contactId, String header) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => ContactListDropdownBloc(
                    contactRepository:
                        ContactRepository(contactAPIClient: ContactAPIClient()))
                  ..add(ContactListDropdownPressed()),
              ),
              BlocProvider(
                  create: (context) => ContactAddToListBloc(
                      contactRepository: ContactRepository(
                          contactAPIClient: ContactAPIClient()))),
            ],
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_add_to_list.png",
                  title: "Add to List"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        "filter_header": header,
                        'contact_id': contactId,
                        "origin": "",
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'filter_header'),
                          FormBuilderHiddenField(value: 'contact_id'),
                          FormBuilderHiddenField(value: 'origin'),
                          BlocBuilder<ContactListDropdownBloc,
                              ContactListDropdownState>(
                            builder: (context, state) {
                              if (state is ContactListDropdownSuccess) {
                                return FormBuilderDropdown(
                                  name: "list_id",
                                  decoration: formGlobal("Select List"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.contactListId,
                                          child: Text(e.listName)))
                                      .toList(),
                                );
                              }
                              if (state is ContactListDropdownFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }

                              return LoaderIndicator();
                            },
                          ),
                          Text(
                              "*Please select list name(s) to add this contact in to the list"),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<ContactAddToListBloc, ContactAddToListState>(
                listener: (context, state) {
                  if (state is ContactAddToListSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});
                  }
                  if (state is ContactAddToListFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<ContactAddToListBloc, ContactAddToListState>(
                  builder: (context, state) {
                    return state is ContactAddToListInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<ContactAddToListBloc>(context)
                                    .add(ContactAddToListPressed(
                                        eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
