import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/contacts/list_pages/list_home.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future contactCreateList(BuildContext context, String contactId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    CreateListBloc(contactRepository: _contactRepository),
              ),
              BlocProvider(
                create: (context) =>
                    CreateListBloc(contactRepository: _contactRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_option_add_to_list.png",
                title: "Create List",
              ),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.disabled,
                      initialValue: {
                        "filter_header": "contact",
                        'contact_id': contactId,
                        "origin": "",
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'filter_header'),
                          FormBuilderHiddenField(value: 'contact_id'),
                          FormBuilderHiddenField(value: 'origin'),
                          FormBuilderTextField(
                            name: "list_name",
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            decoration: formGlobal("List Name"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<CreateListBloc, CreateListState>(
                listener: (context, state) {
                  if (state is CreateListSuccess) {
                    if (state.stateData['status'] == 'duplicate') {
                      errorAlert(context, "List already exist");
                    } else {
                      successAlert(context, "List added successfully");
                    }
                    Navigator.pop(context, "success");
                    if (contactListListBloc != null)
                      contactListListBloc.add(ContactListListRefresh(
                          filterHeader: "list", filterOption: "all"));
                  }
                  if (state is CreateListFailure) {
                    errorAlert(context, state.error);
                    Navigator.pop(context, "failure");
                  }
                },
                child: BlocBuilder<CreateListBloc, CreateListState>(
                  builder: (context, state) {
                    return state is CreateListInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<CreateListBloc>(context).add(
                                    CreateListPressed(eventData: formData));

                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
