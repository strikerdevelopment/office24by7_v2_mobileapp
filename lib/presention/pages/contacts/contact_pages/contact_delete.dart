import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/contacts/list_pages/list_home.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future contactDelete(BuildContext context, String contactId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return BlocProvider(
            create: (context) =>
                ContactDeleteBloc(contactRepository: _contactRepository),
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_delete.png",
                  title: "Delete Contact"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'contact_id': contactId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'contact_id'),
                          Text(
                            "Are you sure you want to Delete Selected Contacts?",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<ContactDeleteBloc, ContactDeleteState>(
                listener: (context, state) {
                  if (state is ContactDeleteSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});

                    if (contactListListBloc != null)
                      contactListListBloc.add(ContactListListRefresh(
                          filterHeader: "list", filterOption: "all"));
                  }
                  if (state is ContactDeleteFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<ContactDeleteBloc, ContactDeleteState>(
                  builder: (context, state) {
                    return state is ContactDeleteInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<ContactDeleteBloc>(context).add(
                                    ContactDeletePressed(eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
