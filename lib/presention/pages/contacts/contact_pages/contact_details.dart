import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/contacts/contacts.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ContactDetails extends StatelessWidget {
  final ContactList contactList;
  ContactDetails({Key key, this.contactList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(title: "Contact Details"),
            BodyInner(
                page: BlocBuilder<ContactDetailsBloc, ContactDetailsState>(
                    // ignore: missing_return
                    builder: (context, state) {
              if (state is ContactDetailsInitial) {
                return LoaderIndicator();
              }
              if (state is ContactDetailsSuccess) {
                final contactInfo = state.contactDetails;
                return ContactDetailsContent(
                    contactInfo: contactInfo, contactList: contactList);
              }
              if (state is ContactsDetailsFailure) {
                return FailedToLoad(message: "Failed to Load");
              }
            })),
          ],
        ),
      ),
    );
  }
}

class ContactDetailsContent extends StatelessWidget {
  const ContactDetailsContent({
    Key key,
    @required this.contactInfo,
    @required this.contactList,
  }) : super(key: key);

  final ContactDetailsModel contactInfo;
  final ContactList contactList;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: tabBodyPadd,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(250),
                  ),
                  child: Container(
                    decoration: roundShadow,
                    height: 50,
                    width: 50,
                    child: ImageLoad(img: '/mobile_app_v2/o_profile_pic.png'),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  profileText(contactInfo.firstName, 16),
                  contactInfo.company == ''
                      ? Text('No Comapny Found')
                      : Text("${contactInfo.company}"),
                ],
              )
            ],
          ),
        ),
        Container(
          padding: tabBodyPadd,
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: textGray)),
          ),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: contactDetailsTabs.map((data) {
                return ContactDetailsTabsData(
                  contactDetailsTabs: data,
                  contactList: contactList,
                );
              }).toList(),
            ),
          ),
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.symmetric(vertical: 0),
            children: [
              ListTile(
                title: Text('First name'),
                subtitle: Text(contactInfo.firstName),
              ),
              ListTile(
                title: Text('Last name'),
                subtitle: Text(contactInfo.lastName),
              ),
              ListTile(
                title: Text('Company Name'),
                subtitle: Text(contactInfo.company),
              ),
              ListTile(
                title: Text('Phone Name'),
                subtitle: Text(contactInfo.phoneNumber),
              ),
              ListTile(
                title: Text('Email'),
                subtitle: Text(contactInfo.email),
              ),
              ListTile(
                title: Text('Service'),
                subtitle: Text(contactInfo.service),
              ),
              ListTile(
                title: Text('Source'),
                subtitle: Text(contactInfo.source),
              ),
              ListTile(
                title: Text('Group'),
                subtitle: Text(contactInfo.group),
              ),
              ListTile(
                title: Text('Created By'),
                subtitle: Text(contactInfo.createdBy),
              ),
            ],
          ),
        ),
        //Center(child: Text("ChatHome Page ${contactInfo.contactId}")),
      ],
    );
  }
}
