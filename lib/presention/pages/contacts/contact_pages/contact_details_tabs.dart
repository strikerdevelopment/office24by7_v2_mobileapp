import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/contacts/contacts.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ContactDetailsTabsData extends StatelessWidget {
  final ContactDetailsTabs contactDetailsTabs;

  final ContactList contactList;

  const ContactDetailsTabsData(
      {Key key, this.contactDetailsTabs, this.contactList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10, left: 10),
      child: FloatingTabButton(
        data: contactDetailsTabs,
        onPressed: () {
          switch (contactDetailsTabs.tabId) {
            case 'call':
              contactCall(context, contactList);

              break;
            case 'text':
              contactSMS(context, contactList.contactId).then((data) {
                if (data['status'] == "success") {
                  successAlert(context, "${data['data']}");
                } else if (data['status'] == "failure") {
                  errorAlert(context, "${data['data']}");
                }
              });
              break;
            case 'email':
              contactEmail(context, contactList.contactId).then((data) {
                if (data['status'] == "success") {
                  successAlert(context, "${data['data']}");
                } else if (data['status'] == "failure") {
                  errorAlert(context, "${data['data']}");
                }
              });
              break;
            case 'task':
              contactTask(context, contactList);
              break;
            case 'note':
              contactNote(context, contactList);
              break;
            case 'lead':
              contactAddToLead(context, contactList);
              break;
            case 'ticket':
              contactTicket(context, contactList);
              break;
            case 'history':
              ContactRepository contactRepository =
                  ContactRepository(contactAPIClient: ContactAPIClient());
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                        create: (context) => ContactDetailsBloc(
                            contactRepository: contactRepository)
                          ..add(ContactDetailsPressed(
                              contactId: contactList.contactId)),
                      ),
                      BlocProvider(
                        create: (context) => ContactHistoryBloc(
                            contactRepository: contactRepository)
                          ..add(
                            ContactHistoryPressed(
                                eventData: contactList.contactId),
                          ),
                      ),
                    ],
                    child: ContactHistory(contactList: contactList),
                  ),
                ),
              );
              break;
          }

          print(contactDetailsTabs.tabId);
        },
      ),
    );
  }
}
