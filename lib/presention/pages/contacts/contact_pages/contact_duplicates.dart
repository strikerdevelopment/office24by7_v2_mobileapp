import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

// ignore: unused_element
ContactsBloc _contactsBloc;

class ContactDuplicates extends StatelessWidget {
  final ContactList data;
  ContactDuplicates({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Duplicate Contacts",
            ),
            BodyInner(
              page: BlocProvider(
                create: (context) =>
                    ContactsBloc(contactRepository: _contactRepository)
                      ..add(ContactDuplicatePressed(cId: data.cId)),
                child: ContactDuplicateBody(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ContactDuplicateBody extends StatefulWidget {
  @override
  _ContactDuplicateBodyState createState() => _ContactDuplicateBodyState();
}

class _ContactDuplicateBodyState extends State<ContactDuplicateBody> {
  @override
  void initState() {
    _contactsBloc = BlocProvider.of<ContactsBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactsBloc, ContactsState>(
      builder: (context, state) {
        if (state is ContactDublicateFailure) {
          return FailedToLoad(message: state.error);
        }
        if (state is ContactDublicateSuccess) {
          return ListView(
            padding: EdgeInsets.all(10),
            children: state.stateData
                .map((e) => ContactDuplicateCard(data: e))
                .toList(),
          );
        }
        return LoaderIndicator();
      },
    );
  }
}

class ContactDuplicateCard extends StatelessWidget {
  final ContactList data;

  const ContactDuplicateCard({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: white,
      elevation: 0,
      child: Container(
        padding: cardPadd,
        child: Row(
          children: [
            Stack(
              children: [
                Container(
                  width: 40,
                  height: 40,
                  child: Center(
                    child: Text(
                      StringSplit(name: '${data.contact}').twoLetter(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: data.color.withOpacity(0.5),
                  ),
                ),
                data.duplicateCount == 0
                    ? Container()
                    : RoundCircle(color: dartpurple),
              ],
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${data.contact.capitalize()}",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    children: [
                      Text("${data.mobile}", style: TextStyle(fontSize: 12)),
                      Flexible(
                        child: data.companyName == "null"
                            ? Container()
                            : Text(" | ${data.companyName}",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 12)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // PopupMenuButton(
            //   onSelected: (value) {
            //     switch (value) {
            //       case "contactDetails":
            //         Navigator.push(
            //           context,
            //           MaterialPageRoute(
            //             builder: (context) => BlocProvider(
            //               create: (context) => ContactDetailsBloc(
            //                   contactRepository: contactRepository)
            //                 ..add(ContactDetailsPressed(
            //                     contactId: data.contactId)),
            //               child: ContactDetails(contactList: data),
            //             ),
            //           ),
            //         );
            //         break;
            //       case "contactHistory":
            //         ContactRepository contactRepository =
            //             ContactRepository(contactAPIClient: ContactAPIClient());
            //         Navigator.push(
            //           context,
            //           MaterialPageRoute(
            //             builder: (context) => MultiBlocProvider(
            //               providers: [
            //                 BlocProvider(
            //                   create: (context) => ContactDetailsBloc(
            //                       contactRepository: contactRepository)
            //                     ..add(ContactDetailsPressed(
            //                         contactId: data.contactId)),
            //                 ),
            //                 BlocProvider(
            //                   create: (context) => ContactHistoryBloc(
            //                       contactRepository: contactRepository)
            //                     ..add(
            //                       ContactHistoryPressed(
            //                           eventData: data.contactId),
            //                     ),
            //                 ),
            //               ],
            //               child: ContactHistory(contactList: data),
            //             ),
            //           ),
            //         );
            //         break;
            //       case "meeting":
            //         contactMeeting(context, data);
            //         break;
            //       case "task":
            //         contactTask(context, data);
            //         break;
            //       case "addToLeads":
            //         contactAddToLead(context, data);
            //         break;
            //       case "addToList":
            //         contactAddToList(context, data.contactId, "contact")
            //             .then((value) {
            //           if (value == "success") {
            //             successAlert(
            //                 context, "Contact Added to List Successfully");
            //           } else if (value == "failure") {
            //             errorAlert(context, "Failed to Add Contact to List");
            //           }
            //         });
            //         break;
            //       case "reassignContact":
            //         contactReassign(context, data.contactId).then((value) {
            //           if (value == "success") {
            //             successAlert(context, "Reassign Added Successfully");
            //             _contactsBloc.add(ContactRefresh(
            //                 filterHeader: "contact",
            //                 filterOption: contactDisplayAs));
            //           } else if (value == "failure") {
            //             errorAlert(context, "Failed to Add Reassign");
            //           }
            //         });
            //         break;
            //       case "deleteContact":
            //         contactDelete(context, data.contactId).then((value) {
            //           if (value["status"] == "success") {
            //             Navigator.pop(context);
            //             successAlert(context, value["data"]);
            //             _contactsBloc.add(ContactRefresh(
            //                 filterHeader: "contact",
            //                 filterOption: contactDisplayAs));
            //           } else if (value["status"] == "failure") {
            //             errorAlert(context, value["data"]);
            //           }
            //         });
            //         break;
            //       case "copyContact":
            //         copyContactCB(context);
            //         break;
            //       case "editContact":
            //         editContactCB(context);
            //         break;
            //       case "viewDuplicate":
            //         Navigator.push(
            //           context,
            //           MaterialPageRoute(
            //             builder: (context) => ContactDuplicates(data: data),
            //           ),
            //         );
            //         break;
            //       default:
            //     }
            //   },
            //   itemBuilder: (context) => cardContactOptions.map((e) {
            //     if (e.id == "editContact") {
            //       if (data.assignedTo != "0") {
            //         if (data.displayStatus != 0) {
            //           return PopupMenuItem(
            //             value: e.id,
            //             child: Row(
            //               children: [
            //                 ImageLoad(img: e.img, width: 25),
            //                 SizedBox(width: 15),
            //                 Text(e.title),
            //               ],
            //             ),
            //           );
            //         }
            //       }
            //     } else if (e.id == "copyContact") {
            //       if (data.assignedTo != "0") {
            //         if (data.displayStatus != 0) {
            //           return PopupMenuItem(
            //             value: e.id,
            //             child: Row(
            //               children: [
            //                 ImageLoad(img: e.img, width: 25),
            //                 SizedBox(width: 15),
            //                 Text(e.title),
            //               ],
            //             ),
            //           );
            //         }
            //       }
            //     } else if (e.id == "deleteContact") {
            //       if (data.assignedTo != "0") {
            //         if (data.displayStatus != 0) {
            //           return PopupMenuItem(
            //             value: e.id,
            //             child: Row(
            //               children: [
            //                 ImageLoad(img: e.img, width: 25),
            //                 SizedBox(width: 15),
            //                 Text(e.title),
            //               ],
            //             ),
            //           );
            //         }
            //       }
            //     } else if (e.id == "viewDuplicate") {
            //       if (data.duplicateCount != 0) {
            //         return PopupMenuItem(
            //           value: e.id,
            //           child: Row(
            //             children: [
            //               ImageLoad(img: e.img, width: 25),
            //               SizedBox(width: 15),
            //               Text(e.title),
            //             ],
            //           ),
            //         );
            //       }
            //     } else {
            //       return PopupMenuItem(
            //         value: e.id,
            //         child: Row(
            //           children: [
            //             ImageLoad(img: e.img, width: 25),
            //             SizedBox(width: 15),
            //             Text(e.title),
            //           ],
            //         ),
            //       );
            //     }
            //   }).toList(),
            //   icon: SizedBox(
            //     width: 20,
            //     height: 20,
            //     child: ImageLoad(
            //       img: '/mobile_app_v2/o_options_card.png',
            //       width: 20,
            //     ),
            //   ),
            //   offset: Offset(0, 20),
            // ),
          ],
        ),
      ),
    );
  }

  copyContactCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ContactformloadBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(
                  ContactFormLoadPressed(),
                ),
            ),
            BlocProvider(
              create: (context) => ContactSourceBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactSourcePressed()),
            ),
            BlocProvider(
              create: (context) => ContactGroupBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactGroupPressed()),
            ),
            BlocProvider(
              create: (context) => CountryBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              )..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) => StateBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CityBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => ContactGetMoreFieldsBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CountryFlagBloc(
                  globalRepository:
                      GlobalRepository(globalApiClient: GlobalApiClient()))
                ..add(CountryFlagPressed()),
            ),
          ],
          child: ContactCopy(data: data),
        ),
      ),
    );
    if (result == "success") {
      Navigator.pop(context);
      successAlert(context, "Contact Copied Successfully...");
    }
  }

  editContactCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ContactformloadBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(
                  ContactFormLoadPressed(),
                ),
            ),
            BlocProvider(
              create: (context) => ContactSourceBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactSourcePressed()),
            ),
            BlocProvider(
              create: (context) => ContactGroupBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactGroupPressed()),
            ),
            BlocProvider(
              create: (context) => CountryBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              )..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) => StateBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CityBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => ContactGetMoreFieldsBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CountryFlagBloc(
                  globalRepository:
                      GlobalRepository(globalApiClient: GlobalApiClient()))
                ..add(CountryFlagPressed()),
            ),
          ],
          child: ContactEdit(data: data),
        ),
      ),
    );

    if (result == "success") {
      Navigator.pop(context);
      successAlert(context, "Contact Edited Successfully...");
    }
  }
}
