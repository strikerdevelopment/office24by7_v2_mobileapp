import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_blocs/global/country_flag/country_flag_bloc.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

class ContactEdit extends StatelessWidget {
  final ContactList data;
  ContactEdit({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Edit Contact",
            ),
            BodyInner(
              page: BlocProvider(
                create: (context) => ContactDetailsForFormBloc(
                    contactRepository: _contactRepository)
                  ..add(
                      ContactDetailsForFormPressed(contactId: data.contactId)),
                child: BlocBuilder<ContactDetailsForFormBloc,
                    ContactDetailsForFormState>(
                  builder: (context, state) {
                    if (state is ContactDetailsForFormFailure) {
                      return FailedToLoad(message: "${state.error}");
                    }
                    if (state is ContactDetailsForFormSuccess) {
                      return EditContactForm(
                        data: state.stateData,
                      );
                    }
                    return LoaderIndicator();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class EditContactForm extends StatefulWidget {
  final Map data;
  EditContactForm({Key key, this.data}) : super(key: key);

  @override
  _EditContactFormState createState() => _EditContactFormState();
}

class _EditContactFormState extends State<EditContactForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  // final ValueChanged _onChanged = (val) => print(val);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: formpadding,
      children: [
        FormBuilder(
          key: _fbKey,
          autovalidateMode: AutovalidateMode.disabled,
          initialValue: {
            "contact_id": widget.data['contact_id'],
            "salutation": widget.data['salutation'] == ""
                ? null
                : widget.data['salutation'],
            "first_name": widget.data['first_name'],
            "last_name": widget.data['last_name'],
            "phone_number":
                '${widget.data['phone_code']} ${widget.data['without_msk_number']}',
            "email_id": widget.data['email_id'],
            "contact_source_id": widget.data['contact_source_id'],
            "source_name": widget.data['source'],
            "contact_group_id": widget.data['contact_group_id'],
            "country": widget.data['country'] == ""
                ? null
                : widget.data['country'] == "0"
                    ? null
                    : widget.data['country'],
            "state": widget.data['state'] == ""
                ? null
                : widget.data['state'] == "0"
                    ? null
                    : widget.data['state'],
            "city": widget.data['city'] == ""
                ? null
                : widget.data['city'] == "0"
                    ? null
                    : widget.data['city'],
            "location": widget.data['location'],
          },
          child: Column(children: [
            FormBuilderHiddenField(value: "contact_id"),
            FormBuilderHiddenField(value: "contact_source_id"),
            FormBuilderDropdown(
              name: "salutation",
              decoration: formGlobal("Salutation"),
              items: ['MR', 'MRS', 'MISS']
                  .map((gender) =>
                      DropdownMenuItem(value: gender, child: Text("$gender")))
                  .toList(),
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "first_name",
              decoration: formGlobal("First Name"),
              keyboardType: TextInputType.text,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: FormBuilderValidators.compose(
                  [FormBuilderValidators.required(context)]),
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "last_name",
              decoration: formGlobal("Last Name"),
              keyboardType: TextInputType.text,
            ),
            fieldDistance,
            BlocListener<CountryFlagBloc, CountryFlagState>(
                listener: (context, state) {
              if (state is CountryFlagFailure) {
                errorAlert(context, state.error);
              }
            }, child: BlocBuilder<CountryFlagBloc, CountryFlagState>(
              builder: (context, state) {
                if (state is CountryFlagFailure) {
                  return Container();
                }
                if (state is CountryFlagSuccess) {
                  return FormBuilderField(
                    name: "phone_number",
                    enabled: true,
                    builder: (FormFieldState<dynamic> field) {
                      return IntePhoneNumber(
                        field: field,
                        countries: state.stateData,
                        initialPhoneNumber: widget.data['without_msk_number'],
                      );
                    },
                  );
                }
                return LoaderSmallIndicator();
              },
            )),
            fieldDistance,
            FormBuilderTextField(
              name: "email_id",
              decoration: formGlobal("Email"),
              keyboardType: TextInputType.text,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.email(context),
              ]),
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "source_name",
              decoration: formGlobal("Source Name"),
              keyboardType: TextInputType.text,
              readOnly: true,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: FormBuilderValidators.compose(
                  [FormBuilderValidators.required(context)]),
            ),
            fieldDistance,
            BlocListener<ContactGroupBloc, ContactGroupState>(
              listener: (context, state) {
                if (state is ContactGroupFailure) {
                  errorAlert(context,
                      "Getting Error in Groups, please contact to Support Team... ");
                }
              },
              child: BlocBuilder<ContactGroupBloc, ContactGroupState>(
                builder: (context, state) {
                  if (state is ContactGroupSuccess) {
                    List<ContactGroup> contactGroupData =
                        state.contactGroupData;
                    return FormBuilderDropdown(
                      name: "contact_group_id",
                      initialValue: widget.data['contact_group_id'],
                      decoration: formGlobal("Select Group"),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                      items: contactGroupData
                          .map((data) => DropdownMenuItem(
                              value: data.contactGroupId,
                              child: Text("${data.contactGroupDesc}")))
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "contact_group_id",
                      decoration: formGlobal("Loading..."),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            BlocListener<CountryBloc, CountryState>(
              listener: (context, state) {
                if (state is CountryFailure) {
                  errorAlert(context,
                      "Getting Error in Country, please contact to Support Team... ");
                }
                if (state is CountrySuccess) {
                  if (widget.data['country_id'] != "0" &&
                      widget.data['country_id'] != "")
                    BlocProvider.of<StateBloc>(context)
                        .add(StatePressed(country: widget.data['country_id']));
                }
              },
              child: BlocBuilder<CountryBloc, CountryState>(
                builder: (context, state) {
                  if (state is CountrySuccess) {
                    List<Country> countryData = state.countryData;
                    Country country;
                    return FormBuilderDropdown(
                      name: "country",
                      onChanged: (value) {
                        BlocProvider.of<StateBloc>(context)
                            .add(StatePressed(country: country.id));
                      },
                      decoration: formGlobal("Select Country"),
                      items: countryData
                          .map(
                            (data) => DropdownMenuItem(
                              value: data.name,
                              onTap: () {
                                setState(() {
                                  country = data;
                                });
                              },
                              child: Text("${data.name}"),
                            ),
                          )
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "country",
                      decoration: formGlobal("Loading..."),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            BlocListener<StateBloc, StateState>(
              listener: (context, state) {
                if (state is StateFailure) {
                  errorAlert(context,
                      "Getting Error in State, please contact to Support Team... ");
                }

                if (state is StateSuccess) {
                  if (widget.data['state_id'] != "0" &&
                      widget.data['state_id'] != "")
                    BlocProvider.of<CityBloc>(context)
                        .add(CityPressed(state: widget.data['state_id']));
                }
              },
              child: BlocBuilder<StateBloc, StateState>(
                builder: (context, state) {
                  if (state is StateSuccess) {
                    List<StateModel> statesData = state.statesData;
                    StateModel statedata;
                    return FormBuilderDropdown(
                      name: "state",
                      decoration: formGlobal("Select State"),
                      onChanged: (value) {
                        BlocProvider.of<CityBloc>(context)
                            .add(CityPressed(state: statedata.id));
                      },
                      items: statesData
                          .map((data) => DropdownMenuItem(
                              value: data.name,
                              onTap: () {
                                setState(() {
                                  statedata = data;
                                });
                              },
                              child: Text("${data.name}")))
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "Loading",
                      decoration: formGlobal("Select State..."),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            BlocListener<CityBloc, CityState>(
              listener: (context, state) {
                if (state is CityFailure) {
                  errorAlert(context,
                      "Getting Error in City, please contact to Support Team... ");
                }
              },
              child: BlocBuilder<CityBloc, CityState>(
                builder: (context, state) {
                  if (state is CitySuccess) {
                    List<City> cityData = state.cityData;
                    return FormBuilderDropdown(
                      name: "city",
                      decoration: formGlobal("Select City"),
                      items: cityData
                          .map((data) => DropdownMenuItem(
                              value: data.name, child: Text("${data.name}")))
                          .toList(),
                    );
                  } else {
                    return FormBuilderTextField(
                      name: "Loading",
                      decoration: formGlobal("Select City"),
                      readOnly: true,
                    );
                  }
                },
              ),
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "location",
              decoration: formGlobal("Location"),
              keyboardType: TextInputType.text,
            ),
            fieldDistance,
            Column(
              children:
                  contactInnerDetails(widget.data['contact_data'], extraList),
            ),
            SizedBox(height: 15),
            BlocProvider(
              create: (context) => ContactEditBloc(
                  contactRepository:
                      ContactRepository(contactAPIClient: ContactAPIClient())),
              child: BlocListener<ContactEditBloc, ContactEditState>(
                listener: (context, state) {
                  if (state is ContactEditFailure) {
                    errorAlert(context, "Failed to Edit Contact...");
                  }
                  if (state is ContactEditSuccess) {
                    Navigator.pop(context, "success");
                  }
                },
                child: BlocBuilder<ContactEditBloc, ContactEditState>(
                  builder: (context, state) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        RaisedGradientButton(
                          child: customText('Back', 16, black),
                          gradient: LinearGradient(colors: grayTtL),
                          width: 100,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        SizedBox(width: 10),
                        state is ContactEditInProgress
                            ? LoaderIndicator()
                            : RaisedGradientButton(
                                child: buttonwhiteText('Submit'),
                                gradient: LinearGradient(colors: purpleTtL),
                                width: 100,
                                onPressed: () {
                                  if (_fbKey.currentState.saveAndValidate()) {
                                    Map formData = _fbKey.currentState.value;
                                    BlocProvider.of<ContactEditBloc>(context)
                                        .add(
                                      ContactEditPressed(
                                        formData: formData,
                                      ),
                                    );
                                    // print(_fbKey.currentState.value);
                                  }
                                },
                              ),
                      ],
                    );
                  },
                ),
              ),
            )
          ]),
        ),
      ],
    );
  }

  List<Widget> contactInnerDetails(List data, List<Map> extraList) {
    data.forEach((field) {
      extraList.forEach((extraField) {
        if (field['field_label'] == extraField['field_label']) {
          extraField['field_value'] = field['field_value'];
        }
      });
    });

    List<Widget> formBuilders = [];
    extraList.map((e) {
      switch (e['field_label']) {
        case 'job_title':
          formBuilders.add(Column(
            children: [
              FormBuilderTextField(
                initialValue: e['field_value'],
                name: "job_title",
                decoration: formGlobal("Job Title"),
                keyboardType: TextInputType.text,
              ),
              fieldDistance,
            ],
          ));
          break;
        case 'company_name':
          formBuilders.add(Column(
            children: [
              FormBuilderTextField(
                name: "company_name",
                initialValue: e['field_value'],
                decoration: formGlobal("Company Name"),
                keyboardType: TextInputType.text,
              ),
              fieldDistance,
            ],
          ));
          break;
        case 'website_1':
          formBuilders.add(Column(
            children: [
              FormBuilderTextField(
                name: "website_1",
                initialValue: e['field_value'],
                decoration: formGlobal("Website"),
                keyboardType: TextInputType.text,
              ),
              fieldDistance,
            ],
          ));
          break;
        case 'comments':
          formBuilders.add(Column(
            children: [
              FormBuilderTextField(
                name: "comments",
                initialValue: e['field_value'],
                decoration: formGlobal("Comments"),
                keyboardType: TextInputType.text,
              ),
              fieldDistance,
            ],
          ));
          break;
        default:
      }
    }).toList();
    return formBuilders;
  }
}

List<Map> extraList = [
  {"field_label": "job_title", "field_value": ""},
  {"field_label": "company_name", "field_value": ""},
  {"field_label": "website_1", "field_value": ""},
  {"field_label": "comments", "field_value": ""},
];
