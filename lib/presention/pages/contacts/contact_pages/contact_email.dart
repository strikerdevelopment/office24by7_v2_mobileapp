import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future contactEmail(BuildContext context, String contactId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    ContactFromEmailBloc(contactRepository: _contactRepository)
                      ..add(ContactFromEmailPressed()),
              ),
              BlocProvider(
                  create: (context) => ContactSendEmailBloc(
                      contactRepository: _contactRepository)),
            ],
            child: ModalBox(
              modalHieght: 700,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_email_modal.png", title: "Email"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'contact_ID': contactId,
                        "shorturl_Text": "",
                        "long_URL": "",
                        "contact_campaign_type": "contact",
                        "contacts_count": "",
                        "campaign_Form_Type": "1",
                        "campaign_Type": "0",
                        "frompage": "contact",
                        "frompageid": "",
                        "attachments": ""
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'contact_ID'),
                          FormBuilderHiddenField(value: 'shorturl_Text'),
                          FormBuilderHiddenField(value: 'long_URL'),
                          FormBuilderHiddenField(
                              value: 'contact_campaign_type'),
                          FormBuilderHiddenField(value: 'campaign_Form_Type'),
                          FormBuilderHiddenField(value: 'campaign_Type'),
                          FormBuilderHiddenField(value: 'frompage'),
                          FormBuilderHiddenField(value: 'frompageid'),
                          FormBuilderHiddenField(value: 'attachments'),
                          FormBuilderTextField(
                            name: "campaign_name",
                            decoration: formGlobal("Campaign Name"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "subject",
                            decoration: formGlobal("Subject"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          BlocBuilder<ContactFromEmailBloc,
                              ContactFromEmailState>(
                            builder: (context, state) {
                              if (state is ContactFromEmailSuccess) {
                                return FormBuilderDropdown(
                                  name: "from_Email",
                                  decoration: formGlobal("From Email"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.fromMail,
                                          child: Text("${e.fromMail}")))
                                      .toList(),
                                );
                              }
                              if (state is ContactFromEmailFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderSmallIndicator();
                            },
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "from_Name",
                            decoration: formGlobal("From Name"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "cc_Email",
                            decoration: formGlobal("Cc"),
                            keyboardType: TextInputType.text,
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "bcc_Email",
                            decoration: formGlobal("Bcc"),
                            keyboardType: TextInputType.text,
                          ),
                          fieldDistance,
                          FormBuilderField(
                            name: "message",
                            builder: (FormFieldState<dynamic> field) {
                              return SummerNoteTextEditor(field: field);
                            },
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<ContactSendEmailBloc, ContactSendEmailState>(
                listener: (context, state) {
                  if (state is ContactSendEmailSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});
                  }
                  if (state is ContactSendEmailFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<ContactSendEmailBloc, ContactSendEmailState>(
                  builder: (context, state) {
                    return state is ContactSendEmailInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<ContactSendEmailBloc>(context)
                                    .add(ContactSendEmailPressed(
                                        eventData: formData));
                                print(formData);
                              }
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
