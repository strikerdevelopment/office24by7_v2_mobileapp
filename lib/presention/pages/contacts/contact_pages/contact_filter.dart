import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future contactFilter(BuildContext context, String contactDisplayAs) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);
      return ModalBox(
        modalHieght: 400,
        modalHeader: ModalHeader(
            img: "/mobile_app_v2/o_option_filter.png", title: "Filter"),
        modalBody: Expanded(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: ListView(children: [
              FormBuilder(
                key: _fbKey,
                autovalidateMode: AutovalidateMode.always,
                initialValue: {},
                child: Column(
                  children: [
                    FormBuilderRadioGroup<String>(
                      name: "display_as",
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                      initialValue: contactDisplayAs,
                      activeColor: purple,
                      options: contactFilterData
                          .map((data) => FormBuilderFieldOption(
                                value: data.type,
                                // ignore: deprecated_member_use
                                child: Text(data.name),
                              ))
                          .toList(growable: false),
                    ),
                    fieldDistance,
                  ],
                ),
              ),
            ]),
          ),
        ),
        modalFooter: ModalFooter(
          firstButton: "Cancel",
          firstFun: () {
            Navigator.pop(context);
          },
          secondButton: "Submit",
          secondFun: () {
            if (_fbKey.currentState.saveAndValidate()) {
              Map formData = _fbKey.currentState.value;
              Navigator.pop(context, formData);
              // print(formData);
            }
          },
        ),
      );
    },
  );
}

List<TypeName> contactFilterData = [
  TypeName(type: "all", name: "All"),
  TypeName(type: "my_contacts", name: "My Contacts"),
  TypeName(type: "modified_by_me", name: "Modified By Me"),
  TypeName(type: "created_by_me", name: "Created By Me"),
];
