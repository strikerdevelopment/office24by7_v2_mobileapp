import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ContactGlobalOptions extends StatelessWidget {
  final int globalSelectedTab;
  const ContactGlobalOptions({Key key, this.globalSelectedTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (globalSelectedTab) {
      case 0:
        return PopupMenuButton(
          onSelected: (value) {
            print(value);
            switch (value) {
              case "deleteContact":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactDelete(context, gloablSelectedContacts)
                        .then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactsBloc>(context).add(
                              ContactRefresh(
                                  filterHeader: "contact",
                                  filterOption: contactDisplayAs));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              case "reassignContact":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactReassign(context, gloablSelectedContacts)
                        .then((value) {
                        if (value == "success") {
                          successAlert(context, "Reassign Added Successfully");
                          BlocProvider.of<ContactsBloc>(context).add(
                              ContactRefresh(
                                  filterHeader: "contact",
                                  filterOption: contactDisplayAs));
                        } else if (value == "failure") {
                          errorAlert(context, "Failed to Add Reassign");
                        }
                      });
                break;
              case "newList":
                contactCreateList(context, gloablSelectedContacts);
                break;
              case "addToList":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactAddToList(
                            context, gloablSelectedContacts, "contact")
                        .then((value) {
                        if (value['status'] == "success") {
                          successAlert(context, value['data']);
                        } else if (value['status'] == "failure") {
                          errorAlert(context, value['data']);
                        }
                      });
                break;
              case "sms":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactSMS(context, gloablSelectedContacts).then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactsBloc>(context).add(
                              ContactRefresh(
                                  filterHeader: "contact",
                                  filterOption: contactDisplayAs));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              case "email":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactEmail(context, gloablSelectedContacts)
                        .then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactsBloc>(context).add(
                              ContactRefresh(
                                  filterHeader: "contact",
                                  filterOption: contactDisplayAs));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              case "voice":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactVoice(context, gloablSelectedContacts)
                        .then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactsBloc>(context).add(
                              ContactRefresh(
                                  filterHeader: "contact",
                                  filterOption: contactDisplayAs));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              default:
            }
          },
          itemBuilder: (context) => cardContactGlobalOptions.map((e) {
            return PopupMenuItem(
              value: e.id,
              child: Row(
                children: [
                  ImageLoad(img: e.img, width: 25),
                  SizedBox(width: 15),
                  Text(e.title),
                ],
              ),
            );
          }).toList(),
          icon: SizedBox(
            width: 30,
            height: 30,
            child: ImageLoad(
              img: '/mobile_app_v2/o_moreoptions_appbar.png',
              width: 30,
            ),
          ),
          offset: Offset(0, 20),
        );
        break;
      default:
        return Container();
    }
  }
}
