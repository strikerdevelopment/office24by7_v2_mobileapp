import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_data/repositories/global_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

ContactGlobalSearchBloc _contactGlobalSearchBloc;

String contactKeyword;

class ContactGlobalSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: BlocProvider(
          create: (context) =>
              ContactGlobalSearchBloc(contactRepository: _contactRepository)
                ..add(ContactGlobalSearchFetched(
                    filterHeader: "contact", keyword: contactKeyword)),
          child: ContactsSearchData(),
        ),
      ),
    );
  }
}

class ContactsSearchData extends StatefulWidget {
  @override
  _ContactsSearchDataState createState() => _ContactsSearchDataState();
}

class _ContactsSearchDataState extends State<ContactsSearchData> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppbarCurve(),
        AppbarNormal(
          title: "Global Search",
          actions: [
            ContactGlobalSearchOptions(globalSelectedTab: globalSelectedTab),
          ],
        ),
        BodyInner(
            page: Container(
          padding: tabBodyPadd,
          child: Column(
            children: [
              TextFormField(
                decoration: searchInput("Search"),
                keyboardType: TextInputType.text,
                onChanged: (value) {
                  setState(() {
                    contactKeyword = value;
                  });

                  BlocProvider.of<ContactGlobalSearchBloc>(context).add(
                      ContactGlobalSearchRefresh(
                          filterHeader: "contact", keyword: contactKeyword));
                },
              ),
              SizedBox(height: 8),
              Expanded(
                child: ContactSearchBody(),
              ),
            ],
          ),
        )),
      ],
    );
  }
}

class ContactSearchBody extends StatefulWidget {
  ContactSearchBody({Key key}) : super(key: key);

  @override
  _ContactSearchBodyState createState() => _ContactSearchBodyState();
}

class _ContactSearchBodyState extends State<ContactSearchBody> {
  MultiSelectController mlController = new MultiSelectController();

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  List selectedItems = [];

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _contactGlobalSearchBloc =
        BlocProvider.of<ContactGlobalSearchBloc>(context);
    mlController.disableEditingWhenNoneSelected = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        BlocListener<ContactGlobalSearchBloc, ContactGlobalSearchState>(
          listener: (context, state) {
            if (state is ContactGlobalSearchInitial) {
              setState(() {
                mlController.deselectAll();
                selectedItems = [];
                gloablSelectedContacts = "";
              });
            }
          },
          child: BlocBuilder<ContactGlobalSearchBloc, ContactGlobalSearchState>(
            builder: (context, state) {
              if (state is ContactGlobalSearchFailure) {
                return FailedToLoad(message: state.error);
              }
              if (state is ContactGlobalSearchSuccess) {
                return state.contacts.isEmpty
                    ? NoDataFound(message: "No data found...")
                    : ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (context, index) {
                          bool last = state.contacts.length == (index + 1);
                          return index >= state.contacts.length
                              ? state.currentState.length >=
                                      Environment.dataDisplayCount
                                  ? LoaderSmallIndicator()
                                  : Container()
                              : MultiSelectItem(
                                  isSelecting: mlController.isSelecting,
                                  onSelected: () {
                                    setState(() {
                                      mlController.toggle(index);
                                      selectedItems =
                                          mlController.selectedIndexes;

                                      final List contactIdStrings = [];
                                      selectedItems
                                          .map((e) => contactIdStrings
                                              .add(state.contacts[e].contactId))
                                          .toList();
                                      gloablSelectedContacts =
                                          contactIdStrings.join(',');
                                    });
                                  },
                                  child: ContactGlobalSearchCard(
                                    data: state.contacts[index],
                                    isActive: mlController.isSelected(index),
                                    last: last,
                                  ),
                                );
                        },
                        itemCount: state.hasReachedMax
                            ? state.contacts.length
                            : state.contacts.length + 1,
                        controller: _scrollController,
                      );
              }
              return LoaderIndicator();
            },
          ),
        ),
        selectedItems.isEmpty
            ? Container()
            : Positioned(
                bottom: 0,
                left: -10,
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: purpleTtW,
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.8, 0.8),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                mlController.deselectAll();
                                gloablSelectedContacts = "";
                              });
                            },
                            child: Icon(Icons.close)),
                      ),
                      Text(
                          "You have selected ${selectedItems.length} contacts"),
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _contactGlobalSearchBloc.add(ContactGlobalSearchFetched(
          filterHeader: "contact", keyword: contactKeyword));
    }
  }
}

class ContactGlobalSearchCard extends StatelessWidget {
  final ContactList data;
  final bool isActive;
  final bool last;

  const ContactGlobalSearchCard({Key key, this.data, this.isActive, this.last})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: white,
      elevation: 0,
      margin: last ? EdgeInsets.only(bottom: 50) : EdgeInsets.only(bottom: 10),
      child: Container(
        padding: cardPadd,
        child: Row(
          children: [
            Stack(
              children: [
                Container(
                  width: 40,
                  height: 40,
                  child: isActive
                      ? Icon(Icons.done, color: white)
                      : Center(
                          child: data.assignedTo != "0"
                              ? Text(
                                  StringSplit(name: '${data.contact}')
                                      .twoLetter(),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : ImageLoad(
                                  img: "/mobile_app_v2/o_option_admin.png",
                                  width: 30,
                                ),
                        ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: isActive ? purple : data.color.withOpacity(0.5),
                  ),
                ),
                data.duplicateCount == 0
                    ? Container()
                    : RoundCircle(color: dartpurple),
              ],
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${data.contact.capitalize()}",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Text("${data.companyName} | ${data.mobile}",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 12)),
                ],
              ),
            ),
            PopupMenuButton(
              onSelected: (value) {
                switch (value) {
                  case "contactDetails":
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BlocProvider(
                          create: (context) => ContactDetailsBloc(
                              contactRepository: contactRepository)
                            ..add(ContactDetailsPressed(
                                contactId: data.contactId)),
                          child: ContactDetails(contactList: data),
                        ),
                      ),
                    );
                    break;
                  case "contactHistory":
                    ContactRepository contactRepository =
                        ContactRepository(contactAPIClient: ContactAPIClient());
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => ContactDetailsBloc(
                                  contactRepository: contactRepository)
                                ..add(ContactDetailsPressed(
                                    contactId: data.contactId)),
                            ),
                            BlocProvider(
                              create: (context) => ContactHistoryBloc(
                                  contactRepository: contactRepository)
                                ..add(
                                  ContactHistoryPressed(
                                      eventData: data.contactId),
                                ),
                            ),
                          ],
                          child: ContactHistory(contactList: data),
                        ),
                      ),
                    );
                    break;
                  case "meeting":
                    contactMeeting(context, data);
                    break;
                  case "task":
                    contactTask(context, data);
                    break;
                  case "addToLeads":
                    contactAddToLead(context, data);
                    break;
                  case "addToList":
                    contactAddToList(context, data.contactId, "contact")
                        .then((value) {
                      if (value['status'] == "success") {
                        successAlert(context, value['data']);
                      } else if (value['status'] == "failure") {
                        errorAlert(context, value['data']);
                      }
                    });
                    break;
                  case "reassignContact":
                    contactReassign(context, data.contactId).then((value) {
                      if (value == "success") {
                        successAlert(context, "Reassign Added Successfully");
                        _contactGlobalSearchBloc.add(ContactGlobalSearchRefresh(
                            filterHeader: "contact", keyword: contactKeyword));
                      } else if (value == "failure") {
                        errorAlert(context, "Failed to Add Reassign");
                      }
                    });
                    break;
                  case "deleteContact":
                    contactDelete(context, data.contactId).then((value) {
                      if (value["status"] == "success") {
                        successAlert(context, value["data"]);
                        _contactGlobalSearchBloc.add(ContactGlobalSearchRefresh(
                            filterHeader: "contact", keyword: contactKeyword));
                      } else if (value["status"] == "failure") {
                        errorAlert(context, value["data"]);
                      }
                    });
                    break;
                  case "copyContact":
                    copyContactCB(context);
                    break;
                  case "editContact":
                    editContactCB(context);
                    break;
                  case "viewDuplicate":
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ContactDuplicates(data: data),
                      ),
                    );
                    break;
                  default:
                }
              },
              itemBuilder: (context) => cardContactOptions.map((e) {
                if (e.id == "editContact") {
                  if (data.assignedTo != "0") {
                    return PopupMenuItem(
                      value: e.id,
                      child: Row(
                        children: [
                          ImageLoad(img: e.img, width: 25),
                          SizedBox(width: 15),
                          Text(e.title),
                        ],
                      ),
                    );
                  }
                } else if (e.id == "deleteContact") {
                  if (data.assignedTo != "0") {
                    return PopupMenuItem(
                      value: e.id,
                      child: Row(
                        children: [
                          ImageLoad(img: e.img, width: 25),
                          SizedBox(width: 15),
                          Text(e.title),
                        ],
                      ),
                    );
                  }
                } else if (e.id == "viewDuplicate") {
                  if (data.duplicateCount != 0) {
                    return PopupMenuItem(
                      value: e.id,
                      child: Row(
                        children: [
                          ImageLoad(img: e.img, width: 25),
                          SizedBox(width: 15),
                          Text(e.title),
                        ],
                      ),
                    );
                  }
                } else {
                  return PopupMenuItem(
                    value: e.id,
                    child: Row(
                      children: [
                        ImageLoad(img: e.img, width: 25),
                        SizedBox(width: 15),
                        Text(e.title),
                      ],
                    ),
                  );
                }
              }).toList(),
              icon: SizedBox(
                width: 20,
                height: 20,
                child: ImageLoad(
                  img: '/mobile_app_v2/o_options_card.png',
                  width: 20,
                ),
              ),
              offset: Offset(0, 20),
            ),
          ],
        ),
      ),
    );
  }

  copyContactCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ContactformloadBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(
                  ContactFormLoadPressed(),
                ),
            ),
            BlocProvider(
              create: (context) => ContactSourceBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactSourcePressed()),
            ),
            BlocProvider(
              create: (context) => ContactGroupBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactGroupPressed()),
            ),
            BlocProvider(
              create: (context) => CountryBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              )..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) => StateBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CityBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => ContactGetMoreFieldsBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              ),
            ),
          ],
          child: ContactCopy(data: data),
        ),
      ),
    );
    if (result == "success") {
      _contactGlobalSearchBloc.add(ContactGlobalSearchRefresh(
          filterHeader: "contact", keyword: contactKeyword));
      successAlert(context, "Contact Copied Successfully...");
    }
  }

  editContactCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ContactformloadBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(
                  ContactFormLoadPressed(),
                ),
            ),
            BlocProvider(
              create: (context) => ContactSourceBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactSourcePressed()),
            ),
            BlocProvider(
              create: (context) => ContactGroupBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactGroupPressed()),
            ),
            BlocProvider(
              create: (context) => CountryBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              )..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) => StateBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CityBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => ContactGetMoreFieldsBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              ),
            ),
          ],
          child: ContactEdit(data: data),
        ),
      ),
    );

    if (result == "success") {
      _contactGlobalSearchBloc.add(ContactGlobalSearchRefresh(
          filterHeader: "contact", keyword: contactKeyword));
      successAlert(context, "Contact Edited Successfully...");
    }
  }
}

class ContactGlobalSearchOptions extends StatelessWidget {
  final int globalSelectedTab;
  const ContactGlobalSearchOptions({Key key, this.globalSelectedTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (globalSelectedTab) {
      case 0:
        return PopupMenuButton(
          onSelected: (value) {
            print(value);
            switch (value) {
              case "deleteContact":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactDelete(context, gloablSelectedContacts)
                        .then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactGlobalSearchBloc>(context).add(
                              ContactGlobalSearchRefresh(
                                  filterHeader: "contact",
                                  keyword: contactKeyword));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              case "reassignContact":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactReassign(context, gloablSelectedContacts)
                        .then((value) {
                        if (value == "success") {
                          successAlert(context, "Reassign Added Successfully");
                          BlocProvider.of<ContactGlobalSearchBloc>(context).add(
                              ContactGlobalSearchRefresh(
                                  filterHeader: "contact",
                                  keyword: contactKeyword));
                        } else if (value == "failure") {
                          errorAlert(context, "Failed to Add Reassign");
                        }
                      });
                break;
              case "newList":
                contactCreateList(context, gloablSelectedContacts);
                break;
              case "addToList":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactAddToList(
                            context, gloablSelectedContacts, "contact")
                        .then((value) {
                        if (value['status'] == "success") {
                          successAlert(context, value['data']);
                        } else if (value['status'] == "failure") {
                          errorAlert(context, value['data']);
                        }
                      });
                break;
              case "sms":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactSMS(context, gloablSelectedContacts).then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactGlobalSearchBloc>(context).add(
                              ContactGlobalSearchRefresh(
                                  filterHeader: "contact",
                                  keyword: contactKeyword));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              case "email":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactEmail(context, gloablSelectedContacts)
                        .then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactGlobalSearchBloc>(context).add(
                              ContactGlobalSearchRefresh(
                                  filterHeader: "contact",
                                  keyword: contactKeyword));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              case "voice":
                gloablSelectedContacts == ""
                    ? errorAlert(context, "Please Select One or More Contacts")
                    : contactVoice(context, gloablSelectedContacts)
                        .then((value) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          BlocProvider.of<ContactGlobalSearchBloc>(context).add(
                              ContactGlobalSearchRefresh(
                                  filterHeader: "contact",
                                  keyword: contactKeyword));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      });
                break;
              default:
            }
          },
          itemBuilder: (context) => cardContactGlobalOptions.map((e) {
            return PopupMenuItem(
              value: e.id,
              child: Row(
                children: [
                  ImageLoad(img: e.img, width: 25),
                  SizedBox(width: 15),
                  Text(e.title),
                ],
              ),
            );
          }).toList(),
          icon: SizedBox(
            width: 30,
            height: 30,
            child: ImageLoad(
              img: '/mobile_app_v2/o_moreoptions_appbar.png',
              width: 30,
            ),
          ),
          offset: Offset(0, 20),
        );
        break;
      default:
        return Container();
    }
  }
}
