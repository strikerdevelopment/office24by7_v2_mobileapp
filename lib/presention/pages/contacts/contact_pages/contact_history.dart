import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/contacts/contacts.dart';
import 'package:office24by7_v2/presention/pages/globals/global.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ContactHistory extends StatelessWidget {
  final ContactList contactList;
  ContactHistory({Key key, this.contactList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(title: "Contact History"),
            BodyInner(
                page: BlocBuilder<ContactDetailsBloc, ContactDetailsState>(
                    // ignore: missing_return
                    builder: (context, state) {
              if (state is ContactDetailsInitial) {
                return LoaderIndicator();
              }
              if (state is ContactDetailsSuccess) {
                final contactInfo = state.contactDetails;
                return ContactHistoryContent(
                    contactInfo: contactInfo, contactList: contactList);
              }
              if (state is ContactsDetailsFailure) {
                return FailedToLoad(message: "Failed to Load");
              }
            })),
          ],
        ),
      ),
    );
  }
}

class ContactHistoryContent extends StatelessWidget {
  const ContactHistoryContent({
    Key key,
    @required this.contactInfo,
    @required this.contactList,
  }) : super(key: key);

  final ContactDetailsModel contactInfo;
  final ContactList contactList;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: tabBodyPadd,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(250),
                  ),
                  child: Container(
                    decoration: roundShadow,
                    height: 50,
                    width: 50,
                    child: ImageLoad(img: '/mobile_app_v2/o_profile_pic.png'),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  profileText(contactInfo.firstName, 16),
                  contactInfo.company == ''
                      ? Text('No Comapny Found')
                      : Text("${contactInfo.company}"),
                ],
              ),
              SizedBox(width: 15),
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: contactHistoryTabs.map((data) {
                      return ContactHistoryTabsData(
                        contactDetailsTabs: data,
                        contactList: contactList,
                      );
                    }).toList(),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: textGray)),
          ),
        ),
        BlocBuilder<ContactHistoryBloc, ContactHistoryState>(
          builder: (context, state) {
            if (state is ContactHistorySuccess) {
              return Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  children: state.stateData.map((e) {
                    return Column(
                      children: [
                        HistoryDataDesign(date: e.historydate),
                        Column(
                          children: e.historyrepeat
                              .map(
                                (e) => Material(
                                  color: white,
                                  child: InkWell(
                                    onTap: () =>
                                        contactHistoryDetails(context, e),
                                    child: HistoryStatusDesign(
                                      time: e.time,
                                      type: e.type,
                                      description: e.description,
                                      image: historyIcon(e.type),
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ],
                    );
                  }).toList(),
                ),
              );
            }
            if (state is ContactHistoryFailure) {
              return Center(
                child: Container(
                  child: Text('${state.error}'),
                ),
              );
            }

            return LoaderIndicator();
          },
        ),
      ],
    );
  }
}

String historyIcon(String type) {
  print(type);
  switch (type) {
    case "Update":
      return "/mobile_app_v2/o_option_task.png";
      break;
    case "Reassign":
      return "/mobile_app_v2/o_option_reassign.png";
      break;
    case "Meeting":
      return "/mobile_app_v2/o_option_meeting.png";
      break;
    case "EMAIL":
      return "/mobile_app_v2/o_email_purple.png";
      break;
    case "SMS":
      return "/mobile_app_v2/o_text_purple.png";
      break;
    case "Lead":
      return "/mobile_app_v2/o_option_add_to_lead.png";
      break;
    case "List":
      return "/mobile_app_v2/o_option_add_to_list.png";
      break;
    case "Call":
      return "/mobile_app_v2/o_call_purple.png";
      break;
    case "OutgoingCall":
      return "/mobile_app_v2/o_option_voice.png";
      break;
    default:
      return "/mobile_app_v2/o_option_task.png";
  }
}

Future contactHistoryDetails(BuildContext context, Historyrepeat data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return ModalBox(
            modalHieght: 700,
            modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_option_contact_details.png",
                title: "Contact History Details"),
            modalBody: Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(children: [
                  Column(
                    children: [
                      ListTile(
                        title: Text('Time'),
                        subtitle: Text('${data.time}'),
                      ),
                      ListTile(
                        title: Text('Type'),
                        subtitle: Text('${data.type}'),
                      ),
                      ListTile(
                        title: Text('AddedBy'),
                        subtitle: Text('${data.addedBy}'),
                      ),
                      ListTile(
                        title: Text('SPOC'),
                        subtitle: Text('${data.spoc}'),
                      ),
                      // ListTile(
                      //   title: Text('typeimg'),
                      //   subtitle: Text("${data.typeimg}"),
                      // ),
                      ListTile(
                        title: Text('Description'),
                        subtitle: Html(data: """${data.description}"""),
                      ),
                      // ListTile(
                      //   title: Text('Attachments'),
                      //   subtitle: Text(data.activityType),
                      // ),
                    ],
                  ),
                ]),
              ),
            ),
            modalFooter: ModalTextFooter(),
          );
        },
      );
    },
  );
}
