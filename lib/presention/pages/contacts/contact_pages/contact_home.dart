import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_blocs/contacts/contact/contact_form_load/contactformload_bloc.dart';
import 'package:office24by7_v2/_blocs/global/country_flag/country_flag_bloc.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_data/repositories/global_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/contacts/contacts.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactsBloc _contactsBloc;
String gloablSelectedContacts = "";

class ContactHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ContactsBody();
  }
}

class ContactsBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: tabBodyPadd,
      child: RefreshIndicator(
        onRefresh: () async {
          _contactsBloc.add(ContactRefresh(
            filterHeader: "contact",
            filterOption: contactDisplayAs,
          ));
        },
        color: purple,
        child: Column(
          children: [
            SearchButton(
                text: "Search Contact",
                onClick: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ContactGlobalSearch()),
                  );
                }),
            SizedBox(height: 8),
            Expanded(
              child: ContactsData(),
            ),
          ],
        ),
      ),
    );
  }
}

class ContactsData extends StatefulWidget {
  ContactsData({Key key}) : super(key: key);

  @override
  _ContactsDataState createState() => _ContactsDataState();
}

class _ContactsDataState extends State<ContactsData> {
  MultiSelectController mlController = new MultiSelectController();

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  List selectedItems = [];

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _contactsBloc = BlocProvider.of<ContactsBloc>(context);
    _contactsBloc.add(ContactFetched(
        filterHeader: "contact", filterOption: contactDisplayAs));
    mlController.disableEditingWhenNoneSelected = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        BlocListener<ContactsBloc, ContactsState>(
          listener: (context, state) {
            if (state is ContactsInitial) {
              setState(() {
                mlController.deselectAll();
                selectedItems = [];
                gloablSelectedContacts = "";
              });
            }
          },
          child: BlocBuilder<ContactsBloc, ContactsState>(
            builder: (context, state) {
              if (state is ContactsFailure) {
                return FailedToLoad(message: state.error);
              }
              if (state is ContactsSuccess) {
                return state.contacts.isEmpty
                    ? NoDataFound(message: "No Data Found")
                    : ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (context, index) {
                          bool last = state.contacts.length == (index + 1);
                          return index >= state.contacts.length
                              ? state.currentState.length >=
                                      Environment.dataDisplayCount
                                  ? LoaderSmallIndicator()
                                  : Container()
                              : MultiSelectItem(
                                  isSelecting: mlController.isSelecting,
                                  onSelected: () {
                                    setState(() {
                                      mlController.toggle(index);
                                      selectedItems =
                                          mlController.selectedIndexes;

                                      final List contactIdStrings = [];
                                      selectedItems
                                          .map((e) => contactIdStrings
                                              .add(state.contacts[e].contactId))
                                          .toList();
                                      gloablSelectedContacts =
                                          contactIdStrings.join(',');
                                    });
                                  },
                                  child: ContactCard(
                                    data: state.contacts[index],
                                    isActive: mlController.isSelected(index),
                                    last: last,
                                  ),
                                );
                        },
                        itemCount: state.hasReachedMax
                            ? state.contacts.length
                            : state.contacts.length + 1,
                        controller: _scrollController,
                      );
              }
              return LoaderIndicator();
            },
          ),
        ),
        selectedItems.isEmpty
            ? Container()
            : Positioned(
                bottom: 0,
                left: -10,
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: purpleTtW,
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.8, 0.8),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                mlController.deselectAll();
                                gloablSelectedContacts = "";
                              });
                            },
                            child: Icon(Icons.close, color: black)),
                      ),
                      Text(
                          "You have selected ${selectedItems.length} contacts"),
                    ],
                  ),
                ),
              ),
        Positioned(
          bottom: 0,
          right: -10,
          child: BlocProvider(
            create: (context) => ContactaddcheckBloc(
                contactRepository:
                    ContactRepository(contactAPIClient: ContactAPIClient()))
              ..add(ContactaddcheckPressed()),
            child: BlocBuilder<ContactaddcheckBloc, ContactaddcheckState>(
              builder: (context, state) {
                if (state is ContactaddcheckSuccess) {
                  return FloatingButtonCustome(
                    onPressed: () {
                      addContactCB(context);
                    },
                    icon: Icon(
                      Icons.add,
                    ),
                  );
                }
                if (state is ContactaddcheckFailure) {
                  return FloatingButtonCustome(
                    onPressed: () {
                      errorAlert(
                        context,
                        "You don't have Previllages, please contact to Support Team...",
                      );
                    },
                    icon: Icon(
                      Icons.add,
                    ),
                  );
                }
                return FloatingActionButton(
                  backgroundColor: white,
                  onPressed: () {},
                  child: LoaderIndicator(),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _contactsBloc.add(ContactFetched(
          filterHeader: "contact", filterOption: contactDisplayAs));
    }
  }

  addContactCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ContactformloadBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(
                  ContactFormLoadPressed(),
                ),
            ),
            BlocProvider(
              create: (context) => ContactSourceBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactSourcePressed()),
            ),
            BlocProvider(
              create: (context) => ContactGroupBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactGroupPressed()),
            ),
            BlocProvider(
              create: (context) => CountryBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              )..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) => StateBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CityBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => ContactGetMoreFieldsBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CountryFlagBloc(
                  globalRepository:
                      GlobalRepository(globalApiClient: GlobalApiClient()))
                ..add(CountryFlagPressed()),
            ),
          ],
          child: ContactAdd(),
        ),
      ),
    );

    if (result == "success") {
      _contactsBloc.add(ContactRefresh(
          filterHeader: "contact", filterOption: contactDisplayAs));
      successAlert(context, "Contact Added Successfully...");
    }
  }
}

class ContactCard extends StatelessWidget {
  final ContactList data;
  final bool isActive;
  final bool last;

  const ContactCard({Key key, this.data, this.isActive, this.last})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: white,
      elevation: 0,
      margin: last ? EdgeInsets.only(bottom: 50) : EdgeInsets.only(bottom: 10),
      child: Container(
        padding: cardPadd,
        child: Row(
          children: [
            Stack(
              children: [
                Container(
                  width: 40,
                  height: 40,
                  child: isActive
                      ? Icon(Icons.done, color: white)
                      : Center(
                          child: data.assignedTo != "0"
                              ? Text(
                                  StringSplit(name: '${data.contact}')
                                      .twoLetter(),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : ImageLoad(
                                  img: "/mobile_app_v2/o_option_admin.png",
                                  width: 30,
                                ),
                        ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: isActive ? purple : data.color.withOpacity(0.5),
                  ),
                ),
                data.duplicateCount == 0
                    ? Container()
                    : RoundCircle(color: dartpurple),
              ],
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${data.contact.capitalize()}",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    children: [
                      Text("${data.mobile}", style: TextStyle(fontSize: 12)),
                      Flexible(
                        child: data.companyName == "null"
                            ? Container()
                            : Text(" | ${data.companyName}",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 12)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            PopupMenuButton(
              onSelected: (value) {
                switch (value) {
                  case "contactDetails":
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BlocProvider(
                          create: (context) => ContactDetailsBloc(
                              contactRepository: contactRepository)
                            ..add(ContactDetailsPressed(
                                contactId: data.contactId)),
                          child: ContactDetails(contactList: data),
                        ),
                      ),
                    );
                    break;
                  case "contactHistory":
                    ContactRepository contactRepository =
                        ContactRepository(contactAPIClient: ContactAPIClient());
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(
                          providers: [
                            BlocProvider(
                              create: (context) => ContactDetailsBloc(
                                  contactRepository: contactRepository)
                                ..add(ContactDetailsPressed(
                                    contactId: data.contactId)),
                            ),
                            BlocProvider(
                              create: (context) => ContactHistoryBloc(
                                  contactRepository: contactRepository)
                                ..add(
                                  ContactHistoryPressed(
                                      eventData: data.contactId),
                                ),
                            ),
                          ],
                          child: ContactHistory(contactList: data),
                        ),
                      ),
                    );
                    break;
                  case "meeting":
                    contactMeeting(context, data);
                    break;
                  case "task":
                    contactTask(context, data);
                    break;
                  case "addToLeads":
                    contactAddToLead(context, data);
                    break;
                  case "addToList":
                    contactAddToList(context, data.contactId, "contact")
                        .then((value) {
                      if (value['status'] == "success") {
                        successAlert(context, value['data']);
                      } else if (value['status'] == "failure") {
                        errorAlert(context, value['data']);
                      }
                    });
                    break;
                  case "reassignContact":
                    contactReassign(context, data.contactId).then((value) {
                      if (value == "success") {
                        successAlert(context, "Reassign Added Successfully");
                        _contactsBloc.add(ContactRefresh(
                            filterHeader: "contact",
                            filterOption: contactDisplayAs));
                      } else if (value == "failure") {
                        errorAlert(context, "Failed to Add Reassign");
                      }
                    });
                    break;
                  case "deleteContact":
                    contactDelete(context, data.contactId).then((value) {
                      if (value["status"] == "success") {
                        successAlert(context, value["data"]);
                        _contactsBloc.add(ContactRefresh(
                            filterHeader: "contact",
                            filterOption: contactDisplayAs));
                      } else if (value["status"] == "failure") {
                        errorAlert(context, value["data"]);
                      }
                    });
                    break;
                  case "copyContact":
                    copyContactCB(context);
                    break;
                  case "editContact":
                    editContactCB(context);
                    break;
                  case "viewDuplicate":
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ContactDuplicates(data: data),
                      ),
                    );
                    break;
                  default:
                }
              },
              itemBuilder: (context) => cardContactOptions.map((e) {
                if (e.id == "editContact") {
                  if (data.assignedTo != "0") {
                    if (data.displayStatus != 0) {
                      return PopupMenuItem(
                        value: e.id,
                        child: Row(
                          children: [
                            ImageLoad(img: e.img, width: 25),
                            SizedBox(width: 15),
                            Text(e.title),
                          ],
                        ),
                      );
                    }
                  }
                } else if (e.id == "copyContact") {
                  if (data.assignedTo != "0") {
                    if (data.displayStatus != 0) {
                      return PopupMenuItem(
                        value: e.id,
                        child: Row(
                          children: [
                            ImageLoad(img: e.img, width: 25),
                            SizedBox(width: 15),
                            Text(e.title),
                          ],
                        ),
                      );
                    }
                  }
                } else if (e.id == "deleteContact") {
                  if (data.assignedTo != "0") {
                    if (data.displayStatus != 0) {
                      return PopupMenuItem(
                        value: e.id,
                        child: Row(
                          children: [
                            ImageLoad(img: e.img, width: 25),
                            SizedBox(width: 15),
                            Text(e.title),
                          ],
                        ),
                      );
                    }
                  }
                } else if (e.id == "viewDuplicate") {
                  if (data.duplicateCount != 0) {
                    return PopupMenuItem(
                      value: e.id,
                      child: Row(
                        children: [
                          ImageLoad(img: e.img, width: 25),
                          SizedBox(width: 15),
                          Text(e.title),
                        ],
                      ),
                    );
                  }
                } else {
                  return PopupMenuItem(
                    value: e.id,
                    child: Row(
                      children: [
                        ImageLoad(img: e.img, width: 25),
                        SizedBox(width: 15),
                        Text(e.title),
                      ],
                    ),
                  );
                }
              }).toList(),
              icon: SizedBox(
                width: 20,
                height: 20,
                child: ImageLoad(
                  img: '/mobile_app_v2/o_options_card.png',
                  width: 20,
                ),
              ),
              offset: Offset(0, 20),
            ),
          ],
        ),
      ),
    );
  }

  copyContactCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ContactformloadBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(
                  ContactFormLoadPressed(),
                ),
            ),
            BlocProvider(
              create: (context) => ContactSourceBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactSourcePressed()),
            ),
            BlocProvider(
              create: (context) => ContactGroupBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactGroupPressed()),
            ),
            BlocProvider(
              create: (context) => CountryBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              )..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) => StateBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CityBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => ContactGetMoreFieldsBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CountryFlagBloc(
                  globalRepository:
                      GlobalRepository(globalApiClient: GlobalApiClient()))
                ..add(CountryFlagPressed()),
            ),
          ],
          child: ContactCopy(data: data),
        ),
      ),
    );
    if (result == "success") {
      _contactsBloc.add(ContactRefresh(
          filterHeader: "contact", filterOption: contactDisplayAs));
      successAlert(context, "Contact Copied Successfully...");
    }
  }

  editContactCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => ContactformloadBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(
                  ContactFormLoadPressed(),
                ),
            ),
            BlocProvider(
              create: (context) => ContactSourceBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactSourcePressed()),
            ),
            BlocProvider(
              create: (context) => ContactGroupBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              )..add(ContactGroupPressed()),
            ),
            BlocProvider(
              create: (context) => CountryBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              )..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) => StateBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CityBloc(
                globalRepository: GlobalRepository(
                  globalApiClient: GlobalApiClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => ContactGetMoreFieldsBloc(
                contactRepository: ContactRepository(
                  contactAPIClient: ContactAPIClient(),
                ),
              ),
            ),
            BlocProvider(
              create: (context) => CountryFlagBloc(
                  globalRepository:
                      GlobalRepository(globalApiClient: GlobalApiClient()))
                ..add(CountryFlagPressed()),
            ),
          ],
          child: ContactEdit(data: data),
        ),
      ),
    );

    if (result == "success") {
      _contactsBloc.add(ContactRefresh(
          filterHeader: "contact", filterOption: contactDisplayAs));
      successAlert(context, "Contact Edited Successfully...");
    }
  }
}
