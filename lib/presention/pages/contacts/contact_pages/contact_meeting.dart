import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_blocs/contacts/contact/contat_sender_id/contact_sender_id_bloc.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/contacts/contact_model/contact_repeat_date.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future contactMeeting(BuildContext context, ContactList contactInfo) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      final ValueChanged _onChanged = (val) => print(val);

      bool notifyDisplay = false;
      bool smsDisplay = false;
      bool emailDisplay = false;
      bool repeatDate = true;
      String repeatDateValue = 'Doesnotrepeat';
      String endsvalueDisplay = 'endson';

      return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => ContactSenderIdBloc(
              contactRepository: ContactRepository(
                contactAPIClient: ContactAPIClient(),
              ),
            )..add(ContactSenderIDPressed()),
          ),
          BlocProvider(
            create: (context) => ContactEmailIdBloc(
              contactRepository: ContactRepository(
                contactAPIClient: ContactAPIClient(),
              ),
            )..add(ContactEmailIDPressed()),
          ),
          BlocProvider(
            create: (context) => ContactAddMeetingBloc(
              contactRepository: ContactRepository(
                contactAPIClient: ContactAPIClient(),
              ),
            ),
          )
        ],
        child: StatefulBuilder(
          builder: (BuildContext context, setState) {
            return ModalBox(
              modalHieght: 700,
              modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_meeting_modal.png",
                title: "Meeting",
              ),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'contact_id': contactInfo.contactId,
                        'contact_name': contactInfo.contact,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'contact_id'),
                          FormBuilderHiddenField(value: 'contact_name'),
                          FormBuilderTextField(
                            name: "reminder_title",
                            decoration: formGlobal("Title"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "reminder_desc",
                            decoration: formGlobal("Description"),

                            //keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'reminder_date',
                                  format: DateFormat('yyyy-MM-dd'),
                                  onChanged: (value) {
                                    if (value == null) {
                                      setState(() {
                                        repeatDate = false;
                                      });
                                    } else {
                                      setState(() {
                                        repeatDate = true;
                                      });
                                    }
                                  },
                                  inputType: InputType.date,
                                  decoration: formGlobal("Date"),
                                  initialValue: DateTime.now(),
                                  firstDate: DateTime.now(),
                                  initialEntryMode:
                                      DatePickerEntryMode.calendar,
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                ),
                              ),
                              fieldWidth,
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'reminder_time',
                                  onChanged: _onChanged,
                                  inputType: InputType.time,
                                  decoration: formGlobal("Time"),
                                  initialValue: DateTime.now(),
                                  firstDate: DateTime.now(),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                ),
                              )
                            ],
                          ),
                          FormBuilderCheckbox(
                            activeColor: purple,
                            name: 'Isnotify',
                            title:
                                Text("Notify", style: TextStyle(fontSize: 16)),
                            // leadingInput: true,
                            initialValue: false,
                            decoration: formGlobalcheckbox(),
                            onChanged: (value) {
                              setState(() {
                                notifyDisplay = value;
                              });
                            },
                          ),
                          notifyDisplay
                              ? Column(
                                  children: [
                                    FormBuilderCheckbox(
                                      activeColor: purple,
                                      name: 'Issms',
                                      title: Text("SMS",
                                          style: TextStyle(fontSize: 16)),
                                      // leadingInput: true,
                                      initialValue: false,
                                      decoration: formGlobalcheckbox(),
                                      onChanged: (value) {
                                        setState(() {
                                          smsDisplay = value;
                                        });
                                      },
                                    ),
                                    smsDisplay
                                        ? BlocBuilder<ContactSenderIdBloc,
                                            ContactSenderIdState>(
                                            builder: (context, state) {
                                              if (state
                                                  is ContactSenderIdSuccess) {
                                                return FormBuilderDropdown(
                                                  name: "SenderID",
                                                  decoration: formGlobal(
                                                      "Select Sender ID"),
                                                  items: state.getSenderID
                                                      .map((data) =>
                                                          DropdownMenuItem(
                                                              value: data.sId,
                                                              child: Text(
                                                                  "${data.senderId}")))
                                                      .toList(),
                                                );
                                              }
                                              if (state
                                                  is ContactSenderIdFailure) {
                                                return Text("${state.error}");
                                              }
                                              return LoaderIndicator();
                                            },
                                          )
                                        : Container(),
                                    FormBuilderCheckbox(
                                      activeColor: purple,
                                      name: 'Isemail',
                                      title: Text("Email",
                                          style: TextStyle(fontSize: 16)),
                                      // leadingInput: true,
                                      initialValue: false,
                                      decoration: formGlobalcheckbox(),
                                      onChanged: (value) {
                                        setState(() {
                                          emailDisplay = value;
                                        });
                                      },
                                    ),
                                    emailDisplay
                                        ? BlocBuilder<ContactEmailIdBloc,
                                            ContactEmailIdState>(
                                            builder: (context, state) {
                                              if (state
                                                  is ContactEmailIdSuccess) {
                                                return FormBuilderDropdown(
                                                  name: "EmailID",
                                                  decoration: formGlobal(
                                                      "Select Email ID"),
                                                  items: state.stateData
                                                      .map(
                                                        (data) =>
                                                            DropdownMenuItem(
                                                          value: data.seId,
                                                          child: Text(
                                                              "${data.fromMail}"),
                                                        ),
                                                      )
                                                      .toList(),
                                                );
                                              }
                                              if (state
                                                  is ContactEmailIdFailure) {
                                                return Text("${state.error}");
                                              }
                                              return LoaderIndicator();
                                            },
                                          )
                                        : Container(),
                                    FormBuilderCheckbox(
                                      activeColor: purple,
                                      name: 'webmessage',
                                      title: Text("Web Message",
                                          style: TextStyle(fontSize: 16)),
                                      // leadingInput: true,
                                      initialValue: false,
                                      decoration: formGlobalcheckbox(),
                                    ),
                                  ],
                                )
                              : Container(),
                          fieldDistance,
                          repeatDate
                              ? FormBuilderDropdown(
                                  name: "Reminders",
                                  decoration: formGlobal("Select Schedule"),
                                  onChanged: (value) {
                                    setState(() {
                                      repeatDateValue = value;
                                    });
                                  },
                                  items: contactRepeatDate
                                      .map(
                                        (data) => DropdownMenuItem(
                                          value: data.type,
                                          child: Text(
                                            "${data.name}",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      )
                                      .toList(),
                                )
                              : Container(),
                          fieldDistance,
                          repeatDateValue == 'Doesnotrepeat'
                              ? Container()
                              : Column(
                                  children: [
                                    FormBuilderRadioGroup(
                                      decoration: formGlobalradiobox('Ends:'),
                                      name: 'endsvalue',
                                      onChanged: (value) {
                                        setState(() {
                                          endsvalueDisplay = value;
                                        });
                                      },
                                      activeColor: purple,
                                      focusColor: purple,
                                      initialValue: 'endson',
                                      options: contactEdson
                                          .map(
                                            (data) => FormBuilderFieldOption(
                                              value: data.type,
                                              child: Text(data.name),
                                            ),
                                          )
                                          .toList(growable: true),
                                    ),
                                    endsvalueDisplay == 'endson'
                                        ? FormBuilderDateTimePicker(
                                            name: 'alertendtdate',
                                            onChanged: (value) {},
                                            inputType: InputType.date,
                                            decoration: formGlobal("Date"),
                                            validator: (val) => null,
                                            //initialValue: DateTime.now(),
                                            firstDate: DateTime.now(),
                                            initialEntryMode:
                                                DatePickerEntryMode.calendar,
                                          )
                                        : FormBuilderTextField(
                                            name: "remaindernumber",
                                            decoration:
                                                formGlobal("Occurrences"),
                                            keyboardType: TextInputType.number,
                                          ),
                                  ],
                                ),
                          fieldDistance,
                          // repeatDateValue == 'custom'
                          //     ? Text(repeatDateValue)
                          //     : Container()
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<ContactAddMeetingBloc, ContactAddMeetingState>(
                listener: (context, state) {
                  if (state is ContactAddMeetingSuccess) {
                    successAlert(context, state.stateData);
                    Navigator.pop(context);
                  }
                  if (state is ContactAddMeetingFailure) {
                    errorAlert(context, state.error);
                    Navigator.pop(context);
                  }
                },
                child:
                    BlocBuilder<ContactAddMeetingBloc, ContactAddMeetingState>(
                  builder: (context, state) {
                    return state is ContactAddMeetingInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData =
                                    Map.of(_fbKey.currentState.value);

                                formData['reminder_date'] = _fbKey
                                    .currentState.value['reminder_date']
                                    .toString();
                                formData['reminder_time'] = _fbKey
                                    .currentState.value['reminder_time']
                                    .toString();
                                formData['Isnotify'] = _fbKey
                                    .currentState.value['Isnotify']
                                    .toString();
                                formData['Issms'] = _fbKey
                                    .currentState.value['Issms']
                                    .toString();
                                formData['Isemail'] = _fbKey
                                    .currentState.value['Isemail']
                                    .toString();
                                formData['webmessage'] = _fbKey
                                    .currentState.value['webmessage']
                                    .toString();

                                print(formData);
                                BlocProvider.of<ContactAddMeetingBloc>(context)
                                    .add(ContactAddMeetingPressed(
                                        eventData: formData));
                              }
                            },
                          );
                  },
                ),
              ),
            );
          },
        ),
      );
    },
  );
}
