import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/contacts/contacts.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

final ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future contactReassign(BuildContext context, String contactId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => ContactHirarchyUserBloc(
                    contactRepository: _contactRepository)
                  ..add(ContactHirarchyUserPressed()),
              ),
              BlocProvider(
                create: (context) => ContactAddToReassignBloc(
                    contactRepository: _contactRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_reassign.png",
                  title: "Reassign"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'contact_id': contactId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'contact_id'),
                          BlocBuilder<ContactHirarchyUserBloc,
                              ContactHirarchyUserState>(
                            builder: (context, state) {
                              if (state is ContactHirarchyUserSuccess) {
                                return FormBuilderDropdown(
                                  name: "user_id",
                                  decoration:
                                      formGlobal("Select User to Reassign"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.userId,
                                          child: Text(
                                              "${e.firstName} ${e.lastName}")))
                                      .toList(),
                                );
                              }
                              if (state is ContactHirarchyUserFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderIndicator();
                            },
                          ),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<ContactAddToReassignBloc,
                  ContactAddToReassignState>(
                listener: (context, state) {
                  if (state is ContactAddToReassignSuccess) {
                    Navigator.pop(context, "success");
                  }
                  if (state is ContactAddToReassignFailure) {
                    Navigator.pop(context, "failure");
                  }
                },
                child: BlocBuilder<ContactAddToReassignBloc,
                    ContactAddToReassignState>(
                  builder: (context, state) {
                    return state is ContactAddToReassignInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<ContactAddToReassignBloc>(
                                        context)
                                    .add(ContactAddToReassignPressed(
                                        eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
