import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future contactSMS(BuildContext context, String contactId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    ContactSenderIdsBloc(contactRepository: _contactRepository)
                      ..add(ContactSenderIdsPressed()),
              ),
              BlocProvider(
                create: (context) =>
                    ContactSmsTypeBloc(contactRepository: _contactRepository)
                      ..add(ContactSmsTypePressed()),
              ),
              BlocProvider(
                create: (context) =>
                    ContactSendSmsBloc(contactRepository: _contactRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 600,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_sms_modal.png", title: "SMS"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'contact_id': contactId,
                        "shorturl_Text": "",
                        "long_URL": "",
                        "campaign_Type": "",
                        "contact_campaign_type": "",
                        "contacts_count": "",
                        "frompage": "",
                        "frompageid": ""
                        //'contact_name': contactInfo.contact,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'contact_id'),
                          FormBuilderHiddenField(value: 'shorturl_Text'),
                          FormBuilderHiddenField(value: 'long_URL'),
                          FormBuilderHiddenField(value: 'campaign_Type'),
                          FormBuilderHiddenField(
                              value: 'contact_campaign_type'),
                          FormBuilderHiddenField(value: 'contacts_count'),
                          FormBuilderHiddenField(value: 'frompage'),
                          FormBuilderHiddenField(value: 'frompageid'),
                          FormBuilderTextField(
                            name: "campaign_name",
                            decoration: formGlobal("Campaign Name"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          BlocBuilder<ContactSenderIdsBloc,
                              ContactSenderIdsState>(
                            builder: (context, state) {
                              if (state is ContactSenderIdsSuccess) {
                                return FormBuilderDropdown(
                                  name: "sender_Name",
                                  decoration: formGlobal("Select Sender ID"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.senderId,
                                          child: Text("${e.senderId}")))
                                      .toList(),
                                );
                              }
                              if (state is ContactSenderIdsFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderSmallIndicator();
                            },
                          ),
                          fieldDistance,
                          BlocBuilder<ContactSmsTypeBloc, ContactSmsTypeState>(
                            builder: (context, state) {
                              if (state is ContactSmsTypeSuccess) {
                                return FormBuilderDropdown(
                                  name: "campaign_Form_Type",
                                  decoration: formGlobal("Select SMS Type"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e, child: Text("$e")))
                                      .toList(),
                                );
                              }
                              if (state is ContactSmsTypeFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderSmallIndicator();
                            },
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "sms_Text_Message",
                            decoration: formGlobal("Message"),
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<ContactSendSmsBloc, ContactSendSmsState>(
                listener: (context, state) {
                  if (state is ContactSendSmsSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});
                  }
                  if (state is ContactSendSmsFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<ContactSendSmsBloc, ContactSendSmsState>(
                  builder: (context, state) {
                    return state is ContactSendSmsInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<ContactSendSmsBloc>(context)
                                    .add(ContactSendSmsPressed(
                                        eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
