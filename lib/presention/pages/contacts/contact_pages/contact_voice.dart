import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future contactVoice(BuildContext context, String contactId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => ContactVoiceCallerIdsBloc(
                    contactRepository: _contactRepository)
                  ..add(ContactVoiceCallerIdsPressed()),
              ),
              BlocProvider(
                create: (context) => ContactVoiceAudioFilesBloc(
                    contactRepository: _contactRepository)
                  ..add(ContactVoiceAudioFilesPressed()),
              ),
              BlocProvider(
                create: (context) =>
                    ContactSendVoiceBloc(contactRepository: _contactRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 700,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_voice.png", title: "Voice"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'contact_id': contactId,
                        "campaign_Type": "1",
                        "provider_Name": "Office24by7",
                        "audio_File_ID": "256",
                        "contact_campaign_type": "",
                        "contacts_count": "",
                        "frompage": "",
                        "frompageid": ""
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'contact_id'),
                          FormBuilderHiddenField(value: 'campaign_Type'),
                          FormBuilderHiddenField(value: 'provider_Name'),
                          FormBuilderHiddenField(value: 'audio_File_ID'),
                          FormBuilderHiddenField(
                              value: 'contact_campaign_type'),
                          FormBuilderHiddenField(value: 'contacts_count'),
                          FormBuilderHiddenField(value: 'frompage'),
                          FormBuilderHiddenField(value: 'frompageid'),
                          FormBuilderTextField(
                            name: "campaign_name",
                            decoration: formGlobal("Campaign Name"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          BlocBuilder<ContactVoiceCallerIdsBloc,
                              ContactVoiceCallerIdsState>(
                            builder: (context, state) {
                              if (state is ContactVoiceCallerIdsSuccess) {
                                return FormBuilderDropdown(
                                  name: "caller_ID",
                                  decoration: formGlobal("Select Caller ID"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.calleridNum,
                                          child: Text("${e.calleridNum}")))
                                      .toList(),
                                );
                              }
                              if (state is ContactVoiceCallerIdsFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderIndicator();
                            },
                          ),
                          fieldDistance,
                          BlocBuilder<ContactVoiceAudioFilesBloc,
                              ContactVoiceAudioFilesState>(
                            builder: (context, state) {
                              if (state is ContactVoiceAudioFilesSuccess) {
                                return FormBuilderDropdown(
                                  name: "audio_File_Path",
                                  decoration: formGlobal("Select Audio File"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.ivrLocation,
                                          child: Text("${e.ivrFilename}")))
                                      .toList(),
                                );
                              }
                              if (state is ContactVoiceAudioFilesFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderIndicator();
                            },
                          ),
                          fieldDistance,
                          FormBuilderDropdown(
                            name: "num_Attempts",
                            decoration: formGlobal("Number of attempts"),
                            items: ["1", "2", "3", "4", "5"]
                                .map((e) => DropdownMenuItem(
                                    value: e, child: Text("$e")))
                                .toList(),
                          ),
                          fieldDistance,
                          FormBuilderDropdown(
                            name: "no_Re_Attempts",
                            decoration: formGlobal("Re-attempt delay(Seconds)"),
                            items: ["5", "10", "20", "30", "40", "50"]
                                .map((e) => DropdownMenuItem(
                                    value: e, child: Text("$e")))
                                .toList(),
                          ),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<ContactSendVoiceBloc, ContactSendVoiceState>(
                listener: (context, state) {
                  if (state is ContactSendVoiceSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});
                  }
                  if (state is ContactSendVoiceFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<ContactSendVoiceBloc, ContactSendVoiceState>(
                  builder: (context, state) {
                    return state is ContactSendVoiceInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;

                                BlocProvider.of<ContactSendVoiceBloc>(context)
                                    .add(ContactSendVoicePressed(
                                        eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
