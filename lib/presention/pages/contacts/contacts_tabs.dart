// import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/contacts/contacts.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

int globalSelectedTab = 0;

String contactDisplayAs = "all";

class ContactsHomeTabs extends StatefulWidget {
  ContactsHomeTabs({Key key}) : super(key: key);

  @override
  _ContactsHomeTabsState createState() => _ContactsHomeTabsState();
}

class _ContactsHomeTabsState extends State<ContactsHomeTabs> {
  @override
  void initState() {
    globalSelectedTab = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: contactHeader(globalSelectedTab),
              actions: contactGlobalappBarActions(context, globalSelectedTab),
            ),
            BodyWithTabSpace(
              page: TabSection(callSetState),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> contactGlobalappBarActions(
      BuildContext context, int globalSelectedTab) {
    switch (globalSelectedTab) {
      case 0:
        return [
          // Badge(
          //   badgeColor: orange,
          //   position: BadgePosition.topEnd(top: 3, end: 5),
          //   animationDuration: Duration(milliseconds: 300),
          //   animationType: BadgeAnimationType.slide,
          //   badgeContent: Text(
          //     '2',
          //     style: TextStyle(color: Colors.white),
          //   ),
          //   child: IconButton(
          //       icon: ImageLoad(
          //         img: "/mobile_app_v2/o_notification_appbar.png",
          //         width: 30,
          //       ),
          //       onPressed: () {
          //         print(gloablSelectedContacts);
          //       }),
          // ),
          IconButton(
            icon: ImageLoad(
              img: "/mobile_app_v2/o_filter_appbar.png",
              width: 30,
            ),
            onPressed: () {
              contactFilter(context, contactDisplayAs).then((data) {
                if (data != null) {
                  print(data);
                  setState(() {
                    contactDisplayAs = data["display_as"];
                  });

                  BlocProvider.of<ContactsBloc>(context).add(ContactRefresh(
                      filterHeader: "contact", filterOption: contactDisplayAs));
                }
              });
            },
          ),
          ContactGlobalOptions(globalSelectedTab: globalSelectedTab),
        ];
        break;
      case 1:
        return [
          // IconButton(
          //   icon: Icon(Icons.refresh),
          //   onPressed: () {
          //     BlocProvider.of<ContactGroupListBloc>(context).add(
          //         ContactGroupListRefresh(
          //             filterHeader: "group", filterOption: "all"));
          //   },
          // ),
          // Badge(
          //   badgeColor: orange,
          //   position: BadgePosition.topEnd(top: 3, end: 5),
          //   animationDuration: Duration(milliseconds: 300),
          //   animationType: BadgeAnimationType.slide,
          //   badgeContent: Text(
          //     '2',
          //     style: TextStyle(color: Colors.white),
          //   ),
          //   child: IconButton(
          //       icon: ImageLoad(
          //         img: "/mobile_app_v2/o_notification_appbar.png",
          //         width: 30,
          //       ),
          //       onPressed: () {
          //         print(gloablSelectedGroups);
          //       }),
          // ),
          GroupGlobalOptions(globalSelectedTab: globalSelectedTab),
        ];
        break;
      case 2:
        return [
          // Badge(
          //   badgeColor: orange,
          //   position: BadgePosition.topEnd(top: 3, end: 5),
          //   animationDuration: Duration(milliseconds: 300),
          //   animationType: BadgeAnimationType.slide,
          //   badgeContent: Text(
          //     '2',
          //     style: TextStyle(color: Colors.white),
          //   ),
          //   child: IconButton(
          //       icon: ImageLoad(
          //         img: "/mobile_app_v2/o_notification_appbar.png",
          //         width: 30,
          //       ),
          //       onPressed: () {
          //         print(gloablSelectedGroups);
          //       }),
          // ),
          SourceGlobalOptions(globalSelectedTab: globalSelectedTab),
        ];
        break;
      case 3:
        return [
          // Badge(
          //   badgeColor: orange,
          //   position: BadgePosition.topEnd(top: 3, end: 5),
          //   animationDuration: Duration(milliseconds: 300),
          //   animationType: BadgeAnimationType.slide,
          //   badgeContent: Text(
          //     '2',
          //     style: TextStyle(color: Colors.white),
          //   ),
          //   child: IconButton(
          //       icon: ImageLoad(
          //         img: "/mobile_app_v2/o_notification_appbar.png",
          //         width: 30,
          //       ),
          //       onPressed: () {
          //         print(gloablSelectedGroups);
          //       }),
          // ),
          CompanyGlobalOptions(globalSelectedTab: globalSelectedTab),
        ];
        break;
      case 4:
        return [
          // Badge(
          //   badgeColor: orange,
          //   position: BadgePosition.topEnd(top: 3, end: 5),
          //   animationDuration: Duration(milliseconds: 300),
          //   animationType: BadgeAnimationType.slide,
          //   badgeContent: Text(
          //     '2',
          //     style: TextStyle(color: Colors.white),
          //   ),
          //   child: IconButton(
          //       icon: ImageLoad(
          //         img: "/mobile_app_v2/o_notification_appbar.png",
          //         width: 30,
          //       ),
          //       onPressed: () {
          //         print(gloablSelectedGroups);
          //       }),
          // ),
          ListGlobalOptions(globalSelectedTab: globalSelectedTab),
        ];
        break;
      default:
        return [Container()];
    }
  }

  void callSetState() {
    setState(() {
      gloablSelectedContacts = "";
      gloablSelectedGroups = "";
      gloablSelectedCompanies = "";
      gloablSelectedSources = "";
      gloablSelectedLists = "";
    }); // it can be called without parameters. It will redraw based on changes done in _SecondWidgetState
  }

  String contactHeader(data) {
    switch (data) {
      case 0:
        return "Contacts";
        break;
      case 1:
        return "Groups";
        break;
      case 2:
        return "Sources";
        break;
      case 3:
        return "Companies";
        break;
      case 4:
        return "Lists";
        break;
      default:
        return "Unknown Header";
    }
  }
}

class TabSection extends StatefulWidget {
  final Function onClick;
  TabSection(this.onClick);

  @override
  _TabSectionState createState() => _TabSectionState();
}

class _TabSectionState extends State<TabSection>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  int selectedTab = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: contactTabs.length, vsync: this);
    _tabController.addListener(() {
      globalSelectedTab = _tabController.index;
      widget.onClick();
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: contactTabs.length,
      initialIndex: selectedTab,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
            child: TabBar(
              controller: _tabController,
              isScrollable: true,
              indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 4.0, color: orange),
                insets: EdgeInsets.symmetric(horizontal: 16.0),
              ),
              tabs: contactTabs.map((tab) {
                return Container(
                  height: 35,
                  child: Tab(text: tab),
                );
              }).toList(),
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: contactTabWidgets
                  .map((widget) => TabBody(tabPage: widget))
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}

List contactTabs = [
  "Contacts",
  "Groups",
  "Sources",
  "Companies",
  "Lists",
];

final ContactRepository contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

List<Widget> contactTabWidgets = [
  ContactHome(),
  GroupHome(),
  SourceHome(),
  CompanyHome(),
  ListHome(),
];
