import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future groupDelete(BuildContext context, String groupId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return BlocProvider(
            create: (context) =>
                GroupDeleteBloc(contactRepository: _contactRepository),
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_delete.png",
                  title: "Delete Group"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'contact_group_id': groupId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'contact_group_id'),
                          Text(
                            "Do you really want to delete this group ?",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<GroupDeleteBloc, GroupDeleteState>(
                listener: (context, state) {
                  if (state is GroupDeleteSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});
                  }
                  if (state is GroupDeleteFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<GroupDeleteBloc, GroupDeleteState>(
                  builder: (context, state) {
                    return state is GroupDeleteInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<GroupDeleteBloc>(context).add(
                                    GroupDeletePressed(eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
