import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactGroupListBloc _contactGroupListBloc;

String gloablSelectedGroups = "";

class GroupHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GroupBody();
  }
}

class GroupBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: tabBodyPadd,
      child: RefreshIndicator(
        onRefresh: () async {
          _contactGroupListBloc.add(ContactGroupListRefresh(
              filterHeader: "group", filterOption: "all"));
        },
        color: purple,
        child: Column(
          children: [
            SearchButton(
                text: "Search Group",
                onClick: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => GroupGlobalSearch(),
                    ),
                  );
                }),
            SizedBox(height: 8),
            Expanded(
              child: GroupsData(),
            ),
          ],
        ),
      ),
    );
  }
}

class GroupsData extends StatefulWidget {
  GroupsData({Key key}) : super(key: key);

  @override
  _GroupsDataState createState() => _GroupsDataState();
}

class _GroupsDataState extends State<GroupsData> {
  MultiSelectController mlController = new MultiSelectController();

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  List selectedItems = [];

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _contactGroupListBloc = BlocProvider.of<ContactGroupListBloc>(context);
    _contactGroupListBloc.add(
        ContactGroupListRefresh(filterHeader: "group", filterOption: "all"));
    mlController.disableEditingWhenNoneSelected = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        BlocListener<ContactGroupListBloc, ContactGroupListState>(
          listener: (context, state) {
            if (state is ContactGroupListInitial) {
              setState(() {
                gloablSelectedGroups = "";
                mlController.deselectAll();
                selectedItems = [];
              });
            }
          },
          child: BlocBuilder<ContactGroupListBloc, ContactGroupListState>(
            builder: (context, state) {
              if (state is ContactGroupListFailure) {
                return FailedToLoad(message: state.error);
              }
              if (state is ContactGroupListSuccess) {
                return state.contacts.isEmpty
                    ? NoDataFound(message: "No Data Found")
                    : ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (context, index) {
                          bool last = state.contacts.length == (index + 1);
                          return index >= state.contacts.length
                              ? state.currentState.length >=
                                      Environment.dataDisplayCount
                                  ? LoaderSmallIndicator()
                                  : Container()
                              : MultiSelectItem(
                                  isSelecting: mlController.isSelecting,
                                  onSelected: () {
                                    setState(() {
                                      mlController.toggle(index);
                                      selectedItems =
                                          mlController.selectedIndexes;

                                      final List contactIdStrings = [];
                                      selectedItems
                                          .map((e) => contactIdStrings
                                              .add(state.contacts[e].gId))
                                          .toList();
                                      gloablSelectedGroups =
                                          contactIdStrings.join(',');
                                    });
                                  },
                                  child: GroupCard(
                                    data: state.contacts[index],
                                    isActive: mlController.isSelected(index),
                                    last: last,
                                  ),
                                );
                        },
                        itemCount: state.hasReachedMax
                            ? state.contacts.length
                            : state.contacts.length + 1,
                        controller: _scrollController,
                      );
              }
              return LoaderIndicator();
            },
          ),
        ),
        selectedItems.isEmpty
            ? Container()
            : Positioned(
                bottom: 0,
                left: -10,
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: purpleTtW,
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.8, 0.8),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                mlController.deselectAll();
                                gloablSelectedGroups = "";
                              });
                            },
                            child: Icon(Icons.close)),
                      ),
                      Text("You have selected ${selectedItems.length} Groups"),
                    ],
                  ),
                ),
              ),
        Positioned(
          bottom: 0,
          right: -10,
          child: FloatingButtonCustome(
            onPressed: () {
              groupAdd(context).then((value) {
                if (value != null) {
                  if (value["status"] == "success") {
                    successAlert(context, value["data"]);
                    _contactGroupListBloc.add(ContactGroupListRefresh(
                        filterHeader: "group", filterOption: "all"));
                  } else if (value["status"] == "failure") {
                    errorAlert(context, value["data"]);
                  }
                }
              });
            },
            icon: Icon(
              Icons.add,
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _contactGroupListBloc.add(
          ContactGroupListFetched(filterHeader: "group", filterOption: "all"));
    }
  }
}

class GroupCard extends StatelessWidget {
  final ContactGroupList data;
  final bool isActive;
  final bool last;
  const GroupCard({Key key, this.data, this.isActive, this.last})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: white,
      elevation: 0,
      margin: last ? EdgeInsets.only(bottom: 50) : EdgeInsets.only(bottom: 10),
      child: Container(
        padding: cardPadd,
        child: Row(
          children: [
            Container(
              width: 40,
              height: 40,
              child: isActive
                  ? Icon(Icons.done, color: white)
                  : Center(
                      child: ImageLoad(
                        img: "/mobile_app_v2/o_group_contact.png",
                        width: 30,
                      ),
                    ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: isActive ? purple : data.color.withOpacity(0.5),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${data.name.capitalize()}",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Text(
                      "No. of Contacts  ${data.count} | ${data.spoc} |  ${data.createDate}",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 12)),
                ],
              ),
            ),
            PopupMenuButton(
              onSelected: (value) {
                print(value);
                switch (value) {
                  case "deleteGroup":
                    groupDelete(context, data.gId).then((value) {
                      if (value != null) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          _contactGroupListBloc.add(ContactGroupListRefresh(
                              filterHeader: "group", filterOption: "all"));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      }
                    });
                    break;
                  case "editGroup":
                    groupEdit(context, data).then((value) {
                      if (value != null) {
                        if (value["status"] == "success") {
                          successAlert(context, value["data"]);
                          _contactGroupListBloc.add(ContactGroupListRefresh(
                              filterHeader: "group", filterOption: "all"));
                        } else if (value["status"] == "failure") {
                          errorAlert(context, value["data"]);
                        }
                      }
                    });
                    break;
                  case "viewContacts":
                    if (data.count != 0) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GroupContactsView(
                                id: data.groupId,
                                filterHeader: "group",
                                name: data.name)),
                      );
                    } else {
                      errorAlert(context, "No Contacts in this Group");
                    }

                    break;
                  default:
                }
              },
              itemBuilder: (context) => cardGroupOptions.map(
                (e) {
                  if (e.id == "deleteGroup") {
                    if (data.createdBy != "0") {
                      return PopupMenuItem(
                        value: e.id,
                        child: Row(
                          children: [
                            ImageLoad(img: e.img, width: 25),
                            SizedBox(width: 15),
                            Text(e.title),
                          ],
                        ),
                      );
                    }
                  }
                  if (e.id == "editGroup") {
                    if (data.createdBy != "0") {
                      return PopupMenuItem(
                        value: e.id,
                        child: Row(
                          children: [
                            ImageLoad(img: e.img, width: 25),
                            SizedBox(width: 15),
                            Text(e.title),
                          ],
                        ),
                      );
                    }
                  }
                  if (e.id == "viewContacts") {
                    return PopupMenuItem(
                      value: e.id,
                      child: Row(
                        children: [
                          ImageLoad(img: e.img, width: 25),
                          SizedBox(width: 15),
                          Text(e.title),
                        ],
                      ),
                    );
                  }
                },
              ).toList(),
              icon: SizedBox(
                width: 20,
                height: 20,
                child: ImageLoad(
                  img: '/mobile_app_v2/o_options_card.png',
                  width: 20,
                ),
              ),
              offset: Offset(0, 20),
            ),
          ],
        ),
      ),
    );
  }
}
