import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future listDelete(BuildContext context, String listId) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return BlocProvider(
            create: (context) =>
                ListDeleteBloc(contactRepository: _contactRepository),
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_delete.png",
                  title: "Delete List"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'list_id': listId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'list_id'),
                          Text(
                            "Do you really want to delete this List ?",
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<ListDeleteBloc, ListDeleteState>(
                listener: (context, state) {
                  if (state is ListDeleteSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});
                  }
                  if (state is ListDeleteFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<ListDeleteBloc, ListDeleteState>(
                  builder: (context, state) {
                    return state is ListDeleteInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<ListDeleteBloc>(context).add(
                                    ListDeletePressed(eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
