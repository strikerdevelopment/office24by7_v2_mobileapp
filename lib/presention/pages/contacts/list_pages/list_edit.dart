import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/contacts/contacts.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

final ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

Future listEdit(BuildContext context, ContactListList data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    ListEditBloc(contactRepository: _contactRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_edit.png", title: "Edit List"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        "list_name": data.name,
                        "list_id": data.lId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'list_id'),
                          FormBuilderTextField(
                            name: "list_name",
                            decoration: formGlobal("List Name"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<ListEditBloc, ListEditState>(
                listener: (context, state) {
                  if (state is ListEditSuccess) {
                    Navigator.pop(context,
                        {"status": "success", "data": state.stateData});
                  }
                  if (state is ListEditFailure) {
                    Navigator.pop(
                        context, {"status": "failure", "data": state.error});
                  }
                },
                child: BlocBuilder<ListEditBloc, ListEditState>(
                  builder: (context, state) {
                    return state is ListEditInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<ListEditBloc>(context)
                                    .add(ListEditPressed(eventData: formData));
                                print(formData);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
