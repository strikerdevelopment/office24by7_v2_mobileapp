import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class SourceGlobalOptions extends StatelessWidget {
  final int globalSelectedTab;
  const SourceGlobalOptions({Key key, this.globalSelectedTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (value) {
        print(value);
        switch (value) {
          case "newList":
            contactCreateList(context, gloablSelectedSources);
            break;
          case "addToList":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Sources")
                : contactAddToList(context, gloablSelectedSources, "source")
                    .then((value) {
                    if (value['status'] == "success") {
                      successAlert(context, value['data']);
                    } else if (value['status'] == "failure") {
                      errorAlert(context, value['data']);
                    }
                  });
            break;
          case "sms":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Groups")
                : contactSMS(context, gloablSelectedSources).then((value) {
                    if (value["status"] == "success") {
                      successAlert(context, value["data"]);
                      BlocProvider.of<ContactsBloc>(context).add(ContactRefresh(
                          filterHeader: "contact",
                          filterOption: contactDisplayAs));
                    } else if (value["status"] == "failure") {
                      errorAlert(context, value["data"]);
                    }
                  });
            break;
          case "email":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Groups")
                : contactEmail(context, gloablSelectedSources).then((value) {
                    if (value["status"] == "success") {
                      successAlert(context, value["data"]);
                      BlocProvider.of<ContactsBloc>(context).add(ContactRefresh(
                          filterHeader: "contact",
                          filterOption: contactDisplayAs));
                    } else if (value["status"] == "failure") {
                      errorAlert(context, value["data"]);
                    }
                  });
            break;
          case "voice":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Groups")
                : contactVoice(context, gloablSelectedSources).then((value) {
                    if (value["status"] == "success") {
                      successAlert(context, value["data"]);
                      BlocProvider.of<ContactsBloc>(context).add(ContactRefresh(
                          filterHeader: "contact",
                          filterOption: contactDisplayAs));
                    } else if (value["status"] == "failure") {
                      errorAlert(context, value["data"]);
                    }
                  });
            break;
          default:
        }
      },
      itemBuilder: (context) => cardSourceGlobalOptions.map((e) {
        return PopupMenuItem(
          value: e.id,
          child: Row(
            children: [
              ImageLoad(img: e.img, width: 25),
              SizedBox(width: 15),
              Text(e.title),
            ],
          ),
        );
      }).toList(),
      icon: SizedBox(
        width: 30,
        height: 30,
        child: ImageLoad(
          img: '/mobile_app_v2/o_moreoptions_appbar.png',
          width: 30,
        ),
      ),
      offset: Offset(0, 20),
    );
  }
}
