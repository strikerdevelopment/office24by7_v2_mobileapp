import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

SourceGlobalSearchBloc _sourceGlobalSearchBloc;

String sourceKeyword;

class SourceGlobalSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: BlocProvider(
          create: (context) =>
              SourceGlobalSearchBloc(contactRepository: _contactRepository)
                ..add(SourceGlobalSearchFetched(
                    filterHeader: "source", keyword: sourceKeyword)),
          child: SourceSearchData(),
        ),
      ),
    );
  }
}

class SourceSearchData extends StatefulWidget {
  @override
  _SourceSearchDataState createState() => _SourceSearchDataState();
}

class _SourceSearchDataState extends State<SourceSearchData> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppbarCurve(),
        AppbarNormal(
          title: "Global Source Search",
          actions: [
            SourceGlobalSearchOptions(globalSelectedTab: globalSelectedTab),
          ],
        ),
        BodyInner(
            page: Container(
          padding: tabBodyPadd,
          child: Column(
            children: [
              TextFormField(
                decoration: searchInput("Search"),
                keyboardType: TextInputType.text,
                onChanged: (value) {
                  setState(() {
                    sourceKeyword = value;
                  });

                  BlocProvider.of<SourceGlobalSearchBloc>(context).add(
                      SourceGlobalSearchRefresh(
                          filterHeader: "source", keyword: sourceKeyword));
                },
              ),
              SizedBox(height: 8),
              Expanded(
                child: SourceSearchBody(),
              ),
            ],
          ),
        )),
      ],
    );
  }
}

class SourceSearchBody extends StatefulWidget {
  SourceSearchBody({Key key}) : super(key: key);

  @override
  _SourceSearchBodyState createState() => _SourceSearchBodyState();
}

class _SourceSearchBodyState extends State<SourceSearchBody> {
  MultiSelectController mlController = new MultiSelectController();

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  List selectedItems = [];

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _sourceGlobalSearchBloc = BlocProvider.of<SourceGlobalSearchBloc>(context);
    mlController.disableEditingWhenNoneSelected = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        BlocListener<SourceGlobalSearchBloc, SourceGlobalSearchState>(
          listener: (context, state) {
            if (state is SourceGlobalSearchInitial) {
              setState(() {
                mlController.deselectAll();
                selectedItems = [];
                gloablSelectedContacts = "";
              });
            }
          },
          child: BlocBuilder<SourceGlobalSearchBloc, SourceGlobalSearchState>(
            builder: (context, state) {
              if (state is SourceGlobalSearchFailure) {
                return FailedToLoad(message: state.error);
              }
              if (state is SourceGlobalSearchSuccess) {
                return state.contacts.isEmpty
                    ? NoDataFound(message: "No data found")
                    : ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (context, index) {
                          bool last = state.contacts.length == (index + 1);
                          return index >= state.contacts.length
                              ? state.currentState.length >=
                                      Environment.dataDisplayCount
                                  ? LoaderSmallIndicator()
                                  : Container()
                              : MultiSelectItem(
                                  isSelecting: mlController.isSelecting,
                                  onSelected: () {
                                    setState(() {
                                      mlController.toggle(index);
                                      selectedItems =
                                          mlController.selectedIndexes;

                                      final List contactIdStrings = [];
                                      selectedItems
                                          .map((e) => contactIdStrings
                                              .add(state.contacts[e].sId))
                                          .toList();
                                      gloablSelectedSources =
                                          contactIdStrings.join(',');
                                    });
                                  },
                                  child: SourceGlobalSearchCard(
                                    data: state.contacts[index],
                                    isActive: mlController.isSelected(index),
                                    last: last,
                                  ),
                                );
                        },
                        itemCount: state.hasReachedMax
                            ? state.contacts.length
                            : state.contacts.length + 1,
                        controller: _scrollController,
                      );
              }
              return LoaderIndicator();
            },
          ),
        ),
        selectedItems.isEmpty
            ? Container()
            : Positioned(
                bottom: 0,
                left: -10,
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: purpleTtW,
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.8, 0.8),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                mlController.deselectAll();
                                gloablSelectedContacts = "";
                              });
                            },
                            child: Icon(Icons.close)),
                      ),
                      Text("You have selected ${selectedItems.length} sources"),
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _sourceGlobalSearchBloc.add(SourceGlobalSearchFetched(
          filterHeader: "source", keyword: sourceKeyword));
    }
  }
}

class SourceGlobalSearchCard extends StatelessWidget {
  final ContactSourceList data;
  final bool isActive;
  final bool last;
  const SourceGlobalSearchCard({Key key, this.data, this.isActive, this.last})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: white,
      elevation: 0,
      margin: last ? EdgeInsets.only(bottom: 50) : EdgeInsets.only(bottom: 10),
      child: Container(
        padding: cardPadd,
        child: Row(
          children: [
            Container(
              width: 40,
              height: 40,
              child: isActive
                  ? Icon(Icons.done, color: white)
                  : Center(
                      child: ImageLoad(
                        img: "/mobile_app_v2/o_source_contact.png",
                        width: 30,
                      ),
                    ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: isActive ? purple : data.color.withOpacity(0.5),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    data.name.capitalize(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Text(
                      "No. of Contacts  ${data.count} | ${data.spoc} |  | ${data.createDate}",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 12)),
                ],
              ),
            ),
            PopupMenuButton(
              onSelected: (value) {
                print(value);
                switch (value) {
                  case "viewContacts":
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GroupContactsView(
                              id: data.sourceId,
                              filterHeader: "source",
                              name: data.name)),
                    );
                    break;
                  case "addToList":
                    contactAddToList(context, data.sourceId, "source")
                        .then((value) {
                      if (value['status'] == "success") {
                        successAlert(context, value['data']);
                      } else if (value['status'] == "failure") {
                        errorAlert(context, value['data']);
                      }
                    });
                    break;
                  default:
                }
              },
              itemBuilder: (context) => cardSourceOptions.map((e) {
                return PopupMenuItem(
                  value: e.id,
                  child: Row(
                    children: [
                      ImageLoad(img: e.img, width: 25),
                      SizedBox(width: 15),
                      Text(e.title),
                    ],
                  ),
                );
              }).toList(),
              icon: SizedBox(
                width: 20,
                height: 20,
                child: ImageLoad(
                  img: '/mobile_app_v2/o_options_card.png',
                  width: 20,
                ),
              ),
              offset: Offset(0, 20),
            ),
          ],
        ),
      ),
    );
  }
}

class SourceGlobalSearchOptions extends StatelessWidget {
  final int globalSelectedTab;
  const SourceGlobalSearchOptions({Key key, this.globalSelectedTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (value) {
        print(value);
        switch (value) {
          case "newList":
            contactCreateList(context, gloablSelectedSources).then((value) {
              if (value == "success") {
                successAlert(context, "List Created Successfully");
              } else if (value == "failure") {
                errorAlert(context, "Failed to Create to List");
              }
            });
            break;
          case "addToList":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Sources")
                : contactAddToList(context, gloablSelectedSources, "source")
                    .then((value) {
                    if (value['status'] == "success") {
                      successAlert(context, value['data']);
                    } else if (value['status'] == "failure") {
                      errorAlert(context, value['data']);
                    }
                  });
            break;
          case "sms":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Groups")
                : contactSMS(context, gloablSelectedSources).then((value) {
                    if (value["status"] == "success") {
                      successAlert(context, value["data"]);
                      BlocProvider.of<ContactsBloc>(context).add(ContactRefresh(
                          filterHeader: "contact",
                          filterOption: contactDisplayAs));
                    } else if (value["status"] == "failure") {
                      errorAlert(context, value["data"]);
                    }
                  });
            break;
          case "email":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Groups")
                : contactEmail(context, gloablSelectedSources).then((value) {
                    if (value["status"] == "success") {
                      successAlert(context, value["data"]);
                      BlocProvider.of<ContactsBloc>(context).add(ContactRefresh(
                          filterHeader: "contact",
                          filterOption: contactDisplayAs));
                    } else if (value["status"] == "failure") {
                      errorAlert(context, value["data"]);
                    }
                  });
            break;
          case "voice":
            gloablSelectedSources == ""
                ? errorAlert(context, "Please Select One or More Groups")
                : contactVoice(context, gloablSelectedSources).then((value) {
                    if (value["status"] == "success") {
                      successAlert(context, value["data"]);
                      BlocProvider.of<ContactsBloc>(context).add(ContactRefresh(
                          filterHeader: "contact",
                          filterOption: contactDisplayAs));
                    } else if (value["status"] == "failure") {
                      errorAlert(context, value["data"]);
                    }
                  });
            break;
          default:
        }
      },
      itemBuilder: (context) => cardSourceGlobalOptions.map((e) {
        return PopupMenuItem(
          value: e.id,
          child: Row(
            children: [
              ImageLoad(img: e.img, width: 25),
              SizedBox(width: 15),
              Text(e.title),
            ],
          ),
        );
      }).toList(),
      icon: SizedBox(
        width: 30,
        height: 30,
        child: ImageLoad(
          img: '/mobile_app_v2/o_moreoptions_appbar.png',
          width: 30,
        ),
      ),
      offset: Offset(0, 20),
    );
  }
}
