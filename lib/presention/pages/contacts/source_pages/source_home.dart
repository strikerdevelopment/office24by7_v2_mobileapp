import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multi_select_item/multi_select_item.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

ContactSourceListBloc _contactSourceListBloc;

String gloablSelectedSources = "";

class SourceHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SourceBody();
  }
}

class SourceBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: tabBodyPadd,
      child: RefreshIndicator(
        onRefresh: () async {
          _contactSourceListBloc.add(ContactSourceListRefresh(
              filterHeader: "source", filterOption: "all"));
        },
        color: purple,
        child: Column(
          children: [
            SearchButton(
                text: "Search Sources",
                onClick: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SourceGlobalSearch()),
                  );
                }),
            SizedBox(height: 8),
            Expanded(
              child: SourceData(),
            ),
          ],
        ),
      ),
    );
  }
}

class SourceData extends StatefulWidget {
  SourceData({Key key}) : super(key: key);

  @override
  _SourceDataState createState() => _SourceDataState();
}

class _SourceDataState extends State<SourceData> {
  MultiSelectController mlController = new MultiSelectController();

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  List selectedItems = [];

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _contactSourceListBloc = BlocProvider.of<ContactSourceListBloc>(context);
    _contactSourceListBloc.add(
        ContactSourceListRefresh(filterHeader: "source", filterOption: "all"));
    // _contactSourceListBloc.add(
    //     ContactSourceListFetched(filterHeader: "source", filterOption: "all"));
    mlController.disableEditingWhenNoneSelected = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        BlocListener<ContactSourceListBloc, ContactSourceListState>(
          listener: (context, state) {
            if (state is ContactSourceListInitial) {
              setState(() {
                gloablSelectedSources = "";
                mlController.deselectAll();
                selectedItems = [];
              });
            }
          },
          child: BlocBuilder<ContactSourceListBloc, ContactSourceListState>(
            builder: (context, state) {
              if (state is ContactSourceListFailure) {
                return FailedToLoad(message: state.error);
              }
              if (state is ContactSourceListSuccess) {
                return state.contacts.isEmpty
                    ? NoDataFound(message: "No Data Found")
                    : ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (context, index) {
                          bool last = state.contacts.length == (index + 1);
                          return index >= state.contacts.length
                              ? state.currentState.length >=
                                      Environment.dataDisplayCount
                                  ? LoaderSmallIndicator()
                                  : Container()
                              : MultiSelectItem(
                                  isSelecting: mlController.isSelecting,
                                  onSelected: () {
                                    setState(() {
                                      mlController.toggle(index);
                                      selectedItems =
                                          mlController.selectedIndexes;

                                      final List contactIdStrings = [];
                                      selectedItems
                                          .map((e) => contactIdStrings
                                              .add(state.contacts[e].sId))
                                          .toList();
                                      gloablSelectedSources =
                                          contactIdStrings.join(',');
                                    });
                                  },
                                  child: SourceCard(
                                    data: state.contacts[index],
                                    isActive: mlController.isSelected(index),
                                    last: last,
                                  ),
                                );
                        },
                        itemCount: state.hasReachedMax
                            ? state.contacts.length
                            : state.contacts.length + 1,
                        controller: _scrollController,
                      );
              }
              return LoaderIndicator();
            },
          ),
        ),
        selectedItems.isEmpty
            ? Container()
            : Positioned(
                bottom: 0,
                left: -10,
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: purpleTtW,
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.8, 0.8),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp,
                    ),
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                mlController.deselectAll();
                                gloablSelectedSources = "";
                              });
                            },
                            child: Icon(Icons.close)),
                      ),
                      Text("You have selected ${selectedItems.length} sources"),
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _contactSourceListBloc.add(ContactSourceListFetched(
          filterHeader: "source", filterOption: "all"));
    }
  }
}

class SourceCard extends StatelessWidget {
  final ContactSourceList data;
  final bool isActive;
  final bool last;
  const SourceCard({Key key, this.data, this.isActive, this.last})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: white,
      elevation: 0,
      margin: last ? EdgeInsets.only(bottom: 50) : EdgeInsets.only(bottom: 10),
      child: Container(
        padding: cardPadd,
        child: Row(
          children: [
            Container(
              width: 40,
              height: 40,
              child: isActive
                  ? Icon(Icons.done, color: white)
                  : Center(
                      child: ImageLoad(
                        img: "/mobile_app_v2/o_source_contact.png",
                        width: 30,
                      ),
                    ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: isActive ? purple : data.color.withOpacity(0.5),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    data.name.capitalize(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Text(
                      "No. of Contacts  ${data.count} | ${data.spoc} | ${data.createDate}",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 12)),
                ],
              ),
            ),
            PopupMenuButton(
              onSelected: (value) {
                print(value);
                switch (value) {
                  case "viewContacts":
                    if (data.count != 0) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GroupContactsView(
                                id: data.sourceId,
                                filterHeader: "source",
                                name: data.name)),
                      );
                    } else {
                      errorAlert(context, "No Contacts in this Source");
                    }

                    break;
                  case "addToList":
                    contactAddToList(context, data.sId, "source").then((value) {
                      if (value['status'] == "success") {
                        successAlert(context, value['data']);
                      } else if (value['status'] == "failure") {
                        errorAlert(context, value['data']);
                      }
                    });
                    break;
                  default:
                }
              },
              itemBuilder: (context) => cardSourceOptions.map((e) {
                return PopupMenuItem(
                  value: e.id,
                  child: Row(
                    children: [
                      ImageLoad(img: e.img, width: 25),
                      SizedBox(width: 15),
                      Text(e.title),
                    ],
                  ),
                );
              }).toList(),
              icon: SizedBox(
                width: 20,
                height: 20,
                child: ImageLoad(
                  img: '/mobile_app_v2/o_options_card.png',
                  width: 20,
                ),
              ),
              offset: Offset(0, 20),
            ),
          ],
        ),
      ),
    );
  }
}
