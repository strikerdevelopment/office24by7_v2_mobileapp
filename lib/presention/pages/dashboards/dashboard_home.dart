import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class DashBoardHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(title: "DashBoard Page"),
            BodyInner(
              page: Center(
                child: Text("DashBoard Page"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
