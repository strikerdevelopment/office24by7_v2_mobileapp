import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class HistoryDataDesign extends StatelessWidget {
  final String date;

  const HistoryDataDesign({Key key, this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Stack(
        children: [
          Positioned(
            top: 23,
            left: 22,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(width: 1, color: textGray)),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(5, 15, 5, 0),
            margin: EdgeInsets.only(left: 55),
            child: Text("$date"),
            color: white,
          ),
        ],
      ),
    );
  }
}

class HistoryStatusDesign extends StatelessWidget {
  final String type;
  final String time;
  final String description;
  final String image;

  const HistoryStatusDesign(
      {Key key, this.type, this.time, this.description, this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.only(top: 15, bottom: 5),
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Positioned(
              top: 22,
              left: 25,
              child: Container(
                width: 40,
                decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(width: 1, color: textGray)),
                ),
              ),
            ),
            Positioned(
              top: -23,
              left: 22,
              child: Container(
                width: 1,
                height: 40,
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(width: 1, color: textGray)),
                ),
              ),
            ),
            Positioned(
              top: 34,
              left: 22,
              child: Container(
                width: 1,
                height: 40,
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(width: 1, color: textGray)),
                ),
              ),
            ),
            Positioned(
              top: 18,
              left: 60,
              child: Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: dartpurple,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            Positioned(
              child: Container(
                width: 45,
                height: 45,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: bodyBG,
                  borderRadius: BorderRadius.circular(50),
                ),
                child: ImageLoad(
                  img: image,
                  width: 20,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 80),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    type,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 5),
                  customText(
                      type == 'Email'
                          ? "$time | click to see mail details"
                          : "$time | $description",
                      13,
                      black),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
