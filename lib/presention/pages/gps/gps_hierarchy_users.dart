import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/gps/gps.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future gpsHierarchyUsers(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => GpsHierarchyUsersBloc(
                    gpsRepository: GpsRepository(gpsAPIClient: GpsAPIClient()))
                  ..add(GpsHierarchyUsersPressed()),
              ),
            ],
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_user_history.png", title: "Users"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {},
                      child: Column(
                        children: [
                          BlocBuilder<GpsHierarchyUsersBloc,
                              GpsHierarchyUsersState>(
                            builder: (context, state) {
                              if (state is GpsHierarchyUsersSuccess) {
                                return FormBuilderDropdown(
                                  name: "user_id",
                                  decoration: formGlobal("Select User"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.userId,
                                          child: Text(e.firstName)))
                                      .toList(),
                                );
                              }
                              if (state is GpsHierarchyUsersFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderIndicator();
                            },
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: ModalFooter(
                firstButton: "Cancel",
                firstFun: () {
                  Navigator.pop(context);
                },
                secondButton: "Submit",
                secondFun: () async {
                  if (_fbKey.currentState.saveAndValidate()) {
                    Map formData = _fbKey.currentState.value;
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GpsUserLocations(
                          userId: formData['user_id'],
                        ),
                      ),
                    );
                    // Navigator.pop(context);
                  }
                },
              ),
            ),
          );
        },
      );
    },
  );
}
