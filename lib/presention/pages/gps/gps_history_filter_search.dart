import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future gpsHistoryFilterSearch(BuildContext context, String selectedUser,
    String selectedFromDate, String selectedToDate, String selectedEventType) {
  bool _isLoading = false;

  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => GpsHierarchyUsersBloc(
                    gpsRepository: GpsRepository(gpsAPIClient: GpsAPIClient()))
                  ..add(GpsHierarchyUsersPressed()),
              ),
              BlocProvider(
                create: (context) => GpsUserEventTypeBloc(
                    gpsRepository: GpsRepository(gpsAPIClient: GpsAPIClient()))
                  ..add(GpsUserEventTypePressed()),
              ),
            ],
            child: ModalBox(
              modalHieght: 400,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_filter.png",
                  title: "History Filter"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {},
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'fromDate',
                                  onChanged: (val) {
                                    setState(() => {
                                          selectedFromDate = val.toString(),
                                        });
                                  },
                                  inputType: InputType.date,
                                  decoration: formGlobal("From Date"),
                                  initialValue:
                                      DateTime.parse(selectedFromDate),
                                  lastDate: DateTime.now(),
                                  initialEntryMode:
                                      DatePickerEntryMode.calendar,
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                ),
                              ),
                              fieldWidth,
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'toDate',
                                  onChanged: (value) {
                                    setState(() => {
                                          selectedToDate = value.toString(),
                                        });
                                  },
                                  inputType: InputType.date,
                                  decoration: formGlobal("To Date"),
                                  initialValue: DateTime.parse(selectedToDate),
                                  firstDate: DateTime.parse(selectedFromDate),
                                  lastDate: DateTime.now(),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                ),
                              )
                            ],
                          ),
                          fieldDistance,
                          BlocListener<GpsHierarchyUsersBloc,
                              GpsHierarchyUsersState>(
                            listener: (context, state) {
                              if (state is GpsHierarchyUsersSuccess) {
                                setState(() => _isLoading = true);
                              }
                            },
                            child: BlocBuilder<GpsHierarchyUsersBloc,
                                GpsHierarchyUsersState>(
                              builder: (context, state) {
                                if (state is GpsHierarchyUsersSuccess) {
                                  return FormBuilderDropdown(
                                    name: "users",
                                    initialValue: selectedUser == ''
                                        ? null
                                        : selectedUser,
                                    decoration: formGlobal("Select User"),
                                    validator: FormBuilderValidators.compose([
                                      FormBuilderValidators.required(context)
                                    ]),
                                    items: state.stateData
                                        .map((e) => DropdownMenuItem(
                                            value: e.userId,
                                            child: Text(e.firstName)))
                                        .toList(),
                                  );
                                }
                                if (state is GpsHierarchyUsersFailure) {
                                  return Center(
                                    child: Container(
                                      child: Text(state.error),
                                    ),
                                  );
                                }
                                return LoaderSmallIndicator();
                              },
                            ),
                          ),
                          fieldDistance,
                          BlocBuilder<GpsUserEventTypeBloc,
                              GpsUserEventTypeState>(
                            builder: (context, state) {
                              if (state is GpsUserEventTypeSuccess) {
                                return FormBuilderDropdown(
                                  name: "eventType",
                                  initialValue: selectedEventType == ''
                                      ? null
                                      : selectedEventType,
                                  onChanged: (val) => {
                                    setState(() => {
                                          selectedEventType = val,
                                        })
                                  },
                                  decoration: formGlobal("Select Event Type"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((e) => DropdownMenuItem(
                                          value: e.name, child: Text(e.name)))
                                      .toList(),
                                );
                              }
                              if (state is GpsUserEventTypeFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderSmallIndicator();
                            },
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: ModalFooter(
                firstButton: "Cancel",
                firstFun: () {
                  Navigator.pop(context);
                },
                secondButton: "Submit",
                secondFun: _isLoading
                    ? () async {
                        if (_fbKey.currentState.saveAndValidate()) {
                          final formData = Map.of(_fbKey.currentState.value);
                          Navigator.pop(context, formData);
                        }
                      }
                    : null,
              ),
            ),
          );
        },
      );
    },
  );
}
