import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

GpsRepository _gpsRepository = GpsRepository(gpsAPIClient: GpsAPIClient());

String selectedEventType = 'All';

class GPSHome extends StatefulWidget {
  @override
  _GPSHomeState createState() => _GPSHomeState();
}

class _GPSHomeState extends State<GPSHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNotLeading(
              title: "GPSHome Page",
              actions: [
                IconButton(
                  icon: ImageLoad(
                    img: "/mobile_app_v2/o_person_appbar.png",
                    width: 30,
                  ),
                  onPressed: () {
                    gpsHierarchyUsers(context);
                  },
                ),
                IconButton(
                  icon: ImageLoad(
                    img: "/mobile_app_v2/o_moreoptions_appbar.png",
                    width: 30,
                  ),
                  onPressed: () {
                    gpsUserEventType(context, selectedEventType);
                  },
                ),
              ],
            ),
            BodyInner(
              page: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0),
                ),
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) =>
                          GpsAddLocationBloc(gpsRepository: _gpsRepository),
                    ),
                    BlocProvider(
                      create: (context) =>
                          GpsFetchLocationBloc(gpsRepository: _gpsRepository),
                    ),
                  ],
                  child: GpsMap(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
