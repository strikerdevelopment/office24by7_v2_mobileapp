import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:geolocator/geolocator.dart';

GpsRepository _gpsRepository = GpsRepository(gpsAPIClient: GpsAPIClient());

class GpsMap extends StatefulWidget {
  @override
  _GpsMapState createState() => _GpsMapState();
}

class _GpsMapState extends State<GpsMap> {
  GoogleMapController _googleMapController;

  @override
  void initState() {
    super.initState();
    _determinePosition();
  }

  locationIntervel() {
    Geolocator.getPositionStream(
      desiredAccuracy: LocationAccuracy.best,
      distanceFilter: 10,
    ).listen((Position position) {
      print('${position.latitude} ${position.longitude}');
      BlocProvider.of<GpsAddLocationBloc>(context).add(GpsAddLocationPressed(
        latitude: '${position.latitude}',
        longitude: '${position.longitude}',
        eventType: selectedEventType,
        deviceId: "007",
        phoneNumber: "8021111110",
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        MultiBlocListener(
          listeners: [
            BlocListener<GpsFetchLocationBloc, GpsFetchLocationState>(
              listener: (context, state) {
                if (state is GpsFetchLocationSuccess) {
                  _googleMapController.animateCamera(
                      CameraUpdate.newCameraPosition(CameraPosition(
                    target: LatLng(
                        state.stateData.latitude, state.stateData.longitude),
                    zoom: 12,
                  )));
                }
              },
            ),
            BlocListener<GpsAddLocationBloc, GpsAddLocationState>(
              listener: (context, state) {
                if (state is GpsAddLocationSuccess) {
                  // successAlert(context, state.stateData);
                }
              },
            ),
          ],
          child: BlocBuilder<GpsFetchLocationBloc, GpsFetchLocationState>(
            builder: (context, state) {
              if (state is GpsFetchLocationSuccess) {
                return GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(
                        state.stateData.latitude, state.stateData.longitude),
                    zoom: 12,
                  ),
                  markers: {
                    Marker(
                      markerId: MarkerId("marker_1"),
                      position: LatLng(
                          state.stateData.latitude, state.stateData.longitude),
                      icon: BitmapDescriptor.defaultMarkerWithHue(
                          BitmapDescriptor.hueViolet),
                    ),
                  },
                  onMapCreated: (controller) =>
                      _googleMapController = controller,
                  zoomControlsEnabled: false,
                  zoomGesturesEnabled: false,
                  myLocationButtonEnabled: false,
                );
              }
              if (state is GpsFetchLocationFailure) {
                return FailedToLoad(message: '${state.error}');
              }
              return LoaderSmallIndicator();
            },
          ),
        ),
        Positioned(
          bottom: 15,
          right: 0,
          child: Column(
            children: [
              FloatingActionButton(
                backgroundColor: white,
                heroTag: 1,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) => GpsLocationHistoryBloc(
                            gpsRepository: _gpsRepository)
                          ..add(GpsLocationHistoryPressed(
                              users: selectedUser,
                              fromDate: selectedFromDate,
                              toDate: selectedToDate,
                              eventType: selectedEventType)),
                        child: GpsUserHistory(),
                      ),
                    ),
                  );
                },
                child: ImageLoad(
                  img: '/mobile_app_v2/o_user_history.png',
                  width: 28,
                ),
              ),
              SizedBox(height: 15),
              FloatingActionButton(
                heroTag: 2,
                backgroundColor: white,
                onPressed: () {
                  BlocProvider.of<GpsFetchLocationBloc>(context)
                      .add(GpsFetchLocationRefreshed());
                },
                child: ImageLoad(
                  img: '/mobile_app_v2/o_refresh_map.png',
                  width: 28,
                ),
              ),
              // FloatingActionButton(
              //   heroTag: 2,
              //   backgroundColor: white,
              //   onPressed: () {
              //     BlocProvider.of<GpsAddLocationBloc>(context)
              //         .add(GpsAddLocationPressed(
              //       latitude: '37.4351081786595',
              //       longitude: '-122.10314881056547',
              //       eventType: selectedEventType,
              //       deviceId: "007",
              //       phoneNumber: "8021111110",
              //     ));
              //   },
              //   child: Icon(Icons.ac_unit),
              // ),
            ],
          ),
        ),
        // Positioned(
        //   top: 15,
        //   right: 0,
        //   left: 0,
        //   child: Container(
        //     child: locationRefresh != null
        //         ? Text(
        //             "${locationRefresh.latitude}, ${locationRefresh.longitude}")
        //         : Text("No location"),
        //   ),
        // ),
      ],
    );
  }

  @override
  void dispose() {
    _googleMapController.dispose();
    super.dispose();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    locationIntervel();
    BlocProvider.of<GpsFetchLocationBloc>(context)
        .add(GpsFetchLocationPressed());
    return await Geolocator.getCurrentPosition();
  }
}
