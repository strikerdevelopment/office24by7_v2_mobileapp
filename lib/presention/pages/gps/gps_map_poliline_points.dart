import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/gps_model/gps_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

String selectedPoint = '0';

class GpsMapPolilinePoints extends StatefulWidget {
  final List<GpsUserLocationModel> locationData;
  GpsMapPolilinePoints({Key key, this.locationData}) : super(key: key);

  @override
  _GpsMapPolilinePointsState createState() => _GpsMapPolilinePointsState();
}

class _GpsMapPolilinePointsState extends State<GpsMapPolilinePoints> {
  GoogleMapController _googleMapController;
  Set<Marker> markers;
  Set<Polyline> _polilines;
  BitmapDescriptor _markerIcon;
  int _polylineIdCounter = 0;

  @override
  void initState() {
    drawMarkers(widget.locationData);
    createPolylines(widget.locationData);
    selectedPoint = widget.locationData.first.gpsLocationId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        BodyInner(
          page: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
            ),
            child: GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                target: LatLng(widget.locationData.first.latitude,
                    widget.locationData.first.longitude),
                zoom: 15,
              ),
              markers: markers != null ? markers : {},
              polylines: _polilines != null ? _polilines : {},
              onMapCreated: (controller) => _googleMapController = controller,
              zoomControlsEnabled: false,
              myLocationButtonEnabled: false,
            ),
          ),
        ),
        Positioned(
          bottom: 10,
          left: 10,
          right: 10,
          child: Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(30.0),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 2),
                    blurRadius: 6.0,
                  )
                ],
              ),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: widget.locationData
                      .map((e) => InkWell(
                            onTap: () => showLocation(e),
                            child: Container(
                              margin: const EdgeInsets.all(15),
                              width: 15,
                              height: 15,
                              decoration: BoxDecoration(
                                color: selectedPoint == e.gpsLocationId
                                    ? purple
                                    : textGray,
                                borderRadius: BorderRadius.circular(20.0),
                                boxShadow: const [
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0, 2),
                                    blurRadius: 6.0,
                                  )
                                ],
                              ),
                            ),
                          ))
                      .toList(),
                ),
              )),
        ),
      ],
    );
  }

  Future<void> drawMarkers(List<GpsUserLocationModel> data) async {
    await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(12, 12)),
            'assets/img/mobile_app_v2/red_square.png')
        .then((d) {
      setState(() {
        _markerIcon = d;
      });
    });

    final Set<Marker> setMarkers = {};
    data.forEach((e) {
      setMarkers.add(
        Marker(
            markerId: MarkerId('location${e.gpsLocationId}'),
            infoWindow: InfoWindow(title: "was here at ${e.gpsTime}"),
            icon: _markerIcon,
            position: LatLng(e.latitude, e.longitude)),
      );
    });
    setState(() {
      markers = setMarkers;
    });
  }

  void createPolylines(List<GpsUserLocationModel> data) {
    final Set<Polyline> setPolyline = {};
    setPolyline.add(Polyline(
      polylineId: PolylineId('polyline1'),
      consumeTapEvents: true,
      color: Colors.orange,
      patterns: [PatternItem.dash(10), PatternItem.gap(10)],
      width: 4,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
      points: _createPoints(data),
      onTap: () {},
    ));

    setState(() {
      _polilines = setPolyline;
    });
  }

  _createPoints(List<GpsUserLocationModel> data) {
    final List<LatLng> points = <LatLng>[];
    final double offset = _polylineIdCounter.ceilToDouble();
    data.forEach((e) {
      points.add(LatLng(e.latitude + offset, e.longitude));
    });
    return points;
  }

  void showLocation(GpsUserLocationModel e) {
    _googleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(e.latitude, e.longitude),
          zoom: 20,
          tilt: 10,
        ),
      ),
    );

    _googleMapController
        .showMarkerInfoWindow(MarkerId('location${e.gpsLocationId}'));

    setState(() {
      selectedPoint = e.gpsLocationId;
    });
  }

  @override
  void dispose() {
    _googleMapController.dispose();
    super.dispose();
  }
}
