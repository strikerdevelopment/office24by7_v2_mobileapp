import 'package:flutter/material.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/gps_model/gps_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class GpsMapPolilineRoute extends StatefulWidget {
  final List<GpsUserLocationModel> locationData;
  GpsMapPolilineRoute({Key key, this.locationData}) : super(key: key);

  @override
  _GpsMapPolilineRouteState createState() => _GpsMapPolilineRouteState();
}

class _GpsMapPolilineRouteState extends State<GpsMapPolilineRoute> {
  GoogleMapController _googleMapController;
  Marker _origin;
  Marker _destination;
  Directions _info;

  @override
  void initState() {
    if (widget.locationData.first != null)
      addOriginMarker(widget.locationData.first);
    if (widget.locationData.length > 1)
      addDestinationMarker(widget.locationData.last);
    if (widget.locationData.length > 1) addRoutes(widget.locationData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        BodyInner(
          page: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
            ),
            child: GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                target: LatLng(widget.locationData.first.latitude,
                    widget.locationData.first.longitude),
                zoom: 15,
              ),
              markers: {
                if (_origin != null) _origin,
                if (_destination != null) _destination,
              },
              onMapCreated: (controller) => _googleMapController = controller,
              zoomControlsEnabled: false,
              myLocationButtonEnabled: false,
              polylines: {
                if (_info != null)
                  Polyline(
                    polylineId: PolylineId('overview_polyline'),
                    color: red,
                    width: 5,
                    points: _info.polylinePoints
                        .map((e) => LatLng(e.latitude, e.longitude))
                        .toList(),
                  ),
              },
            ),
          ),
        ),
        Positioned(
          bottom: 15,
          right: 15,
          child: Column(
            children: [
              if (_origin != null)
                FloatingActionButton(
                  heroTag: 2,
                  backgroundColor: white,
                  onPressed: () {
                    _googleMapController.animateCamera(
                      CameraUpdate.newCameraPosition(
                        CameraPosition(
                          target: _origin.position,
                          zoom: 20,
                          tilt: 10,
                        ),
                      ),
                    );
                    _googleMapController
                        .showMarkerInfoWindow(MarkerId('origin'));
                  },
                  child: Icon(
                    Icons.first_page,
                    color: purple,
                  ),
                ),
              SizedBox(height: 15),
              if (_destination != null)
                FloatingActionButton(
                  heroTag: 2,
                  backgroundColor: white,
                  onPressed: () {
                    _googleMapController.animateCamera(
                      CameraUpdate.newCameraPosition(
                        CameraPosition(
                          target: _destination.position,
                          zoom: 20,
                          tilt: 10,
                        ),
                      ),
                    );
                    _googleMapController
                        .showMarkerInfoWindow(MarkerId('destination'));
                  },
                  child: Icon(
                    Icons.last_page,
                    color: purple,
                  ),
                ),
              SizedBox(height: 15),
              FloatingActionButton(
                heroTag: 2,
                backgroundColor: white,
                onPressed: () {
                  _googleMapController.animateCamera(
                    _info != null
                        ? CameraUpdate.newLatLngBounds(_info.bounds, 30)
                        : CameraUpdate.newCameraPosition(
                            CameraPosition(
                              target: _origin.position,
                              zoom: 20,
                              tilt: 10,
                            ),
                          ),
                  );
                },
                child: Icon(
                  Icons.center_focus_strong,
                  color: purple,
                ),
              ),
            ],
          ),
        ),
        if (_info != null)
          Positioned(
            bottom: 10,
            child: Container(
              padding: const EdgeInsets.symmetric(
                vertical: 6.0,
                horizontal: 12.0,
              ),
              decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 2),
                    blurRadius: 6.0,
                  )
                ],
              ),
              child: Text(
                'Distance: ${_info.totalDistance}, Duration: ${_info.totalDuration}',
                style: const TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
      ],
    );
  }

  void addOriginMarker(GpsUserLocationModel userLocation) {
    setState(() {
      _origin = Marker(
          markerId: MarkerId("origin"),
          infoWindow: InfoWindow(title: "was here at ${userLocation.gpsTime}"),
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          position: LatLng(userLocation.latitude, userLocation.longitude));
    });
  }

  void addDestinationMarker(GpsUserLocationModel userLocation) {
    setState(() {
      _destination = Marker(
          markerId: MarkerId("destination"),
          infoWindow: InfoWindow(title: "was here at ${userLocation.gpsTime}"),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          position: LatLng(userLocation.latitude, userLocation.longitude));
    });
  }

  void addRoutes(List<GpsUserLocationModel> userLocations) async {
    final directions =
        await GpsRepository(gpsAPIClient: GpsAPIClient()).getGpsGoogleDirection(
      userLocations.first.latitude,
      userLocations.first.longitude,
      userLocations.last.latitude,
      userLocations.last.longitude,
    );
    setState(() {
      _info = directions;
    });
  }

  @override
  void dispose() {
    _googleMapController.dispose();
    super.dispose();
  }
}
