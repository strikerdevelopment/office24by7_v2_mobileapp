import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/gps/gps.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

GpsRepository _gpsRepository = GpsRepository(gpsAPIClient: GpsAPIClient());

bool _switchToPolyline = true;

class GpsUserLocations extends StatefulWidget {
  final String userId;

  const GpsUserLocations({Key key, this.userId}) : super(key: key);

  @override
  _GpsUserLocationsState createState() => _GpsUserLocationsState();
}

class _GpsUserLocationsState extends State<GpsUserLocations> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          alignment: Alignment.center,
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Map History",
              actions: [
                TextButton(
                    onPressed: () {
                      setState(() {
                        _switchToPolyline = !_switchToPolyline;
                      });
                    },
                    child: Text(
                      _switchToPolyline
                          ? "Switch to points"
                          : "Switch to Rutes",
                      style: TextStyle(color: white),
                    ))
              ],
            ),
            BlocProvider(
              create: (context) =>
                  GpsUserLocationBloc(gpsRepository: _gpsRepository)
                    ..add(GpsUserLocationPressed(
                        users: widget.userId, fromDate: "", toDate: "")),
              child: BlocBuilder<GpsUserLocationBloc, GpsUserLocationState>(
                builder: (context, state) {
                  if (state is GpsUserLocationSuccess) {
                    return state.stateData.isEmpty
                        ? NoDataFound(message: 'No data found...')
                        : state.stateData.first.latitude != null
                            ? _switchToPolyline
                                ? GpsMapPolilineRoute(
                                    locationData: state.stateData)
                                : GpsMapPolilinePoints(
                                    locationData: state.stateData)
                            : FailedToLoad(
                                message:
                                    'Latitudes & Longitudes are not available');
                  }
                  if (state is GpsUserLocationFailure) {
                    return FailedToLoad(message: '${state.error}');
                  }
                  return LoaderSmallIndicator();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
