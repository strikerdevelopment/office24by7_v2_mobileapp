import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/gps/gps_home.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future gpsUserEventType(BuildContext context, String _selectedEventType) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => GpsUserEventTypeBloc(
                    gpsRepository: GpsRepository(gpsAPIClient: GpsAPIClient()))
                  ..add(GpsUserEventTypePressed()),
              ),
            ],
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_user_history.png",
                  title: "User Event Type"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {},
                      child: Column(
                        children: [
                          BlocBuilder<GpsUserEventTypeBloc,
                              GpsUserEventTypeState>(
                            builder: (context, state) {
                              if (state is GpsUserEventTypeSuccess) {
                                return FormBuilderRadioGroup<String>(
                                  name: "event_type",
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  initialValue: _selectedEventType,
                                  activeColor: purple,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  options: state.stateData
                                      .map((data) => FormBuilderFieldOption(
                                            value: data.name,
                                            // ignore: deprecated_member_use
                                            child: Text(data.name),
                                          ))
                                      .toList(growable: false),
                                );
                              }
                              if (state is GpsUserEventTypeFailure) {
                                return Center(
                                  child: Container(
                                    child: Text(state.error),
                                  ),
                                );
                              }
                              return LoaderIndicator();
                            },
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: ModalFooter(
                firstButton: "Cancel",
                firstFun: () {
                  Navigator.pop(context);
                },
                secondButton: "Submit",
                secondFun: () async {
                  if (_fbKey.currentState.saveAndValidate()) {
                    Map formData = _fbKey.currentState.value;
                    selectedEventType = formData['event_type'];
                    print(selectedEventType);
                    Navigator.pop(context);
                    successAlert(context, "Event updated successfully.");
                  }
                },
              ),
            ),
          );
        },
      );
    },
  );
}
