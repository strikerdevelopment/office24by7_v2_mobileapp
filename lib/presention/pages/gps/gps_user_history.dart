import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:office24by7_v2/_blocs/gps_bloc/gps_bloc.dart';
import 'package:office24by7_v2/_models/gps_model/gps_model.dart';
import 'package:office24by7_v2/presention/pages/globals/global.dart';
import 'package:office24by7_v2/presention/pages/gps/gps.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

// GpsRepository _gpsRepository = GpsRepository(gpsAPIClient: GpsAPIClient());
DateTime nowData = DateTime.now();
String selectedUser = '';
String selectedFromDate =
    DateTime(nowData.year, nowData.month - 1, nowData.day).toString();
String selectedToDate = DateTime.now().toString();
String selectedFilterEvent = 'All';

class GpsUserHistory extends StatefulWidget {
  @override
  _GpsUserHistoryState createState() => _GpsUserHistoryState();
}

class _GpsUserHistoryState extends State<GpsUserHistory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "User Location History",
              actions: [
                IconButton(
                  icon: ImageLoad(
                    img: "/mobile_app_v2/o_filter_appbar.png",
                    width: 30,
                  ),
                  onPressed: () {
                    gpsHistoryFilterSearch(
                            context,
                            selectedUser,
                            selectedFromDate,
                            selectedToDate,
                            selectedFilterEvent)
                        .then((value) {
                      setState(() => {
                            selectedUser = value['users'] == null
                                ? selectedUser
                                : value['users'],
                            selectedFilterEvent = value['eventType'] == null
                                ? selectedFilterEvent
                                : value['eventType'],
                            selectedFromDate = value['fromDate'].toString(),
                            selectedToDate = value['toDate'].toString(),
                          });

                      BlocProvider.of<GpsLocationHistoryBloc>(context).add(
                          GpsLocationHistoryPressed(
                              users: value['users'],
                              fromDate: value['fromDate'].toString(),
                              toDate: value['toDate'].toString(),
                              eventType: value['eventType']));
                    });
                  },
                ),
              ],
            ),
            BodyInner(
              page:
                  BlocBuilder<GpsLocationHistoryBloc, GpsLocationHistoryState>(
                builder: (context, state) {
                  if (state is GpsLocationHistorySuccess) {
                    return state.stateData.isEmpty
                        ? NoDataFound(message: 'No data found...')
                        : GpsUserHistoryBody(history: state.stateData);
                  }
                  if (state is GpsLocationHistoryFailure) {
                    return FailedToLoad(message: state.error);
                  }
                  return LoaderSmallIndicator();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GpsUserHistoryBody extends StatelessWidget {
  final List<GpsLocationHistoryModel> history;

  const GpsUserHistoryBody({Key key, this.history}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        padding: EdgeInsets.all(10),
        children: history
            .map(
              (e) => Column(
                children: [
                  HistoryDataDesign(
                      date: DateFormat('MMM dd, yyyy, kk:mm').format(e.date)),
                  Column(
                    children: e.historyFields
                        .map(
                          (e) => InkWell(
                            onTap: () {
                              gpsUserHistoryDetails(context, e);
                            },
                            child: Material(
                              color: white,
                              child: HistoryStatusDesign(
                                time: '${e.time}',
                                type: "${e.type}",
                                description: e.description,
                                image: "/mobile_app_v2/o_option_reassign.png",
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  )
                ],
              ),
            )
            .toList(),
      ),
    );
  }
}
