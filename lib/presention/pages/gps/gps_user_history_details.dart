import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future gpsUserHistoryDetails(BuildContext context, GpsHistoryField data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return ModalBox(
            modalHieght: 700,
            modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_option_contact_details.png",
                title: "User Location Details"),
            modalBody: Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(children: [
                  Column(
                    children: [
                      ListTile(
                        title: Text('User'),
                        subtitle: Text('${data.user}'),
                      ),
                      ListTile(
                        title: Text('Activity Type'),
                        subtitle: Text('${data.type}'),
                      ),
                      ListTile(
                        title: Text('Activity Time'),
                        subtitle: Text('${data.time}'),
                      ),
                      ListTile(
                          title: Text('Activity Description'),
                          subtitle: Html(data: data.description)),

                      // ListTile(
                      //   title: Text('Attachments'),
                      //   subtitle: Text(data.activityType),
                      // ),
                    ],
                  ),
                ]),
              ),
            ),
            modalFooter: ModalTextFooter(),
          );
        },
      );
    },
  );
}
