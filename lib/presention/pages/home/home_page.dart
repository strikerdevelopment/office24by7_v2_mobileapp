import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNotLeading(
              title: "Home Page",
              actions: [
                // action button
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {},
                ),
                // action button
                IconButton(
                  icon: Icon(Icons.filter_list),
                  onPressed: () {},
                ),
              ],
            ),
            BodyInner(
              page: Center(child: Text("Home Page")),
            ),
          ],
        ),
      ),
    );
  }
}
