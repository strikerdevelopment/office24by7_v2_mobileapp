import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/chat/chat_home.dart';
import 'package:office24by7_v2/presention/pages/gps/gps_home.dart';
import 'package:office24by7_v2/presention/pages/more/more_home.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:http/http.dart' as http;

LoginRepository _loginRepository =
    LoginRepository(loginApiClient: LoginApiClient(httpClient: http.Client()));

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  FCMNotificationService _fcmNotificationService = FCMNotificationService();

  @override
  void initState() {
    _fcmNotificationService.initialize();
    _fcmNotificationService.instantNotification();
    _fcmNotificationService.fcmOnTapListner(context);
    checkVersion(context);

    super.initState();
  }

  int _currentIndex = 0;

  final List<Widget> _children = [
    CallLogHome(),
    OpportunitiesHome(),
    GPSHome(),
    ChatHome(),
    BlocProvider(
      create: (context) => UserInfoBloc(loginRepository: _loginRepository)
        ..add(UserInfoPressed()),
      child: MoreHome(),
    ),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: _children.elementAt(_currentIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: _currentIndex == 0
                ? ImageLoad(img: "/mobile_app_v2/o_collog_blue.png", width: 35)
                : ImageLoad(img: "/mobile_app_v2/o_collog_gray.png", width: 35),
            label: 'Call Log',
          ),
          BottomNavigationBarItem(
            icon: _currentIndex == 1
                ? ImageLoad(img: "/mobile_app_v2/o_oppo_blue.png", width: 35)
                : ImageLoad(img: "/mobile_app_v2/o_oppo_gray.png", width: 35),
            label: 'Opportunities',
          ),
          BottomNavigationBarItem(
            icon: _currentIndex == 2
                ? ImageLoad(
                    img: "/mobile_app_v2/o_gps_tracking_blue.png", width: 35)
                : ImageLoad(
                    img: "/mobile_app_v2/o_gps_tracking_gray.png", width: 35),
            label: 'GPS',
          ),
          BottomNavigationBarItem(
            icon: _currentIndex == 3
                ? ImageLoad(img: "/mobile_app_v2/o_chat_blue.png", width: 35)
                : ImageLoad(img: "/mobile_app_v2/o_chat_gray.png", width: 35),
            label: 'Chat',
          ),
          BottomNavigationBarItem(
            icon: _currentIndex == 4
                ? ImageLoad(img: "/mobile_app_v2/o_more_blue.png", width: 35)
                : ImageLoad(img: "/mobile_app_v2/o_more_gray.png", width: 35),
            label: 'More',
          )
        ],
        backgroundColor: white,
        currentIndex: _currentIndex,
        selectedItemColor: dartpurple,
        unselectedItemColor: textGray,
        unselectedFontSize: 11,
        selectedFontSize: 12,
        onTap: onTabTapped,
      ),
    );
  }
}
