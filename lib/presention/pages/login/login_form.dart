import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/login/login.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:http/http.dart' as http;

bool forceLogoutState = false;
LoginBloc loginBloc;

final LoginRepository _loginRepository =
    LoginRepository(loginApiClient: LoginApiClient(httpClient: http.Client()));

final FirebaseRepository _firebaseRepository =
    FirebaseRepository(firebaseAPIClient: FirebaseAPIClient());

final AuthenticationBloc _authenticationBloc = AuthenticationBloc(
    loginRepository: _loginRepository, firebaseRepository: _firebaseRepository);

class LoginForm extends StatefulWidget {
  const LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  var data;
  bool autoValidate = true;
  bool readOnly = false;
  bool showSegmentedControl = true;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    loginBloc = BlocProvider.of<LoginBloc>(context);

    _loginPressed(_fbKey) {
      if (_fbKey.currentState.saveAndValidate()) {
        print(_fbKey.currentState.value['login_id']);

        loginBloc
            .add(LoginPressed(userID: _fbKey.currentState.value['login_id']));
      }
    }

    return FormBuilder(
      key: _fbKey,
      autovalidateMode: AutovalidateMode.always,
      child: SingleChildScrollView(
        child: BlocListener<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is LoginLoadFailure) {
              setState(() {
                forceLogoutState = true;
              });
              errorAlert(context, '${state.error}');
            }
            if (state is LoginLoadSuccess) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BlocProvider(
                    create: (context) => OtpBloc(
                        loginRepository: _loginRepository,
                        authenticationBloc: _authenticationBloc,
                        firebaseRepository: _firebaseRepository),
                    child: OtpPage(otpInfo: state.otpInfo),
                  ),
                ),
              );
              setState(() {
                forceLogoutState = false;
              });
            }
            if (state is LoginLoadInProgress) {
              setState(() {
                forceLogoutState = false;
              });
            }
          },
          child: BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) {
              return Column(
                children: [
                  FormBuilderTextField(
                    name: 'login_id',
                    decoration: loginInput("Enter Login ID"),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(context),
                    ]),
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(height: 20),
                  RaisedGradientButton(
                    child: buttonwhiteText('Sign In'),
                    gradient: LinearGradient(colors: purpleTtL),
                    onPressed: () =>
                        // ignore: unnecessary_statements
                        state is! LoginLoadInProgress
                            ? _loginPressed(_fbKey)
                            : null,
                  ),
                  Container(
                      child: state is LoginLoadInProgress
                          ? LoaderIndicator()
                          : null),
                  SizedBox(height: 50),
                  TextButton(
                    onPressed: () {
                      LaunchURL launchURL =
                          LaunchURL(url: 'https://office24by7.com/contact-us');
                      launchURL.launchURL();
                    },
                    child: Text("Contact for support"),
                  ),
                  BlocProvider(
                    create: (context) => ForceLogoutBloc(
                        loginRepository: LoginRepository(
                            loginApiClient:
                                LoginApiClient(httpClient: http.Client()))),
                    child: ForceLogout(
                      loginId: _fbKey.currentState.value['login_id'],
                      forceLogoutFlag: forceLogoutState,
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class ForceLogout extends StatefulWidget {
  final String loginId;
  bool forceLogoutFlag;
  ForceLogout({Key key, this.loginId, this.forceLogoutFlag}) : super(key: key);

  @override
  _ForceLogoutState createState() => _ForceLogoutState();
}

class _ForceLogoutState extends State<ForceLogout> {
  bool loadingState = false;

  @override
  void initState() {
    widget.forceLogoutFlag = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForceLogoutBloc, ForceLogoutState>(
      listener: (context, state) {
        if (state is ForceLogoutSuccess) {
          setState(() {
            widget.forceLogoutFlag = false;
            loadingState = false;
          });
          successAlert(context, '${state.stateData}');
        }
        if (state is ForceLogoutFailure) {
          setState(() {
            widget.forceLogoutFlag = true;
            loadingState = false;
          });
          errorAlert(context, '${state.error}');
        }
        if (state is ForceLogoutInProgress) {
          setState(() {
            loadingState = true;
          });
        }
      },
      child: widget.forceLogoutFlag
          ? loadingState
              ? LoaderSmallIndicator()
              : TextButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(red)),
                  onPressed: () {
                    BlocProvider.of<ForceLogoutBloc>(context)
                        .add(ForceLogoutPressed(loginId: widget.loginId));
                  },
                  child: Text(
                    "Force Logout",
                    style: TextStyle(color: white),
                  ),
                )
          : Container(),
    );
  }
}
