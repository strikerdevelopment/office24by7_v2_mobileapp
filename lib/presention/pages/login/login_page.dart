import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/presention/pages/login/login.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatelessWidget {
  final LoginRepository loginRepository = LoginRepository(
      loginApiClient: LoginApiClient(httpClient: http.Client()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            LoginCurve(),
            //AppbarNormal(),
            Positioned(
              top: safeAreaHeight(context),
              left: 0.0,
              right: 0.0,
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(250)),
                    child: Container(
                      padding: EdgeInsets.all(30),
                      decoration: roundShadow,
                      height: 200,
                      width: 200,
                      child: ImageLoad(img: '/mobile_app_v2/o_logo.png'),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: 290,
              left: 0.0,
              right: 0.0,
              bottom: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                height: MediaQuery.of(context).size.height - 290,
                child: BlocProvider(
                  create: (contex) =>
                      LoginBloc(loginRepository: loginRepository),
                  child: LoginForm(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
