import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/login/login.dart';
import 'package:office24by7_v2/_models/login/login_info.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

Timer _timer;
int _start = 0;

class OtpPage extends StatefulWidget {
  final OtpInfo otpInfo;

  OtpPage({Key key, this.otpInfo}) : super(key: key);

  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  void startTimer() {
    _start = 120;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  _launchURL() async {
    const url = 'https://office24by7.com/contact-us';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    startTimer();
    if (widget.otpInfo.loginid == "playstoretest") {
      BlocProvider.of<OtpBloc>(context).add(
        OtpWithoutPressed(
          loginInfo: LoginInfo(
              userId: widget.otpInfo.userid,
              userAuthToken: widget.otpInfo.userAuthToken,
              sessionName: widget.otpInfo.sessionName),
        ),
      );
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    OtpBloc otpBloc = BlocProvider.of<OtpBloc>(context);

    _otpPressed(pin) {
      OtpInfo otpInfoPost = OtpInfo(
        otpcode: pin,
        otpType: widget.otpInfo.otpType,
        userid: widget.otpInfo.userid,
        mobile: widget.otpInfo.mobile,
      );
      otpBloc.add(OtpPressed(otpInfo: otpInfoPost));
    }

    return Scaffold(
      body: BlocProvider(
        create: (context) => ResendOtpBloc(
            loginRepository: LoginRepository(
                loginApiClient: LoginApiClient(httpClient: http.Client()))),
        child: Container(
          color: bodyBG,
          child: Stack(
            children: [
              AppbarCurve(),
              AppbarNormal(title: "OTP Verification"),
              Positioned(
                top: safeAreaHeight(context),
                left: 0.0,
                right: 0.0,
                bottom: 0.0,
                child: Container(
                  padding:
                      EdgeInsets.only(top: 0, bottom: 15, left: 15, right: 15),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: bodyBox,
                    child: SingleChildScrollView(
                      child: BlocListener<OtpBloc, OtpState>(
                        listener: (context, state) {
                          if (state is OtpFailure) {
                            errorAlert(context,
                                "Invalid OTP, Please Contact to Support Team");
                          }
                          if (state is OtpSuccess) {
                            successAlert(context, "Successfully Loggedin...");

                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        IndexPage()),
                                (Route<dynamic> route) => route is IndexPage);
                          }
                        },
                        child: BlocBuilder<OtpBloc, OtpState>(
                            builder: (context, state) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 150),
                              Text(
                                  "Enter 6 digit OTP code, Sent to: ${widget.otpInfo.mobile}"),
                              SizedBox(height: 20),
                              // Text("Your OTP is: ${otpInfo.otpcode}"),
                              SizedBox(height: 20),
                              PinEntryTextField(
                                showFieldAsBox: true,
                                fields: 6,
                                onSubmit: (String pin) {
                                  state is! OtpInProgress
                                      ? _otpPressed(pin)
                                      // ignore: unnecessary_statements
                                      : null;
                                },
                              ),
                              Container(
                                child: state is OtpInProgress
                                    ? LoaderIndicator()
                                    : null,
                              ),
                              //SizedBox(height: 20),
                              //Text("01:35"),
                              SizedBox(height: 100),
                              BlocListener<ResendOtpBloc, ResendOtpState>(
                                listener: (context, state) {
                                  if (state is ResendOtpSuccess) {
                                    successAlert(context, state.stateData);
                                  }
                                  if (state is ResendOtpFailure) {
                                    successAlert(context, state.error);
                                  }
                                },
                                child:
                                    BlocBuilder<ResendOtpBloc, ResendOtpState>(
                                  builder: (context, state) {
                                    final minutesStr = ((_start / 60) % 60)
                                        .floor()
                                        .toString()
                                        .padLeft(2, '0');
                                    final secondsStr = (_start % 60)
                                        .floor()
                                        .toString()
                                        .padLeft(2, '0');

                                    if (state is ResendOtpInProgress) {
                                      return LoaderSmallIndicator();
                                    }
                                    return TextButton(
                                      onPressed: _start == 0
                                          ? () {
                                              startTimer();
                                              BlocProvider.of<ResendOtpBloc>(
                                                      context)
                                                  .add(ResendOtpPressed(
                                                      otpInfo: widget.otpInfo));
                                            }
                                          : null,
                                      child: _start == 0
                                          ? purpleText(
                                              "Did not receive an OTP?", 16)
                                          : purpleText(
                                              "$minutesStr:$secondsStr", 16),
                                    );
                                  },
                                ),
                              ),
                              SizedBox(height: 20),
                              Container(
                                width: 150,
                                child: ImageLoad(
                                  img: '/mobile_app_v2/o_logo_with_name.png',
                                ),
                              ),
                              SizedBox(height: 20),
                              TextButton(
                                onPressed: () {
                                  _launchURL();
                                },
                                child: Text("Contact for support"),
                              )
                            ],
                          );
                        }),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
