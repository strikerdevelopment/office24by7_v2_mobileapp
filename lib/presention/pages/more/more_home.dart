import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/login/login.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:package_info/package_info.dart';

class MoreHome extends StatefulWidget {
  @override
  _MoreHomeState createState() => _MoreHomeState();
}

class _MoreHomeState extends State<MoreHome> {
  int _selectedIndex;

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  _onSelected(int index) async {
    setState(() {
      _selectedIndex = index;
    });
    if (menuList[index].pageid != 'logout') {
      Navigator.pushNamed(context, menuList[index].router);
    } else if (menuList[index].pageid == 'logout') {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("userToken");
      String session = prefs.getString("userSession");

      BlocProvider.of<LoginBloc>(context)
          .add(LogoutPressed(token: token, session: session));

      BlocProvider.of<AuthenticationBloc>(context)
          .add(AuthenticationLoggedOut());
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => route is LoginPage);
    }
  }

  @override
  void initState() {
    _initPackageInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            //AppbarNormal(title: "MoreHome Page"),
            Positioned(
              top: safeAreaHeight(context) - 30,
              left: 0.0,
              right: 0.0,
              bottom: 0.0,
              child: Container(
                padding:
                    EdgeInsets.only(top: 0, bottom: 0, left: 10, right: 10),
                child: Container(
                  decoration: moreBox,
                  padding:
                      EdgeInsets.only(top: 10, bottom: 0, left: 10, right: 10),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
                        decoration: BoxDecoration(
                            border: Border(
                          bottom: BorderSide(width: 1, color: textGray),
                        )),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                              child: RoundImage(
                                  height: 50,
                                  width: 50,
                                  img: '/mobile_app_v2/o_profile_pic.png'),
                            ),
                            Flexible(
                                child: BlocBuilder<UserInfoBloc, UserInfoState>(
                              builder: (context, state) {
                                if (state is UserInfoFailure) {
                                  return profileText("${state.error}", 10);
                                }
                                if (state is UserInfoSuccess) {
                                  return Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      profileText(
                                          "${state.stateData.userInfo.firstName} ${state.stateData.userInfo.lastName}",
                                          18),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.phone,
                                            size: 14,
                                          ),
                                          Text(
                                            " ${state.stateData.userInfo.mobileNumber} | ",
                                          ),
                                          Icon(
                                            Icons.email,
                                            size: 14,
                                          ),
                                          SizedBox(width: 5),
                                          Flexible(
                                            child: Text(
                                              "${state.stateData.userInfo.emailId}",
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  );
                                }
                                return LinearProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation<Color>(purple),
                                  backgroundColor: purple.withOpacity(0.3),
                                );
                              },
                            )),
                          ],
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: menuList.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              leading: _selectedIndex != null &&
                                      _selectedIndex == index
                                  ? Container(
                                      width: 40,
                                      height: 40,
                                      padding: EdgeInsets.all(5),
                                      decoration: moreActiveListBox,
                                      child: ImageLoad(
                                          img: menuList[index].acitveImg),
                                    )
                                  : Container(
                                      width: 40,
                                      height: 40,
                                      padding: EdgeInsets.all(5),
                                      decoration: moreListBox,
                                      child:
                                          ImageLoad(img: menuList[index].img),
                                    ),
                              title: _selectedIndex != null &&
                                      _selectedIndex == index
                                  ? darkPurpleText(menuList[index].title, 16)
                                  : Text(menuList[index].title),
                              onTap: () => _onSelected(index),
                            );
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: Border(
                          top: BorderSide(width: 1, color: textGray),
                        )),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                "v${_packageInfo.version}",
                                style: TextStyle(fontSize: 10),
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                LaunchURL launchURL = LaunchURL(
                                    url:
                                        'https://office24by7.com/terms-of-services');
                                launchURL.launchURL();
                              },
                              child: Text("Legal"),
                            ),
                            TextButton(
                              onPressed: () {
                                LaunchURL launchURL =
                                    LaunchURL(url: 'https://office24by7.com/');
                                launchURL.launchURL();
                              },
                              child: Text("Settings"),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
