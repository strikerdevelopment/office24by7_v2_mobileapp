import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

final OppoRepository _oppoRepository =
    OppoRepository(oppoAPIClient: OppoAPIClient());

Future oppoActivityFilter(BuildContext context, OppoLoadData data) {
  DateTime nowData = DateTime.now();

  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => OppoLeadActivityFilterTypeBloc(
                    oppoRepository: _oppoRepository)
                  ..add(OppoLeadActivityFilterTypePressed()),
              ),
              BlocProvider(
                create: (context) =>
                    OppoLeadActivityBloc(oppoRepository: _oppoRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 400,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_filter.png",
                  title: "Lead Activity Filter"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {"lead_id": data.leadId},
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'lead_id'),
                          BlocBuilder<OppoLeadActivityFilterTypeBloc,
                              OppoLeadActivityFilterTypeState>(
                            builder: (context, state) {
                              if (state is OppoLeadActivityFilterTypeSuccess) {
                                return FormBuilderDropdown(
                                  name: "activity_type",
                                  decoration: formGlobal("Select Activity"),
                                  initialValue: 'All',
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((data) => DropdownMenuItem(
                                          value: data, child: Text(data)))
                                      .toList(),
                                );
                              }
                              if (state is OppoLeadActivityFilterTypeFailure) {
                                return Text("${state.error}");
                              }

                              return LoaderIndicator();
                            },
                          ),
                          fieldDistance,
                          FormBuilderDateTimePicker(
                            name: 'from_date',
                            onChanged: (value) {},
                            inputType: InputType.date,
                            decoration: formGlobal("From Date"),
                            validator: (val) => null,
                            initialValue: DateTime(
                                nowData.year, nowData.month - 1, nowData.day),
                            //firstDate: DateTime.now(),
                            lastDate: DateTime.now(),
                            initialEntryMode: DatePickerEntryMode.calendar,

                            // validator: FormBuilderValidators.compose(
                            //     [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          FormBuilderDateTimePicker(
                            name: 'to_date',
                            onChanged: (value) {},
                            inputType: InputType.date,
                            decoration: formGlobal("To Date"),
                            validator: (val) => null,
                            initialValue: nowData,
                            //firstDate: DateTime.now(),
                            lastDate: nowData,
                            initialEntryMode: DatePickerEntryMode.calendar,
                            // validator: FormBuilderValidators.compose(
                            //     [FormBuilderValidators.required(context)]),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: ModalFooter(
                firstButton: "Cancel",
                firstFun: () {
                  Navigator.pop(context);
                },
                secondButton: "Submit",
                secondFun: () async {
                  if (_fbKey.currentState.saveAndValidate()) {
                    Map formData = _fbKey.currentState.value;
                    // formData['from_date'] = formateDateTimeAPI(
                    //     _fbKey.currentState.value['from_date']);
                    // formData['to_date'] = formateDateTimeAPI(
                    //     _fbKey.currentState.value['to_date']);
                    print(formData);

                    Navigator.pop(context, formData);
                  }
                  //Navigator.pop(context);
                },
              ),
            ),
          );
        },
      );
    },
  );
}
