import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class OppotActivityData extends StatelessWidget {
  final OppoDetailsTabs oppoDetailsTabs;
  final OppoLoadData data;

  const OppotActivityData({Key key, this.oppoDetailsTabs, this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10),
      child: FloatingHistoryOppoTabButton(
        data: oppoDetailsTabs,
        onPressed: () {
          switch (oppoDetailsTabs.tabId) {
            case 'call':
              oppoCall(context, data).then((value) {
                if (value['status'] == 'success') {
                  successAlert(context, value['message']);
                } else {
                  errorAlert(context, value['message']);
                }
              });
              break;
            case 'text':
              oppoSMS(context);
              break;
            case 'email':
              oppoEmail(context, data);
              break;
            case 'task':
              oppoTask(context);
              break;
            // case 'note':
            //   contactNote(context, contactList);
            //   break;
            // case 'lead':
            //   contactAddToLead(context, contactList);
            //   break;
            // case 'ticket':
            //   contactTicket(context, contactList);
            //   break;
            // case 'history':
            //   contactCall(context, contactList);
            //   break;
          }
          print(oppoDetailsTabs.tabId);
        },
      ),
    );
  }
}
