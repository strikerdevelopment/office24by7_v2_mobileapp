import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/contacts/contacts.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

OppoRepository _oppoRepository = OppoRepository(oppoAPIClient: OppoAPIClient());

GenericRepository _genericRepository =
    GenericRepository(genericAPIClient: GenericAPIClient());

List<OppoFormFieldsModel> _formIds;

class OppoAddLead extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Add Lead",
            ),
            BodyInner(
              page: AddLeadForm(),
            ),
          ],
        ),
      ),
    );
  }
}

class AddLeadForm extends StatefulWidget {
  AddLeadForm({Key key}) : super(key: key);

  @override
  _AddLeadFormState createState() => _AddLeadFormState();
}

class _AddLeadFormState extends State<AddLeadForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  // final ValueChanged _onChanged = (val) => print(val);

  String divisionID;
  String statusActiveID;

  List<GetMoreFields> getPhoneNumbers = [];

  List<dynamic> multiListValues = [];

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: formpadding,
      children: [
        FormBuilder(
          key: _fbKey,
          autovalidateMode: AutovalidateMode.always,
          initialValue: {},
          child: Column(
            children: [
              BlocListener<OppoDivisionDdBloc, OppoDivisionDdState>(
                listener: (context, state) {
                  if (state is OppoDivisionDdFailure) {
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoDivisionDdBloc, OppoDivisionDdState>(
                    builder: (context, state) {
                  if (state is OppoDivisionDdSuccess) {
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderDropdown(
                        name: "division",
                        decoration: formGlobal("Select Division"),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        onChanged: (value) {
                          setState(() {
                            divisionID = value;
                          });

                          BlocProvider.of<OppoDivisionFormDdBloc>(context)
                            ..add(OppoDivisionFormDdPressed(divisionID: value));
                          BlocProvider.of<OppoDivisionUserDdBloc>(context)
                            ..add(OppoDivisionUserDdPressed(eventData: value));
                          BlocProvider.of<OppoDivisionSourceDdBloc>(context)
                            ..add(
                                OppoDivisionSourceDdPressed(divisionID: value));
                          BlocProvider.of<OppoDivisionStatusesDdBloc>(context)
                            ..add(OppoDivisionStatusesDdPressed(
                                eventData: value));
                          BlocProvider.of<OppoDivisionPriorityDdBloc>(context)
                            ..add(OppoDivisionPriorityDdPressed(
                                divisionID: value));
                        },
                        items: state.stateData
                            .map((data) => DropdownMenuItem(
                                value: data.divisionId,
                                child: Text(data.divisionName)))
                            .toList(),
                      ),
                    );
                  }
                  if (state is OppoDivisionDdFailure) {
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderTextField(
                        name: "division",
                        decoration: formGlobal("No divisions found"),
                        readOnly: true,
                      ),
                    );
                  }
                  return Container(
                    margin: fieldMargin,
                    child: FormBuilderTextField(
                      name: "division",
                      decoration: formGlobal("Loading..."),
                      readOnly: true,
                    ),
                  );
                }),
              ),
              BlocListener<OppoDivisionFormDdBloc, OppoDivisionFormDdState>(
                listener: (context, state) {
                  if (state is OppoDivisionFormDdFailure) {
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoDivisionFormDdBloc,
                    OppoDivisionFormDdState>(builder: (context, state) {
                  if (state is OppoDivisionFormDdSuccess) {
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderDropdown(
                        name: "custom_form_id",
                        decoration: formGlobal("Select Custom Form"),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        onChanged: (value) =>
                            BlocProvider.of<OppoFormFieldsBloc>(context)
                              ..add(OppoFormFieldsPressed(formID: value)),
                        items: state.stateData
                            .map((data) => DropdownMenuItem(
                                value: data.formId, child: Text(data.formDesc)))
                            .toList(),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
              ),
              BlocListener<OppoDivisionUserDdBloc, OppoDivisionUserDdState>(
                listener: (context, state) {
                  if (state is OppoDivisionUserDdFailure) {
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoDivisionUserDdBloc,
                    OppoDivisionUserDdState>(builder: (context, state) {
                  if (state is OppoDivisionUserDdSuccess) {
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderDropdown(
                        name: "assigned_to",
                        decoration: formGlobal("Select Assinged to"),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: state.stateData
                            .map((data) => DropdownMenuItem(
                                  value: data.userId,
                                  child: Text(data.userName),
                                ))
                            .toList(),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
              ),
              BlocListener<OppoFormFieldsBloc, OppoFormFieldsState>(
                listener: (context, state) {
                  if (state is OppoFormFieldsFailure) {
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoFormFieldsBloc, OppoFormFieldsState>(
                    builder: (context, state) {
                  if (state is OppoFormFieldsInProgress) {
                    return LoaderSmallIndicator();
                  }
                  if (state is OppoFormFieldsSuccess) {
                    _formIds = state.stateData;

                    return Column(
                      children: state.stateData.map((e) {
                        switch (e.fieldType) {
                          case "select":
                            switch (e.fieldName) {
                              case "source_id":
                                return BlocListener<OppoDivisionSourceDdBloc,
                                    OppoDivisionSourceDdState>(
                                  listener: (context, state) {
                                    if (state is OppoDivisionSourceDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionSourceDdBloc,
                                          OppoDivisionSourceDdState>(
                                      builder: (context, state) {
                                    if (state is OppoDivisionSourceDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          name: "source_id",
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          items: state.stateData
                                              .map((data) => DropdownMenuItem(
                                                  value: data.sourceId,
                                                  child: Text(data.sourceDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );

                                break;
                              case "status_id":
                                return BlocListener<OppoDivisionStatusesDdBloc,
                                    OppoDivisionStatusesDdState>(
                                  listener: (context, state) {
                                    if (state
                                        is OppoDivisionStatusesDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionStatusesDdBloc,
                                          OppoDivisionStatusesDdState>(
                                      builder: (context, state) {
                                    if (state
                                        is OppoDivisionStatusesDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          name: "status_id",
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          onChanged: (value) {
                                            setState(() {
                                              statusActiveID = value;
                                            });
                                            BlocProvider.of<
                                                    OppoDivisionStagesDdBloc>(
                                                context)
                                              ..add(OppoDivisionStagesDdPressed(
                                                  divisionID: divisionID,
                                                  statusID: value));
                                          },
                                          items: state.stateData
                                              .map((data) => DropdownMenuItem(
                                                  value: data.statusId,
                                                  child: Text(data.statusDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );
                                break;
                              case "stage_id":
                                return BlocListener<OppoDivisionStagesDdBloc,
                                    OppoDivisionStagesDdState>(
                                  listener: (context, state) {
                                    if (state is OppoDivisionStagesDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionStagesDdBloc,
                                          OppoDivisionStagesDdState>(
                                      builder: (context, state) {
                                    if (state is OppoDivisionStagesDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          name: "stage_id",
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          items: state.stateData
                                              .where((e) =>
                                                  e.statusId == statusActiveID)
                                              .toList()
                                              .map((data) => DropdownMenuItem(
                                                  value: data.stageId,
                                                  child: Text(data.stageDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );
                                break;
                              case "priority_id":
                                return BlocListener<OppoDivisionPriorityDdBloc,
                                    OppoDivisionPriorityDdState>(
                                  listener: (context, state) {
                                    if (state
                                        is OppoDivisionPriorityDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionPriorityDdBloc,
                                          OppoDivisionPriorityDdState>(
                                      builder: (context, state) {
                                    if (state
                                        is OppoDivisionPriorityDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          name: "priority_id",
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          items: state.stateData
                                              .map((data) => DropdownMenuItem(
                                                  value: data.priorityId,
                                                  child:
                                                      Text(data.priorityDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );
                                break;
                              case "country":
                                return BlocBuilder<CountryBloc, CountryState>(
                                  builder: (context, state) {
                                    if (state is CountrySuccess) {
                                      List<Country> countryData =
                                          state.countryData;
                                      Country country;
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          name: e.fieldName,
                                          onChanged: (value) {
                                            BlocProvider.of<StateBloc>(context)
                                                .add(StatePressed(
                                                    country: country.id));
                                          },
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          decoration:
                                              formGlobal("Select Country"),
                                          items: countryData
                                              .map(
                                                (data) => DropdownMenuItem(
                                                  value: data.name,
                                                  onTap: () {
                                                    setState(() {
                                                      country = data;
                                                    });
                                                  },
                                                  child: Text(data.name),
                                                ),
                                              )
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderTextField(
                                          name: e.fieldName,
                                          decoration:
                                              formGlobal("No countries found"),
                                          readOnly: true,
                                        ),
                                      );
                                    }
                                  },
                                );
                                break;
                              case "state":
                                return BlocListener<StateBloc, StateState>(
                                  listener: (context, state) {
                                    if (state is StateFailure) {
                                      errorAlert(context,
                                          "Getting Error in State, please contact to Support Team... ");
                                    }
                                  },
                                  child: BlocBuilder<StateBloc, StateState>(
                                    builder: (context, state) {
                                      if (state is StateSuccess) {
                                        List<StateModel> statesData =
                                            state.statesData;
                                        StateModel statedata;
                                        return Container(
                                          margin: fieldMargin,
                                          child: FormBuilderDropdown(
                                            name: e.fieldName,
                                            decoration:
                                                formGlobal("Select State"),
                                            onChanged: (value) {
                                              BlocProvider.of<CityBloc>(context)
                                                  .add(CityPressed(
                                                      state: statedata.id));
                                            },
                                            validator: e.isRequired == "1"
                                                ? FormBuilderValidators
                                                    .compose([
                                                    FormBuilderValidators
                                                        .required(context)
                                                  ])
                                                : null,
                                            items: statesData
                                                .map((data) => DropdownMenuItem(
                                                    value: data.name,
                                                    onTap: () {
                                                      setState(() {
                                                        statedata = data;
                                                      });
                                                    },
                                                    child: Text(data.name)))
                                                .toList(),
                                          ),
                                        );
                                      } else {
                                        return Container(
                                          margin: fieldMargin,
                                          child: FormBuilderTextField(
                                            name: e.fieldName,
                                            decoration:
                                                formGlobal("States not found"),
                                            readOnly: true,
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                );
                                break;
                              case "city":
                                return BlocListener<CityBloc, CityState>(
                                  listener: (context, state) {
                                    if (state is CityFailure) {
                                      errorAlert(context,
                                          "Getting Error in City, please contact to Support Team... ");
                                    }
                                  },
                                  child: BlocBuilder<CityBloc, CityState>(
                                    builder: (context, state) {
                                      if (state is CitySuccess) {
                                        List<City> cityData = state.cityData;
                                        return Container(
                                          margin: fieldMargin,
                                          child: FormBuilderDropdown(
                                            name: e.fieldName,
                                            validator: e.isRequired == "1"
                                                ? FormBuilderValidators
                                                    .compose([
                                                    FormBuilderValidators
                                                        .required(context)
                                                  ])
                                                : null,
                                            decoration:
                                                formGlobal("Select City"),
                                            items: cityData
                                                .map((data) => DropdownMenuItem(
                                                    value: data.name,
                                                    child: Text(data.name)))
                                                .toList(),
                                          ),
                                        );
                                      } else {
                                        return Container(
                                          margin: fieldMargin,
                                          child: FormBuilderTextField(
                                            name: e.fieldName,
                                            decoration:
                                                formGlobal("Cities not found"),
                                            readOnly: true,
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                );
                                break;
                              default:
                                return BlocProvider(
                                  create: (context) =>
                                      CustomDependentDropdownBloc(
                                          genericRepository: _genericRepository)
                                        ..add(CustomDependentDropdownPressed(
                                            formData: GenericModel(
                                          apiName: 'CustomFieldOptionsGet',
                                          fieldid: e.fieldId,
                                          parentFieldOptionId: "",
                                        ))),
                                  child: CustomDropdown(data: e),
                                );
                            }
                            break;
                          case "text":
                            switch (e.fieldName) {
                              case "date_and_time":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderDateTimePicker(
                                    name: e.fieldName,
                                    onChanged: (value) {},
                                    inputType: InputType.both,
                                    decoration: formGlobal(e.fieldLabel),
                                    firstDate: DateTime.now(),
                                    initialEntryMode:
                                        DatePickerEntryMode.calendar,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "date":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderDateTimePicker(
                                    name: e.fieldName,
                                    onChanged: (value) {},
                                    inputType: InputType.date,
                                    decoration: formGlobal(e.fieldLabel),
                                    //initialValue: DateTime.now(),
                                    firstDate: DateTime.now(),
                                    initialEntryMode:
                                        DatePickerEntryMode.calendar,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "time":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderDateTimePicker(
                                    name: e.fieldName,
                                    onChanged: (value) {},
                                    inputType: InputType.time,
                                    decoration: formGlobal(e.fieldLabel),
                                    //initialValue: DateTime.now(),
                                    firstDate: DateTime.now(),
                                    initialEntryMode:
                                        DatePickerEntryMode.calendar,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "phone_number":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderField(
                                    name: "phone_number",
                                    builder: (FormFieldState<dynamic> field) {
                                      return IntePhoneNumber(
                                        field: field,
                                        countries: ['IN'],
                                      );
                                    },
                                  ),
                                );
                                break;
                              case "budget":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderTextField(
                                    name: e.fieldName,
                                    decoration: formGlobal(e.fieldLabel),
                                    keyboardType: TextInputType.number,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "email_id":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderTextField(
                                    name: e.fieldName,
                                    decoration: formGlobal(e.fieldLabel),
                                    keyboardType: TextInputType.emailAddress,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context),
                                            FormBuilderValidators.email(context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              default:
                                switch (e.validationType) {
                                  case "datetime":
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderDateTimePicker(
                                        name: e.fieldName,
                                        onChanged: (value) {},
                                        inputType: InputType.both,
                                        decoration: formGlobal(e.fieldLabel),
                                        firstDate: DateTime.now(),
                                        initialEntryMode:
                                            DatePickerEntryMode.calendar,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                    break;
                                  case "date":
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderDateTimePicker(
                                        name: e.fieldName,
                                        onChanged: (value) {},
                                        inputType: InputType.date,
                                        decoration: formGlobal(e.fieldLabel),
                                        //initialValue: DateTime.now(),
                                        firstDate: DateTime.now(),
                                        initialEntryMode:
                                            DatePickerEntryMode.calendar,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                    break;
                                  case "time":
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderDateTimePicker(
                                        name: e.fieldName,
                                        onChanged: (value) {},
                                        inputType: InputType.time,
                                        decoration: formGlobal(e.fieldLabel),
                                        //initialValue: DateTime.now(),
                                        firstDate: DateTime.now(),
                                        initialEntryMode:
                                            DatePickerEntryMode.calendar,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                    break;
                                  default:
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderTextField(
                                        name: e.fieldName,
                                        decoration: formGlobal(e.fieldLabel),
                                        keyboardType: TextInputType.text,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                }
                            }
                            break;
                          case "radio":
                            return BlocProvider(
                              create: (context) => CustomDependentDropdownBloc(
                                  genericRepository: _genericRepository)
                                ..add(CustomDependentDropdownPressed(
                                    formData: GenericModel(
                                  apiName: 'CustomFieldOptionsGet',
                                  fieldid: e.fieldId,
                                  parentFieldOptionId: "",
                                ))),
                              child: BlocBuilder<CustomDependentDropdownBloc,
                                  CustomDependentDropdownState>(
                                builder: (context, state) {
                                  if (state is CustomDependentDropdownFailure) {
                                    return Container();
                                  }
                                  if (state is CustomDependentDropdownSuccess) {
                                    return FormBuilderRadioGroup<String>(
                                      decoration:
                                          formGlobalradiobox(e.fieldLabel),
                                      name: e.fieldName,
                                      onChanged: (value) {},
                                      activeColor: purple,
                                      focusColor: purple,
                                      validator: e.isRequired == "1"
                                          ? FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  context)
                                            ])
                                          : null,
                                      options: state.stateData
                                          .map(
                                            (data) => FormBuilderFieldOption(
                                              value: data.optionKey,
                                              child: Text(data.optionValue),
                                            ),
                                          )
                                          .toList(growable: true),
                                    );
                                  }
                                  return LoaderSmallIndicator();
                                },
                              ),
                            );
                            break;
                          case "textarea":
                            return Container(
                              margin: fieldMargin,
                              child: FormBuilderTextField(
                                name: e.fieldName,
                                decoration: formGlobal(e.fieldLabel),
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                validator: e.isRequired == "1"
                                    ? FormBuilderValidators.compose([
                                        FormBuilderValidators.required(context)
                                      ])
                                    : null,
                              ),
                            );
                            break;
                          case "checkbox":
                            return BlocProvider(
                              create: (context) => CustomDependentDropdownBloc(
                                  genericRepository: _genericRepository)
                                ..add(CustomDependentDropdownPressed(
                                    formData: GenericModel(
                                  apiName: 'CustomFieldOptionsGet',
                                  fieldid: e.fieldId,
                                  parentFieldOptionId: "",
                                ))),
                              child: BlocBuilder<CustomDependentDropdownBloc,
                                  CustomDependentDropdownState>(
                                builder: (context, state) {
                                  if (state is CustomDependentDropdownFailure) {
                                    return Container();
                                  }
                                  if (state is CustomDependentDropdownSuccess) {
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderCheckboxGroup<String>(
                                        activeColor: purple,
                                        decoration: formGlobalcheckboxLabel(
                                            e.fieldLabel),
                                        name: e.fieldName,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                        options: state.stateData
                                            .map(
                                              (data) => FormBuilderFieldOption(
                                                value: data.optionKey,
                                              ),
                                            )
                                            .toList(),
                                      ),
                                    );
                                  }
                                  return LoaderSmallIndicator();
                                },
                              ),
                            );
                            break;
                          case "multiselect":
                            return BlocProvider(
                              create: (context) => CustomDependentDropdownBloc(
                                  genericRepository: _genericRepository)
                                ..add(CustomDependentDropdownPressed(
                                    formData: GenericModel(
                                  apiName: 'CustomFieldOptionsGet',
                                  fieldid: e.fieldId,
                                  parentFieldOptionId: "",
                                ))),
                              child: BlocBuilder<CustomDependentDropdownBloc,
                                  CustomDependentDropdownState>(
                                builder: (context, state) {
                                  if (state is CustomDependentDropdownFailure) {
                                    return Container();
                                  }
                                  if (state is CustomDependentDropdownSuccess) {
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderCheckboxGroup<String>(
                                        activeColor: purple,
                                        decoration: formGlobalcheckboxLabel(
                                            e.fieldLabel),
                                        name: e.fieldName,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                        options: state.stateData
                                            .map(
                                              (data) => FormBuilderFieldOption(
                                                value: data.optionKey,
                                              ),
                                            )
                                            .toList(),
                                      ),
                                    );
                                  }
                                  return LoaderSmallIndicator();
                                },
                              ),
                            );
                            break;
                          default:
                            return Text("${e.fieldType} ==> Missed");
                        }
                      }).toList(),
                    );
                  } else {
                    return Container();
                  }
                }),
              ),
              fieldDistance,
              BlocProvider(
                create: (context) =>
                    OppoAddLeadBloc(oppoRepository: _oppoRepository),
                child: BlocListener<OppoAddLeadBloc, OppoAddLeadState>(
                  listener: (context, state) {
                    if (state is OppoAddLeadFailure) {
                      errorAlert(context, state.error);
                    }
                    if (state is OppoAddLeadSuccess) {
                      Navigator.pop(context, "success");
                    }
                  },
                  child: BlocBuilder<OppoAddLeadBloc, OppoAddLeadState>(
                    builder: (context, state) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          RaisedGradientButton(
                            child: customText('Reset', 16, black),
                            gradient: LinearGradient(colors: grayTtL),
                            width: 100,
                            onPressed: () {
                              _fbKey.currentState.reset();
                            },
                          ),
                          SizedBox(width: 10),
                          state is OppoAddLeadInProgress
                              ? LoaderIndicator()
                              : RaisedGradientButton(
                                  child: buttonwhiteText('Submit'),
                                  gradient: LinearGradient(colors: purpleTtL),
                                  width: 100,
                                  onPressed: () {
                                    //Navigator.pushNamed(context, '/contacts');

                                    if (_fbKey.currentState.saveAndValidate()) {
                                      final formData =
                                          Map.of(_fbKey.currentState.value);
                                      _formIds
                                          .map((e) => {
                                                if (e.fieldName ==
                                                        "date_and_time" ||
                                                    e.fieldName == "date" ||
                                                    e.fieldName == "time")
                                                  {
                                                    formData[e.fieldName] =
                                                        formData[e.fieldName]
                                                            .toString(),
                                                  },
                                                if (e.validationType ==
                                                        "datetime" ||
                                                    e.fieldName == "date" ||
                                                    e.fieldName == "time")
                                                  {
                                                    formData[e.fieldName] =
                                                        formData[e.fieldName]
                                                            .toString(),
                                                  }
                                              })
                                          .toList();
                                      BlocProvider.of<OppoAddLeadBloc>(context)
                                          .add(
                                        OppoAddLeadPressed(
                                          eventData: formData,
                                        ),
                                      );
                                    }
                                  },
                                ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

Map<String, BuildContext> dependencyContext = {};

class CustomDropdown extends StatefulWidget {
  final OppoFormFieldsModel data;

  const CustomDropdown({Key key, this.data}) : super(key: key);

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  @override
  Widget build(BuildContext context) {
    dependencyContext[widget.data.fieldId] = context;

    return Column(
      children: [
        BlocBuilder<CustomDependentDropdownBloc, CustomDependentDropdownState>(
          builder: (context, state) {
            if (state is CustomDependentDropdownFailure) {
              return Container();
            }
            if (state is CustomDependentDropdownSuccess) {
              return state.stateData.isEmpty
                  ? Container()
                  : Container(
                      margin: fieldMargin,
                      child: FormBuilderDropdown(
                        name: widget.data.fieldName,
                        decoration: formGlobal(widget.data.fieldLabel),
                        key: UniqueKey(),
                        items: state.stateData
                            .map((childdata) => DropdownMenuItem(
                                  value: widget.data.isDependent == "1"
                                      ? childdata.fieldOptionId
                                      : childdata.optionValue,
                                  child: Text(
                                    childdata.optionValue,
                                  ),
                                ))
                            .toList(),
                        onChanged: widget.data.isDependent == "1"
                            ? (value) {
                                BlocProvider.of<CustomDependentDropdownBloc>(
                                        dependencyContext[
                                            widget.data.chieldFieldId])
                                    .add(CustomDependentDropdownPressed(
                                        formData: GenericModel(
                                            apiName: 'CustomFieldOptionsGet',
                                            fieldid: widget.data.chieldFieldId,
                                            parentFieldOptionId: value)));
                              }
                            : null,
                      ),
                    );
            }
            return LoaderSmallIndicator();
          },
        ),
      ],
    );
  }
}
