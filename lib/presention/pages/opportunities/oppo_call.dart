import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

CallogRepository _callogRepository =
    CallogRepository(callogAPIClient: CallogAPIClient());

Future oppoCall(BuildContext context, OppoLoadData data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return BlocProvider(
        create: (context) =>
            ClickToCallBloc(callogRepository: _callogRepository),
        child: ModalBox(
          modalHieght: 250,
          modalHeader: ModalHeader(
              img: "/mobile_app_v2/o_call_modal.png", title: "Call"),
          modalBody: Expanded(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
              child: ListView(children: [
                Text("Are you sure yor want call to ${data.phoneNumber}",
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center),
              ]),
            ),
          ),
          modalFooter: BlocListener<ClickToCallBloc, ClickToCallState>(
            listener: (context, state) {
              if (state is ClickToCallSuccess) {
                Navigator.pop(
                    context, {"status": "success", "message": state.stateData});
              }
              if (state is ClickToCallFailure) {
                Navigator.pop(
                    context, {"status": "failure", "message": state.error});
              }
            },
            child: BlocBuilder<ClickToCallBloc, ClickToCallState>(
              builder: (context, state) {
                return state is ClickToCallInProgress
                    ? LoaderSmallIndicator()
                    : ModalFooter(
                        firstButton: "Cancel",
                        firstFun: () {
                          Navigator.pop(context);
                        },
                        secondButton: "Dial",
                        secondFun: () {
                          BlocProvider.of<ClickToCallBloc>(context).add(
                              ClickToCallPressed(
                                  leadId: data.leadId,
                                  phoneNumber: data.withoutnumbermasking));
                        },
                      );
              },
            ),
          ),
        ),
      );
    },
  );
}
