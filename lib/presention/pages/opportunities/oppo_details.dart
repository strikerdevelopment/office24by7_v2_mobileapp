import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/opportunities/oppo_details_tabs.dart';
import 'package:office24by7_v2/presention/pages/opportunities/opportunities.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class OppoDetails extends StatelessWidget {
  final OppoLoadData data;

  OppoDetails({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(title: "Lead Details"),
            BodyInner(page: OppoDetailsContent(data: data)),
          ],
        ),
      ),
    );
  }
}

class OppoDetailsContent extends StatelessWidget {
  final OppoLoadData data;
  const OppoDetailsContent({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: tabBodyPadd,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(250),
                  ),
                  child: Container(
                    decoration: roundShadow,
                    height: 50,
                    width: 50,
                    child: ImageLoad(img: '/mobile_app_v2/o_profile_pic.png'),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  profileText(data.fullName, 16),
                  Container(
                    width: 250,
                    child: customText(
                      "${data.sourceTypeDesc} - ${data.sourceDesc}",
                      14,
                      black,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          padding: tabBodyPadd,
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: textGray)),
          ),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: oppoDetailsTabs.map((tabData) {
                return OppoDetailsTabsData(
                  oppoDetailsTabs: tabData,
                  data: data,
                );
              }).toList(),
            ),
          ),
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.symmetric(vertical: 0),
            children: [
              ListTile(
                title: Text('Full name'),
                subtitle: Text(data.fullName),
              ),
              ListTile(
                title: Text('Phone Number'),
                subtitle: Text(data.phoneNumber),
              ),
              // ListTile(
              //   title: Text('Phone Name'),
              //   subtitle: Text("asdfadfasdf"),
              // ),
              ListTile(
                title: Text('Email'),
                subtitle: Text(data.emailId),
              ),
              ListTile(
                title: Text('Source Type'),
                subtitle: Text(data.sourceDesc),
              ),
              ListTile(
                title: Text('Status'),
                subtitle: Text(data.statusDesc),
              ),
              ListTile(
                title: Text('Priority'),
                subtitle: Text(data.priorityDesc),
              ),
              ListTile(
                title: Text('Assigned to User'),
                subtitle: Text(data.assignedToUser),
              ),
              ListTile(
                title: Text('Created by User'),
                subtitle: Text(data.createdByUser),
              ),
              ListTile(
                title: Text('Modified by User'),
                subtitle: Text(data.modifiedByUser),
              ),
              ListTile(
                title: Text('Modified by User'),
                subtitle: Text(data.modifiedByUser),
              ),
              ListTile(
                title: Text(dateType == "created_datetime"
                    ? 'Created Date'
                    : 'Modified Date'),
                subtitle: Text(formateDateTimeWidget(
                    dateType == "created_datetime"
                        ? data.createdDatetime
                        : data.modifiedDatetime)),
              ),
              ListTile(
                title: Text('Modified Date'),
                subtitle: Text(data.modifiedDatetime),
              ),
            ],
          ),
        ),
        //Center(child: Text("ChatHome Page ${contactInfo.contactId}")),
      ],
    );
  }
}
