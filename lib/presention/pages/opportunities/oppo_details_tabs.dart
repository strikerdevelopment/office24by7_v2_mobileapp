import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

final OppoRepository _oppoRepository =
    OppoRepository(oppoAPIClient: OppoAPIClient());

class OppoDetailsTabsData extends StatelessWidget {
  final OppoLoadData data;
  final OppoDetailsTabs oppoDetailsTabs;

  const OppoDetailsTabsData({Key key, this.oppoDetailsTabs, this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10, left: 10),
      child: FloatingTabOppoButton(
        data: oppoDetailsTabs,
        onPressed: () {
          switch (oppoDetailsTabs.tabId) {
            case 'call':
              oppoCall(context, data).then((value) {
                if (value['status'] == 'success') {
                  successAlert(context, value['message']);
                } else {
                  errorAlert(context, value['message']);
                }
              });
              break;
            case 'text':
              oppoSMS(context);
              break;
            case 'email':
              oppoEmail(context, data);
              break;
            case 'task':
              oppoTask(context);
              break;
            case 'note':
              oppoNote(context, data);
              break;
            // case 'lead':
            //   contactAddToLead(context);
            //   break;
            // case 'ticket':
            //   contactTicket(context);
            //   break;
            case 'history':
              // ContactRepository contactRepository =
              //     ContactRepository(contactAPIClient: ContactAPIClient());
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BlocProvider(
                    create: (context) =>
                        OppoLeadActivityBloc(oppoRepository: _oppoRepository)
                          ..add(OppoLeadActivityPressed(
                              leadID: data.leadId,
                              activityType: "All",
                              fromDate: "",
                              toDate: "")),
                    child: OppoLeadActivity(data: data),
                  ),
                ),
              );
              break;
          }

          print(oppoDetailsTabs.tabId);
        },
      ),
    );
  }
}
