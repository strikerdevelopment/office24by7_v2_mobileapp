import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
// import 'package:multiselect_formfield/multiselect_formfield.dart';

OppoRepository _oppoRepository = OppoRepository(oppoAPIClient: OppoAPIClient());

GenericRepository _genericRepository =
    GenericRepository(genericAPIClient: GenericAPIClient());

List<OppoFormFieldsModel> _formIds;

class OppoEditLead extends StatelessWidget {
  final OppoLoadData oppoLoadData;
  OppoEditLead({Key key, this.oppoLoadData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Edit Lead",
            ),
            BodyInner(
              page: EditLeadForm(
                oppoLoadData: oppoLoadData,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class EditLeadForm extends StatefulWidget {
  final OppoLoadData oppoLoadData;

  EditLeadForm({Key key, this.oppoLoadData}) : super(key: key);

  @override
  _EditLeadFormState createState() => _EditLeadFormState();
}

class _EditLeadFormState extends State<EditLeadForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  String divisionID;
  String statusActiveID;

  @override
  void initState() {
    BlocProvider.of<OppoDivisionFormDdBloc>(context)
      ..add(OppoDivisionFormDdPressed(
          divisionID: widget.oppoLoadData.divisionId));
    BlocProvider.of<OppoDivisionUserDdBloc>(context)
      ..add(
          OppoDivisionUserDdPressed(eventData: widget.oppoLoadData.divisionId));
    BlocProvider.of<OppoDivisionSourceDdBloc>(context)
      ..add(OppoDivisionSourceDdPressed(
          divisionID: widget.oppoLoadData.divisionId));
    BlocProvider.of<OppoDivisionStatusesDdBloc>(context)
      ..add(OppoDivisionStatusesDdPressed(
          eventData: widget.oppoLoadData.divisionId));

    BlocProvider.of<OppoDivisionPriorityDdBloc>(context)
      ..add(OppoDivisionPriorityDdPressed(
          divisionID: widget.oppoLoadData.divisionId));

    BlocProvider.of<OppoDivisionStagesDdBloc>(context)
      ..add(OppoDivisionStagesDdPressed(
          divisionID: widget.oppoLoadData.divisionId,
          statusID: widget.oppoLoadData.statusId));

    BlocProvider.of<OppoFormFieldsBloc>(context)
      ..add(OppoFormFieldsPressed(formID: widget.oppoLoadData.formId));

    BlocProvider.of<StateBloc>(context)
        .add(StatePressed(country: widget.oppoLoadData.state));
    BlocProvider.of<CityBloc>(context)
        .add(CityPressed(state: widget.oppoLoadData.city));
    statusActiveID = widget.oppoLoadData.statusId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: formpadding,
      children: [
        FormBuilder(
          key: _fbKey,
          autovalidateMode: AutovalidateMode.always,
          initialValue: {
            "leadrecord_id": widget.oppoLoadData.leadId,
          },
          child: Column(
            children: [
              FormBuilderHiddenField(value: 'leadrecord_id'),
              BlocListener<OppoDivisionDdBloc, OppoDivisionDdState>(
                listener: (context, state) {
                  if (state is OppoDivisionDdFailure) {
                    errorAlert(context, state.error);
                  }
                },
                
                child: BlocBuilder<OppoDivisionDdBloc, OppoDivisionDdState>(
                    builder: (context, state) {
                  if (state is OppoDivisionDdSuccess) {
                    // print(
                    //     "asdfa asdfad asdfadf ${widget.oppoLoadData.jsonData}");
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderDropdown(
                        name: "division",
                        initialValue:
                            widget.oppoLoadData.jsonData["division_id"],
                        decoration: formGlobal("Select Division"),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        onChanged: (value) {
                          setState(() {
                            divisionID = value;
                          });

                          BlocProvider.of<OppoDivisionFormDdBloc>(context)
                            ..add(OppoDivisionFormDdPressed(divisionID: value));
                          BlocProvider.of<OppoDivisionUserDdBloc>(context)
                            ..add(OppoDivisionUserDdPressed(eventData: value));
                          BlocProvider.of<OppoDivisionSourceDdBloc>(context)
                            ..add(
                                OppoDivisionSourceDdPressed(divisionID: value));
                          BlocProvider.of<OppoDivisionStatusesDdBloc>(context)
                            ..add(OppoDivisionStatusesDdPressed(
                                eventData: value));
                          BlocProvider.of<OppoDivisionPriorityDdBloc>(context)
                            ..add(OppoDivisionPriorityDdPressed(
                                divisionID: value));
                        },
                        items: state.stateData
                            .map((data) => DropdownMenuItem(
                                value: data.divisionId,
                                child: Text(data.divisionName)))
                            .toList(),
                      ),
                    );
                  } else {
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderTextField(
                        name: "division",
                        decoration: formGlobal("Loading..."),
                        readOnly: true,
                      ),
                    );
                  }
                }),
              ),
              BlocListener<OppoDivisionFormDdBloc, OppoDivisionFormDdState>(
                listener: (context, state) {
                  if (state is OppoDivisionFormDdFailure) {
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoDivisionFormDdBloc,
                    OppoDivisionFormDdState>(builder: (context, state) {
                  if (state is OppoDivisionFormDdSuccess) {
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderDropdown(
                        name: "custom_form_id",
                        initialValue:
                            widget.oppoLoadData.jsonData["form_id"] == null
                                ? null
                                : widget.oppoLoadData.jsonData["form_id"],
                        decoration: formGlobal("Select Custom Form"),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        onChanged: (value) =>
                            BlocProvider.of<OppoFormFieldsBloc>(context)
                              ..add(OppoFormFieldsPressed(formID: value)),
                        items: state.stateData
                            .map((data) => DropdownMenuItem(
                                value: data.formId, child: Text(data.formDesc)))
                            .toList(),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
              ),
              BlocListener<OppoDivisionUserDdBloc, OppoDivisionUserDdState>(
                listener: (context, state) {
                  if (state is OppoDivisionUserDdFailure) {
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoDivisionUserDdBloc,
                    OppoDivisionUserDdState>(builder: (context, state) {
                  if (state is OppoDivisionUserDdSuccess) {
                    return Container(
                      margin: fieldMargin,
                      child: FormBuilderDropdown(
                        name: "assigned_to",
                        initialValue:
                            widget.oppoLoadData.jsonData["assigned_to"] == null
                                ? null
                                : widget.oppoLoadData.jsonData["assigned_to"],
                        decoration: formGlobal("Select Assinged to"),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: state.stateData
                            .map((data) => DropdownMenuItem(
                                value: data.userId, child: Text(data.userName)))
                            .toList(),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
              ),
              BlocListener<OppoFormFieldsBloc, OppoFormFieldsState>(
                listener: (context, state) {
                  if (state is OppoFormFieldsFailure) {
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoFormFieldsBloc, OppoFormFieldsState>(
                    builder: (context, state) {
                  if (state is OppoFormFieldsInProgress) {
                    return LoaderSmallIndicator();
                  }
                  if (state is OppoFormFieldsSuccess) {
                    _formIds = state.stateData;
                    return Column(
                      children: state.stateData.map((e) {
                        switch (e.fieldType) {
                          case "select":
                            switch (e.fieldName) {
                              case "source_id":
                                return BlocListener<OppoDivisionSourceDdBloc,
                                    OppoDivisionSourceDdState>(
                                  listener: (context, state) {
                                    if (state is OppoDivisionSourceDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionSourceDdBloc,
                                          OppoDivisionSourceDdState>(
                                      builder: (context, state) {
                                    if (state is OppoDivisionSourceDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          initialValue: getInitialValue(
                                              widget.oppoLoadData.jsonData, e),
                                          name: e.fieldName,
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          items: state.stateData
                                              .map((data) => DropdownMenuItem(
                                                  value: data.sourceId,
                                                  child: Text(data.sourceDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );
                                break;
                              case "status_id":
                                return BlocListener<OppoDivisionStatusesDdBloc,
                                    OppoDivisionStatusesDdState>(
                                  listener: (context, state) {
                                    if (state
                                        is OppoDivisionStatusesDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionStatusesDdBloc,
                                          OppoDivisionStatusesDdState>(
                                      builder: (context, state) {
                                    if (state
                                        is OppoDivisionStatusesDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          initialValue: getInitialValue(
                                              widget.oppoLoadData.jsonData, e),
                                          name: e.fieldName,
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          onChanged: (value) {
                                            setState(() {
                                              statusActiveID = value;
                                            });
                                            BlocProvider.of<
                                                    OppoDivisionStagesDdBloc>(
                                                context)
                                              ..add(OppoDivisionStagesDdPressed(
                                                  divisionID: divisionID,
                                                  statusID: value));
                                          },
                                          items: state.stateData
                                              .map((data) => DropdownMenuItem(
                                                  value: data.statusId,
                                                  child: Text(data.statusDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );
                                break;
                              case "stage_id":
                                return BlocListener<OppoDivisionStagesDdBloc,
                                    OppoDivisionStagesDdState>(
                                  listener: (context, state) {
                                    if (state is OppoDivisionStagesDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionStagesDdBloc,
                                          OppoDivisionStagesDdState>(
                                      builder: (context, state) {
                                    if (state is OppoDivisionStagesDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          initialValue: getInitialValue(
                                              widget.oppoLoadData.jsonData, e),
                                          name: e.fieldName,
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          items: state.stateData
                                              .where((e) =>
                                                  e.statusId == statusActiveID)
                                              .toList()
                                              .map((data) => DropdownMenuItem(
                                                  value: data.stageId,
                                                  child: Text(data.stageDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );
                                break;
                              case "priority_id":
                                return BlocListener<OppoDivisionPriorityDdBloc,
                                    OppoDivisionPriorityDdState>(
                                  listener: (context, state) {
                                    if (state
                                        is OppoDivisionPriorityDdFailure) {
                                      errorAlert(context, state.error);
                                    }
                                  },
                                  child: BlocBuilder<OppoDivisionPriorityDdBloc,
                                          OppoDivisionPriorityDdState>(
                                      builder: (context, state) {
                                    if (state
                                        is OppoDivisionPriorityDdSuccess) {
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          initialValue: getInitialValue(
                                              widget.oppoLoadData.jsonData, e),
                                          name: e.fieldName,
                                          decoration: formGlobal(e.fieldLabel),
                                          validator: e.isRequired == "1"
                                              ? FormBuilderValidators.compose([
                                                  FormBuilderValidators
                                                      .required(context)
                                                ])
                                              : null,
                                          items: state.stateData
                                              .map((data) => DropdownMenuItem(
                                                  value: data.priorityId,
                                                  child:
                                                      Text(data.priorityDesc)))
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }),
                                );
                                break;
                              case "country":
                                return BlocBuilder<CountryBloc, CountryState>(
                                  builder: (context, state) {
                                    if (state is CountrySuccess) {
                                      List<Country> countryData =
                                          state.countryData;
                                      Country country;
                                      return Container(
                                        margin: fieldMargin,
                                        child: FormBuilderDropdown(
                                          initialValue: getInitialValue(
                                              widget.oppoLoadData.jsonData, e),
                                          name: e.fieldName,
                                          onChanged: (value) {
                                            BlocProvider.of<StateBloc>(context)
                                                .add(StatePressed(
                                                    country: country.id));
                                          },
                                          decoration:
                                              formGlobal("Select Country"),
                                          items: countryData
                                              .map(
                                                (data) => DropdownMenuItem(
                                                  value: data.name,
                                                  onTap: () {
                                                    setState(() {
                                                      country = data;
                                                    });
                                                  },
                                                  child: Text(data.name),
                                                ),
                                              )
                                              .toList(),
                                        ),
                                      );
                                    } else {
                                      return FormBuilderTextField(
                                        name: "country",
                                        decoration: formGlobal("Loading..."),
                                        readOnly: true,
                                      );
                                    }
                                  },
                                );
                                break;
                              case "state":
                                return BlocListener<StateBloc, StateState>(
                                  listener: (context, state) {
                                    if (state is StateFailure) {
                                      errorAlert(context,
                                          "Getting Error in State, please contact to Support Team... ");
                                    }
                                  },
                                  child: BlocBuilder<StateBloc, StateState>(
                                    builder: (context, state) {
                                      if (state is StateSuccess) {
                                        List<StateModel> statesData =
                                            state.statesData;
                                        StateModel statedata;
                                        return Container(
                                          margin: fieldMargin,
                                          child: FormBuilderDropdown(
                                            initialValue: getInitialValue(
                                                widget.oppoLoadData.jsonData,
                                                e),
                                            name: e.fieldName,
                                            decoration:
                                                formGlobal("Select State"),
                                            onChanged: (value) {
                                              BlocProvider.of<CityBloc>(context)
                                                  .add(CityPressed(
                                                      state: statedata.id));
                                            },
                                            items: statesData
                                                .map((data) => DropdownMenuItem(
                                                    value: data.name,
                                                    onTap: () {
                                                      setState(() {
                                                        statedata = data;
                                                      });
                                                    },
                                                    child: Text(data.name)))
                                                .toList(),
                                          ),
                                        );
                                      } else {
                                        return FormBuilderTextField(
                                          name: "Loading",
                                          decoration:
                                              formGlobal("Select State..."),
                                          readOnly: true,
                                        );
                                      }
                                    },
                                  ),
                                );
                                break;
                              case "city":
                                return BlocListener<CityBloc, CityState>(
                                  listener: (context, state) {
                                    if (state is CityFailure) {
                                      errorAlert(context,
                                          "Getting Error in City, please contact to Support Team... ");
                                    }
                                  },
                                  child: BlocBuilder<CityBloc, CityState>(
                                    builder: (context, state) {
                                      if (state is CitySuccess) {
                                        List<City> cityData = state.cityData;
                                        return Container(
                                          margin: fieldMargin,
                                          child: FormBuilderDropdown(
                                            initialValue: getInitialValue(
                                                widget.oppoLoadData.jsonData,
                                                e),
                                            name: e.fieldName,
                                            decoration:
                                                formGlobal("Select City"),
                                            items: cityData
                                                .map((data) => DropdownMenuItem(
                                                    value: data.name,
                                                    child: Text(data.name)))
                                                .toList(),
                                          ),
                                        );
                                      } else {
                                        return Container(
                                          margin: fieldMargin,
                                          child: FormBuilderTextField(
                                            name: "Loading",
                                            decoration:
                                                formGlobal("Select City"),
                                            readOnly: true,
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                );
                                break;
                              default:
                                isParentListValues['${e.chieldFieldId}'] =
                                    widget.oppoLoadData.jsonData[e.fieldName];

                                String isParentID =
                                    isParentListValues["${e.fieldId}"];

                                return e.isParentDependent == '0'
                                    ? BlocProvider(
                                        create: (context) =>
                                            CustomDependentDropdownBloc(
                                                genericRepository:
                                                    _genericRepository)
                                              ..add(
                                                  CustomDependentDropdownPressed(
                                                      formData: GenericModel(
                                                apiName:
                                                    'CustomFieldOptionsGet',
                                                fieldid: e.fieldId,
                                                parentFieldOptionId: "",
                                              ))),
                                        child: CustomEditDropdown(
                                            data: e,
                                            oppoLoadData: widget.oppoLoadData),
                                      )
                                    : BlocProvider(
                                        create: (context) =>
                                            CustomDependentDropdownBloc(
                                                genericRepository:
                                                    _genericRepository)
                                              ..add(
                                                  CustomDependentDropdownPressed(
                                                      formData: GenericModel(
                                                apiName:
                                                    'CustomFieldOptionsGet',
                                                fieldid: e.fieldId,
                                                parentFieldOptionId: isParentID,
                                              ))),
                                        child: CustomEditDropdown(
                                            data: e,
                                            oppoLoadData: widget.oppoLoadData),
                                      );
                            }
                            break;
                          case "text":
                            switch (e.fieldName) {
                              case "date_and_time":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderDateTimePicker(
                                    initialValue: getInitialValue(
                                        widget.oppoLoadData.jsonData, e),
                                    name: e.fieldName,
                                    onChanged: (value) {},
                                    inputType: InputType.both,
                                    decoration: formGlobal(e.fieldLabel),
                                    //initialValue: DateTime.now(),
                                    firstDate: DateTime.now(),
                                    initialEntryMode:
                                        DatePickerEntryMode.calendar,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "date":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderDateTimePicker(
                                    initialValue: getInitialValue(
                                        widget.oppoLoadData.jsonData, e),
                                    name: e.fieldName,
                                    onChanged: (value) {},
                                    inputType: InputType.date,
                                    decoration: formGlobal(e.fieldLabel),
                                    firstDate: DateTime.now(),
                                    initialEntryMode:
                                        DatePickerEntryMode.calendar,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "time":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderDateTimePicker(
                                    initialValue: getInitialValue(
                                        widget.oppoLoadData.jsonData, e),
                                    name: e.fieldName,
                                    onChanged: (value) {},
                                    inputType: InputType.time,
                                    decoration: formGlobal(e.fieldLabel),
                                    firstDate: DateTime.now(),
                                    initialEntryMode:
                                        DatePickerEntryMode.calendar,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "phone_number":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderField(
                                    initialValue: getInitialValue(
                                        widget.oppoLoadData.jsonData, e),
                                    name: e.fieldName,
                                    builder: (FormFieldState<dynamic> field) {
                                      return IntePhoneNumber(
                                        field: field,
                                        countries: ['IN'],
                                        initialPhoneNumber: widget
                                            .oppoLoadData.withoutnumbermasking,
                                      );
                                    },
                                  ),
                                );
                                break;
                              case "budget":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderTextField(
                                    initialValue: getInitialValue(
                                        widget.oppoLoadData.jsonData, e),
                                    name: e.fieldName,
                                    decoration: formGlobal(e.fieldLabel),
                                    keyboardType: TextInputType.number,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              case "email_id":
                                return Container(
                                  margin: fieldMargin,
                                  child: FormBuilderTextField(
                                    initialValue: getInitialValue(
                                        widget.oppoLoadData.jsonData, e),
                                    name: e.fieldName,
                                    decoration: formGlobal(e.fieldLabel),
                                    keyboardType: TextInputType.emailAddress,
                                    validator: e.isRequired == "1"
                                        ? FormBuilderValidators.compose([
                                            FormBuilderValidators.required(
                                                context),
                                            FormBuilderValidators.email(context)
                                          ])
                                        : null,
                                  ),
                                );
                                break;
                              default:
                                switch (e.validationType) {
                                  case "datetime":
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderDateTimePicker(
                                        initialValue: getInitialValue(
                                            widget.oppoLoadData.jsonData, e),
                                        name: e.fieldName,
                                        onChanged: (value) {},
                                        inputType: InputType.both,
                                        decoration: formGlobal(e.fieldLabel),
                                        firstDate: DateTime.now(),
                                        initialEntryMode:
                                            DatePickerEntryMode.calendar,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                    break;
                                  case "date":
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderDateTimePicker(
                                        initialValue: getInitialValue(
                                            widget.oppoLoadData.jsonData, e),
                                        name: e.fieldName,
                                        onChanged: (value) {},
                                        inputType: InputType.date,
                                        decoration: formGlobal(e.fieldLabel),
                                        //initialValue: DateTime.now(),
                                        firstDate: DateTime.now(),
                                        initialEntryMode:
                                            DatePickerEntryMode.calendar,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                    break;
                                  case "time":
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderDateTimePicker(
                                        initialDate: widget.oppoLoadData
                                                    .jsonData[e.fieldName] ==
                                                null
                                            ? null
                                            : DateTime.parse(widget.oppoLoadData
                                                .jsonData[e.fieldName]),
                                        name: e.fieldName,
                                        onChanged: (value) {},
                                        inputType: InputType.time,
                                        decoration: formGlobal(e.fieldLabel),
                                        firstDate: DateTime.now(),
                                        initialEntryMode:
                                            DatePickerEntryMode.calendar,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                    break;
                                  default:
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderTextField(
                                        initialValue: getInitialValue(
                                            widget.oppoLoadData.jsonData, e),
                                        name: e.fieldName,
                                        decoration: formGlobal(e.fieldLabel),
                                        keyboardType: TextInputType.text,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                      ),
                                    );
                                }
                            }
                            break;
                          case "radio":
                            return BlocProvider(
                              create: (context) => CustomDependentDropdownBloc(
                                  genericRepository: _genericRepository)
                                ..add(CustomDependentDropdownPressed(
                                    formData: GenericModel(
                                  apiName: 'CustomFieldOptionsGet',
                                  fieldid: e.fieldId,
                                  parentFieldOptionId: "",
                                ))),
                              child: BlocBuilder<CustomDependentDropdownBloc,
                                  CustomDependentDropdownState>(
                                builder: (context, state) {
                                  if (state is CustomDependentDropdownFailure) {
                                    return Container();
                                  }
                                  if (state is CustomDependentDropdownSuccess) {
                                    return FormBuilderRadioGroup<String>(
                                      initialValue: getInitialValue(
                                          widget.oppoLoadData.jsonData, e),
                                      decoration:
                                          formGlobalradiobox(e.fieldLabel),
                                      name: e.fieldName,
                                      onChanged: (value) {},
                                      activeColor: purple,
                                      focusColor: purple,
                                      validator: e.isRequired == "1"
                                          ? FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  context)
                                            ])
                                          : null,
                                      options: state.stateData
                                          .map(
                                            (data) => FormBuilderFieldOption(
                                              value: data.optionKey,
                                              child: Text(data.optionValue),
                                            ),
                                          )
                                          .toList(growable: true),
                                    );
                                  }
                                  return LoaderSmallIndicator();
                                },
                              ),
                            );
                            break;
                          case "textarea":
                            return Container(
                              margin: fieldMargin,
                              child: FormBuilderTextField(
                                initialValue: getInitialValue(
                                    widget.oppoLoadData.jsonData, e),
                                name: e.fieldName,
                                decoration: formGlobal(e.fieldLabel),
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                validator: e.isRequired == "1"
                                    ? FormBuilderValidators.compose([
                                        FormBuilderValidators.required(context)
                                      ])
                                    : null,
                              ),
                            );
                            break;
                          case "checkbox":
                            return BlocProvider(
                              create: (context) => CustomDependentDropdownBloc(
                                  genericRepository: _genericRepository)
                                ..add(CustomDependentDropdownPressed(
                                    formData: GenericModel(
                                  apiName: 'CustomFieldOptionsGet',
                                  fieldid: e.fieldId,
                                  parentFieldOptionId: "",
                                ))),
                              child: BlocBuilder<CustomDependentDropdownBloc,
                                  CustomDependentDropdownState>(
                                builder: (context, state) {
                                  if (state is CustomDependentDropdownFailure) {
                                    return Container();
                                  }
                                  if (state is CustomDependentDropdownSuccess) {
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderCheckboxGroup<String>(
                                        initialValue: getInitialValue(
                                            widget.oppoLoadData.jsonData, e),
                                        activeColor: purple,
                                        decoration: formGlobalcheckboxLabel(
                                            e.fieldLabel),
                                        name: e.fieldName,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                        options: state.stateData
                                            .map(
                                              (data) => FormBuilderFieldOption(
                                                value: data.optionKey,
                                              ),
                                            )
                                            .toList(),
                                      ),
                                    );
                                  }
                                  return LoaderSmallIndicator();
                                },
                              ),
                            );
                            break;
                          case "multiselect":
                            return BlocProvider(
                              create: (context) => CustomDependentDropdownBloc(
                                  genericRepository: _genericRepository)
                                ..add(CustomDependentDropdownPressed(
                                    formData: GenericModel(
                                  apiName: 'CustomFieldOptionsGet',
                                  fieldid: e.fieldId,
                                  parentFieldOptionId: "",
                                ))),
                              child: BlocBuilder<CustomDependentDropdownBloc,
                                  CustomDependentDropdownState>(
                                builder: (context, state) {
                                  if (state is CustomDependentDropdownFailure) {
                                    return Container();
                                  }
                                  if (state is CustomDependentDropdownSuccess) {
                                    return Container(
                                      margin: fieldMargin,
                                      child: FormBuilderCheckboxGroup<String>(
                                        initialValue: getInitialValue(
                                            widget.oppoLoadData.jsonData, e),
                                        activeColor: purple,
                                        decoration: formGlobalcheckboxLabel(
                                            e.fieldLabel),
                                        name: e.fieldName,
                                        validator: e.isRequired == "1"
                                            ? FormBuilderValidators.compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ])
                                            : null,
                                        options: state.stateData
                                            .map(
                                              (data) => FormBuilderFieldOption(
                                                value: data.optionKey,
                                              ),
                                            )
                                            .toList(),
                                      ),
                                    );
                                  }
                                  return LoaderSmallIndicator();
                                },
                              ),
                            );
                            break;
                          default:
                            return Text("${e.fieldType} ==> Missed");
                        }
                      }).toList(),
                    );
                  } else {
                    return Container();
                  }
                }),
              ),
              fieldDistance,
              BlocProvider(
                create: (context) =>
                    OppoEditLeadBloc(oppoRepository: _oppoRepository),
                child: BlocListener<OppoEditLeadBloc, OppoEditLeadState>(
                  listener: (context, state) {
                    if (state is OppoEditLeadFailure) {
                      errorAlert(context, state.error);
                    }
                    if (state is OppoEditLeadSuccess) {
                      successAlert(context, state.stateData);
                      Navigator.pop(context, "success");
                    }
                  },
                  child: BlocBuilder<OppoEditLeadBloc, OppoEditLeadState>(
                    builder: (context, state) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          RaisedGradientButton(
                            child: customText('Reset', 16, black),
                            gradient: LinearGradient(colors: grayTtL),
                            width: 100,
                            onPressed: () {
                              _fbKey.currentState.reset();
                            },
                          ),
                          SizedBox(width: 10),
                          state is OppoEditLeadInProgress
                              ? LoaderIndicator()
                              : RaisedGradientButton(
                                  child: buttonwhiteText('Submit'),
                                  gradient: LinearGradient(colors: purpleTtL),
                                  width: 100,
                                  onPressed: () {
                                    //Navigator.pushNamed(context, '/contacts');

                                    if (_fbKey.currentState.saveAndValidate()) {
                                      final Map formData =
                                          Map.of(_fbKey.currentState.value);

                                      print(formData);

                                      _formIds
                                          .map((e) => {
                                                if (e.fieldName ==
                                                        "date_and_time" ||
                                                    e.fieldName == "date" ||
                                                    e.fieldName == "time")
                                                  {
                                                    formData[e.fieldName] =
                                                        formData[e.fieldName]
                                                            .toString(),
                                                  },
                                                if (e.validationType ==
                                                        "datetime" ||
                                                    e.fieldName == "date" ||
                                                    e.fieldName == "time")
                                                  {
                                                    formData[e.fieldName] =
                                                        formData[e.fieldName]
                                                            .toString(),
                                                  }
                                              })
                                          .toList();
                                      BlocProvider.of<OppoEditLeadBloc>(context)
                                          .add(
                                        OppoEditLeadPressed(
                                          eventData: formData,
                                        ),
                                      );
                                      //print(_fbKey.currentState.value);
                                    }
                                  },
                                ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

getInitialValue(Map formData, OppoFormFieldsModel field) {
  if (formData[field.fieldName] == null ||
      formData[field.fieldName] == '' ||
      formData[field.fieldName] == "0") {
    return null;
  } else if (field.fieldName == "phone_number") {
    return "${formData['phone_code']} ${formData['withoutnumbermasking']}";
  } else if (field.fieldName == "date_and_time" ||
      field.fieldName == "date" ||
      field.fieldName == "time") {
    return DateTime.parse(formData[field.fieldName]);
  } else if (field.validationType == "datetime" ||
      field.validationType == "date" ||
      field.validationType == "time") {
    return DateTime.parse(formData[field.fieldName]);
  } else if (field.fieldType == "checkbox" ||
      field.fieldType == "multiselect") {
    print(formData[field.fieldName]);
    return formData[field.fieldName].split(',');
  } else {
    return formData[field.fieldName];
  }
}

Map<String, BuildContext> dependencyEditContext = {};
Map isParentListValues = {};

class CustomEditDropdown extends StatefulWidget {
  final OppoLoadData oppoLoadData;
  final OppoFormFieldsModel data;

  const CustomEditDropdown({Key key, this.data, this.oppoLoadData})
      : super(key: key);

  @override
  _CustomEditDropdownState createState() => _CustomEditDropdownState();
}

class _CustomEditDropdownState extends State<CustomEditDropdown> {
  @override
  void initState() {
    // final Map jsonFormData = json.decode(voterInfoToJson(widget.voterData));
    setState(() {
      isParentListValues['${widget.data.fieldId}'] =
          widget.oppoLoadData.jsonData[widget.data.fieldName];
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    dependencyEditContext[widget.data.fieldId] = context;

    return Column(
      children: [
        BlocBuilder<CustomDependentDropdownBloc, CustomDependentDropdownState>(
          builder: (context, state) {
            if (state is CustomDependentDropdownFailure) {
              return Container();
            }
            if (state is CustomDependentDropdownSuccess) {
              return state.stateData.isEmpty
                  ? Container()
                  : Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: FormBuilderDropdown(
                        key: UniqueKey(),
                        name: widget.data.fieldName,
                        decoration: formGlobal(widget.data.fieldLabel),
                        initialValue:
                            isParentListValues["${widget.data.fieldId}"] == ""
                                ? null
                                : isParentListValues["${widget.data.fieldId}"],
                        items: state.stateData
                            .map((childdata) => DropdownMenuItem(
                                  value: widget.data.isDependent == "1"
                                      ? childdata.fieldOptionId
                                      : childdata.optionValue,
                                  child: Text(
                                    childdata.optionValue,
                                  ),
                                ))
                            .toList(),
                        onChanged: widget.data.isDependent == "1"
                            ? (value) {
                                setState(() {
                                  isParentListValues["${widget.data.fieldId}"] =
                                      value;
                                  isParentListValues[
                                      "${widget.data.chieldFieldId}"] = "";
                                });

                                BlocProvider.of<CustomDependentDropdownBloc>(
                                        dependencyEditContext[
                                            widget.data.chieldFieldId])
                                    .add(CustomDependentDropdownPressed(
                                        formData: GenericModel(
                                            apiName: 'CustomFieldOptionsGet',
                                            fieldid: widget.data.chieldFieldId,
                                            parentFieldOptionId: value)));
                              }
                            : null,
                      ),
                    );
            }
            return LoaderSmallIndicator();
          },
        ),
      ],
    );
  }
}
