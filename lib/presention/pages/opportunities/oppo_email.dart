import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

final OppoRepository _oppoRepository =
    OppoRepository(oppoAPIClient: OppoAPIClient());

Future oppoEmail(BuildContext context, OppoLoadData data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    OppoEmailIdBloc(oppoRepository: _oppoRepository)
                      ..add(OppoEmailIdPressed()),
              ),
              BlocProvider(
                create: (context) =>
                    OppoAddEmailBloc(oppoRepository: _oppoRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 1000,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_email_modal.png", title: "Email"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        "lead_id": data.leadId,
                        "removedlistOfFiles": "",
                        "attachments": "",
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'lead_id'),
                          FormBuilderHiddenField(value: 'removedlistOfFiles'),
                          FormBuilderHiddenField(value: 'attachments'),
                          BlocBuilder<OppoEmailIdBloc, OppoEmailIdState>(
                            builder: (context, state) {
                              if (state is OppoEmailIdSuccess) {
                                return FormBuilderDropdown(
                                  name: "send_from_email",
                                  decoration: formGlobal("Select From Mail"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((data) => DropdownMenuItem(
                                          value: data.fromMail,
                                          child: Text("${data.fromMail}")))
                                      .toList(),
                                );
                              }
                              if (state is OppoEmailIdFailure) {
                                return Text("${state.error}");
                              }
                              return LoaderSmallIndicator();
                            },
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "send_from_email_cc",
                            decoration: formGlobal("Enter CC Mail"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.email(context)]),
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "send_from_email_bcc",
                            decoration: formGlobal("Enter Bcc Mail"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.email(context)]),
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "send_mail_subject",
                            decoration: formGlobal("Enter Subject"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          FormBuilderField(
                            name: "send_email_content",
                            builder: (FormFieldState<dynamic> field) {
                              return SummerNoteTextEditor(field: field);
                            },
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                        ],
                      ),
                    ),
                    fieldDistance,
                  ]),
                ),
              ),
              modalFooter: BlocListener<OppoAddEmailBloc, OppoAddEmailState>(
                listener: (context, state) {
                  if (state is OppoAddEmailSuccess) {
                    Navigator.pop(context);
                    successAlert(context, state.stateData);
                  }
                  if (state is OppoAddEmailFailure) {
                    Navigator.pop(context);
                    errorAlert(context, state.error);
                  }
                },
                child: BlocBuilder<OppoAddEmailBloc, OppoAddEmailState>(
                  builder: (context, state) {
                    return state is OppoAddEmailInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<OppoAddEmailBloc>(context).add(
                                    OppoAddEmailPressed(eventData: formData));
                                print(formData);
                              }
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
