import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future oppoFilter(
  BuildContext context,
  String dynamicDisplayAs,
  String dynamicDisplayType,
) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return ModalBox(
            modalHieght: 1000,
            modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_option_add_to_lead.png",
                title: "Leads Filter"),
            modalBody: Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(children: [
                  FormBuilder(
                    key: _fbKey,
                    autovalidateMode: AutovalidateMode.always,
                    initialValue: {},
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Display as", style: TextStyle(fontSize: 18)),
                        // ignore: deprecated_member_use
                        FormBuilderRadioGroup<String>(
                          name: "display_as",
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          initialValue: dynamicDisplayAs,
                          activeColor: purple,
                          options: displayAs
                              .map((data) => FormBuilderFieldOption(
                                    value: data.type,
                                    // ignore: deprecated_member_use
                                    child: Text(data.name),
                                  ))
                              .toList(growable: false),
                        ),
                        fieldDistance,
                        Text("Type of Lead", style: TextStyle(fontSize: 18)),
                        // ignore: deprecated_member_use
                        FormBuilderRadioGroup<String>(
                          name: "type_of_lead",
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          initialValue: dynamicDisplayType,
                          activeColor: purple,
                          options: typeOfLead
                              .map((data) => FormBuilderFieldOption(
                                    value: data.type,
                                    // ignore: deprecated_member_use
                                    child: Text(data.name),
                                  ))
                              .toList(growable: false),
                        ),
                        fieldDistance,
                      ],
                    ),
                  ),
                ]),
              ),
            ),
            modalFooter: ModalFooter(
              firstButton: "Cancel",
              firstFun: () {
                Navigator.pop(context);
              },
              secondButton: "Submit",
              secondFun: () async {
                if (_fbKey.currentState.saveAndValidate()) {
                  Map formData = _fbKey.currentState.value;
                  Navigator.pop(context, formData);
                  // print(formData);
                }
                //Navigator.pop(context);
              },
            ),
          );
        },
      );
    },
  );
}
