import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/globals/global.dart';
import 'package:office24by7_v2/presention/pages/opportunities/oppo_leadactivity_details.dart';
import 'package:office24by7_v2/presention/pages/opportunities/opportunities.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:intl/intl.dart';

class OppoLeadActivity extends StatelessWidget {
  final OppoLoadData data;

  OppoLeadActivity({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Lead Activity",
              actions: [
                IconButton(
                  icon: ImageLoad(
                    img: "/mobile_app_v2/o_filter_appbar.png",
                    width: 30,
                  ),
                  onPressed: () {
                    oppoActivityFilter(context, data).then((data) {
                      BlocProvider.of<OppoLeadActivityBloc>(context)
                          .add(OppoLeadActivityPressed(
                        activityType: data['activity_type'],
                        leadID: data['lead_id'],
                        fromDate: formateDateTimeAPI(data['from_date']),
                        toDate: formateDateTimeAPI(data['to_date']),
                      ));
                      print(data['to_date']);
                    });
                  },
                )
              ],
            ),
            BodyInner(
              page: OppoHistoryContent(data: data),
            ),
          ],
        ),
      ),
    );
  }
}

class OppoHistoryContent extends StatelessWidget {
  final OppoLoadData data;
  const OppoHistoryContent({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: tabBodyPadd,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(250),
                  ),
                  child: Container(
                    decoration: roundShadow,
                    height: 50,
                    width: 50,
                    child: ImageLoad(img: '/mobile_app_v2/o_profile_pic.png'),
                  ),
                ),
              ),
              Container(
                width: 120,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    profileText(data.fullName, 16),
                    customText(data.location, 14, black)
                  ],
                ),
              ),
              SizedBox(width: 15),
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: oppoHistoryTabs.map((historyData) {
                      return OppotActivityData(
                        oppoDetailsTabs: historyData,
                        data: data,
                      );
                    }).toList(),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: textGray)),
          ),
        ),
        Expanded(
          child: BlocBuilder<OppoLeadActivityBloc, OppoLeadActivityState>(
              builder: (context, state) {
            if (state is OppoLeadActivityInProgress) {
              return LoaderIndicator();
            }
            if (state is OppoLeadActivitySuccess) {
              if (state.stateData.length == 0) {
                return NoDataFound(message: "No Lead History Found...");
              }

              return ListView(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                children: state.stateData.map((e) {
                  return Column(
                    children: [
                      HistoryDataDesign(
                          date: DateFormat('yyyy-MM-dd – kk:mm')
                              .format(e.activitydate)),
                      Column(
                        children: e.data
                            .map(
                              (e) => Material(
                                color: white,
                                child: InkWell(
                                  onTap: () =>
                                      oppoLeadActivityDetails(context, e),
                                  child: HistoryStatusDesign(
                                    time: DateFormat('kk:mm')
                                        .format(e.createdDatetime),
                                    type: "${e.activityType}",
                                    description:
                                        e.activityDesc.replaceAll("\n", " "),
                                    image: oppoHistoryIcon(e.activityType),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                      )
                    ],
                  );
                }).toList(),
              );
            }
            if (state is OppoLeadActivityFailure) {
              return FailedToLoad(message: state.error);
            }
            return LoaderIndicator();
          }),
        ),
      ],
    );
  }
}

String oppoHistoryIcon(String type) {
  switch (type) {
    case "Assignment":
      return "/mobile_app_v2/o_option_reassign.png";
      break;
    case "Note":
      return "/mobile_app_v2/o_option_note.png";
      break;
    case "Voice Call":
      return "/mobile_app_v2/o_call_purple.png";
      break;
    case "Reminder":
      return "/mobile_app_v2/o_option_reminder.png";
      break;
    case "Update":
      return "/mobile_app_v2/o_option_task.png";
      break;
    case "Reassign":
      return "/mobile_app_v2/o_option_reassign.png";
      break;
    case "Meeting":
      return "/mobile_app_v2/o_option_meeting.png";
      break;
    case "Email":
      return "/mobile_app_v2/o_email_purple.png";
      break;
    case "Add Lead":
      return "/mobile_app_v2/o_option_add_to_lead.png";
      break;
    case "Call":
      return "/mobile_app_v2/o_call_purple.png";
      break;
    default:
      return "/mobile_app_v2/o_option_task.png";
  }
}
