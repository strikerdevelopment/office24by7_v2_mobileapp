import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future oppoLeadActivityDetails(BuildContext context, Datum data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return ModalBox(
            modalHieght: 700,
            modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_option_contact_details.png",
                title: "Lead Activity Details"),
            modalBody: Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(children: [
                  Column(
                    children: [
                      ListTile(
                        title: Text('Full Name'),
                        subtitle:
                            Text('${data.leadFirstName} ${data.leadLastName}'),
                      ),
                      ListTile(
                        title: Text('Activity Type'),
                        subtitle: Text('${data.activityType}'),
                      ),
                      ListTile(
                        title: Text('Activity Status'),
                        subtitle: Text('${data.activityStatus}'),
                      ),
                      data.activityType == 'Note'
                          ? ListTile(
                              title: Text('Note description'),
                              subtitle: Text(data.leadNotes[0].noteDesc),
                            )
                          : Container(),
                      ListTile(
                          title: Text('Activity Description'),
                          subtitle: Html(data: data.activityDesc)),
                      ListTile(
                        title: Text('Create Date'),
                        subtitle:
                            Text(formateDateTimeWidget(data.createdDatetime)),
                      ),
                      ListTile(
                        title: Text('Activity User Name'),
                        subtitle: Text(data.userName == null
                            ? 'No data'
                            : '${data.userName}'),
                      ),

                      // ListTile(
                      //   title: Text('Attachments'),
                      //   subtitle: Text(data.activityType),
                      // ),
                    ],
                  ),
                ]),
              ),
            ),
            modalFooter: ModalTextFooter(),
          );
        },
      );
    },
  );
}
