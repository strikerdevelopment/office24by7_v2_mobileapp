import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/contacts/contact_model/contact_repeat_date.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

final OppoRepository _oppoRepository =
    OppoRepository(oppoAPIClient: OppoAPIClient());

Future oppoMeeting(BuildContext context, OppoLoadData data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      final ValueChanged _onChanged = (val) => print(val);

      bool notifyDisplay = false;
      bool smsDisplay = false;
      bool emailDisplay = false;
      bool repeatDate = false;
      String repeatDateValue = 'Doesnotrepeat';
      String endsvalueDisplay = 'endson';

      return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => OppoDivisionUserDdBloc(
                  oppoRepository: _oppoRepository)
                ..add(OppoDivisionUserDdPressed(eventData: data.divisionId))),
          BlocProvider(
            create: (context) =>
                OppoSenderIdBloc(oppoRepository: _oppoRepository)
                  ..add(OppoSenderIdPressed()),
          ),
          BlocProvider(
            create: (context) =>
                OppoEmailIdBloc(oppoRepository: _oppoRepository)
                  ..add(OppoEmailIdPressed()),
          ),
          BlocProvider(
            create: (context) =>
                OppoDateTypeBloc(oppoRepository: _oppoRepository)
                  ..add(OppoDateTypePressed()),
          ),
          BlocProvider(
            create: (context) =>
                OppoAddMeetingBloc(oppoRepository: _oppoRepository),
          )
        ],
        child: StatefulBuilder(
          builder: (BuildContext context, setState) {
            return ModalBox(
              modalHieght: 700,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_meeting_modal.png", title: "Meeting"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        "lead_record_id": data.leadId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'lead_record_id'),
                          BlocBuilder<OppoDivisionUserDdBloc,
                              OppoDivisionUserDdState>(
                            builder: (context, state) {
                              if (state is OppoDivisionUserDdSuccess) {
                                return FormBuilderDropdown(
                                  name: "assignto",
                                  decoration: formGlobal("Select User"),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  items: state.stateData
                                      .map((data) => DropdownMenuItem(
                                          value: data.userId,
                                          child: Text("${data.userName}")))
                                      .toList(),
                                );
                              }
                              if (state is OppoDivisionUserDdFailure) {
                                return Text("${state.error}");
                              }
                              return LoaderIndicator();
                            },
                          ),
                          fieldDistance,
                          FormBuilderDateTimePicker(
                            name: 'meeting_date',
                            onChanged: (value) {
                              if (value == null) {
                                setState(() {
                                  repeatDate = false;
                                });
                              } else {
                                setState(() {
                                  repeatDate = true;
                                });
                              }
                            },
                            inputType: InputType.date,
                            decoration: formGlobal("Select Meeting Date"),
                            //initialValue: DateTime.now(),
                            firstDate: DateTime.now(),
                            initialEntryMode: DatePickerEntryMode.calendar,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'meeting_Fromtime',
                                  onChanged: _onChanged,
                                  inputType: InputType.time,
                                  decoration: formGlobal("From Time"),
                                  //initialValue: DateTime.now(),
                                  firstDate: DateTime.now(),
                                  validator: (val) {
                                    if (val == null) return 'Required';
                                    return null;
                                  },
                                ),
                              ),
                              fieldWidth,
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'meeting_Totime',
                                  onChanged: _onChanged,
                                  inputType: InputType.time,
                                  decoration: formGlobal("To Time"),
                                  //initialValue: DateTime.now(),
                                  firstDate: DateTime.now(),
                                  validator: (val) {
                                    if (val == null) return 'Required';
                                    return null;
                                  },
                                ),
                              )
                            ],
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "comments",
                            decoration: formGlobal("Description"),
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          FormBuilderCheckbox(
                            activeColor: purple,
                            name: 'Isnotify',
                            title:
                                Text("Notify", style: TextStyle(fontSize: 16)),
                            // leadingInput: true,
                            initialValue: false,
                            decoration: formGlobalcheckbox(),
                            onChanged: (value) {
                              setState(() {
                                notifyDisplay = value;
                              });
                            },
                          ),
                          notifyDisplay
                              ? Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: FormBuilderDateTimePicker(
                                            name: 'Reminder_date',
                                            onChanged: (value) {},
                                            inputType: InputType.date,
                                            decoration:
                                                formGlobal("Notify Date"),
                                            //initialValue: DateTime.now(),
                                            firstDate: DateTime.now(),
                                            validator: FormBuilderValidators
                                                .compose(notifyDisplay
                                                    ? [
                                                        FormBuilderValidators
                                                            .required(context)
                                                      ]
                                                    : []),
                                            initialEntryMode:
                                                DatePickerEntryMode.calendar,
                                          ),
                                        ),
                                        fieldWidth,
                                        Expanded(
                                          child: FormBuilderDateTimePicker(
                                            name: 'Reminder_time',
                                            onChanged: _onChanged,
                                            inputType: InputType.time,
                                            decoration:
                                                formGlobal("Notify Time"),
                                            //initialValue: DateTime.now(),
                                            validator: FormBuilderValidators
                                                .compose(notifyDisplay
                                                    ? [
                                                        FormBuilderValidators
                                                            .required(context)
                                                      ]
                                                    : []),
                                            firstDate: DateTime.now(),
                                          ),
                                        )
                                      ],
                                    ),
                                    fieldDistance,
                                    FormBuilderCheckbox(
                                      activeColor: purple,
                                      name: 'Issms',
                                      title: Text("SMS",
                                          style: TextStyle(fontSize: 16)),
                                      // leadingInput: true,
                                      initialValue: false,
                                      decoration: formGlobalcheckbox(),
                                      onChanged: (value) {
                                        setState(() {
                                          smsDisplay = value;
                                        });
                                      },
                                    ),
                                    smsDisplay
                                        ? BlocBuilder<OppoSenderIdBloc,
                                            OppoSenderIdState>(
                                            builder: (context, state) {
                                              if (state
                                                  is OppoSenderIdSuccess) {
                                                return FormBuilderDropdown(
                                                  name: "SenderID",
                                                  decoration: formGlobal(
                                                      "Select Sender ID"),
                                                  items: state.stateData
                                                      .map((data) =>
                                                          DropdownMenuItem(
                                                              value: data.sId,
                                                              child: Text(
                                                                  "${data.senderId}")))
                                                      .toList(),
                                                );
                                              }
                                              if (state
                                                  is OppoSenderIdFailure) {
                                                return Text("${state.error}");
                                              }
                                              return LoaderIndicator();
                                            },
                                          )
                                        : Container(),
                                    FormBuilderCheckbox(
                                      activeColor: purple,
                                      name: 'Isemail',
                                      title: Text("Email",
                                          style: TextStyle(fontSize: 16)),
                                      // leadingInput: true,
                                      initialValue: false,
                                      decoration: formGlobalcheckbox(),
                                      onChanged: (value) {
                                        setState(() {
                                          emailDisplay = value;
                                        });
                                      },
                                    ),
                                    emailDisplay
                                        ? BlocBuilder<OppoEmailIdBloc,
                                            OppoEmailIdState>(
                                            builder: (context, state) {
                                              if (state is OppoEmailIdSuccess) {
                                                return FormBuilderDropdown(
                                                  name: "EmailID",
                                                  decoration: formGlobal(
                                                      "Select Email ID"),
                                                  items: state.stateData
                                                      .map((data) =>
                                                          DropdownMenuItem(
                                                              value:
                                                                  data.fromMail,
                                                              child: Text(
                                                                  "${data.fromMail}")))
                                                      .toList(),
                                                );
                                              }
                                              if (state is OppoEmailIdFailure) {
                                                return Text("${state.error}");
                                              }
                                              return LoaderIndicator();
                                            },
                                          )
                                        : Container(),
                                  ],
                                )
                              : Container(),
                          fieldDistance,
                          repeatDate
                              ? BlocBuilder<OppoDateTypeBloc,
                                  OppoDateTypeState>(
                                  builder: (context, state) {
                                    if (state is OppoDateTypeSuccess) {
                                      return FormBuilderDropdown(
                                        name: "Reminders",
                                        decoration:
                                            formGlobal("Select Schedule"),
                                        onChanged: (value) {
                                          setState(() {
                                            repeatDateValue = value;
                                          });
                                        },
                                        items: state.stateData
                                            .map(
                                              (data) => DropdownMenuItem(
                                                value: data.type,
                                                child: Text(
                                                  "${data.name}",
                                                  style:
                                                      TextStyle(fontSize: 16),
                                                ),
                                              ),
                                            )
                                            .toList(),
                                      );
                                    }
                                    if (state is OppoDateTypeFailure) {
                                      return Text("${state.error}");
                                    }
                                    return LoaderIndicator();
                                  },
                                )
                              : Container(),
                          fieldDistance,
                          repeatDateValue == 'Doesnotrepeat'
                              ? Container()
                              : Column(
                                  children: [
                                    FormBuilderRadioGroup(
                                      decoration: formGlobalradiobox('Ends:'),
                                      name: 'endsvalue',
                                      onChanged: (value) {
                                        setState(() {
                                          endsvalueDisplay = value;
                                        });
                                      },
                                      activeColor: purple,
                                      focusColor: purple,
                                      initialValue: 'endson',
                                      options: contactEdson
                                          .map(
                                            (data) => FormBuilderFieldOption(
                                              value: data.type,
                                              child: Text(data.name),
                                            ),
                                          )
                                          .toList(growable: true),
                                    ),
                                    endsvalueDisplay == 'endson'
                                        ? FormBuilderDateTimePicker(
                                            name: 'alert_end_date',
                                            onChanged: (value) {},
                                            inputType: InputType.date,
                                            decoration: formGlobal("Date"),
                                            //initialValue: DateTime.now(),
                                            firstDate: DateTime.now(),
                                            initialEntryMode:
                                                DatePickerEntryMode.calendar,
                                          )
                                        : FormBuilderTextField(
                                            name: "remaindernumber",
                                            decoration:
                                                formGlobal("Occurrences"),
                                            keyboardType: TextInputType.number,
                                          ),
                                  ],
                                ),
                          fieldDistance,
                          repeatDateValue == 'custom'
                              ? FormBuilderDropdown(
                                  name: "Repeatdays",
                                  decoration: formGlobal("Repeat On"),
                                  items: weekData
                                      .map(
                                        (data) => DropdownMenuItem(
                                          value: data.type,
                                          child: Text(
                                            "${data.name}",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      )
                                      .toList(),
                                )
                              : Container()
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<OppoAddMeetingBloc, OppoAddMeetingState>(
                listener: (context, state) {
                  if (state is OppoAddMeetingSuccess) {
                    Navigator.pop(context, "success");
                  }
                  if (state is OppoAddMeetingFailure) {
                    Navigator.pop(context, "failure");
                  }
                },
                child: BlocBuilder<OppoAddMeetingBloc, OppoAddMeetingState>(
                  builder: (context, state) {
                    return state is OppoAddMeetingInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData =
                                    Map.of(_fbKey.currentState.value);

                                formData["meeting_date"] =
                                    formData["meeting_date"].toString();
                                formData["meeting_Fromtime"] =
                                    formData["meeting_Fromtime"].toString();
                                formData["meeting_Totime"] =
                                    formData["meeting_Totime"].toString();
                                formData["Reminder_date"] =
                                    formData["Reminder_date"].toString();
                                formData["Reminder_time"] =
                                    formData["Reminder_time"].toString();
                                formData["alert_end_date"] =
                                    formData["alert_end_date"].toString();

                                BlocProvider.of<OppoAddMeetingBloc>(context)
                                    .add(OppoAddMeetingPressed(
                                        eventData: formData));
                                print(formData);
                              }
                            },
                          );
                  },
                ),
              ),
            );
          },
        ),
      );
    },
  );
}
