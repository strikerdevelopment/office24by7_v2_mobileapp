import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future oppoNote(BuildContext context, OppoLoadData data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return BlocProvider(
            create: (context) => OppoNoteBloc(
                oppoRepository: OppoRepository(oppoAPIClient: OppoAPIClient())),
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_note_modal.png", title: "Create Note"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        'lead_record_id': data.leadId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'lead_record_id'),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "comment",
                            decoration: formGlobal("Write Comment"),
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: BlocListener<OppoNoteBloc, OppoNoteState>(
                listener: (context, state) {
                  if (state is OppoNoteSuccess) {
                    Navigator.pop(context, "success");
                  }
                  if (state is OppoNoteFailure) {
                    Navigator.pop(context, "failure");
                  }
                },
                child: BlocBuilder<OppoNoteBloc, OppoNoteState>(
                  builder: (context, state) {
                    return state is OppoNoteInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                BlocProvider.of<OppoNoteBloc>(context)
                                    .add(OppoNotePressed(eventData: formData));
                                print(_fbKey.currentState.value);
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
