import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future oppoReassign(BuildContext context, OppoLoadData data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => OppoDivisionUserDdBloc(
                    oppoRepository:
                        OppoRepository(oppoAPIClient: OppoAPIClient()))
                  ..add(OppoDivisionUserDdPressed(eventData: data.divisionId)),
              ),
              BlocProvider(
                create: (context) => OppoAddReassignBloc(
                    oppoRepository:
                        OppoRepository(oppoAPIClient: OppoAPIClient())),
              ),
            ],
            child: ModalBox(
              modalHieght: 300,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_reassign.png",
                  title: "Reassign"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        "lead_record_id": data.leadId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'lead_record_id'),
                          BlocBuilder<OppoDivisionUserDdBloc,
                                  OppoDivisionUserDdState>(
                              builder: (context, state) {
                            if (state is OppoDivisionUserDdSuccess) {
                              return FormBuilderDropdown(
                                name: "assignee_id",
                                decoration: formGlobal("Select Assigned User"),
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context)]),
                                items: state.stateData
                                    .map((data) => DropdownMenuItem(
                                        value: data.userId,
                                        child: Text(data.userName)))
                                    .toList(),
                              );
                            }
                            if (state is OppoDivisionUserDdFailure) {
                              return FailedToLoad(message: state.error);
                            }
                            return LoaderIndicator();
                          }),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<OppoAddReassignBloc, OppoAddReassignState>(
                listener: (context, state) {
                  if (state is OppoAddReassignSuccess) {
                    Navigator.pop(context, "success");
                  }
                  if (state is OppoAddReassignFailure) {
                    Navigator.pop(context, "failure");
                  }
                },
                child: BlocBuilder<OppoAddReassignBloc, OppoAddReassignState>(
                  builder: (context, state) {
                    return state is OppoAddReassignInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData = _fbKey.currentState.value;
                                print(_fbKey.currentState.value);
                                BlocProvider.of<OppoAddReassignBloc>(context)
                                    .add(OppoAddReassignPressed(
                                        eventData: formData));
                              }
                              //Navigator.pop(context);
                            },
                          );
                  },
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
