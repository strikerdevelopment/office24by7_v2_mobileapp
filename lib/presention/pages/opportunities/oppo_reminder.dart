import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

final OppoRepository _oppoRepository =
    OppoRepository(oppoAPIClient: OppoAPIClient());

Future oppoReminder(BuildContext context, OppoLoadData data) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      final ValueChanged _onChanged = (val) => print(val);

      bool notifyDisplay = false;
      bool smsDisplay = false;
      bool emailDisplay = false;
      bool repeatDate = true;
      String repeatDateValue = 'Doesnotrepeat';
      String endsvalueDisplay = 'endson';

      return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                OppoSenderIdBloc(oppoRepository: _oppoRepository)
                  ..add(OppoSenderIdPressed()),
          ),
          BlocProvider(
            create: (context) =>
                OppoEmailIdBloc(oppoRepository: _oppoRepository)
                  ..add(OppoEmailIdPressed()),
          ),
          BlocProvider(
            create: (context) =>
                OppoDateTypeBloc(oppoRepository: _oppoRepository)
                  ..add(OppoDateTypePressed()),
          ),
          BlocProvider(
            create: (context) =>
                OppoAddReminderBloc(oppoRepository: _oppoRepository),
          )
        ],
        child: StatefulBuilder(
          builder: (BuildContext context, setState) {
            return ModalBox(
              modalHieght: 700,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_reminder.png",
                  title: "Reminder"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {
                        "lead_record_id": data.leadId,
                      },
                      child: Column(
                        children: [
                          FormBuilderHiddenField(value: 'lead_record_id'),
                          FormBuilderTextField(
                            name: "Reminder_title",
                            decoration: formGlobal("Reminder Title"),
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'Reminder_date',
                                  onChanged: (value) {
                                    if (value == null) {
                                      setState(() {
                                        repeatDate = false;
                                      });
                                    } else {
                                      setState(() {
                                        repeatDate = true;
                                      });
                                    }
                                  },
                                  inputType: InputType.date,
                                  decoration: formGlobal("Select Date"),
                                  initialValue: DateTime.now(),
                                  firstDate: DateTime.now(),
                                  validator: (val) {
                                    if (val == null) return 'Required';
                                    return null;
                                  },
                                  initialEntryMode:
                                      DatePickerEntryMode.calendar,
                                ),
                              ),
                              fieldWidth,
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'Reminder_time',
                                  onChanged: _onChanged,
                                  inputType: InputType.time,
                                  decoration: formGlobal("Select Time"),
                                  initialValue: DateTime.now(),
                                  validator: (val) {
                                    if (val == null) return 'Required';
                                    return null;
                                  },
                                  firstDate: DateTime.now(),
                                ),
                              )
                            ],
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "descriptions",
                            decoration: formGlobal("Description"),
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            validator: FormBuilderValidators.compose(
                                [FormBuilderValidators.required(context)]),
                          ),
                          fieldDistance,
                          FormBuilderCheckbox(
                            activeColor: purple,
                            name: 'Isnotify',
                            title:
                                Text("Notify", style: TextStyle(fontSize: 16)),
                            // leadingInput: true,
                            initialValue: false,
                            decoration: formGlobalcheckbox(),
                            onChanged: (value) {
                              setState(() {
                                notifyDisplay = value;
                              });
                            },
                          ),
                          notifyDisplay
                              ? Column(
                                  children: [
                                    FormBuilderCheckbox(
                                      activeColor: purple,
                                      name: 'Issms',
                                      title: Text("SMS",
                                          style: TextStyle(fontSize: 16)),
                                      // leadingInput: true,
                                      initialValue: false,
                                      decoration: formGlobalcheckbox(),
                                      onChanged: (value) {
                                        setState(() {
                                          smsDisplay = value;
                                        });
                                      },
                                    ),
                                    smsDisplay
                                        ? BlocBuilder<OppoSenderIdBloc,
                                            OppoSenderIdState>(
                                            builder: (context, state) {
                                              if (state
                                                  is OppoSenderIdSuccess) {
                                                return FormBuilderDropdown(
                                                  name: "SenderID",
                                                  decoration: formGlobal(
                                                      "Select Sender ID"),
                                                  items: state.stateData
                                                      .map((data) =>
                                                          DropdownMenuItem(
                                                              value: data.sId,
                                                              child: Text(
                                                                  "${data.senderId}")))
                                                      .toList(),
                                                );
                                              }
                                              if (state
                                                  is OppoSenderIdFailure) {
                                                return Text("${state.error}");
                                              }
                                              return LoaderIndicator();
                                            },
                                          )
                                        : Container(),
                                    FormBuilderCheckbox(
                                      activeColor: purple,
                                      name: 'Isemail',
                                      title: Text("Email",
                                          style: TextStyle(fontSize: 16)),
                                      // leadingInput: true,
                                      initialValue: false,
                                      decoration: formGlobalcheckbox(),
                                      onChanged: (value) {
                                        setState(() {
                                          emailDisplay = value;
                                        });
                                      },
                                    ),
                                    emailDisplay
                                        ? BlocBuilder<OppoEmailIdBloc,
                                            OppoEmailIdState>(
                                            builder: (context, state) {
                                              if (state is OppoEmailIdSuccess) {
                                                return FormBuilderDropdown(
                                                  name: "EmailID",
                                                  decoration: formGlobal(
                                                      "Select Email ID"),
                                                  items: state.stateData
                                                      .map((data) =>
                                                          DropdownMenuItem(
                                                              value:
                                                                  data.fromMail,
                                                              child: Text(
                                                                  "${data.fromMail}")))
                                                      .toList(),
                                                );
                                              }
                                              if (state is OppoEmailIdFailure) {
                                                return Text("${state.error}");
                                              }
                                              return LoaderIndicator();
                                            },
                                          )
                                        : Container(),
                                  ],
                                )
                              : Container(),
                          fieldDistance,
                          repeatDate
                              ? BlocBuilder<OppoDateTypeBloc,
                                  OppoDateTypeState>(
                                  builder: (context, state) {
                                    if (state is OppoDateTypeSuccess) {
                                      return FormBuilderDropdown(
                                        name: "Reminders",
                                        decoration:
                                            formGlobal("Select Schedule"),
                                        validator:
                                            FormBuilderValidators.compose([
                                          FormBuilderValidators.required(
                                              context)
                                        ]),
                                        onChanged: (value) {
                                          setState(() {
                                            repeatDateValue = value;
                                          });
                                        },
                                        items: state.stateData
                                            .map(
                                              (data) => DropdownMenuItem(
                                                value: data.type,
                                                child: Text(
                                                  "${data.name}",
                                                  style:
                                                      TextStyle(fontSize: 16),
                                                ),
                                              ),
                                            )
                                            .toList(),
                                      );
                                    }
                                    if (state is OppoDateTypeFailure) {
                                      return Text("${state.error}");
                                    }
                                    return LoaderIndicator();
                                  },
                                )
                              : Container(),
                          fieldDistance,
                          repeatDateValue == 'Doesnotrepeat'
                              ? Container()
                              : Column(
                                  children: [
                                    FormBuilderRadioGroup(
                                      decoration: formGlobalradiobox('Ends:'),
                                      name: 'endsvalue',
                                      onChanged: (value) {
                                        setState(() {
                                          endsvalueDisplay = value;
                                        });
                                      },
                                      activeColor: purple,
                                      focusColor: purple,
                                      initialValue: 'endson',
                                      options: contactEdson
                                          .map(
                                            (data) => FormBuilderFieldOption(
                                              value: data.type,
                                              child: Text(data.name),
                                            ),
                                          )
                                          .toList(growable: true),
                                    ),
                                    endsvalueDisplay == 'endson'
                                        ? FormBuilderDateTimePicker(
                                            name: 'Endon_date',
                                            onChanged: (value) {},
                                            inputType: InputType.date,
                                            decoration: formGlobal("Date"),
                                            //initialValue: DateTime.now(),
                                            firstDate: DateTime.now(),
                                            initialEntryMode:
                                                DatePickerEntryMode.calendar,
                                          )
                                        : FormBuilderTextField(
                                            name: "Remindernumber",
                                            decoration:
                                                formGlobal("Occurrences"),
                                            keyboardType: TextInputType.number,
                                          ),
                                  ],
                                ),
                          fieldDistance,
                          repeatDateValue == 'custom'
                              ? FormBuilderDropdown(
                                  name: "Repeatdays",
                                  decoration: formGlobal("Repeat On"),
                                  items: weekData
                                      .map(
                                        (data) => DropdownMenuItem(
                                          value: data.type,
                                          child: Text(
                                            "${data.name}",
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      )
                                      .toList(),
                                )
                              : Container()
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter:
                  BlocListener<OppoAddReminderBloc, OppoAddReminderState>(
                listener: (context, state) {
                  if (state is OppoAddReminderSuccess) {
                    successAlert(context, state.stateData);
                    Navigator.pop(context);
                  }
                  if (state is OppoAddReminderFailure) {
                    errorAlert(context, state.error);
                    Navigator.pop(context);
                  }
                },
                child: BlocBuilder<OppoAddReminderBloc, OppoAddReminderState>(
                  builder: (context, state) {
                    return state is OppoAddReminderInProgress
                        ? LoaderIndicator()
                        : ModalFooter(
                            firstButton: "Cancel",
                            firstFun: () {
                              Navigator.pop(context);
                            },
                            secondButton: "Submit",
                            secondFun: () async {
                              if (_fbKey.currentState.saveAndValidate()) {
                                Map formData =
                                    Map.of(_fbKey.currentState.value);
                                formData["Reminder_date"] =
                                    formData["Reminder_date"].toString();
                                formData["Reminder_time"] =
                                    formData["Reminder_time"].toString();
                                formData["Endon_date"] =
                                    formData["Endon_date"].toString();

                                BlocProvider.of<OppoAddReminderBloc>(context)
                                    .add(OppoAddReminderPressed(
                                        eventData: formData));
                                print(formData);
                              }
                            },
                          );
                  },
                ),
              ),
            );
          },
        ),
      );
    },
  );
}
