import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:intl/intl.dart';

OppoRepository _oppoRepository = OppoRepository(oppoAPIClient: OppoAPIClient());

String divisionID;

Future oppoSearch(
  BuildContext context,
  String dynamicDisplayAs,
  String dynamicDisplayType,
  String dateType,
  String fromDate,
  String toDate,
  String searchValue,
) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      final ValueChanged _onChanged = (val) => print(val);
      final DateTime parsedFromDate = DateTime(
          DateTime.now().year, DateTime.now().month - 2, DateTime.now().day);
      final DateTime parsedToDate = DateTime.now();

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    OppoDivisionDdBloc(oppoRepository: _oppoRepository)
                      ..add(OppoDivisionDdPressed()),
              ),
              BlocProvider(
                create: (context) =>
                    OppoDivisionUserDdBloc(oppoRepository: _oppoRepository),
              ),
            ],
            child: ModalBox(
              modalHieght: 1000,
              modalHeader: ModalHeader(
                  img: "/mobile_app_v2/o_option_add_to_lead.png",
                  title: "Leads Search"),
              modalBody: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView(children: [
                    FormBuilder(
                      key: _fbKey,
                      autovalidateMode: AutovalidateMode.always,
                      initialValue: {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FormBuilderDropdown(
                            name: "dateType",
                            decoration: formGlobal("Select Date Type"),
                            onChanged: (value) {},
                            initialValue: dateType,
                            items: dateTypeDd
                                .map(
                                  (data) => DropdownMenuItem(
                                    value: data.type,
                                    child: Text(
                                      "${data.name}",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                          fieldDistance,
                          Row(
                            children: [
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'fromDate',
                                  onChanged: (val) {
                                    setState(() => {
                                          fromDate = val.toString(),
                                        });
                                  },
                                  inputType: InputType.date,
                                  decoration: formGlobal("From Date"),
                                  format: DateFormat('yyyy-MM-dd'),
                                  lastDate: DateTime.now(),
                                  validator: (val) => null,
                                  initialValue: parsedFromDate,
                                  initialEntryMode:
                                      DatePickerEntryMode.calendar,
                                ),
                              ),
                              fieldWidth,
                              Expanded(
                                child: FormBuilderDateTimePicker(
                                  name: 'toDate',
                                  onChanged: _onChanged,
                                  inputType: InputType.date,
                                  format: DateFormat('yyyy-MM-dd'),
                                  decoration: formGlobal("To Date"),
                                  firstDate: DateTime.parse(fromDate),
                                  lastDate: DateTime.now(),
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(context)
                                  ]),
                                  // validator: (val) => null,
                                  initialValue: parsedToDate,
                                  //firstDate: DateTime.now(),
                                  initialEntryMode:
                                      DatePickerEntryMode.calendar,
                                ),
                              )
                            ],
                          ),
                          fieldDistance,
                          FormBuilderDropdown(
                            name: "display_as",
                            decoration: formGlobal("Select Display As"),
                            initialValue: dynamicDisplayAs,
                            onChanged: (value) {},
                            items: displayAs
                                .map(
                                  (data) => DropdownMenuItem(
                                    value: data.type,
                                    child: Text(
                                      "${data.name}",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                          fieldDistance,
                          FormBuilderDropdown(
                            name: "type_of_lead",
                            decoration: formGlobal("Select Type of Lead"),
                            initialValue: dynamicDisplayType,
                            onChanged: (value) {},
                            items: typeOfLead
                                .map(
                                  (data) => DropdownMenuItem(
                                    value: data.type,
                                    child: Text(
                                      "${data.name}",
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                          fieldDistance,
                          FormBuilderTextField(
                            name: "searchValue",
                            decoration: formGlobal("Search..."),
                            keyboardType: TextInputType.text,
                            initialValue: searchValue,
                          ),
                          fieldDistance,
                          BlocBuilder<OppoDivisionDdBloc, OppoDivisionDdState>(
                              builder: (context, state) {
                            if (state is OppoDivisionDdSuccess) {
                              return FormBuilderDropdown(
                                name: "division",
                                decoration: formGlobal("Select Division"),
                                onChanged: (value) {
                                  setState(() {
                                    divisionID = value;
                                  });

                                  BlocProvider.of<OppoDivisionUserDdBloc>(
                                      context)
                                    ..add(OppoDivisionUserDdPressed(
                                        eventData: value));
                                },
                                items: state.stateData
                                    .map((data) => DropdownMenuItem(
                                        value: data.divisionId,
                                        child: Text(data.divisionName)))
                                    .toList(),
                              );
                            } else {
                              return FormBuilderTextField(
                                name: "division",
                                decoration: formGlobal("Loading..."),
                                readOnly: true,
                              );
                            }
                          }),
                          fieldDistance,
                          BlocListener<OppoDivisionUserDdBloc,
                              OppoDivisionUserDdState>(
                            listener: (context, state) {
                              if (state is OppoDivisionUserDdFailure) {
                                errorAlert(context, state.error);
                              }
                            },
                            child: BlocBuilder<OppoDivisionUserDdBloc,
                                    OppoDivisionUserDdState>(
                                builder: (context, state) {
                              if (state is OppoDivisionUserDdSuccess) {
                                return FormBuilderDropdown(
                                  name: "assigned_to",
                                  decoration: formGlobal("Select Assinged to"),
                                  items: state.stateData
                                      .map((data) => DropdownMenuItem(
                                          value: data.userId,
                                          child: Text(data.userName)))
                                      .toList(),
                                );
                              } else {
                                return Container();
                              }
                            }),
                          ),
                          fieldDistance,
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              modalFooter: ModalFooter(
                firstButton: "Reset",
                firstFun: () {
                  _fbKey.currentState.fields['fromDate']
                      .didChange(parsedFromDate);
                  _fbKey.currentState.fields['toDate'].didChange(parsedToDate);
                  _fbKey.currentState.reset();
                },
                secondButton: "Submit",
                secondFun: () async {
                  if (_fbKey.currentState.saveAndValidate()) {
                    Map formData = _fbKey.currentState.value;

                    // formData['fromDate'] = DateFormat('yyyy-MM-dd')
                    //     .format(_fbKey.currentState.value['fromDate']);
                    // formData['toDate'] = DateFormat('yyyy-MM-dd')
                    //     .format(_fbKey.currentState.value['toDate']);

                    print(formData);
                    Navigator.pop(context, formData);
                    // print(formData);
                  }
                  //Navigator.pop(context);
                },
              ),
            ),
          );
        },
      );
    },
  );
}
