// import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_data/repositories/global_repository.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/opportunities/oppo_filter.dart';
import 'package:office24by7_v2/presention/pages/opportunities/oppo_reminder.dart';
import 'package:office24by7_v2/presention/pages/opportunities/oppo_task.dart';
import 'package:office24by7_v2/presention/pages/opportunities/opportunities.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';
import 'package:intl/intl.dart';

DateTime nowDate = DateTime.now();
final OppoRepository _oppoRepository =
    OppoRepository(oppoAPIClient: OppoAPIClient());

GlobalRepository _globalRepository =
    GlobalRepository(globalApiClient: GlobalApiClient());

GenericRepository _genericRepository =
    GenericRepository(genericAPIClient: GenericAPIClient());

OppoHeaderBloc _oppoHeaderBloc;
String dynamicDisplayAs = "status";
String dynamicDisplayType = "all";
String dateType = "created_datetime";
String fromDate = DateFormat('yyyy-MM-dd')
    .format(DateTime(nowDate.year, nowDate.month - 2, nowDate.day));
String toDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
String searchValue = "";
String assignedTo = "";

class OpportunitiesHome extends StatefulWidget {
  @override
  _OpportunitiesHomeState createState() => _OpportunitiesHomeState();
}

class _OpportunitiesHomeState extends State<OpportunitiesHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNotLeading(
              title: "Opportunities",
              actions: [
                IconButton(
                  icon: ImageLoad(
                    img: "/mobile_app_v2/o_search_appbar.png",
                    width: 30,
                  ),
                  onPressed: () {
                    oppoSearch(
                      context,
                      dynamicDisplayAs,
                      dynamicDisplayType,
                      dateType,
                      fromDate,
                      toDate,
                      searchValue,
                    ).then((data) {
                      if (data != null) {
                        print(data);
                        setState(() {
                          dateType = data["dateType"];
                          fromDate =
                              DateFormat('yyyy-MM-dd').format(data["fromDate"]);
                          toDate =
                              DateFormat('yyyy-MM-dd').format(data["toDate"]);
                          dynamicDisplayAs = data["display_as"];
                          dynamicDisplayType = data["type_of_lead"];
                          searchValue = data['searchValue'];
                          assignedTo = data['assigned_to'] == null
                              ? ""
                              : data['assigned_to'];
                        });
                        _oppoHeaderBloc.add(
                            OppoHeaderPressed(displayAs: data["display_as"]));
                      }
                    });
                  },
                ),
                IconButton(
                  icon: ImageLoad(
                    img: "/mobile_app_v2/o_filter_appbar.png",
                    width: 30,
                  ),
                  onPressed: () {
                    oppoFilter(context, dynamicDisplayAs, dynamicDisplayType)
                        .then((data) {
                      if (data != null) {
                        print(data);
                        setState(() {
                          dynamicDisplayAs = data["display_as"];
                          dynamicDisplayType = data["type_of_lead"];
                        });
                        _oppoHeaderBloc.add(
                            OppoHeaderPressed(displayAs: data["display_as"]));
                      }
                    });
                  },
                ),
                // Badge(
                //   badgeColor: orange,
                //   position: BadgePosition.topEnd(top: 3, end: 5),
                //   animationDuration: Duration(milliseconds: 300),
                //   animationType: BadgeAnimationType.slide,
                //   badgeContent: Text(
                //     '2',
                //     style: TextStyle(color: Colors.white),
                //   ),
                //   child: IconButton(
                //       icon: ImageLoad(
                //         img: "/mobile_app_v2/o_notification_appbar.png",
                //         width: 30,
                //       ),
                //       onPressed: () {
                //         //successAlert(context, "Contact Copied Successfully...");
                //       }),
                // ),
              ],
            ),
            BodyWithTabSpace(
                page: MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) =>
                      OppoHeaderBloc(oppoRepository: _oppoRepository)
                        ..add(OppoHeaderPressed(displayAs: dynamicDisplayAs)),
                ),
                // BlocProvider(
                //   create: (context) => SubjectBloc(),
                // ),
              ],
              child: OpportuinitiesTabSection(displayAs: dynamicDisplayAs),
            )),
          ],
        ),
      ),
      floatingActionButton: OppoAddLeadButton(),
    );
  }
}

class OppoAddLeadButton extends StatelessWidget {
  const OppoAddLeadButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingButtonCustome(
      onPressed: () {
        addLeadCB(context);
      },
      icon: Icon(
        Icons.add,
      ),
    );
  }

  addLeadCB(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) =>
                  OppoDivisionDdBloc(oppoRepository: _oppoRepository)
                    ..add(OppoDivisionDdPressed()),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionFormDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionUserDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionSourceDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionStatusesDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionStagesDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionPriorityDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoFormFieldsBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  CountryBloc(globalRepository: _globalRepository)
                    ..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) =>
                  StateBloc(globalRepository: _globalRepository),
            ),
            BlocProvider(
              create: (context) =>
                  CityBloc(globalRepository: _globalRepository),
            ),
            BlocProvider(
              create: (context) => CustomDependentDropdownBloc(
                  genericRepository: _genericRepository),
            ),
          ],
          child: OppoAddLead(),
        ),
      ),
    );

    if (result == "success") {
      _oppoHeaderBloc.add(OppoHeaderPressed(displayAs: "status"));
      successAlert(context, "Lead Added Successfully...");
    }
  }
}

class OpportuinitiesTabSection extends StatelessWidget {
  final String displayAs;

  OpportuinitiesTabSection({Key key, this.displayAs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _oppoHeaderBloc = BlocProvider.of<OppoHeaderBloc>(context);
    return BlocBuilder<OppoHeaderBloc, OppoHeaderState>(
      builder: (context, state) {
        if (state is OppoHeaderFailure) {
          return FailedToLoad(message: state.error);
        }
        if (state is OppoHeaderSuccess) {
          return DefaultTabController(
            length: state.stateData.length,
            initialIndex: 0,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: TabBar(
                    isScrollable: true,
                    indicator: UnderlineTabIndicator(
                      borderSide: BorderSide(width: 4.0, color: orange),
                      insets: EdgeInsets.symmetric(horizontal: 16.0),
                    ),
                    tabs: state.stateData.map((tab) {
                      return Container(
                        height: 25,
                        child: Column(
                          children: [
                            //Text("23", style: TextStyle(fontSize: 20)),
                            Text(tab.headerDesc),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: state.stateData
                        .map((e) => OppoTabBody(
                              tabPage: BlocProvider(
                                create: (context) => OppoDataLoadBloc(
                                    oppoRepository: _oppoRepository)
                                  ..add(
                                    OppoDataLoadPressed(
                                      displayType: dynamicDisplayType,
                                      displayAs: displayAs,
                                      displayID: e.headerId,
                                      dateType: dateType,
                                      fromDate: fromDate,
                                      toDate: toDate,
                                      searchValue: searchValue,
                                      assignedTo: assignedTo,
                                    ),
                                  ),
                                child: OppoTabBodyDetails(data: e),
                              ),
                            ))
                        .toList(),
                  ),
                ),
              ],
            ),
          );
        }
        return FullyLoaderIndicator();
      },
    );
  }
}

class OppoTabBodyDetails extends StatefulWidget {
  final OppoHeader data;

  const OppoTabBodyDetails({Key key, this.data}) : super(key: key);

  @override
  _OppoTabBodyDetailsState createState() => _OppoTabBodyDetailsState();
}

class _OppoTabBodyDetailsState extends State<OppoTabBodyDetails> {
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  OppoDataLoadBloc _oppoDataLoadBloc;

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _oppoDataLoadBloc = BlocProvider.of<OppoDataLoadBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OppoDataLoadBloc, OppoDataLoadState>(
        builder: (context, state) {
      if (state is OppoDataLoadSuccess) {
        if (state.stateData.isEmpty) {
          return NoDataFound(message: "No Leads Found");
        }

        return ListView.builder(
          padding: EdgeInsets.all(0),
          itemBuilder: (BuildContext context, int index) {
            bool last = state.stateData.length == (index + 1);
            print(state.currentState.length);
            return index >= state.stateData.length
                ? state.currentState.length >= Environment.dataDisplayCount
                    ? LoaderSmallIndicator()
                    : Container()
                : LeadCardDetails(data: state.stateData[index], last: last);
          },
          itemCount: state.hasReachedMax
              ? state.stateData.length
              : state.stateData.length + 1,
          controller: _scrollController,
        );
      }
      if (state is OppoDataLoadFailure) {
        return FailedToLoad(message: state.error);
      }
      return FullyLoaderIndicator();
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _oppoDataLoadBloc.add(OppoDataLoadPressed(
        displayType: dynamicDisplayType,
        displayAs: dynamicDisplayAs,
        displayID: widget.data.headerId,
        dateType: dateType,
        fromDate: fromDate,
        toDate: toDate,
        searchValue: searchValue,
        assignedTo: assignedTo,
      ));
    }
  }
}

class LeadCardDetails extends StatelessWidget {
  final OppoLoadData data;
  final bool last;

  const LeadCardDetails({Key key, this.data, this.last}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      margin: last ? EdgeInsets.only(bottom: 80) : EdgeInsets.only(bottom: 15),
      decoration: oppoBox,
      child: Stack(
        children: [
          Positioned(
            top: 15,
            left: 15,
            right: 15,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 200,
                  child: customText(
                      "${data.sourceTypeDesc} - ${data.sourceDesc}", 14, black),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 25, 0),
                  child: Text(
                      "LS : ${data.leadScore} | CS : ${data.customerScore}"),
                ),
              ],
            ),
          ),
          Positioned(
            top: 40,
            left: 15,
            right: 15,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                customBoldText(data.fullName.capitalize(), 18, black),
                SizedBox(height: 10),
                Row(
                  children: [
                    ImageLoad(
                        img: '/mobile_app_v2/o_oppo_card_date.png', width: 18),
                    SizedBox(width: 5),
                    Text(
                        dateType == "created_datetime"
                            ? "  ${formateCallsLogDT(data.createdDatetime)}"
                            : "  ${formateCallsLogDT(DateTime.parse(data.modifiedDatetime))}",
                        style: TextStyle(color: textGray)),
                    SizedBox(width: 15),
                    ImageLoad(
                        img: '/mobile_app_v2/o_oppo_card_rupees.png',
                        width: 18),
                    SizedBox(width: 5),
                    Text("${data.budget}", style: TextStyle(color: textGray)),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            top: 100,
            left: 15,
            right: 15,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          oppoTransfer(context, data).then((value) {
                            if (value == "success") {
                              successAlert(
                                  context, "Lead Transfered Successfully");
                            } else if (value == "failure") {
                              errorAlert(context, "Failed to Transfer Lead");
                            }
                          });
                        },
                        child: RoundImage(
                          height: 30,
                          width: 30,
                          img: '/mobile_app_v2/o_oppo_charecter_1.png',
                        ),
                      ),
                      SizedBox(width: 5),
                      InkWell(
                        onTap: () {
                          oppoReassign(context, data).then((value) {
                            if (value == "success") {
                              successAlert(
                                  context, "Reasign Added Successfully");
                            } else if (value == "failure") {
                              errorAlert(context, "Failed to Add Reasign");
                            }
                          });
                        },
                        child: RoundImage(
                          height: 30,
                          width: 30,
                          img: '/mobile_app_v2/o_oppo_charecter_2.png',
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      RoundedImageBg(
                        width: 35,
                        height: 35,
                        img: "/mobile_app_v2/o_call_purple.png",
                        bgColor: bodyBG,
                        padding: 5,
                        onPressed: () {
                          oppoCall(context, data).then((value) {
                            if (value['status'] == 'success') {
                              successAlert(context, value['message']);
                            } else {
                              errorAlert(context, value['message']);
                            }
                          });
                        },
                      ),
                      SizedBox(width: 5),
                      // RoundedImageBg(
                      //   width: 35,
                      //   height: 35,
                      //   img: "/mobile_app_v2/o_text_purple.png",
                      //   bgColor: bodyBG,
                      //   padding: 5,
                      //   onPressed: () {
                      //     oppoSMS(context);
                      //   },
                      // ),
                      // SizedBox(width: 5),
                      RoundedImageBg(
                        width: 35,
                        height: 35,
                        img: "/mobile_app_v2/o_email_purple.png",
                        bgColor: bodyBG,
                        padding: 5,
                        onPressed: () {
                          oppoEmail(context, data);
                        },
                      ),
                      SizedBox(width: 5),
                      RoundedImageBg(
                        width: 35,
                        height: 35,
                        img: "/mobile_app_v2/o_lead_activity_purple.png",
                        bgColor: bodyBG,
                        padding: 5,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                create: (context) => OppoLeadActivityBloc(
                                    oppoRepository: _oppoRepository)
                                  ..add(OppoLeadActivityPressed(
                                      leadID: data.leadId,
                                      activityType: "All",
                                      fromDate: "",
                                      toDate: "")),
                                child: OppoLeadActivity(data: data),
                              ),
                            ),
                          );
                        },
                      ),
                      SizedBox(width: 5),
                      PopupMenuButton(
                        onSelected: (value) {
                          switch (value) {
                            case "leadDetails":
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => OppoDetails(data: data),
                                ),
                              );
                              break;
                            case "leadReassign":
                              oppoReassign(context, data).then((value) {
                                if (value == "success") {
                                  successAlert(
                                      context, "Reasign Added Successfully");
                                } else if (value == "failure") {
                                  errorAlert(context, "Failed to Add Reasign");
                                }
                              });
                              break;
                            case "leadTransfer":
                              oppoTransfer(context, data).then((value) {
                                if (value == "success") {
                                  successAlert(
                                      context, "Lead Transfered Successfully");
                                } else if (value == "failure") {
                                  errorAlert(
                                      context, "Failed to Transfer Lead");
                                }
                              });
                              break;
                            case "note":
                              oppoNote(context, data).then((value) {
                                if (value == "success") {
                                  successAlert(
                                      context, "Note Added Successfully");
                                } else if (value == "failure") {
                                  errorAlert(context, "Failed to Add Note");
                                }
                              });
                              break;
                            case "task":
                              oppoTask(context);
                              break;
                            case "meeting":
                              oppoMeeting(context, data).then((value) {
                                if (value == "success") {
                                  successAlert(
                                      context, "Meeting Added Successfully");
                                } else if (value == "failure") {
                                  errorAlert(context, "Failed to Add Meeting");
                                }
                              });
                              break;
                            case "reminder":
                              oppoReminder(context, data);
                              break;
                            case "editLead":
                              oppoEditLeadCB(context, data);
                              break;
                            // default:

                          }
                        },
                        itemBuilder: (context) => cardLeadOptions.map((e) {
                          return PopupMenuItem(
                            value: e.id,
                            child: Row(
                              children: [
                                ImageLoad(img: e.img, width: 25),
                                SizedBox(width: 15),
                                Text(e.title),
                              ],
                            ),
                          );
                        }).toList(),
                        icon: RoundedImageBg(
                          width: 35,
                          height: 35,
                          img: "/mobile_app_v2/o_more_options_purple.png",
                          bgColor: bodyBG,
                          padding: 5,
                        ),
                        offset: Offset(0, 20),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: 10,
            child: ClipPath(
              clipper: TicketShapeClipper(),
              child: Container(
                height: 25,
                width: 18,
                color: convertColor(data.priorityBgcolor),
              ),
            ),
          ),
        ],
      ),
    );
  }

  oppoEditLeadCB(BuildContext context, OppoLoadData data) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) =>
                  OppoDivisionDdBloc(oppoRepository: _oppoRepository)
                    ..add(OppoDivisionDdPressed()),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionFormDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionUserDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionSourceDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionStatusesDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionStagesDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoDivisionPriorityDdBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  OppoFormFieldsBloc(oppoRepository: _oppoRepository),
            ),
            BlocProvider(
              create: (context) =>
                  CountryBloc(globalRepository: _globalRepository)
                    ..add(CountryPressed()),
            ),
            BlocProvider(
              create: (context) =>
                  StateBloc(globalRepository: _globalRepository),
            ),
            BlocProvider(
              create: (context) =>
                  CityBloc(globalRepository: _globalRepository),
            ),
            BlocProvider(
              create: (context) => CustomDependentDropdownBloc(
                  genericRepository: _genericRepository),
            ),
          ],
          child: OppoEditLead(oppoLoadData: data),
        ),
      ),
    );

    // if (result == "success") {
    //   _oppoHeaderBloc.add(OppoHeaderPressed(displayAs: "status"));
    //   successAlert(context, "Lead Updated Successfully...");
    // }
  }
}
