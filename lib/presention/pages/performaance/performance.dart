import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:office24by7_v2/_data/api_calls/api_call.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

// ignore: must_be_immutable
class Performance extends StatefulWidget {
  @override
  _PerformanceState createState() => _PerformanceState();
}

class _PerformanceState extends State<Performance> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Performance",
              // actions: [
              //   IconButton(
              //     icon: Icon(Icons.clear_all),
              //     onPressed: () {
              //       setState(() {
              //         lstAPICalls = [];
              //       });
              //     },
              //   )
              // ],
            ),
            Positioned(
              top: 95,
              left: 0.0,
              right: 0.0,
              bottom: 0.0,
              child: Container(
                padding:
                    EdgeInsets.only(top: 0, bottom: 15, left: 15, right: 15),
                child: Container(
                  decoration: bodyBox,
                  child: ListView(
                    children: lstAPICalls.isEmpty
                        ? NoDataFound(
                            message: "No Data Found...",
                          )
                        : lstAPICalls
                            .map((e) => ListTile(
                                  title: Text("${e.url}"),
                                  subtitle: Text(
                                    "Loading Time: ${e.apiCallTime / 1000} sec",
                                    style: TextStyle(
                                        color: e.apiCallTime / 1000 > 1
                                            ? red
                                            : Colors.green),
                                  ),
                                  trailing: IconButton(
                                    onPressed: () {
                                      _showMyDialog(context, e);
                                    },
                                    icon: Icon(Icons.more_vert),
                                  ),
                                ))
                            .toList(),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

Future<void> _showMyDialog(context, APICall data) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('API Information...!'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Html(
                  data:
                      """<Strong>URL</Strong> :<br> <pre>${data.url}</pre>"""),
              SizedBox(height: 10),
              Html(
                  data:
                      """<Strong>method</Strong> :<br> <pre>${data.method}</pre>"""),
              SizedBox(height: 10),
              Html(
                  data:
                      """<Strong>Body</Strong> :<br> <pre>${data.body}</pre>"""),
              SizedBox(height: 10),
              Html(
                  data:
                      """<Strong>OutPut Response</Strong> :<br> <pre>${data.apiResponse.body}</pre>"""),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Close'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
