import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/loaders/image_load.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Container(
              width: 100,
              child: ImageLoad(img: "/mobile_app_v2/o_logo.png"),
            ),
          ),
          Center(
            child: Text("Office24by7"),
          ),
        ],
      ),
    );
  }
}
