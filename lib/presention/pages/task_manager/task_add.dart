import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/task/task_division/task_division_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_division_form/task_division_form_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_form_fields/task_form_fields_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_source/task_source_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_user_division/task_user_division_bloc.dart';
import 'package:office24by7_v2/_data/data_providers/data_provider.dart';
import 'package:office24by7_v2/_data/repositories/task_repository.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/_models/task_model/task_division_form.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/appbar/app_bar_normal.dart';
import 'package:office24by7_v2/presention/widgets/body/body_inner.dart';
import 'package:office24by7_v2/presention/widgets/buttons/buttons.dart';
import 'package:office24by7_v2/presention/widgets/curves/app_bar_curve.dart';
import 'package:office24by7_v2/presention/widgets/loaders/loader_indicator.dart';

class TaskAdd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Add Task",
            ),
            BodyInner(
              page: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: BlocProvider(
                  create: (context) => TaskDivisionBloc(
                      taskRepository:
                          TaskRepository(taskAPIClient: TaskAPIClient()))
                    ..add(TaskDivisionPressed()),
                  child: BlocProvider(
                    create: (context) => TaskDivisionFormBloc(
                        taskRepository:
                            TaskRepository(taskAPIClient: TaskAPIClient()))
                      ..add(TaskDivisionFormPressed()),
                    child: BlocProvider(
                      create: (context) => TaskUserDivisionBloc(
                          taskRepository:
                              TaskRepository(taskAPIClient: TaskAPIClient()))
                        ..add(TaskUserDivisionPressed()),
                      child: BlocProvider(
                        create: (context) => TaskSourceBloc(
                            taskRepository:
                                TaskRepository(taskAPIClient: TaskAPIClient()))
                          ..add(TaskSourcePressed()),
                        child: AddTaskForm(),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AddTaskForm extends StatefulWidget {
  const AddTaskForm({Key key}) : super(key: key);

  @override
  _AddTaskFormState createState() => _AddTaskFormState();
}

class _AddTaskFormState extends State<AddTaskForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      FormBuilder(
        key: _fbKey,
        autovalidateMode: AutovalidateMode.always,
        initialValue: {},
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FormBuilderTextField(
              name: "reminder_title",
              decoration: formGlobal("Title"),
              keyboardType: TextInputType.text,
              // validators: [
              //   FormBuilderValidators.required(),
              // ],
            ),
            fieldDistance,
            BlocBuilder<TaskDivisionBloc, TaskDivisionState>(
              builder: (context, state) {
                if (state is TaskDivisionSuccess) {
                  return FormBuilderDropdown(
                    name: "Name",
                    decoration: formGlobal("Select Division"),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: state.statedata
                        .map((e) => DropdownMenuItem(
                            value: e.divisionId, child: Text(e.divisionName)))
                        .toList(),
                  );
                }
                if (state is TaskDivisionFailure) {
                  return Text(state.error);
                }
                return LoaderSmallIndicator();
              },
            ),
            fieldDistance,
            BlocBuilder<TaskDivisionFormBloc, TaskDivisionFormState>(
              builder: (context, state) {
                if (state is TaskDivisionFormSuccess) {
                  return FormBuilderDropdown(
                    name: "Name",
                    decoration: formGlobal("Select Custom Form"),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: state.statedata
                        .map((e) => DropdownMenuItem(
                            value: e.formId, child: Text(e.formDesc)))
                        .toList(),
                  );
                }
                if (state is TaskDivisionFormFailure) {
                  return Text(state.error);
                }
                return LoaderSmallIndicator();
              },
            ),
            fieldDistance,
            BlocBuilder<TaskUserDivisionBloc, TaskUserDivisionState>(
              builder: (context, state) {
                if (state is TaskUserDivisionSuccess) {
                  return FormBuilderDropdown(
                    name: "Name",
                    decoration: formGlobal("Select Assign Users"),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: state.statedata
                        .map((e) => DropdownMenuItem(
                            value: e.userId, child: Text(e.userName)))
                        .toList(),
                  );
                }
                if (state is TaskUserDivisionFailure) {
                  return Text(state.error);
                }
                return LoaderSmallIndicator();
              },
            ),
            fieldDistance,
            BlocBuilder<TaskSourceBloc, TaskSourceState>(
              builder: (context, state) {
                if (state is TaskSourceSuccess) {
                  return FormBuilderDropdown(
                    name: "Name",
                    decoration: formGlobal("Select Task Type"),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: state.statedata
                        .map((e) => DropdownMenuItem(
                            value: e.sourceId, child: Text(e.sourceTypeName)))
                        .toList(),
                  );
                }
                if (state is TaskSourceFailure) {
                  return Text(state.error);
                }
                return LoaderSmallIndicator();
              },
            ),
            fieldDistance,
            Row(
              children: [
                Expanded(
                  child: FormBuilderDateTimePicker(
                    name: "Name",
                    inputType: InputType.date,
                    firstDate: DateTime.now(),
                    decoration: formGlobal(""),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: FormBuilderDateTimePicker(
                    name: "Name",
                    inputType: InputType.time,
                    firstDate: DateTime.now(),
                    decoration: formGlobal(" "),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                  ),
                ),
              ],
            ),
            fieldDistance,
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RaisedGradientButton(
                  child: customText('Reset', 16, black),
                  gradient: LinearGradient(colors: grayTtL),
                  width: 100,
                  onPressed: () {
                    _fbKey.currentState.reset();
                  },
                ),
                SizedBox(width: 10),
                RaisedGradientButton(
                  child: buttonwhiteText('Submit'),
                  gradient: LinearGradient(colors: purpleTtL),
                  width: 100,
                  onPressed: () {
                    //Navigator.pushNamed(context, '/contacts');
                    if (_fbKey.currentState.saveAndValidate()) {
                      print(_fbKey.currentState.value);
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    ]);
  }
}
