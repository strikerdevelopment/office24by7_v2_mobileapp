import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_blocs/task/task_division_form/task_division_form_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_priorities/task_priorities_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_source/task_source_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_status/task_status_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_user_division/task_user_division_bloc.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_data/repositories/repository.dart';
import 'package:office24by7_v2/_models/task_model/task_division_form.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class TaskEdit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Edit Task",
            ),
            BodyInner(
              page: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: BlocProvider(
                  create: (context) => TaskStatusBloc(
                      taskRepository:
                          TaskRepository(taskAPIClient: TaskAPIClient()))
                    ..add(TaskStatusPressed()),
                  child: BlocProvider(
                    create: (context) => TaskPrioritiesBloc(
                        taskRepository:
                            TaskRepository(taskAPIClient: TaskAPIClient()))
                      ..add(TaskPrioritiesPressed()),
                    child: BlocProvider(
                      create: (context) => TaskDivisionFormBloc(
                          taskRepository:
                              TaskRepository(taskAPIClient: TaskAPIClient()))
                        ..add(TaskDivisionFormPressed()),
                      child: BlocProvider(
                        create: (context) => TaskUserDivisionBloc(
                            taskRepository:
                                TaskRepository(taskAPIClient: TaskAPIClient()))
                          ..add(TaskUserDivisionPressed()),
                        child: BlocProvider(
                          create: (context) => TaskSourceBloc(
                              taskRepository: TaskRepository(
                                  taskAPIClient: TaskAPIClient()))
                            ..add(TaskSourcePressed()),
                          child: EditTaskForm(),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class EditTaskForm extends StatefulWidget {
  const EditTaskForm({Key key}) : super(key: key);

  @override
  _EditTaskFormState createState() => _EditTaskFormState();
}

class _EditTaskFormState extends State<EditTaskForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      FormBuilder(
        key: _fbKey,
        autovalidateMode: AutovalidateMode.always,
        initialValue: {},
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FormBuilderTextField(
              name: "reminder_title",
              decoration: formGlobal("Title"),
              keyboardType: TextInputType.text,
              // validators: [
              //   FormBuilderValidators.required(),
              // ],
            ),
            fieldDistance,
            Row(
              children: [
                Expanded(
                  child: BlocBuilder<TaskStatusBloc, TaskStatusState>(
                      builder: (context, state) {
                    if (state is TaskStatusSuccess) {
                      return FormBuilderDropdown(
                        name: "Name",
                        decoration: formGlobal("Status"),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: state.statedata
                            .map((e) => DropdownMenuItem(
                                value: e.statusId, child: Text(e.statusDesc)))
                            .toList(),
                      );
                    }
                    if (state is TaskStatusFailure) {
                      return Text(state.error);
                    }
                    return LoaderSmallIndicator();
                  }),
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: BlocBuilder<TaskPrioritiesBloc, TaskPrioritiesState>(
                    builder: (context, state) {
                      if (state is TaskPrioritiesSuccess) {
                        return FormBuilderDropdown(
                          name: "Name",
                          decoration: formGlobal("Priority"),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: state.statedata
                              .map((e) => DropdownMenuItem(
                                  value: e.priorityId,
                                  child: Text(e.priorityDesc)))
                              .toList(),
                        );
                      }
                      if (state is TaskPrioritiesFailure) {
                        return Text(state.error);
                      }
                      return LoaderSmallIndicator();
                    },
                  ),
                ),
              ],
            ),
            fieldDistance,
            BlocBuilder<TaskDivisionFormBloc, TaskDivisionFormState>(
              builder: (context, state) {
                if (state is TaskDivisionFormSuccess) {
                  return FormBuilderDropdown(
                    name: "Name",
                    decoration: formGlobal("Task Form"),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: state.statedata
                        .map((e) => DropdownMenuItem(
                            value: e.formId, child: Text(e.formDesc)))
                        .toList(),
                  );
                }
                if (state is TaskDivisionFormFailure) {
                  return Text(state.error);
                }
                return LoaderSmallIndicator();
              },
            ),
            fieldDistance,
            BlocBuilder<TaskUserDivisionBloc, TaskUserDivisionState>(
              builder: (context, state) {
                if (state is TaskUserDivisionSuccess) {
                  return FormBuilderDropdown(
                    name: "Name",
                    decoration: formGlobal("Assign to"),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: state.statedata
                        .map((e) => DropdownMenuItem(
                            value: e.userId, child: Text(e.userName)))
                        .toList(),
                  );
                }
                if (state is TaskUserDivisionFailure) {
                  return Text(state.error);
                }
                return LoaderSmallIndicator();
              },
            ),
            fieldDistance,
            BlocBuilder<TaskSourceBloc, TaskSourceState>(
              builder: (context, state) {
                if (state is TaskSourceSuccess) {
                  return FormBuilderDropdown(
                    name: "Name",
                    decoration: formGlobal("Task Type"),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required(context)]),
                    items: state.statedata
                        .map((e) => DropdownMenuItem(
                            value: e.sourceId, child: Text(e.sourceTypeName)))
                        .toList(),
                  );
                }
                if (state is TaskSourceFailure) {
                  return Text(state.error);
                }
                return LoaderSmallIndicator();
              },
            ),
            fieldDistance,
            FormBuilderTextField(
              name: "reminder_title",
              decoration: formGlobal("Description"),
              keyboardType: TextInputType.text,
              // validators: [
              //   FormBuilderValidators.required(),
              // ],
            ),
            fieldDistance,
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RaisedGradientButton(
                  child: customText('Reset', 16, black),
                  gradient: LinearGradient(colors: grayTtL),
                  width: 100,
                  onPressed: () {
                    _fbKey.currentState.reset();
                  },
                ),
                SizedBox(width: 10),
                RaisedGradientButton(
                  child: buttonwhiteText('Submit'),
                  gradient: LinearGradient(colors: purpleTtL),
                  width: 100,
                  onPressed: () {
                    //Navigator.pushNamed(context, '/contacts');
                    if (_fbKey.currentState.saveAndValidate()) {
                      print(_fbKey.currentState.value);
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    ]);
  }
}
