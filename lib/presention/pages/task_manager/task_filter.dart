import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/_models/global/global_model.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/styles/input_decorations.dart';
import 'package:office24by7_v2/presention/widgets/dialogbox/modal_box.dart';
import 'package:office24by7_v2/presention/widgets/dialogbox/modal_footer.dart';
import 'package:office24by7_v2/presention/widgets/dialogbox/modal_header.dart';

Future taskFilter(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);
      return ModalBox(
        modalHieght: 400,
        modalHeader: ModalHeader(
            img: "/mobile_app_v2/o_option_filter.png", title: "Filter"),
        modalBody: Expanded(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: ListView(children: [
              FormBuilder(
                key: _fbKey,
                autovalidateMode: AutovalidateMode.always,
                initialValue: {},
                child: Column(
                  children: [
                    FormBuilderRadioGroup<String>(
                      name: "display_as",
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                      activeColor: purple,
                      options: taskFilterData
                          .map((data) => FormBuilderFieldOption(
                                value: data.type,
                                // ignore: deprecated_member_use
                                child: Text(data.name),
                              ))
                          .toList(growable: false),
                    ),
                    fieldDistance,
                  ],
                ),
              ),
            ]),
          ),
        ),
        modalFooter: ModalFooter(
          firstButton: "Cancel",
          firstFun: () {
            Navigator.pop(context);
          },
          secondButton: "Submit",
          secondFun: () {
            if (_fbKey.currentState.saveAndValidate()) {
              Map formData = _fbKey.currentState.value;
              Navigator.pop(context, formData);
              // print(formData);
            }
          },
        ),
      );
    },
  );
}

List<TypeName> taskFilterData = [
  TypeName(type: "all", name: "All"),
  TypeName(type: "my_contacts", name: "My Contacts"),
  TypeName(type: "modified_by_me", name: "Modified By Me"),
  TypeName(type: "created_by_me", name: "Created By Me"),
];
