import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_records/task_records_bloc.dart';
import 'package:office24by7_v2/_data/data_providers/data_provider.dart';
import 'package:office24by7_v2/_data/repositories/repository.dart';
import 'package:office24by7_v2/_models/task_model/task_card_options.dart';
import 'package:office24by7_v2/_models/task_model/task_records.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_add.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_attachment.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_edit.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_history.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_note.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_reassign.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_reminder.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_subtask.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_transfer.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/styles/input_decorations.dart';
import 'package:office24by7_v2/presention/styles/paddings.dart';
import 'package:office24by7_v2/presention/widgets/appbar/appbar.dart';
import 'package:office24by7_v2/presention/widgets/body/body_inner.dart';
import 'package:office24by7_v2/presention/widgets/curves/app_bar_curve.dart';
import 'package:office24by7_v2/presention/widgets/data_loaders/data_loaders.dart';
import 'package:office24by7_v2/presention/widgets/loaders/image_load.dart';
import 'package:office24by7_v2/presention/widgets/loaders/loader_indicator.dart';

class TaskGlobalSearch extends StatelessWidget {
  const TaskGlobalSearch({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: TaskSearchData(),
      ),
    );
  }
}

class TaskSearchData extends StatefulWidget {
  final TaskDataModel taskData;
  const TaskSearchData({Key key, this.taskData}) : super(key: key);

  @override
  _TaskSearchDataState createState() => _TaskSearchDataState();
}

class _TaskSearchDataState extends State<TaskSearchData> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppbarCurve(),
        AppbarNormal(
          title: "Global Search",
        ),
        BodyInner(
            page: Container(
          padding: tabBodyPadd,
          child: Column(
            children: [
              TextFormField(
                decoration: searchInput("Search"),
                keyboardType: TextInputType.text,
                onChanged: (value) {},
              ),
              SizedBox(height: 8),
              Expanded(
                child: BlocProvider(
                  create: (context) => TaskRecordsBloc(
                      taskRepository:
                          TaskRepository(taskAPIClient: TaskAPIClient()))
                    ..add(
                      TaskRecordsPressed(
                        displayType: 'all',
                        globalSearchValue: '',
                        displayAs: 'Status',
                        limit: '20',
                        offset: '0',
                        searchDateFrom: '',
                        searchDateTo: '',
                        searchDuration: '',
                        searchStatusId: '',
                        searchassignedTo: '',
                      ),
                    ),
                  child: BlocBuilder<TaskRecordsBloc, TaskRecordsState>(
                    builder: (context, state) {
                      if (state is TaskRecordsSuccess) {
                        return TaskSearchBody(taskData: state.statedata);
                      }
                      if (state is TaskRecordsFailure) {
                        return FailedToLoad(message: state.error);
                      }
                      return LoaderSmallIndicator();
                    },
                  ),
                ),
              ),
            ],
          ),
        )),
      ],
    );
  }
}

class TaskSearchBody extends StatefulWidget {
  final TaskDataModel taskData;
  const TaskSearchBody({Key key, this.taskData}) : super(key: key);

  @override
  _TaskSearchBodyState createState() => _TaskSearchBodyState();
}

class _TaskSearchBodyState extends State<TaskSearchBody> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        BlocBuilder<TaskRecordsBloc, TaskRecordsState>(
            builder: (context, state) {
          return ListView(
            children: widget.taskData.taskData
                .map((_) => Padding(
                      padding: cardPadd,
                      child: Row(
                        children: [
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius: BorderRadius.circular(50)),
                            child: IconButton(
                                onPressed: () {}, icon: Icon(Icons.assignment)),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  _.taskTitle,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.arrow_forward,
                                      color: purple,
                                      size: 16,
                                    ),
                                    Text(
                                      _.taskDesc,
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          PopupMenuButton(
                            onSelected: (value) {
                              print(value);
                              switch (value) {
                                case 'editTask':
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TaskEdit(),
                                    ),
                                  );
                                  break;
                                case 'addTask':
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => TaskAdd(),
                                    ),
                                  );
                                  break;
                                case 'taskReassign':
                                  taskReassign(context);
                                  break;
                                case 'taskTransfer':
                                  tasktransfer(context);
                                  break;
                                case 'tasknote':
                                  tasknote(context);
                                  break;
                                case 'taskhistory':
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Taskhistory(),
                                    ),
                                  );
                                  break;
                                case 'taskReminder':
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Taskreminder(),
                                    ),
                                  );
                                  break;

                                case 'taskAttachment':
                                  taskattachment(context);
                                  break;
                                case 'subtask':
                                  subtask(context);
                                  break;
                                default:
                              }
                            },
                            itemBuilder: (context) => taskCardOptions.map((e) {
                              return PopupMenuItem(
                                value: e.id,
                                child: Row(
                                  children: [
                                    ImageLoad(img: e.img, width: 25),
                                    SizedBox(width: 15),
                                    Text(e.title),
                                  ],
                                ),
                              );
                            }).toList(),
                            icon: SizedBox(
                              width: 20,
                              height: 20,
                              child: ImageLoad(
                                img: '/mobile_app_v2/o_options_card.png',
                                width: 20,
                              ),
                            ),
                            offset: Offset(0, 20),
                          ),
                        ],
                      ),
                    ))
                .toList(),
          );
        }),
      ],
    );
  }
}
