import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class Taskhistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Task History",
            ),
            BodyInner(
              page: Container(
                color: Colors.white,
                height: double.infinity,
                child: ListView(
                  children: [
                    Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 30, left: 30, right: 10),
                          height: 1,
                          color: Colors.black,
                        ),
                        Container(
                          color: Colors.white,
                          margin: EdgeInsets.only(left: 60, top: 17),
                          child: Text("Today - Thursday, May 7, 2020"),
                        ),
                      ],
                    ),
                    Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 30),
                          height: 800,
                          width: 1,
                          color: Colors.black,
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 30, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.assignment_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 40),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 40,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 40,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Update",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "09:25AM | Task details are updated ",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 120, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.double_arrow_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 120),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 120,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 120,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Reassign",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "10:05AM | Task Reassigned ",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 200, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.attachment_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 200),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 200,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 200,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Attachment",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "03:12AM | Attachment the document ",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 280, left: 30, right: 10),
                          height: 1,
                          color: Colors.black,
                        ),
                        Container(
                          color: Colors.white,
                          margin: EdgeInsets.only(left: 60, top: 272),
                          child: Text("Yesterday - wednesday, May 6, 2020"),
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 320, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.assignment_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 320),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 320,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 320,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Subtask",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "09:47AM | subtask assign by jason david ",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 400, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.login_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 400),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 400,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 400,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Transfer",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "04:22AM | Lead transfered to jason david",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 480, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.alarm_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 480),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 480,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 480,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Reminder",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "05:14PM | Lead Transfered to jason david ",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 560, left: 30, right: 10),
                          height: 1,
                          color: Colors.black,
                        ),
                        Container(
                          color: Colors.white,
                          margin: EdgeInsets.only(left: 60, top: 552),
                          child: Text("Tuesday, May 5, 2020"),
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 600, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.assignment_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 600),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 600,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 600,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Update",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "09:25AM | Task details are updated ",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 680, left: 5),
                              height: 50,
                              width: 50,
                              child: Icon(
                                Icons.double_arrow_outlined,
                                color: Color(0xFF7469EC),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Color(0xFF7469EC),
                                  )),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 680),
                              height: 1,
                              width: 20,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 680,
                              ),
                              height: 10,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Color(0xFF7469EC),
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 680,
                                left: 10,
                              ),
                              height: 40,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Reassign",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "09:25AM | Task details are updated ",
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
//           BodyInner(
//             page: Column(
//               children: [
//                 Container(
//                   padding: tabBodyPadd,
//                   child: Row(
//                     children: [
//                       Container(
//                         margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
//                         child: ClipRRect(
//                           borderRadius: BorderRadius.all(
//                             Radius.circular(250),
//                           ),
//                           child: Container(
//                             decoration: roundShadow,
//                             height: 50,
//                             width: 50,
//                             child: ImageLoad(
//                                 img: '/mobile_app_v2/o_profile_pic.png'),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Container(
//                   decoration: BoxDecoration(
//                     border:
//                         Border(bottom: BorderSide(width: 1, color: textGray)),
//                   ),
//                 ),
//                 Expanded(
//                   child: ListView(
//                       padding:
//                           EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       children: [
//                         Column(
//                           children: [
//                             HistoryDataDesign(date: e.historydate),
//                             Column(
//                               children: e.historyrepeat
//                                   .map(
//                                     (e) => Material(
//                                       color: white,
//                                       child: InkWell(
//                                         onTap: () =>
//                                             contactHistoryDetails(context, e),
//                                         child: HistoryStatusDesign(
//                                           time: e.time,
//                                           type: e.type,
//                                           description: e.description,
//                                           image: historyIcon(e.type),
//                                         ),
//                                       ),
//                                     ),
//                                   )
//                                   .toList(),
//                             ),
//                           ],
//                         ),
//                       ]),
//                 ),
//               ],
//             ),
//           ),
//         ]),
//       ),
//     );
//   }
// }

// String taskhistoryIcon(String type) {
//   print(type);
//   switch (type) {
//     case "Update":
//       return "/mobile_app_v2/o_option_task.png";
//       break;
//     case "Reassign":
//       return "/mobile_app_v2/o_option_reassign.png";
//       break;
//     case "Meeting":
//       return "/mobile_app_v2/o_option_meeting.png";
//       break;
//     case "EMAIL":
//       return "/mobile_app_v2/o_email_purple.png";
//       break;
//     case "SMS":
//       return "/mobile_app_v2/o_text_purple.png";
//       break;
//     case "Lead":
//       return "/mobile_app_v2/o_option_add_to_lead.png";
//       break;
//     case "List":
//       return "/mobile_app_v2/o_option_add_to_list.png";
//       break;
//     case "Call":
//       return "/mobile_app_v2/o_call_purple.png";
//       break;
//     case "OutgoingCall":
//       return "/mobile_app_v2/o_option_voice.png";
//       break;
//     default:
//       return "/mobile_app_v2/o_option_task.png";
//   }
// }

// Future taskHistoryDetails(BuildContext context, Historyrepeat data) {
//   return showDialog(
//     context: context,
//     builder: (BuildContext context) {
//       return StatefulBuilder(
//         builder: (BuildContext context, setState) {
//           return ModalBox(
//             modalHieght: 700,
//             modalHeader: ModalHeader(
//                 img: "/mobile_app_v2/o_option_contact_details.png",
//                 title: "Contact History Details"),
//             modalBody: Expanded(
//               child: Padding(
//                 padding: const EdgeInsets.all(20),
//                 child: ListView(children: [
//                   Column(
//                     children: [
//                       ListTile(
//                         title: Text('Time'),
//                         subtitle: Text('${data.time}'),
//                       ),
//                       ListTile(
//                         title: Text('Type'),
//                         subtitle: Text('${data.type}'),
//                       ),
//                       ListTile(
//                         title: Text('AddedBy'),
//                         subtitle: Text('${data.addedBy}'),
//                       ),
//                       ListTile(
//                         title: Text('SPOC'),
//                         subtitle: Text('${data.spoc}'),
//                       ),
//                       // ListTile(
//                       //   title: Text('typeimg'),
//                       //   subtitle: Text("${data.typeimg}"),
//                       // ),
//                       ListTile(
//                         title: Text('Description'),
//                         subtitle: Html(data: """${data.description}"""),
//                       ),
//                       // ListTile(
//                       //   title: Text('Attachments'),
//                       //   subtitle: Text(data.activityType),
//                       // ),
//                     ],
//                   ),
//                 ]),
//               ),
//             ),
//             modalFooter: ModalTextFooter(),
//           );
//         },
//       );
//     },
//   );
// }

// class HistoryDataDesign extends StatelessWidget {
//   final String date;

//   const HistoryDataDesign({Key key, this.date}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: double.infinity,
//       child: Stack(
//         children: [
//           Positioned(
//             top: 23,
//             left: 22,
//             right: 0,
//             child: Container(
//               decoration: BoxDecoration(
//                 border: Border(bottom: BorderSide(width: 1, color: textGray)),
//               ),
//             ),
//           ),
//           Container(
//             padding: EdgeInsets.fromLTRB(5, 15, 5, 0),
//             margin: EdgeInsets.only(left: 55),
//             child: Text("$date"),
//             color: white,
//           ),
//         ],
//       ),
//     );
//   }
// }

// class HistoryStatusDesign extends StatelessWidget {
//   final String type;
//   final String time;
//   final String description;
//   final String image;

//   const HistoryStatusDesign(
//       {Key key, this.type, this.time, this.description, this.image})
//       : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: double.infinity,
//       child: Padding(
//         padding: const EdgeInsets.only(top: 15, bottom: 5),
//         child: Stack(
//           clipBehavior: Clip.none,
//           children: [
//             Positioned(
//               top: 22,
//               left: 25,
//               child: Container(
//                 width: 40,
//                 decoration: BoxDecoration(
//                   border: Border(bottom: BorderSide(width: 1, color: textGray)),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: -23,
//               left: 22,
//               child: Container(
//                 width: 1,
//                 height: 40,
//                 decoration: BoxDecoration(
//                   border: Border(left: BorderSide(width: 1, color: textGray)),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 34,
//               left: 22,
//               child: Container(
//                 width: 1,
//                 height: 40,
//                 decoration: BoxDecoration(
//                   border: Border(left: BorderSide(width: 1, color: textGray)),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 18,
//               left: 60,
//               child: Container(
//                 width: 10,
//                 height: 10,
//                 decoration: BoxDecoration(
//                   color: dartpurple,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//               ),
//             ),
//             Positioned(
//               child: Container(
//                 width: 45,
//                 height: 45,
//                 padding: EdgeInsets.all(8),
//                 decoration: BoxDecoration(
//                   color: bodyBG,
//                   borderRadius: BorderRadius.circular(50),
//                 ),
//                 child: ImageLoad(
//                   img: image,
//                   width: 20,
//                 ),
//               ),
//             ),
//             Container(
//               margin: EdgeInsets.only(left: 80),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Text(
//                     type,
//                     style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
//                   ),
//                   SizedBox(height: 5),
//                   customText("$time | $description", 13, black),
//                 ],
//               ),
//             ),
          ],
        ),
      ),
    );
  }
}
