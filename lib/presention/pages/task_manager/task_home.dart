import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/task/task_records/task_records_bloc.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/pages/pages.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_filter.dart';
import 'package:office24by7_v2/presention/pages/task_manager/task_globalsearch.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class TaskHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => TaskRecordsBloc(
            taskRepository: TaskRepository(taskAPIClient: TaskAPIClient()))
          ..add(
            TaskRecordsPressed(
              displayType: 'all',
              globalSearchValue: '',
              displayAs: 'Status',
              limit: '20',
              offset: '0',
              searchDateFrom: '',
              searchDateTo: '',
              searchDuration: '',
              searchStatusId: '',
              searchassignedTo: '',
            ),
          ),
        child: BlocBuilder<TaskRecordsBloc, TaskRecordsState>(
          builder: (context, state) {
            if (state is TaskRecordsSuccess) {
              return TaskBody(taskData: state.statedata);
            }
            if (state is TaskRecordsFailure) {
              return FailedToLoad(message: state.error);
            }
            return LoaderSmallIndicator();
          },
        ),
      ),
    );
  }
}

class TaskBody extends StatefulWidget {
  final TaskDataModel taskData;
  const TaskBody({Key key, this.taskData}) : super(key: key);

  @override
  _TaskBodyState createState() => _TaskBodyState();
}

class _TaskBodyState extends State<TaskBody>
    with SingleTickerProviderStateMixin {
  List<TaskStatus> tabAll = [
    TaskStatus(
        statusId: "",
        statusDesc: "All",
        statusOrder: "0",
        fontColor: "",
        backgroundColor: "",
        leadCnt: "",
        header: null,
        data: null)
  ];

  List<TaskStatus> tabs = [];

  TabController _controller;

  @override
  void initState() {
    if (widget.taskData.status != null) {
      tabs = widget.taskData.status;
    }
    tabs = [
      ...tabAll,
      ...widget.taskData.status,
    ];
    _controller = TabController(length: tabs.length, vsync: this);
    _controller.addListener(() {
      BlocProvider.of<TaskRecordsBloc>(context).add(
        TaskRecordsPressed(
          displayType: tabs[_controller.index].statusDesc,
          globalSearchValue: '',
          displayAs: 'Status',
          limit: '20',
          offset: '0',
          searchDateFrom: '',
          searchDateTo: '',
          searchDuration: '',
          searchStatusId: tabs[_controller.index].statusId,
          searchassignedTo: '',
        ),
      );
      print(_controller.index);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      initialIndex: 0,
      child: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Task Page",
              actions: [
                Badge(
                  position: BadgePosition.topEnd(top: 14, end: -1),
                  elevation: 0,
                  shape: BadgeShape.circle,
                  badgeColor: Colors.amber,
                  borderSide: BorderSide(color: Colors.white),
                  badgeContent: Text(
                    '5',
                    style: TextStyle(color: Colors.white, fontSize: 8),
                  ),
                  child: Icon(
                    Icons.notifications_outlined,
                    size: 25,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                IconButton(
                  icon: Icon(Icons.filter_alt_outlined),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => taskFilter(context)),
                    // );
                  },
                ),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
            BodyWithTabSpace(
              page: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                    child: TabBar(
                        isScrollable: true,
                        controller: _controller,
                        indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(width: 4.0, color: orange),
                          insets: EdgeInsets.symmetric(horizontal: 16.0),
                        ),
                        tabs: tabs
                            .map(
                              (status) => Container(
                                height: 25,
                                child: Column(
                                  children: [
                                    Text(status.statusDesc),
                                  ],
                                ),
                              ),
                            )
                            .toList()),
                  ),
                  Expanded(
                    child: BlocBuilder<TaskRecordsBloc, TaskRecordsState>(
                      builder: (context, state) {
                        return TabBarView(
                          children: tabs
                              .map(
                                (e) => TabBody(
                                  tabPage: Container(
                                    padding: tabBodyPadd,
                                    child: Column(
                                      children: [
                                        SearchButton(
                                            text: "Search Task",
                                            onClick: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TaskGlobalSearch()),
                                              );
                                            }),
                                        SizedBox(height: 8),
                                        Expanded(
                                          child: ListView(
                                            children: widget.taskData.taskData
                                                .map((_) => Padding(
                                                      padding: cardPadd,
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height: 40,
                                                            width: 40,
                                                            decoration: BoxDecoration(
                                                                color: Colors
                                                                    .grey[100],
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            50)),
                                                            child: IconButton(
                                                                onPressed:
                                                                    () {},
                                                                icon: Icon(Icons
                                                                    .assignment)),
                                                          ),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Text(
                                                                  _.taskTitle,
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          15),
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                ),
                                                                Row(
                                                                  children: [
                                                                    Icon(
                                                                      Icons
                                                                          .arrow_forward,
                                                                      color:
                                                                          purple,
                                                                      size: 16,
                                                                    ),
                                                                    Text(
                                                                      _.taskDesc,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              12),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          PopupMenuButton(
                                                            onSelected:
                                                                (value) {
                                                              print(value);
                                                              switch (value) {
                                                                case 'editTask':
                                                                  Navigator
                                                                      .push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              TaskEdit(),
                                                                    ),
                                                                  );
                                                                  break;
                                                                case 'addTask':
                                                                  Navigator
                                                                      .push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              TaskAdd(),
                                                                    ),
                                                                  );
                                                                  break;
                                                                case 'taskReassign':
                                                                  taskReassign(
                                                                      context);
                                                                  break;
                                                                case 'taskTransfer':
                                                                  tasktransfer(
                                                                      context);
                                                                  break;
                                                                case 'tasknote':
                                                                  tasknote(
                                                                      context);
                                                                  break;
                                                                case 'taskhistory':
                                                                  Navigator
                                                                      .push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              Taskhistory(),
                                                                    ),
                                                                  );
                                                                  break;
                                                                case 'taskReminder':
                                                                  Navigator
                                                                      .push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              Taskreminder(),
                                                                    ),
                                                                  );
                                                                  break;

                                                                case 'taskAttachment':
                                                                  taskattachment(
                                                                      context);
                                                                  break;
                                                                case 'subtask':
                                                                  subtask(
                                                                      context);
                                                                  break;
                                                                default:
                                                              }
                                                            },
                                                            itemBuilder:
                                                                (context) =>
                                                                    taskCardOptions
                                                                        .map(
                                                                            (e) {
                                                              return PopupMenuItem(
                                                                value: e.id,
                                                                child: Row(
                                                                  children: [
                                                                    ImageLoad(
                                                                        img: e
                                                                            .img,
                                                                        width:
                                                                            25),
                                                                    SizedBox(
                                                                        width:
                                                                            15),
                                                                    Text(e
                                                                        .title),
                                                                  ],
                                                                ),
                                                              );
                                                            }).toList(),
                                                            icon: SizedBox(
                                                              width: 20,
                                                              height: 20,
                                                              child: ImageLoad(
                                                                img:
                                                                    '/mobile_app_v2/o_options_card.png',
                                                                width: 20,
                                                              ),
                                                            ),
                                                            offset:
                                                                Offset(0, 20),
                                                          ),
                                                        ],
                                                      ),
                                                    ))
                                                .toList(),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: FloatingButtonCustome(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TaskAdd(),
                    ),
                  );
                  // errorAlert(
                  //   context,
                  //   "You don't have Previllages, please contact to Support Team...",
                  // );
                },
                icon: Icon(
                  Icons.add,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
