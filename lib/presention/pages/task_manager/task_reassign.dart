import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

Future taskReassign(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
      // final ValueChanged _onChanged = (val) => print(val);

      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return ModalBox(
            modalHieght: 300,
            modalHeader: ModalHeader(
                img: "/mobile_app_v2/o_option_reassign.png",
                title: "Task Reassign"),
            modalBody: Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(children: [
                  FormBuilder(
                    key: _fbKey,
                    autovalidateMode: AutovalidateMode.always,
                    initialValue: {},
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Reassign to", style: TextStyle(fontSize: 20)),
                        FormBuilderDropdown(
                          name: "Name",
                          decoration: formGlobal("Select User to Reassign"),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: ["Jason David", "Thomas Cook"]
                              .map((Name) => DropdownMenuItem(
                                  value: "title", child: Text("$Name")))
                              .toList(),
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ),
            modalFooter: ModalFooter(
              firstButton: "Cancel",
              firstFun: () {
                Navigator.pop(context);
              },
              secondButton: "Submit",
              secondFun: () async {
                if (_fbKey.currentState.saveAndValidate()) {
                  Map formData = _fbKey.currentState.value;
                  Navigator.pop(context);
                  print(formData);
                }
                //Navigator.pop(context);
              },
            ),
          );
        },
      );
    },
  );
}
