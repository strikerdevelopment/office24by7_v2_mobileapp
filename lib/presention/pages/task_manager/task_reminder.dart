import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/appbar/appbar.dart';
import 'package:office24by7_v2/presention/widgets/body/body.dart';
import 'package:office24by7_v2/presention/widgets/buttons/buttons.dart';
import 'package:office24by7_v2/presention/widgets/curves/app_bar_curve.dart';

class Taskreminder extends StatelessWidget {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: bodyBG,
      child: Stack(
        children: [
          AppbarCurve(),
          AppbarNormal(
            title: "Reminder",
          ),
          BodyInner(
            page: Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: ListView(children: [
                  FormBuilder(
                    key: _fbKey,
                    autovalidateMode: AutovalidateMode.always,
                    initialValue: {},
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FormBuilderTextField(
                          name: "reminder_title",
                          decoration: formGlobal("Title"),
                          keyboardType: TextInputType.text,
                          // validators: [
                          //   FormBuilderValidators.required(),
                          // ],
                        ),
                        fieldDistance,
                        Row(
                          children: [
                            Expanded(
                              child: FormBuilderDateTimePicker(
                                name: 'reminder_date',
                                onChanged: (value) {
                                  // if (value == null) {
                                  //   setState(() {
                                  //     repeatDate = false;
                                  //   });
                                  // } else {
                                  //   setState(() {
                                  //     repeatDate = true;
                                  //   });
                                  // }
                                },
                                inputType: InputType.date,
                                decoration: formGlobal("Date"),
                                // initialValue: DateTime.now(),
                                firstDate: DateTime.now(),
                                initialEntryMode: DatePickerEntryMode.calendar,
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context)]),
                              ),
                            ),
                            fieldWidth,
                            Expanded(
                              child: FormBuilderDateTimePicker(
                                name: 'reminder_time',
                                onChanged: (value) {},
                                inputType: InputType.time,
                                decoration: formGlobal("Time"),
                                // initialValue: DateTime.now(),
                                firstDate: DateTime.now(),
                                validator: FormBuilderValidators.compose(
                                    [FormBuilderValidators.required(context)]),
                              ),
                            )
                          ],
                        ),
                        fieldDistance,
                        FormBuilderTextField(
                          name: "reminder_title",
                          decoration: formGlobal("Description"),
                          keyboardType: TextInputType.text,
                          // validators: [
                          //   FormBuilderValidators.required(),
                          // ],
                        ),
                        FormBuilderCheckbox(
                          activeColor: purple,
                          name: 'Isnotify',
                          title: Text("Notify", style: TextStyle(fontSize: 16)),
                          // leadingInput: true,
                          initialValue: false,
                          decoration: formGlobalcheckbox(),
                          onChanged: (value) {
                            // setState(() {
                            //   notifyDisplay = value;
                            // });
                          },
                        ),
                        FormBuilderCheckbox(
                          activeColor: purple,
                          name: 'Isnotify',
                          title: Text("SMS", style: TextStyle(fontSize: 16)),
                          // leadingInput: true,
                          initialValue: false,
                          decoration: formGlobalcheckbox(),
                          onChanged: (value) {
                            // setState(() {
                            //   notifyDisplay = value;
                            // });
                          },
                        ),
                        FormBuilderDropdown(
                          name: "Id",
                          decoration: formGlobal("Sender Id"),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: ["456321", "456987"]
                              .map((Id) => DropdownMenuItem(
                                  value: "title", child: Text("$Id")))
                              .toList(),
                        ),
                        FormBuilderCheckbox(
                          activeColor: purple,
                          name: 'Isnotify',
                          title: Text("Email", style: TextStyle(fontSize: 16)),
                          // leadingInput: true,
                          initialValue: false,
                          decoration: formGlobalcheckbox(),
                          onChanged: (value) {
                            // setState(() {
                            //   notifyDisplay = value;
                            // });
                          },
                        ),
                        FormBuilderDropdown(
                          name: "Mail",
                          decoration: formGlobal("Email to"),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required(context)]),
                          items: ["Jason.12@gmail.com", "Thomas.36@yahoo.com"]
                              .map((Mail) => DropdownMenuItem(
                                  value: "title", child: Text("$Mail")))
                              .toList(),
                        ),
                        fieldDistance,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            RaisedGradientButton(
                              child: customText('Reset', 16, black),
                              gradient: LinearGradient(colors: grayTtL),
                              width: 100,
                              onPressed: () {
                                _fbKey.currentState.reset();
                              },
                            ),
                            SizedBox(width: 10),
                            RaisedGradientButton(
                              child: buttonwhiteText('Submit'),
                              gradient: LinearGradient(colors: purpleTtL),
                              width: 100,
                              onPressed: () {
                                //Navigator.pushNamed(context, '/contacts');
                                if (_fbKey.currentState.saveAndValidate()) {
                                  print(_fbKey.currentState.value);
                                }
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
