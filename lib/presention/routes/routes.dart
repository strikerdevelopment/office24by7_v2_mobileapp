import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:office24by7_v2/_blocs/blocs.dart';
import 'package:office24by7_v2/_data/data.dart';
import 'package:http/http.dart' as http;
import 'package:office24by7_v2/presention/pages/pages.dart';

final LoginRepository loginRepository =
    LoginRepository(loginApiClient: LoginApiClient(httpClient: http.Client()));

final FirebaseRepository _firebaseRepository =
    FirebaseRepository(firebaseAPIClient: FirebaseAPIClient());

final ContactRepository _contactRepository =
    ContactRepository(contactAPIClient: ContactAPIClient());

final AuthenticationBloc authenticationBloc = AuthenticationBloc(
    loginRepository: loginRepository, firebaseRepository: _firebaseRepository);

final routes = {
  '/otppage': (context) => BlocProvider(
        create: (context) => OtpBloc(
            loginRepository: loginRepository,
            authenticationBloc: authenticationBloc,
            firebaseRepository: _firebaseRepository),
        child: OtpPage(),
      ),
  '/call_log': (context) => CallLogHome(),
  '/dash_boards': (context) => DashBoardHome(),
  //contact routes
  '/contacts': (context) => MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                ContactsBloc(contactRepository: _contactRepository),
          ),
          BlocProvider(
            create: (context) =>
                ContactGroupListBloc(contactRepository: _contactRepository),
          ),
          BlocProvider(
            create: (context) =>
                ContactSourceListBloc(contactRepository: _contactRepository),
          ),
          BlocProvider(
            create: (context) =>
                ContactCompanyListBloc(contactRepository: _contactRepository),
          ),
          BlocProvider(
            create: (context) =>
                ContactListListBloc(contactRepository: _contactRepository),
          ),
        ],
        child: ContactsHomeTabs(),
      ),

  '/dialer': (context) => DialerHome(),
  '/task_manager': (context) => TaskHome(),
  '/opportunities': (context) => OpportunitiesHome(),
  '/ticketing_manager': (context) => TicketHome(),
  '/performance': (context) => Performance(),
};
