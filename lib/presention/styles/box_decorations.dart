import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';

const BoxDecoration appBarDecoration = BoxDecoration(
  gradient: LinearGradient(
    colors: purpleTtL,
    begin: const FractionalOffset(0.0, 0.0),
    end: const FractionalOffset(0.5, 0.0),
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp,
  ),
  borderRadius: BorderRadius.only(
    bottomLeft: Radius.elliptical(500, 100),
    bottomRight: Radius.elliptical(500, 100),
  ),
);

BoxDecoration bodyBox = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.all(Radius.circular(20.0)),
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.3),
      spreadRadius: 2,
      blurRadius: 5,
      offset: Offset(0, 3), // changes position of shadow
    ),
  ],
);

BoxDecoration roundShadow = BoxDecoration(
  color: Colors.white,
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 2,
      blurRadius: 2,
      offset: Offset(3, 3), // changes position of shadow
    ),
  ],
);

BoxDecoration moreBox = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.only(
      topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.3),
      spreadRadius: 2,
      blurRadius: 5,
      offset: Offset(0, 3), // changes position of shadow
    ),
  ],
);

BoxDecoration moreListBox = BoxDecoration(
  color: bodyBG,
  borderRadius: BorderRadius.all(Radius.circular(10.0)),
);

BoxDecoration moreActiveListBox = BoxDecoration(
  gradient: LinearGradient(
    colors: purpleTtL,
    begin: const FractionalOffset(0.0, 0.0),
    end: const FractionalOffset(0.5, 0.5),
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp,
  ),
  borderRadius: BorderRadius.all(Radius.circular(10.0)),
);

BoxDecoration oppoBox = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.all(Radius.circular(15.0)),
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.3),
      spreadRadius: 2,
      blurRadius: 5,
      offset: Offset(0, 3), // changes position of shadow
    ),
  ],
);
