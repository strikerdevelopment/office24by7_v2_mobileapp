import 'package:flutter/material.dart';

const Color bodyBG = Color(0xFFF1F1F2);
const Color white = Colors.white;
const Color purple = Color(0xFFA66BF9);
const Color dartpurple = Color(0xFF7469EC);
const Color red = Colors.red;
const Color green = Colors.green;
const Color textGray = Color(0xFFA6A8AB);
const Color formBorderColor = Color(0xFFE0E0E0);
const Color black = Colors.black;
const Color orange = Color(0xFFf9ac1b);
const Color transparent = Colors.transparent;

const purpleTtL = <Color>[Color(0xFF7469EC), Color(0xFFA67DF4)];
const grayTtL = <Color>[Color(0xFFF1F1F2), Color(0xFFF1F1F2)];

const purpleTtW = <Color>[Color(0xFFbfb9f6), white];

const Color chatRightColor = Color(0xFFebe8fd);
const Color chatLeftColor = Color(0xFFd6f7ff);

Color hexColor(String hex) {
  final colorCode = hex == null ? '0xFFebe8fd' : hex.replaceAll('#', '0xFF');
  // print(colorCode);
  return Color(int.parse(colorCode));
}
