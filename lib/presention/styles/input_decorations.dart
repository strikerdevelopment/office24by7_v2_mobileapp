import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';

InputDecoration loginInput(String hintText) {
  return InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: white, width: 2),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: purple, width: 2),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    hintText: hintText,
    fillColor: white,
    filled: true,
    contentPadding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
  );
}

InputDecoration searchInput(String hintText) {
  return InputDecoration(
    suffixIcon: Icon(Icons.search),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: bodyBG, width: 2),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: purple, width: 2),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(120.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    hintText: hintText,
    fillColor: bodyBG,
    filled: true,
    contentPadding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
  );
}

FocusNode myFocusNode = new FocusNode();

InputDecoration formGlobal(String hintText) {
  return InputDecoration(
    //suffixIcon: Icon(Icons.search),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: formBorderColor, width: 2),
    ),

    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: purple, width: 2),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    disabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: formBorderColor, width: 2),
    ),
    hintText: hintText,
    labelText: hintText,
    //labelStyle: TextStyle(backgroundColor: red),
    fillColor: bodyBG,
    filled: false,
    contentPadding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
  );
}

InputDecoration formCustomGlobal(
    String hintText, FormFieldState<dynamic> field) {
  return InputDecoration(
    //suffixIcon: Icon(Icons.search),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: formBorderColor, width: 2),
    ),

    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: purple, width: 2),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    disabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
      borderSide: BorderSide(color: formBorderColor, width: 2),
    ),
    hintText: hintText,
    labelText: hintText,
    //labelStyle: TextStyle(backgroundColor: red),
    fillColor: bodyBG,
    filled: false,
    errorText: field.errorText,
    contentPadding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
  );
}

InputDecoration formGlobalcheckbox() {
  return InputDecoration(
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
    fillColor: bodyBG,
    filled: false,
    contentPadding: EdgeInsets.all(0),
  );
}

InputDecoration formGlobalcheckboxLabel(String label) {
  return InputDecoration(
    labelText: label,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
    fillColor: bodyBG,
    filled: false,
    contentPadding: EdgeInsets.all(0),
  );
}

InputDecoration formGlobalradiobox(String labeltext) {
  return InputDecoration(
    labelText: labeltext,
    border: InputBorder.none,
    focusedBorder: InputBorder.none,
    enabledBorder: InputBorder.none,
    errorBorder: InputBorder.none,
    disabledBorder: InputBorder.none,
    fillColor: bodyBG,
    filled: false,
    contentPadding: EdgeInsets.all(0),
  );
}

SizedBox fieldDistance = SizedBox(height: 15);
EdgeInsets fieldMargin = EdgeInsets.only(bottom: 15);
SizedBox fieldWidth = SizedBox(width: 15);

InputDecoration chatTextInput(String hintText) {
  return InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
      borderSide: BorderSide(color: bodyBG, width: 2),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
      borderSide: BorderSide(color: purple, width: 2),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
      borderSide: BorderSide(color: red, width: 2),
    ),
    hintText: hintText,
    fillColor: bodyBG,
    filled: true,
    contentPadding: EdgeInsets.only(top: 10.0, right: 30, bottom: 10, left: 10),
  );
}
