import 'package:flutter/cupertino.dart';

const EdgeInsets tabBodyPadd = EdgeInsets.all(10);
const EdgeInsets cardPadd =
    EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 6);

const EdgeInsets formpadding = EdgeInsets.all(20);
