import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';

Text buttonwhiteText(String data) {
  return Text(
    data,
    style: TextStyle(color: white, fontSize: 16),
  );
}

Text purpleText(String data, double size) {
  return Text(
    data,
    style: TextStyle(color: purple, fontSize: size),
  );
}

Text darkPurpleText(String data, double size) {
  return Text(
    data,
    style: TextStyle(color: dartpurple, fontSize: size),
  );
}

Text profileText(String data, double size) {
  return Text(
    data,
    overflow: TextOverflow.ellipsis,
    style: TextStyle(color: black, fontSize: size, fontWeight: FontWeight.bold),
  );
}

Text customText(String data, double size, Color color) {
  return Text(
    data,
    overflow: TextOverflow.ellipsis,
    style: TextStyle(color: color, fontSize: size),
  );
}

Text customBoldText(String data, double size, Color color) {
  return Text(
    data,
    overflow: TextOverflow.ellipsis,
    style: TextStyle(color: color, fontSize: size, fontWeight: FontWeight.bold),
  );
}

Text customTextRight(String data, double size, Color color) {
  return Text(
    data,
    overflow: TextOverflow.ellipsis,
    style: TextStyle(color: color, fontSize: size),
    textAlign: TextAlign.right,
  );
}
