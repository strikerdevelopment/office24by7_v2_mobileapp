import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';

ThemeData themeData = ThemeData(
  primaryColor: purple,
  fontFamily: 'GoogleSans',
  visualDensity: VisualDensity.adaptivePlatformDensity,
  appBarTheme: AppBarTheme(
    elevation: 0.0,
    color: Colors.transparent,
  ),
);

double safeAreaHeight(context) {
  return 55 + MediaQuery.of(context).padding.top;
}

Widget dateTimeTheme(BuildContext context, Widget child) {
  return Theme(
    data: ThemeData.light().copyWith(
      colorScheme: ColorScheme.light(
          //primary: purple,
          // onPrimary: Colors.white,
          //surface: dartpurple,
          // onSurface: black,
          ),
      //dialogBackgroundColor: white,
      primaryColor: dartpurple,
      accentColor: dartpurple,
      buttonColor: dartpurple,
    ),
    child: child,
  );
}
