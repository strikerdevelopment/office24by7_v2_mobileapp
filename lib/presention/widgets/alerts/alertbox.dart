import 'package:flutter/material.dart';

class AlertSuccessBox extends StatelessWidget {
  final Widget child;
  const AlertSuccessBox({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(10),
      color: Colors.green[100],
      child: child,
    );
  }
}

class AlertDangerBox extends StatelessWidget {
  final Widget child;
  const AlertDangerBox({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(10),
      color: Colors.red[100],
      child: child,
    );
  }
}
