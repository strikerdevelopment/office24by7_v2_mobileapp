import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';

class AppbarNormalChatHistory extends StatelessWidget {
  final Widget child;
  final List<Widget> actions;
  const AppbarNormalChatHistory({Key key, this.child, this.actions})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0.0,
      left: 0.0,
      right: 0.0,
      child: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: child,
          actions: actions),
    );
  }
}

class AppbarChatHistoryMultSelect extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  final List<Widget> actions;
  const AppbarChatHistoryMultSelect(
      {Key key, this.child, this.actions, this.onPressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0.0,
      left: 0.0,
      right: 0.0,
      child: AppBar(
          leading: IconButton(
            icon: Icon(Icons.close, color: white),
            onPressed: onPressed,
          ),
          title: child,
          actions: actions),
    );
  }
}
