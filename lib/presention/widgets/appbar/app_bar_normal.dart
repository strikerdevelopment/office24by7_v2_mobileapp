import 'package:flutter/material.dart';

class AppbarNormal extends StatelessWidget {
  final String title;
  final List<Widget> actions;
  const AppbarNormal({Key key, this.title, this.actions}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0.0,
      left: 0.0,
      right: 0.0,
      child: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(title),
          actions: actions),
    );
  }
}
