import 'package:flutter/material.dart';

class AppbarNotLeading extends StatelessWidget {
  final String title;
  final List<Widget> actions;
  const AppbarNotLeading({Key key, this.title, this.actions}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0.0,
      left: 0.0,
      right: 0.0,
      child: AppBar(title: Text(title), actions: actions),
    );
  }
}
