import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:office24by7_v2/presention/styles/colors.dart';
import 'package:office24by7_v2/presention/widgets/data_loaders/data_loaders.dart';

final player = AudioPlayer();

Future<void> audioPlayeBottomSheet(BuildContext context, String audioUrl) {
  return showModalBottomSheet(
    context: context,
    builder: (BuildContext context) {
      return audioUrl.isEmpty
          ? NoDataFound(message: "File Not Exist...")
          : AudioPlayeBottomSheet(audioUrl: audioUrl);
    },
  ).whenComplete(() => player.stop());
}

class AudioPlayeBottomSheet extends StatefulWidget {
  final String audioUrl;

  const AudioPlayeBottomSheet({Key key, @required this.audioUrl})
      : super(key: key);

  @override
  _AudioPlayeBottomSheetState createState() => _AudioPlayeBottomSheetState();
}

class _AudioPlayeBottomSheetState extends State<AudioPlayeBottomSheet> {
  double _duration = 0;
  double _position = 0;
  bool togglePlayPause = true;

  @override
  void initState() {
    setUrl();
    player.durationStream.listen((state) {
      setState(() => _duration = state.inSeconds.toDouble());
    });
    player.positionStream.listen((state) {
      setState(() => _position = state.inSeconds.toDouble());
      if (_duration == _position) {
        setState(() {
          togglePlayPause = true;
          _position = 0;
        });
      }
    });
    super.initState();
  }

  setUrl() async {
    try {
      await player.setUrl(widget.audioUrl);
    } on PlayerException catch (e) {
      print("Error code: ${e.code}");
      print("Error message: ${e.message}");
    } on PlayerInterruptedException catch (e) {
      print("Connection aborted: ${e.message}");
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 80,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              togglePlayPause
                  ? IconButton(
                      onPressed: () {
                        player.play();
                        setState(() => togglePlayPause = false);
                      },
                      icon: Icon(Icons.play_arrow),
                    )
                  : IconButton(
                      onPressed: () {
                        player.pause();
                        setState(() => togglePlayPause = true);
                      },
                      icon: Icon(Icons.pause),
                    ),
              Text(durationToString(_position.toInt())),
              Slider(
                activeColor: dartpurple,
                max: _duration,
                min: 0,
                value: _position,
                onChanged: (value) async {
                  setState(() => _position = value);
                  await player.seek(Duration(seconds: _position.toInt()));
                },
              ),
              Text(durationToString(_duration.toInt())),
            ],
          )
        ],
      ),
    );
  }

  String durationToString(int minutes) {
    var d = Duration(minutes: minutes);
    List<String> parts = d.toString().split(':');
    return '${parts[0].padLeft(2, '0')}:${parts[1].padLeft(2, '0')}';
  }
}
