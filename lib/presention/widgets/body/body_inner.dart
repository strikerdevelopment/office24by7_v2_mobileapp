import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';

class BodyInner extends StatelessWidget {
  final Widget page;

  const BodyInner({Key key, this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: safeAreaHeight(context),
      left: 0.0,
      right: 0.0,
      bottom: 0.0,
      child: Container(
        padding: EdgeInsets.only(top: 0, bottom: 0, left: 10, right: 10),
        child: Container(decoration: moreBox, child: page),
      ),
    );
  }
}
