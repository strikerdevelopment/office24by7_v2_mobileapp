import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';

class BodyWithTabSpace extends StatelessWidget {
  final Widget page;

  const BodyWithTabSpace({Key key, this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: safeAreaHeight(context),
      left: 0.0,
      right: 0.0,
      bottom: 0.0,
      child: page,
    );
  }
}

class TabBody extends StatelessWidget {
  final Widget tabPage;
  const TabBody({Key key, this.tabPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 10, right: 10),
      child: Container(decoration: moreBox, child: tabPage),
    );
  }
}

class OppoTabBody extends StatelessWidget {
  final Widget tabPage;
  const OppoTabBody({Key key, this.tabPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 10, right: 10),
      child: Container(child: tabPage),
    );
  }
}
