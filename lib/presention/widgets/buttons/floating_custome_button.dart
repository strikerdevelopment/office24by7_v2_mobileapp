import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';

class FloatingButtonCustome extends StatelessWidget {
  final Function onPressed;
  final Icon icon;

  const FloatingButtonCustome({Key key, this.onPressed, this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPressed,
      child: Container(
        width: 60,
        height: 60,
        child: icon,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(colors: purpleTtL)),
      ),
    );
  }
}
