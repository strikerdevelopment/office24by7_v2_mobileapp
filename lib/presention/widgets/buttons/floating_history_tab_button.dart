import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/loaders/image_load.dart';

class FloatingHistoryTabButton extends StatelessWidget {
  final ContactDetailsTabs data;
  final int index;
  final Function onPressed;

  const FloatingHistoryTabButton(
      {Key key, this.data, this.index, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 55,
          height: 55,
          padding: EdgeInsets.symmetric(vertical: 8),
          child: FloatingActionButton(
            heroTag: data.img,
            onPressed: onPressed,
            child: Container(
              width: 55,
              height: 55,
              padding: EdgeInsets.all(8),
              child: ImageLoad(
                img: data.activeimg,
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: purpleTtL)),
            ),
          ),
        ),
        //SizedBox(height: 5),
        // Text(data.iconName),
      ],
    );
  }
}

class FloatingHistoryOppoTabButton extends StatelessWidget {
  final OppoDetailsTabs data;
  final int index;
  final Function onPressed;

  const FloatingHistoryOppoTabButton(
      {Key key, this.data, this.index, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 55,
          height: 55,
          padding: EdgeInsets.symmetric(vertical: 8),
          child: FloatingActionButton(
            heroTag: data.img,
            onPressed: onPressed,
            child: Container(
              width: 55,
              height: 55,
              padding: EdgeInsets.all(8),
              child: ImageLoad(
                img: data.activeimg,
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: purpleTtL)),
            ),
          ),
        ),
        //SizedBox(height: 5),
        // Text(data.iconName),
      ],
    );
  }
}

class FloatingCallsLogTabButton extends StatelessWidget {
  final CallsLogTabModel data;
  final int index;
  final Function onPressed;

  const FloatingCallsLogTabButton(
      {Key key, this.data, this.index, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 55,
          height: 55,
          padding: EdgeInsets.symmetric(vertical: 8),
          child: FloatingActionButton(
            heroTag: data.img,
            onPressed: onPressed,
            child: Container(
              width: 55,
              height: 55,
              padding: EdgeInsets.all(8),
              child: ImageLoad(
                img: data.activeimg,
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: purpleTtL)),
            ),
          ),
        ),
        //SizedBox(height: 5),
        // Text(data.iconName),
      ],
    );
  }
}
