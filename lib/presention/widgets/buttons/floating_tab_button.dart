import 'package:flutter/material.dart';
import 'package:office24by7_v2/_models/models.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/loaders/image_load.dart';

class FloatingTabButton extends StatelessWidget {
  final ContactDetailsTabs data;
  final int index;
  final Function onPressed;

  const FloatingTabButton({Key key, this.data, this.index, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 50,
          height: 50,
          child: FloatingActionButton(
            heroTag: data.img,
            onPressed: onPressed,
            child: Container(
              width: 60,
              height: 60,
              padding: EdgeInsets.all(8),
              child: ImageLoad(
                img: data.activeimg,
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: purpleTtL)),
            ),
          ),
        ),
        SizedBox(height: 5),
        Text(data.iconName),
      ],
    );
  }
}

class FloatingTabOppoButton extends StatelessWidget {
  final OppoDetailsTabs data;
  final int index;
  final Function onPressed;

  const FloatingTabOppoButton({Key key, this.data, this.index, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 50,
          height: 50,
          child: FloatingActionButton(
            heroTag: data.img,
            onPressed: onPressed,
            child: Container(
              width: 60,
              height: 60,
              padding: EdgeInsets.all(8),
              child: ImageLoad(
                img: data.activeimg,
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(colors: purpleTtL)),
            ),
          ),
        ),
        SizedBox(height: 5),
        Text(data.iconName),
      ],
    );
  }
}
