import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';

class SearchButton extends StatelessWidget {
  final String text;
  final Function onClick;
  const SearchButton({Key key, this.text, this.onClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: 50,
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: bodyBG,
              borderRadius: BorderRadius.circular(60),
            ),
            child:
                Text("$text", style: TextStyle(fontSize: 16, color: textGray)),
          ),
          Positioned(
            right: 15,
            top: 12,
            child: Icon(Icons.search),
          ),
        ],
      ),
    );
  }
}
