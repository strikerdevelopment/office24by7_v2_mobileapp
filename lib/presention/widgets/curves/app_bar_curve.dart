import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';

class AppbarCurve extends StatelessWidget {
  const AppbarCurve({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      left: 0.0,
      right: 0.0,
      child: Container(
        height: 250,
        decoration: appBarDecoration,
      ),
    );
  }
}
