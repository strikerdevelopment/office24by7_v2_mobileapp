import 'package:flutter/material.dart';

convertColor(String color) {
  String result = '0xFF${color.replaceAll("#", "")}';
  return Color(int.parse(result));
}
