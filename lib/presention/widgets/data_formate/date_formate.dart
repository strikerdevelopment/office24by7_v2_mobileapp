import 'package:intl/intl.dart';

String formateDateTimeWidget(DateTime data) {
  if (data == null) {
    return null;
  }
  return DateFormat('yyyy-MM-dd kk:mm').format(data);
}

String formateDateTimeAPI(DateTime data) {
  if (data == null) {
    return null;
  }
  return DateFormat('yyyy-MM-dd kk:mm:ss').format(data);
}

String formateDateAPI(DateTime data) {
  if (data == null) {
    return null;
  }
  return DateFormat('yyyy-MM-dd').format(data);
}

String formateCallsLogDT(DateTime data) {
  if (data == null) {
    return null;
  }
  return DateFormat('MMM dd, yyyy, kk:mm:ss').format(data);
}
