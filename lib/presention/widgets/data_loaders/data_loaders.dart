import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/loaders/image_load.dart';

class NoDataFound extends StatelessWidget {
  final String message;
  const NoDataFound({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ImageLoad(img: "/mobile_app_v2/no_data_found.png", width: 150),
            Text(message, style: TextStyle(color: textGray)),
          ],
        ),
      ),
    );
  }
}

class FailedToLoad extends StatelessWidget {
  final String message;
  const FailedToLoad({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ImageLoad(img: "/mobile_app_v2/error_loading_data.png", width: 150),
            Text(message, style: TextStyle(color: textGray)),
          ],
        ),
      ),
    );
  }
}
