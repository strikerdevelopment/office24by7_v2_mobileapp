import 'package:flutter/material.dart';

class ModalBox extends StatelessWidget {
  final Widget modalHeader;
  final Widget modalBody;
  final Widget modalFooter;
  final double modalHieght;

  const ModalBox(
      {this.modalHeader,
      this.modalBody,
      this.modalFooter,
      this.modalHieght,
      Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.all(15),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        height: modalHieght,
        child: Column(
          children: [modalHeader, modalBody, modalFooter],
        ),
      ),
    );
  }
}
