import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ModalFooter extends StatelessWidget {
  final String firstButton;
  final String secondButton;
  final Function firstFun;
  final Function secondFun;

  const ModalFooter(
      {Key key,
      this.firstButton,
      this.firstFun,
      this.secondButton,
      this.secondFun})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 1, color: textGray),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RaisedGradientButton(
            child: customText(firstButton, 16, black),
            gradient: LinearGradient(colors: grayTtL),
            width: 100,
            onPressed: firstFun,
          ),
          SizedBox(width: 10),
          RaisedGradientButton(
            child: buttonwhiteText(secondButton),
            gradient: LinearGradient(colors: purpleTtL),
            width: 100,
            onPressed: secondFun,
          ),
          SizedBox(width: 10.0),
        ],
      ),
    );
  }
}

class ModalTextFooter extends StatelessWidget {
  const ModalTextFooter({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
    );
  }
}
