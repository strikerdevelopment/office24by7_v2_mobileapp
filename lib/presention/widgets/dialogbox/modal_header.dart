import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class ModalHeader extends StatelessWidget {
  final String img;
  final String title;

  const ModalHeader({Key key, this.img, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(15.0, 8, 5, 8),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: textGray),
        ),
      ),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: bodyBG,
                borderRadius: BorderRadius.all(Radius.circular(50))),
            child: ImageLoad(
              img: img,
              width: 20,
            ),
          ),
          SizedBox(width: 10),
          Expanded(
              child: Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          )),
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }
}
