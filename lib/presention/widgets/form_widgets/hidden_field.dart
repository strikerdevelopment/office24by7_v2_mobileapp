import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class FormBuilderHiddenField extends StatelessWidget {
  final String value;
  const FormBuilderHiddenField({Key key, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 0,
      child: IgnorePointer(
        ignoring: true,
        child: Opacity(
          opacity: 0,
          child: FormBuilderTextField(
            name: value,
          ),
        ),
      ),
    );
  }
}
