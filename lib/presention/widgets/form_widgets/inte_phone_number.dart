export 'inte_phone_number.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';

class IntePhoneNumber extends StatefulWidget {
  final FormFieldState<dynamic> field;
  final String iso;
  final String placeholder;
  final String initialPhoneNumber;
  final List<String> countries;
  IntePhoneNumber({
    Key key,
    @required this.field,
    this.initialPhoneNumber,
    this.iso = 'IN',
    this.placeholder = 'Phone Number',
    @required this.countries,
  }) : super(key: key);

  @override
  _IntePhoneNumberState createState() => _IntePhoneNumberState();
}

class _IntePhoneNumberState extends State<IntePhoneNumber> {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return IntlPhoneField(
      decoration: formGlobal(widget.placeholder),
      initialCountryCode: widget.iso,
      initialValue: widget.initialPhoneNumber,
      countries: widget.countries,
      autoValidate: false,
      onChanged: (phone) {
        widget.field.didChange("${phone.countryCode} ${phone.number}");
        // print("${phone.countryCode} ${phone.number}");
      },
      validator: (value) {
        String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
        RegExp regExp = new RegExp(pattern);
        if (value.length == 0) {
          return 'Please enter mobile number';
        }
        if (!regExp.hasMatch(value)) {
          return 'Please enter valid mobile number';
        }
        return null;
      },
    );
  }
}
