import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_summernote/flutter_summernote.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class SummerNoteTextEditor extends StatefulWidget {
  final FormFieldState<dynamic> field;
  SummerNoteTextEditor({Key key, this.field}) : super(key: key);

  @override
  _SummerNoteTextEditorState createState() => _SummerNoteTextEditorState();
}

class _SummerNoteTextEditorState extends State<SummerNoteTextEditor> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                padding: EdgeInsets.all(15),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  border: Border.all(
                      width: 2,
                      color: widget.field.errorText == null
                          ? formBorderColor
                          : red),
                ),
                child: widget.field.value == null
                    ? Text("Enter Message")
                    : Html(data: """${widget.field.value}"""),
              ),
              Positioned(
                top: -7,
                left: 14,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 3),
                  color: Colors.white,
                  child: widget.field.value != null
                      ? Text(
                          "Enter Message",
                          style: TextStyle(color: Colors.grey, fontSize: 12),
                        )
                      : Container(),
                ),
              )
            ],
          ),
          widget.field.errorText == null
              ? Container()
              : Container(
                  padding: EdgeInsets.only(top: 10, left: 15),
                  child: Text(
                    widget.field.errorText,
                    style: TextStyle(color: red, fontSize: 12),
                  ),
                ),
        ],
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SummerNoteEditor(field: widget.field)),
        );
      },
    );
  }
}

class SummerNoteEditor extends StatefulWidget {
  final FormFieldState<dynamic> field;
  SummerNoteEditor({Key key, this.field}) : super(key: key);

  @override
  _SummerNoteEditorState createState() => _SummerNoteEditorState();
}

class _SummerNoteEditorState extends State<SummerNoteEditor> {
  GlobalKey<FlutterSummernoteState> _keyEditor = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bodyBG,
        child: Stack(
          children: [
            AppbarCurve(),
            AppbarNormal(
              title: "Enter Text",
              actions: [
                IconButton(
                  icon: Icon(Icons.save),
                  onPressed: () async {
                    await _keyEditor.currentState.getText();
                    Navigator.pop(context);
                  },
                )
              ],
            ),
            BodyInner(
              page: FlutterSummernote(
                hint: "",
                key: _keyEditor,
                hasAttachment: true,
                value: widget.field.value,
                returnContent: (value) {
                  widget.field.didChange(value);
                },
                customToolbar: """
                [
                  ['style', ['bold', 'italic', 'underline', 'clear']],
                  ['font', ['strikethrough', 'superscript', 'subscript']],
                  ['insert', ['link', 'table', 'hr']]
                ]
                """,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
