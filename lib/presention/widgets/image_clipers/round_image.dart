import 'package:flutter/cupertino.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class RoundImage extends StatelessWidget {
  final double width;
  final double height;
  final String img;
  const RoundImage({Key key, this.height, this.img, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(
        Radius.circular(250),
      ),
      child: Container(
        decoration: roundShadow,
        height: height,
        width: width,
        child: ImageLoad(img: img),
      ),
    );
  }
}
