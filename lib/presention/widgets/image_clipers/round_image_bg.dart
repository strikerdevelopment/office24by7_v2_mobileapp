import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/widgets/widgets.dart';

class RoundedImageBg extends StatelessWidget {
  final double width;
  final double height;
  final String img;
  final Color bgColor;
  final double padding;
  final Function onPressed;

  const RoundedImageBg(
      {Key key,
      this.bgColor,
      this.height,
      this.img,
      this.onPressed,
      this.width,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: BorderRadius.circular(50),
      ),
      child: IconButton(
        //tooltip: "lead activity",
        padding: EdgeInsets.all(padding),
        icon: ImageLoad(
          img: img,
        ),
        onPressed: onPressed,
      ),
    );
  }
}
