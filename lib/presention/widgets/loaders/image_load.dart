import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:office24by7_v2/_environment/environment.dart';
import 'package:office24by7_v2/presention/widgets/loaders/loader_indicator.dart';

class ImageLoad extends StatelessWidget {
  final String img;
  final double width;
  const ImageLoad({Key key, this.img, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "${Environment.cdnUrl}$img",
      width: width,
    );
    // return CachedNetworkImage(
    //   width: width,
    //   imageUrl: '${Environment.cdnUrl}$img',
    //   //placeholder: (context, url) => CircularProgressIndicator(),
    //   placeholder: (context, url) => Container(
    //     child: Text(''),
    //   ),
    //   errorWidget: (context, url, error) => Icon(Icons.error),
    // );
  }
}

class ImageChatLoad extends StatelessWidget {
  final String img;
  final double width;
  const ImageChatLoad({Key key, this.img, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      width: width,
      imageUrl: img,
      placeholder: (context, url) => LoaderSmallIndicator(),
      // placeholder: (context, url) => Container(
      //   child: Text(''),
      // ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
