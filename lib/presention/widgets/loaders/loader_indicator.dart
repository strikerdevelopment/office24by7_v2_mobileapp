import 'package:flutter/material.dart';
import 'package:office24by7_v2/presention/styles/styles.dart';

class LoaderIndicator extends StatelessWidget {
  const LoaderIndicator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Center(
          child: CircularProgressIndicator(
            strokeWidth: 2,
            valueColor: AlwaysStoppedAnimation<Color>(purple),
          ),
        ),
      ),
    );
  }
}

class LoaderSmallIndicator extends StatelessWidget {
  const LoaderSmallIndicator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Center(
          child: SizedBox(
            width: 20,
            height: 20,
            child: CircularProgressIndicator(
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(purple),
            ),
          ),
        ),
      ),
    );
  }
}

class LoaderInLineIndicator extends StatelessWidget {
  const LoaderInLineIndicator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 20,
      height: 20,
      child: CircularProgressIndicator(
        strokeWidth: 2,
        valueColor: AlwaysStoppedAnimation<Color>(purple),
      ),
    );
  }
}

class WhiteLoaderIndicator extends StatelessWidget {
  const WhiteLoaderIndicator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Center(
          child: CircularProgressIndicator(
            strokeWidth: 2,
            valueColor: AlwaysStoppedAnimation<Color>(white),
          ),
        ),
      ),
    );
  }
}

class WhiteLoaderSmallIndicator extends StatelessWidget {
  const WhiteLoaderSmallIndicator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 20,
      height: 20,
      child: CircularProgressIndicator(
        strokeWidth: 2,
        valueColor: AlwaysStoppedAnimation<Color>(white),
      ),
    );
  }
}

class FullyLoaderIndicator extends StatelessWidget {
  const FullyLoaderIndicator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Center(
            child: CircularProgressIndicator(
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(purple),
            ),
          ),
        ),
      ),
    );
  }
}
