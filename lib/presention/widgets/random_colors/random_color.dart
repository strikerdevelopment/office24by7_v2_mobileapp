import 'package:flutter/material.dart';
import 'package:random_color/random_color.dart';

RandomColor _randomColor = RandomColor();

Color randomColor() {
  return _randomColor.randomColor(colorBrightness: ColorBrightness.light);
}
