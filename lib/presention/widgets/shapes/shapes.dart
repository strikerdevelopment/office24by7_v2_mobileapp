import 'package:flutter/material.dart';

class RoundCircle extends StatelessWidget {
  final Color color;
  final double bottom;
  final double right;
  const RoundCircle({Key key, this.color, this.bottom = 0, this.right = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: bottom,
      right: right,
      child: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50), color: color),
      ),
    );
  }
}

class RoundCircleText extends StatelessWidget {
  final List<Color> color;
  final Widget child;
  final double width;
  final double height;
  const RoundCircleText(
      {Key key, this.color, this.child, this.width = 30, this.height = 30})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: child),
      width: width,
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: LinearGradient(colors: color)),
    );
  }
}
