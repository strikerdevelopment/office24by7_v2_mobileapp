class StringSplit {
  final String name;

  const StringSplit({this.name}) : assert(name != null);

  String twoLetter() {
    List<String> names = name.split(" ");
    String initials = "";
    int numWords = 2;

    // if (numWords < names.length) {
    //   numWords = names.length;
    // }
    if (names.length == 2) {
      for (var i = 0; i < numWords; i++) {
        initials += '${names[i][0]}';
      }
      return initials.toUpperCase();
    } else {
      return '${name[0]}${name[1]}'.toUpperCase();
    }
  }
}
