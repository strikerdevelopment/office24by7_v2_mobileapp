import 'package:flutter/cupertino.dart';
import 'package:new_version/new_version.dart';

void checkVersion(BuildContext context) async {
  final newVersion = NewVersion(
    androidId: "com.office24by7.office24by7_v2",
    iOSId: "com.office24by7.office24by7V2",
  );

  final status = await newVersion.getVersionStatus();

  if (status.canUpdate) {
    newVersion.showUpdateDialog(
      context: context,
      versionStatus: status,
      dialogTitle: "UPDATE!!!",
      dismissButtonText: "Skip",
      dialogText: "Please update the app from " +
          "${status.localVersion}" +
          " to " +
          "${status.storeVersion}",
      dismissAction: () {
        Navigator.pop(context);
      },
      updateButtonText: "Lets update",
    );
  }
  print("DEVICE : " + status.localVersion);
  print("STORE : " + status.storeVersion);
}
